The aim of publishing the source code is to establish and build confidence in the public domain, while obtaining feedback from professional experts and the opportunity to make improvements. Constructive contributions and findings on the source code are always welcome.
At this stage, we would like to highlight the key points in the terms of use:

*  The OEV (Federal Chancellery Ordinance on Electronic Voting) allows the source code to be examined, modified, compiled and executed for ideational purposes. Publication is organized and regulated in line with this idea.
*  The published code, including the source code, is the intellectual property of the companies Scytl and Post CH Ltd. The code in question is proprietary and not subject to a free and open source software (FOSS) licence.
*  Distributing or publishing the access tools, granting access to third parties and sharing the source code are not permitted.
*  Post CH Ltd must be consulted before any findings may be published. 
*  All feedback regarding vulnerabilities found in the electronic voting solution source code is welcome. All feedback must be reported on the GitLab.com platform as an issue and set to “confidential”. Swiss Post will incorporate the reported vulnerabilities into the development of the system.


Please refer to the [Wiki](https://gitlab.com/swisspost/evoting-solution/wikis/home) for complementary information as well as a presentation of the published Source Code