/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.domain;

public enum LogField {

    OBJECTTYPE("ObjectType"),
    OBJECTID("ObjectId"),
    USER("User"),
    ELECTIONEVENT("ElectionEvent");

    private String fieldName;

    private LogField(String name) {
        fieldName = name;
    }

    public String getFieldName() {
        return fieldName;
    }
}
