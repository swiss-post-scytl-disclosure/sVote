/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.formatter;

import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;

/**
 * Specifies the log message format, it will be used by {@link LoggingWriter}.
 */
public interface MessageFormatter {

    /**
     * Given a {@link LogContent} that contains all log information, this method
     * builds a specific message.
     *
     * @param logContent
     * @return String log message
     */
    String buildMessage(LogContent logContent);

}
