/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.factory;

import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;

/**
 * Based on factory method pattern, it makes the logging easily extensible. The
 * idea allow children decide which log technology use, techologies such as
 * log4j, logback, etc.
 */
public interface SecureLoggingFactory {

    /**
     * Given a class type the factory will return and log instance for this
     * class.
     *
     * @param clazz
     * @return LoggingWriter
     */
    SecureLoggingWriter getLogger(final Class<?> clazz);

    /**
     * Given a logger name the factory will return and log instance for the
     * name.
     *
     * @param loggerName Logger name
     * @return LoggingWriter
     */
    SecureLoggingWriter getLogger(final String loggerName);
}
