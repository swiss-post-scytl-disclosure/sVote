/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.domain;

/**
 * Defines mandatory methods necessary to identify a log
 */
public interface LogEvent {

    public String getLayer();

    public String getAction();

    public String getOutcome();

    public String getInfo();
}
