/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.writer;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;

/**
 * Controls the levels logs and the process of log message construction
 */
public interface LoggingWriter {

    /**
     * Given an log level and a POJO that contains all the log information,
     * creates and writes a log in a file, database or anything else depending
     * on the chosen implementation.
     *
     * @param level
     * @param logContent
     */
    void log(Level level, LogContent logContent);

}
