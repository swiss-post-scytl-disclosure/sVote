/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.domain;

public class TransactionInfo {

    private final Long trackId;

    private final String tenant;

    private final String clientIpAddress;

    private final String serverIpAddress;

    public TransactionInfo(final Long trackId, final String tenant, final String clientIpAddress,
            final String serverIpAddress) {
        this.trackId = trackId;
        this.tenant = tenant;
        this.clientIpAddress = clientIpAddress;
        this.serverIpAddress = serverIpAddress;
    }

    public Long getTrackId() {
        return trackId;
    }

    public String getClientIpAddress() {
        return clientIpAddress;
    }

    public String getServerIpAddress() {
        return serverIpAddress;
    }

    public String getTenant() {
        return tenant;
    }
}
