/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.domain;

/**
 * Defines the logging levels supported by the logging library
 */
public enum Level {
    INFO, DEBUG, ERROR, WARN
}
