/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.domain;

/**
 * Test implementation
 */
public enum LogEventTest implements LogEvent {

    TEST_EVENT("TEST", "ACTION", "OUTCOME", "INFO");

    private final String layer;

    private final String action;

    private final String outcome;

    private final String info;

    private LogEventTest(final String layer, final String action,
            final String outcome, final String info) {
        this.layer = layer;
        this.action = action;
        this.outcome = outcome;
        this.info = info;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getAction()
     */
    @Override
    public String getAction() {
        return action;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getOutcome()
     */
    @Override
    public String getOutcome() {
        return outcome;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getInfo()
     */
    @Override
    public String getInfo() {
        return info;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getLayer()
     */
    @Override
    public String getLayer() {
        return layer;
    }

}
