/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.api.domain;

import com.scytl.products.oscore.logging.api.domain.LogContent.LogContentBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Unit test for {@link LogContent}
 *
 * @author jalves
 */
public class LogContentTest {

    @Test
    public void whenBuildLogInfoThenCheckAttributes() {

        LogContentBuilder builder = new LogContentBuilder();

        builder.logEvent(LogEventTest.TEST_EVENT);
        builder.objectType("objectType");
        builder.objectId("objectId");
        builder.user("userID");
        builder.electionEvent("electionEvent");

        final Map<String, String> additionalInfo = new HashMap<String, String>();
        additionalInfo.put("exception", "message");

        builder.additionalInfo("key", "value");
        builder.additionalInfo("key2", "value2");

        LogContent logContent = builder.createLogInfo();

        Assert.assertEquals("objectType", logContent.getObjectType());
        Assert.assertEquals("objectId", logContent.getObjectId());

        String value = logContent.getAdditionalInfo().get("key");
        String value2 = logContent.getAdditionalInfo().get("key2");

        Assert.assertNotNull(value);
        Assert.assertNotNull(value2);
        Assert.assertEquals("value", value);
        Assert.assertEquals("value2", value2);
    }
}
