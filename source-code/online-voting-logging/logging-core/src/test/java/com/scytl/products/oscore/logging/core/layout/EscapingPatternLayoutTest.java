/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.core.layout;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.security.Security;

public class EscapingPatternLayoutTest {
    
    @BeforeClass
    public static void beforeClass() {
        if (Security
            .getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Test
    public void testFormat() throws Exception {
        EscapingPatternLayout layout = new EscapingPatternLayout();
        LoggingEvent event = new LoggingEvent(this.getClass().getCanonicalName(), Logger
                .getRootLogger(), Level.INFO, "logging\n\r\b\f|message", null);
        String format = layout.format(event);

        assertEquals("logging(n)(r)(b)(f)|message" + System.getProperty("line.separator"), format);
    }
}
