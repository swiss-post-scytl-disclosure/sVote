/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.core.itest;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.domain.LogField;
import com.scytl.products.oscore.logging.api.exceptions.LoggingException;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.core.formatter.SplunkFormatter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;

public class LogRequiredFieldsTest {

    @Test
    public void shouldNotThrowIfRequiredFieldsIsEmpty() {

        TransactionInfoProvider transactionInfoProvider = new TransactionInfoProvider();
        transactionInfoProvider.generate("","","");

        LogContent.LogContentBuilder builder = new LogContent.LogContentBuilder();

        builder.logEvent(LogEventTest.TEST_EVENT);
        LogContent logContent = builder.createLogInfo();

        MessageFormatter formatter = new SplunkFormatter("","",transactionInfoProvider);
        try {
            formatter.buildMessage(logContent);
        } catch (LoggingException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void shouldNotThrowIfRequiredFieldsIsNull() {

        TransactionInfoProvider transactionInfoProvider = new TransactionInfoProvider();
        transactionInfoProvider.generate("","","");

        LogContent.LogContentBuilder builder = new LogContent.LogContentBuilder();

        builder.logEvent(LogEventTest.TEST_EVENT);
        LogContent logContent = builder.createLogInfo();


        MessageFormatter formatter = new SplunkFormatter("","",transactionInfoProvider, null);
        formatter.buildMessage(logContent);

    }

    @Test(expected = LoggingException.class)
    public void shouldThrowIfRequiredFieldIsNotPresent() {

        TransactionInfoProvider transactionInfoProvider = new TransactionInfoProvider();
        transactionInfoProvider.generate("","","");

        LogContent.LogContentBuilder builder = new LogContent.LogContentBuilder();

        builder.logEvent(LogEventTest.TEST_EVENT);
        LogContent logContent = builder.createLogInfo();


        MessageFormatter formatter = new SplunkFormatter("","",transactionInfoProvider, LogField.USER);
        formatter.buildMessage(logContent);

    }

    @Test
    public void shouldNotThrowIfRequiredFieldIsPresent() {

        TransactionInfoProvider transactionInfoProvider = new TransactionInfoProvider();
        transactionInfoProvider.generate("","","");

        LogContent.LogContentBuilder builder = new LogContent.LogContentBuilder();

        builder.logEvent(LogEventTest.TEST_EVENT);
        builder.user("test");
        LogContent logContent = builder.createLogInfo();


        MessageFormatter formatter =
            new SplunkFormatter("","",transactionInfoProvider, LogField.USER);
        formatter.buildMessage(logContent);

    }

    @Test
    public void shouldNotThrowIfManyRequiredFieldArePresent() {

        TransactionInfoProvider transactionInfoProvider = new TransactionInfoProvider();
        transactionInfoProvider.generate("","","");

        LogContent.LogContentBuilder builder = new LogContent.LogContentBuilder();

        builder.logEvent(LogEventTest.TEST_EVENT);
        builder.objectId("object id test");
        builder.user("test");
        LogContent logContent = builder.createLogInfo();


        MessageFormatter formatter =
            new SplunkFormatter("","",transactionInfoProvider, LogField.USER,
                LogField.OBJECTID);
        formatter.buildMessage(logContent);

    }

    @Test(expected = LoggingException.class)
    public void shouldThrowIfOneOfManyRequiredFieldsIsNotPresent() {

        TransactionInfoProvider transactionInfoProvider = new TransactionInfoProvider();
        transactionInfoProvider.generate("","","");

        LogContent.LogContentBuilder builder = new LogContent.LogContentBuilder();

        builder.logEvent(LogEventTest.TEST_EVENT);
        builder.objectId("object id test");
        LogContent logContent = builder.createLogInfo();


        MessageFormatter formatter =
            new SplunkFormatter("","",transactionInfoProvider, LogField.USER,
                LogField.OBJECTID);
        formatter.buildMessage(logContent);

    }
}
