/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.core.writer;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import org.apache.log4j.Logger;

/**
 * @author Fulgencio R. Jara (fulgencio.jara@scytl.com)
 */
public class CommonsLoggingWriterLog4j {

    protected final Logger logger;

    protected final MessageFormatter messageFormatter;

    public CommonsLoggingWriterLog4j(final Logger logger,
            final MessageFormatter messageFormatter) {
        this.logger = logger;
        this.messageFormatter = messageFormatter;
    }

    protected void addMessageWithLevel(final Level level,
            final String message) {

        if (level == Level.INFO) {
            logger.info(message);
        } else if (level == Level.ERROR) {
            logger.error(message);
        } else if (level == Level.WARN) {
            logger.warn(message);
        } else if (level == Level.DEBUG) {
            logger.debug(message);
        }
    }
}
