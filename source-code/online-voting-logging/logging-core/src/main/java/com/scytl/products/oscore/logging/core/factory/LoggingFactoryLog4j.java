/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.core.factory;

import com.scytl.products.oscore.logging.api.factory.LoggingFactory;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.oscore.logging.core.writer.LoggingWriterLog4j;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LoggingFactoryLog4j implements LoggingFactory {
    private final MessageFormatter messageFormatter;

    public LoggingFactoryLog4j(final MessageFormatter messageFormatter) {
        this.messageFormatter = messageFormatter;
    }

    /**
     * @see LoggingFactory#getLogger(java.lang.Class)
     */
    @Override
    public LoggingWriter getLogger(final Class<?> clazz) {
        final Logger logger = LogManager.getLogger(clazz);

        return new LoggingWriterLog4j(logger, messageFormatter);
    }

    @Override
    public LoggingWriter getLogger(final String loggerName) {
        final Logger logger = LogManager.getLogger(loggerName);

        return new LoggingWriterLog4j(logger, messageFormatter);
    }

}
