/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.core.factory;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.scytl.products.oscore.logging.api.factory.LoggingFactory;
import com.scytl.products.oscore.logging.api.factory.SecureLoggingFactory;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.writer.SecureLoggingWriterLog4j;

public class SecureLoggingFactoryLog4j implements SecureLoggingFactory {
    private final MessageFormatter messageFormatter;

    public SecureLoggingFactoryLog4j(final MessageFormatter messageFormatter) {
        this.messageFormatter = messageFormatter;
    }

    /**
     * @see LoggingFactory#getLogger(Class)
     */
    @Override
    public SecureLoggingWriter getLogger(final Class<?> clazz) {
        final Logger logger = LogManager.getLogger(clazz);

        return new SecureLoggingWriterLog4j(logger, messageFormatter);
    }

    @Override
    public SecureLoggingWriter getLogger(final String loggerName) {
        final Logger logger = LogManager.getLogger(loggerName);

        return new SecureLoggingWriterLog4j(logger, messageFormatter);
    }

}
