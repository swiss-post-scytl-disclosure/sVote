/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.core.writer;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import org.apache.log4j.Logger;

public class SecureLoggingWriterLog4j extends CommonsLoggingWriterLog4j implements SecureLoggingWriter {

    public SecureLoggingWriterLog4j(final Logger logger, final MessageFormatter messageFormatter) {
        super(logger, messageFormatter);
    }

    /**
     * @see LoggingWriter#log(Level, LogContent)
     */
    @Override
    public void log(final Level level, final LogContent logContent) {
        String message = messageFormatter.buildMessage(logContent);

        addMessageWithLevel(level, message);

    }

    @Override
    @Deprecated
    public void log(final LogContent content) {
        log(Level.INFO, content, null);
    }

    /**
     * Logs the content with the secure logger. Optionally can also log to a
     * non-secure logging writer. This avoids calling twice the logging
     * module, one for logging in secure log and another for insecure
     *
     * @param content       Cotnent to log
     * @param loggingWriter Non-secure log writer to also log the content
     */
    @Override
    public void log(final Level level, final LogContent content, final LoggingWriter loggingWriter) {
        if (loggingWriter != null) {
            loggingWriter.log(Level.INFO, content);
        }

        String message = messageFormatter.buildMessage(content);
        logger.info(message);
    }


}
