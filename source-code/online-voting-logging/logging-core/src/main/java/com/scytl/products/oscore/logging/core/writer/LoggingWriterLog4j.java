/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */

package com.scytl.products.oscore.logging.core.writer;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import org.apache.log4j.Logger;

public class LoggingWriterLog4j extends CommonsLoggingWriterLog4j implements LoggingWriter {

    /**
     * @param logger
     * @param messageFormatter
     */
    public LoggingWriterLog4j(final Logger logger, final MessageFormatter messageFormatter) {
        super(logger, messageFormatter);
    }

    /**
     * @see LoggingWriter#log(Level, LogContent)
     */
    @Override
    public void log(final Level level, final LogContent logContent) {
        String message = messageFormatter.buildMessage(logContent);

        addMessageWithLevel(level, message);

    }
}
