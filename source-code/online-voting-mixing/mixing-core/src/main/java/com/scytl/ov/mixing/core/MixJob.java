/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import java.util.List;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Extension of {@link Job}, which mixes a list of {@link Mixable}.
 *
 * @author aakimov
 */
@ThreadSafe
public interface MixJob extends Job {
    /**
     * Returns the mix.
     *
     * @return the mix
     * @throws InvalidMixablesException (MixingException)
     *             the mixables are invalid
     * @throws MixingException
     *             the mixing failed.
     */
    <T extends Mixable> List<T> getMix(Class<T> cls) throws MixingException;

    /**
     * Starts mixing given mixables.
     * <P>
     * Any InvalidMixablesException or MixingException that occurs during processing will be thrown when the results are
     * retrieved.
     *
     * @param mixables
     *            the mixables
     * @throws IllegalStateException
     *             the job has already started, finished or canceled or mandatory job parameters are not set.
     */
    void start(List<Mixable> mixables);
}
