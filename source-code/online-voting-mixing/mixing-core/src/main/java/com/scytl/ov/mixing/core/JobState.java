/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * Job state.
 *
 * @author aakimov
 */
public enum JobState {
  /**
   * Job has been created but has not started yet.
   */
  CREATED,
  /**
   * Job has started.
   */
  STARTED,
  /**
   * Job has finished either successfully or with exception. This state is terminal, once reached it
   * cannot be changed.
   */
  FINISHED,
  /**
   * Job has been canceled by client. This state is terminal, once reached it
   * cannot be changed.
   */
  CANCELED
}
