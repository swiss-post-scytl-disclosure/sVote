/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.ov.mixing.commons.configuration.MatrixDimensions;

/**
 * Encapsulates the parameters used to configure the proof generation process.
 *
 * @author afries
 * @date Apr 26, 2018
 */
public final class ProofGenerationConfigParams {

    private final MatrixDimensions matrixDimensions;

    private final int numIterations;

    private final int mu;

    private final ZpSubgroup zpGroup;

    public ProofGenerationConfigParams(MatrixDimensions matrixDimensions, int numIterations, int mu,
            ZpSubgroup zpGroup) {

        this.matrixDimensions = matrixDimensions;
        this.numIterations = numIterations;
        this.mu = mu;
        this.zpGroup = zpGroup;
    }

    public MatrixDimensions getMatrixDimensions() {
        return matrixDimensions;
    }

    public int getNumIterations() {
        return numIterations;
    }

    public int getMu() {
        return mu;
    }

    public ZpSubgroup getZpGroup() {
        return zpGroup;
    }
}
