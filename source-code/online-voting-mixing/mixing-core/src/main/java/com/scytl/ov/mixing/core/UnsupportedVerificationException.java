/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * The mixing algorithm does not support verification.
 *
 * @author aakimov
 */
@SuppressWarnings("serial")
public final class UnsupportedVerificationException extends MixingException {

  /**
   * Constructor.
   *
   * @param message the message.
   */
  public UnsupportedVerificationException(String message) {
    super(message);
  }
}
