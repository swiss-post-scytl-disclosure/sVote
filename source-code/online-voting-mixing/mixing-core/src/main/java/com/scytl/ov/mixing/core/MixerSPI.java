/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Service provider interface of {@link Mixer}.
 *
 * @author aakimov
 */
@ThreadSafe
public interface MixerSPI {
  /**
   * Returns the algorithm.
   *
   * @return the algorithm.
   */
  String algorithm();

  /**
   * Creates a new verifiable mixing job.
   *
   * @param context the context
   * @return the new job
   * @throws UnsupportedVerificationException (MixingException) the verification is not supported
   * @throws MixingException failed to create the job.
   */
	MixAndProveJob newMixAndProveJob(MixerContext context) throws MixingException;

  /**
   * Creates a new mixing job to run in the specified mixer context.
   *
   * @param context the context
   * @return the new job
   * @throws MixingException failed to create the job.
   */
  MixJob newMixJob(MixerContext context) throws MixingException;

  /**
   * Creates a new verification job to run in the specified mixer context.
   *
   * @param context the context
   * @return the new job
   * @throws UnsupportedVerificationException (MixingException) the verification is not supported
   * @throws MixingException failed to create the job.
   */
	VerifyJob newVerifyJob(MixerContext context) throws MixingException;

  /**
   * Shut the downs the provider.
   */
  void shutdown();
}
