/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * The mixing algorithm has not been found.
 *
 * @author aakimov
 */
@SuppressWarnings("serial")
public final class AlgorithmNotFoundException extends MixingException {

  /**
   * Constructor.
   *
   * @param message the message.
   */
  public AlgorithmNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param message the message
   * @param cause the cause.
   */
  public AlgorithmNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
}
