/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * General mixing exception.
 *
 * @author aakimov
 */
@SuppressWarnings("serial")
public class MixingException extends Exception {

  /**
   * Constructor.
   *
   * @param message the message.
   */
  public MixingException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param message the message
   * @param cause the cause.
   */
  public MixingException(String message, Throwable cause) {
    super(message, cause);
  }
}
