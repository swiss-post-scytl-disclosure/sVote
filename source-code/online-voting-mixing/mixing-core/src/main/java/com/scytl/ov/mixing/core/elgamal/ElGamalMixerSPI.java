/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import com.scytl.ov.mixing.core.MixAndProveJob;
import com.scytl.ov.mixing.core.MixJob;
import com.scytl.ov.mixing.core.MixerContext;
import com.scytl.ov.mixing.core.MixerSPI;
import com.scytl.ov.mixing.core.MixingException;
import com.scytl.ov.mixing.core.VerifyJob;

/**
 * Implementation of {@link MixerSPI}.
 * 
 * @author aakimov
 */
public final class ElGamalMixerSPI implements MixerSPI {

    private static final ElGamalMixerSPI INSTANCE = new ElGamalMixerSPI();

    private ElGamalMixerSPI() {
    }

    /**
     * Returns the instance.
     * 
     * @return the instance.
     */
    public static ElGamalMixerSPI getInstance() {
        return INSTANCE;
    }

    @Override
    public String algorithm() {
        return "ElGamal";
    }

    @Override
    public MixAndProveJob newMixAndProveJob(MixerContext context) throws MixingException {
        return new ElGamalMixAndProveJob();
    }

    @Override
    public MixJob newMixJob(MixerContext context) throws MixingException {
        return new ElGamalMixJob();
    }

    @Override
    public VerifyJob newVerifyJob(MixerContext context) throws MixingException {
        return new ElGamalVerifyJob();
    }

    @Override
    public void shutdown() {
        // nothing to do
    }
}
