/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import java.util.EventObject;

/**
 * Job event.
 *
 * @author aakimov
 */
@SuppressWarnings("serial")
public final class JobEvent extends EventObject {

  /**
   * Constructor.
   *
   * @param source
   */
  public JobEvent(Job source) {
    super(source);
  }

  @Override
  public Job getSource() {
    return (Job) super.getSource();
  }
}
