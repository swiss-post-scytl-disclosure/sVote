/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * The mixing verification failed.
 *
 * @author aakimov
 */
@SuppressWarnings("serial")
public final class VerificationFailedException extends MixingException {

  /**
   * Constructor.
   *
   * @param message the message.
   */
  public VerificationFailedException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param message the message
   * @param cause the cause.
   */
  public VerificationFailedException(String message, Throwable cause) {
    super(message, cause);
  }
}
