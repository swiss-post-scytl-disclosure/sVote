/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.ov.mixing.core.Mixable;

/**
 * Extension of {@link Mixable} supported by ElGamal mixing.
 * 
 * @author aakimov
 */
public interface ElGamalMixable extends Mixable {
    /**
     * Returns the ciphertext.
     * 
     * @return the ciphertext.
     */
    Ciphertext getCiphertext();

    /**
     * Sets the ciphertext.
     * 
     * @param ciphertext
     *            the ciphertext.
     * @throws IllegalArgumentException
     *             if the received ciphertext is null.
     */
    void setCiphertext(Ciphertext ciphertext);
}
