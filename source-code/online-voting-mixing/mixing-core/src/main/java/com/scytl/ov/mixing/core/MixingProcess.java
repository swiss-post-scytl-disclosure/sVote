/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * Represents a process with the mixing.
 * 
 * @author afries
 * @date Apr 24, 2018
 */
public enum MixingProcess {

    SHUFFLE,

    REENCRYPTION,

    PROOF_GENERATION;
}
