/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Mixer is the engine which provides a runtime for mixing jobs.
 *
 * @author aakimov
 */
@ThreadSafe
public interface Mixer {
  /**
   * Returns whether the mixer is shut down.
   *
   * @return the mixer is shut down.
   */
  boolean isShutdown();

  /**
   * Creates a new verifiable mixing job for a given algorithm.
   *
   * @param algorithm the algorithm
   * @return the new job
   * @throws AlgorithmNotFoundException (MixingException) the algorithm not has not been found
   * @throws UnsupportedVerificationException (MixingException) verification is not supported
   * @throws MixingException failed to create the job.
   */
	MixAndProveJob newMixAndProveJob(String algorithm) throws MixingException;

  /**
   * Creates a new mixing job for a given algorithm.
   *
   * @param algorithm the algorithm
   * @return the new job
   * @throws AlgorithmNotFoundException (MixingException) the algorithm not has not been found
   * @throws MixingException failed to create the job.
   */
	MixJob newMixJob(String algorithm) throws MixingException;

  /**
   * Creates a new verification job for a given algorithm.
   *
   * @param algorithm the algorithm
   * @return the new job
   * @throws AlgorithmNotFoundException (MixingException) the algorithm is not supported
   * @throws UnsupportedVerificationException (MixingException) the verification is not supported
   * @throws MixingException failed to create the job.
   */
	VerifyJob newVerifyJob(String algorithm) throws MixingException;

  /**
   * Shuts the mixer down. This operation cancels all the pending jobs.
   */
  void shutdown();
}
