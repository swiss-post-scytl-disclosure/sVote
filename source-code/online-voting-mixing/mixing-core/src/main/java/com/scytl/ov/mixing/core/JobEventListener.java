/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import java.util.EventListener;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Job event listener.
 *
 * @author aakimov
 */
@ThreadSafe
public interface JobEventListener extends EventListener {
    /**
     * Job progress changed.
     *
     * @param e
     *            the event.
     */
    void progressChanged(JobEvent e);

    /**
     * Job state changed.
     *
     * @param e
     *            the event.
     */
    void stateChanged(JobEvent e);
}
