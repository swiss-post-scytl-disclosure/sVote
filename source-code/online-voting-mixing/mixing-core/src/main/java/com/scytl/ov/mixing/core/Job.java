/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import java.time.Duration;
import java.util.concurrent.TimeoutException;

import javax.annotation.Nonnegative;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Generic interface for a long-running asynchronous activity.
 *
 * @author aakimov
 */
@ThreadSafe
public interface Job {
  /**
   * Adds a given event listener.
   *
   * @param listener the listener.
   */
  void addEventListener(JobEventListener listener);

  /**
   * Waits for the job termination.
   *
   * @throws InterruptedException the thread is interrupted.
   */
  void await() throws InterruptedException;

  /**
   * Waits for the job termination no more than a given timeout..
   *
   * @param timeout the timeout
   * @throws InterruptedException the thread is interrupted
   * @throws TimeoutException the timeout has expired.
   */
  void await(Duration timeout) throws InterruptedException, TimeoutException;

  /**
   * Cancels the job. If the job is already finished or canceled then no operation.
   */
  void cancel();

  /**
   * Returns the progress.
   *
   * @return the progress.
   */
  @Nonnegative
  int progress();

  /**
   * Removes a given event listener.
   *
   * @param listener the listener.
   */
  void removeEventListener(JobEventListener listener);

  /**
   * Sets the job parameters. This operation cannot be used after the job is started.
   *
   * @param parameters the parameters
   * @throws InvalidJobParametersException the parameters are invalid
   * @throws IllegalStateException the job is already started.
   */
  void setParameters(JobParameters parameters) throws InvalidJobParametersException;

  /**
   * Returns the state.
   *
   * @return the state.
   */
  JobState state();
}
