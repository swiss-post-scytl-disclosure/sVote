/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

/**
 * Encapsulates the outputs returned after reencryption.
 *
 * @author afries
 * @date Apr 16, 2018
 */
public final class ReencryptionOutput {

    private Ciphertext ciphertext;

    private Exponent exponent;

    /**
     * Initialize.
     * 
     * @param ciphertext
     *            the ciphertext.
     * @param exponent
     *            the random exponent that was used during the encryption.
     */
    public ReencryptionOutput(Ciphertext ciphertext, Exponent exponent) {

        this.ciphertext = ciphertext;
        this.exponent = exponent;
    }

    /**
     * @return the ciphertext.
     */
    public Ciphertext getCiphertext() {
        return this.ciphertext;
    }

    /**
     * @return the random exponent that was used during encryption.
     */
    public Exponent getExponent() {
        return this.exponent;
    }
}
