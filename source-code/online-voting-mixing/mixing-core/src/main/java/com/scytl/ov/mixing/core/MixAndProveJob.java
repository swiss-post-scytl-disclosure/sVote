/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import javax.annotation.concurrent.ThreadSafe;

import com.scytl.ov.mixing.commons.beans.proofs.Proof;

/**
 * Extension of {@link MixJob} which produces a proof to guarantee the correctness of the mixing.
 *
 * @author aakimov
 */
@ThreadSafe
public interface MixAndProveJob extends MixJob {
    /**
     * Returns the proof.
     *
     * @return the proof
     * @throws InvalidMixablesException
     *             the mixables are invalid
     * @throws MixingException
     *             the mixing failed.
     * @throws IllegalStateException
     *             the job has not finished yet or is canceled.
     */
    Proof getProof() throws MixingException;
}
