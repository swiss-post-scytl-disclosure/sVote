/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * The job parameters are invalid.
 *
 * @author aakimov
 */
@SuppressWarnings("serial")
public final class InvalidJobParametersException extends MixingException {

  /**
   * Constructor.
   *
   * @param message the message.
   */
  public InvalidJobParametersException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param message the message
   * @param cause the cause.
   */
  public InvalidJobParametersException(String message, Throwable cause) {
    super(message, cause);
  }
}
