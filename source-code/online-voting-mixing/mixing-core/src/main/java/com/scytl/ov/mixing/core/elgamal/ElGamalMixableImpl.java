/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import static java.util.Objects.requireNonNull;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;

/**
 * A mixable ElGamal ciphertext.
 */
public class ElGamalMixableImpl implements ElGamalMixable {

    private Ciphertext ciphertext;

    /**
     * Create an ElGamalMixableImpl.
     * 
     * @throws IllegalArgumentException
     *             if the received ciphertext is null.
     */
    public ElGamalMixableImpl(Ciphertext ciphertext) {

        requireNonNull(ciphertext,"The ciphertext was not initialized");
        this.ciphertext = ciphertext;
    }

    @Override
    public void setCiphertext(Ciphertext ciphertext) {

        requireNonNull(ciphertext,"The ciphertext was not initialized");
        this.ciphertext = ciphertext;
    }

    @Override
    public Ciphertext getCiphertext() {

        return ciphertext;
    }
}
