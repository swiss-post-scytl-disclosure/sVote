/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import java.util.List;

import javax.annotation.concurrent.ThreadSafe;

import com.scytl.ov.mixing.commons.beans.proofs.Proof;

/**
 * Extension of {@link Job}, which verifies the correctness of mixing.
 *
 * @author aakimov
 */
@ThreadSafe
public interface VerifyJob extends Job {
  /**
   * Checks if the verification is success.
   *
   * @throws InvalidMixablesException (MixingException) the mixables are invalid
   * @throws VerificationFailedException (MixingException) the verification failed
   * @throws MixingException the mixing failed.
   */
  void check() throws MixingException;

  /**
   * Starts verification of mixing.
   *
   * @param mixables the original mixables
   * @param mix the mix
   * @param proof the proof
   * @throws IllegalStateException the job has already started, finished or canceled or mandatory
   *         job parameters are not set.
   */
  <T extends Mixable> void start(List<T> mixables, List<T> mix, Proof proof);
}
