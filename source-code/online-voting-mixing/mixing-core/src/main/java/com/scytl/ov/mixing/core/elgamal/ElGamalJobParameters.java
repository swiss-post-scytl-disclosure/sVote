/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import static java.util.Objects.requireNonNull;

import javax.annotation.concurrent.Immutable;

import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.ov.mixing.core.JobParameters;
import com.scytl.ov.mixing.core.ProofGenerationConfigParams;

/**
 * Parameters of ElGamal mixing and verification operations.
 * 
 * @author aakimov
 */
@Immutable
public final class ElGamalJobParameters implements JobParameters {

    private final ElGamalEncryptionParameters encryptionParameters;

    private final ElGamalPublicKey publicKey;

    private final ProofGenerationConfigParams proofGenerationConfigParams;

    /**
     * Constructor.
     * 
     * @param publicKey
     *            the public key.
     */
    public ElGamalJobParameters(ElGamalEncryptionParameters elGamalEncryptionParameters, ElGamalPublicKey publicKey,
            ProofGenerationConfigParams proofGenerationConfigParams) {
        this.encryptionParameters = requireNonNull(elGamalEncryptionParameters, "Encryption parameters is null.");
        this.publicKey = requireNonNull(publicKey, "Public key is null.");
        this.proofGenerationConfigParams = proofGenerationConfigParams;
    }

    /**
     * Returns the public key.
     * 
     * @return the public key.
     */
    public ElGamalPublicKey publicKey() {
        return publicKey;
    }

    /**
     * Returns the encryption parameters.
     * 
     * @return the encryption parameters.
     */
    public ElGamalEncryptionParameters encryptionParameters() {
        return encryptionParameters;
    }

    /**
     * Return the proof generation config params.
     * 
     * @return the proof generation config params.
     */
    public ProofGenerationConfigParams getProofGenerationConfigParams() {
        return proofGenerationConfigParams;
    }

    @Override
    public String toString() {
        return "ElGamalJobParameters [encryptionParameters=' + encryptionParameters + 'publicKey=" + publicKey + "]";
    }
}
