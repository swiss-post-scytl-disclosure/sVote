/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

/**
 * Generic interface for the objects which can be mixed.
 *
 * @author aakimov
 */
public interface Mixable {
}
