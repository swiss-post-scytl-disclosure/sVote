/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * <p>Core API and implementation of mixing.
 * 
 * <p>Mixing and verification operations are long-running job represented by
 * {@link com.scytl.ov.mixing.core.Job} interface and it's subclasses
 * {@link com.scytl.ov.mixing.core.MixJob}, {@link com.scytl.ov.mixing.core.MixAndProveJob} and
 * {@link com.scytl.ov.mixing.core.VerifyJob}. Client creates a new instance of the required job,
 * initializes it with {@link com.scytl.ov.mixing.core.JobParameters} and starts it. Client can
 * wait for job to finish or can register a {@link com.scytl.ov.mixing.core.JobEventListener} to
 * handle the job completion asynchronously.
 * 
 * <p>{@link com.scytl.ov.mixing.core.Mixer} is the engine responsible for creating jobs. It has
 * pluggable implementation, while starting the application client can create an instance of
 * {@link com.scytl.ov.mixing.core.Mixer} using several independent implementations represented by
 * {@link com.scytl.ov.mixing.core.MixerSPI}. Mixer also provides resources to be shared by jobs
 * during execution. Such resource are available via {@link com.scytl.ov.mixing.core.MixerContext}.
 * To release the allocated shared resources application must shutdown mixer during it's own
 * termination.
 * 
 * <p>All the {@link com.scytl.ov.mixing.core.MixerSPI} implementations must support simple mixing
 * operation. Mixing with proof generation is optional.
 *
 * @author aakimov
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.scytl.ov.mixing.core;
