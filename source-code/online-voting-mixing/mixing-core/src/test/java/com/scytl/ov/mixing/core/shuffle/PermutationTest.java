/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.shuffle;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.ov.mixing.shuffle.Permutation;

public class PermutationTest {

    private static int[] fourElementArray = {2, 0, 3, 1 };

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void givenValidArrayWithFourElementsWhenCreatePermutationThenOk() {

        new Permutation(fourElementArray);
    }

    @Test
    public void givenValidPermutationWhenGetLengthThenOk() {

        Permutation permutation = new Permutation(fourElementArray);

        assertEquals(fourElementArray.length, permutation.getLength());
    }

    @Test
    public void givenValidPermutationWhenGetIndexInSourceListThenOk() {

        Permutation permutation = new Permutation(fourElementArray);

        assertEquals(2, permutation.getCorrespondingIndexInSourceList(0));
        assertEquals(0, permutation.getCorrespondingIndexInSourceList(1));
        assertEquals(3, permutation.getCorrespondingIndexInSourceList(2));
        assertEquals(1, permutation.getCorrespondingIndexInSourceList(3));
    }

    @Test
    public void givenNullArrayWhenCreatePermutationThenException() {

        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("The permutation must be initialized and contain at least 1 element");

        int[] indexes = null;

        new Permutation(indexes);
    }

    @Test
    public void givenArrayWithOneElementWhenCreatePermutationThenOk() {

        int[] indexes = {0 };

        new Permutation(indexes);
    }

    @Test
    public void givenArrayWithRepeatedElementThenException() {

        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("The given permutation contains repeated elements");

        int[] arrayWithRepeatedValue = {2, 0, 3, 2, 1 };

        new Permutation(arrayWithRepeatedValue);
    }

    @Test
    public void givenArrayWithValueLargerThanNumberElementsThenException() {

        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("The given permutation contains values which are outside the valid range");

        int[] arrayWithValueLargerThanSize = {2, 0, 4, 1 };

        new Permutation(arrayWithValueLargerThanSize);
    }

    @Test
    public void givenMultiElementPermutationWhenToStringThenOk() {

        String expectedString = "Permutation=[2,0,3,1]";
        assertEquals(expectedString, new Permutation(fourElementArray).toString());
    }

    @Test
    public void givenSingleElementPermutationWhenToStringThenOk() {

        int[] arrayWithOneElement = {0 };

        String expectedString = "Permutation=[0]";
        assertEquals(expectedString, new Permutation(arrayWithOneElement).toString());
    }
}
