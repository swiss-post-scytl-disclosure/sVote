/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.ov.mixing.commons.beans.proofs.Proof;
import com.scytl.ov.mixing.core.InvalidMixablesException;
import com.scytl.ov.mixing.core.JobState;
import com.scytl.ov.mixing.core.JobStateAndProgressListener;
import com.scytl.ov.mixing.core.MixingException;

public class ElGamalVerifyJobTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void whenCreatedThenStateAndProgressInitialized() {

        ElGamalVerifyJob elGamalVerifyJob = new ElGamalVerifyJob();

        JobState expectedInitialState = JobState.CREATED;
        int expectedInitialProgress = 0;

        assertEquals(expectedInitialState, elGamalVerifyJob.state());
        assertEquals(expectedInitialProgress, elGamalVerifyJob.progress());
    }

    //////////////////////////////////////
    //
    // Adding and removing listener tests
    //
    //////////////////////////////////////

    @Test
    public void whenListenerAddedAndRemovedThenOk() {

        ElGamalVerifyJob elGamalVerifyJob = new ElGamalVerifyJob();

        JobStateAndProgressListener listener = new JobStateAndProgressListener();
        elGamalVerifyJob.addEventListener(listener);
        elGamalVerifyJob.removeEventListener(listener);
    }

    @Test
    public void whenTryToRemoveUnaddedListenerThenNoAffect() {

        ElGamalVerifyJob elGamalVerifyJob = new ElGamalVerifyJob();

        JobStateAndProgressListener listener = new JobStateAndProgressListener();
        elGamalVerifyJob.removeEventListener(listener);
    }

    //////////////////////////////////////
    //
    // Starting and JobState tests
    //
    //////////////////////////////////////

    @Test
    public void whenStartBeforeSettingParamtersThenException() throws InvalidMixablesException, MixingException {

        expectedEx.expect(IllegalStateException.class);
        expectedEx.expectMessage("Started the job without first setting the job parameters");

        ElGamalVerifyJob elGamalVerifyJob = new ElGamalVerifyJob();

        List<ElGamalMixableImpl> mixables = new ArrayList<>();
        List<ElGamalMixableImpl> mixedMixables = new ArrayList<>();
        Proof proof = null;

        elGamalVerifyJob.start(mixables, mixedMixables, proof);
    }
}
