/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.CiphertextImpl;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;

public class ElGamalMixableImplTest {

    @Test
    public void whenSetAndGetThenOk() throws GeneralCryptoLibException {

        Ciphertext ciphertext = createCiphertext(getGroup());

        ElGamalMixableImpl elGamalMixableImpl = new ElGamalMixableImpl(ciphertext);

        Ciphertext returnedCiphertext = elGamalMixableImpl.getCiphertext();

        assertEquals(ciphertext, returnedCiphertext);
    }

    private ZpSubgroup getGroup() throws GeneralCryptoLibException {

        BigInteger p = TestData.P_TINY_SIZE;
        BigInteger q = TestData.Q_TINY_SIZE;
        BigInteger g = TestData.G_TINY_SIZE;

        return new ZpSubgroup(g, p, q);
    }

    private Ciphertext createCiphertext(ZpSubgroup group) throws GeneralCryptoLibException {

        ZpGroupElement gamma = new ZpGroupElement(new BigInteger("9"), group);
        ZpGroupElement phi1 = new ZpGroupElement(new BigInteger("4"), group);
        ZpGroupElement phi2 = new ZpGroupElement(new BigInteger("2"), group);
        List<ZpGroupElement> phis = new ArrayList<>();
        phis.add(phi1);
        phis.add(phi2);
        return new CiphertextImpl(gamma, phis);
    }
}
