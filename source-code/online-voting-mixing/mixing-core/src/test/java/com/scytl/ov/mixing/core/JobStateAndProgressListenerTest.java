/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.ov.mixing.core.elgamal.ElGamalMixJob;

import mockit.Mock;
import mockit.MockUp;

public class JobStateAndProgressListenerTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void whenProgressChangedThenQueriedThenOk() {

        JobStateAndProgressListener listener = new JobStateAndProgressListener();

        int mockedProgressValue = 55;

        new MockUp<ElGamalMixJob>() {
            @Mock
            int progress() {
                return mockedProgressValue;
            }
        };

        JobEvent jobEvent = new JobEvent(new ElGamalMixJob());

        listener.progressChanged(jobEvent);

        assertEquals(mockedProgressValue, listener.progress());
    }

    @Test
    public void whenStateChangedThenQueriedThenOk() {

        JobStateAndProgressListener listener = new JobStateAndProgressListener();

        JobState mockedStateValue = JobState.STARTED;

        new MockUp<ElGamalMixJob>() {
            @Mock
            public JobState state() {
                return mockedStateValue;
            }

        };

        JobEvent jobEvent = new JobEvent(new ElGamalMixJob());

        listener.stateChanged(jobEvent);

        assertEquals(mockedStateValue, listener.state());
    }

    @Test
    public void givenListenerWithUninitializedStateWhenQueriedThenException() {

        expectedEx.expect(IllegalStateException.class);
        expectedEx.expectMessage("The state has not yet been initialized");

        JobStateAndProgressListener listener = new JobStateAndProgressListener();

        listener.state();
    }
}
