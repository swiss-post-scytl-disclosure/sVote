/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import java.math.BigInteger;

public class TestData {

    public static final BigInteger P_MEDIUM_SIZE = new BigInteger("15294034768093677312256663166625633354362303");;

    public static final BigInteger Q_MEDIUM_SIZE = new BigInteger("7647017384046838656128331583312816677181151");

    public static final BigInteger G_MEDIUM_SIZE = new BigInteger("2");

    public static final BigInteger P_SMALL_SIZE = new BigInteger("347");;

    public static final BigInteger Q_SMALL_SIZE = new BigInteger("173");

    public static final BigInteger G_SMALL_SIZE = new BigInteger("3");

    public static final BigInteger P_TINY_SIZE = new BigInteger("23");;

    public static final BigInteger Q_TINY_SIZE = new BigInteger("11");

    public static final BigInteger G_TINY_SIZE = new BigInteger("2");

}
