/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.core.elgamal;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;

public class ClientMixableForTesting extends ElGamalMixableImpl {

    private int someClientField;

    public ClientMixableForTesting(Ciphertext ciphertext, int someClientField) {

        super(ciphertext);
        this.someClientField = someClientField;
    }

    public int getSomeClientField() {
        return this.someClientField;
    }

}
