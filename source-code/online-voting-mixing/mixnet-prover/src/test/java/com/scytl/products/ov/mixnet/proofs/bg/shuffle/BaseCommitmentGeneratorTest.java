/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.shuffle;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.mixnet.commons.beans.Permutation;
import com.scytl.products.ov.mixnet.commons.tools.RandomOracleHash;
import com.scytl.products.ov.mixnet.proofs.bg.BasicProofsInitializer;
import com.scytl.products.ov.mixnet.proofs.bg.state.CommonStateLabels;
import com.scytl.products.ov.mixnet.proofs.bg.state.MapBasedHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;
import org.junit.Test;

public class BaseCommitmentGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    // Using one of the implementations of BaseCommitmentGenerator
    private FirstCommitmentGenerator _sut;

    @Test
    public void generate_correctly_the_commitment() throws GeneralCryptoLibException {

        final Permutation permutation = getMockedPermutation();

        // Using one of the implementations to test the generate method.
        _sut = new FirstCommitmentGenerator(config, permutation, commitmentParams, privateAndPublicCommitmentsGenerator,
                limMultiExpo, concurrencyLevel);

        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));
        _harvester.collect(CommonStateLabels.ORIGINAL_CIPHERTEXTS, getMockedEncryptedCiphertexts());

        _sut.generate(_harvester);

    }

}
