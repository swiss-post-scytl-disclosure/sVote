/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.multiexp;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.products.ov.mixnet.commons.tools.CommitmentTools;
import com.scytl.products.ov.mixnet.commons.tools.MatrixArranger;
import com.scytl.products.ov.mixnet.commons.tools.RandomOracleHash;
import com.scytl.products.ov.mixnet.proofs.bg.BasicProofsInitializer;
import com.scytl.products.ov.mixnet.proofs.bg.multiexp.basic.BasicProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.multiexp.basic.ParallelBasicProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.multiexp.reduction.ParallelReductionProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.BaseCommitmentGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.SecondCommitmentGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.ShuffleProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.state.MapBasedHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;

public class MultiExponentiationProofGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    private MultiExponentiationProofGenerator _sut;

    @Before
    public void initHarvester() {
        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));

        final Exponent[][] exponents =
            MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);
        _harvester.collect(BaseCommitmentGenerator.Produced.EXPONENTS, exponents);
        final PrivateCommitment[] privateCommitmentsB = getMockedPrivateCommitments(exponents);
        _harvester.collect(SecondCommitmentGenerator.Produced.PRIVATE_COMMITS, privateCommitmentsB);
        _harvester.collect(SecondCommitmentGenerator.Produced.PUBLIC_COMMITS,
            CommitmentTools.makePublic(privateCommitmentsB));
    }

    @Test
    public void generate_the_proof_correctly()
            throws GeneralCryptoLibException {

        final BasicProofGenerator basicProofGenerator =
            new ParallelBasicProofGenerator(config, commitmentParams, limMultiExpo, computeAllE, concurrencyLevel);

        _sut =
            new MultiExponentiationProofGenerator(config, getMockedReEncryptedCiphertexts(), getMockedRandomExponents(),
                //
                basicProofGenerator, //
                new ParallelReductionProofGenerator(config, commitmentParams.getCommitmentLength(), basicProofGenerator,
                    commitmentParams, computeAllE, limMultiExpo, concurrencyLevel));

        _sut.generate(_harvester);
    }
}
