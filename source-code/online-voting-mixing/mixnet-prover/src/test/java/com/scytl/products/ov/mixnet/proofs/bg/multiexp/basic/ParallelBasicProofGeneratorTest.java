/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.multiexp.basic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.beans.proofs.MultiExponentiationBasicProofAnswer;
import com.scytl.products.ov.mixnet.commons.beans.proofs.MultiExponentiationBasicProofInitialMessage;
import com.scytl.products.ov.mixnet.commons.homomorphic.impl.GjosteenElGamalRandomness;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PublicCommitment;
import com.scytl.products.ov.mixnet.commons.tools.CommitmentTools;
import com.scytl.products.ov.mixnet.commons.tools.ExponentTools;
import com.scytl.products.ov.mixnet.commons.tools.MatrixArranger;
import com.scytl.products.ov.mixnet.commons.tools.RandomOracleHash;
import com.scytl.products.ov.mixnet.proofs.bg.BasicProofsInitializer;
import com.scytl.products.ov.mixnet.proofs.bg.multiexp.MultiExponentiationProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.SecondCommitmentGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.ShuffleProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.state.CommonStateLabels;
import com.scytl.products.ov.mixnet.proofs.bg.state.MapBasedHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;

public class ParallelBasicProofGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    private ParallelBasicProofGenerator _sut;

    @Before
    public void initHarvester() throws GeneralCryptoLibException {

        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));

        _harvester.collect(CommonStateLabels.CIPHERTEXTS, getMockedReEncryptedCiphertexts());

        final Exponent[][] exponents =
            MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);
        _harvester.collect(BasicInitialMessageGenerator.Produced.EXPONENTS_MULTIARR_B, exponents);

        final PrivateCommitment[] privateCommitmentsB = getMockedPrivateCommitments(exponents);
        final PublicCommitment[] publicCommitmentsB = CommitmentTools.makePublic(privateCommitmentsB);
        _harvester.collect(SecondCommitmentGenerator.Produced.PRIVATE_COMMITS, privateCommitmentsB);
        _harvester.collect(SecondCommitmentGenerator.Produced.PUBLIC_COMMITS, publicCommitmentsB);

        _harvester.collect(MultiExponentiationProofGenerator.Produced.CALCULATED_RANDOM_EXPONENT,
            new GjosteenElGamalRandomness(ExponentTools.getRandomExponent(q)));

    }

    @Test
    public void generate_the_basic_proof_correctly() throws GeneralCryptoLibException {

        _sut = new ParallelBasicProofGenerator(config, commitmentParams, limMultiExpo, computeAllE, concurrencyLevel);

        System.out.println("generate_the_basic_proof_correctly 1");
        _sut.generate(_harvester);
        System.out.println("generate_the_basic_proof_correctly 2");

        final MultiExponentiationBasicProofInitialMessage multiExponentiationBasicProofInitialMessage =
            _harvester.offer(BasicInitialMessageGenerator.Produced.BASIC_MULTIEXP_INIT_MSG,
                MultiExponentiationBasicProofInitialMessage.class);

        final MultiExponentiationBasicProofAnswer multiExponentiationBasicProofAnswer = _harvester
            .offer(BasicAnswerGenerator.Produced.BASIC_MULTIEXP_ANSWER, MultiExponentiationBasicProofAnswer.class);

        assertThat(multiExponentiationBasicProofInitialMessage != null, is(true));
        assertThat(multiExponentiationBasicProofAnswer != null, is(true));

    }
}
