/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.singlevalprod;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.beans.proofs.SingleValueProductProofAnswer;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PublicCommitment;
import com.scytl.products.ov.mixnet.commons.tools.MatrixArranger;
import com.scytl.products.ov.mixnet.commons.tools.RandomOracleHash;
import com.scytl.products.ov.mixnet.proofs.bg.BasicProofsInitializer;
import com.scytl.products.ov.mixnet.proofs.bg.product.ProductProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.ShuffleProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.state.MapBasedHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;

public class ParallelSingleValueProductProofGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    private ParallelSingleValueProductProofGenerator _sut;

    @Before
    public void initHarvester() {
        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));
        final Exponent[][] exponents =
            MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);
        final PrivateCommitment[] mockedPrivateCommitments = getMockedPrivateCommitments(exponents);
        final PublicCommitment mockedPublicCommitment = mockedPrivateCommitments[0].makePublicCommitment();
        _harvester.collect(ProductProofGenerator.Produced.PRIVATE_COMMITS_Cb, mockedPrivateCommitments[0]);
        _harvester.collect(ProductProofGenerator.Produced.PUBLIC_COMMITMENT_Cb, mockedPublicCommitment);

    }

    @Test
    public void generate_the_proof_correctly() {

        _sut = new ParallelSingleValueProductProofGenerator(n, commitmentParams, q, limMultiExpo);
        _sut.generate(_harvester);
        final SingleValueProductProofAnswer singleValueProductProofAnswer =
            _harvester.offer(SingleValueProductProofGenerator.Produced.ANSWER, SingleValueProductProofAnswer.class);

        assertThat(singleValueProductProofAnswer != null, is(true));
    }
}
