/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.shuffle;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.beans.Permutation;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;
import com.scytl.products.ov.mixnet.proofs.bg.BasicProofsInitializer;

public class ShuffleProofInputDataTest extends BasicProofsInitializer {

    private ShuffleProofInputData _sut;

    @Test
    public void return_the_same_parameters_that_are_set() throws GeneralCryptoLibException {

        final Ciphertext[][] ballots = getMockedEncryptedCiphertexts();

        final Permutation permutation = getMockedPermutation();

        final Ciphertext[][] reencryptedCiphertexts = getMockedReEncryptedCiphertexts();

        final Randomness[] exponents = getMockedRandomExponents();

        _sut = new ShuffleProofInputData(ballots, permutation, privateAndPublicCommitmentsGenerator,
            reencryptedCiphertexts, exponents);

        assertThat(_sut.getPermutation(), is(permutation));
        assertThat(_sut.getPrivateAndPublicCommitmentsGenerator(), is(privateAndPublicCommitmentsGenerator));
        assertThat(_sut.getReencryptedCiphertexts(), is(reencryptedCiphertexts));
        assertThat(_sut.getRho(), is(exponents));

    }

}
