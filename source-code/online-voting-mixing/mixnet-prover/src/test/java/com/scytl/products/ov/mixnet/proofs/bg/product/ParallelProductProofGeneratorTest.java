/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.product;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.beans.proofs.ProductProofMessage;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.products.ov.mixnet.commons.tools.MatrixArranger;
import com.scytl.products.ov.mixnet.commons.tools.RandomOracleHash;
import com.scytl.products.ov.mixnet.proofs.bg.BasicProofsInitializer;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.FirstCommitmentGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.SecondCommitmentGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.shuffle.ShuffleProofGenerator;
import com.scytl.products.ov.mixnet.proofs.bg.state.MapBasedHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;

public class ParallelProductProofGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    private ParallelProductProofGenerator _sut;

    @Before
    public void initHarvester() {
        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));

        final Exponent[][] exponents =
            MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);

        final PrivateCommitment[] privateCommitmentsA = getMockedPrivateCommitments(exponents);

        _harvester.collect(FirstCommitmentGenerator.Produced.PRIVATE_COMMITS, privateCommitmentsA);

        final PrivateCommitment[] privateCommitmentsB = getMockedPrivateCommitments(exponents);

        _harvester.collect(SecondCommitmentGenerator.Produced.PRIVATE_COMMITS, privateCommitmentsB);
    }

    @Test
    public void generate_the_proof_correctly() {

        _sut = new ParallelProductProofGenerator(config.getMatrixDimensions(), commitmentParams,
            config.getZpGroupParams().getQ(), limMultiExpo);
        _sut.generate(_harvester);

        final ProductProofMessage productProofMessage =
            _harvester.offer(ProductProofGenerator.Produced.PRODUCT_PROOF_MSG, ProductProofMessage.class);

        assertThat(productProofMessage != null, is(true));
    }
}
