/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.state;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class MapBasedHarvesterTest {

    @Test
    public void ifNoStateAssociatedWithLabelThenNullReturnedAtRetrieval()
            throws Exception {

        final StateHarvester harvester = new MapBasedHarvester();

        final StateLabel nonPresentStateLabel = new StateLabel() {
        };

        final Object typedRetrieval =
                harvester.offer(nonPresentStateLabel, Integer.class);

        Assert.assertNull(typedRetrieval);
    }

    @Test
    public void ifStateAssociatedWithLabelThenReturnedAtRetrieval()
            throws Exception {

        final StateHarvester harvester = new MapBasedHarvester();

        final StateLabel stateLabel = new StateLabel() {
        };

        final Integer state = 8;

        harvester.collect(stateLabel, state);

        final Integer typedRetrieval =
                harvester.offer(stateLabel, Integer.class);

        Assert.assertNotNull(typedRetrieval);
        Assert.assertEquals(state, typedRetrieval);
    }
}
