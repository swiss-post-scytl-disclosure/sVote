/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.multiexp.reduction;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateLabel;

/**
 * Defines the methods to generate a multi-exponentiation proof with reduction steps.
 */
public interface ReductionProofGenerator {

    /**
     * Generates the initial message and the answer of the multi-exponentiation proof and allows the {@code harvester}
     * to collect the results.
     *
     * @param harvester
     * @throws GeneralCryptoLibException 
     */
    void generate(StateHarvester harvester) throws GeneralCryptoLibException;

    enum Produced implements StateLabel {
        REDUCT_MULTIEXP_ANSWER, REDUCT_MULTIEXP_INIT_MSG, //
        MULTIEXP_REDUCT_RO, EXPONENTS_MULTIARR_B, EXPONENTS_ARR_S, RANDOMNESS_TAU_ARR, //
        B, S, //
    }

}
