/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.multiexp.basic;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateLabel;

/**
 * Defines the methods to generate the initial message of the multi-exponentiation proof without reduction.
 */
public interface BasicInitialMessageGenerator {

    /**
     * Generates the initial message and allows the {@code harvester} to collect the result.
     *
     * @param harvester
     * @throws GeneralCryptoLibException 
     * @throws StateRetrievalException
     */
    void generate(StateHarvester harvester) throws GeneralCryptoLibException;

    enum Produced implements StateLabel {
        BASIC_MULTIEXP_INIT_MSG,
        //
        EXPONENTS_A0, EXPONENTS_R0, PRIVATE_COMMIT_BASIC_MEXP,
        //
        EXPONENTS_MULTIARR_B, EXPONENTS_ARR_S, PRIVATE_COMMITS_BASIC_MEXP,
        //
        RANDOMNESS_TAU_ARR, EXPONENTS_ARR, E
    }

}
