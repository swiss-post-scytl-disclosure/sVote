/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.shuffle;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.beans.Permutation;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;

public class ShuffleProofInputData {

    private final Ciphertext[][] encryptedCiphertexts;

    private final Permutation permutation;

    private final PrivateAndPublicCommitmentsGenerator privateAndPublicCommitmentsGenerator;

    private final Ciphertext[][] reencryptedCiphertexts;

    private final Randomness[] rho;

    public ShuffleProofInputData(final Ciphertext[][] encryptedCiphertexts, final Permutation permutation,
            // final SecondAnswerGenerator secondAnswerGenerator,
            final PrivateAndPublicCommitmentsGenerator privateAndPublicCommitmentsGenerator,
            final Ciphertext[][] reencryptedCiphertexts, final Randomness[] rho) {
        this.encryptedCiphertexts = encryptedCiphertexts;
        this.permutation = permutation;
        this.privateAndPublicCommitmentsGenerator = privateAndPublicCommitmentsGenerator;
        this.reencryptedCiphertexts = reencryptedCiphertexts;
        this.rho = rho;
    }

    public Ciphertext[][] getEncryptedCiphertexts() {
        return encryptedCiphertexts;
    }

    public Permutation getPermutation() {
        return permutation;
    }

    public Ciphertext[][] getReencryptedCiphertexts() {
        return reencryptedCiphertexts;
    }

    public Randomness[] getRho() {
        return rho;
    }

    public PrivateAndPublicCommitmentsGenerator getPrivateAndPublicCommitmentsGenerator() {
        return privateAndPublicCommitmentsGenerator;
    }
}
