/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.state;

/**
 * Interface to be implemented by the <i>collecting parameters</i> used in the proof generators.
 */
public interface StateHarvester {

    /**
     * <i>Stores</i> the given object associated with the given label.
     *
     * @param label
     * @param object
     */
    void collect(StateLabel label, Object object);

    /**
     * Provides the object associated with the given label.
     *
     * @param label
     * @param clazz
     *            The type of the item
     * @return The item associated to the given label, as an instance of the given class, or <b>null</b> if no item is
     *         associated.
     * @throws StateRetrievalException
     */
    <T> T offer(StateLabel label, Class<T> clazz);

}
