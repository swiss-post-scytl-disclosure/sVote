/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.state;

/**
 * Labels which are shared among all proofs.
 */
public enum CommonStateLabels implements StateLabel {

    ORIGINAL_CIPHERTEXTS, CIPHERTEXTS, REDUCT_ITERATIONS_LEFT
}
