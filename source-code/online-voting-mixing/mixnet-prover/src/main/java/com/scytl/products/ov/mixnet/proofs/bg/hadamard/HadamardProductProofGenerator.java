/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.hadamard;

import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateLabel;

/**
 * Defines the required methods to generate the hadamard product proof.
 */
public interface HadamardProductProofGenerator {

    /**
     * Generates the hadamard product proof message and allows the {@code harvester} to collect the result.
     *
     * @param harvester
     * @throws StateRetrievalException
     */
    void generate(StateHarvester harvester);

    enum Produced implements StateLabel {
        INITIAL_MSG, ANSWER, //
        RANDOM_ORACLE, EXPONENTS_MATRIX_B, EXPONENTS_S, PRIVATE_COMMITMENTS_D, CHALLENGE_INNER_PRODUCT,
        PUBLIC_COMMITMENTS_CA
    }

}
