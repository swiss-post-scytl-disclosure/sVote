/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.multiexp.basic;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateLabel;

/**
 * Defines the methods to generate a multi-exponentiation proof without reduction steps.
 */
public interface BasicProofGenerator {

    /**
     * Generates the initial message and the answer of the multi-exponentiation proof and allows the {@code harvester}
     * to collect the results.
     *
     * @param harvester
     * @throws GeneralCryptoLibException 
     */
    void generate(StateHarvester harvester) throws GeneralCryptoLibException;

    enum Produced implements StateLabel {
        MULTIEXP_BASIC_RO
    }

}
