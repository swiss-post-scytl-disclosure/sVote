/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.shuffle;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateHarvester;
import com.scytl.products.ov.mixnet.proofs.bg.state.StateLabel;

/**
 * Defines the required methods to generate the second answer.
 */
public interface SecondAnswerGenerator {

    /**
     * Generates the second answer and allows the {@code harvester} to collect the result.
     *
     * @param harvester
     * @throws GeneralCryptoLibException 
     */
    void generate(StateHarvester harvester) throws GeneralCryptoLibException;

    enum Produced implements StateLabel {
        SHUFFLE_SECOND_ANSWER
    }

}
