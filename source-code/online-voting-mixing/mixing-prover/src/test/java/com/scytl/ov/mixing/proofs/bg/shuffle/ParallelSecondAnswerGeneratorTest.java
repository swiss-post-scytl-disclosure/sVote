/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.shuffle;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.beans.proofs.ShuffleProofSecondAnswer;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PublicCommitment;
import com.scytl.ov.mixing.commons.tools.CommitmentTools;
import com.scytl.ov.mixing.commons.tools.MatrixArranger;
import com.scytl.ov.mixing.commons.tools.RandomOracleHash;
import com.scytl.ov.mixing.proofs.bg.BasicProofsInitializer;
import com.scytl.ov.mixing.proofs.bg.state.CommonStateLabels;
import com.scytl.ov.mixing.proofs.bg.state.MapBasedHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;

public class ParallelSecondAnswerGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    private ParallelSecondAnswerGenerator _sut;

    @Before
    public void initHarvester() throws GeneralCryptoLibException {

        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));

        final Exponent[][] exponents =
            MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);

        _harvester.collect(BaseCommitmentGenerator.Produced.EXPONENTS, exponents);

        final PrivateCommitment[] privateCommitmentsA = getMockedPrivateCommitments(exponents);

        _harvester.collect(FirstCommitmentGenerator.Produced.PRIVATE_COMMITS, privateCommitmentsA);

        final PrivateCommitment[] privateCommitmentsB = getMockedPrivateCommitments(exponents);
        final PublicCommitment[] publicCommitmentsB = CommitmentTools.makePublic(privateCommitmentsB);

        _harvester.collect(SecondCommitmentGenerator.Produced.PRIVATE_COMMITS, privateCommitmentsB);
        _harvester.collect(SecondCommitmentGenerator.Produced.PUBLIC_COMMITS, publicCommitmentsB);

        _harvester.collect(CommonStateLabels.ORIGINAL_CIPHERTEXTS, getMockedEncryptedCiphertexts());
    }

    @Test
    public void generate_second_answer_correctly()
            throws GeneralCryptoLibException {

        _sut = new ParallelSecondAnswerGenerator(config, getMockedRandomExponents(), getMockedReEncryptedCiphertexts(),
            commitmentParams, computeAllE, limMultiExpo);

        _sut.generate(_harvester);

        final ShuffleProofSecondAnswer shuffleProofSecondAnswer =
            _harvester.offer(SecondAnswerGenerator.Produced.SHUFFLE_SECOND_ANSWER, ShuffleProofSecondAnswer.class);

        assertThat(shuffleProofSecondAnswer != null, is(true));
    }

}
