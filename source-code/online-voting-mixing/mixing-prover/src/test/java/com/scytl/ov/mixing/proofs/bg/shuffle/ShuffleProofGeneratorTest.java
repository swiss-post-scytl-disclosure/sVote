/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.shuffle;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.CiphertextImpl;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.ov.mixing.commons.beans.proofs.BayerGrothProof;
import com.scytl.ov.mixing.commons.homomorphic.Randomness;
import com.scytl.ov.mixing.proofs.bg.BasicProofsInitializer;

public class ShuffleProofGeneratorTest extends BasicProofsInitializer {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private ShuffleProofGenerator _prover;

    @Ignore
    @Test
    public void generate_shuffle_proof_correctly()
            throws GeneralCryptoLibException {

        final Ciphertext[][] encryptedCiphertexts = getMockedEncryptedCiphertexts();

        final com.scytl.ov.mixing.shuffle.Permutation permutation = getMockedPermutation();

        final Ciphertext[][] reencryptedCiphertexts = getMockedReEncryptedCiphertexts();

        final Randomness[] exponents = getMockedRandomExponents();

        _prover = new ShuffleProofGenerator(config, encryptedCiphertexts, permutation,
            privateAndPublicCommitmentsGenerator, limMultiExpo, reencryptedCiphertexts, exponents, computeAllE);

        final BayerGrothProof shuffleProof = _prover.generate();

        assertThat(shuffleProof.getInitialMessage().length, is(m));
        assertThat(shuffleProof.getFirstAnswer().length, is(m));
        assertThat(shuffleProof.getSecondAnswer() != null, is(true));
    }

    @Test
    public void throw_an_exception_when_the_encrypted_ciphertexts_is_null()
            throws GeneralCryptoLibException {

        final CiphertextImpl[][] encryptedCiphertexts = null;

        final com.scytl.ov.mixing.shuffle.Permutation permutation = getMockedPermutation();

        final Ciphertext[][] reencryptedCiphertexts = getMockedReEncryptedCiphertexts();

        final Randomness[] exponents = getMockedRandomExponents();

        thrown.expect(IllegalArgumentException.class);

        _prover = new ShuffleProofGenerator(config, encryptedCiphertexts, permutation,
            privateAndPublicCommitmentsGenerator, limMultiExpo, reencryptedCiphertexts, exponents, computeAllE);
    }

    @Test
    public void throw_an_exception_when_the_permutation_is_null()
            throws GeneralCryptoLibException {

        final Ciphertext[][] encryptedCiphertexts = getMockedEncryptedCiphertexts();

        final com.scytl.ov.mixing.shuffle.Permutation permutation = null;

        final Ciphertext[][] reencryptedCiphertexts = getMockedReEncryptedCiphertexts();

        final Randomness[] exponents = getMockedRandomExponents();

        thrown.expect(IllegalArgumentException.class);

        _prover = new ShuffleProofGenerator(config, encryptedCiphertexts, permutation,
            privateAndPublicCommitmentsGenerator, limMultiExpo, reencryptedCiphertexts, exponents, computeAllE);
    }

    @Test
    public void throw_an_exception_when_the_ciphertexts_are_null()
            throws GeneralCryptoLibException {

        final Ciphertext[][] encryptedCiphertexts = getMockedEncryptedCiphertexts();

        final com.scytl.ov.mixing.shuffle.Permutation permutation = getMockedPermutation();

        final CiphertextImpl[][] reencryptedCiphertexts = null;

        final Randomness[] exponents = getMockedRandomExponents();

        thrown.expect(IllegalArgumentException.class);

        _prover = new ShuffleProofGenerator(config, encryptedCiphertexts, permutation,
            privateAndPublicCommitmentsGenerator, limMultiExpo, reencryptedCiphertexts, exponents, computeAllE);
    }

    @Test
    public void throw_an_exception_when_the_exponents_are_null()
            throws GeneralCryptoLibException {

        final Ciphertext[][] encryptedCiphertexts = getMockedEncryptedCiphertexts();

        final com.scytl.ov.mixing.shuffle.Permutation permutation = getMockedPermutation();

        final Ciphertext[][] reencryptedCiphertexts = getMockedReEncryptedCiphertexts();

        final Randomness[] exponents = null;

        thrown.expect(IllegalArgumentException.class);

        _prover = new ShuffleProofGenerator(config, encryptedCiphertexts, permutation,
            privateAndPublicCommitmentsGenerator, limMultiExpo, reencryptedCiphertexts, exponents, computeAllE);
    }

}
