/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.shuffle;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.ov.mixing.commons.tools.RandomOracleHash;
import com.scytl.ov.mixing.proofs.bg.BasicProofsInitializer;
import com.scytl.ov.mixing.proofs.bg.state.CommonStateLabels;
import com.scytl.ov.mixing.proofs.bg.state.MapBasedHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;

public class BaseCommitmentGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    // Using one of the implementations of BaseCommitmentGenerator
    private FirstCommitmentGenerator _sut;

    @Test
    public void generate_correctly_the_commitment() throws GeneralCryptoLibException {

        final com.scytl.ov.mixing.shuffle.Permutation permutation = getMockedPermutation();

        // Using one of the implementations to test the generate method.
        _sut = new FirstCommitmentGenerator(config, permutation, commitmentParams, privateAndPublicCommitmentsGenerator,
            limMultiExpo);

        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));
        _harvester.collect(CommonStateLabels.ORIGINAL_CIPHERTEXTS, getMockedEncryptedCiphertexts());

        _sut.generate(_harvester);

    }

}
