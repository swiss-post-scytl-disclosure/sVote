/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.hadamard;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.beans.proofs.HadamardProductProofAnswer;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.ov.mixing.commons.tools.CommitmentTools;
import com.scytl.ov.mixing.commons.tools.MatrixArranger;
import com.scytl.ov.mixing.commons.tools.RandomOracleHash;
import com.scytl.ov.mixing.proofs.bg.BasicProofsInitializer;
import com.scytl.ov.mixing.proofs.bg.product.ProductProofGenerator;
import com.scytl.ov.mixing.proofs.bg.shuffle.ShuffleProofGenerator;
import com.scytl.ov.mixing.proofs.bg.state.MapBasedHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;

public class ParallelHadamardProductProofGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    private ParallelHadamardProductProofGenerator _sut;

    @Before
    public void initHarvester() {

        _harvester.collect(ShuffleProofGenerator.Produced.RANDOM_ORACLE, new RandomOracleHash(q));

        final Exponent[][] exponents =
            MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);

        final PrivateCommitment[] mockedPrivateCommitments = getMockedPrivateCommitments(exponents);

        _harvester.collect(ProductProofGenerator.Produced.PRIVATE_COMMITS_Ca, mockedPrivateCommitments);
        _harvester.collect(ProductProofGenerator.Produced.PRIVATE_COMMITS_Cb, mockedPrivateCommitments[0]);
        _harvester.collect(ProductProofGenerator.Produced.PUBLIC_COMMITMENT_Cb,
            CommitmentTools.makePublic(mockedPrivateCommitments)[0]);

    }

    @Test
    public void generate_the_proof_correctly() {

        _sut = new ParallelHadamardProductProofGenerator(m, n, q, commitmentParams, limMultiExpo);

        _sut.generate(_harvester);

        final HadamardProductProofAnswer hadamardProductProofAnswer =
            _harvester.offer(HadamardProductProofGenerator.Produced.ANSWER, HadamardProductProofAnswer.class);

        assertThat(hadamardProductProofAnswer != null, is(true));
    }
}
