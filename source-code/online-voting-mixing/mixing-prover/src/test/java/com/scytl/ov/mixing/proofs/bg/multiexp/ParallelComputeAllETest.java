/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.multiexp;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.tools.MatrixArranger;
import com.scytl.ov.mixing.proofs.bg.BasicProofsInitializer;

public class ParallelComputeAllETest extends BasicProofsInitializer {

    private Exponent[][] exponentMatrix;

    private Ciphertext[][] mockedReEncryptedCiphertexts;

    private ParallelComputeAllE _sut;

    private Ciphertext[] E;

    private Ciphertext[] Eprime;

    @Before
    public void init() throws GeneralCryptoLibException {
        _sut = new ParallelComputeAllE(ciphertextTools);

        exponentMatrix = MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);

        mockedReEncryptedCiphertexts = getMockedReEncryptedCiphertexts();

        E = _sut.computeAllENoRandomizing(exponentMatrix, mockedReEncryptedCiphertexts, q);

        Eprime = _sut.addA0FactorsIncludingE0(E, mockedReEncryptedCiphertexts, exponentMatrix[0]);
    }

    @Test
    public void compute_all_E_non_randomizing_correctly() {

        final Ciphertext[] ciphertexts =
            _sut.computeAllENoRandomizing(exponentMatrix, mockedReEncryptedCiphertexts, q);

        assertThat(ciphertexts[0] != null, is(true));

    }

    @Test
    public void add_randomizing_encryption() {

        Ciphertext[] ciphertexts =
            _sut.addA0FactorsIncludingE0(Eprime, mockedReEncryptedCiphertexts, exponentMatrix[0]);
        assertThat(ciphertexts[0] != null, is(true));

    }

    @Test
    public void add_a0_factors_including_e0() {

        Ciphertext[] ciphertexts =
            _sut.addA0FactorsIncludingE0(E, mockedReEncryptedCiphertexts, exponentMatrix[0]);
        assertThat(ciphertexts[0] != null, is(true));

    }
}
