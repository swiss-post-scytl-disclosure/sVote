/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.shuffle;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigInteger;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.proofs.bg.BasicProofsInitializer;

public class MathUtilsTest extends BasicProofsInitializer {

    @Test
    public void compute_the_products_sequence_of_a_number_to_the_power_of_zero_correctly() {

        try {

            final long number = 10;
            final Exponent anyExponent = new Exponent(q, new BigInteger("10"));
            final int power = 0;

            performProductSequence(number, anyExponent, power);

        } catch (GeneralCryptoLibException ex) {
            throw new CryptoLibException(ex);
        }
    }

    @Test
    public void compute_the_products_sequence_of_two_to_the_power_of_two_correctly() {

        try {

            final long two = 2;
            final Exponent twoExponent = new Exponent(q, new BigInteger("2"));
            final int power = 2;

            performProductSequence(two, twoExponent, power);

        } catch (GeneralCryptoLibException ex) {
            throw new CryptoLibException(ex);
        }
    }

    @Test
    public void compute_the_products_sequence_of_thre_to_the_power_of_two_correctly() {

        try {

            final long three = 3;
            final Exponent threeExponent = new Exponent(q, new BigInteger("3"));
            final int power = 2;

            performProductSequence(three, threeExponent, power);

        } catch (GeneralCryptoLibException ex) {
            throw new CryptoLibException(ex);
        }
    }

    private void performProductSequence(final long two, final Exponent twoExponent, final int power) {
        final Exponent[] exponents = MathUtils.productsSequence(twoExponent, power);

        long result = two;

        for (Exponent exponent : exponents) {
            final String resultString = Long.toString(result);
            assertThat(exponent.getValue().toString().equals(resultString), is(true));
            result = (long) Math.pow(result, power);
        }
    }
}
