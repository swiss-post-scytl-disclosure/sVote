/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.multiexp.basic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.beans.proofs.MultiExponentiationBasicProofInitialMessage;
import com.scytl.ov.mixing.commons.homomorphic.impl.GjosteenElGamalRandomness;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.ov.mixing.commons.tools.CommitmentTools;
import com.scytl.ov.mixing.commons.tools.ExponentTools;
import com.scytl.ov.mixing.commons.tools.MatrixArranger;
import com.scytl.ov.mixing.commons.tools.RandomOracleHash;
import com.scytl.ov.mixing.proofs.bg.BasicProofsInitializer;
import com.scytl.ov.mixing.proofs.bg.multiexp.MultiExponentiationProofGenerator;
import com.scytl.ov.mixing.proofs.bg.shuffle.SecondCommitmentGenerator;
import com.scytl.ov.mixing.proofs.bg.state.CommonStateLabels;
import com.scytl.ov.mixing.proofs.bg.state.MapBasedHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;

public class ParallelBasicInitialMessageGeneratorTest extends BasicProofsInitializer {

    private final StateHarvester _harvester = new MapBasedHarvester();

    private ParallelBasicInitialMessageGenerator _sut;

    @Before
    public void initHarvester() throws GeneralCryptoLibException {

        _harvester.collect(BasicProofGenerator.Produced.MULTIEXP_BASIC_RO, new RandomOracleHash(q));

        _harvester.collect(CommonStateLabels.CIPHERTEXTS, getMockedReEncryptedCiphertexts());

        final Exponent[][] exponents =
            MatrixArranger.transformPermutationToExponentMatrix(getMockedPermutation(), q, m, n);
        _harvester.collect(BasicInitialMessageGenerator.Produced.EXPONENTS_MULTIARR_B, exponents);

        final PrivateCommitment[] privateCommitmentsB = getMockedPrivateCommitments(exponents);
        _harvester.collect(SecondCommitmentGenerator.Produced.PRIVATE_COMMITS, privateCommitmentsB);
        _harvester.collect(SecondCommitmentGenerator.Produced.PUBLIC_COMMITS,
            CommitmentTools.makePublic(privateCommitmentsB));

        _harvester.collect(MultiExponentiationProofGenerator.Produced.CALCULATED_RANDOM_EXPONENT,
            new GjosteenElGamalRandomness(ExponentTools.getRandomExponent(q)));

    }

    @Test
    public void generate_the_basic_initial_message_correctly() throws GeneralCryptoLibException {

        _sut = new ParallelBasicInitialMessageGenerator(config, commitmentParams, limMultiExpo, computeAllE,
            concurrencyLevel);
        _sut.generate(_harvester);

        final MultiExponentiationBasicProofInitialMessage multiExponentiationBasicProofInitialMessage =
            _harvester.offer(BasicInitialMessageGenerator.Produced.BASIC_MULTIEXP_INIT_MSG,
                MultiExponentiationBasicProofInitialMessage.class);

        assertThat(multiExponentiationBasicProofInitialMessage != null, is(true));

    }
}
