/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.shuffle;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

/**
 * Provides some mathematical utilities.
 */
public class MathUtils {

    /**
     * Non-public constructor
     */
	private MathUtils() {    
    }
    
	/**
     * Exponentiates the given challenge to power, keeping all the intermediate values of the iterative multiplication.
     *
     * @param challengeX
     * @return
     */
    public static Exponent[] productsSequence(final Exponent challengeX, final int power) {

        try {

            final Exponent[] result = new Exponent[power];
            Exponent acummulated = challengeX;

            for (int i = 0; i < result.length; i++) {
                result[i] = acummulated;
                acummulated = acummulated.multiply(challengeX);
            }
            return result;

        } catch (GeneralCryptoLibException ex) {
            throw new CryptoLibException(ex);
        }
    }
}
