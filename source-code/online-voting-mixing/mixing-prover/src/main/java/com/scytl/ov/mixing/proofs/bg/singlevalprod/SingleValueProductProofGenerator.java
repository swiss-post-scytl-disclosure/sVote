/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.singlevalprod;

import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateLabel;

/**
 * Defines the required methods to generate the single value product proof.
 */
public interface SingleValueProductProofGenerator {

    /**
     * Generates the single value product proof message and allows the {@code harvester} to collect the result.
     *
     * @param harvester
     */
    void generate(StateHarvester harvester);

    enum Produced implements StateLabel {
        INITIAL_MSG, ANSWER, //
        RANDOM_ORACLE, EXPONENTS_A, EXPONENTS_B, EXPONENTS_D, //
        EXPONENTS_LOW_DELTA, EXPONENTS_CAPITAL_DELTA, EXPONENT_RD, EXPONENTS_DELTA, //
        EXPONENT_SX, EXPONENT_S1, STATEMENT_CA, STATEMENT_B
    }

}
