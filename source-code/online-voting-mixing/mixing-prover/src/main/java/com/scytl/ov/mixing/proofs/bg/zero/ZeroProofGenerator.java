/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.zero;

import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateLabel;

/**
 * Defines the required methods to generate the zero proof.
 */
public interface ZeroProofGenerator {

    /**
     * Generates the zero proof and allows the {@code harvester} to collect the result.
     *
     * @param harvester
     */
    void generate(StateHarvester harvester);

    enum Produced implements StateLabel {
        INITIAL_MSG, ANSWER, //
        RANDOM_ORACLE, //
        PRIVATE_COMMITMENTS_cA, PRIVATE_COMMITMENTS_cB, PRIVATE_COMMITMENTS_A, //
        EXPONENTS_MATRIX_A, EXPONENTS_R, EXPONENTS_MATRIX_B, EXPONENTS_S, EXPONENTS_T, PUBLIC_COMMITMENTS_CA, PUBLIC_COMMITMENTS_CB
    }

}
