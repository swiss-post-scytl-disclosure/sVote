/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.multiexp.basic;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.CommitmentParams;
import com.scytl.ov.mixing.commons.proofs.bg.config.ProofsGeneratorConfigurationParams;
import com.scytl.ov.mixing.commons.tools.MultiExponentiation;
import com.scytl.ov.mixing.commons.tools.RandomOracleHash;
import com.scytl.ov.mixing.proofs.bg.multiexp.ComputeAllE;
import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;

/**
 * Implements the parallel mode.
 */
public class ParallelBasicProofGenerator implements BasicProofGenerator {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private final BasicInitialMessageGenerator initMessageGen;

    private final BasicAnswerGenerator answerGen;

    private final BigInteger groupOrder;

    public ParallelBasicProofGenerator(final ProofsGeneratorConfigurationParams config, final CommitmentParams params,
            final MultiExponentiation limMultiExpo, final ComputeAllE computeAllE, final int concurrencyLevel) {
        super();
		this.initMessageGen = new ParallelBasicInitialMessageGenerator(config, params, limMultiExpo, computeAllE,
				concurrencyLevel);
		this.answerGen = new ParallelBasicAnswerGenerator();
		this.groupOrder = config.getZpGroupParams().getQ();
    }

    @Override
    public void generate(final StateHarvester harvester) throws GeneralCryptoLibException {

        harvester.collect(Produced.MULTIEXP_BASIC_RO, new RandomOracleHash(groupOrder));

        LOG.info("\t MEP B Generating initial message...");
        initMessageGen.generate(harvester);

        LOG.info("\t MEP B Generating answer...");
        answerGen.generate(harvester);
    }
}
