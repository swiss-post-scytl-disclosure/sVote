/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.multiexp.basic;

import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateLabel;

/**
 * Defines the methods to generate the answer of the multi-exponentiation proof without reduction.
 */
@FunctionalInterface 
public interface BasicAnswerGenerator {

    enum Produced implements StateLabel {
        BASIC_MULTIEXP_ANSWER
    }

    /**
     * Generates the answer and allows the {@code harvester} to collect the results.
     *
     * @param harvester
     * @throws StateRetrievalException
     */
    void generate(StateHarvester harvester);

}
