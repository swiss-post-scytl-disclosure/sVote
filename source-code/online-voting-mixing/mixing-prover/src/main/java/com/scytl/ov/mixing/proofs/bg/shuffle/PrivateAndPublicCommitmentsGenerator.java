/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.shuffle;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.CommitmentParams;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PublicCommitment;
import com.scytl.ov.mixing.commons.tools.MultiExponentiation;

/**
 * Generates and sets the values of two given arrays of {@link PrivateCommitment} and {@link PublicCommitment},
 * respectively.
 */
public interface PrivateAndPublicCommitmentsGenerator {

    /**
     * Generates and sets the values of the given {@link PrivateCommitment} and {@link PublicCommitment} arrays.
     *
     * @param exponents
     * @param r
     * @param m
     * @param comParams
     * @param privateCommitments
     * @param publicCommitments
     * @param limMultiExpo
     */
    void generateAndSet(final Exponent[][] exponents, final Exponent[] r, final int m, final CommitmentParams comParams,
            final PrivateCommitment[] privateCommitments, final PublicCommitment[] publicCommitments,
            final MultiExponentiation limMultiExpo, final int concurrencyLevel);

}
