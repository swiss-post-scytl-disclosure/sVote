/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.product;

import com.scytl.ov.mixing.proofs.bg.state.StateHarvester;
import com.scytl.ov.mixing.proofs.bg.state.StateLabel;

/**
 * Defines the required methods to generate the product proof.
 */
public interface ProductProofGenerator {

    /**
     * Generates the product proof message and allows the {@code harvester} to collect the result.
     *
     * @param harvester
     */
    void generate(StateHarvester harvester);

    enum Produced implements StateLabel {
        PRODUCT_PROOF_MSG, //
        PRIVATE_COMMITS_Ca, PRIVATE_COMMITS_Cb, PUBLIC_COMMITMENT_Cb
    }

}
