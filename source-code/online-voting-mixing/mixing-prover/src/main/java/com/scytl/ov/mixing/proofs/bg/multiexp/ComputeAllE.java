/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.proofs.bg.multiexp;

import java.math.BigInteger;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.homomorphic.Cryptosystem;
import com.scytl.ov.mixing.commons.homomorphic.Randomness;

public interface ComputeAllE {

    Ciphertext[] computeAllENoRandomizing(Exponent[][] a, Ciphertext[][] vecC, BigInteger order);

    Ciphertext[] addRandomizingEncryption(Ciphertext[] ePrime, Exponent[] b, Randomness[] tau,
            Cryptosystem cryptosystem, final int concurrencyLevel);

    Ciphertext[] addA0FactorsIncludingE0(Ciphertext[] ePrime, Ciphertext[][] vecC, Exponent[] a0);

}
