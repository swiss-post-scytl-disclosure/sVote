/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;
import com.scytl.products.ov.mixnet.commons.beans.ShuffleOutput;
import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.CommitmentParams;
import com.scytl.products.ov.mixnet.commons.proofs.bg.config.ProofsGeneratorConfigurationParams;
import com.scytl.products.ov.mixnet.commons.tools.MultiExponentiation;
import com.scytl.products.ov.mixnet.proofs.bg.multiexp.ParallelComputeAllE;

/**
 * Input object for the shuffle proof service, for better readability of a multiple-parameter function call.
 */
public class ShuffleProofInput {

  private ShuffleProofInput(final ElGamalEncryptedBallots encryptedBallots,
      final ShuffleOutput<ElGamalEncryptedBallots> shuffleOutput,
      final int m, final int n, final CommitmentParams commitmentParams,
      final ProofsGeneratorConfigurationParams
          proofsGeneratorConfigurationParams,
      final ApplicationConfig applicationConfig,
      final MultiExponentiation multiExponentiation,
      final ParallelComputeAllE parallelComputeAllE,
      final Integer batchNum,
      final int partitionSize, final int offset) {
  }

  static Builder builder() {
    return new Builder();
  }

  /**
   * Implementation of the Builder pattern to alleviate the creation of
   * ShuffleProofInput instances.
   */
  static class Builder {
    private ElGamalEncryptedBallots encryptedBallots;
    private ShuffleOutput<ElGamalEncryptedBallots> shuffleOutput;
    private int m;
    private int n;
    private CommitmentParams commitmentParams;
    private ProofsGeneratorConfigurationParams proofsGeneratorConfigurationParams;
    private ApplicationConfig applicationConfig;
    private MultiExponentiation multiExponentiation;
    private ParallelComputeAllE parallelComputeAllE;
    private Integer batchNum;
    private int partitionSize;
    private int offset;

    private Builder() {
      // Prevent new instances from outside use of the constructor.
    }

    /**
     * Build a new object with the current parameters.
     *
     * @return a new ShuffleProofInput object.
     */
    ShuffleProofInput build() {
      return new ShuffleProofInput(
          encryptedBallots, shuffleOutput, m, n, commitmentParams,
          proofsGeneratorConfigurationParams, applicationConfig,
          multiExponentiation, parallelComputeAllE, batchNum,
          partitionSize, offset);
    }

    Builder addEncryptedBallots(ElGamalEncryptedBallots encryptedBallots) {
      this.encryptedBallots = encryptedBallots;

      return this;
    }
  }
}
