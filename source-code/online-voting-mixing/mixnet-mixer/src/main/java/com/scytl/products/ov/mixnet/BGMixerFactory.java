/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import java.util.concurrent.ForkJoinPool;

/**
 * Factory of {@link BGMixer}.
 */
public interface BGMixerFactory {
    /**
     * Creates a new {@link BGMixer} instance for a given executor.
     * 
     * @param executor
     *            the executor
     * @return a new instance.
     */
    BGMixer newBGMixer(ForkJoinPool executor);
}
