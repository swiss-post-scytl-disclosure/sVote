/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;
import com.scytl.products.ov.mixnet.commons.beans.ShuffleOutput;
import com.scytl.products.ov.mixnet.commons.beans.proofs.ShuffleProof;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.CommitmentParams;
import com.scytl.products.ov.mixnet.commons.proofs.bg.config.ProofsGeneratorConfigurationParams;
import com.scytl.products.ov.mixnet.commons.tools.MultiExponentiation;
import com.scytl.products.ov.mixnet.proofs.bg.multiexp.ParallelComputeAllE;

/**
 * Generates shuffle proofs.
 */
public interface ShuffleProofService {

  /**
   * Generates a shuffle proof for a ballot box.
   *
   * @return a proof of the shuffle
   */
  ShuffleProof generate(final ElGamalEncryptedBallots encryptedBallots,
      final ShuffleOutput<ElGamalEncryptedBallots> shuffleOutput,
      final int m, final int n, final CommitmentParams commitmentParams,
      final ProofsGeneratorConfigurationParams
          proofsGeneratorConfigurationParams,
      final int concurrencyLevel,
      final MultiExponentiation multiExponentiation,
      final ParallelComputeAllE parallelComputeAllE,
      final int batchNum,
      final int partitionSize, final int offset);
}
