/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.cryptolib.elgamal.bean.ElGamalComputationsValues;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;

import java.util.ArrayList;
import java.util.List;

public class PartialBallotDecryptionService extends AbstractBallotDecryptionService {

	public PartialBallotDecryptionService(TransactionInfoProvider transactionInfoProvider,
			PrimitivesServiceAPI primitivesService, ElGamalServiceAPI elGamalService, 
			ProofsServiceAPI proofsService, SecureLoggingWriter secureLog) {
		super(transactionInfoProvider, primitivesService, elGamalService, proofsService, secureLog);
	}

	@Override
	protected List<ZpGroupElement> provideProofInput(List<ZpGroupElement> plaintexts) throws GeneralCryptoLibException {
		return plaintexts.subList(1, plaintexts.size());
	}

	@Override
	protected List<ZpGroupElement> provideDecryptionFinalResults(ElGamalComputationsValues values,
			List<ZpGroupElement> results) throws GeneralCryptoLibException {
		List<ZpGroupElement> mergedResults = new ArrayList<>();
		mergedResults.add(values.getGamma());
		mergedResults.addAll(results);
		return mergedResults;
	}

}
