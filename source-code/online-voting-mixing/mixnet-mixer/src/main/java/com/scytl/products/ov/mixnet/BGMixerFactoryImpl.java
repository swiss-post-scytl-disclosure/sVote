/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import java.util.concurrent.ForkJoinPool;

import com.scytl.products.ov.mixnet.commons.io.JSONProofsWriter;
import com.scytl.products.ov.mixnet.shuffler.ElGamalShuffler;

/**
 * Implementation of {@link BGMixerFactory}.
 */
public final class BGMixerFactoryImpl implements BGMixerFactory {
    private final ElGamalShuffler shuffler;

    private final JSONProofsWriter proofsWriter;

    private final BallotDecryptionService ballotDecryptionService;

    private final ShuffleProofService shuffleProofService;

    /**
     * Constructor.
     * 
     * @param shuffler
     * @param proofsWriter
     * @param ballotDecryptionService
     * @param shuffleProofService
     */
    public BGMixerFactoryImpl(ElGamalShuffler shuffler,
            JSONProofsWriter proofsWriter,
            BallotDecryptionService ballotDecryptionService,
            ShuffleProofService shuffleProofService) {
        this.shuffler = shuffler;
        this.proofsWriter = proofsWriter;
        this.ballotDecryptionService = ballotDecryptionService;
        this.shuffleProofService = shuffleProofService;
    }

    @Override
    public BGMixer newBGMixer(ForkJoinPool executor) {
        return new BGMixer(shuffler, proofsWriter, ballotDecryptionService,
            shuffleProofService, executor);
    }
}
