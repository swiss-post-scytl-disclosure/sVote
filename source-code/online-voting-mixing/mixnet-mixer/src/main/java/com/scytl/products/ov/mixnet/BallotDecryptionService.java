/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.beans.VoteWithProof;

/**
 * Decrypts encrypted ballots.
 */
public interface BallotDecryptionService {

    /**
     * Decrypts a set of encrypted ballots.
     *
     * @param encryptedBallots
     *            the encrypted ballots
     * @param privateKey
     *            the ElGamal private key to decrypt the ballots
     * @return the input ballots' votes with proofs
     * @throws GeneralCryptoLibException
     *             on unsuccessful decryption
     */
    List<VoteWithProof> decrypt(List<Ciphertext> encryptedBallots, ElGamalPrivateKey privateKey,
            final int batchNum, final int partitionSize, final int offset);
}
