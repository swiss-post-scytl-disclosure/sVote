/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.preprocess;

import java.util.List;

import org.junit.Test;

/**
 *
 */
public class PartitionerDemoTest {

    @Test
    public void try_different_values() {

        int minimumBatchSize = 512;
        Partitioner _sut = new Partitioner(minimumBatchSize);

        int N = 1;

        List<Integer> partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 2;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 3;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 511;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 512;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 513;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 1023;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 1024;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 1025;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 1033;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 1535;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 1536;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 1537;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 2047;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 2048;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 2049;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 2559;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 2560;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 2561;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 3072;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

        N = 3073;

        partitions = _sut.generatePartitionsOfMinimumSize(N);
        printValues(N, partitions);

    }

    /**
     * @param N
     * @param partitions
     */
    private void printValues(final int N, final List<Integer> partitions) {
        System.out.println("For MinimumSize = 512 and N = " + N + " the partition has " + partitions.size()
            + " element/s -> " + partitions);
    }

}
