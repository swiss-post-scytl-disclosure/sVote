/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.tools;

import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.beans.Permutation;

public class MatrixArrangerTest {

    @Test
    public void asMatrixTest() {
        int m = 2;
        int n = 3;

        final int[] permutationAsIntArray = new int[m * n];
        for (int i = 0; i < permutationAsIntArray.length; i++) {
            permutationAsIntArray[i] = i;
        }
        Permutation permutation = new Permutation(permutationAsIntArray);

        BigInteger order = new BigInteger("13");
        Exponent[][] result = MatrixArranger.transformPermutationToExponentMatrix(permutation, order, m, n);
        Assert.assertEquals(new BigInteger("1"), result[0][0].getValue());
        Assert.assertEquals(new BigInteger("2"), result[0][1].getValue());
        Assert.assertEquals(new BigInteger("3"), result[0][2].getValue());
        Assert.assertEquals(new BigInteger("4"), result[1][0].getValue());
        Assert.assertEquals(new BigInteger("5"), result[1][1].getValue());
        Assert.assertEquals(new BigInteger("6"), result[1][2].getValue());
    }

}
