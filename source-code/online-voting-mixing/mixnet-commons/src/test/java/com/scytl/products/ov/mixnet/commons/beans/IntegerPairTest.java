/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.beans;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IntegerPairTest {

    private static IntPair _target;

    @BeforeClass
    public static void setUp() throws IOException {

        _target = new IntPair(new Integer("10"), new Integer("20"));
    }

    @Test
    public void return_expected_values() {

        String errorMsg = "The left value that was returned was not the expected value";
        assertEquals(errorMsg, 10, _target.getLeft());

        errorMsg = "The right value that was returned was not the expected value";
        assertEquals(errorMsg, 20, _target.getRight());
    }

    @Test
    public void implement_the_toString_method() {

        String errorMsg = "The string return from the toString method was empty";
        assertTrue(errorMsg, _target.toString().length() > 0);
    }
}
