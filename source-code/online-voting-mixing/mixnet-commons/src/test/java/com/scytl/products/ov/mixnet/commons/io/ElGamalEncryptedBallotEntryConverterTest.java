/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.CiphertextImpl;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;

public class ElGamalEncryptedBallotEntryConverterTest {

    private static ElGamalEncryptedBallotEntryConverter target;

    private static CiphertextImpl encryptedBallot;

    private static String gammaAsString;

    private static String phi1AsString;

    private static String phi2AsString;

    @BeforeClass
    public static void setUp() {

        try {
            target = new ElGamalEncryptedBallotEntryConverter();

            gammaAsString = "4012556967239296284125919604329103854893595";
            phi1AsString = "6054959594806155309765068576952771674747761";
            phi2AsString = "11583975953794885768215634543368899452667420";

            final BigInteger p = new BigInteger("15294034768093677312256663166625633354362303");
            final BigInteger q = new BigInteger("7647017384046838656128331583312816677181151");

            final List<ZpGroupElement> elements = new ArrayList<>();
            elements.add(new ZpGroupElement(new BigInteger(gammaAsString), p, q));
            elements.add(new ZpGroupElement(new BigInteger(phi1AsString), p, q));
            elements.add(new ZpGroupElement(new BigInteger(phi2AsString), p, q));

            encryptedBallot = new CiphertextImpl(elements.get(0), elements.subList(1, elements.size()));

        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }

    @Test(expected = RuntimeException.class)
    public void givenNullEncyptedBallotWhenConvertThenException() {

        final CiphertextImpl nullElGamalEncryptedBallot = null;
        target.convertEntry(nullElGamalEncryptedBallot);
    }

    @Test
    public void givenEncyptedBallotWhenConvertThenOk() {

        final String[] output = target.convertEntry(encryptedBallot);

        final int expectedArrayLength = 3;
        final int actualArrayLength = output.length;

        String errorMsg = "The generated array did not contain the expected number of elements. Expected: "
            + expectedArrayLength + ", but was: " + actualArrayLength;
        assertEquals(errorMsg, expectedArrayLength, output.length);

        errorMsg = "The first value in the generated array did not have expected value";
        assertStringEqualsExpectedString(errorMsg, gammaAsString, output[0]);

        errorMsg = "The second value in the generated array did not have expected value";
        assertStringEqualsExpectedString(errorMsg, phi1AsString, output[1]);

        errorMsg = "The third value in the generated array did not have expected value";
        assertStringEqualsExpectedString(errorMsg, phi2AsString, output[2]);
    }

    private void assertStringEqualsExpectedString(final String errorMsg, final String expectedString,
            final String actualString) {

        assertEquals(errorMsg, expectedString, actualString);
    }
}
