/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.beans.proofs;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigInteger;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.tools.ExponentTools;

public class SingleValueProductProofAnswerTest {

    private static final BigInteger _q = new BigInteger("11");

    private static Exponent[] _tildeA;

    private static Exponent[] _tildeB;

    private static Exponent _tildeR;

    private static Exponent _tildeS;

    private static SingleValueProductProofAnswer _target;

    @BeforeClass
    public static void setUp() throws IOException {

        _tildeA = new Exponent[1];
        _tildeA[0] = ExponentTools.getRandomExponent(_q);

        _tildeB = new Exponent[1];
        _tildeB[0] = ExponentTools.getRandomExponent(_q);

        _tildeR = ExponentTools.getRandomExponent(_q);

        _tildeS = ExponentTools.getRandomExponent(_q);

        _target = new SingleValueProductProofAnswer(_tildeA, _tildeB, _tildeR, _tildeS);
    }

    @Test
    public void retrieve_tildeA() {

        final String errorMsg = "retrieved 'tilde a' was not the expected value";
        assertArrayEquals(errorMsg, _tildeA, _target.getExponentsTildeA());
    }

    @Test
    public void retrieve_tildeB() {

        final String errorMsg = "retrieved 'tilde b' was not the expected value";
        assertArrayEquals(errorMsg, _tildeB, _target.getExponentsTildeB());
    }

    @Test
    public void retrieve_tildeR() {

        final String errorMsg = "retrieved 'tilde r' was not the expected value";
        assertEquals(errorMsg, _tildeR, _target.getExponentTildeR());
    }

    @Test
    public void retrieve_tildeS() {

        final String errorMsg = "retrieved 'tilde s' was not the expected value";
        assertEquals(errorMsg, _tildeS, _target.getExponentTildeS());
    }

    @Test
    public void construct_expected_string_representation() {

        final String returnedStringRepresentation = _target.toString();

        final StringBuilder sb = new StringBuilder();
        for (Exponent a_tildeA : _tildeA) {
            sb.append(a_tildeA);
        }
        for (Exponent a_tildeB : _tildeB) {
            sb.append(a_tildeB);
        }
        sb.append(_tildeR);
        sb.append(_tildeS);

        String errorMsg = "retrieved string representation did not match expected value";
        assertTrue(errorMsg, returnedStringRepresentation.equals(sb.toString()));
    }
}
