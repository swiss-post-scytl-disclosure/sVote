/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.tests;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;

public class CommonTestingUtilities {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    public static Properties loadGroupConfig(final InputStream inputStream) throws IOException {

        final Properties config = new Properties();

        try (final InputStream bufferedInput = new BufferedInputStream(inputStream)) {

            config.load(bufferedInput);
        }

        return config;
    }

    public static ElGamalEncryptedBallots filterBallots(final ElGamalEncryptedBallots encryptedBallots_VerifierCopy,
            final int numRequiredElements) {

        if (numRequiredElements > encryptedBallots_VerifierCopy.getBallots().size()) {
            LOG.error("Trying to obtain more ballots than available, returning null...");
            return null;
        }

        List<Ciphertext> listBallots = new ArrayList<>();

        List<Ciphertext> extractedList = encryptedBallots_VerifierCopy.getBallots();

        for (int i = 0; i < numRequiredElements; i++) {
            listBallots.add(extractedList.get(i));
        }

        return new ElGamalEncryptedBallots(listBallots);
    }
}
