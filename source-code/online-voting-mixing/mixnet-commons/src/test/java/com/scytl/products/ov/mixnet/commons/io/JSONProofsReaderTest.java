/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.products.ov.mixnet.commons.beans.proofs.ShuffleProof;
import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.VerifierConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;

public class JSONProofsReaderTest {

    private static JSONProofsReader jsonProofsReader;

    private static LocationsProvider locations;

    @BeforeClass
    public static void setUp() throws FileNotFoundException {

        final ApplicationConfig mixnetConfig = new ApplicationConfig();
        final VerifierConfig verifierConfig = new VerifierConfig();
        List<InputStream> proofsInputOfBatch = new ArrayList<>();
        proofsInputOfBatch.add(new FileInputStream(Paths.get("src/test/resources/input/proofs.json").toFile()));
        verifierConfig.setProofsInputOfBatch(proofsInputOfBatch);
        mixnetConfig.setVerifier(verifierConfig);

        locations = LocationsProvider.build(mixnetConfig);

        jsonProofsReader = new JSONProofsReader();
    }

    @Test
    public void whenReadingJSONtoShuffleProofsThenOK() throws IOException {

        final ShuffleProof shuffleProof =
            jsonProofsReader.read(locations.getVerifierLocationsProvider().getProofsInputOfBatch(1));

        final String errorMsg = "Expected that the ShuffleProof would have been initialized";
        assertNotNull(errorMsg, shuffleProof);
    }
}
