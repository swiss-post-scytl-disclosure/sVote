/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.constants;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ConstantsTest {

    @Test
    public void have_a_valid_p_set() {

        String errorMsg = "P is null";
        assertNotNull(errorMsg, Constants.P);

        errorMsg = "P is empty";
        assertTrue(errorMsg, Constants.P.length() > 0);
    }

    @Test
    public void have_a_valid_order_set() {

        String errorMsg = "The order is null";
        assertNotNull(errorMsg, Constants.ORDER);

        errorMsg = "The order is empty";
        assertTrue(errorMsg, Constants.ORDER.length() > 0);
    }

    @Test
    public void have_a_valid_generator_set() {

        String errorMsg = "The generator is null";
        assertNotNull(errorMsg, Constants.GENERATOR);

        errorMsg = "The generator is empty";
        assertTrue(errorMsg, Constants.GENERATOR.length() > 0);
    }

    @Test
    public void have_a_valid_CSV_file_extension_set() {

        String errorMsg = "The CSV file extension is null";
        assertNotNull(errorMsg, Constants.CSV_FILE_EXTENSION);
    }

    @Test
    public void have_a_valid_JSON_file_extension_set() {

        String errorMsg = "The JSON file extension is null";
        assertNotNull(errorMsg, Constants.JSON_FILE_EXTENSION);
    }

    @Test
    public void have_a_valid_element_delimiter_set() {

        String errorMsg = "The element delimiter is null";
        assertNotNull(errorMsg, Constants.ELEMENT_DELIMITER);
    }

    @Test
    public void have_a_valid_multiplicative_indentity() {

        String errorMsg = "The multiplicative identity is null";
        assertNotNull(errorMsg, Constants.MULTIPLICATIVE_IDENTITY);
    }

    @Test
    public void have_a_valid_prefix_encrypted_ballots() {

        String errorMsg = "The prefix encrypted ballots is null";
        assertNotNull(errorMsg, Constants.PREFIX_ENCRYPTED_BALLOTS);
    }

    @Test
    public void have_a_valid_config_file() {

        String errorMsg = "The config file is null";
        assertNotNull(errorMsg, Constants.CONFIG_FILE);

        errorMsg = "The config file is empty";
        assertTrue(errorMsg, Constants.CONFIG_FILE.length() > 0);
    }

    @Test
    public void have_a_valid_input_path() {

        String errorMsg = "The input path is null";
        assertNotNull(errorMsg, Constants.INPUT_PATH);
    }
}
