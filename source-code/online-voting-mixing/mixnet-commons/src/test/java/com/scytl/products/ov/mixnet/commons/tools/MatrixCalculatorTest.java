/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.tools;

import com.scytl.products.ov.mixnet.commons.configuration.MatrixDimensions;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 *
 */
public class MatrixCalculatorTest {

    /**
     * This is a test only to validate that there are not more than 1 possible
     * candidate combination with the same minor m
     */
    @Test
    public void getMinorMTest() {

        final MatrixCalculator matrixCalculator = new MatrixCalculator();

        for (int N = 0; N < 8192; N++) {
            List<MatrixDimensions> combinations = matrixCalculator.generateCombinationsWithoutOnes(N);
            MatrixDimensions result = matrixCalculator.getMatrixWithSmallestM(combinations);
            if (result != null) {
                Assert.assertEquals(N, (result.getNumberOfColumns() * result.getNumberOfRows()));
            }
        }
    }
}
