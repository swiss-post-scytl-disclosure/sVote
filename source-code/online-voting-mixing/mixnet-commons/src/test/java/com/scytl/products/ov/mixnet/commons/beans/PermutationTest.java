/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.beans;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class PermutationTest {

    private static Permutation _target;

    @BeforeClass
    public static void setUp() throws IOException {

        int[] permutationAsIntArray = new int[5];
        permutationAsIntArray[0] = 4;
        permutationAsIntArray[1] = 3;
        permutationAsIntArray[2] = 2;
        permutationAsIntArray[3] = 1;
        permutationAsIntArray[4] = 0;

        _target = new Permutation(permutationAsIntArray);
    }

    @Test
    public void retrieve_permutation_length() {

        String errorMsg = "return permutation length was not the expected value";
        assertEquals(errorMsg, 5, _target.getLength());
    }

    @Test
    public void retrieve_destination_index() {

        String errorMsg = "return permutation length was not the expected value";
        assertEquals(errorMsg, 4, _target.destinationOf(0));
        assertEquals(errorMsg, 3, _target.destinationOf(1));
        assertEquals(errorMsg, 2, _target.destinationOf(2));
        assertEquals(errorMsg, 1, _target.destinationOf(3));
        assertEquals(errorMsg, 0, _target.destinationOf(4));
    }
}
