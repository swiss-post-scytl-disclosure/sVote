/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.tools;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.products.ov.mixnet.commons.tools.LoopTools.Range;

/**
 *
 */
public class LoopToolsTest {

    @Test
    public void asRangesList() {

        final int[][] inputs = // { items, rangesCount }
            {{6, 2 }, {7, 2 }, {2, 2 }, {5, 1 }, {1, 1 }, {8, 0 }, {15, 4 }, {7, 3 }, {3, 5 } };

        final List<List<Range>> allExpectedRanges = new ArrayList<>(inputs.length);

        List<Range> expected = new ArrayList<>();
        expected.add(new Range(0, 3));
        expected.add(new Range(3, 6));
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        expected.add(new Range(0, 4));
        expected.add(new Range(4, 7));
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        expected.add(new Range(0, 1));
        expected.add(new Range(1, 2));
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        expected.add(new Range(0, 5));
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        expected.add(new Range(0, 1));
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        expected.add(new Range(0, 4));
        expected.add(new Range(4, 8));
        expected.add(new Range(8, 12));
        expected.add(new Range(12, 15));
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        expected.add(new Range(0, 3));
        expected.add(new Range(3, 5));
        expected.add(new Range(5, 7));
        allExpectedRanges.add(expected);

        expected = new ArrayList<>();
        expected.add(new Range(0, 1));
        expected.add(new Range(1, 2));
        expected.add(new Range(2, 3));
        allExpectedRanges.add(expected);

        for (int i = 0; i < inputs.length; i++) {
            final int[] input = inputs[i];
            System.out.println(i);
            final List<Range> actualRanges = LoopTools.asRangesList(input[0], input[1]);

            final List<Range> expectedRanges = allExpectedRanges.get(i);

            Assert.assertEquals(expectedRanges.size(), actualRanges.size());

            for (int j = 0; j < actualRanges.size(); j++) {

                Assert.assertEquals(expectedRanges.get(j).getStart(), actualRanges.get(j).getStart());

                Assert.assertEquals(expectedRanges.get(j).getEnd(), actualRanges.get(j).getEnd());
            }
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void asRangesListWhenNegativeRangesCountThenException() {
        LoopTools.asRangesList(4, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void asRangesListWhenNegativeArrayLengthThenException() {
        LoopTools.asRangesList(-4, 1);
    }

    @Test
    public void asRangesListWhenZeroArrayLengthThenTrivialRangeReturned() {

        final List<Range> ranges = LoopTools.asRangesList(0, 2);
        Assert.assertEquals(1, ranges.size());
        Assert.assertEquals(0, ranges.get(0).getStart());
        Assert.assertEquals(0, ranges.get(0).getEnd());
    }

}
