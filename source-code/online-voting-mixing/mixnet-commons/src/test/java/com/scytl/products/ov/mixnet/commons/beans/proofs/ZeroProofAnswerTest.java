/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.beans.proofs;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigInteger;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.tools.ExponentTools;

public class ZeroProofAnswerTest {

    private static final BigInteger _q = new BigInteger("11");

    private static Exponent[] _a;

    private static Exponent[] _b;

    private static Exponent _r;

    private static Exponent _s;

    private static Exponent _t;

    private static ZeroProofAnswer _target;

    @BeforeClass
    public static void setUp() throws IOException {

        _a = new Exponent[1];
        _a[0] = ExponentTools.getRandomExponent(_q);
        _b = new Exponent[1];
        _b[0] = ExponentTools.getRandomExponent(_q);
        _r = ExponentTools.getRandomExponent(_q);
        _s = ExponentTools.getRandomExponent(_q);
        _t = ExponentTools.getRandomExponent(_q);

        _target = new ZeroProofAnswer(_a, _b, _r, _s, _t);
    }

    @Test
    public void retrieve_a() {

        String errorMsg = "returned 'a' was not the expected value";
        assertEquals(errorMsg, _a[0], _target.getExponentsA()[0]);
    }

    @Test
    public void retrieve_b() {

        String errorMsg = "returned 'b' was not the expected value";
        assertEquals(errorMsg, _b[0], _target.getExponentsB()[0]);

    }

    @Test
    public void retrieve_r() {

        String errorMsg = "returned 'r' was not the expected value";
        assertEquals(errorMsg, _r, _target.getExponentR());

    }

    @Test
    public void retrieve_s() {

        String errorMsg = "returned 's' was not the expected value";
        assertEquals(errorMsg, _s, _target.getExponentS());

    }

    @Test
    public void retrieve_t() {

        String errorMsg = "returned 't' was not the expected value";
        assertEquals(errorMsg, _t, _target.getExponentT());
    }
}
