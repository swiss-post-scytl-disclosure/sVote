/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.configuration;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MatrixDimensionsTest {

    private static MatrixDimensions _target;

    private static int _m;

    private static int _n;

    @BeforeClass
    public static void setUp() throws IOException {

        _m = 64;

        _n = 8;

        _target = new MatrixDimensions(_m, _n);
    }

    @Test
    public void return_expected_values() {

        String errorMsg = "The left value that was returned was not the expected value";
        assertEquals(errorMsg, new Integer("64"), _target.getLeft());

        errorMsg = "The right value that was returned was not the expected value";
        assertEquals(errorMsg, new Integer("8"), _target.getRight());
    }
}
