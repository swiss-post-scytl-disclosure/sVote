/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.concurrent.operations;

import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * An interface to encapsulate different implementations of aggregation
 * operations executed after the split (fork) and execution of a
 * {@code RecursiveTask} list. This is an implementation example:
 * {@code GroupElementMultiplyOperation}
 */
public interface AggregateOperation<T, V extends RecursiveTask<?>> {

    /*
     * Execute the operation that aggregates all the results of all the executed
     * tasks
     */
    T execute(List<V> tasks);

}
