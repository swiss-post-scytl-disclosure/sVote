/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io.ciphertext;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;

public class CiphertextModule extends SimpleModule {

    private static final long serialVersionUID = 1L;

    private static final String NAME = "CustomCiphertextModule";

    private static final VersionUtil VERSION_UTIL = new VersionUtil() {
    };

    public CiphertextModule() {
        super(NAME, VERSION_UTIL.version());
        addSerializer(Ciphertext.class, new CiphertextSerializer());
        addDeserializer(Ciphertext.class, new CiphertextDeserializer());
    }
}
