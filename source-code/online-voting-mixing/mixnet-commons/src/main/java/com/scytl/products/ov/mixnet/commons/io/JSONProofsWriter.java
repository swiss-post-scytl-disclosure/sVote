/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scytl.products.ov.mixnet.commons.beans.proofs.ShuffleProof;
import com.scytl.products.ov.mixnet.commons.exceptions.MixingException;
import com.scytl.products.ov.mixnet.commons.io.ciphertext.CustomObjectMapper;

import java.io.IOException;
import java.io.OutputStream;

public class JSONProofsWriter implements ProofsWriter {

    @Override
    public void write(final OutputStream output, final ShuffleProof shuffleProof) throws IOException {

        validatePath(output);

        final ObjectMapper mapper = new CustomObjectMapper();
        try (OutputStream outputStream = output) {
	        mapper.setSerializationInclusion(Include.NON_NULL).writerWithDefaultPrettyPrinter()
	            .writeValue(output, shuffleProof);
        }
    }

	private static void validatePath(final OutputStream path) {

		if (null == path) {
			throw new MixingException("Could not write to output");
		}
	}
}
