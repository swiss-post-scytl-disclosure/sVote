/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.configuration.locations;

public class DefaultLocationNames {

	public static final String ENCRYPTED_BALLOTS_OUTPUT_FILE_NAME = "encryptedBallots";

    public static final String REENCRYPTED_BALLOTS_OUTPUT_FILE_NAME = "reencryptedBallots";

    public static final String PROOFS_OUTPUT_FILE_NAME = "proofs";

    public static final String PUBLIC_KEY_OUTPUT_FILE_NAME = "publicKey";

    public static final String COMMITMENT_PARAMETERS_OUTPUT_FILE_NAME = "commitmentParameters";

    public static final String ENCRYPTION_PARAMETERS_OUTPUT_FILE_NAME = "encryptionParameters";

    public static final String VERIFIER_OUTPUT_FILE_NAME = "verifierResult";

    public static final String VOTES_WITH_PROOF_FILE_NAME = "votesWithProof";

    public static final String DECRYPTED_VOTES_OUTPUT_FILE_NAME = "votes";

    public static final String AUDITABLE_VOTES_FILE_NAME = "auditableVotes.csv";
    
    /**
	 * Non-public constructor
	 */
	private DefaultLocationNames() {
	}
}
