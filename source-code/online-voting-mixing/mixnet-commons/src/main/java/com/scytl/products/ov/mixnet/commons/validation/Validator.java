/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.validation;

/**
 * Performs validations on some input.
 * 
 * @param <T>
 *            the type on input on which this Validator operates.
 */
public interface Validator<T> {

    /**
     * Validate the received input.
     * 
     * @param input
     *            the input to be validated.
     * @return a ValidationResult, which is the result of validating
     *         {@code input}.
     */
    ValidationResult validate(T input);
}
