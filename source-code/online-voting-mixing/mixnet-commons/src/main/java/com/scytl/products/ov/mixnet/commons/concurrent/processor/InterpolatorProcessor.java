/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.concurrent.processor;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.concurrent.ParallelProcessAction;
import com.scytl.products.ov.mixnet.commons.tools.CiphertextTools;
import com.scytl.products.ov.mixnet.commons.tools.LoopTools.Range;

public class InterpolatorProcessor implements ParallelProcessAction {

    private final Ciphertext[] cE;

    private final Ciphertext[] c;

    private final Exponent[][] e;

    private final CiphertextTools t;

    /**
     *
     */
    public InterpolatorProcessor(final Ciphertext[] cE, final Ciphertext[] c, final Exponent[][] e,
            final CiphertextTools t) {
        this.cE = cE;
        this.c = c;
        this.e = e;
        this.t = t;

    }

    /**
     * @see com.scytl.products.ov.mixnet.commons.concurrent.ParallelProcessAction#run(com.scytl.products.ov.mixnet.commons
     *      .tools.LoopTools.Range)
     */
    @Override
    public void run(final Range range) {
        cE[range.getStart()] = t.compVecCiphVecExp(c, e[range.getStart()]);
    }
}
