/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.configuration;

public class ApplicationConfig {

    private int concurrencyLevel;

    private int numberOfParallelBatches;

    private int numOptions;

    private int minimumBatchSize;

    private MixerConfig mixer;

    private VerifierConfig verifier;
    
    public int getConcurrencyLevel() {
        return concurrencyLevel;
    }

    public void setConcurrencyLevel(final int concurrencyLevel) {
        this.concurrencyLevel = concurrencyLevel;
    }

    public int getNumOptions() {
        return numOptions;
    }

    public void setNumOptions(final int numOptions) {
        this.numOptions = numOptions;
    }

    public MixerConfig getMixer() {
        return mixer;
    }

    public void setMixer(final MixerConfig mixer) {
        this.mixer = mixer;
    }

    public VerifierConfig getVerifier() {
        return verifier;
    }

    public void setVerifier(final VerifierConfig verifier) {
        this.verifier = verifier;
    }

    public boolean hasMixerConfig() {
        return this.mixer != null;
    }

    public boolean hasVerifierConfig() {
        return this.verifier != null;
    }

    public int getMinimumBatchSize() {
        return minimumBatchSize;
    }

    public void setMinimumBatchSize(final int minimumBatchSize) {
        this.minimumBatchSize = minimumBatchSize;
    }

    public int getNumberOfParallelBatches() {
        return numberOfParallelBatches;
    }

    public void setNumberOfParallelBatches(final int numberOfParallelBatches) {
        this.numberOfParallelBatches = numberOfParallelBatches;
    }
}
