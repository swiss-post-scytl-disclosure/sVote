/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scytl.products.ov.mixnet.commons.beans.proofs.ShuffleProof;
import com.scytl.products.ov.mixnet.commons.io.ciphertext.CustomObjectMapper;

import java.io.IOException;
import java.io.InputStream;

public class JSONProofsReader implements ProofsReader {

    @Override
    public ShuffleProof read(final InputStream is) throws IOException {

        final ObjectMapper mapper = new CustomObjectMapper();
        
        ShuffleProof shuffleProof = null;
        try (InputStream proofInput = is){
        	shuffleProof = mapper.readValue(is, ShuffleProof.class);
        }
        return shuffleProof;
    }
}
