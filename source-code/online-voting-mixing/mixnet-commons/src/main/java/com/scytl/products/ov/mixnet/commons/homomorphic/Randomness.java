/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.homomorphic;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public interface Randomness {

    Randomness add(Randomness r);

    Randomness multiply(Exponent e);

    boolean isRandomness();

    Exponent getExponent();
}
