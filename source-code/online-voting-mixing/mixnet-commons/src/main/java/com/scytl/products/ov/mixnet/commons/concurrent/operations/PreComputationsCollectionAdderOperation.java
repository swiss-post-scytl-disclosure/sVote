/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.concurrent.operations;

import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.concurrent.LoopParallelizerTask;

public class PreComputationsCollectionAdderOperation
        implements AggregateOperation<List<Ciphertext>, LoopParallelizerTask<List<Ciphertext>>> {

    @Override
    public List<Ciphertext> execute(final List<LoopParallelizerTask<List<Ciphertext>>> tasks) {

        List<Ciphertext> preComputations = tasks.get(0).join();
        for (int i = 1; i < tasks.size(); i++) {
            preComputations.addAll(tasks.get(i).join());
        }

        return preComputations;
    }
}
