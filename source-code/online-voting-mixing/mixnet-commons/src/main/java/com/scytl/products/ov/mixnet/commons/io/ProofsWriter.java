/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import java.io.IOException;
import java.io.OutputStream;

import com.scytl.products.ov.mixnet.commons.beans.proofs.ShuffleProof;

public interface ProofsWriter {

    void write(OutputStream outputStream, ShuffleProof shuffleProof) throws IOException;
}
