/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.configuration;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class VerifierConfig {

    private boolean verify;
    
    private List<InputStream> encryptedBallotsInputOfBatch;
    
    private List<InputStream> reEncryptedBallotsInputOfBatch;
    
    private List<InputStream> publicKeyInputOfBatch;
    
    private List<InputStream> commitmentParametersInputOfBatch;

    private List<InputStream> proofsInputOfBatch;
    
    private List<InputStream> encryptionParametersInputOfBatch;
    
    private List<InputStream> votesWithProofInputOfBatch;
    
    private List<InputStream> decryptedVotesInputOfBatch;

    private OutputStream output;

	public boolean isVerify() {
		return verify;
	}

	public void setVerify(boolean verify) {
		this.verify = verify;
	}

	public List<InputStream> getEncryptedBallotsInputOfBatch() {
		return encryptedBallotsInputOfBatch;
	}

	public void setEncryptedBallotsInputOfBatch(List<InputStream> encryptedBallotsInputOfBatch) {
		this.encryptedBallotsInputOfBatch = encryptedBallotsInputOfBatch;
	}

	public List<InputStream> getReEncryptedBallotsInputOfBatch() {
		return reEncryptedBallotsInputOfBatch;
	}

	public void setReEncryptedBallotsInputOfBatch(List<InputStream> reEncryptedBallotsInputOfBatch) {
		this.reEncryptedBallotsInputOfBatch = reEncryptedBallotsInputOfBatch;
	}

	public List<InputStream> getPublicKeyInputOfBatch() {
		return publicKeyInputOfBatch;
	}

	public void setPublicKeyInputOfBatch(List<InputStream> publicKeyInputOfBatch) {
		this.publicKeyInputOfBatch = publicKeyInputOfBatch;
	}

	public List<InputStream> getCommitmentParametersInputOfBatch() {
		return commitmentParametersInputOfBatch;
	}

	public void setCommitmentParametersInputOfBatch(List<InputStream> commitmentParametersInputOfBatch) {
		this.commitmentParametersInputOfBatch = commitmentParametersInputOfBatch;
	}

	public List<InputStream> getProofsInputOfBatch() {
		return proofsInputOfBatch;
	}

	public void setProofsInputOfBatch(List<InputStream> proofsInputOfBatch) {
		this.proofsInputOfBatch = proofsInputOfBatch;
	}

	public List<InputStream> getEncryptionParametersInputOfBatch() {
		return encryptionParametersInputOfBatch;
	}

	public void setEncryptionParametersInputOfBatch(List<InputStream> encryptionParametersInputOfBatch) {
		this.encryptionParametersInputOfBatch = encryptionParametersInputOfBatch;
	}

	public List<InputStream> getVotesWithProofInputOfBatch() {
		return votesWithProofInputOfBatch;
	}

	public void setVotesWithProofInputOfBatch(List<InputStream> votesWithProofInputOfBatch) {
		this.votesWithProofInputOfBatch = votesWithProofInputOfBatch;
	}

	public List<InputStream> getDecryptedVotesInputOfBatch() {
		return decryptedVotesInputOfBatch;
	}

	public void setDecryptedVotesInputOfBatch(List<InputStream> decryptedVotesInputOfBatch) {
		this.decryptedVotesInputOfBatch = decryptedVotesInputOfBatch;
	}

	public OutputStream getOutput() {
		return output;
	}

	public void setOutput(OutputStream output) {
		this.output = output;
	}

    
}
