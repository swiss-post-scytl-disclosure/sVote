/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.homomorphic.impl;

import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;

public class GjosteenElGamalRandomness implements Randomness {

    private final Exponent r;

    public GjosteenElGamalRandomness(final long value, final BigInteger order) throws GeneralCryptoLibException {
        this(new Exponent(order, BigInteger.valueOf(value)));
    }

    @JsonCreator
    public GjosteenElGamalRandomness(@JsonProperty("randomnessValue") final Exponent r) {
        this.r = r;
    }

    public Exponent getRandomnessValue() {
        return r;
    }

    @Override
    public Randomness add(final Randomness r) {

        try {

            return new GjosteenElGamalRandomness(this.r.add(((GjosteenElGamalRandomness) r).getRandomnessValue()));

        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }

    @Override
    public Randomness multiply(final Exponent exponent) {

        try {

            return new GjosteenElGamalRandomness(r.multiply(exponent));

        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }

    @Override
    @JsonIgnore
    public boolean isRandomness() {
        return true;
    }

    @Override
    @JsonIgnore
    public Exponent getExponent() {
        return r;
    }

    @Override
    public String toString() {
        return r.toString();
    }
}
