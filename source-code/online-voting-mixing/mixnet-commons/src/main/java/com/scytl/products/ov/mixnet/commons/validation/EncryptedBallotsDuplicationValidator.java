/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.validation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;

/**
 * Validates that an EncryptedBallots does not contain any duplicated encrypted ballots.
 */
public final class EncryptedBallotsDuplicationValidator implements EncryptedBallotsValidator {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    @Override
    public ValidationResult validate(final ElGamalEncryptedBallots encryptedBallots) {
        List<Ciphertext> listBallots = encryptedBallots.getBallots();

        Set<Ciphertext> setUniqueBallots = new HashSet<>(listBallots);

        ValidationResult validationResult;
        if (setUniqueBallots.size() < listBallots.size()) {

            int numVotes = listBallots.size();
            int numUnique = setUniqueBallots.size();

            LOG.warn("Duplicated votes were detected. Total votes: " + numVotes + ", total unique votes: " + numUnique);

            validationResult = ValidationResult.positiveValidation();
        } else {
            validationResult = ValidationResult.positiveValidation();
        }

        return validationResult;
    }
}
