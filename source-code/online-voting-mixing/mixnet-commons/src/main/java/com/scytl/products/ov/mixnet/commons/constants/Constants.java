/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.constants;

import java.math.BigInteger;

public class Constants {

	public static final String P = "p";

    public static final String ORDER = "q";

    public static final String GENERATOR = "g";

    public static final String CSV_FILE_EXTENSION = ".csv";

    public static final String JSON_FILE_EXTENSION = ".json";

    public static final String ELEMENT_DELIMITER = ";";

    public static final BigInteger MULTIPLICATIVE_IDENTITY = BigInteger.ONE;

    public static final String PREFIX_ENCRYPTED_BALLOTS = "encryptedBallots_n";

    public static final String CONFIG_FILE = "config.properties";

    public static final String INPUT_PATH = "input/";

    // the following are just needed for the logging system

    public static final String LOGGING_TENANT = "1";

    public static final String LOGGING_LOCALHOST = "127.0.0.1";		//NOSONAR "Make this IP "127.0.0.1" address configurable."

    public static final String LOGGING_APPLICATION = "OV";

    public static final String LOGGING_COMPONENT = "MIV";

    public static final String SECURE_LOGGER_NAME = "SecureLogger";
    
    /**
	 * Non-public constructor
	 */
	private Constants() {
	}
	
}
