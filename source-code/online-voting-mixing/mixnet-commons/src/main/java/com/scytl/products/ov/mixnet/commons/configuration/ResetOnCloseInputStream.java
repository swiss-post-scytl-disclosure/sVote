/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.configuration;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

class ResetOnCloseInputStream extends InputStream {

	private final InputStream decorated;

	public ResetOnCloseInputStream(InputStream anInputStream) {
		if (!anInputStream.markSupported()) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				IOUtils.copy(anInputStream, baos);
				anInputStream.close();
				decorated = new ByteArrayInputStream(baos.toByteArray());
			} catch (IOException e) {
	            throw new IllegalArgumentException("input stream cannot be consumed", e);
			}
		} else {
			anInputStream.mark(Integer.MAX_VALUE);
			decorated = anInputStream;
		}

	}

	@Override
	public void close() throws IOException {
		decorated.reset();
	}

	@Override
	public int read() throws IOException {
		return decorated.read();
	}
}
