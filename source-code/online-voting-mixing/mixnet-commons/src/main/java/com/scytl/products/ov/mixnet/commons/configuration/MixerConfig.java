/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.configuration;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class MixerConfig {

    private boolean mix;

    private Integer warmUpSize;   
    
    private InputStream publicKeyInput;

    private InputStream encryptedBallotsInput;

    private InputStream encryptionParametersInput;

    private InputStream ballotDecryptionKeyInput;

    private List<OutputStream> encryptedBallotsOutputOfBatch;
    
    private List<OutputStream> reEncryptedBallotsOutputOfBatch;
    
    private List<OutputStream> publicKeyOutputOfBatch;
    
    private List<OutputStream> commitmentParametersOutputOfBatch;

    private List<OutputStream> proofsOutputOfBatch;
    
    private List<OutputStream> encryptionParametersOutputOfBatch;
    
    private List<OutputStream> votesWithProofOutputOfBatch;
    
    private List<OutputStream> decryptedVotesOutputOfBatch;
    
    public boolean isMix() {
        return mix;
    }

    public void setMix(final boolean mix) {
        this.mix = mix;
    }

    public Integer getWarmUpSize() {
        return warmUpSize;
    }

    public void setWarmUpSize(Integer warmUpSize) {
        this.warmUpSize = warmUpSize;
    }

	public InputStream getPublicKeyInput() {
		return publicKeyInput;
	}

	public void setPublicKeyInput(InputStream publicKeyInput) {
		this.publicKeyInput = new ResetOnCloseInputStream(publicKeyInput);
	}

	public InputStream getEncryptedBallotsInput() {
		return encryptedBallotsInput;
	}

	public void setEncryptedBallotsInput(InputStream encryptedBallotsInput) {
		this.encryptedBallotsInput = encryptedBallotsInput;
	}

	public InputStream getEncryptionParametersInput() {
		return encryptionParametersInput;
	}

	public void setEncryptionParametersInput(InputStream encryptionParametersInput) {
		this.encryptionParametersInput = new ResetOnCloseInputStream(encryptionParametersInput);
	}

	public InputStream getBallotDecryptionKeyInput() {
		return ballotDecryptionKeyInput;
	}

	public void setBallotDecryptionKeyInput(InputStream ballotDecryptionKeyInput) {
		this.ballotDecryptionKeyInput = new ResetOnCloseInputStream(ballotDecryptionKeyInput);
	}

	public List<OutputStream> getEncryptedBallotsOutputOfBatch() {
		return encryptedBallotsOutputOfBatch;
	}

	public void setEncryptedBallotsOutputOfBatch(List<OutputStream> encryptedBallotsOutputOfBatch) {
		this.encryptedBallotsOutputOfBatch = encryptedBallotsOutputOfBatch;
	}

	public List<OutputStream> getReEncryptedBallotsOutputOfBatch() {
		return reEncryptedBallotsOutputOfBatch;
	}

	public void setReEncryptedBallotsOutputOfBatch(List<OutputStream> reEncryptedBallotsOutputOfBatch) {
		this.reEncryptedBallotsOutputOfBatch = reEncryptedBallotsOutputOfBatch;
	}

	public List<OutputStream> getPublicKeyOutputOfBatch() {
		return publicKeyOutputOfBatch;
	}

	public void setPublicKeyOutputOfBatch(List<OutputStream> publicKeyOutputOfBatch) {
		this.publicKeyOutputOfBatch = publicKeyOutputOfBatch;
	}

	public List<OutputStream> getCommitmentParametersOutputOfBatch() {
		return commitmentParametersOutputOfBatch;
	}

	public void setCommitmentParametersOutputOfBatch(List<OutputStream> commitmentParametersOutputOfBatch) {
		this.commitmentParametersOutputOfBatch = commitmentParametersOutputOfBatch;
	}

	public List<OutputStream> getProofsOutputOfBatch() {
		return proofsOutputOfBatch;
	}

	public void setProofsOutputOfBatch(List<OutputStream> proofsOutputOfBatch) {
		this.proofsOutputOfBatch = proofsOutputOfBatch;
	}

	public List<OutputStream> getEncryptionParametersOutputOfBatch() {
		return encryptionParametersOutputOfBatch;
	}

	public void setEncryptionParametersOutputOfBatch(List<OutputStream> encryptionParametersOutputOfBatch) {
		this.encryptionParametersOutputOfBatch = encryptionParametersOutputOfBatch;
	}

	public List<OutputStream> getVotesWithProofOutputOfBatch() {
		return votesWithProofOutputOfBatch;
	}

	public void setVotesWithProofOutputOfBatch(List<OutputStream> votesWithProofOutputOfBatch) {
		this.votesWithProofOutputOfBatch = votesWithProofOutputOfBatch;
	}

	public List<OutputStream> getDecryptedVotesOutputOfBatch() {
		return decryptedVotesOutputOfBatch;
	}

	public void setDecryptedVotesOutputOfBatch(List<OutputStream> decryptedVotesOutputOfBatch) {
		this.decryptedVotesOutputOfBatch = decryptedVotesOutputOfBatch;
	}
    
}
