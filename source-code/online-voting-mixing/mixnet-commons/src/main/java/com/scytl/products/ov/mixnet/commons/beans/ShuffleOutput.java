/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.beans;

import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;

public class ShuffleOutput<T> {

    private final Permutation permutation;

    private final Randomness[] exponents;

    private final T encryptedBallots;

    public ShuffleOutput(final Permutation permutation, final Randomness[] exponents, final T encryptedBallots) {

        this.permutation = permutation;

        this.exponents = exponents;

        this.encryptedBallots = encryptedBallots;
    }

    public Permutation getPermutation() {
        return permutation;
    }

    public Randomness[] getExponents() {
        return exponents;
    }

    public T getReEncryptedBallots() {
        return encryptedBallots;
    }

}
