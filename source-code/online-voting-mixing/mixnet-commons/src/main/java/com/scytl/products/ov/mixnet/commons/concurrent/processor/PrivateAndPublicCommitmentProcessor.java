/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.concurrent.processor;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.concurrent.ParallelProcessAction;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.CommitmentParams;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PublicCommitment;
import com.scytl.products.ov.mixnet.commons.tools.LoopTools.Range;
import com.scytl.products.ov.mixnet.commons.tools.MultiExponentiation;

/**
 * Base case to be parallelized using {@code LoopParallelizerAction}
 */
public class PrivateAndPublicCommitmentProcessor implements ParallelProcessAction {

    private final PrivateCommitment[] privateCommitments;

    private final PublicCommitment[] publicCommitments;

    private final Exponent[][] exponents;

    private final Exponent[] r;

    private final CommitmentParams comParams;

    private final MultiExponentiation multiExponentiation;

    public PrivateAndPublicCommitmentProcessor(final PrivateCommitment[] privateCommitmens,
            final PublicCommitment[] publicCommitments, final Exponent[][] exponents, final Exponent[] r,
            final CommitmentParams comParams, final MultiExponentiation multiExponentiation) {

        this.privateCommitments = privateCommitmens;
        this.publicCommitments = publicCommitments;

        this.exponents = exponents;
        this.r = r;
        this.comParams = comParams;
        this.multiExponentiation = multiExponentiation;
    }

    /**
     * @see com.scytl.products.ov.mixnet.commons.concurrent.ParallelProcessAction#run(Range)
     */
    @Override
    public void run(final Range range) {
        for (int i = range.getStart(), rangeEnd = range.getEnd(); i < rangeEnd; i++) {

            privateCommitments[i] =
                new PrivateCommitment(exponents[i], r[i], comParams, multiExponentiation);

            publicCommitments[i] = privateCommitments[i].makePublicCommitment();
        }
    }

}
