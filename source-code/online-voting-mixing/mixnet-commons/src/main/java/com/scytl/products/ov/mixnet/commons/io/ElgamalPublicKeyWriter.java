/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;

/**
 * @author afries
 */
public class ElgamalPublicKeyWriter {

	 /**
     * Non-public constructor
     */
	private ElgamalPublicKeyWriter() {    
    }
	
	public static void serializeToStream(ElGamalPublicKey key, final OutputStream output) throws IOException, GeneralCryptoLibException {

        List<String> linesToBeWritten = new ArrayList<>();

        linesToBeWritten.add(key.toJson());

        try (OutputStream outputStream = output) {
            IOUtils.writeLines(linesToBeWritten, null, outputStream, StandardCharsets.UTF_8);
        }
    }
}
