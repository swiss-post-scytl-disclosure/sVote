/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.concurrent.processor;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.products.ov.mixnet.commons.concurrent.ParallelProcessTask;
import com.scytl.products.ov.mixnet.commons.homomorphic.Cryptosystem;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;
import com.scytl.products.ov.mixnet.commons.homomorphic.impl.GjosteenElGamalPlaintext;
import com.scytl.products.ov.mixnet.commons.homomorphic.impl.GjosteenElGamalRandomness;
import com.scytl.products.ov.mixnet.commons.tools.LoopTools;

public class PreComputationCalculatorProcessor implements ParallelProcessTask<List<Ciphertext>> {

    private final ZpGroupElement[] arrayOfIdentityElement;

    private final Randomness[] requiredRandomExponents;

    private final Cryptosystem cryptoSystem;

    public PreComputationCalculatorProcessor(final ZpGroupElement[] arrayOfIdentityElement,
            final Randomness[] requiredRandomExponents, final Cryptosystem cryptoSystem) {

        this.arrayOfIdentityElement = arrayOfIdentityElement;
        this.requiredRandomExponents = requiredRandomExponents;
        this.cryptoSystem = cryptoSystem;
    }

    @Override
    public List<Ciphertext> run(final LoopTools.Range range) {

        List<Ciphertext> preComputations = new ArrayList<>();

        for (int i = range.getStart(), rangeEnd = range.getEnd(); i < rangeEnd; i++) {

            preComputations.add(cryptoSystem.encrypt(new GjosteenElGamalPlaintext(arrayOfIdentityElement),
                new GjosteenElGamalRandomness(requiredRandomExponents[i].getExponent())));
        }

        return preComputations;
    }
}
