/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.infrastructure.log;

/**
 * Constants to log decryption.
 */
public class DecryptionLogConstants {

    /**
     * A name of the votes with errors file - additional information.
     */
    public static final String INFO_AUDITABLE_VOTES_FILE_ID = "#dec_votes_e_file_id";

    /**
     * A key_id (fingerprint) - additional information.
     */
    public static final String INFO_PUBLIC_KEY_ID = "#pubkey_id";

    /**
     * An error description - additional information.
     */
    public static final String INFO_ERR_DESC = "#err_desc";

    /**
     * A mixed ballot box id - additional information.
     */
    public static final String INFO_MIXED_BALLOT_BOX_ID = "#mbb_id";

    /**
     * A mixed ballot box hash - additional information.
     */
    public static final String INFO_MIXED_BALLOT_BOX_HASH = "#mbb_hash";

    /**
     * A name of the decryption proofs file - additional information.
     */
    public static final String INFO_DEC_PROOFS_FILE_ID = "#dec_proofs_file_id";

    /**
     * A hash of the decryption proofs file - additional information.
     */
    public static final String INFO_DEC_PROOFS_FILE_HASH = "#dec_proofs_file_h";

    /**
     * A name of the decrypted votes file - additional information.
     */
    public static final String INFO_DEC_VOTES_FILE_ID = "#dec_votes_file_id";

    /**
     * A hash of the decrypted votes file - additional information.
     */
    public static final String INFO_DEC_VOTES_FILE_HASH = "#dec_votes_file_h";

    /**
     * Name of the file with voting options - additional information.
     */
    public static final String INFO_VOTE_OPTIONS_FILE_ID = "#voteopts_file_id";

    /**
     * A hash of the file with voting options - additional information.
     */
    public static final String INFO_VOTE_OPTIONS_FILE_HASH = "#voteopts_votes_file_h";

    /**
     * A hash of the votes with errors file - additional information.
     */
    public static final String INFO_AUDITABLE_VOTES_FILE_HASH = "#dec_votes_e_file_h";

    /**
     * A hash of the vote - additional information.
     */
    public static final String INFO_HASH_VOTE = "#hashVote";

    /**
     * A ballotID - additional information.
     */
    public static final String INFO_BALLOT_ID = "#b_id";

    /**
     * A task id - additional information.
     */
    public static final String INFO_TASK_ID = "#task_id";

    /**
     * An offset - additional information.
     */
    public static final String INFO_OFFSET = "#offset";

    /**
     * A number of ballots - additional information.
     */
    public static final String INFO_NUM_BALLOTS = "#num_ballots";

    /**
     * - additional information.
     */
    public static final String INFO_ = "#";

    /**
     * Non-public constructor
     */
	private DecryptionLogConstants() {
    }

}