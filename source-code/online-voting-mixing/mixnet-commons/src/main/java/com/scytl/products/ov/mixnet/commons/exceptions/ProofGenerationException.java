/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.exceptions;

public class ProofGenerationException extends MixingException {

    private static final long serialVersionUID = 1278914122127633253L;

    public ProofGenerationException(final String message) {
        super(message);
    }

    public ProofGenerationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ProofGenerationException(final Throwable cause) {
        super(cause);
    }
}
