/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.beans.proofs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Encapsulates the output of a {@link SecondAnswerGenerator}.
 */
public final class ShuffleProofSecondAnswer {

    private final MultiExponentiationBasicProofInitialMessage iniMEBasic;

    private final MultiExponentiationBasicProofAnswer ansMEBasic;

    private final MultiExponentiationReductionInitialMessage iniMEReduct;

    private final MultiExponentiationReductionAnswer ansMEReduct;

    private final ProductProofMessage msgPA;

    @JsonCreator
    public ShuffleProofSecondAnswer(
            @JsonProperty("iniMEReduct") final MultiExponentiationReductionInitialMessage iniMEReduct,
            @JsonProperty("ansMEReduct") final MultiExponentiationReductionAnswer ansMEReduct,
            @JsonProperty("msgPA") final ProductProofMessage msgPA,
            @JsonProperty("iniMEBasic") final MultiExponentiationBasicProofInitialMessage iniMEBasic,
            @JsonProperty("ansMEBasic") final MultiExponentiationBasicProofAnswer ansMEBasic) {
    	this.iniMEBasic = iniMEBasic;
    	this.ansMEBasic = ansMEBasic;
    	this.iniMEReduct = iniMEReduct;
    	this.ansMEReduct = ansMEReduct;
    	this.msgPA = msgPA;
    }

    public ShuffleProofSecondAnswer(final MultiExponentiationReductionInitialMessage iniMEReduct,
            final MultiExponentiationReductionAnswer ansMEReduct, final ProductProofMessage msgPA) {
    	this.iniMEBasic = null;
    	this.ansMEBasic = null;
    	this.iniMEReduct = iniMEReduct;
    	this.ansMEReduct = ansMEReduct;
    	this.msgPA = msgPA;
    }

    public ShuffleProofSecondAnswer(final MultiExponentiationBasicProofInitialMessage iniMEBasic,
            final MultiExponentiationBasicProofAnswer ansMEBasic, final ProductProofMessage msgPA) {
    	this.iniMEBasic = iniMEBasic;
    	this.ansMEBasic = ansMEBasic;
    	this.iniMEReduct = null;
    	this.ansMEReduct = null;
    	this.msgPA = msgPA;
    }

    /**
     * @return Returns the iniMEBasic.
     */
    public MultiExponentiationBasicProofInitialMessage getIniMEBasic() {
        return iniMEBasic;
    }

    /**
     * @return Returns the ansMEBasic.
     */
    public MultiExponentiationBasicProofAnswer getAnsMEBasic() {
        return ansMEBasic;
    }

    /**
     * @return Returns the iniMEReduct.
     */
    public MultiExponentiationReductionInitialMessage getIniMEReduct() {
        return iniMEReduct;
    }

    /**
     * @return Returns the ansMEReduct.
     */
    public MultiExponentiationReductionAnswer getAnsMEReduct() {
        return ansMEReduct;
    }

    /**
     * @return Returns the msgPA.
     */
    public ProductProofMessage getMsgPA() {
        return msgPA;
    }

    @Override
    public String toString() {
        final StringBuilder strbldr = new StringBuilder();
        if ((iniMEBasic != null) && (ansMEBasic != null)) {
            strbldr.append(iniMEBasic.toString());
            strbldr.append(ansMEBasic.toString());
        } else {
            strbldr.append(iniMEReduct.toString());
            strbldr.append(ansMEReduct.toString());
        }
        strbldr.append(msgPA.toString());
        return strbldr.toString();
    }
}
