/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.security.MessageDigest;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

public class HashCalculatingWriter extends OutputStreamWriter {

	private MessageDigest rawMessageDigest;

	public HashCalculatingWriter(OutputStream os, MessageDigest rawMessageDigest) throws GeneralCryptoLibException {
		super(os);
		this.rawMessageDigest = rawMessageDigest;
	}

	@Override
	public Writer append(CharSequence csq) throws IOException {
		rawMessageDigest.update(csq.toString().getBytes(getEncoding()));
		return super.append(csq);
	}

	public byte[] getHash() {
		return rawMessageDigest.digest();
	}
}
