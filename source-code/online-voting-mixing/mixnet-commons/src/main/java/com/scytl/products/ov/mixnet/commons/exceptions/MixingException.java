/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.exceptions;

public class MixingException extends RuntimeException {

    private static final long serialVersionUID = 1346589586967314487L;

    public MixingException(final String message) {
        super(message);
    }

    public MixingException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public MixingException(final Throwable cause) {
        super(cause);
    }
}
