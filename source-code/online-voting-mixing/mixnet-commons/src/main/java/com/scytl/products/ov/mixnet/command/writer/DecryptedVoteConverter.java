/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.command.writer;


import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.scytl.products.ov.mixnet.commons.beans.VoteWithProof;
import java.util.stream.Collectors;

/**
 * A utility class to prepare votes with proof for storage in the decrypted votes CSV file.
 */
public class DecryptedVoteConverter implements CSVEntryConverter<VoteWithProof> {

  @Override
  public String[] convertEntry(final VoteWithProof vote) {
    // Convert the decrypted vote values to an array of strings.
    return vote.getDecrypted().stream().map(bigInt -> bigInt.toString()).collect(
        Collectors.toList()).toArray(new String[vote.getDecrypted().size()]);
  }
}
