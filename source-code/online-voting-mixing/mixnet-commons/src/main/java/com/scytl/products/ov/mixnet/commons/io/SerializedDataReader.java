/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;

public abstract class SerializedDataReader {

    protected static String readLineAndConfirmNotNull(final BufferedReader reader) throws IOException {

        final String line = reader.readLine();

        if (line == null) {
            throw new EOFException("The line that was read from file was null. File was : " + reader);
        }
        return line;
    }
}
