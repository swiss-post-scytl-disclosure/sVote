/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.concurrent;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.mixnet.commons.tools.LoopTools.Range;

/**
 * An interface to encapsulate implementations of different base case actions
 * (returning a T object). An example of implementation is
 * {@code EncryptConcurrentCalculatorProcessor}
 */
public interface ParallelProcessTask<T> {

    T run(Range range) throws GeneralCryptoLibException;

}
