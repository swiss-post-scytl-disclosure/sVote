/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.validation;

import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;

/**
 * A validator of objects which are {@code EncryptedBallots}.
 */
public interface EncryptedBallotsValidator extends
        Validator<ElGamalEncryptedBallots> {
}
