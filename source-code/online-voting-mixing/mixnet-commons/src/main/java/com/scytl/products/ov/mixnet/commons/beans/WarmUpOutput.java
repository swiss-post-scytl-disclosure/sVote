/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.beans;

import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;

public class WarmUpOutput {

    private Randomness[] randomExponents = null;

    private List<Ciphertext> preComputations = null;

    public WarmUpOutput(final Randomness[] randomExponents, final List<Ciphertext> preComputations) {
    	this.randomExponents = randomExponents;
    	this.preComputations = preComputations;
    }

    public Randomness[] getRandomExponents() {
        return randomExponents;
    }

    public List<Ciphertext> getPreComputations() {
        return preComputations;
    }
}
