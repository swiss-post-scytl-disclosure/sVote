/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.proofs.bg.multiexp.basic;

import java.math.BigInteger;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.CommitmentParams;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.products.ov.mixnet.commons.tools.ExponentTools;
import com.scytl.products.ov.mixnet.commons.tools.MultiExponentiation;

/**
 * This class aids the initial message generation by providing several methods.
 */
public final class InitialMessageHelper {

	/**
	 * Non-public constructor
	 */
	private InitialMessageHelper() {
	}

	
	/**
     * @param m
     * @param b
     * @param s
     * @return
     */
    public static PrivateCommitment[] calculatePrivateCommitments(final int m, final Exponent[] b, final Exponent[] s,
            final CommitmentParams comParams, final MultiExponentiation limMultiExpo) {
        final int cBLength = 2 * m;
        final PrivateCommitment[] cB = new PrivateCommitment[cBLength];
        for (int k = 0; k < cBLength; k++) {
            cB[k] = new PrivateCommitment(b[k], s[k], comParams, limMultiExpo);
        }
        return cB;
    }

    /**
     * @param m
     * @param order
     * @return
     */
    public static Exponent[] calculateExponentsArrayS(final int m, final BigInteger order) {

        try {
            final Exponent[] s = ExponentTools.getVectorRandomExponent(2 * m, order);
            s[m] = new Exponent(order, BigInteger.ZERO);
            return s;
        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }

    /**
     * @param m
     * @param order
     * @return
     */
    public static Exponent[] calculateExponentsMultiarrayB(final int m, final BigInteger order) {

        try {
            final Exponent[] b = new Exponent[2 * m];
            Exponent zero = new Exponent(order, BigInteger.ZERO);
            for (int i = 0; i < b.length; i++) {
                if (i == m) {
                    b[i] = zero;
                } else {
                    b[i] = ExponentTools.getRandomExponent(order);
                }
            }
            return b;

        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }
}
