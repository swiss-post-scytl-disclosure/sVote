/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.io;

import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;

import java.io.IOException;

/**
 *
 */
public interface OutputProcessor {

    void storeEncryptedBallotsToCSV(final String outputPathAsString, final ElGamalEncryptedBallots encryptedBallots, final LocationsProvider locationsProvider) throws IOException;

    void storeReEncryptedBallotsToCSV(final String outputPathAsString, final ElGamalEncryptedBallots encryptedBallots, final LocationsProvider locationsProvider) throws IOException;

    void storeProofsToJSON();

}
