/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.commons.configuration;

import com.scytl.products.ov.mixnet.commons.beans.Pair;

/**
 * Encapsulates the matrix dimensions.
 */
public final class MatrixDimensions extends Pair<Integer, Integer> {

    /**
     * @param m
     * @param n
     */
    public MatrixDimensions(final int m, final int n) {
        super(m, n);
    }

    /**
     * @return Returns the number of rows.
     */
    public int getNumberOfRows() {
        return getLeft();
    }

    /**
     * @return Returns the number of columns.
     */
    public int getNumberOfColumns() {
        return getRight();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MatrixDimensions [_m=" + getLeft() + ", _n=" + getRight()
            + "]";
    }

}
