/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.spring;

import java.util.Enumeration;

import org.apache.log4j.LogManager;

import com.scytl.products.oscore.logging.api.exceptions.LoggingException;
import com.scytl.products.ov.mixnet.commons.constants.Constants;
import com.scytl.slogger.SecureAppender;
import com.scytl.slogger.SecureLoggerException;

/**
 * Configuration of secure logger.
 */
public class MixingSecureLoggerConfig {

    private static final String CIPHER_PKCS12_PSWD = "649VRY52GXXCNJH48X5F";

    private static final String SIGNATURE_PKCS12_PSWD = "GXXCNJH48X5F649VRY52";

    private static final String CIPHER_PKCS12_CERTIFICATE = "cipherKeyStore.p12";

    private static final String SIGNATURE_PKCS12_CERTIFICATE = "signatureKeyStore.p12";

    public MixingSecureLoggerConfig() {
        try {
            configure();
        } catch (SecureLoggerException e) {
            throw new LoggingException("Failed to configure the logging system", e);
        }
    }

    private static void configure() throws SecureLoggerException {

        org.apache.log4j.Logger logger = LogManager.getLogger(Constants.SECURE_LOGGER_NAME);
        Enumeration<?> appenders = logger.getAllAppenders();

        while (appenders.hasMoreElements()) {
            final Object appender = appenders.nextElement();

            if (appender instanceof SecureAppender) {
                SecureAppender secureAppender = (SecureAppender) appender;
                secureAppender.setCipherPkcs12FileName(CIPHER_PKCS12_CERTIFICATE);
                secureAppender.setCipherPkcs12Password(CIPHER_PKCS12_PSWD);
                secureAppender.setSignaturePkcs12FileName(SIGNATURE_PKCS12_CERTIFICATE);
                secureAppender.setSignaturePkcs12Password(SIGNATURE_PKCS12_PSWD);
                secureAppender.activateOptions();
            }
        }
    }
}
