/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

import org.junit.Test;

/**
 * Test of multiple phis, based on data that was captured during an e2e test of sVote.
 */
public class BGMixnetMultiplePhisITest {

    /**
     * This test was added when a bug was detected that the mixing failed if there are more phis than encrypted ballots.
     * <P>
     * This was added here (instead of in the engine module) because it is much easier to create a test based on
     * captured data by running Application.main().
     * <P>
     * Another test that tests the same scenario (more phis than votes) was also added in the the engine module, but
     * which uses the test data that exists there.
     */
    @Test
    public void more_phis_than_votes_10phis_2votes() throws IOException {

        String[] params =
            new String[] {"src/test/resources/input/cleansed_ballotbox_10subkeys_2votes/applicationConfig.json" };
        Application.main(params);

        File outputFileOfVerifier =
            Paths.get("target/cleansed_ballotbox_10subkeys_2votes/verifierResult.json").toFile();

        String firstLineFromVerifierOutputFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(outputFileOfVerifier))) {
            firstLineFromVerifierOutputFile = br.readLine();
        } catch (IOException e) {
            System.out.println("Error while trying to read line from verifier output file");
        }

        String expectedVerifierResult = "{\"1\":true}";
        assertEquals(expectedVerifierResult, firstLineFromVerifierOutputFile);
    }
}
