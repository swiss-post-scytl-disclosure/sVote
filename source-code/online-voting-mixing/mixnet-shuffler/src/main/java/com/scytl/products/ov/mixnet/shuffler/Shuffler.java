/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.shuffler;

import java.util.List;

import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.products.ov.mixnet.commons.beans.ShuffleOutput;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;

/**
 * Defines shuffle functionality.
 */
public interface Shuffler<T> {

    ShuffleOutput<T> sequentialShuffle(final ZpSubgroup group, final ElGamalPublicKey publicKey,
            final int concurrencyLevel, T encryptedBallots, final Randomness[] completedBatchRandomExponents,
            final List<Ciphertext> batchPreComputations);

}
