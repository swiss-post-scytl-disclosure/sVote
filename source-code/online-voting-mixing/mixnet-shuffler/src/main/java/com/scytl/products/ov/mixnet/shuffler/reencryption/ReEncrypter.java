/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.shuffler.reencryption;

import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;

/**
 *
 */
public interface ReEncrypter {

    ElGamalEncryptedBallots reEncrypt(ElGamalEncryptedBallots elGamalEncryptedBallots, Randomness[] randomExponents,
            final List<Ciphertext> batchPreComputations);
}
