/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.tools;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.List;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.ov.mixing.commons.beans.ElGamalEncryptedBallot;
import com.scytl.ov.mixing.commons.beans.VoteWithProof;

/**
 * Tests of {@link VoteWithProofConverter}.
 */
public class VoteWithProofConverterTest {

    private static ConfigObjectMapper objectMapper = new ConfigObjectMapper();

    @Test
    public void testConvertEntry() throws GeneralCryptoLibException {
        VoteWithProofConverter converter = new VoteWithProofConverter(objectMapper);
        ZpSubgroup group = new ZpSubgroup(BigInteger.valueOf(2), BigInteger.valueOf(7), BigInteger.valueOf(3));
        List<ZpGroupElement> elements =
            asList(new ZpGroupElement(BigInteger.valueOf(2), group), new ZpGroupElement(BigInteger.valueOf(4), group));
        Ciphertext encrypted = ElGamalEncryptedBallot.create(elements);
        List<BigInteger> decryptedVotes = asList(BigInteger.valueOf(3), BigInteger.valueOf(5));
        String proof = "proof";
        VoteWithProof vote = new VoteWithProof(encrypted, decryptedVotes, proof);

        String[] entry = converter.convertEntry(vote);

        assertEquals("[{\"value\":2,\"p\":7,\"q\":3},{\"value\":4,\"p\":7,\"q\":3}]", entry[0]);
        assertEquals("[3,5]", entry[1]);
        assertEquals(proof, entry[2]);
    }
}
