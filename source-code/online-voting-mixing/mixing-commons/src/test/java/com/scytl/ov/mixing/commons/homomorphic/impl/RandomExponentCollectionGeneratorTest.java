/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.homomorphic.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.ov.mixing.commons.homomorphic.Randomness;

public class RandomExponentCollectionGeneratorTest {

    private static RandomnessGenerator target;

    @BeforeClass
    public static void setUp() {

        final BigInteger order = new BigInteger("23");
        target = new RandomnessGenerator(order);
    }

    @Test
    public void givenNullListWhenPermutateThenException() {

        final int numExponentsToGenerate = 10;

        final Randomness[] rho = target.generate(numExponentsToGenerate);

        final int numExponentsGenerated = rho.length;

        final String errorMsg = "The number of generated exponents does not match the expected value. Expected: "
            + numExponentsToGenerate + ", but it was: " + numExponentsGenerated;
        assertEquals(errorMsg, numExponentsToGenerate, numExponentsGenerated);
    }
}
