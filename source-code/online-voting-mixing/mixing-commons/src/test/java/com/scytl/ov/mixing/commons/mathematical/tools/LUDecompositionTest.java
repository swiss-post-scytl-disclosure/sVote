/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.mathematical.tools;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigInteger;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

public class LUDecompositionTest {

    private static LUDecomposition _luDecompositionSmall;

    @BeforeClass
    public static void setUp() throws IOException {

        try {

            final BigInteger order = new BigInteger("11");

            final Exponent zero = new Exponent(order, BigInteger.ZERO);
            final Exponent one = new Exponent(order, BigInteger.ONE);

            final Exponent[][] matrixA = {{one, zero }, {one, one } };

            _luDecompositionSmall = new LUDecomposition(matrixA, order);

        } catch (GeneralCryptoLibException ex) {
            throw new CryptoLibException(ex);
        }
    }

    @Test
    public void return_if_matrix_is_square() {

        final Exponent[][] result = _luDecompositionSmall.getInverseMatrix();

        String errorMsg = "returned array did not have expected size";
        assertEquals(errorMsg, 2, result.length);
        assertEquals(errorMsg, 2, result[0].length);
    }
}
