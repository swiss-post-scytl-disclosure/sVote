/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.beans;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalComputationsValues;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;

/**
 * Tests of {@link ElGamalEncryptedBallot}.
 */
public class ElGamalEncryptedBallotTest {
    @Test
    public void testCreate() throws GeneralCryptoLibException {
        ZpSubgroup group = new ZpSubgroup(BigInteger.valueOf(2), BigInteger.valueOf(7), BigInteger.valueOf(3));
        List<ZpGroupElement> elements =
            asList(new ZpGroupElement(BigInteger.valueOf(2), group), new ZpGroupElement(BigInteger.valueOf(4), group));
        ElGamalEncryptedBallot ballot = ElGamalEncryptedBallot.create(elements);
        assertEquals(BigInteger.valueOf(2), ballot.getGamma().getValue());
        assertEquals(1, ballot.getPhis().size());
        assertEquals(BigInteger.valueOf(4), ballot.getPhis().get(0).getValue());
        Assert.assertFalse(ballot.isPoisonPill());
        ElGamalComputationsValues values = ballot.getValues();
        Assert.assertEquals(BigInteger.valueOf(2), values.getGamma().getValue());
        Assert.assertEquals(1, values.getPhis().size());
        Assert.assertEquals(BigInteger.valueOf(4), values.getPhis().get(0).getValue());
    }

    @Test
    public void testPoisonPill() {
        ElGamalEncryptedBallot ballot = ElGamalEncryptedBallot.poisonPill();
        Assert.assertTrue(ballot.isPoisonPill());
    }
}
