/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.mathematical.impl;

import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 *
 */
public class ExponentTest {

    @Test
    public void negate() throws GeneralCryptoLibException {

        BigInteger q = new BigInteger("11");

        com.scytl.cryptolib.mathematical.groups.impl.Exponent e =
            new com.scytl.cryptolib.mathematical.groups.impl.Exponent(q, new BigInteger("2"));
        Assert.assertEquals(
            e.multiply(new com.scytl.cryptolib.mathematical.groups.impl.Exponent(q, new BigInteger("-1"))), e.negate());

    }
}
