/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.concurrent.processor;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.concurrent.ParallelProcessAction;
import com.scytl.ov.mixing.commons.tools.CiphertextTools;
import com.scytl.ov.mixing.commons.tools.LoopTools.Range;

/**
 *
 */
public class A0Processor implements ParallelProcessAction {

    private final Ciphertext[] cE;

    private final Ciphertext[][] cB;

    private final Exponent[] eX;

    private final CiphertextTools t;

    private final int m;

    public A0Processor(final Ciphertext[] cE, final Ciphertext[][] cB, final Exponent[] Ex, final CiphertextTools t,
            final int m) {

        this.cE = cE;
        this.cB = cB;
        this.eX = Ex;
        this.t = t;
        this.m = m;
    }

    /**
     * @see com.scytl.products.ov.mixnet.commons.concurrent.ParallelProcessAction#run(com.scytl.products.ov.mixnet.commons
     *      .tools.LoopTools.Range)
     */
    @Override
    public void run(final Range range) {
        final Ciphertext aux = t.compVecCiphVecExp(cB[m - range.getStart() - 1], eX);
        try {
            cE[range.getStart()] = range.getStart() == 0 ? aux : cE[range.getStart()].multiply(aux);
        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }
}
