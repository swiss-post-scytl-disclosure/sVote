/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.io;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scytl.ov.mixing.commons.beans.proofs.BayerGrothProof;
import com.scytl.ov.mixing.commons.io.ciphertext.CustomObjectMapper;

public class JSONProofsReader implements ProofsReader {

    @Override
    public BayerGrothProof read(final InputStream is) throws IOException {

        final ObjectMapper mapper = new CustomObjectMapper();

        BayerGrothProof shuffleProof = null;
        try (InputStream proofInput = is) {
            shuffleProof = mapper.readValue(is, BayerGrothProof.class);
        }
        return shuffleProof;
    }
}
