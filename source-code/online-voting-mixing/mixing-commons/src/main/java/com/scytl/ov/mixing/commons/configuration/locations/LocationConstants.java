/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.configuration.locations;

/**
 *
 */
public class LocationConstants {

	public static final String PUBLIC_KEY_JSON_TAG = "publicKey";

    public static final String BALLOT_DECRYPTION_KEY_JSON_TAG = "ballotDecryptionKey";

    public static final String ENCRYPTED_BALLOTS_JSON_TAG = "encryptedBallots";

    public static final String ENCRYPTION_PARAMETERS_JSON_TAG = "encryptionParameters";
    
    /**
	 * Non-public constructor
	 */
	private LocationConstants() {
	}

}
