/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.scytl.ov.mixing.commons.beans.VoteWithProof;

/**
 * A utility class to convert encrypted and decrypted vote and decryption proof to string.
 */
public class VoteWithProofConverter implements CSVEntryConverter<VoteWithProof> {

    private final ConfigObjectMapper objectMapper;

    public VoteWithProofConverter(ConfigObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String[] convertEntry(final VoteWithProof vote) {

        try {
            String[] values = new String[3];

            // Encode encrypted ballot
            values[0] = objectMapper.fromJavaToJSON(vote.getEncrypted().getElements());

            // Encode decrypted votes
            values[1] = objectMapper.fromJavaToJSON(vote.getDecrypted());

            // Store proof
            values[2] = vote.getProof();

            return values;
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

}
