/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.concurrent.processor;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.ov.mixing.commons.concurrent.ParallelProcessTask;
import com.scytl.ov.mixing.commons.tools.LoopTools;

public class MultiplyConcurrentCalculatorProcessor implements ParallelProcessTask<List<Ciphertext>> {

    private final List<Ciphertext> preComputations;

    private final List<Ciphertext> inputEncryptedBallots;

    public MultiplyConcurrentCalculatorProcessor(final List<Ciphertext> preComputations,
            final List<Ciphertext> inputEncryptedBallots) {

        this.preComputations = preComputations;
        this.inputEncryptedBallots = inputEncryptedBallots;
    }

    @Override
    public List<Ciphertext> run(final LoopTools.Range range) {
        List<Ciphertext> reEncryptedBallots = new ArrayList<>();

        for (int i = range.getStart(), rangeEnd = range.getEnd(); i < rangeEnd; i++) {

            final Ciphertext encryptedBallot = inputEncryptedBallots.get(i);

            final Ciphertext multiplicationResult;
            try {
                multiplicationResult = encryptedBallot.multiply(preComputations.get(i));
            } catch (GeneralCryptoLibException e) {
                throw new CryptoLibException(e);
            }

            reEncryptedBallots.add(multiplicationResult);
        }

        return reEncryptedBallots;
    }
}
