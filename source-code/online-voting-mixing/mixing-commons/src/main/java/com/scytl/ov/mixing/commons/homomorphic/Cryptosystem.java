/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.homomorphic;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

public interface Cryptosystem {

    Randomness getFreshRandomness();

    Randomness[] getVectorFreshRandomness(int length);

    Ciphertext encrypt(final Plaintext p);

    Ciphertext encrypt(final Plaintext p, final Randomness r);

    Ciphertext encryptRaisingToRandom(Exponent b, Randomness tau);

    Ciphertext getEncryptionOf1();

    Ciphertext getEncryptionOf1(Randomness r);

    Randomness get0Randomness();

    int getNumberOfMessages();
}
