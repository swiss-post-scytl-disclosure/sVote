/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.io;

import java.util.List;

import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.ov.mixing.commons.exceptions.InvalidInputException;

public final class ElGamalEncryptedBallotEntryConverter implements CSVEntryConverter<Ciphertext> {

    /**
     * @see com.googlecode.jcsv.writer.CSVEntryConverter#convertEntry(java.lang.Object)
     */
    @Override
    public String[] convertEntry(final Ciphertext encryptedBallot) {

        validateInputs(encryptedBallot);

        final List<ZpGroupElement> phis = encryptedBallot.getPhis();
        final int columnsSize = encryptedBallot.getPhis().size() + 1;

        String[] columns = new String[columnsSize];

        columns[0] = encryptedBallot.getGamma().getValue().toString();

        for (int i = 1; i < columnsSize; i++) {
            String tmp = phis.get(i - 1).getValue().toString();
            columns[i] = tmp;
        }

        return columns;
    }

    private static void validateInputs(final Ciphertext encryptedBallot) {

        if (encryptedBallot == null) {
            throw new InvalidInputException("The ballot must be initialized");
        }
    }
}
