/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.homomorphic;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

public interface RandomnessCollection {

    RandomnessCollection add(RandomnessCollection r);

    RandomnessCollection multiply(Exponent e);

}
