/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 10/08/15.
 */
package com.scytl.ov.mixing.commons.validation;

import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileValidator {
	
	private static final Logger LOG = LoggerFactory.getLogger("std");
	
	/**
	 * Non-public constructor
	 */
	private FileValidator() {
	}
	
    public static void checkFile(final Path file) {

        if (!Files.exists(file)) {
            LOG.error("File '" + file.getFileName() + "' does not exists.");
            throw new IllegalArgumentException("File '" + file.getFileName() + "' does not exist.");
        }
    }
}
