/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.beans;

public abstract class Pair<LEFT, RIGHT> {

    private final LEFT left;

    private final RIGHT right;

    public Pair(final LEFT left, final RIGHT right) {

        this.left = left;
        this.right = right;
    }

    public LEFT getLeft() {
        return left;
    }

    public RIGHT getRight() {
        return right;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Pair [_left=" + left + ", _right=" + right + "]";
    }

}
