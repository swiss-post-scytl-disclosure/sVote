/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.command.writer;

import java.util.stream.Collectors;

import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.scytl.ov.mixing.commons.beans.VoteWithProof;

/**
 * A utility class to prepare votes with proof for storage in the decrypted votes CSV file.
 */
public class DecryptedVoteConverter implements CSVEntryConverter<VoteWithProof> {

    @Override
    public String[] convertEntry(final VoteWithProof vote) {
        // Convert the decrypted vote values to an array of strings.
        return vote.getDecrypted().stream().map(bigInt -> bigInt.toString()).collect(Collectors.toList())
            .toArray(new String[vote.getDecrypted().size()]);
    }
}
