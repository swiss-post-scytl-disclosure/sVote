/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.exceptions;

public class ShufflerException extends MixingException {

    private static final long serialVersionUID = 1336945678787911549L;

    public ShufflerException(final String message) {
        super(message);
    }

    public ShufflerException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ShufflerException(final Throwable cause) {
        super(cause);
    }
}
