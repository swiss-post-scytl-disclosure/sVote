/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.homomorphic.impl;

import java.math.BigInteger;
import java.util.List;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.homomorphic.Randomness;
import com.scytl.ov.mixing.commons.tools.ExponentTools;

public class RandomnessGenerator {

    private final BigInteger groupOrder;

    public RandomnessGenerator(final BigInteger groupOrder) {
        this.groupOrder = groupOrder;
    }

    public Randomness[] generate(final int numberExponents) {

        List<Exponent> randomListExponent = ExponentTools.getRandomListExponent(numberExponents, groupOrder);

        final Randomness[] rho = new Randomness[numberExponents];
        for (int i = 0; i < randomListExponent.size(); i++) {
            Exponent exponent = randomListExponent.get(i);
            rho[i] = new GjosteenElGamalRandomness(exponent);
        }

        return rho;
    }
}
