/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.validation;

import java.util.Iterator;
import java.util.List;

import com.scytl.ov.mixing.commons.ballots.ElGamalEncryptedBallots;

/**
 * Manages the validation of some input using some validator(s) stored within this EncryptedBallotsValidatorManager.
 */
public final class EncryptedBallotsValidatorManager implements EncryptedBallotsValidator {

    private final List<EncryptedBallotsValidator> validators;

    /**
     * Create a EncryptedBallotsValidatorManager, setting the received list of validators.
     * 
     * @param validators
     *            the list of validators set in this EncryptedBallotsValidatorManager.
     */
    public EncryptedBallotsValidatorManager(final List<EncryptedBallotsValidator> validators) {
        this.validators = validators;
    }

    /**
     * @see com.scytl.products.ov.mixnet.commons.validation.Validator#validate(java.lang.Object)
     */
    @Override
    public ValidationResult validate(final ElGamalEncryptedBallots input) {

        final Iterator<EncryptedBallotsValidator> it = validators.iterator();

        ValidationResult lastValidationResult = ValidationResult.createNegativeValidation("No validations performed");

        boolean keepValidating = it.hasNext();

        while (keepValidating) {

            final Validator<ElGamalEncryptedBallots> validator = it.next();

            final ValidationResult lastValidation = validator.validate(input);

            lastValidationResult = lastValidation;

            keepValidating = it.hasNext() && lastValidation.getResult();
        }

        return lastValidationResult;
    }
}
