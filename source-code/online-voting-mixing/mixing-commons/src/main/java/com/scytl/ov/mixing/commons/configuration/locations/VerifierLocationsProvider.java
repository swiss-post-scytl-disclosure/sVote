/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.configuration.locations;

import java.io.InputStream;
import java.io.OutputStream;

import com.scytl.ov.mixing.commons.configuration.VerifierConfig;

public class VerifierLocationsProvider {

    private VerifierConfig verifierConfig;

    public VerifierLocationsProvider(final VerifierConfig verifierConfig) {
        this.verifierConfig = verifierConfig;
    }

    public OutputStream getVerifierOutputStream() {
        return verifierConfig.getOutput();
    }

    public InputStream getEncryptedBallotsInputOfBatch(int batchNum) {
        return verifierConfig.getEncryptedBallotsInputOfBatch().get(batchNum - 1);
    }

    public int getEncryptedBallotsInputNumber() {
        return verifierConfig.getEncryptedBallotsInputOfBatch().size();
    }

    public InputStream getReEncryptedBallotsInputOfBatch(int batchNum) {
        return verifierConfig.getReEncryptedBallotsInputOfBatch().get(batchNum - 1);
    }

    public InputStream getPublicKeyInputOfBatch(int batchNum) {
        return verifierConfig.getPublicKeyInputOfBatch().get(batchNum - 1);
    }

    public InputStream getCommitmentParametersInputOfBatch(int batchNum) {
        return verifierConfig.getCommitmentParametersInputOfBatch().get(batchNum - 1);
    }

    public InputStream getProofsInputOfBatch(int batchNum) {
        return verifierConfig.getProofsInputOfBatch().get(batchNum - 1);
    }

    public InputStream getEncryptionParametersInputOfBatch(int batchNum) {
        return verifierConfig.getEncryptionParametersInputOfBatch().get(batchNum - 1);
    }

}
