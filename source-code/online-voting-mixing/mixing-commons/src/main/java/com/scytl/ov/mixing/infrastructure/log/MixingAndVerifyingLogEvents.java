/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 7/09/15.
 */
package com.scytl.ov.mixing.infrastructure.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

public enum MixingAndVerifyingLogEvents implements LogEvent {

    SHUFREENCR_STARTING_MIXING_SINGLE_BATCH("MIX", "SHUFREENCR", "000",
            "Starting the mixing process using a single batch"),

    SHUFREENCR_STARTING_MIXING_MULTIPLE_BATCHES("MIX", "SHUFREENCR", "000",
            "Starting the mixing process using multiple batches"),

    SHUFREENCR_SUCCESS_SHUFFLING_AND_REENCRYPTING_THE_ENCRYPTED_BALLOTS("MIX", "SHUFREENCR", "000",
            "Successfully shuffled and re-encrypted the encrypted ballots"),

    SHUFREENCR_ERROR_SHUFFLING_AND_REENCRYPTING_THE_ENCRYPTED_BALLOTS("MIX", "SHUFREENCR", "1101",
            "Error while shuffling and re-encrypting the encrypted ballots"),

    SHUFREENCR_NO_SHUFFLING_AND_NO_REENCRYPTING_THE_ENCRYPTED_BALLOTS("MIX", "SHUFREENCR", "000",
            "No shuffling and no re-encrypting the encrypted ballots"),

    SHUFREENCR_ERROR_SHUFFLING_THE_ENCRYPTED_BALLOTS("MIX", "SHUFREENCR", "1102",
            "Error while shuffling the encrypted ballots"),

    SHUFREENCR_ERROR_REENCRYPTING_THE_ENCRYPTED_BALLOTS("MIX", "SHUFREENCR", "1103",
            "Error while re-encrypting the encrypted votes"),

    SHUFREENCR_SUCCESS_GENERATING_CRYPTOGRAPHIC_PROOF_OF_MIXING("MIX", "SHUFREENCR", "000",
            "Succesfully generated the cryptographic proof for the mixing"),

    SHUFREENCR_ERROR_GENERATING_CRYPTOGRAPHIC_PROOF_OF_MIXING("MIX", "SHUFREENCR", "1104",
            "Error generating the cryptographic proof for the mixing"),

    SHUFREENCR_NO_GENERATING_CRYPTOGRAPHIC_PROOF_OF_MIXING("MIX", "SHUFREENCR", "000",
            "Cryptographic proof for the mixing not generated"),

    SHUFREENCR_SUCCESS_WRITING_THE_SHUFFLED_AND_REENCRYPTING_BALLOT_BOX("MIX", "SHUFREENCR", "000",
            "Successfully wrote the shuffled and re-encrypted ballot box"),

    SHUFREENCR_ERROR_WRITING_THE_SHUFFLED_AND_REENCRYPTING_BALLOT_BOX("MIX", "SHUFREENCR", "1105",
            "Error while writing the shuffled and re-encrypted ballot box"),

    SHUFREENCR_POSITIVE_RESULT_VERIFYING_A_BATCH("MIX", "SHUFREENCR", "000",
            "Positive result while verifying a batch of outputs"),

    SHUFREENCR_NEGATIVE_RESULT_VERIFYING_A_BATCH("MIX", "SHUFREENCR", "000",
            "Negative result while verifying a batch of outputs"),

    SHUFREENCR_ERROR_VERIFYING_CRYPTOGRAPHIC_PROOF_OF_MIXING("MIX", "SHUFREENCR", "1106",
            "Error verifying the cryptographic proof of the mixing"),

    SHUFREENCR_POSITIVE_RESULT_VERIFYING_ALL_BATCH_OUTPUTS("MIX", "SHUFREENCR", "000",
            "All of the sets of proofs verified successfully"),

    SHUFREENCR_NEGATIVE_RESULT_VERIFYING_ALL_BATCH_OUTPUTS("MIX", "SHUFREENCR", "000",
            "At least one of the sets of proofs did not verify successfully"),

    SHUFREENCR_SUCCESS_GENERATING_MIXED_BALLOT_BOX("MIX", "SHUFREENCR", "000", "All mixing has finished successfully"),

    SHUFREENCR_ERROR_GENERATING_MIXED_BALLOT_BOX("MIX", "SHUFREENCR", "1107",
            "All mixing has not finished successfully");

    private final String layer;

    private final String action;

    private final String outcome;

    private final String info;

    MixingAndVerifyingLogEvents(final String layer, final String action, final String outcome, final String info) {
        this.layer = layer;
        this.action = action;
        this.outcome = outcome;
        this.info = info;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getAction()
     */
    @Override
    public String getAction() {
        return action;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getOutcome()
     */
    @Override
    public String getOutcome() {
        return outcome;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getInfo()
     */
    @Override
    public String getInfo() {
        return info;
    }

    /**
     * @see com.scytl.products.oscore.logging.api.domain.LogEvent#getLayer()
     */
    @Override
    public String getLayer() {
        return layer;
    }
}
