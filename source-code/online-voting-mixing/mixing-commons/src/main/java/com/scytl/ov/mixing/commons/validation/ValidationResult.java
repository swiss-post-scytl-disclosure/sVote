/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.validation;

/**
 * Encapsulates the result of performing a validation on some input.
 */
public final class ValidationResult {
	
	private static final ValidationResult POSITIVE_VALIDATION = new ValidationResult("", true);
	
    private final String info;

    private final boolean result;

    private ValidationResult(final String info, final boolean result) {
        this.info = info;
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public boolean getResult() {
        return result;
    }


    /**
     * Obtain a positive ValidationResult.
     * <p>
     * Note that the same instance is returned in every call.
     * 
     * @return a positive ValidationResult.
     */
    public static ValidationResult positiveValidation() {
        return POSITIVE_VALIDATION;
    }

    /**
     * Obtain a negative ValidationResult, setting the received message within the returned result.
     * 
     * @return a negative ValidationResult.
     */
    public static ValidationResult createNegativeValidation(final String info) {
        return new ValidationResult(info, false);
    }
}
