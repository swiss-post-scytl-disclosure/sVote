/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.ov.mixing.commons.ballots.ElGamalEncryptedBallots;
import com.scytl.ov.mixing.commons.exceptions.InvalidInputException;
import com.scytl.ov.mixing.commons.validation.EncryptedBallotsValidatorManager;
import com.scytl.ov.mixing.commons.validation.ValidationResult;

/**
 * Loads an {@code ElGamalEncryptedBallots} from a source.
 */
public final class ElGamalEncryptedBallotsLoader {

    private final EncryptedBallotsValidatorManager validator;

    /**
     * Create an ElGamalEncryptedBallotsLoader, including the initialization of the validators that will be used while
     * loading data. The following validators are initialized:
     * <ul>
     * <li>{@code EncryptedBallotsDuplicationValidator}</li>
     * </ul>
     */
    public ElGamalEncryptedBallotsLoader(final EncryptedBallotsValidatorManager validator) {

    	this.validator = validator;

    }

    public ElGamalEncryptedBallots loadCSV(final ZpSubgroup zpGroup, final InputStream inputstream) throws IOException {

        ElGamalEncryptedBallotEntryParser entryParser = new ElGamalEncryptedBallotEntryParser(zpGroup);

        final ElGamalEncryptedBallots elGamalEncryptedBallots;

        try (Reader reader = new InputStreamReader(inputstream);
                CSVReader<Ciphertext> elGamalEncryptedBallotReader =
                    new CSVReaderBuilder<Ciphertext>(reader).entryParser(entryParser).build()) {

            elGamalEncryptedBallots = new ElGamalEncryptedBallots(elGamalEncryptedBallotReader.readAll());
        }

        final ValidationResult validation = validator.validate(elGamalEncryptedBallots);

        if (!validation.getResult()) {
            throw new InvalidInputException(validation.getInfo());
        }

        return elGamalEncryptedBallots;
    }
}
