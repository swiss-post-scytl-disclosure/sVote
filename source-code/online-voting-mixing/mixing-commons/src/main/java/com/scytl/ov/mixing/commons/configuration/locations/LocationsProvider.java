/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.configuration.locations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.ov.mixing.commons.configuration.ApplicationConfig;
import com.scytl.ov.mixing.commons.configuration.MixerConfig;
import com.scytl.ov.mixing.commons.configuration.VerifierConfig;

/**
 * Provides I/O locations.
 */
public class LocationsProvider {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private MixerLocationsProvider mixerLocationsProvider;

    private VerifierLocationsProvider verifierLocationsProvider;

    private LocationsProvider() {
    }

    public static LocationsProvider build(final ApplicationConfig applicationConfig) {

        LocationsProvider locationsProvider = new LocationsProvider();
        if (!applicationConfig.hasMixerConfig() && !applicationConfig.hasVerifierConfig()) {
            LOG.error("The given configuration does not contain parameters for the mixer and for the verifier");
            throw new IllegalArgumentException(
                "A minimum configuration must be provided either for the mixer or the verifier (or both)");
        }

        if (applicationConfig.hasMixerConfig()) {
            final MixerConfig mixerConfig = applicationConfig.getMixer();
            locationsProvider.setMixerLocationsProvider(new MixerLocationsProvider(mixerConfig));
        }

        if (applicationConfig.hasVerifierConfig()) {
            final VerifierConfig verifierConfig = applicationConfig.getVerifier();
            locationsProvider.setVerifierLocationsProvider(new VerifierLocationsProvider(verifierConfig));
        }

        return locationsProvider;
    }

    public MixerLocationsProvider getMixerLocationsProvider() {
        return mixerLocationsProvider;
    }

    public void setMixerLocationsProvider(MixerLocationsProvider mixerLocationsProvider) {
        this.mixerLocationsProvider = mixerLocationsProvider;
    }

    public VerifierLocationsProvider getVerifierLocationsProvider() {
        return verifierLocationsProvider;
    }

    public void setVerifierLocationsProvider(VerifierLocationsProvider verifierLocationsProvider) {
    	this.verifierLocationsProvider = verifierLocationsProvider;
    }
}
