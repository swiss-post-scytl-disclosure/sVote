/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.concurrent.operations;

import java.util.List;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.ov.mixing.commons.concurrent.LoopParallelizerTask;

/**
 * An implementation of {@code AggregateOperation} that wait to finish and multiply the results of the split tasks.
 *
 * @Returns the resulting value as a {@code GroupElement}
 */
public class GroupElementMultiplyOperation
        implements AggregateOperation<ZpGroupElement, LoopParallelizerTask<ZpGroupElement>> {

    /**
     * @see com.scytl.products.ov.mixnet.commons.concurrent.operations.AggregateOperation#execute(java.util.List)
     */
    @Override
    public ZpGroupElement execute(final List<LoopParallelizerTask<ZpGroupElement>> tasks) {

        try {

            ZpGroupElement element = tasks.get(0).join();

            for (int i = 1; i < tasks.size(); i++) {
                element = element.multiply(tasks.get(i).join());
            }

            return element;

        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }

    }

}
