/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.ballots;

import java.util.List;
import java.util.stream.Collectors;

import com.scytl.cryptolib.elgamal.bean.CiphertextImpl;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;

/**
 * Encapsulates a list of ballots that have been encrypted using the ElGamal cryptosystem.
 * <p>
 * See {@link CiphertextImpl} for the description of the contents of an encrypted ballot that has been encrypted using
 * the ElGamal cryptosystem.
 */
public final class ElGamalEncryptedBallots {

    private final List<Ciphertext> ballots;

    /**
     * Create a CiphertextImpls, setting the received list of ballots.
     * 
     * @param ballots
     *            a list of {@link CiphertextImpl}.
     * @throws IllegalArgumentException
     *             if {@code ballots} is null
     */
    public ElGamalEncryptedBallots(final List<Ciphertext> ballots) {

        List<Ciphertext> defensiveListBallots = getDefensiveList(ballots);

        validateInputs(defensiveListBallots);

        this.ballots = ballots;
    }

    public List<Ciphertext> getBallots() {
        return getDefensiveList(ballots);
    }

    private static List<Ciphertext> getDefensiveList(final List<Ciphertext> elements) {

        return elements.stream().collect(Collectors.toList());
    }

    private static void validateInputs(final List<Ciphertext> ballots) {
        if (ballots == null) {
            throw new IllegalArgumentException("The list of ballots must be initialized");
        }
    }
}
