/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.concurrent;

import com.scytl.ov.mixing.commons.tools.LoopTools.Range;

/**
 * An interface to encapsulate implementations of different base case actions (without any return value). An example of
 * implementation is {@code PrivateAndPublicCommitmentProcessor}
 */
@FunctionalInterface
public interface ParallelProcessAction {

    void run(Range range);

}
