/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.configuration.locations;

import java.io.InputStream;
import java.io.OutputStream;

import com.scytl.ov.mixing.commons.configuration.MixerConfig;

public class MixerLocationsProvider {

    private MixerConfig mixerConfig;

    public MixerLocationsProvider(final MixerConfig mixerConfig) {
        this.mixerConfig = mixerConfig;
    }

    public InputStream getEncryptedBallotsInput() {
        return mixerConfig.getEncryptedBallotsInput();
    }

    public InputStream getEncryptionParametersInput() {
        return mixerConfig.getEncryptionParametersInput();
    }

    public InputStream getPublicKeyInput() {
        return mixerConfig.getPublicKeyInput();
    }

    public InputStream getBallotDecryptionKeyInput() {
        return mixerConfig.getBallotDecryptionKeyInput();
    }

    public OutputStream getEncryptedBallotsOutputOfBatch(int batchNum) {
        return mixerConfig.getEncryptedBallotsOutputOfBatch().get(batchNum - 1);
    }

    public OutputStream getReEncryptedBallotsOutputOfBatch(int batchNum) {
        return mixerConfig.getReEncryptedBallotsOutputOfBatch().get(batchNum - 1);
    }

    public OutputStream getPublicKeyOutputOfBatch(int batchNum) {
        return mixerConfig.getPublicKeyOutputOfBatch().get(batchNum - 1);
    }

    public OutputStream getCommitmentParametersOutputOfBatch(int batchNum) {
        return mixerConfig.getCommitmentParametersOutputOfBatch().get(batchNum - 1);
    }

    public OutputStream getProofsOutputOfBatch(int batchNum) {
        return mixerConfig.getProofsOutputOfBatch().get(batchNum - 1);
    }

    public OutputStream getEncryptionParametersOutputOfBatch(int batchNum) {
        return mixerConfig.getEncryptionParametersOutputOfBatch().get(batchNum - 1);
    }

    public OutputStream getVotesWithProofOutputOfBatch(int batchNum) {
        return mixerConfig.getVotesWithProofOutputOfBatch().get(batchNum - 1);
    }

    public OutputStream getDecryptedVotesOutputOfBatch(int batchNum) {
        return mixerConfig.getDecryptedVotesOutputOfBatch().get(batchNum - 1);
    }
}
