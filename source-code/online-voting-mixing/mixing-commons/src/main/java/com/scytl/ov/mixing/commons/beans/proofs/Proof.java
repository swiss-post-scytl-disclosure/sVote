/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.beans.proofs;

/**
 * Generic interface for the proofs which guarantees the correctness of mixing.
 *
 * @author aakimov
 */
public interface Proof {
}
