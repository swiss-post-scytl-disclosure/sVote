/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.concurrent.operations;

import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.ov.mixing.commons.concurrent.LoopParallelizerTask;

/**
 * An implementation of {@code AggregateOperation} that wait to finish the {@code LoopParallelizerTask} and add the
 * partial result List to another List of {@code ElGamalEncryptedBallot} with all the results.
 *
 * @Returns the complete List of {@code ElGamalEncryptedBallot}
 */
public class EncryptedBallotsCollectionAdderOperation
        implements AggregateOperation<List<Ciphertext>, LoopParallelizerTask<List<Ciphertext>>> {

    /**
     * @see com.scytl.products.ov.mixnet.commons.concurrent.operations.AggregateOperation#execute(java.util.List)
     */
    @Override
    public List<Ciphertext> execute(final List<LoopParallelizerTask<List<Ciphertext>>> tasks) {

        List<Ciphertext> reEncryptedBallots = tasks.get(0).join();
        for (int i = 1; i < tasks.size(); i++) {
            reEncryptedBallots.addAll(tasks.get(i).join());
        }

        return reEncryptedBallots;
    }

}
