/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.concurrent.processor;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.ov.mixing.commons.concurrent.ParallelProcessTask;
import com.scytl.ov.mixing.commons.homomorphic.Cryptosystem;
import com.scytl.ov.mixing.commons.homomorphic.Randomness;
import com.scytl.ov.mixing.commons.homomorphic.impl.GjosteenElGamalPlaintext;
import com.scytl.ov.mixing.commons.homomorphic.impl.GjosteenElGamalRandomness;
import com.scytl.ov.mixing.commons.tools.LoopTools;

public class PreComputationCalculatorProcessor implements ParallelProcessTask<List<Ciphertext>> {

    private final ZpGroupElement[] arrayOfIdentityElement;

    private final Randomness[] requiredRandomExponents;

    private final Cryptosystem cryptoSystem;

    public PreComputationCalculatorProcessor(final ZpGroupElement[] arrayOfIdentityElement,
            final Randomness[] requiredRandomExponents, final Cryptosystem cryptoSystem) {

        this.arrayOfIdentityElement = arrayOfIdentityElement;
        this.requiredRandomExponents = requiredRandomExponents;
        this.cryptoSystem = cryptoSystem;
    }

    @Override
    public List<Ciphertext> run(final LoopTools.Range range) {

        List<Ciphertext> preComputations = new ArrayList<>();

        for (int i = range.getStart(), rangeEnd = range.getEnd(); i < rangeEnd; i++) {

            preComputations.add(cryptoSystem.encrypt(new GjosteenElGamalPlaintext(arrayOfIdentityElement),
                new GjosteenElGamalRandomness(requiredRandomExponents[i].getExponent())));
        }

        return preComputations;
    }
}
