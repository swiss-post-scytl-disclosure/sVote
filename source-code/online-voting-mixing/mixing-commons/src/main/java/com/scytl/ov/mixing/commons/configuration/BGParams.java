/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.configuration;

public class BGParams {

    private Integer m;

    private Integer n;

    private Integer mu;

    private Integer numIterations;

    public Integer getM() {
        return m;
    }

    public void setM(final Integer m) {
        this.m = m;
    }

    public Integer getN() {
        return n;
    }

    public void setN(final Integer n) {
        this.n = n;
    }

    public Integer getMu() {
        return mu;
    }

    public void setMu(final Integer mu) {
        this.mu = mu;
    }

    public Integer getNumIterations() {
        return numIterations;
    }

    public void setNumIterations(final Integer numIterations) {
        this.numIterations = numIterations;
    }

    @Override
    public String toString() {
        return "BGParams{" + "_m=" + m + ", _n=" + n + ", _mu=" + mu + ", _numIterations=" + numIterations + '}';
    }
}
