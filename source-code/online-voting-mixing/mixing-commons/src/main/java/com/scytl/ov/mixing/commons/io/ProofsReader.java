/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.io;

import java.io.IOException;
import java.io.InputStream;

import com.scytl.ov.mixing.commons.beans.proofs.BayerGrothProof;

@FunctionalInterface
public interface ProofsReader {

    BayerGrothProof read(InputStream inputStream) throws IOException;
}
