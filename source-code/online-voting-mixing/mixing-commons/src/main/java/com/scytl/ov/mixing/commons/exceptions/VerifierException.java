/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.exceptions;

public class VerifierException extends RuntimeException {

    private static final long serialVersionUID = 1098989592637986287L;

    public VerifierException(final String message) {
        super(message);
    }

    public VerifierException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public VerifierException(final Throwable cause) {
        super(cause);
    }
}
