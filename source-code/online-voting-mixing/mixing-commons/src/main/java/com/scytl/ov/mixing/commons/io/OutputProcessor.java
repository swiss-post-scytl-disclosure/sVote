/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.io;

import java.io.IOException;

import com.scytl.ov.mixing.commons.ballots.ElGamalEncryptedBallots;
import com.scytl.ov.mixing.commons.configuration.locations.LocationsProvider;

/**
 *
 */
public interface OutputProcessor {

    void storeEncryptedBallotsToCSV(final String outputPathAsString, final ElGamalEncryptedBallots encryptedBallots,
            final LocationsProvider locationsProvider) throws IOException;

    void storeReEncryptedBallotsToCSV(final String outputPathAsString, final ElGamalEncryptedBallots encryptedBallots,
            final LocationsProvider locationsProvider) throws IOException;

    void storeProofsToJSON();

}
