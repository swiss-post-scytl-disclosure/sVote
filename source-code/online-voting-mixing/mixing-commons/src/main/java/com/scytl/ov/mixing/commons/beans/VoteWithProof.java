/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.beans;

import java.math.BigInteger;
import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;

/**
 * A bean to encapsulate the encrypted and decrypted vote and decryption proof.
 */
public class VoteWithProof {

    private final Ciphertext encrypted;
    private final List<BigInteger> decrypted;

    /**
     * JSON representation of the Decryption proof.
     */
    private final String proof;

    public VoteWithProof(final Ciphertext encrypted, final List<BigInteger> decryptedVotes, String proof) {
        this.encrypted = encrypted;
        this.decrypted = decryptedVotes;
        this.proof = proof;
    }

    public Ciphertext getEncrypted() {
        return encrypted;
    }

    public List<BigInteger> getDecrypted() {
        return decrypted;
    }

    public String getProof() {
        return proof;
    }
}
