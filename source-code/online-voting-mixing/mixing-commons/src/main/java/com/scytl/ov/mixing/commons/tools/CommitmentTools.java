/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.tools;

import com.scytl.ov.mixing.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PublicCommitment;

public class CommitmentTools {

	/**
	 * Non-public constructor
	 */
	private CommitmentTools() {
	}
	
	public static PublicCommitment[] makePublic(final PrivateCommitment[] notCommitedStatementCa) {
        PublicCommitment[] publicCommitments = new PublicCommitment[notCommitedStatementCa.length];
        for (int i = 0; i < notCommitedStatementCa.length; i++) {
            publicCommitments[i] = notCommitedStatementCa[i].makePublicCommitment();
        }
        return publicCommitments;
    }
}
