/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.beans;

import static java.util.Collections.emptyList;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalComputationsValues;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;

/**
 * A bean to encapsulate a ElGamal encrypted ballot information.
 */
public final class ElGamalEncryptedBallot implements Ciphertext {
    private final List<ZpGroupElement> elements;

    private final boolean poisonPill;

    private ElGamalEncryptedBallot(List<ZpGroupElement> elements,
            boolean poisonPill) {
        this.elements = elements;
        this.poisonPill = poisonPill;
    }

    public static ElGamalEncryptedBallot create(final List<ZpGroupElement> elements) {
        if ((elements == null) || elements.isEmpty()) {
            throw new IllegalArgumentException(
                    "The list of group elements must be initialized and must contain at least 1 elements.");
        }
        return new ElGamalEncryptedBallot(new ArrayList<>(elements), false);
    }

    public static ElGamalEncryptedBallot poisonPill() {
        return new ElGamalEncryptedBallot(emptyList(), true);
    }

    @Override
    public ZpGroupElement getGamma() {
        return elements.get(0);
    }

    @Override
    public List<ZpGroupElement> getPhis() {
        return elements.subList(1, elements.size());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((elements == null) ? 0 : elements.hashCode());
        result = prime * result + (poisonPill ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ElGamalEncryptedBallot other = (ElGamalEncryptedBallot) obj;
        if (elements == null) {
            if (other.elements != null) {
                return false;
            }
        } else if (!elements.equals(other.elements)) {
            return false;
        }
        if (poisonPill != other.poisonPill) {
            return false;
        }
        return true;
    }

    public ElGamalComputationsValues getValues() {
        ZpGroupElement gamma = getGamma();
        List<ZpGroupElement> phis = getPhis();
        try {
            return new ElGamalComputationsValues(gamma, phis);
        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException("Exception while trying to obtain ElGamal values: " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "ElGamalEncryptedBallot [_gamma=" + getGamma() + ", _phis=" + getPhis() + "]";
    }

    @Override
    public List<ZpGroupElement> getElements() {
        return elements;
    }

    public boolean isPoisonPill() {
        return poisonPill;
    }
}
