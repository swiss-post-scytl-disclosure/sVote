/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.tools;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.ov.mixing.commons.homomorphic.Randomness;

public class RandomnessTools {

	/**
	 * Non-public constructor
	 */
	private RandomnessTools() {
	}
	
	public static Randomness evaluatePolynomialFrom1(final Randomness[] e, final Exponent challenge) {
        Randomness result = e[e.length - 1];
        for (int i = e.length - 2; i >= 0; i--) {
            result = result.multiply(challenge).add(e[i]);
        }
        return result;
    }

    public static Randomness evaluatePolynomialFromX(final Randomness[] e, final Exponent challenge) {
        return evaluatePolynomialFrom1(e, challenge).multiply(challenge);
    }
}
