/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.proofs.bg.config;

import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.ov.mixing.commons.configuration.MatrixDimensions;
import com.scytl.ov.mixing.commons.exceptions.InvalidInputException;
import com.scytl.ov.mixing.commons.homomorphic.Cryptosystem;

/**
 * Contains the required parameters for the configuration of the proofs generator.
 */
public class ProofsGeneratorConfigurationParams {

    private final int concurrencyLevel;

    private final MatrixDimensions matrixDimensions;

    private final Cryptosystem cryptosystem;

    private final int numIterations;

    private final int mu;

    private final ZpSubgroup zpGroup;

    /**
     * @param matrixM
     * @param matrixN
     * @param cryptosystem
     * @param numIterations
     * @param mu
     * @param groupParams
     * @param concurrencyLevel
     */
    public ProofsGeneratorConfigurationParams(final int matrixM, final int matrixN, final Cryptosystem cryptosystem,
            final int numIterations, final int mu, final ZpSubgroup zpGroup, final int concurrencyLevel) {
        super();

        if (numIterations != 0) {
            throw new InvalidInputException("Number of iterations > 0 is currently not supported.");
        }

        this.matrixDimensions = new MatrixDimensions(matrixM, matrixN);
        this.cryptosystem = cryptosystem;
        this.numIterations = numIterations;
        this.mu = mu;
        this.zpGroup = zpGroup;
        this.concurrencyLevel = concurrencyLevel;
    }

    /**
     * @return Returns the cryptosystem.
     */
    public Cryptosystem getCryptosystem() {
        return cryptosystem;
    }

    public boolean applyReduction() {
        return numIterations != 0;
    }

    /**
     * @return Returns the numIterations.
     */
    public int getNumIterations() {
        return numIterations;
    }

    /**
     * @return Returns the mu.
     */
    public int getMu() {
        return mu;
    }

    /**
     * @return Returns the matrixDimensions.
     */
    public MatrixDimensions getMatrixDimensions() {
        return matrixDimensions;
    }

    /**
     * @return Returns the zpGroupParams.
     */
    public ZpSubgroup getZpGroupParams() {
        return zpGroup;
    }

    /**
     * @return Returns the concurrencyLevel.
     */
    public int getConcurrencyLevel() {
        return concurrencyLevel;
    }

}
