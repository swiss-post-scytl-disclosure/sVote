/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.io.ciphertext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class CustomObjectMapper extends ObjectMapper {

    private static final long serialVersionUID = -577061455838015150L;

    public CustomObjectMapper() {
        registerModule(new CiphertextModule());
        enable(SerializationFeature.INDENT_OUTPUT);
    }
}
