/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.tools;

import java.math.BigInteger;
import java.security.SecureRandom;

public class BigIntTools {
	
	/**
	 * Non-public constructor
	 */
	private BigIntTools() {		 
	}
	
	public static BigInteger generateBigInteger(final BigInteger max) {
        SecureRandom random = new SecureRandom();
        BigInteger expo;
        do {
            expo = new BigInteger(max.bitLength(), random);
        } while (expo.compareTo(max) >= 0);
        return expo;
    }

    public static BigInteger generateInvertibleBigInteger(final BigInteger max) {
        BigInteger expo;
        do {
            expo = generateBigInteger(max);
        } while (BigInteger.ZERO.equals(expo));
        return expo;
    }
}
