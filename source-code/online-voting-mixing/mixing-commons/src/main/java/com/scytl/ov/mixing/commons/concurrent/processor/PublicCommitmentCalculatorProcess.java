/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.concurrent.processor;

import com.scytl.ov.mixing.commons.concurrent.ParallelProcessAction;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PrivateCommitment;
import com.scytl.ov.mixing.commons.proofs.bg.commitments.PublicCommitment;
import com.scytl.ov.mixing.commons.tools.LoopTools.Range;

/**
 * Base case to be parallelized using {@code LoopParallelizerAction}
 */
public class PublicCommitmentCalculatorProcess implements ParallelProcessAction {

    private final PublicCommitment[] publicCommitments;

    private final PrivateCommitment[] privateCommitments;

    public PublicCommitmentCalculatorProcess(final PublicCommitment[] publicCommitment,
            final PrivateCommitment[] privateCommitment) {

        this.publicCommitments = publicCommitment;
        this.privateCommitments = privateCommitment;

    }

    /**
     * @see com.scytl.products.ov.mixnet.commons.concurrent.ParallelProcessAction#run(com.scytl.products.ov.mixnet.commons
     *      .tools.LoopTools.Range)
     */
    @Override
    public void run(final Range range) {
        for (int i = range.getStart(), rangeEnd = range.getEnd(); i < rangeEnd; i++) {
            publicCommitments[i] = privateCommitments[i].makePublicCommitment();
        }
    }

}
