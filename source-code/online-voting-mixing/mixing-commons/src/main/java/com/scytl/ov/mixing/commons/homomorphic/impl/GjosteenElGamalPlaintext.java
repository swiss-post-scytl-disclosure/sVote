/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.homomorphic.impl;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.ov.mixing.commons.homomorphic.Plaintext;

public class GjosteenElGamalPlaintext implements Plaintext {

    private final ZpGroupElement[] m;

    public GjosteenElGamalPlaintext(final ZpGroupElement[] m) {
        this.m = m;
    }

    public ZpGroupElement getValue(final int i) {
        return m[i];
    }

    @Override
    public GjosteenElGamalPlaintext multiply(final Plaintext p) {

        try {

            ZpGroupElement[] aux = new ZpGroupElement[m.length];
            for (int i = 0; i < aux.length; i++) {
                aux[i] = m[i].multiply(((GjosteenElGamalPlaintext) p).getValue(i));
            }
            return new GjosteenElGamalPlaintext(aux);

        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }
}
