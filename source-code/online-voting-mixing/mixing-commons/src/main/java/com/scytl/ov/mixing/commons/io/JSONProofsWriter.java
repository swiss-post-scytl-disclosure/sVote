/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.ov.mixing.commons.io;

import java.io.IOException;
import java.io.OutputStream;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scytl.ov.mixing.commons.beans.proofs.BayerGrothProof;
import com.scytl.ov.mixing.commons.exceptions.MixingException;
import com.scytl.ov.mixing.commons.io.ciphertext.CustomObjectMapper;

public class JSONProofsWriter implements ProofsWriter {

    @Override
    public void write(final OutputStream output, final BayerGrothProof shuffleProof) throws IOException {

        validatePath(output);

        final ObjectMapper mapper = new CustomObjectMapper();
        try (OutputStream outputStream = output) {
            mapper.setSerializationInclusion(Include.NON_NULL).writerWithDefaultPrettyPrinter().writeValue(output,
                shuffleProof);
        }
    }

    private static void validatePath(final OutputStream path) {

        if (null == path) {
        	 throw new MixingException("Could not write to output");
        }
    }
}
