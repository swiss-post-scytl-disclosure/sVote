/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.api;

import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;
import com.scytl.products.ov.mixnet.commons.exceptions.VerifierException;
import org.junit.BeforeClass;
import org.junit.Test;

public class VerifyingServiceTest {

    private static VerifyingService _target;

    @BeforeClass
    public static void setUp() {

        _target = new VerifyingService(null);
    }

    @Test(expected = VerifierException.class)
    public void throw_expected_exception_when_passed_null_config() {

        ApplicationConfig applicationConfigNull = null;
        LocationsProvider locationsProvider = null;
        _target.verify(applicationConfigNull, locationsProvider);
    }
}
