/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.proofs.bg.shuffle;

import org.junit.Ignore;
import org.junit.experimental.categories.Category;

import com.scytl.cryptolib.elgamal.bean.CiphertextImpl;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;
import com.scytl.products.ov.mixnet.commons.proofs.bg.commitments.CommitmentParams;
import com.scytl.products.ov.mixnet.commons.proofs.bg.config.ProofsGeneratorConfigurationParams;
import com.scytl.products.ov.mixnet.commons.tests.categories.SlowTest;
import com.scytl.products.ov.mixnet.commons.tools.MultiExponentiation;
import com.scytl.products.ov.mixnet.proofs.bg.multiexp.ComputeAllE;

// un-ignore it when reduction will be supported
// it requires adding data to Random Oracle on Reduction step. This step is executed when
// ProofsGeneratorConfigurationParams#_numIterations > 0.
@Ignore
@Category(SlowTest.class)
public class ParallelShuffleReductionProofITest extends BaseShuffleReductionITest {

    @Override
    protected SecondAnswerGenerator secondAnswerGenerator(final ProofsGeneratorConfigurationParams config,
            final CommitmentParams pars, final CiphertextImpl[][] reencryptedCiphertexts, final Randomness[] rho,
            final MultiExponentiation limMultiExpo, final ComputeAllE computeAllE, final int concurrencyLevel) {

        return new ParallelSecondAnswerGenerator(config, rho, reencryptedCiphertexts, pars, computeAllE, limMultiExpo, concurrencyLevel);
    }

    /**
     * @see com.scytl.products.ov.mixnet.proofs.bg.shuffle.BaseShuffleProofIT#commitmentsInformer()
     */
    @Override
    protected PrivateAndPublicCommitmentsGenerator commitmentsInformer() {
        return new ParallelPrivateAndPublicCommitmentsGenerator();
    }
}
