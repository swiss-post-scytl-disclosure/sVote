/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.api;

import static org.mockito.Mockito.mock;

import com.scytl.products.ov.mixnet.MixnetLoadBalancer;
import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.MixerConfig;
import com.scytl.products.ov.mixnet.commons.configuration.VerifierConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;
import java.io.IOException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class MixingServiceTest {

  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();

  @Test(expected = NullPointerException.class)
  public void fail_on_null_config() {
    MixingService sut = new MixingService(mock(MixnetLoadBalancer.class));
    sut.mix(null, null);
  }

  @Test(expected = NullPointerException.class)
  public void fail_if_no_load_balancer() {
    new MixingService(null);
  }

  @Test
  public void run_with_minimal_config() throws IOException {
    ApplicationConfig applicationConfig = getApplicationConfig(
        temporaryFolder.getRoot().toString());
    LocationsProvider locationsProvider = LocationsProvider.build(applicationConfig);

    MixnetLoadBalancer loadBalancer = mock(MixnetLoadBalancer.class);

    MixingService sut = new MixingService(loadBalancer);

    sut.mix(applicationConfig, locationsProvider);
  }

  /**
   * @return a configuration for the mixer.
   */
  private ApplicationConfig getApplicationConfig(String tempDir) {
    ApplicationConfig applicationConfig = new ApplicationConfig();
    applicationConfig.setMixer(new MixerConfig());
    applicationConfig.setVerifier(new VerifierConfig());

    return applicationConfig;
  }
}
