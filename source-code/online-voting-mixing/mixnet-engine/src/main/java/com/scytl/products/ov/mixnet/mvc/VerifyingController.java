/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.mvc;

import java.time.Duration;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.mixnet.api.VerifyingServiceAPI;
import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;
import com.scytl.products.ov.mixnet.commons.exceptions.VerifierException;

public class VerifyingController {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private final VerifyingServiceAPI verifyingService;

    public VerifyingController(VerifyingServiceAPI verifyingService) {
        this.verifyingService = verifyingService;
    }

    /**
     * @param applicationConfig
     * @return
     */
    public Boolean verify(final ApplicationConfig applicationConfig, final LocationsProvider locationsProvider) throws VerifierException {

        if (applicationConfig == null) {
            throw new VerifierException("The received application config was null");
        }

        if (!applicationConfig.hasVerifierConfig() || !applicationConfig.getVerifier().isVerify()) {

            LOG.info("Application configured to not perform verifying");
            return false;
        }

        Instant start = Instant.now();
        Boolean result = verifyingService.verify(applicationConfig, locationsProvider);

        Instant end = Instant.now();
        LOG.info("Verify duration was: " + Duration.between(start, end));

        return result;
    }
}
