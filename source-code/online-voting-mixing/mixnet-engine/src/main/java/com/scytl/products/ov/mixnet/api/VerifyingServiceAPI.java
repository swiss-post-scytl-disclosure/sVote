/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.api;

import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;
import com.scytl.products.ov.mixnet.commons.exceptions.VerifierException;

public interface VerifyingServiceAPI {

    /**
     * Performs the verification of the proofs according to the given configuration.
     *
     * @param applicationConfig
     * @return {@code true} If proofs have been successfully verified, {@code false} otherwise.
     */
    boolean verify(final ApplicationConfig applicationConfig, final LocationsProvider locationsProvider) throws VerifierException;
}
