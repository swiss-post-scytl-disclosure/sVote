/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet;

import java.io.IOException;
import java.util.List;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;
import com.scytl.products.ov.mixnet.commons.exceptions.MixingException;
import com.scytl.products.ov.mixnet.commons.homomorphic.Randomness;

public interface MixnetLoadBalancer {

    /**
     * Orchestrates the execution of one/several concurrent mixing operations.
     *
     * @param randomExponents
     * @param preComputations
     */
    void balance(ApplicationConfig applicationConfig, LocationsProvider locationsProvider,
            final Randomness[] randomExponents, final List<Ciphertext> preComputations)
            throws IOException, MixingException;
}
