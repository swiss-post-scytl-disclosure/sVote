/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.batches;

public class MixingResult {

    private final boolean result;

    private final int batchNum;

    private final int partitionSize;

    private final int offset;

    public MixingResult(final boolean result, final int batchNum, final int partitionSize, final int offset) {
        this.result = result;
        this.batchNum = batchNum;
        this.partitionSize = partitionSize;
        this.offset = offset;
    }

    public boolean getResult() {
        return result;
    }

    public int getBatchNum() {
        return batchNum;
    }

    public int getPartitionSize() {
        return partitionSize;
    }

    public int getOffset() {
        return offset;
    }
}
