/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.batches;

import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.products.ov.mixnet.NonMixer;
import com.scytl.products.ov.mixnet.commons.ballots.ElGamalEncryptedBallots;
import com.scytl.products.ov.mixnet.commons.configuration.BGParams;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;

/**
 * This class creates non mixing batches which share common business data such
 * as the encryption parameters, the public key, I/O configuration, among
 * others...
 */
public class NonMixingBatchFactory {

    private final NonMixer nonMixer;

    private final ZpSubgroup zpGroup;

    private final ElGamalPublicKey publicKey;

    private final ElGamalPrivateKey ballotDecryptionKey;

    private final LocationsProvider locationsProvider;

    private PrimitivesServiceAPI primitivesService;

    private NonMixingBatchFactory(final NonMixer nonMixer, final ZpSubgroup zpGroup, final ElGamalPublicKey publicKey,
            final ElGamalPrivateKey ballotDecryptionKey, final LocationsProvider locationsProvider,
            PrimitivesServiceAPI primitivesService) {

        this.nonMixer = nonMixer;
        this.zpGroup = zpGroup;
        this.publicKey = publicKey;
        this.ballotDecryptionKey = ballotDecryptionKey;
        this.locationsProvider = locationsProvider;
        this.primitivesService = primitivesService;
    }

    public static NonMixingBatchFactory aNew(final NonMixer nonMixer, final ZpSubgroup zpGroup,
            final ElGamalPublicKey publicKey, final ElGamalPrivateKey ballotDecryptionKey,
            final LocationsProvider locationsProvider, PrimitivesServiceAPI primitivesService) {
        return new NonMixingBatchFactory(nonMixer, zpGroup, publicKey, ballotDecryptionKey, locationsProvider,
            primitivesService);
    }

    public NonMixingBatch create(final int partitionSize, final ElGamalEncryptedBallots batchElGamalEncryptedBallots,
            final int batchNum, final int offset, final BGParams bGParams, final int i) {

        return new NonMixingBatch(partitionSize, nonMixer, batchElGamalEncryptedBallots, offset, batchNum, bGParams,
            zpGroup, i, publicKey, ballotDecryptionKey, locationsProvider, primitivesService);
    }
}