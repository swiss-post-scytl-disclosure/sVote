/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.api;

import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;
import com.scytl.products.ov.mixnet.commons.exceptions.MixingException;

public interface MixingServiceAPI {

    /**
     * @param applicationConfig
     */
    void warmUp(ApplicationConfig applicationConfig) throws MixingException;

    /**
     * Performs the mixing given the specified configuration. Configuration has to be properly set.
     *
     * @param applicationConfig The required configuration to perform a mixing.
     * @throws MixingException if there were any problems during the mixing.
     */
    void mix(ApplicationConfig applicationConfig, LocationsProvider locationsProvider) throws MixingException;

}
