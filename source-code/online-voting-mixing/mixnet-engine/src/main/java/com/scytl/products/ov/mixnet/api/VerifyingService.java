/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.api;

import com.scytl.products.ov.mixnet.BGVerifier;
import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;
import com.scytl.products.ov.mixnet.commons.configuration.locations.LocationsProvider;
import com.scytl.products.ov.mixnet.commons.exceptions.VerifierException;

public final class VerifyingService implements VerifyingServiceAPI {

    private final BGVerifier bgVerifier;

    public VerifyingService(final BGVerifier bgVerifier) {
        this.bgVerifier = bgVerifier;
    }

    /**
     * @param applicationConfig
     * @return
     */
    @Override
	public boolean verify(final ApplicationConfig applicationConfig, final LocationsProvider locationsProvider)
			throws VerifierException {

        if (applicationConfig == null) {
            throw new VerifierException("The received application config was null");
        }

        try {

            return bgVerifier.verify(locationsProvider);
           
        } catch (Exception e) {
            throw new VerifierException("Error while executing verify operation", e);
        }
    }
}
