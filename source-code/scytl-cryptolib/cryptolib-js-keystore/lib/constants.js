/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

/**
 * Defines constants for the keystore module.
 */
module.exports = Object.freeze({CHARACTER_MAX_RADIX: 36});
