# Key Store Module

This module defines the key store API for Scytl's JavaScript cryptographic library. The starting point for using this module is the instantiation of a key store service.

## How to view, build and test the source code of this module

Start by git cloning the project `comm/scytl-cryptolib` from `Stash`.

The source code of this module will be found in the directory `scytl-cryptolib/cryptolib-js-keystore/lib`.

To build this module, change to the directory `scytl-cryptolib/cryptolib-js-keystore` and do the following:

```java
npm install
--or--
mvn clean install -DskipTests
```
The unit tests of this module will be found in the directory `scytl-cryptolib/cryptolib-js-keystore/spec`. To run the tests, change to the directory `scytl-cryptolib/cryptolib-js-keystore` and do the following:

```javascript
karma test
--or--
mvn test
```
To only run a chosen test suite, add an `f` in front of the corresponding `describe` statement. For example:

```javascript
fdescribe('load a PKCS12 key store that should be able to', function()
...
```
To only run a chosen test, add an `f` in front of the corresponding `it` statement. For example:

```javascript
fit('retrieve a private key, given its storage alias name and password',
...
```
**Note:** To build and test the entire `scytl-cryptolib` project, change to the directory `scytl-cryptolib` and do the following:

```javascript
mvn clean install
```
## How to generate the JSDoc for this module

To generate the JSDoc for this module, change to the directory `scytl-cryptolib/cryptolib-js-keystore`, build the module (if not already done) and do the following:

```javascript
node_modules/.bin/jsdoc lib
```
This will generate a file called `out`. Double click on the file `out/index.html` to view the JSDoc.

## How to `npm` install this module

To `npm` install this module in standalone mode, do the following:

```javascript
npm install --registry https://nexus.scytl.net/content/groups/public-npm/ scytl-keystore
```
To `npm` install this module as a dependency of your own module, do the following:

1. Add the following lines to the `dependencies` section of your module's `package.json` file.
    ```javascript
    "scytl-codec": "^2.1.0",
    "scytl-cryptopolicy": "^2.1.0",
    "scytl-pbkdf": "^2.1.0",
    "scytl-certificate": "^2.1.0",
    "scytl-symmetric": "^2.1.0",
    "scytl-mathematical": "^2.1.0",
    "scytl-elgamal": "^2.1.0",
    "scytl-keystore": "^2.1.0",
    ```
2. Make sure that the `.npmrc` file of your module contains the following line.
    ```javascript
    registry=https://nexus.scytl.net/content/groups/public-npm/
    ```
3. Install all dependencies.
    ```javascript
    npm install
    ```

## How to instantiate a key store service

The following example shows the default way to create a new instance of a key store service.

```javascript
var keyStore = require('scytl-keystore');

var keyStoreService = keyStore.newService();
```
The following example shows how to override the default cryptographic policy used for key store operations by changing the PBKDF derivation key length to 16 bytes.

``` java
var cryptoPolicy = require('scytl-cryptopolicy');
var keyStore = require('scytl-keystore');

var myPolicy = cryptoPolicy.newInstance();

myPolicy.primitives.keyDerivation.pbkdf.keyLengthBytes =
  cryptoPolicy.options.primitives.keyDerivation.pbkdf.keyLengthBytes.KL_16;

var keyStoreService = keyStore.newService({policy: myPolicy});
```
**Note:** It is desirable to use the property `CryptographicPolicy.options` to override the default policy, as shown above, in order to ensure that one is using an option that is allowed by the cryptographic policy.

The following example shows how to supply one's own PBKDF service, symmetric service and ElGamal service to the key store service.

``` java
var keyStore = require('scytl-keystore');
var pbkdf = require('scytl-pbkdf');
var symmetric = require('scytl-symmetric');
var elGamal = require('scytl-elgamal');

var myPbkdfService = pbkdf.newService({policy: myPolicy});
var mySymmetricService = symmetric.newService({policy: myPolicy});
var myElGamalService = elGamal.newService();
var keyStoreService = keyStore.newService({
  policy: myPolicy,
  pbkdfService: myPbkdfService,
  symmetricCryptographyService: mySymmetricService,
  elGamalCryptographyService: myElGamalService
});
```
**Note** If both a service and the cryptographic policy are specified as options, one must ensure that the specified service uses the specified policy, as shown above.

## How to load a key store.

The following examples show how to create a new `Pkcs12KeyStore` object and load it with an existing PKCS #12 key store, given the key store's password. The key store may be provided as either a DER encoded ASN.1 structure, or a Base64 encoding of such a structure.

```javascript
var pkcs12keyStore = keyStoreService.newPkcs12KeyStore(keyStoreDer, password);

var pkcs12KeyStore = keyStoreService.newPkcs12KeyStore(keyStoreDerB64, password);
```

The following examples show how to create a new `ScytlKeyStore` object and load it with an existing Scytl key store, given the key store's password. The key store may be provided as either an Object with expected properties, or a JSON string representation of such an object.

```javascript
var scytlKeyStore = keyStoreService.newScytlKeyStore(keyStoreObj, password);

var scytlKeyStore = keyStoreService.newScytlKeyStore(keyStoreObjJson, password);
```
## How to retrieve the contents of a PKCS #12 key store

In the examples that follow, the PKCS #12 key store will contain two private key entries, and their associated certificate chains. The storage alias names of the private keys will be `rootPrivateKeyAlias` and `leafPrivateKeyAlias`. The certificate associated with the private key with alias `rootPrivateKeyAlias` will be self-signed. The certificate associated with the private key with alias `leafPrivateKeyAlias` will be signed by the self-signed certificate.

The following examples show how to retrieve a private key from a PKCS #12 key store, given the key's storage alias name and storage password. The retrieved private key will be in PEM format.

```javascript
var rootPrivateKeyPem = pkcs12KeyStore.getPrivateKey(rootPrivateKeyAlias, password);
var leafPrivateKeyPem = pkcs12KeyStore.getPrivateKey(leafPrivateKeyAlias, password);
```
The following examples show how to retrieve a certificate from a PKCS #12 key store, given the storage alias name of its associated private key entry. The retrieved certificate will be in PEM format.

```javascript
var rootCertificatePem = pkcs12KeyStore.getCertificate(rootPrivateKeyAlias);
var leafCertificatePem = pkcs12KeyStore.getCertificate(leafPrivateKeyAlias);
```
The following examples show how to retrieve a certificate from a PKCS #12 key store, given its subject common name. The retrieved certificate will be in PEM format.

```javascript
var rootCertificatePem = pkcs12KeyStore.getCertificateBySubject(rootSubjectCommonName);
var leafCertificatePem = pkcs12KeyStore.getCertificateBySubject(leafSubjectCommonName);
```
The following examples show how to retrieve a certificate chain from a PKCS #12 key store, given the storage alias name of its associated private key entry. The certificates in a retrieved chain will be in PEM format. In these examples, the individual certificates are then extracted from the given chain. The certificates in a given chain are in order of increasing authority.

```javascript
var rootCertificateChain = pkcs12KeyStore.getCertificateChain(rootPrivateKeyAlias);
var rootCertificatePem = rootCertificateChain[0];

var leafCertificateChain = pkcs12KeyStore.getCertificateChain(leafPrivateKeyAlias);
var leafCertificatePem = leafCertificateChain[0];
var rootCertificatePem = leafCertificateChain[1];
```
## How to retrieve the contents of a Scytl key store

The Scytl key store differs from the PKCS #12 key store in the following respects:

* It provides for the storage of secret and ElGamal private keys, in addition to private keys and certificates.

* Its storage passwords are derived from the passwords provided as input, using a PBKDF algorithm, salt and a number of iterations.

The underlying key store used to store private keys and certificates in a Scytl key store is a PKCS #12 key store. The derived passwords are stored in a cache so that they only need to be generated once per key store instance.

The following example shows how to retrieve a private key from a Scytl key store, given the key's storage alias name and pre-derivation storage password. The retrieved private key will be in PEM format.

```javascript
var privateKeyPem = scytlKeyStore.getPrivateKey(privateKeyAlias, password);
```
The following example shows how to retrieve a certificate from a Scytl key store, given the storage alias name of its associated private key entry. The retrieved certificate will be in PEM format.

```javascript
var certificatePem = scytlKeyStore.getCertificate(privateKeyAlias);
```
The following example shows how to retrieve a certificate from a Scytl key store, given its subject common name. The retrieved certificate will be in PEM format.

```javascript
var certificatePem = scytlKeyStore.getCertificateBySubject(subjectCommonName);
```
The following example shows how to retrieve a certificate chain from a Scytl key store, given the storage alias name of its associated private key entry. The certificates in a retrieved chain will be in PEM format. In this example, the individual certificates are then extracted from the given chain. The certificates in a given chain are in order of increasing authority.

```javascript
var leafCertificateChain = scytlKeyStore.getCertificateChain(leafPrivateKeyAlias);
var leafCertificatePem = leafCertificateChain[0];
var rootCertificatePem = leafCertificateChain[1];
```
The following example shows how to retrieve a secret key from a Scytl key store, given the key's storage alias name and pre-derivation storage password.

```javascript
var secretKey = scytlKeyStore.getSecretKey(secretKeyAlias, password);
```

The following example shows how to retrieve an ElGamal private key from a Scytl key store, given the key's storage alias name and pre-derivation storage password. The retrieved key will be in the form of an `ElGamalPrivateKey` object.

```javascript
var elGamalPrivateKey = scytlKeyStore.getElGamalPrivateKey(elGamalPrivateKeyAlias, password);
```
