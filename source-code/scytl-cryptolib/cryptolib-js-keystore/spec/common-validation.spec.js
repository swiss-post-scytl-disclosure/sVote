/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var keyStore = require('../lib/index');

describe('The key store module that should be able to ...', function() {
  var nonObject_;
  var emptyObject_;

  beforeAll(function() {
    nonObject_ = 999;
    emptyObject_ = {};
  });

  describe('create a key store service that should be able to ..', function() {
    it('throw an error when being created, using an invalid cryptographic policy',
       function() {
         expect(function() {
           Object.create(keyStore.newService({policy: null}));
         }).toThrow();

         expect(function() {
           Object.create(keyStore.newService({policy: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(keyStore.newService({policy: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when being created, using an invalid PBKDF service object',
       function() {
         expect(function() {
           Object.create(keyStore.newService({pbkdfService: null}));
         }).toThrow();

         expect(function() {
           Object.create(keyStore.newService({pbkdfService: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(keyStore.newService({pbkdfService: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when being created, using an invalid symmetric cryptography service object',
       function() {
         expect(function() {
           Object.create(
               keyStore.newService({symmetricCryptographyService: null}));
         }).toThrow();

         expect(function() {
           Object.create(
               keyStore.newService({symmetricCryptographyService: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(keyStore.newService(
               {symmetricCryptographyService: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when being created, using an invalid ElGamal cryptography service object',
       function() {
         expect(function() {
           Object.create(
               keyStore.newService({elGamalCryptographyService: null}));
         }).toThrow();

         expect(function() {
           Object.create(
               keyStore.newService({elGamalCryptographyService: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(
               keyStore.newService({elGamalCryptographyService: emptyObject_}));
         }).toThrow();
       });
  });
});
