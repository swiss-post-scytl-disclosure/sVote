/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.binary;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests of {@link ByteArrays}
 */
public class ByteArraysTest {

    @Test
    public void testConstantTimeEquals() {
        byte[] array1 = {1, 2, 3 };
        byte[] array2 = {4, 5, 6 };
        byte[] array3 = {7, 8 };
        byte[] array4 = {};

        assertTrue(ByteArrays.constantTimeEquals(array1, array1));
        assertTrue(ByteArrays.constantTimeEquals(array4, array4));
        assertFalse(ByteArrays.constantTimeEquals(array1, array2));
        assertFalse(ByteArrays.constantTimeEquals(array1, array3));
        assertFalse(ByteArrays.constantTimeEquals(array1, null));
        assertFalse(ByteArrays.constantTimeEquals(null, array1));
        assertFalse(ByteArrays.constantTimeEquals(null, null));
    }

    @Test
    public void testConcatenateTwo() {
        byte[] array1 = {1, 2, 3 };
        byte[] array2 = {4, 5, };
        byte[] array3 = {};

        assertArrayEquals(new byte[] {1, 2, 3, 4, 5 },
            ByteArrays.concatenate(array1, array2));
        assertArrayEquals(new byte[] {1, 2, 3 },
            ByteArrays.concatenate(array1, array3));
        assertArrayEquals(new byte[0],
            ByteArrays.concatenate(array3, array3));
    }

    @Test
    public void testConcatenateMoreThanTwo() {
        byte[] array1 = {1, 2, 3 };
        byte[] array2 = {4, 5, 6 };
        byte[] array3 = {7, 8 };
        byte[] array4 = {};
        assertArrayEquals(new byte[] {1, 2, 3, 4, 5, 6, 7, 8 },
            ByteArrays.concatenate(array1, array2, array3, array4));
    }
}
