/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;

/**
 * Tests of {@link com.scytl.cryptolib.api.exceptions.CryptoLibException}.
 */
public class CryptoLibExceptionTest {

    private static final String INVALID_CAUSE = "The cause should be: ";

    private static final String INVALID_MESSAGE =
        "The message should be: ";

    @Test
    public void testMessageConstructor() throws Exception {
        String expectedMessage = "errorMessage";

        CryptoLibException e = new CryptoLibException(expectedMessage);

        Assert.assertEquals(INVALID_MESSAGE, expectedMessage,
            e.getMessage());
    }

    @Test
    public void testCauseConstructor() throws Exception {
        Throwable expectedCause = new IllegalArgumentException();

        CryptoLibException e = new CryptoLibException(expectedCause);

        Assert.assertEquals(INVALID_CAUSE, expectedCause, e.getCause());
    }

    @Test
    public void testMessageAndCauseConstructor() throws Exception {
        String expectedMessage = "errorMessage";
        Throwable expectedCause = new IllegalArgumentException();

        CryptoLibException e =
            new CryptoLibException(expectedMessage, expectedCause);

        Assert.assertEquals(INVALID_MESSAGE, expectedMessage,
            e.getMessage());
        Assert.assertEquals(INVALID_CAUSE, expectedCause, e.getCause());
    }

}
