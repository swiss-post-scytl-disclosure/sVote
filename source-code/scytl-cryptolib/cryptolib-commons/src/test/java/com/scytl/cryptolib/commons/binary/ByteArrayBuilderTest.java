/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.binary;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

/**
 * Tests of {@link ByteArrayBuilder}.
 */
public class ByteArrayBuilderTest {
    @Test
    public void testBuild() {
        ByteArrayBuilder builder = new ByteArrayBuilder();
        builder.append((byte) 0);
        builder.append(new byte[] {1, 2 });
        builder.append(new byte[0]);
        builder.append(new byte[] {3, 4, 5, 6 }, 1, 2);
        assertArrayEquals(new byte[] {0, 1, 2, 4, 5 }, builder.build());
    }

    @Test
    public void testBuildEmpty() {
        assertArrayEquals(new byte[0], new ByteArrayBuilder().build());
    }
}
