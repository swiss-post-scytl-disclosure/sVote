/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.concurrent;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import com.scytl.cryptolib.api.services.ServiceFactory;

/**
 * This class creates a pool object factory from a service factory in order to
 * fulfill the pool.
 * 
 * @param <T>
 *            The type of objects created by this factory.
 */
class BasePooledServiceFactory<T> extends BasePooledObjectFactory<T> {
    private final ServiceFactory<T> _factory;

    /**
     * Constructor that stores the given factory in order to create objects pool
     * ready.
     * 
     * @param factory
     *            Factory for creating an objects pool.
     */
    public BasePooledServiceFactory(final ServiceFactory<T> factory) {
        _factory = factory;
    }

    @Override
    public T create() throws Exception {
        return _factory.create();
    }

    @Override
    public PooledObject<T> wrap(final T obj) {
        return new DefaultPooledObject<T>(obj);
    }

}
