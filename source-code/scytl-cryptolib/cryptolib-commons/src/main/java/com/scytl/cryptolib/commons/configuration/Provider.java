/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.configuration;

/**
 * Enum which defines the eligible providers.
 */
public enum Provider {
    BOUNCY_CASTLE("BC"), SUN("SUN"), SUN_JCE("SunJCE"), SUN_JSSE("SunJSSE"), SUN_RSA_SIGN(
            "SunRsaSign"), SUN_MSCAPI("SunMSCAPI"), XML_DSIG("XMLDSig"), OPENSSL("OpenSSL"), DEFAULT;

    private String _providerName;

    private Provider() {
    }

    private Provider(final String providerName) {
        _providerName = providerName;
    }

    public String getProviderName() {
        return _providerName;
    }
}
