/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.utils.serialization;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Base64;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * A JSON serializer that has been customized so that objects of type BigInteger
 * are always serialized into Base64 encoded strings, in order to be
 * deserializable via JavaScript.
 */
public class BigIntegerSerializer extends JsonSerializer<BigInteger> {

    @Override
    public void serialize(final BigInteger value,
            final JsonGenerator generator,
            final SerializerProvider serializers)
            throws IOException {

        String encodedValue = Base64.getEncoder().encodeToString(value.toByteArray());
        generator.writeString(encodedValue);
    }

    @Override
    public Class<BigInteger> handledType() {

        return BigInteger.class;
    }
}
