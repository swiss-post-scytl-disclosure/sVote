/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.utils.system;

/**
 * Represents a type of operating system.
 * <P>
 * In this enum, all operating system are viewed as being of one of the
 * following types:
 * <ol>
 * <li>LINUX.</li>
 * <li>WINDOWS.</li>
 * <li>OTHER.</li>
 * </ol>
 */
public enum OperatingSystem {
    LINUX, WINDOWS, OTHER;
    
    private static OperatingSystem current = OTHER;
    static {
        String name = System.getProperty("os.name");
        if (name != null) {
            if (name.startsWith("Linux") || name.startsWith("LINUX")) {
                current = LINUX;
            } else if (name.startsWith("Windows")) {
                current = WINDOWS;
            }
        }
    }
    
    /**
     * Returns the current operation system.
     * 
     * @return the current operation system.
     */
    public static OperatingSystem current() {
        return current;
    }
    
    /**
     * Returns whether the operation system is the current one.
     * @return
     */
    public final boolean isCurrent() {
        return this == current();
    }
}
