/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.utils.serialization;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Object which can be serialized to JSON format.
 * <p>
 * Classes implementing this interface
 * <ul>
 * <li>should provide a complimentary static method {@code fromJson} for
 * creating instances from given JSON strings.</li>
 * <li>must guarantee that instances returned by {@code fromJson} are valid and
 * fully initialized.</li>
 * <li>can be used directly with Jackson FasterXML library for both
 * serialization and deserialization.</li>
 * <li>must guarantee that instances deserialized by Jackson FasterXML are valid
 * and fully initialized.</li>
 * </ul>
 */
public interface JsonSerializable {
    /**
     * Serializes the instance into JSON format.
     * 
     * @return the JSON string
     * @throws GeneralCryptoLibException
     *             failed to serialize the instance.
     */
    String toJson() throws GeneralCryptoLibException;
}
