/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.constants;

/**
 * Declaration of constants common to all modules.
 */
public final class Constants {

    /**
     * This class cannot be instantiated.
     */
    private Constants() {
    }

    /**
     * The relative path of the properties file that should be used for the
     * cryptoLib policy configuration.
     */
    public static final String CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH =
        "properties/cryptolibPolicy.properties";
}
