/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.commons.concurrent;

import com.scytl.cryptolib.api.services.ServiceFactory;

/**
 * Abstract class for factories.
 * 
 * @param <T>
 *            This parameter sets the class of objects that this factory is
 *            going to create.
 */
public abstract class BaseFactory<T> implements ServiceFactory<T> {

    private final Class<T> _objectType;

    /**
     * Creates an instance of factory what will create objects of specified
     * {@code objectType}. This factory will create services configured with
     * default values.
     * 
     * @param objectType
     *            Interface of the objects created by this factory.
     */
    protected BaseFactory(final Class<T> objectType) {
        _objectType = objectType;
    }

    @Override
    public final Class<T> getCreatedObjectType() {
        return _objectType;
    }

}
