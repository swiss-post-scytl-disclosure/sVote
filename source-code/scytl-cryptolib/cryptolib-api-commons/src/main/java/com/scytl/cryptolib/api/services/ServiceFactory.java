/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.api.services;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Interface that any factory of services should implement. All the objects
 * created by it should be equivalent.
 * 
 * @param <T>
 *            The type of service to be created.
 */
public interface ServiceFactory<T> {

    /**
     * Creates a new instance of the service.
     * 
     * @return A new instance of the service.
     * @throws GeneralCryptoLibException
     *             if the service creation fails.
     */
    T create() throws GeneralCryptoLibException;

    /**
     * Returns the type of service which this factory creates.
     * 
     * @return The type of service which this factory creates.
     */
    Class<T> getCreatedObjectType();
}
