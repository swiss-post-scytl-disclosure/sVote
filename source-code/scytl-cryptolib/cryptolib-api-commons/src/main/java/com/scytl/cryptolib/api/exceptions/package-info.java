/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Provides exceptions that can be thrown from Crypto Lib.
 */
package com.scytl.cryptolib.api.exceptions;

