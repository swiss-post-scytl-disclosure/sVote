/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = EncryptedSecretKeyPair;

/**
 * @class EncryptedSecretKeyPair
 * @classdesc Encapsulates the asymmetric encryption of the pair of secret keys
 *            used to generate and open a digital envelope. The key pair
 *            consists of an encryption key to symmetrically encrypt the
 *            envelope's data and a MAC key to verify the envelope's integrity.
 *            The keys are bitwise concatenated before encrypting. This object
 *            is instantiated internally by the methods {@link
 *            DigitalEnvelopeGenerator.generate} and
 *            {@link DigitalEnvelopeOpener.open}.
 * @property {Uint8Array} encryptedKeyPair The asymmetrically encrypted secret
 *           key pair.
 * @property {string} publicKey The public key used to asymmetrically encrypt
 *           the secret key pair, in PEM format.
 */
function EncryptedSecretKeyPair(encryptedKeyPair, publicKey) {
  return Object.freeze(
      {encryptedKeyPair: encryptedKeyPair, publicKey: publicKey});
}
