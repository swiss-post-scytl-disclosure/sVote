#
# Copyright 2018 Scytl Secure Electronic Voting SA
#
# All rights reserved
#
# See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
#

# Key defines: PRNG for LINUX systems
# Options: [NATIVE_PRNG_SUN]
primitives.securerandom.linux=

# Key defines: PRNG for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
primitives.securerandom.windows=

# Key defines: algorithm - hash - provider
# Options: [ MGF1_SHA256_BC | MGF1_SHA256_DEFAULT]
primitives.kdfderivation=

# Key defines: algorithm - hash - provider - keyLength
# Options: [PBKDF2_SHA256_BC_KL128 | PBKDF2_SHA256_BC_KL256]
primitives.pbkdfderivation=

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
primitives.pbkdfderivation.securerandom.linux=

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
primitives.pbkdfderivation.securerandom.windows=

# Key defines: algorithm - provider
# Options: [SHA256_SUN | SHA256_BC]
primitives.messagedigest=

# Key defines: algorithm - key size - provider
# Options: [AES_128_BC | AES_128_SUNJCE]
symmetric.encryptionsecretkey=

# Key defines: algorithm - key size - provider
# Options: [HMACwithSHA256_256]
symmetric.macsecretkey=

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
symmetric.key.securerandom.linux=

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
symmetric.key.securerandom.windows=

# Key defines: algorithm - provider
# Options: [HMACwithSHA256_SUN, HMACwithSHA256_BC]
symmetric.mac=

# Key defines: algorithm - initialization vector size - authentication tag size - provider
# Options: [AESwithGCMandNOPADDING_96_128_BC]
symmetric.cipher=

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_SUN_RSA_SIGN | RSA_3072_F4_SUN_RSA_SIGN | RSA_4096_F4_SUN_RSA_SIGN | RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC | RSA_2048_OPENSSL_RSA_SIGN | RSA_3072_OPENSSL_RSA_SIGN | RSA_4096_OPENSSL_RSA_SIGN]
asymmetric.signingkeypair=

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC | RSA_2048_OPENSSL_RSA_SIGN | RSA_3072_OPENSSL_RSA_SIGN | RSA_4096_OPENSSL_RSA_SIGN]
asymmetric.encryptionkeypair=

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
asymmetric.keypair.securerandom.linux=

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
asymmetric.keypair.securerandom.windows=

# Key defines: algorithm - provider
# Options: [RSAwithECBandOAEPWITHSHA256ANDMGF1PADDING_SUN, RSAwithECBandOAEPWITHSHA256ANDMGF1PADDING_BC, RSAwithRSAKEMANDKDF2ANDSHA256_BC]
asymmetric.cipher=

# Key defines: PRNG for LINUX systems
# Options: [NATIVE_PRNG_SUN]
asymmetric.cipher.securerandom.linux=

# Key defines: PRNG for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
asymmetric.cipher.securerandom.windows=

# Key defines: algorithm - key size - provider
# Options: [AES_128_BC | AES_128_SUNJCE]
asymmetric.cipher.symmetric.encryptionsecretkey=

# Key defines: algorithm - initialization vector size - authentication tag size - provider
# Options: [AESwithGCMandNOPADDING_96_128_BC]
asymmetric.cipher.symmetric.cipher=

# Key defines: algorithm - padding md - padding mgf - padding mgf md - padding salt size - padding trailer field - provider 
# Options: [SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC]
asymmetric.signer=

# Key defines: signature algorithm - digest algorithm - transform method - canonicalization method - mechanism type - provider 
# Options: [SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG]
asymmetric.xml.signer=

# Key defines: hash and algorithm - provider
# Options: [SHA256_WITH_RSA_BC]
certificates.x509certificate=

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
certificates.x509certificate.securerandom.linux=

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
certificates.x509certificate.securerandom.windows=

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
elgamal.securerandom.linux=

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
elgamal.securerandom.windows=

# Key defines: provider
# Options: [ SUN | BC | DEFAULT ]
stores.keystore=

# Key defines: type - provider
# Options: [PKCS12_BC | PKCS12_SUN_JSSE | PKCS12_DEFAULT]
scytl.keystore.p12=

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
proofs.securerandom.linux=

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
proofs.securerandom.windows=

# Key defines: character set to be supported the proofs hash builder
# Options: [UTF8]
proofs.hashbuilder.charset=

# Key defines: hash to be used by the proofs hash builder
# Options: [SHA256_SUN | SHA256_BC | SHA256_DEFAULT | SHA512_224_BC | SHA512_224_DEFAULT]
proofs.hashbuilder.messagedigest=

# Key defines: PRNG for LINUX systems
# Options: [NATIVE_PRNG_SUN]
digital.envelope.securerandom.linux=

# Key defines: PRNG for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
digital.envelope.securerandom.windows=

# Key defines: algorithm - key size - provider
# Options: [AES_128_BC | AES_128_SUNJCE]
digital.envelope.symmetric.encryptionsecretkey=

# Key defines: algorithm - key size - provider
# Options: [HMACwithSHA256_256]
digital.envelope.symmetric.macsecretkey=

# Key defines: algorithm - provider
# Options: [HMACwithSHA256_SUN, HMACwithSHA256_BC, HMACwithSHA256_DEFAULT]
digital.envelope.symmetric.mac=

# Key defines: algorithm - initialization vector size - authentication tag size - provider
# Options: [AESwithGCMandNOPADDING_96_128_BC, AESwithGCMandNOPADDING_96_128_DEFAULT]
digital.envelope.symmetric.cipher=

# Key defines: algorithm - provider
# Options: [RSAwithRSAKEMANDKDF1ANDSHA256_BC]
digital.envelope.asymmetric.cipher=

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_SUN_RSA_SIGN | RSA_3072_F4_SUN_RSA_SIGN | RSA_4096_F4_SUN_RSA_SIGN | RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC]
digital.envelope.asymmetric.signingkeypair=

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_SUN_RSA_SIGN | RSA_3072_F4_SUN_RSA_SIGN | RSA_4096_F4_SUN_RSA_SIGN | RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC]
digital.envelope.asymmetric.encryptionkeypair=
