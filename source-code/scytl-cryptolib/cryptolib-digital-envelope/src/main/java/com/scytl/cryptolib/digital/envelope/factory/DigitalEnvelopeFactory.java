/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.factory;

import com.scytl.cryptolib.digital.envelope.configuration.DigitalEnvelopePolicy;

/**
 * A factory class for generating or opening digital envelopes.
 */
public class DigitalEnvelopeFactory {

    private final DigitalEnvelopePolicy _digitalEnvelopePolicy;

    /**
     * Constructs a digital envelope factory, using the digital envelope policy
     * provided as input.
     * 
     * @param digitalEnvelopePolicy
     *            the digital envelope policy, as an object of type
     *            {@link DigitalEnvelopePolicy} , to be used to configure this
     *            digital envelope factory.
     *            <P>
     *            NOTE: The provided {@code DigitalEnvelopePolicy} object should
     *            be immutable. If this is the case, then the entire
     *            {@code DigitalEnvelopeFactory} object is thread safe.
     */
    public DigitalEnvelopeFactory(
            final DigitalEnvelopePolicy digitalEnvelopePolicy) {

        _digitalEnvelopePolicy = digitalEnvelopePolicy;
    }

    /**
     * Creates a new digital envelope generator according to the given digital
     * envelope policy.
     * 
     * @return the digital envelope generator, as an object of type
     *         {@link CryptoDigitalEnvelopeGenerator}.
     */
    public CryptoDigitalEnvelopeGenerator createGenerator() {

        return new CryptoDigitalEnvelopeGenerator(_digitalEnvelopePolicy);
    }

    /**
     * Creates a new digital envelope opener according to the given digital
     * envelope policy.
     * 
     * @return the digital envelope opener, as an object of type
     *         {@link CryptoDigitalEnvelopeOpener}.
     */
    public CryptoDigitalEnvelopeOpener createOpener() {

        return new CryptoDigitalEnvelopeOpener(_digitalEnvelopePolicy);
    }
}
