/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.configuration;

import com.scytl.cryptolib.asymmetric.cipher.configuration.AsymmetricCipherPolicy;
import com.scytl.cryptolib.asymmetric.keypair.configuration.KeyPairPolicy;
import com.scytl.cryptolib.symmetric.cipher.configuration.SymmetricCipherPolicy;
import com.scytl.cryptolib.symmetric.key.configuration.SecretKeyPolicy;
import com.scytl.cryptolib.symmetric.key.configuration.SymmetricKeyPolicy;
import com.scytl.cryptolib.symmetric.mac.configuration.MacPolicy;

/**
 * Configuration policy for generating and opening digital envelopes.
 * <P>
 * Implementations of this interface must be immutable.
 */
public interface DigitalEnvelopePolicy extends SecretKeyPolicy,
        KeyPairPolicy, SymmetricKeyPolicy, MacPolicy,
        SymmetricCipherPolicy, AsymmetricCipherPolicy {
}
