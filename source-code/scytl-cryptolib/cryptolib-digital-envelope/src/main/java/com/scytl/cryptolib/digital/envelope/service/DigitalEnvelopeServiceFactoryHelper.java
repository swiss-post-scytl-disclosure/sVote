/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.service;

import com.scytl.cryptolib.api.digital.envelope.DigitalEnvelopeServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.ServiceFactoryHelper;

/**
 * This class sets up default factories of {@link DigitalEnvelopeServiceAPI}
 * objects.
 */
public final class DigitalEnvelopeServiceFactoryHelper {

    /**
     * This is a helper class. It cannot be instantiated.
     */
    private DigitalEnvelopeServiceFactoryHelper() {
    }

    /**
     * Retrieves a new factory of non thread-safe services.
     * <p>
     * Default factory is {@link BasicDigitalEnvelopeServiceFactory}.
     * 
     * @param params
     *            list of parameters used in the creation of the default
     *            factory.
     * @return the new factory.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<DigitalEnvelopeServiceAPI> getInstance(
            final Object... params) {
        return ServiceFactoryHelper.get(
            BasicDigitalEnvelopeServiceFactory.class, params);

    }

    /**
     * Retrieves a new factory of thread-safe services.
     * <p>
     * Default factory is {link {@link PollingDigitalEnvelopeServiceFactory}.
     * 
     * @param params
     *            list of parameters used in the creation of the default
     *            factory.
     * @return the new factory of thread-safe services.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<DigitalEnvelopeServiceAPI> getFactoryOfThreadSafeServices(
            final Object... params) {
        return ServiceFactoryHelper.get(
            PollingDigitalEnvelopeServiceFactory.class, params);
    }
}
