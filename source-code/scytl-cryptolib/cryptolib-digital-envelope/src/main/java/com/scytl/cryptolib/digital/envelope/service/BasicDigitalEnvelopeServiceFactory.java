/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.service;

import com.scytl.cryptolib.api.digital.envelope.DigitalEnvelopeServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link DigitalEnvelopeService} objects which are non thread-safe.
 */
public class BasicDigitalEnvelopeServiceFactory extends
        PropertiesConfiguredFactory<DigitalEnvelopeServiceAPI> {

    /**
     * Creates an instance of factory that will create services configured with
     * default values.
     */
    public BasicDigitalEnvelopeServiceFactory() {
        super(DigitalEnvelopeServiceAPI.class);
    }

    /**
     * This factory will create services configured with the given configuration
     * file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     */
    public BasicDigitalEnvelopeServiceFactory(final String path) {
        super(DigitalEnvelopeServiceAPI.class, path);
    }

    @Override
    protected DigitalEnvelopeServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new DigitalEnvelopeService(_path);
    }

    @Override
    protected DigitalEnvelopeServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new DigitalEnvelopeService();
    }
}
