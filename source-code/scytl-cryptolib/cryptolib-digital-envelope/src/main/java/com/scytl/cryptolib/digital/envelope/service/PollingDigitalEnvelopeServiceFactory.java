/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.service;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.digital.envelope.DigitalEnvelopeServiceAPI;
import com.scytl.cryptolib.commons.concurrent.PooledProxiedServiceFactory;

/**
 * This factory creates thread-safe services. Thread-safe services proxy all
 * requests to a pool of non thread-safe service instances.
 */
public class PollingDigitalEnvelopeServiceFactory extends
        PooledProxiedServiceFactory<DigitalEnvelopeServiceAPI> {

    /**
     * Constructor that uses default values.
     */
    public PollingDigitalEnvelopeServiceFactory() {
        super(new BasicDigitalEnvelopeServiceFactory(),
            new GenericObjectPoolConfig());
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param path
     *            the path where the properties file is located.
     * @param poolConfig
     *            the pool configuration.
     */
    public PollingDigitalEnvelopeServiceFactory(final String path,
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicDigitalEnvelopeServiceFactory(path), poolConfig);
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingDigitalEnvelopeServiceFactory(
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicDigitalEnvelopeServiceFactory(), poolConfig);
    }
}
