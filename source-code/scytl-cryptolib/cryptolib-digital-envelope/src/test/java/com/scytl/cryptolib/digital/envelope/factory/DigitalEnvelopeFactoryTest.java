/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.factory;

import static mockit.Deencapsulation.getField;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.digital.envelope.configuration.DigitalEnvelopePolicy;
import com.scytl.cryptolib.digital.envelope.configuration.DigitalEnvelopePolicyFromProperties;

public class DigitalEnvelopeFactoryTest {

    private static DigitalEnvelopePolicy _digitalEnvelopePolicy;

    private static DigitalEnvelopePolicy _digitalEnvelopePolicyFromFactory;

    @BeforeClass
    public static void setUp() {

        Security.addProvider(new BouncyCastleProvider());

        _digitalEnvelopePolicy =
            new DigitalEnvelopePolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);

        DigitalEnvelopeFactory digitalEnvelopeFactory =
            new DigitalEnvelopeFactory(_digitalEnvelopePolicy);
        _digitalEnvelopePolicyFromFactory =
            getField(digitalEnvelopeFactory, "_digitalEnvelopePolicy");

    }

    @AfterClass
    public static void removeBouncyCastleProvider() {

        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Test
    public void whenCreateDigitalEnvelopeFactorySecureRandomPolicyIsExpected() {

        Assert.assertEquals(_digitalEnvelopePolicy
            .getSecureRandomAlgorithmAndProvider(),
            _digitalEnvelopePolicyFromFactory
                .getSecureRandomAlgorithmAndProvider());
    }

    @Test
    public void whenCreateDigitalEnvelopeFactorySecretKeyPolicyIsExpected() {

        Assert.assertEquals(
            _digitalEnvelopePolicy.getSecretKeyAlgorithmAndSpec(),
            _digitalEnvelopePolicyFromFactory.getSecretKeyAlgorithmAndSpec());
    }

    @Test
    public void whenCreateDigitalEnvelopeFactoryHmacSecretKeyPolicyIsExpected() {

        Assert.assertEquals(_digitalEnvelopePolicy
            .getHmacSecretKeyAlgorithmAndSpec(),
            _digitalEnvelopePolicyFromFactory
                .getHmacSecretKeyAlgorithmAndSpec());
    }

    @Test
    public void whenCreateDigitalEnvelopeFactoryMacPolicyIsExpected() {

        Assert.assertEquals(
            _digitalEnvelopePolicy.getMacAlgorithmAndProvider(),
            _digitalEnvelopePolicyFromFactory.getMacAlgorithmAndProvider());
    }

    @Test
    public void whenCreateDigitalEnvelopeFactorySymmetricCipherPolicyIsExpected() {

        Assert.assertEquals(_digitalEnvelopePolicy
            .getSymmetricCipherAlgorithmAndSpec(),
            _digitalEnvelopePolicyFromFactory
                .getSymmetricCipherAlgorithmAndSpec());
    }

    @Test
    public void whenCreateDigitalEnvelopeFactoryAsymmetricCipherPolicyIsExpected() {

        Assert.assertEquals(_digitalEnvelopePolicy
            .getAsymmetricCipherAlgorithmAndSpec(),
            _digitalEnvelopePolicyFromFactory
                .getAsymmetricCipherAlgorithmAndSpec());
    }
}
