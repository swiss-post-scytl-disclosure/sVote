/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = SecretKeyGenerator;

/**
 * @class SecretKeyGenerator
 * @classdesc The secret key generator API. To instantiate this object, use the
 *            method {@link SymmetricCryptographyService.newKeyGenerator}.
 * @hideconstructor
 * @param {Policy}
 *            policy The cryptographic policy to use.
 * @param {SecureRandomService}
 *            secureRandomService The secure random service to use.
 */
function SecretKeyGenerator(policy, secureRandomService) {
  var randomGenerator_ = secureRandomService.newRandomGenerator();

  /**
   * Generates a secret key to use for symmetric encryption.
   *
   * @function nextEncryptionKey
   * @memberof SecretKeyGenerator
   * @returns {Uint8Array} The encryption secret key.
   */
  this.nextEncryptionKey = function() {
    return randomGenerator_.nextBytes(
        policy.symmetric.secretKey.encryption.lengthBytes);
  };

  /**
   * Generates a secret key to use for MAC generation.
   *
   * @function nextMacKey
   * @memberof SecretKeyGenerator
   * @returns {Uint8Array} The MAC secret key.
   */
  this.nextMacKey = function() {
    return randomGenerator_.nextBytes(
        policy.symmetric.secretKey.mac.lengthBytes);
  };
}
