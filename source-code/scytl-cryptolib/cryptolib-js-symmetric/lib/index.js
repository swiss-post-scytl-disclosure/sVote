/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var SymmetricCryptographyService = require('./service');
var validator = require('./input-validator');

module.exports = {
  /**
   * Creates a new SymmetricCryptographyService object, which encapsulates a
   * symmetric cryptography service.
   *
   * @function newService
   * @global
   * @param {Object}
   *            [options] An object containing optional arguments.
   * @param {Policy}
   *            [options.policy=Default policy] The cryptographic policy to
   *            use.
   * @param {SecureRandomService}
   *            [options.secureRandomService=Created internally] The secure
   *            random service to use.
   * @returns {SymmetricCryptographyService} The new
   *          SymmetricCryptographyService object.
   * @throws {Error}
   *             If the input data validation fails.
   * @example <caption> How to use a cryptographic policy that sets the key
   *          length of the symmetric cipher to 32 bytes</caption>
   *
   * var cryptoPolicy = require('scytl-cryptopolicy');
   * var symmetric = require('scytl-symmetric');
   *
   * var myPolicy = cryptoPolicy.newInstance();
   *
   * myPolicy.symmetric.cipher.algorithm.AES_GCM.keyLengthBytes =
   * cryptoPolicy.options.symmetric.cipher.algorithm.AES_GCM.keyLengthBytes.KL_32;
   *
   * var symmetricService = symmetric.newService({policy: myPolicy});
   */
  newService: function(options) {
    checkData(options);

    return new SymmetricCryptographyService(options);
  }
};

function checkData(options) {
  options = options || {};

  if (typeof options.policy !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.policy,
        'Cryptographic policy provided to symmetric cryptography service');
  }

  if (typeof options.secureRandomService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.secureRandomService,
        'Secure random service object provided to symmetric cryptography service');
  }
}
