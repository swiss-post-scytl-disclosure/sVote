/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var SecretKeyGenerator = require('./key-generator');
var SymmetricCipher = require('./cipher');
var MacHandler = require('./mac-handler');
var cryptoPolicy = require('scytl-cryptopolicy');
var secureRandom = require('scytl-securerandom');

module.exports = SymmetricCryptographyService;

/**
 * @class SymmetricCryptographyService
 * @classdesc The symmetric cryptography service API. To instantiate this
 *            object, use the method {@link newService}.
 * @hideconstructor
 * @param {Object}
 *            [options] An object containing optional arguments.
 * @param {Policy}
 *            [options.policy=Default policy] The cryptographic policy to use.
 * @param {SecureRandomService}
 *            [options.secureRandomService=Created internally] The secure random
 *            service to use.
 */
function SymmetricCryptographyService(options) {
  options = options || {};

  var policy_;
  if (options.policy) {
    policy_ = options.policy;
  } else {
    policy_ = cryptoPolicy.newInstance();
  }

  var secureRandomService_;
  if (options.secureRandomService) {
    secureRandomService_ = options.secureRandomService;
  } else {
    secureRandomService_ = secureRandom.newService();
  }

  /**
   * Creates a new SecretKeyGenerator object for generating secret keys.
   *
   * @function newKeyGenerator
   * @memberof SymmetricCryptographyService
   * @returns {SecretKeyGenerator} The new SecretKeyGenerator object.
   */
  this.newKeyGenerator = function() {
    return new SecretKeyGenerator(policy_, secureRandomService_);
  };

  /**
   * Creates a new SymmetricCipher object for symmetrically encrypting or
   * decrypting data. It must be initialized with a secret key.
   *
   * @function newCipher
   * @memberof SymmetricCryptographyService
   * @returns {SymmetricCipher} The new SymmetricCipher object.
   */
  this.newCipher = function() {
    return new SymmetricCipher(policy_, secureRandomService_);
  };

  /**
   * Creates a new MacHandler object for generating or verifying a MAC. It
   * must be initialized with a secret key.
   *
   * @function newMacHandler
   * @memberof SymmetricCryptographyService
   * @returns {MacHandler} The new MacHandler object.
   */
  this.newMacHandler = function() {
    return new MacHandler(policy_);
  };
}
