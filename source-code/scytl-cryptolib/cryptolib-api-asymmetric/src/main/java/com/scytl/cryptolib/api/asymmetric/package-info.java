/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Provides APIs to work with asymmetric encryption.
 */
package com.scytl.cryptolib.api.asymmetric;
