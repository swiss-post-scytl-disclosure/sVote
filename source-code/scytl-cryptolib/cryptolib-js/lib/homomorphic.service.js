/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
cryptolib.modules.homomorphic = cryptolib.modules.homomorphic || {};

cryptolib.modules.homomorphic.service = function(box) {
  'use strict';

  box.homomorphic = box.homomorphic || {};
  /**
   * A module that provides high-level ElGamal cryptographic services.
   *
   * @exports homomorphic/service
   */
  box.homomorphic.service = {};

  var elGamalFactory;
  var keyFactory;

  cryptolib('homomorphic.cipher', 'homomorphic.keypair', function(box) {

    elGamalFactory = new box.homomorphic.cipher.factory.ElGamalCipherFactory();
    keyFactory = new box.homomorphic.keypair.factory.KeyFactory();
  });

  /**
   * @function createElGamalKeyPair
   * @param elGamalPublicKey
   *            {Object} an ElGamal public key.
   * @param elGamalPrivateKey
   *            {Object} an ElGamal private key.
   *
   * @returns {Object} an object of ElGamalKeyPair.
   */
  box.homomorphic.service.createElGamalKeyPair = function(
      elGamalPublicKey, elGamalPrivateKey) {
    return keyFactory.createKeyPair(elGamalPublicKey, elGamalPrivateKey);
  };

  /**
   * @function createElGamalPublicKey
   * @param serializedElGamalPublicKeyB64
   *            {String] a serialized ElGamalPublicKey, in base 64 encoded
   *            format.
   *
   * @returns {Object} a ElGamalPublicKey, as an object.
   */
  box.homomorphic.service.createElGamalPublicKey = function(
      serializedElGamalPublicKeyB64) {
    return keyFactory.createPublicKey(serializedElGamalPublicKeyB64);
  };

  /**
   * @function createElGamalPrivateKey
   * @param serializedElGamalPrivateKeyB64
   *            {String} a serialized ElGamalPrivateKey, in base 64 encoded
   *            format.
   *
   * @returns {Object} a ElGamalPrivateKey, as an object.
   */
  box.homomorphic.service.createElGamalPrivateKey = function(
      serializedElGamalPrivateKeyB64) {
    return keyFactory.createPrivateKey(serializedElGamalPrivateKeyB64);
  };

  /**
   * @function createEncrypter
   * @param elGamalPublicKey
   *            {Object} a public key, as an ElGamalPublicKey object.
   * @param [cryptoRandomInteger] {Object} an optional parameter to set the
   * source of randomness for the generation of the ElGamal encryption
   * exponent. If not provided, the source will be created internally.
   *
   * @returns {Object} an ElGamalEncrypter.
   */
  box.homomorphic.service.createEncrypter = function(
      elGamalPublicKey, cryptoRandomInteger) {
    return elGamalFactory.createEncrypter(
        elGamalPublicKey, cryptoRandomInteger);
  };

  /**
   * @function createDecrypter
   * @param elGamalPrivateKey
   *            {Object} a private key, as an ElGamalPrivateKey object.
   *
   * @returns {Object} an ElGamalDecrypter.
   */
  box.homomorphic.service.createDecrypter = function(elGamalPrivateKey) {
    return elGamalFactory.createDecrypter(elGamalPrivateKey);
  };

  /**
   * @function createRandomDecrypter
   * @param elGamalPublicKey
   *            {Object} a public key, as an ElGamalPublicKey object.
   * @param cryptoRandomInteger {Object} the
   * source of randomness for the generation of the ElGamal encryption
   * exponent.
   * @returns {Object} an ElGamalRandomDecrypter.
   */
  box.homomorphic.service.createRandomDecrypter = function(
      elGamalPublicKey, cryptoRandomInteger) {
    return elGamalFactory.createRandomDecrypter(
        elGamalPublicKey, cryptoRandomInteger);
  };
};
