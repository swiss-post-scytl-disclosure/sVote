/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
cryptolib.modules.commons = cryptolib.modules.commons || {};

/**
 * Defines our exception wrapper.
 *
 * @param message.
 *            The string to use in the error description.
 */
cryptolib.modules.commons.exceptions = function(box) {
  'use strict';

  box.commons = box.commons || {};
  box.commons.exceptions = {};

  box.commons.exceptions.CryptoLibException = function(
      customMessage, originalMessage) {
    this.customMessage = customMessage;
    this.originalMessage = originalMessage;
  };

  box.commons.exceptions.CryptoLibException.prototype = {
    /**
     * Returns the exception message.
     *
     * @return {string} The message.
     */
    toString: function() {

      var errorMessage = 'ERROR:  ';

      if (typeof this.originalMessage !== 'undefined') {
        errorMessage += this.customMessage +
            '. Original Error Message: ' + this.originalMessage;
      } else {
        errorMessage += this.customMessage;
      }
      return errorMessage;
    }
  };
};
