/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
////////////////////////////////////////////////
//
// Functionalities added to JavaScript types
//
////////////////////////////////////////////////

/**
 * An equals method for arrays.
 *
 * @param array
 *            the array to which this array should be compared.
 * @param strict
 *            a boolean value which specifies whether or not the elements must
 *            be in the same order in both arrays. This parameter is optional,
 *            if it is not supplied, then a default value of 'true' is assigned
 *            to it.
 */
Array.prototype.equals = function(array, strict) {
  'use strict';

  if (!array) {
    return false;
  }

  if (arguments.length === 1) {
    strict = true;
  }

  if (this.length !== array.length) {
    return false;
  }

  for (var i = 0; i < this.length; i++) {
    if (this[i] instanceof Array && array[i] instanceof Array) {
      if (!this[i].equals(array[i], strict)) {
        return false;
      }
    } else if (strict && (!this[i].equals(array[i]))) {
      return false;
    } else if (!strict) {
      return this.sort().equals(array.sort(), true);
    }
  }
  return true;
};

/**
 * Allows all of the elements of one array to be added into another array.
 */
Array.prototype.addAll = function() {
  'use strict';

  for (var a = 0; a < arguments.length; a++) {
    var arr = arguments[a];
    for (var i = 0; i < arr.length; i++) {
      this.push(arr[i]);
    }
  }
};
