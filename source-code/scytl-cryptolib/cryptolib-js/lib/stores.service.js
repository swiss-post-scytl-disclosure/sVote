/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
cryptolib.modules.stores = cryptolib.modules.stores || {};

/**
 * A module that provides stores services.
 * <P>
 */
cryptolib.modules.stores.service = function(box) {
  'use strict';

  box.stores = box.stores || {};
  box.stores.service = {};

  var sks;

  cryptolib('stores.sks', function(box) {
    sks = box.stores.sks;
  });

  /**
   * @static
   *
   * @returns a Sks object.
   */
  box.stores.service.loadStore = function(store) {
    return new sks.SksReader(store);
  };
};
