/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Symmetric service...', function() {
  'use strict';

  describe('should be able to ..', function() {
    it('return a secret key for encryption', function() {
      cryptolib('symmetric.service', function(box) {
        var secretKey = box.symmetric.service.getSecretKeyForEncryption();
        expect(secretKey).toBeDefined();
      });
    });

    it('return a secret key for mac', function() {
      cryptolib('symmetric.service', function(box) {
        var secretKey = box.symmetric.service.getSecretKeyForMac();
        expect(secretKey).toBeDefined();
      });
    });

    it('encrypt and decrypt', function() {
      cryptolib('symmetric.service', 'commons.utils', function(box) {
        // generate a secret key
        var secretKeyB64 = box.symmetric.service.getSecretKeyForEncryption();

        // encrypt and decrypt data
        var converters = new box.commons.utils.Converters();
        var dataB64 = converters.base64Encode('anyString');

        var encryptedDataBase64 =
            box.symmetric.service.encrypt(secretKeyB64, dataB64);

        var decryptedDataBase64 =
            box.symmetric.service.decrypt(secretKeyB64, encryptedDataBase64);

        expect(dataB64 === decryptedDataBase64).toBeTruthy();
      });
    });

    it('compute a MAC and verify it', function() {
      cryptolib('symmetric.service', 'commons.utils', function(box) {
        // generate a secret key
        var secretKeyB64 = box.symmetric.service.getSecretKeyForMac();

        // encrypt and decrypt data
        var converters = new box.commons.utils.Converters();
        var dataB64 = converters.base64Encode('anyString');

        var macB64 = box.symmetric.service.getMac(secretKeyB64, [dataB64]);

        var verified =
            box.symmetric.service.verifyMac(macB64, secretKeyB64, [dataB64]);

        expect(verified).toBeTruthy();
      });
    });
  });

  describe('bug #90835', function() {
    it('if the original message contains spaces, they should be preserved',
       function() {
         cryptolib('symmetric.service', 'commons.utils', function(box) {
           var text = 'text with some spaces';
           var secretKeyB64 = box.symmetric.service.getSecretKeyForEncryption();

           // encrypt and decrypt data
           var converters = new box.commons.utils.Converters();
           var dataB64 = converters.base64Encode(text);

           var encryptedDataBase64 =
               box.symmetric.service.encrypt(secretKeyB64, dataB64);

           var decryptedDataBase64 =
               box.symmetric.service.decrypt(secretKeyB64, encryptedDataBase64);

           var decryptedData = converters.base64Decode(decryptedDataBase64);

           // Verify that data was successfully decrypted.
           expect(decryptedData).toEqual(text);
         });
       });

    it('if the original message contains only spaces, they should be preserved',
       function() {
         cryptolib('symmetric.service', 'commons.utils', function(box) {
           var text = '    ';
           var secretKeyB64 = box.symmetric.service.getSecretKeyForEncryption();

           // encrypt and decrypt data
           var converters = new box.commons.utils.Converters();
           var dataB64 = converters.base64Encode(text);

           var encryptedDataBase64 =
               box.symmetric.service.encrypt(secretKeyB64, dataB64);

           var decryptedDataBase64 =
               box.symmetric.service.decrypt(secretKeyB64, encryptedDataBase64);

           var decryptedData = converters.base64Decode(decryptedDataBase64);

           // Verify that data was successfully decrypted.
           expect(decryptedData).toEqual(text);
         });
       });
  });
});
