/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Primitives service...', function() {
  'use strict';

  describe('should be able to ..', function() {
    it('return crypto random bytes generator', function() {
      cryptolib('primitives.service', function(box) {

        var cryptoRandomBytes = box.primitives.service.getCryptoRandomBytes();

        expect(cryptoRandomBytes).toBeDefined();

        cryptoRandomBytes.nextRandom(10);
      });
    });

    it('return crypto random integer generator', function() {
      cryptolib('primitives.service', function(box) {

        var cryptoRandomInteger =
            box.primitives.service.getCryptoRandomInteger();

        expect(cryptoRandomInteger).toBeDefined();

        cryptoRandomInteger.nextRandom(10);
      });
    });

    it('return the message digest of a data array in Base64 encoded format.',
       function() {
         cryptolib('primitives.service', 'commons.utils', function(box) {
           var converters = new box.commons.utils.Converters();

           var dataB64 = converters.base64Encode('Ox2fUJq1gAbX');

           var arrayDataBase64 = [dataB64];

           var digested = box.primitives.service.getHash(arrayDataBase64);

           expect(digested).toBeDefined();
         });
       });

    it('return a PBKDF secret key generator.', function() {
      cryptolib('primitives.service', function(box) {
        var generator = box.primitives.service.getPbkdfSecretKeyGenerator();
        expect(generator).toBeDefined();
      });
    });
  });
});
