/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('CryptoSecretKeyGenerator ...', function() {
  'use strict';

  describe('should be able to ..', function() {
    it('generate encryption keys of the given length.', function() {
      cryptolib('symmetric.secretkey', 'commons', function(box) {

        var secretKeyFactory =
            new box.symmetric.secretkey.factory.SecretKeyFactory();

        var cryptoSecretKeyGenerator =
            secretKeyFactory.getCryptoSecretKeyGeneratorForEncryption();

        var converters = new box.commons.utils.Converters();

        // Generate key of a given length.
        var cryptoSecretKey = cryptoSecretKeyGenerator.generate();

        expect(cryptoSecretKey).toBeDefined();

        // Base64 decode encrypted data and decrypt data.
        var secretKey = converters.base64Decode(cryptoSecretKey);

        // Verify that the secret key is of length 10
        expect(
            secretKey.length ===
            box.policies.symmetric.secretkey.encryption.lengthBytes)
            .toBeTruthy();
      });
    });

    it('generate secret keys for mac of the given length.', function() {
      cryptolib('symmetric.secretkey', 'commons', function(box) {

        var secretKeyFactory =
            new box.symmetric.secretkey.factory.SecretKeyFactory();

        var cryptoSecretKeyGenerator =
            secretKeyFactory.getCryptoSecretKeyGeneratorForMac();

        var converters = new box.commons.utils.Converters();

        // Generate key of a given length.
        var cryptoSecretKey = cryptoSecretKeyGenerator.generate();

        expect(cryptoSecretKey).toBeDefined();

        // Base64 decode encrypted data and decrypt data.
        var secretKey = converters.base64Decode(cryptoSecretKey);

        // Verify that the secret key is of length 10
        expect(
            secretKey.length ===
            box.policies.symmetric.secretkey.mac.lengthBytes)
            .toBeTruthy();
      });
    });

    it('throw an error when the given length for encryption is not a number.',
       function() {
         var f = function(box) {
           var secretKeyFactory =
               new box.symmetric.secretkey.factory.SecretKeyFactory();

           var cryptoSecretKeyGenerator =
               secretKeyFactory.getCryptoSecretKeyGeneratorForEncryption();

           var thrownError = false;
           try {
             // Generate key of a given length.
             cryptoSecretKeyGenerator.generate();
           } catch (error) {
             thrownError = true;
           }
           expect(thrownError).toBeTruthy();
         };

         f.policies = {
           symmetric: {
             secretkey: {
               encryption: {lengthBytes: 'AAA'},
               mac: {lengthBytes: 'AAA'},
               secureRandom: {provider: 'Scytl'}
             }
           }
         };

         cryptolib('symmetric.secretkey', f);
       });

    it('throw an error when the given length for encryption is not within the accepted range of values.',
       function() {
         var f = function(box) {
           var secretKeyFactory =
               new box.symmetric.secretkey.factory.SecretKeyFactory();

           var cryptoSecretKeyGenerator =
               secretKeyFactory.getCryptoSecretKeyGeneratorForEncryption();

           var thrownError = false;
           try {
             // Generate key of a given length.
             cryptoSecretKeyGenerator.generate();
           } catch (error) {
             thrownError = true;
           }
           expect(thrownError).toBeTruthy();
         };

         f.policies = {
           symmetric: {
             secretkey: {
               encryption: {lengthBytes: -1},
               mac: {lengthBytes: -1},
               secureRandom: {provider: 'Scytl'}
             }
           }
         };

         cryptolib('symmetric.secretkey', f);
       });
  });
});
