/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
// Test HMAC message digester with SHA-256 hash function.
describe('HmacDigest with SHA-256 hash function ...', function() {
  'use strict';

  var _testString = 'Ox2fUJq1gAbX';
  var _hmacSecretKeyBase64 = 'lFoxPuafE3Kk4jcqqHAatdwfsRS8ZcaF9N4gg+85KoA=';
  var _hmacMessageDigestBase64 = '6qiFgZH6VWXti+ltAqmBO9CryybDJC6oHDJsD4bxVlI=';

  describe('should be able to ..', function() {
    it('verify identity when using identical data.', function() {
      cryptolib('symmetric.mac', 'commons.utils', function(box) {

        var macFactory = new box.symmetric.mac.factory.MacFactory();
        var converters = new box.commons.utils.Converters();

        var hmacSha256Digester = macFactory.create();

        var dataB64 = converters.base64Encode(_testString);
        var arrayDataB64 = [dataB64];

        var macB64 =
            hmacSha256Digester.generate(_hmacSecretKeyBase64, arrayDataB64);

        var verified = hmacSha256Digester.verify(
            macB64, _hmacSecretKeyBase64, arrayDataB64);

        // Verify that new message digest is equal to original.
        expect(verified).toBeTruthy();
      });
    });

    it('generate identical message digests when using identical data.',
       function() {
         cryptolib('symmetric.mac', 'commons.utils', function(box) {

           var macFactory = new box.symmetric.mac.factory.MacFactory();
           var converters = new box.commons.utils.Converters();

           var hmacSha256Digester = macFactory.create();

           var dataB64 = converters.base64Encode(_testString);
           var arrayDataB64 = [dataB64];

           var macB64_1 =
               hmacSha256Digester.generate(_hmacSecretKeyBase64, arrayDataB64);

           var macB64_2 =
               hmacSha256Digester.generate(_hmacSecretKeyBase64, arrayDataB64);

           // Verify that new message digest is equal to original.
           expect(macB64_1).toBe(macB64_2);
         });
       });
  });

  it('generate different message digests when using different data.',
     function() {
       cryptolib('symmetric.mac', 'commons.utils', function(box) {

         var macFactory = new box.symmetric.mac.factory.MacFactory();
         var converters = new box.commons.utils.Converters();

         var hmacSha256Digester = macFactory.create();

         var dataB64 = converters.base64Encode(_testString);
         var arrayDataB64 = [dataB64];

         var macB64_1 =
             hmacSha256Digester.generate(_hmacSecretKeyBase64, arrayDataB64);

         dataB64 = converters.base64Encode(_testString + 'x');
         arrayDataB64 = [dataB64];

         var macB64_2 =
             hmacSha256Digester.generate(_hmacSecretKeyBase64, arrayDataB64);

         // Verify that new message digest is equal to original.
         expect(macB64_1).not.toBe(macB64_2);
       });
     });

  it('generate message digest identical to that generated using Java HMACwithSHA256.',
     function() {
       cryptolib('symmetric.mac', 'commons.utils', function(box) {
         var macFactory = new box.symmetric.mac.factory.MacFactory();
         var converters = new box.commons.utils.Converters();

         var hmacSha256Digester = macFactory.create();

         var dataB64 = converters.base64Encode(_testString);
         var arrayDataB64 = [dataB64];

         var macB64 =
             hmacSha256Digester.generate(_hmacSecretKeyBase64, arrayDataB64);

         // Verify that new message digest is equal to original.
         expect(macB64).toBe(_hmacMessageDigestBase64);
       });
     });

  it('throw an error when the the given data to digest is incorrect.',
     function() {
       cryptolib('symmetric.mac', 'commons.utils', function(box) {
         var macFactory = new box.symmetric.mac.factory.MacFactory();
         var hmacSha256Digester = macFactory.create();

         expect(function() {
           hmacSha256Digester.generate(_hmacSecretKeyBase64, []);
         }).toThrow();
       });
     });

  it('throw an error when the given hash algorithm is wrong.', function() {
    var f = function(box) {
      var macFactory = new box.symmetric.mac.factory.MacFactory();
      var hmacSha256Digester = macFactory.create();
      var converters = new box.commons.utils.Converters();
      var dataB64 = converters.base64Encode(_testString);
      var arrayDataB64 = [dataB64];

      expect(function() {
        hmacSha256Digester.generate(_hmacSecretKeyBase64, arrayDataB64);
      }).toThrow();
    };
    f.policies = {symmetric: {mac: {hash: 'WRONG', provider: 'Forge'}}};
    cryptolib('symmetric.mac', 'commons.utils', f);
  });

  it('throw an error when the given provider is wrong.', function() {
    var f = function(box) {
      var macFactory = new box.symmetric.mac.factory.MacFactory();

      expect(function() {
        macFactory.create();
      }).toThrow();
    };
    f.policies = {symmetric: {mac: {hash: 'SHA256', provider: 'WRONG'}}};
    cryptolib('symmetric.mac', 'commons.utils', f);
  });

  it('generates a valid MAC', function() {

    var originalMessage = 'test';
    var secretKeyB64fromHTML = 'lFoxPuafE3Kk4jcqqHAatdwfsRS8ZcaF9N4gg+85KoA=';

    cryptolib('symmetric.mac', 'commons.utils', function(box) {
      var macFactory = new box.symmetric.mac.factory.MacFactory();
      var converters = new box.commons.utils.Converters();
      var hmacSha256Digester = macFactory.create();
      var dataB64 = converters.base64Encode(originalMessage);
      var arrayDataB64 = [dataB64];
      var macB64 =
          hmacSha256Digester.generate(secretKeyB64fromHTML, arrayDataB64);

      expect(macB64).toBe('px+hdJi/ucaNctN2yM9ZIGE9/mvK1ZhmOGvsZ19bSRI=');
    });
  });

  it('generates a valid MAC with special characters', function() {

    var originalMessage = 'tést';
    var secretKeyB64fromHTML = 'lFoxPuafE3Kk4jcqqHAatdwfsRS8ZcaF9N4gg+85KoA=';

    cryptolib('symmetric.mac', 'commons.utils', function(box) {
      var macFactory = new box.symmetric.mac.factory.MacFactory();
      var converters = new box.commons.utils.Converters();
      var hmacSha256Digester = macFactory.create();
      var dataB64 = converters.base64Encode(originalMessage);
      var arrayDataB64 = [dataB64];
      var macB64 =
          hmacSha256Digester.generate(secretKeyB64fromHTML, arrayDataB64);

      expect(macB64).toBe('6zRTAMeZqMvJaBCtwewm19R5ECQ4manZnk8uI4oOWus=');
    });
  });
});
