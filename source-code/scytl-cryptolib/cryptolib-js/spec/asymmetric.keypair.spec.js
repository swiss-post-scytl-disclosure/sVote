/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Crypto Encryption Key Pair Generator...', function() {
  'use strict';

  describe('of the FORGE provider ..', function() {
    describe('should be able to ..', function() {
      it('be defined', function() {
        cryptolib('asymmetric.keypair', function(box) {

          var keyPairGeneratorFactory =
              new box.asymmetric.keypair.factory.KeyPairGeneratorFactory();

          var encryptionKeyPairGenerator =
              keyPairGeneratorFactory.getEncryptionCryptoKeyPairGenerator();
          expect(encryptionKeyPairGenerator).toBeDefined();
        });
      });

      it('Generate RSA key pair for encryption', function() {
        // Note: Key size here is set to value lower than minimum value allowed
        // by policy in order to keep test time reasonable.
        var keyBitLength = 1024;

        var f = function(box) {
          var keyPairGeneratorFactory =
              new box.asymmetric.keypair.factory.KeyPairGeneratorFactory();

          var encryptionKeyPairGenerator =
              keyPairGeneratorFactory.getEncryptionCryptoKeyPairGenerator();

          var cryptoEncryptionKeyPair = encryptionKeyPairGenerator.generate();

          var publicKeyPem = (cryptoEncryptionKeyPair.getPublicKey()).getPem();
          var privateKeyPem =
              (cryptoEncryptionKeyPair.getPrivateKey()).getPem();

          var publicKey = box.forge.pki.publicKeyFromPem(publicKeyPem);
          var privateKey = box.forge.pki.privateKeyFromPem(privateKeyPem);

          expect(publicKey.n.bitLength()).toBe(keyBitLength);
          expect(privateKey.n.bitLength()).toBe(keyBitLength);
        };

        f.policies = {
          asymmetric: {
            keypair: {
              encryption: {
                algorithm: Config.asymmetric.keypair.encryption.algorithm.RSA,
                provider: Config.asymmetric.keypair.encryption.provider.FORGE,
                keyLength: keyBitLength,
                publicExponent:
                    Config.asymmetric.keypair.encryption.publicExponent.F4
              },
              secureRandom: {
                provider: Config.asymmetric.keypair.secureRandom.provider.SCYTL
              }
            }
          }
        };

        cryptolib('asymmetric.keypair', f);
      });

      it('throw an error when the given algorithm is wrong.', function() {

        var f = function(box) {

          var keyPairGeneratorFactory =
              new box.asymmetric.keypair.factory.KeyPairGeneratorFactory();

          expect(function() {
            keyPairGeneratorFactory.getEncryptionCryptoKeyPairGenerator()
                .generate();
          }).toThrow();
        };

        f.policies = {
          asymmetric: {
            keypair: {
              encryption: {algorithm: 'WRONG', provider: 'Forge'},
              secureRandom: {provider: 'Scytl'}
            }
          }
        };

        cryptolib('asymmetric.keypair', f);
      });

      it('throw an error when the given provider is wrong.', function() {

        var f = function(box) {

          var keyPairGeneratorFactory =
              new box.asymmetric.keypair.factory.KeyPairGeneratorFactory();

          expect(function() {
            keyPairGeneratorFactory.getEncryptionCryptoKeyPairGenerator()
                .generate();
          }).toThrow();
        };

        f.policies = {
          asymmetric: {
            keypair: {
              encryption: {algorithm: 'CORRECT', provider: 'WRONG'},
              secureRandom: {provider: 'Scytl'}
            }
          }
        };

        cryptolib('asymmetric.keypair', f);
      });
    });
  });
});

describe('Crypto Encryption Key Pair ...', function() {
  'use strict';

  describe('should be able to ..', function() {
    it('be defined', function() {
      cryptolib('asymmetric.keypair', function(box) {

        var keyPairGeneratorFactory =
            new box.asymmetric.keypair.factory.KeyPairGeneratorFactory();

        var encryptionKeyPairGenerator =
            keyPairGeneratorFactory.getEncryptionCryptoKeyPairGenerator();

        var encryptionKeyPair = encryptionKeyPairGenerator.generate();

        expect(encryptionKeyPair).toBeDefined();
      });
    });
  });
});
