/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
// Test Sha256 hash function.
describe('Message digest ...', function() {
  'use strict';

  var _data = 'Ox2fUJq1gAbX';

  describe('when algorithm is SHA256 ..', function() {
    describe('should be able to ..', function() {
      it('generate identical message digests when using identical data.',
         function() {
           cryptolib(
               'primitives.messagedigest', 'commons.utils', function(box) {

                 var messageDigestFactory = new box.primitives.messagedigest
                                                .factory.MessageDigestFactory();
                 var converters = new box.commons.utils.Converters();

                 var sha256Digest =
                     messageDigestFactory.getCryptoMessageDigest();

                 sha256Digest.start();

                 var dataB64;

                 dataB64 = converters.base64Encode(_data);

                 var arrayDataBase64 = [dataB64];

                 sha256Digest.update(arrayDataBase64);

                 var msgDigest = sha256Digest.digest();

                 // Generate new message digest from same test string.
                 sha256Digest.start();

                 sha256Digest.update(arrayDataBase64);
                 var msgDigestNew = sha256Digest.digest();

                 // Verify that new message digest is equal to original.
                 expect(msgDigestNew).toBe(msgDigest);
               });
         });

      it('generate different message digests when using different data.',
         function() {
           cryptolib(
               'primitives.messagedigest', 'commons.utils', function(box) {
                 // Generate message digest from test string.
                 var messageDigestFactory = new box.primitives.messagedigest
                                                .factory.MessageDigestFactory();
                 var converters = new box.commons.utils.Converters();

                 var sha256Digest =
                     messageDigestFactory.getCryptoMessageDigest();
                 sha256Digest.start();

                 var dataB64;

                 dataB64 = converters.base64Encode(_data);

                 var arrayDataBase64 = [dataB64];

                 sha256Digest.update(arrayDataBase64);

                 var msgDigest = sha256Digest.digest();

                 // Generate new message digest from slightly altered test
                 // string.
                 sha256Digest.start();

                 dataB64 = converters.base64Encode(_data + 'x');

                 arrayDataBase64 = [dataB64];

                 sha256Digest.update(arrayDataBase64);
                 var msgDigestNew = sha256Digest.digest();

                 // Verify that new message digest is not equal to original.
                 expect(msgDigestNew).not.toBe(msgDigest);
               });
         });

      it('throw an error when the array of data is null.', function() {
        cryptolib('primitives.messagedigest', function(box) {
          // Generate message digest from test string.

          var messageDigestFactory =
              new box.primitives.messagedigest.factory.MessageDigestFactory();

          var sha256Digest = messageDigestFactory.getCryptoMessageDigest();
          sha256Digest.start();

          expect(function() {
            sha256Digest.update(undefined);
          }).toThrow();

          expect(function() {
            sha256Digest.update(null);
          }).toThrow();
        });
      });

      it('throw an error when the array of data contains less than one element.',
         function() {
           cryptolib('primitives.messagedigest', function(box) {
             // Generate message digest from test string.

             var messageDigestFactory = new box.primitives.messagedigest.factory
                                            .MessageDigestFactory();

             var sha256Digest = messageDigestFactory.getCryptoMessageDigest();
             sha256Digest.start();

             expect(function() {
               sha256Digest.update([]);
             }).toThrow();
           });
         });
    });
  });

  describe('when algorithm is SHA512/224 ..', function() {
    describe('should be able to ..', function() {
      it('generate identical message digests when using identical data.',
         function() {

           var f = function(box) {
             var messageDigestFactory = new box.primitives.messagedigest.factory
                                            .MessageDigestFactory();
             var converters = new box.commons.utils.Converters();

             var digester = messageDigestFactory.getCryptoMessageDigest();

             digester.start();

             var dataB64;

             dataB64 = converters.base64Encode(_data);

             var arrayDataBase64 = [dataB64];

             digester.update(arrayDataBase64);

             var msgDigest = digester.digest();

             // Generate new message digest from same test string.
             digester.start();

             digester.update(arrayDataBase64);
             var msgDigestNew = digester.digest();

             // Verify that new message digest is equal to original.
             expect(msgDigestNew).toBe(msgDigest);
           };

           f.policies = {
             primitives:
                 {messagedigest: {algorithm: 'SHA512/224', provider: 'Forge'}}
           };

           cryptolib('primitives.messagedigest', 'commons.utils', f);
         });

      it('generate different message digests when using different data.',
         function() {

           var f = function(box) {
             // Generate message digest from test string.

             var messageDigestFactory = new box.primitives.messagedigest.factory
                                            .MessageDigestFactory();
             var converters = new box.commons.utils.Converters();

             var digester = messageDigestFactory.getCryptoMessageDigest();
             digester.start();

             var dataB64;

             dataB64 = converters.base64Encode(_data);

             var arrayDataBase64 = [dataB64];

             digester.update(arrayDataBase64);

             var msgDigest = digester.digest();

             // Generate new message digest from slightly altered test string.
             digester.start();

             dataB64 = converters.base64Encode(_data + 'x');

             arrayDataBase64 = [dataB64];

             digester.update(arrayDataBase64);
             var msgDigestNew = digester.digest();

             // Verify that new message digest is not equal to original.
             expect(msgDigestNew).not.toBe(msgDigest);
           };

           f.policies = {
             primitives:
                 {messagedigest: {algorithm: 'SHA512/224', provider: 'Forge'}}
           };

           cryptolib('primitives.messagedigest', 'commons.utils', f);
         });

      it('throw an error when the array of data is null.', function() {

        var f = function(box) {
          // Generate message digest from test string.

          var messageDigestFactory =
              new box.primitives.messagedigest.factory.MessageDigestFactory();

          var digester = messageDigestFactory.getCryptoMessageDigest();
          digester.start();

          expect(function() {
            digester.update(undefined);
          }).toThrow();

          expect(function() {
            digester.update(null);
          }).toThrow();
        };

        f.policies = {
          primitives:
              {messagedigest: {algorithm: 'SHA512/224', provider: 'Forge'}}
        };

        cryptolib('primitives.messagedigest', f);
      });

      it('throw an error when the array of data contains less than one element.',
         function() {

           var f = function(box) {
             // Generate message digest from test string.

             var messageDigestFactory = new box.primitives.messagedigest.factory
                                            .MessageDigestFactory();

             var digester = messageDigestFactory.getCryptoMessageDigest();
             digester.start();

             expect(function() {
               digester.update([]);
             }).toThrow();
           };

           f.policies = {
             primitives:
                 {messagedigest: {algorithm: 'SHA512/224', provider: 'Forge'}}
           };

           cryptolib('primitives.messagedigest', f);
         });
    });
  });
});
