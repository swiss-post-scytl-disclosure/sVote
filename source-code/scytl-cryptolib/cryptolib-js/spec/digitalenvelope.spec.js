/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Digital envelope module...', function() {
  'use strict';

  var _rootPrivateKeyPem =
      '-----BEGIN RSA PRIVATE KEY-----MIIEogIBAAKCAQEAngCJ2RYh3XOvghmAi2MT26/fx4Hx8gYkYS2zaf9c2H8Qns1hnIHO8pIlHze/stUmNzim6nlytbJLQHmVDu64Pf9dn1ClAW2caKqUo4ofjnOzDd6RocQByjTsvZ1nOcG5BMYtOWGua+QIVFrxvxZgL6T3ShQ+9J3/1LiQDUmWj0kMoct6vRJuNkJxVFegtIEqYZYTTFI1+Y25k040NFu0e9IDwDXQA63EqKL3DjrgxdjOY+KXu4zU6DUcL4VcKaTMs1+pDfU6bSOtrecuasKkEmNE9fuy9WejFqNGqlrNVCeYmOOnNXvI0vCdH1bw4/CpqddxvGZOStczF8vGJVcMVQIDAQABAoIBAB/MEfHLP3N2pZp0EWd7v/JVOZ7H6u3/CHE6JkItrvyuzaR3xq4dfY1ZlfjrWjveI3u3fffwCwK/598I2NORfSiU7L10GFDqQzDZK2KiGGmtpRokcYBxlr6f2gjq1WxNLTPlwhIrM1PpJzf6uW3wTdkoxM92tZi691JS9kKGTbN1+LWIITWk6sC3n4Atu8LI19/zSfrl16/1iYSF7n9ylz2tyqQAyBEQXgrMRrg3KkPU83P5CwdIdDFyb4pwZ75U8OXuOfhcv0ilGIH62t2d9eVaFw0RKSf4QOYAdcVPAA75m+RzTSM7jDX4OGiPsoa1iWS4fqcdPfchFQSnJhhrL2ECgYEA8VV9JMYKE4qRY/srxhKnsVXhAVX8WkOjBT+VQSEBtI9ay5wld1JYLjWnCfaScXUQhFTXrR1f4JDAQ1K2MYwJ8FF1whW6WR0FvUi6IZX9C+pNfN4YtbVOYg4P8PhLF4hnV8UyqFCYN4Apw3JadS0kKK370eIhz1g+307pwu47h00CgYEAp5qhAi0HdB/aleb/GHGBr//xDHwm87mKyi6BKhiNP5phM7gnoyItIoxT2ArUgYC9lrrt0hi4wZsCxixLciWjGw9rFBY0Z23ie59cRyY9TlV3WTNFLkIl7nBB5PJaJAu7scXx/R4Ozgc0brX4mXs1ZORzlDbRKNFYNnsGkRKhZSkCgYA55vT7mbhZL+nqPx0ljNxHI/+0PlkpnwjB/Ztl4PFzzOFP8MrcchlOHPlS3qIMLrYjyedlVaLnUlOO0417HcpUqnbCdkwbjWcPHx/pZv8rmK+2weLT1ghUZmNUwX3iy4tf96RL1epvhoR+rDUf4BDI4dWFaegKw3VyRpC3gEkwEQKBgCaCXtAzLG1ADGc45g/ltJiNxALMW97QGNWPjdnwPjQI8qCBhYn0Bk7T00fmZSFERUtms9H8ICdLyH1kHAmkIC/NgRriZzQEiW9CFF+8p4ViGcQDBbg1NqXsYReLn658i6mzA4DW8Svhd+igIviQ2JnP68Z4OeKZBx2tcrrOfwrRAoGAGyXTcOvhr+5ayXkQ36VAr2YcAUs2SeN0HwmYBP2ErCgd/KXri7RwWXqEQtvpvAOGJq/tXyc7P5BnBhlqQtUm9WX7kqTy/dIS+pJP0UMm02N9U+b02G0auPmBKM35LXO4YGhZep1Vig+MLycR8OSe0cd4gzk52u3Th9I3e0bV9Fw=-----END RSA PRIVATE KEY-----';
  var _rootPublicKeyPem =
      '-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAngCJ2RYh3XOvghmAi2MT26/fx4Hx8gYkYS2zaf9c2H8Qns1hnIHO8pIlHze/stUmNzim6nlytbJLQHmVDu64Pf9dn1ClAW2caKqUo4ofjnOzDd6RocQByjTsvZ1nOcG5BMYtOWGua+QIVFrxvxZgL6T3ShQ+9J3/1LiQDUmWj0kMoct6vRJuNkJxVFegtIEqYZYTTFI1+Y25k040NFu0e9IDwDXQA63EqKL3DjrgxdjOY+KXu4zU6DUcL4VcKaTMs1+pDfU6bSOtrecuasKkEmNE9fuy9WejFqNGqlrNVCeYmOOnNXvI0vCdH1bw4/CpqddxvGZOStczF8vGJVcMVQIDAQAB-----END PUBLIC KEY-----';
  var _userPrivateKeyPem =
      '-----BEGIN RSA PRIVATE KEY-----MIIEowIBAAKCAQEAglypZU45bnf2opnRI+Y51VqouBpvDT33xIB/OtbwKzwVpi+JrjBFtfk33tE9t/dSRs79CK94HRhCWcOiLa2qWPrjeZ9SBiEyScrhIvRZVBF41zBgwQNuRvJCsKmAqlZaFNJDZxEP4repmlBn1CfVFmfrXmOKqwP5F7l9ZtucveRzsfmF1yVPFkW8TMuB3YqMiyymyqHlS8ujCsu5I8tpgPbwuxdMOY94fNhSXrYkY8IuX1g1zdq/Z1jluOaR/UqK4UpnbuJaH/F0VgDNiWh6cTD0DFGEk0b70i5wU4Q3L/S6XZQRvSuADoCbhwBKuFL5pW5n865oLVb5S3wuVdWaGwIDAQABAoIBAC/tn34Wf3kE9BGeGc1oFLVDaqqdVVz5/oEpeR2J7q0GnzMFYUpAhzC7WvY52cYsUPyll1Q9Jx0TUTmteo/uvKWQQFfz4nVMeS+2PoXabolBDzuWlsv/1eiRo0FOYHa/3siu8YcQN9X0DpAkpbfTmT1uoZOHZ3EuucMmOFu7vGn38Grw8bSxpR0uvTtnb8ygC+aB51y38RMyhzQQanrM8FMeAfDAy6IB0Yo7b0c50Cxa6Ax4nqn9LXyGakr5WeAMkgTIOA/GId9SZD4e5eRpq+628pOeR4O9datFltgl6r1+A4ii2VrJsDqeatGtODlX6KRKqwFHoGIa2TjgSZLuorECgYEAxeSZDOOgFsI5mB7RkRzZaQ9znJ15sgdyZiAFZAOUah4hSGdAXNAnZTlrdacduXEu3EfkpuPToX7xZSv5FRYwfBwMwCLeytlGLPjQzWejZGbo4+KqgzWb9fECDYVtDPlJ/+yLih9nt67BHweJKxYydl18rVigdVyy22X86NijSykCgYEAqKPUrXZAo+TJvmTw4tgsibJgvXBYBhmsej8mGNQw+Nyp2gV28sgm61ifIeXKS8teq+MFwGA6cHQedbsCqhMHokdhESZmlbWxhSFLihQcewBxwvrBwbaxI23yXRzwMewznZFL032PpcbqrmwFmcSSEZ3nmbvTH6ShqLW+pzDNp6MCgYBQLzdgxJ7qedqSa/JohTMG4e7rh9d2rpPJE7J7ewPZF8pOpx+qO+Gqn2COdJ+Ts2vUcAETKn9nEaPIZc/wnmQY9dioxbhWo0FPGaaphBPtq9Ez/XUv4zoFppk5V1X/isdUPsmvttf00oeIBiqrXbwmv+yz5JRn2Z7TTXjz9Ev+OQKBgQCUuoCMRzl1EgcXIqEL/0kwW6BUEqufHa9u1Ri9Vw6lvL8T6DPipMEmWK9nzuid9gtVns/ovTVtDgv7GuabplLaPQePf4WDzY11c0rSyS/hDyBFrK+LL5uEOqhAlJAGB2HyOj1clWVF+GvrTpuV5LZKUS/79pmZU7G7QCaX/0Ow7wKBgC/kDH7cmWQnWvvJ5izrx/7PogQVPOLELeUIGLu/hjsSdDKiFCxCUZ948+9NuG+DnpXDWzw//r8mPBRRGGsqFws5Aipp7yjQ3kRDCCzGelPCVhHyfmKqA+8ewXPulKS3/wIyHIvaXmsuAtTfurHtpRyzjKmCBK1Y6WQ3trIXvo7s-----END RSA PRIVATE KEY-----';
  var _userPublicKeyPem =
      '-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAglypZU45bnf2opnRI+Y51VqouBpvDT33xIB/OtbwKzwVpi+JrjBFtfk33tE9t/dSRs79CK94HRhCWcOiLa2qWPrjeZ9SBiEyScrhIvRZVBF41zBgwQNuRvJCsKmAqlZaFNJDZxEP4repmlBn1CfVFmfrXmOKqwP5F7l9ZtucveRzsfmF1yVPFkW8TMuB3YqMiyymyqHlS8ujCsu5I8tpgPbwuxdMOY94fNhSXrYkY8IuX1g1zdq/Z1jluOaR/UqK4UpnbuJaH/F0VgDNiWh6cTD0DFGEk0b70i5wU4Q3L/S6XZQRvSuADoCbhwBKuFL5pW5n865oLVb5S3wuVdWaGwIDAQAB-----END PUBLIC KEY-----';

  var _factory;
  var _generator;
  var _opener;
  var _data;
  var _envelope;

  // Initialize variables common to all digital envelope tests.
  beforeEach(function() {
    cryptolib('commons.utils', 'digitalenvelope', function(box) {
      _factory = new box.digitalenvelope.factory.DigitalEnvelopeFactory();
      _generator = _factory.getDigitalEnvelopeGenerator();
      _opener = _factory.getDigitalEnvelopeOpener();

      var converters = new box.commons.utils.Converters();
      _data = converters.base64Encode('Ox2fUJq1gAbX');

      _envelope =
          _generator.generate(_data, [_userPublicKeyPem, _rootPublicKeyPem]);
    });
  });

  describe('should be able to ..', function() {

    it('create a digital envelope factory', function() {
      expect(_factory).toBeDefined();
    });

    it('and create a digital envelope generator', function() {
      expect(_generator).toBeDefined();
    });

    it('and create a digital envelope opener', function() {
      expect(_opener).toBeDefined();
    });

    it('and generate a digital envelope', function() {
      expect(_envelope).toBeDefined();
    });

    it('and open a digital envelope', function() {
      expect(_opener.open(_envelope, _userPrivateKeyPem)).toBe(_data);
      expect(_opener.open(_envelope, _rootPrivateKeyPem)).toBe(_data);
    });
  });
});
