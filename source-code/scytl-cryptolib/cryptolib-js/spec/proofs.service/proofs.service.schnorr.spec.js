/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Proofs service ...', function() {
  'use strict';

  // Initialize variables common to all proof tests.
  beforeEach(initializeTestVars);

  // Schnorr proof tests.
  describe('should be able to ..', function() {
    var voter_id = '00000';
    var election_id = '22222';
    var num_progress_checks = 1;

    var initialized = false;
    var witness;
    var exponentiatedGenerator;
    var proof;
    var preComputedValues;
    var anotherWitness;
    var anotherExponentiatedGenerator;
    var anInvalidExponentiatedGenerator;
    var anotherVoterId;
    var anotherElectionId;

    // Initialize variables common to Schnorr proof tests.
    beforeEach(function() {
      if (!initialized) {
        cryptolib('commons.mathematical', 'proofs.service', function(box) {
          witness = generateRandomExponent(_zpSubgroup);
          exponentiatedGenerator =
              _zpSubgroup.getGenerator().exponentiate(witness);
          proof = box.proofs.service.createSchnorrProof(
              voter_id, election_id, exponentiatedGenerator, witness,
              _zpSubgroup);
          preComputedValues = box.proofs.service.preComputeSchnorrProof(
              voter_id, election_id, _zpSubgroup);

          do {
            anotherWitness = generateRandomExponent(_zpSubgroup);
          } while (anotherWitness.getValue().equals(witness.getValue()));
          do {
            var witnessFromOtherGroup;
            do {
              witnessFromOtherGroup =
                  generateRandomExponent(_anotherZpSubgroup);
            } while (
                witnessFromOtherGroup.getValue().equals(witness.getValue()));

            anotherExponentiatedGenerator =
                _anotherZpSubgroup.getGenerator().exponentiate(
                    witnessFromOtherGroup);
          } while (anotherExponentiatedGenerator.getElementValue().equals(
              exponentiatedGenerator.getElementValue()));
          var qElement =
              new box.commons.mathematical.ZpGroupElement(_q, _p, _q);
          anInvalidExponentiatedGenerator = qElement.exponentiate(witness);
          anotherVoterId = voter_id + '0';
          anotherElectionId = election_id + '0';
        });

        initialized = true;
      }
    });

    it('generate and verify a Schnorr proof', function() {
      cryptolib('proofs.service', function(box) {
        var verification = box.proofs.service.verifySchnorrProof(
            exponentiatedGenerator, voter_id, election_id, proof, _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and generate and verify a Schnorr proof, using pre-computed values',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(preComputedValues).toBeDefined();

           var anotherProof = box.proofs.service.createSchnorrProof(
               voter_id, election_id, exponentiatedGenerator, witness,
               _zpSubgroup, preComputedValues);

           var verification = box.proofs.service.verifySchnorrProof(
               exponentiatedGenerator, voter_id, election_id, anotherProof,
               _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and generate and verify a Schnorr proof, using null for the pre-computed values option',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createSchnorrProof(
               voter_id, election_id, exponentiatedGenerator, witness,
               _zpSubgroup, null);

           var verification = box.proofs.service.verifySchnorrProof(
               exponentiatedGenerator, voter_id, election_id, anotherProof,
               _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('generate and verify a Schnorr proof, using serialization', function() {
      cryptolib('commons.mathematical', 'proofs', function(box) {
        var proofAsJson = proof.stringify();

        var proofFromJson = box.proofs.utils.deserializeProof(proofAsJson);

        var verification = box.proofs.service.verifySchnorrProof(
            exponentiatedGenerator, voter_id, election_id, proofFromJson,
            _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('generate and verify a Schnorr proof, using pre-computed values and serialization',
       function() {
         cryptolib('commons.mathematical', 'proofs', function(box) {
           var preComputedValuesAsJson = preComputedValues.stringify();

           var preComputedValuesFromJson =
               box.proofs.utils.deserializeProofPreComputedValues(
                   preComputedValuesAsJson);

           var anotherProof = box.proofs.service.createSchnorrProof(
               voter_id, election_id, exponentiatedGenerator, witness,
               _zpSubgroup, preComputedValuesFromJson);

           var verification = box.proofs.service.verifySchnorrProof(
               exponentiatedGenerator, voter_id, election_id, anotherProof,
               _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when generating a Schnorr proof', function() {
      cryptolib('proofs.service', function(box) {
        initializeProgressMeter();
        var anotherProof = box.proofs.service.createSchnorrProof(
            voter_id, election_id, exponentiatedGenerator, witness, _zpSubgroup,
            progressCallback, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

        expect(_progressPercentSum)
            .toBe(getProgressPercentSum(
                num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
        expect(_progressPercentLatest).toBe(100);

        var verification = box.proofs.service.verifySchnorrProof(
            exponentiatedGenerator, voter_id, election_id, anotherProof,
            _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and measure progress when pre-computing a Schnorr proof', function() {
      cryptolib('proofs.service', function(box) {
        initializeProgressMeter();
        box.proofs.service.preComputeSchnorrProof(
            voter_id, election_id, _zpSubgroup, progressCallback,
            PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

        expect(_progressPercentSum)
            .toBe(getProgressPercentSum(
                num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
        expect(_progressPercentLatest).toBe(100);

        var anotherProof = box.proofs.service.createSchnorrProof(
            voter_id, election_id, exponentiatedGenerator, witness, _zpSubgroup,
            preComputedValues);
        var verification = box.proofs.service.verifySchnorrProof(
            exponentiatedGenerator, voter_id, election_id, anotherProof,
            _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and measure progress when verifying a Schnorr proof', function() {
      cryptolib('proofs.service', function(box) {
        initializeProgressMeter();
        var verification = box.proofs.service.verifySchnorrProof(
            exponentiatedGenerator, voter_id, election_id, proof, _zpSubgroup,
            progressCallback, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
        expect(verification).toBeTruthy();

        expect(_progressPercentSum)
            .toBe(getProgressPercentSum(
                num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
        expect(_progressPercentLatest).toBe(100);
      });
    });

    it('and fail to verify a Schnorr proof that was generated with the wrong witness',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createSchnorrProof(
               voter_id, election_id, exponentiatedGenerator, anotherWitness,
               _zpSubgroup);

           var verification = box.proofs.service.verifySchnorrProof(
               exponentiatedGenerator, voter_id, election_id, anotherProof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a Schnorr proof that was generated with the wrong exponentiated generator',
       function() {
         cryptolib('commons.mathematical', 'proofs.service', function(box) {
           var anotherProof = box.proofs.service.createSchnorrProof(
               voter_id, election_id, anotherExponentiatedGenerator, witness,
               _anotherZpSubgroup);

           var verification = box.proofs.service.verifySchnorrProof(
               anotherExponentiatedGenerator, voter_id, election_id,
               anotherProof, _anotherZpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a Schnorr proof that was generated with an invalid exponentiated generator',
       function() {
         cryptolib('commons.mathematical', 'proofs.service', function(box) {
           var anotherProof = box.proofs.service.createSchnorrProof(
               voter_id, election_id, anInvalidExponentiatedGenerator, witness,
               _zpSubgroup);

           var verification = box.proofs.service.verifySchnorrProof(
               anInvalidExponentiatedGenerator, voter_id, election_id,
               anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a Schnorr proof when using the wrong business data',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification = box.proofs.service.verifySchnorrProof(
               exponentiatedGenerator, anotherVoterId, election_id, proof,
               _zpSubgroup);
           expect(verification).toBeFalsy();

           verification = box.proofs.service.verifySchnorrProof(
               exponentiatedGenerator, voter_id, anotherElectionId, proof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and throw an exception when generating a Schnorr proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createSchnorrProof(
                 null, election_id, exponentiatedGenerator, witness,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createSchnorrProof(
                 voter_id, null, exponentiatedGenerator, witness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createSchnorrProof(
                 voter_id, election_id, null, witness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createSchnorrProof(
                 voter_id, election_id, exponentiatedGenerator, null,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createSchnorrProof(
                 voter_id, election_id, exponentiatedGenerator, witness, null);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a Schnorr proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createSchnorrProof(
                 voter_id, election_id, exponentiatedGenerator, witness,
                 _zpSubgroup, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a Schnorr proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.createSchnorrProof(
                 voter_id, election_id, exponentiatedGenerator, witness,
                 _zpSubgroup, progressCallback, progressCallback);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing values for a Schnorr proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.preComputeSchnorrProof(
                 null, election_id, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.preComputeSchnorrProof(
                 voter_id, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.preComputeSchnorrProof(
                 voter_id, election_id, null);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a Schnorr proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 null, voter_id, election_id, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, null, election_id, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, voter_id, null, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, voter_id, election_id, null,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, voter_id, election_id, proof, null);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a Schnorr proof, using the wrong exponentiated generator',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 anotherExponentiatedGenerator, voter_id, election_id, proof,
                 _anotherZpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a Schnorr proof that was generated with the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createSchnorrProof(
               voter_id, election_id, exponentiatedGenerator, witness,
               _anotherZpSubgroup);

           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, voter_id, election_id, anotherProof,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a Schnorr proof, using the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, voter_id, election_id, proof,
                 _anotherZpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a Schnorr proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, voter_id, election_id, proof,
                 _zpSubgroup, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a Schnorr proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.verifySchnorrProof(
                 exponentiatedGenerator, voter_id, election_id, proof,
                 _zpSubgroup, progressCallback, progressCallback);
           }).toThrow();
         });
       });
  });
});
