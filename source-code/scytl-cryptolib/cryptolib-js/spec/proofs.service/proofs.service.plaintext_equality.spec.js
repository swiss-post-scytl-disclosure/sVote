/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Proofs service ...', function() {
  'use strict';

  // Initialize variables common to all proof tests.
  beforeEach(initializeTestVars);

  // Plaintext equality proof tests.
  describe('should be able to ..', function() {
    var num_plaintext_elements = 3;
    var num_progress_checks = (num_plaintext_elements * 2) + 2;

    var initialized = false;
    var plaintext;
    var primaryPublicKey;
    var secondaryPublicKey;
    var primaryCiphertext;
    var secondaryCiphertext;
    var primaryWitness;
    var secondaryWitness;
    var proof;
    var preComputedValues;
    var anotherPlaintext;
    var anotherPrimaryPublicKey;
    var anotherSecondaryPublicKey;
    var anotherPrimaryCiphertext;
    var anotherSecondaryCiphertext;
    var anotherPrimaryWitness;
    var anotherSecondaryWitness;
    var aShorterPrimaryPublicKey;
    var aShorterSecondaryPublicKey;
    var aShorterPrimaryCiphertext;

    // Initialize variables common to plaintext equality proof tests.
    beforeEach(function() {
      if (!initialized) {
        cryptolib('proofs.service', function(box) {
          plaintext = generatePlaintext(num_plaintext_elements);
          primaryPublicKey =
              generateElGamalKeyPair(num_plaintext_elements).getPublicKey();
          secondaryPublicKey =
              generateElGamalKeyPair(num_plaintext_elements).getPublicKey();
          primaryCiphertext = generateCiphertext(primaryPublicKey, plaintext);
          secondaryCiphertext =
              generateCiphertext(secondaryPublicKey, plaintext);
          primaryWitness = primaryCiphertext.getR();
          secondaryWitness = secondaryCiphertext.getR();
          proof = box.proofs.service.createPlaintextEqualityProof(
              primaryCiphertext, primaryPublicKey, primaryWitness,
              secondaryCiphertext, secondaryPublicKey, secondaryWitness,
              _zpSubgroup);
          preComputedValues =
              box.proofs.service.preComputePlaintextEqualityProof(
                  primaryPublicKey, secondaryPublicKey, _zpSubgroup);

          do {
            anotherPlaintext = generatePlaintext(num_plaintext_elements);
          } while (anotherPlaintext.equals(plaintext));
          do {
            anotherPrimaryPublicKey =
                generateElGamalKeyPair(num_plaintext_elements).getPublicKey();
          } while (anotherPrimaryPublicKey.getGroupElementsArray().equals(
              primaryPublicKey.getGroupElementsArray()));
          do {
            anotherSecondaryPublicKey =
                generateElGamalKeyPair(num_plaintext_elements).getPublicKey();
          } while (anotherSecondaryPublicKey.getGroupElementsArray().equals(
              secondaryPublicKey.getGroupElementsArray()));
          anotherPrimaryCiphertext =
              generateCiphertext(primaryPublicKey, anotherPlaintext);
          anotherSecondaryCiphertext =
              generateCiphertext(secondaryPublicKey, anotherPlaintext);
          do {
            anotherPrimaryWitness = generateRandomExponent(_zpSubgroup);
          } while (anotherPrimaryWitness.getValue().equals(
              primaryWitness.getValue()));
          do {
            anotherSecondaryWitness = generateRandomExponent(_zpSubgroup);
          } while (anotherSecondaryWitness.getValue().equals(
              secondaryWitness.getValue()));
          aShorterPrimaryPublicKey =
              generateElGamalKeyPair(num_plaintext_elements - 1).getPublicKey();
          aShorterSecondaryPublicKey =
              generateElGamalKeyPair(num_plaintext_elements - 1).getPublicKey();
          var subPlaintext = generatePlaintext(num_plaintext_elements = 1);
          aShorterPrimaryCiphertext =
              generateCiphertext(primaryPublicKey, subPlaintext);
        });

        initialized = true;
      }
    });

    it('generate and verify a plaintext equality proof', function() {
      cryptolib('proofs.service', function(box) {
        var verification = box.proofs.service.verifyPlaintextEqualityProof(
            primaryCiphertext, primaryPublicKey, secondaryCiphertext,
            secondaryPublicKey, proof, _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and generate and verify a plaintext equality, using pre-computed values',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(preComputedValues).toBeDefined();

           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               secondaryCiphertext, secondaryPublicKey, secondaryWitness,
               _zpSubgroup, preComputedValues);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and generate and verify a plaintext equality proof, using null for the pre-computed values option',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               secondaryCiphertext, secondaryPublicKey, secondaryWitness,
               _zpSubgroup, null);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('generate and verify a plaintext equality proof, using serialization',
       function() {
         cryptolib('commons.mathematical', 'proofs', function(box) {
           var proofAsJson = proof.stringify();

           var proofFromJson = box.proofs.utils.deserializeProof(proofAsJson);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, proofFromJson, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('generate and verify a plaintext equality proof, using pre-computed values and serialization',
       function() {
         cryptolib('commons.mathematical', 'proofs', function(box) {
           var preComputedValuesAsJson = preComputedValues.stringify();

           var preComputedValuesFromJson =
               box.proofs.utils.deserializeProofPreComputedValues(
                   preComputedValuesAsJson);

           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               secondaryCiphertext, secondaryPublicKey, secondaryWitness,
               _zpSubgroup, preComputedValuesFromJson);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when generating a plaintext equality proof',
       function() {
         cryptolib('proofs.service', function(box) {
           initializeProgressMeter();
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               secondaryCiphertext, secondaryPublicKey, secondaryWitness,
               _zpSubgroup, progressCallback,
               PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

           expect(_progressPercentSum)
               .toBe(getProgressPercentSum(
                   num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
           expect(_progressPercentLatest).toBe(100);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when pre-computing a plaintext equality proof',
       function() {
         cryptolib('proofs.service', function(box) {
           initializeProgressMeter();
           preComputedValues =
               box.proofs.service.preComputePlaintextEqualityProof(
                   primaryPublicKey, secondaryPublicKey, _zpSubgroup,
                   progressCallback, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

           expect(_progressPercentSum)
               .toBe(getProgressPercentSum(
                   num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
           expect(_progressPercentLatest).toBe(100);

           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               secondaryCiphertext, secondaryPublicKey, secondaryWitness,
               _zpSubgroup, preComputedValues);
           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when verifying a plaintext equality proof',
       function() {
         cryptolib('proofs.service', function(box) {
           initializeProgressMeter();
           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, proof, _zpSubgroup, progressCallback,
               PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           expect(verification).toBeTruthy();

           expect(_progressPercentSum)
               .toBe(getProgressPercentSum(
                   num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
           expect(_progressPercentLatest).toBe(100);
         });
       });

    it('and fail to verify a plaintext equality proof that was generated with the wrong primary ciphertext',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               anotherPrimaryCiphertext, primaryPublicKey,
               anotherPrimaryCiphertext.getR(), secondaryCiphertext,
               secondaryPublicKey, secondaryWitness, _zpSubgroup);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               anotherPrimaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof that was generated with the wrong primary public key',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, anotherPrimaryPublicKey, primaryWitness,
               secondaryCiphertext, secondaryPublicKey, secondaryWitness,
               _zpSubgroup);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, anotherPrimaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof that was generated with the wrong primary witness',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, anotherPrimaryWitness,
               secondaryCiphertext, secondaryPublicKey, secondaryWitness,
               _zpSubgroup);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof that was generated with the wrong secondary ciphertext',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               anotherSecondaryCiphertext, secondaryPublicKey,
               anotherSecondaryCiphertext.getR(), _zpSubgroup);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, anotherSecondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof that was generated with the wrong secondary public key',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               secondaryCiphertext, anotherSecondaryPublicKey, secondaryWitness,
               _zpSubgroup);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               anotherSecondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof that was generated with the wrong secondary witness',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, primaryWitness,
               secondaryCiphertext, secondaryPublicKey, anotherSecondaryWitness,
               _zpSubgroup);

           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof when using the wrong primary ciphertext',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               anotherPrimaryCiphertext, primaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, proof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof when using the wrong primary public key',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, anotherPrimaryPublicKey, secondaryCiphertext,
               secondaryPublicKey, proof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof when using the wrong secondary ciphertext',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, anotherSecondaryCiphertext,
               secondaryPublicKey, proof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext equality proof when using the wrong secondary public key',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification = box.proofs.service.verifyPlaintextEqualityProof(
               primaryCiphertext, primaryPublicKey, secondaryCiphertext,
               anotherSecondaryPublicKey, proof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and throw an exception when generating a plaintext equality proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 null, primaryPublicKey, primaryWitness, secondaryCiphertext,
                 secondaryPublicKey, secondaryWitness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, null, primaryWitness, secondaryCiphertext,
                 secondaryPublicKey, secondaryWitness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, null, secondaryCiphertext,
                 secondaryPublicKey, secondaryWitness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, primaryWitness, null,
                 secondaryPublicKey, secondaryWitness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, primaryWitness,
                 secondaryCiphertext, null, secondaryWitness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, primaryWitness,
                 secondaryCiphertext, secondaryPublicKey, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, primaryWitness,
                 secondaryCiphertext, secondaryPublicKey, secondaryWitness,
                 null);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext equality proof, using the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, primaryWitness,
                 secondaryCiphertext, secondaryPublicKey, secondaryWitness,
                 _anotherZpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext equality proof, using primary and secondary public keys with different lengths',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, aShorterPrimaryPublicKey, primaryWitness,
                 secondaryCiphertext, secondaryPublicKey, secondaryWitness,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext equality proof, using primary and secondary ciphertexts with different lengths',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 aShorterPrimaryCiphertext, primaryPublicKey,
                 aShorterPrimaryCiphertext.getR(), secondaryCiphertext,
                 secondaryPublicKey, secondaryWitness, _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext equality proof using public keys and ciphertexts with different lengths',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, aShorterPrimaryPublicKey, primaryWitness,
                 secondaryCiphertext, aShorterSecondaryPublicKey,
                 secondaryWitness, _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext equality proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, primaryWitness,
                 secondaryCiphertext, secondaryPublicKey, secondaryWitness,
                 PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext equality proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.createPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, primaryWitness,
                 secondaryCiphertext, secondaryPublicKey, secondaryWitness,
                 progressCallback, progressCallback);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing values for a plaintext equality proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.preComputePlaintextEqualityProof(
                 null, secondaryPublicKey, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.preComputePlaintextEqualityProof(
                 primaryPublicKey, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.preComputePlaintextEqualityProof(
                 primaryPublicKey, secondaryPublicKey, null);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext equality proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 null, primaryPublicKey, secondaryCiphertext,
                 secondaryPublicKey, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, null, secondaryCiphertext,
                 secondaryPublicKey, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, null, secondaryPublicKey,
                 proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, secondaryCiphertext, null,
                 proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, secondaryCiphertext,
                 secondaryPublicKey, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, secondaryCiphertext,
                 secondaryPublicKey, proof, null);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext equality proof, using the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, secondaryCiphertext,
                 secondaryPublicKey, proof, _anotherZpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext equality proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, secondaryCiphertext,
                 secondaryPublicKey, proof, _zpSubgroup,
                 PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext equality proof with a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.verifyPlaintextEqualityProof(
                 primaryCiphertext, primaryPublicKey, secondaryCiphertext,
                 secondaryPublicKey, proof, _zpSubgroup, progressCallback,
                 progressCallback);
           }).toThrow();
         });
       });
  });
});
