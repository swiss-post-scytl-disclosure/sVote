/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Proofs service ...', function() {
  'use strict';

  // Initialize variables common to all proof tests.
  beforeEach(initializeTestVars);

  // Plaintext exponent equality proof tests.
  describe('should be able to ..', function() {
    var num_plaintext_elements = 5;
    var num_progress_checks = num_plaintext_elements;

    var initialized = false;
    var plaintext;
    var ciphertext;
    var primaryWitness;
    var secondaryWitness;
    var proof;
    var preComputedValues;
    var anotherPlaintext;
    var anotherprimaryWitness;
    var anotherSecondaryWitness;
    var anotherCiphertext;
    var aShorterPlaintext;

    // Initialize variables common to plaintext exponent equality proof tests.
    beforeEach(function() {
      if (!initialized) {
        cryptolib('proofs.service', function(box) {
          plaintext = generatePlaintext(num_plaintext_elements);
          primaryWitness = generateRandomExponent(_zpSubgroup);
          secondaryWitness = generateRandomExponent(_zpSubgroup);
          ciphertext = generateExponentCiphertext(
              plaintext, primaryWitness, secondaryWitness);
          proof = box.proofs.service.createPlaintextExponentEqualityProof(
              ciphertext, plaintext, primaryWitness, secondaryWitness,
              _zpSubgroup);
          preComputedValues =
              box.proofs.service.preComputePlaintextExponentEqualityProof(
                  plaintext, _zpSubgroup);

          do {
            anotherPlaintext = generatePlaintext(num_plaintext_elements);
          } while (anotherPlaintext.equals(plaintext));
          do {
            anotherprimaryWitness = generateRandomExponent(_zpSubgroup);
          } while (anotherprimaryWitness.getValue().equals(
              primaryWitness.getValue()));
          do {
            anotherSecondaryWitness = generateRandomExponent(_zpSubgroup);
          } while (anotherSecondaryWitness.getValue().equals(
              secondaryWitness.getValue()));
          anotherCiphertext = generateExponentCiphertext(
              anotherPlaintext, primaryWitness, secondaryWitness);
          aShorterPlaintext = generatePlaintext(num_plaintext_elements - 1);
        });

        initialized = true;
      }
    });

    it('generate and verify a plaintext exponent equality proof', function() {
      cryptolib('proofs.service', function(box) {
        var verification =
            box.proofs.service.verifyPlaintextExponentEqualityProof(
                ciphertext, plaintext, proof, _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and generate and verify a plaintext exponent equality proof, using pre-computed values',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(preComputedValues).toBeDefined();

           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, plaintext, primaryWitness, secondaryWitness,
                   _zpSubgroup, preComputedValues);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and generate and verify a plaintext exponent equality proof, using null for the pre-computed values option',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, plaintext, primaryWitness, secondaryWitness,
                   _zpSubgroup, null);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('generate and verify a plaintext exponent equality proof, using serialization',
       function() {
         cryptolib('commons.mathematical', 'proofs', function(box) {
           var proofAsJson = proof.stringify();

           var proofFromJson = box.proofs.utils.deserializeProof(proofAsJson);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, proofFromJson, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('generate and verify a plaintext exponent equality proof, using pre-computed values and serialization',
       function() {
         cryptolib('commons.mathematical', 'proofs', function(box) {
           var preComputedValuesAsJson = preComputedValues.stringify();

           var preComputedValuesFromJson =
               box.proofs.utils.deserializeProofPreComputedValues(
                   preComputedValuesAsJson);

           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, plaintext, primaryWitness, secondaryWitness,
                   _zpSubgroup, preComputedValuesFromJson);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when generating a plaintext exponent equality proof',
       function() {
         cryptolib('proofs.service', function(box) {
           initializeProgressMeter();
           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, plaintext, primaryWitness, secondaryWitness,
                   _zpSubgroup, progressCallback,
                   PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

           expect(_progressPercentSum)
               .toBe(getProgressPercentSum(
                   num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
           expect(_progressPercentLatest).toBe(100);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when pre-computing values for a plaintext exponent equality proof',
       function() {
         cryptolib('proofs.service', function(box) {
           initializeProgressMeter();
           box.proofs.service.preComputePlaintextExponentEqualityProof(
               plaintext, _zpSubgroup, progressCallback,
               PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

           expect(_progressPercentSum)
               .toBe(getProgressPercentSum(
                   num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
           expect(_progressPercentLatest).toBe(100);

           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, plaintext, primaryWitness, secondaryWitness,
                   _zpSubgroup, preComputedValues);
           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when verifying a plaintext exponent equality proof',
       function() {
         cryptolib('proofs.service', function(box) {
           initializeProgressMeter();
           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, proof, _zpSubgroup, progressCallback,
                   PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           expect(verification).toBeTruthy();

           expect(_progressPercentSum)
               .toBe(getProgressPercentSum(
                   num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
           expect(_progressPercentLatest).toBe(100);
         });
       });

    it('and fail to verify a plaintext exponent equality proof that was generated with the wrong plaintext',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, anotherPlaintext, primaryWitness,
                   secondaryWitness, _zpSubgroup);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, anotherPlaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext exponent equality proof that was generated with the wrong primary witness',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherprimaryWitness,
                   secondaryWitness, _zpSubgroup);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext exponent equality proof that was generated with the wrong secondary witness',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof =
               box.proofs.service.createPlaintextExponentEqualityProof(
                   ciphertext, plaintext, primaryWitness,
                   anotherSecondaryWitness, _zpSubgroup);

           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, anotherProof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext exponent equality proof when using the wrong plaintext',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, anotherPlaintext, proof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext exponent equality proof when using the wrong ciphertext',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   anotherCiphertext, plaintext, proof, _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify a plaintext exponent equality proof when using the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           var verification =
               box.proofs.service.verifyPlaintextExponentEqualityProof(
                   ciphertext, plaintext, proof, _anotherZpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and throw an exception when generating a plaintext exponent equality proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 null, plaintext, primaryWitness, secondaryWitness,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             proof = box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, null, primaryWitness, secondaryWitness,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, plaintext, null, secondaryWitness, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, plaintext, primaryWitness, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, plaintext, primaryWitness, secondaryWitness, null);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext exponent equality proof, using the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, plaintext, primaryWitness, secondaryWitness,
                 _anotherZpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext exponent equality proof, using plaintext and ciphertext with different lengths',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, aShorterPlaintext, primaryWitness,
                 secondaryWitness, _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext exponent equality proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, plaintext, primaryWitness, secondaryWitness,
                 _zpSubgroup, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing values for a plaintext exponent equality proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.preComputePlaintextExponentEqualityProof(
                 null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.preComputePlaintextExponentEqualityProof(
                 plaintext, null);
           }).toThrow();
         });
       });

    it('and throw an exception when generating a plaintext exponent equality proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.createPlaintextExponentEqualityProof(
                 ciphertext, plaintext, primaryWitness, secondaryWitness,
                 _zpSubgroup, progressCallback, progressCallback);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext exponent equality proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyPlaintextExponentEqualityProof(
                 null, plaintext, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextExponentEqualityProof(
                 ciphertext, null, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextExponentEqualityProof(
                 ciphertext, plaintext, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyPlaintextExponentEqualityProof(
                 ciphertext, plaintext, proof, null);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext exponent equality proof, using plaintext and ciphertext with different lengths',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyPlaintextExponentEqualityProof(
                 ciphertext, aShorterPlaintext, proof, _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext exponent equality proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyPlaintextExponentEqualityProof(
                 ciphertext, plaintext, proof, _zpSubgroup,
                 PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying a plaintext exponent equality proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.verifyPlaintextExponentEqualityProof(
                 ciphertext, plaintext, proof, _zpSubgroup, progressCallback,
                 progressCallback);
           }).toThrow();
         });
       });
  });
});
