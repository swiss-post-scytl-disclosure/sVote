/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Proofs service ...', function() {
  'use strict';

  // Initialize variables common to all proof tests.
  beforeEach(initializeTestVars);

  // OR-proof tests.
  describe('should be able to ..', function() {
    var public_key_length = 1;
    var num_elements = 5;
    var max_data_length = 30;
    var num_progress_checks = num_elements * 2;

    var initialized = false;
    var publicKey;
    var elements;
    var index;
    var ciphertext;
    var witness;
    var data;
    var proof;
    var preComputedValues;
    var anotherElements;
    var anotherPublicKey;
    var anotherCiphertext;
    var anotherWitness;
    var aLongerPublicKey;
    var aLongerCiphertext;
    var aShorterElements;
    var aLongerElements;

    // Initialize variables common to OR-proof tests.
    beforeEach(function() {
      if (!initialized) {
        cryptolib('proofs.service', function(box) {
          publicKey = generateElGamalKeyPair(public_key_length).getPublicKey();
          elements = generatePlaintext(num_elements);
          index = generateRandomInteger(0, (num_elements - 1));
          ciphertext = generateCiphertext(publicKey, [elements[index]]);
          witness = ciphertext.getR();
          var dataLength = generateRandomInteger(1, max_data_length);
          data = generateRandomString(dataLength);

          proof = box.proofs.service.createORProof(
              publicKey, ciphertext, witness, elements, index, data,
              _zpSubgroup);
          preComputedValues = box.proofs.service.preComputeORProof(
              publicKey, num_elements, _zpSubgroup);

          do {
            anotherElements = generatePlaintext(num_elements);
          } while (anotherElements.equals(elements) ||
                   anotherElements[index].equals(elements[index]));
          do {
            anotherPublicKey =
                generateElGamalKeyPair(public_key_length).getPublicKey();
          } while (anotherPublicKey.getGroupElementsArray().equals(
              publicKey.getGroupElementsArray()));
          anotherCiphertext =
              generateCiphertext(anotherPublicKey, [elements[index]]);
          anotherWitness = anotherCiphertext.getR();
          do {
            aLongerPublicKey =
                generateElGamalKeyPair(public_key_length + 1).getPublicKey();
          } while (aLongerPublicKey.getGroupElementsArray().equals(
              publicKey.getGroupElementsArray()));
          aLongerCiphertext =
              generateCiphertext(aLongerPublicKey, [elements[0], elements[1]]);
          aShorterElements = generatePlaintext(1);
          aLongerElements = [];
          aLongerElements.addAll(elements);
          aLongerElements.push(elements[0]);
        });

        initialized = true;
      }
    });

    it('generate and verify an OR-proof', function() {
      cryptolib('proofs.service', function(box) {
        var verification = box.proofs.service.verifyORProof(
            publicKey, ciphertext, elements, data, proof, _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and generate and verify an OR-proof, using pre-computed values',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, witness, elements, index, data,
               _zpSubgroup, preComputedValues);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and generate and verify an OR-proof, using an empty string for the data option',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, witness, elements, index, '',
               _zpSubgroup);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, elements, '', anotherProof, _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and generate and verify an OR-proof, using null for the data option',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, witness, elements, index, null,
               _zpSubgroup);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, elements, null, anotherProof,
               _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and generate and verify an OR-proof, using null for the pre-computed values option',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, witness, elements, index, data,
               _zpSubgroup, null);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('generate and verify an OR-proof, using serialization', function() {
      cryptolib('commons.mathematical', 'proofs', function(box) {
        var proofAsJson = proof.stringify();

        var proofFromJson = box.proofs.utils.deserializeProof(proofAsJson);

        var verification = box.proofs.service.verifyORProof(
            publicKey, ciphertext, elements, data, proofFromJson, _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('generate and verify an OR-proof, using pre-computed values and serialization',
       function() {
         cryptolib('commons.mathematical', 'proofs', function(box) {
           var preComputedValuesAsJson = preComputedValues.stringify();

           var preComputedValuesFromJson =
               box.proofs.utils.deserializeProofPreComputedValues(
                   preComputedValuesAsJson);

           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, witness, elements, index, data,
               _zpSubgroup, preComputedValuesFromJson);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeTruthy();
         });
       });

    it('and measure progress when generating an OR-proof', function() {
      cryptolib('proofs.service', function(box) {
        initializeProgressMeter();
        var anotherProof = box.proofs.service.createORProof(
            publicKey, ciphertext, witness, elements, index, data, _zpSubgroup,
            progressCallback, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

        expect(_progressPercentSum)
            .toBe(getProgressPercentSum(
                num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
        expect(_progressPercentLatest).toBe(100);

        var verification = box.proofs.service.verifyORProof(
            publicKey, ciphertext, elements, data, anotherProof, _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and measure progress when pre-computing an OR-proof', function() {
      cryptolib('proofs.service', function(box) {
        initializeProgressMeter();
        preComputedValues = box.proofs.service.preComputeORProof(
            publicKey, num_elements, _zpSubgroup, progressCallback,
            PROGRESS_PERCENT_MIN_CHECK_INTERVAL);

        expect(_progressPercentSum)
            .toBe(getProgressPercentSum(
                num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
        expect(_progressPercentLatest).toBe(100);

        var anotherProof = box.proofs.service.createORProof(
            publicKey, ciphertext, witness, elements, index, data, _zpSubgroup,
            preComputedValues);
        var verification = box.proofs.service.verifyORProof(
            publicKey, ciphertext, elements, data, anotherProof, _zpSubgroup);
        expect(verification).toBeTruthy();
      });
    });

    it('and measure progress when verifying an OR-proof', function() {
      cryptolib('proofs.service', function(box) {
        initializeProgressMeter();
        var verification = box.proofs.service.verifyORProof(
            publicKey, ciphertext, elements, data, proof, _zpSubgroup,
            progressCallback, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
        expect(verification).toBeTruthy();

        expect(_progressPercentSum)
            .toBe(getProgressPercentSum(
                num_progress_checks, PROGRESS_PERCENT_MIN_CHECK_INTERVAL));
        expect(_progressPercentLatest).toBe(100);
      });
    });

    it('and fail to verify an OR-proof that was generated with the wrong public key',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               anotherPublicKey, ciphertext, witness, elements, index, data,
               _zpSubgroup);

           var verification = box.proofs.service.verifyORProof(
               anotherPublicKey, ciphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify an OR-proof that was generated with the wrong ciphertext',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               publicKey, anotherCiphertext, witness, elements, index, data,
               _zpSubgroup);

           var verification = box.proofs.service.verifyORProof(
               publicKey, anotherCiphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify an OR-proof that was generated with the wrong witness',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, anotherWitness, elements, index, data,
               _zpSubgroup);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify an OR-proof that was generated with the wrong elements',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, witness, anotherElements, index, data,
               _zpSubgroup);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, anotherElements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify an OR-proof that was pre-computed with the wrong public key',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherPreComputedValues = box.proofs.service.preComputeORProof(
               anotherPublicKey, num_elements, _zpSubgroup);

           var anotherProof = box.proofs.service.createORProof(
               publicKey, ciphertext, witness, elements, index, data,
               _zpSubgroup, anotherPreComputedValues);

           var verification = box.proofs.service.verifyORProof(
               publicKey, ciphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and fail to verify an OR-proof that was pre-computed and generated with the wrong public key',
       function() {
         cryptolib('proofs.service', function(box) {
           var anotherPreComputedValues = box.proofs.service.preComputeORProof(
               anotherPublicKey, num_elements, _zpSubgroup);

           var anotherProof = box.proofs.service.createORProof(
               anotherPublicKey, ciphertext, witness, elements, index, data,
               _zpSubgroup, anotherPreComputedValues);

           var verification = box.proofs.service.verifyORProof(
               anotherPublicKey, ciphertext, elements, data, anotherProof,
               _zpSubgroup);
           expect(verification).toBeFalsy();
         });
       });

    it('and throw an exception when generating an OR-proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 null, ciphertext, witness, elements, index, data, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, null, witness, elements, index, data, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, null, elements, index, data,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, null, index, data,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, elements, null, data,
                 _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, elements, index, data, null);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, elements, index, data,
                 _anotherzpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using a public key with more than one element',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 aLongerPublicKey, ciphertext, witness, elements, index, data,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using a ciphertext with more than two elements',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, aLongerCiphertext, witness, elements, index, data,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using less than two elements',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, aShorterElements, index, data,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using a negative index',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, elements, -1, data,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using an index that is greater than the number of elements minus one',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, elements, num_elements, data,
                 _zpSubgroup);
           }).toThrow();
         });
       });


    it('and throw an exception when generating an OR-proof from pre-computed values, using too many elements',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, aLongerElements, index, data,
                 _zpSubgroup, preComputedValues);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, elements, index, data,
                 _zpSubgroup, PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when generating an OR-proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.createORProof(
                 publicKey, ciphertext, witness, elements, index, data,
                 _zpSubgroup, progressCallback, progressCallback);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing an OR-proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.preComputeORProof(
                 null, num_elements, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.preComputeORProof(publicKey, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.preComputeORProof(
                 publicKey, num_elements, null);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing an OR-proof, using a public key with more than one element',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.preComputeORProof(
                 aLongerPublicKey, num_elements, _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing an OR-proof, using less than two elements',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.preComputeORProof(publicKey, 1, _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing an OR-proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.preComputeORProof(
                 publicKey, num_elements, _zpSubgroup,
                 PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when pre-computing an OR-proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.preComputeORProof(
                 publicKey, num_elements, _zpSubgroup, progressCallback,
                 progressCallback);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying an OR-proof, using null input data',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyORProof(
                 null, ciphertext, elements, data, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, null, elements, data, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, ciphertext, null, data, proof, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, ciphertext, elements, data, null, _zpSubgroup);
           }).toThrow();

           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, ciphertext, elements, data, proof, null);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying an OR-proof, using the wrong mathematical group',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, ciphertext, elements, data, proof,
                 _anotherZpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying an OR-proof, using a public key with more than one element',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyORProof(
                 aLongerPublicKey, ciphertext, elements, data, proof,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying an OR-proof, using a ciphertext with more than two elements',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, aLongerCiphertext, elements, data, proof,
                 _zpSubgroup);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying an OR-proof, using less than two elements',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, ciphertext, aShorterElements, data, proof,
                 _zpSubgroup);
           }).toThrow();
         });
       });


    it('and throw an exception when verifying an OR-proof, using a progress callback function that is not a function',
       function() {
         cryptolib('proofs.service', function(box) {
           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, ciphertext, elements, data, proof, _zpSubgroup,
                 PROGRESS_PERCENT_MIN_CHECK_INTERVAL);
           }).toThrow();
         });
       });

    it('and throw an exception when verifying an OR-proof, using a progress percent check interval that is not a number',
       function() {
         cryptolib('proofs.service', function(box) {
           var percentMeasuredLatest = 0;
           var progressCallback = function(progressPercent) {
             percentMeasuredLatest = progressPercent;
           };

           expect(function() {
             box.proofs.service.verifyORProof(
                 publicKey, ciphertext, elements, data, proof, _zpSubgroup,
                 progressCallback, progressCallback);
           }).toThrow();
         });
       });
  });
});
