/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Homomorphic cipher', function() {
  'use strict';

  it('should be able to encrypt and decrypt small messages', function() {
    cryptolib(
        'homomorphic.cipher', 'commons.utils', 'homomorphic.keypair',
        'commons.mathematical', function(box) {

          var elGamalCipherFactory =
              new box.homomorphic.cipher.factory.ElGamalCipherFactory();

          var keyFactory = new box.homomorphic.keypair.factory.KeyFactory();

          var q = new box.forge.jsbn.BigInteger('11');
          var generator = new box.forge.jsbn.BigInteger('2');
          var p = new box.forge.jsbn.BigInteger('23');
          var _smallZpSubgroup =
              new box.commons.mathematical.ZpSubgroup(generator, p, q);


          var messages = [];
          // Convenience method to add elements to `messages`.
          messages.add = function(value) {
            messages.push(new box.commons.mathematical.ZpGroupElement(
                new box.forge.jsbn.BigInteger(value), _smallZpSubgroup.getP(),
                _smallZpSubgroup.getQ()));
          };

          messages.add('1');
          messages.add('18');

          var converters = new box.commons.utils.Converters();

          var serializedElGamalPublicKey =
              '{"publicKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"elements":["Ag==","BA=="]}}';

          var serializedElGamalPublicKeyB64 =
              converters.base64Encode(serializedElGamalPublicKey);

          var elGamalPublicKey =
              keyFactory.createPublicKey(serializedElGamalPublicKeyB64);

          var serializedElGamalPrivateKey =
              '{"privateKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"exponents":["AQ==","Ag=="]}}';

          var serializedElGamalPrivateKeyB64 =
              converters.base64Encode(serializedElGamalPrivateKey);

          var elGamalPrivateKey =
              keyFactory.createPrivateKey(serializedElGamalPrivateKeyB64);

          var encrypter =
              elGamalCipherFactory.createEncrypter(elGamalPublicKey);

          var encryptedMessage = encrypter.encryptGroupElements(messages)
                                     .getElGamalComputationValues();

          var decrypter =
              elGamalCipherFactory.createDecrypter(elGamalPrivateKey);

          var decryptedMessage = decrypter.decrypt(encryptedMessage, true);

          expect(messages).toBeDefined();
          expect(decryptedMessage).toBeDefined();

          expect(decryptedMessage.length).toBe(messages.length);

          for (var i = 0; i < decryptedMessage.length; i++) {
            expect(decryptedMessage[i].equals(messages[i])).toBeTruthy();
          }
        });
  });

  it('should be able to encrypt and decrypt small messages with compression',
     function() {
       cryptolib(
           'homomorphic.cipher', 'commons.utils', 'homomorphic.keypair',
           'commons.mathematical', function(box) {

             var elGamalCipherFactory =
                 new box.homomorphic.cipher.factory.ElGamalCipherFactory();

             var keyFactory = new box.homomorphic.keypair.factory.KeyFactory();

             var q = new box.forge.jsbn.BigInteger('11');
             var generator = new box.forge.jsbn.BigInteger('2');
             var p = new box.forge.jsbn.BigInteger('23');
             var _smallZpSubgroup =
                 new box.commons.mathematical.ZpSubgroup(generator, p, q);


             var messages = [];

             var keyValue = box.forge.jsbn.BigInteger.ONE;
             var element = new box.commons.mathematical.ZpGroupElement(
                 keyValue, _smallZpSubgroup.getP(), _smallZpSubgroup.getQ());
             messages.push(element);

             keyValue = new box.forge.jsbn.BigInteger('18');
             element = new box.commons.mathematical.ZpGroupElement(
                 keyValue, _smallZpSubgroup.getP(), _smallZpSubgroup.getQ());
             messages.push(element);

             var converters = new box.commons.utils.Converters();

             var serializedElGamalPublicKey =
                 '{"publicKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"elements":["Ag==", "Ag==", "BA=="]}}';

             var serializedElGamalPublicKeyB64 =
                 converters.base64Encode(serializedElGamalPublicKey);

             var elGamalPublicKey =
                 keyFactory.createPublicKey(serializedElGamalPublicKeyB64);

             var serializedElGamalPrivateKey =
                 '{"privateKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"exponents":["AQ==","AQ==","Ag=="]}}';

             var serializedElGamalPrivateKeyB64 =
                 converters.base64Encode(serializedElGamalPrivateKey);

             var elGamalPrivateKey =
                 keyFactory.createPrivateKey(serializedElGamalPrivateKeyB64);

             var encrypter =
                 elGamalCipherFactory.createEncrypter(elGamalPublicKey);

             var encryptedMessage = encrypter.encryptGroupElements(messages)
                                        .getElGamalComputationValues();

             var decrypter =
                 elGamalCipherFactory.createDecrypter(elGamalPrivateKey);

             var decryptedMessage = decrypter.decrypt(encryptedMessage, true);

             expect(messages).toBeDefined();
             expect(decryptedMessage).toBeDefined();
             expect(decryptedMessage.length).toBe(messages.length);

             for (var k = 0; k < decryptedMessage.length; k++) {
               expect(decryptedMessage[k].equals(messages[k])).toBeTruthy();
             }
           });

     });

  it('ElGamalComputationValues::stringify() must return the expected value json {"ciphertext":{"p":"Fw==","q":"Cw==","gamma":"Aw==","phis":["BA==","BQ=="]}}',
     function() {
       cryptolib(
           'homomorphic.cipher', 'commons.utils', 'homomorphic.keypair',
           'commons.mathematical', function(box) {
             var p = new box.forge.jsbn.BigInteger('' + 23);
             var q = new box.forge.jsbn.BigInteger('' + 11);
             var gammaValue = new box.forge.jsbn.BigInteger('' + 3);
             var phiValue1 = new box.forge.jsbn.BigInteger('' + 4);
             var phiValue2 = new box.forge.jsbn.BigInteger('' + 5);

             var gamma =
                 new box.commons.mathematical.ZpGroupElement(gammaValue, p, q);

             var phi1 =
                 new box.commons.mathematical.ZpGroupElement(phiValue1, p, q);
             var phi2 =
                 new box.commons.mathematical.ZpGroupElement(phiValue2, p, q);


             var elGamalEncrypterValues =
                 new box.homomorphic.cipher.ElGamalEncrypterValues(
                     null, gamma, [phi1, phi2]);

             var jsonAsString =
                 elGamalEncrypterValues.getElGamalComputationValues()
                     .stringify();
             expect(jsonAsString).toContain('"ciphertext":');
             expect(jsonAsString).toContain('"p":"Fw=="');
             expect(jsonAsString).toContain('"q":"Cw=="');
             expect(jsonAsString).toContain('"gamma":"Aw=="');
             expect(jsonAsString).toContain('"phis":');
             expect(jsonAsString.length)
                 .toBe(
                     '{"ciphertext":{"p":"Fw==","q":"Cw==","gamma":"Aw==","phis":["BA==","BQ=="]}}'
                         .length);
           });
     });

  it('should be able to encrypt and decrypt large messages', function() {
    cryptolib(
        'homomorphic.cipher', 'commons.utils', 'homomorphic.keypair',
        'commons.mathematical', function(box) {
          var elGamalCipherFactory =
              new box.homomorphic.cipher.factory.ElGamalCipherFactory();

          var keyFactory = new box.homomorphic.keypair.factory.KeyFactory();

          var p = new box.forge.jsbn.BigInteger(
              '25878792566670842099842137716422866466252991028815773139028451679515364679624923581358662655689289205766441980239548823737806954397019411202244121935752456749381769565031670387914863935577896116425654849306598185507995737892509839616944496073707445338806101425467388977937489020456783676102620561970644684015868766028080049372849872115052208214439472603355483095640041515460851475971118272125133224007949688443680429668091313474118875081620746919907567682398209044343652147328622866834600839878114285018818463110227111614032671442085465843940709084719667865761125514800243342061732684028802646193202210299179139410607');
          var q = new box.forge.jsbn.BigInteger(
              '12939396283335421049921068858211433233126495514407886569514225839757682339812461790679331327844644602883220990119774411868903477198509705601122060967876228374690884782515835193957431967788948058212827424653299092753997868946254919808472248036853722669403050712733694488968744510228391838051310280985322342007934383014040024686424936057526104107219736301677741547820020757730425737985559136062566612003974844221840214834045656737059437540810373459953783841199104522171826073664311433417300419939057142509409231555113555807016335721042732921970354542359833932880562757400121671030866342014401323096601105149589569705303');
          var g = new box.forge.jsbn.BigInteger(
              '23337993065784550228812110720552652305178266477392633588884900695706615523553977368516877521940228584865573144621632575456086035440118913707895716109366641541746808409917179478292952139273396531060021729985473121368590574110220870149822495151519706210399569901298027813383104891697930149341258267962490850297875794622068418425473578455187344232698462829084010585324877420343904740081787639502967515631687068869545665294697583750184911025514712871193837246483893950501015755683415509019863976071649325968623617568219864744389709563087949389080252971419711636380986100047871404548371112472694814597772988558887480308242');
          var element1 = new box.forge.jsbn.BigInteger(
              '10813600900774814195443606333960059840422668831651681762833741452209135730650864452130214650095752196571587702814444040062441118243327189166429884552159103387262050740332278254634882734924804644475224947872860032029859548750165768253226371255576644038692950180048665862716256869418687777905840129397057458233193867066348913299433809742746548665012143326171826247072111249488645241753486115635552758795831876906471529661551590917603438603247246774800973230633192834223627007745551660750221242611378094020803537796039757375005102923882805858377750794407562467092342593013192207441450132449006118950054688176587133961409');
          var element2 = new box.forge.jsbn.BigInteger(
              '9853567379170719777334566185703517962033907422082302290399539440945952704005195929747815492644386838518442489536168205168599971300023125537903954233589099450715318090993081445448000242320602780219383745672665060815409904594899636902384560406070750905212550608478584637229051043378424454988191292610492156611060987968379597746425215082609688334075995471885941390029962061830957304051378886661637882858376301662242976957205952138813983434210148145737465280463387503441953147312554462853944227422544694220254912841114943534241459615181877522308107316310818302531331583848802731174268791963178918947077447910490965216158');
          var element3 = new box.forge.jsbn.BigInteger(
              '15794379152029066838713804099734041126263932884995939213200727618402701551640677264676158281588744352993384547095094861783499389509798314014022069284171927996645400863301983568835178765405753107568243261485121885250020254379553972077632932375247364025813645736274557285578258334201060356153010774984159753076425595005818465482275931002143043064230447825744365372619735966376047086115776775312766029914867090765966581873109229928865155261673639010273082385667041760650533946369556580260795606883221254081319827267155147350340767060491743854037592935823565514546490962430394933911057965977363230092438984010824700325167');
          var exponent1 = new box.forge.jsbn.BigInteger(
              '19932290298126983822867373246283694543834419444448689387027938121267278297690369083865205185937225553946698937771529278177448965152988087940288333275652867866197468584904193639808285279573404886376170146138923013466997865394411013033589133127405286904582891808686947313183243255910228279949181818238658864287585958169067297934354631387987451313123988211850808977971146508607133777397002899865332248785578309214179182602443053969288258864213973827536851717416916385105956235720163370004298189528382210074328598493302826727281275733318359279641378534728810069395703258431116367239913924616427295756294583567073396132741');
          var exponent2 = new box.forge.jsbn.BigInteger(
              '7577689178299053378025254165498777990855334617862256718327226463721764741583484454093269000328148335682856492924773910667544587336325922889256151180332100653754327285144917296147066439365449004239784202513534311409488352456747202671798659147817096528453115200056937800230539886947124207663419998972244638553002561054271980225111687656902641911235998639469734777832498679320977231339705464833025304454996110583985661736573069868478918748203761800351243404612284301662483435138804753635654540381762463438773405967794218429676239599853078295451426556494902828889387202479882230599369972987637455867152360420905963929754');
          var exponent3 = new box.forge.jsbn.BigInteger(
              '129547331161844130899924173735104728702220700682642138851022406761777994632728337153635583150724889632689084497665212926336088840842855597736181764744122977345480665146165524664735861913761423291109155186483836806324970977794125768575887671259015293979900588616487195411477129839925141145884519189238456836206862720534398725375349481201999965487100563522135988094619173050092410477941592825803862292425503577691441721033806466916501473758812704373810862790693842410609705251142958265348495650713103821889837809522418329172422392659115758749365074645804533890934339070499112165048929164809093509978635413193830265570');

          var _smallZpSubgroup =
              new box.commons.mathematical.ZpSubgroup(g, p, q);

          var messages = [];

          // Convenience method to populate `messages` that helps with
          // readability.
          messages.add = function(value) {
            this.push(new box.commons.mathematical.ZpGroupElement(
                new box.forge.jsbn.BigInteger(value), _smallZpSubgroup.getP(),
                _smallZpSubgroup.getQ()));
          };

          messages.add('197');
          messages.add('199');
          messages.add('211');

          var converters = new box.commons.utils.Converters();
          var serializedElGamalPublicKey = '{"publicKey":{"zpSubgroup":{"p":"' +
              converters.base64FromBigInteger(p) + '","q":"' +
              converters.base64FromBigInteger(q) + '","g":"' +
              converters.base64FromBigInteger(g) + '"},"elements":["' +
              converters.base64FromBigInteger(element1) + '","' +
              converters.base64FromBigInteger(element2) + '","' +
              converters.base64FromBigInteger(element3) + '"]}}';


          var serializedElGamalPublicKeyB64 =
              converters.base64Encode(serializedElGamalPublicKey);

          var elGamalPublicKey =
              keyFactory.createPublicKey(serializedElGamalPublicKeyB64);

          var serializedElGamalPrivateKey =
              '{"privateKey":{"zpSubgroup":{"p":"' +
              converters.base64FromBigInteger(p) + '","q":"' +
              converters.base64FromBigInteger(q) + '","g":"' +
              converters.base64FromBigInteger(g) + '"},"exponents":["' +
              converters.base64FromBigInteger(exponent1) + '","' +
              converters.base64FromBigInteger(exponent2) + '","' +
              converters.base64FromBigInteger(exponent3) + '"]}}';

          var serializedElGamalPrivateKeyB64 =
              converters.base64Encode(serializedElGamalPrivateKey);

          var elGamalPrivateKey =
              keyFactory.createPrivateKey(serializedElGamalPrivateKeyB64);

          var encrypter =
              elGamalCipherFactory.createEncrypter(elGamalPublicKey);

          var encryptedMessage = encrypter.encryptGroupElements(messages)
                                     .getElGamalComputationValues();

          var decrypter =
              elGamalCipherFactory.createDecrypter(elGamalPrivateKey);

          var decryptedMessage = decrypter.decrypt(encryptedMessage, true);

          expect(messages).toBeDefined();
          expect(decryptedMessage).toBeDefined();

          expect(decryptedMessage.length === messages.length);

          for (var k = 0; k < decryptedMessage.length; k++) {
            expect(decryptedMessage[k].equals(messages[k])).toBeTruthy();
          }

          // error when messages.length > keys length
          expect(function() {
            messages.add('199');
            encrypter.encryptGroupElements(messages)
                .getElGamalComputationValues();
          }).toThrow();
        });
  });

  it('should be able to encrypt and decrypt large messages as strings', function() {
    cryptolib(
        'homomorphic.cipher', 'commons.utils', 'homomorphic.keypair',
        'commons.mathematical', function(box) {

          var elGamalCipherFactory =
              new box.homomorphic.cipher.factory.ElGamalCipherFactory();

          var keyFactory = new box.homomorphic.keypair.factory.KeyFactory();

          var q = new box.forge.jsbn.BigInteger(
              '12939396283335421049921068858211433233126495514407886569514225839757682339812461790679331327844644602883220990119774411868903477198509705601122060967876228374690884782515835193957431967788948058212827424653299092753997868946254919808472248036853722669403050712733694488968744510228391838051310280985322342007934383014040024686424936057526104107219736301677741547820020757730425737985559136062566612003974844221840214834045656737059437540810373459953783841199104522171826073664311433417300419939057142509409231555113555807016335721042732921970354542359833932880562757400121671030866342014401323096601105149589569705303');
          var generator = new box.forge.jsbn.BigInteger(
              '23337993065784550228812110720552652305178266477392633588884900695706615523553977368516877521940228584865573144621632575456086035440118913707895716109366641541746808409917179478292952139273396531060021729985473121368590574110220870149822495151519706210399569901298027813383104891697930149341258267962490850297875794622068418425473578455187344232698462829084010585324877420343904740081787639502967515631687068869545665294697583750184911025514712871193837246483893950501015755683415509019863976071649325968623617568219864744389709563087949389080252971419711636380986100047871404548371112472694814597772988558887480308242');
          var p = new box.forge.jsbn.BigInteger(
              '25878792566670842099842137716422866466252991028815773139028451679515364679624923581358662655689289205766441980239548823737806954397019411202244121935752456749381769565031670387914863935577896116425654849306598185507995737892509839616944496073707445338806101425467388977937489020456783676102620561970644684015868766028080049372849872115052208214439472603355483095640041515460851475971118272125133224007949688443680429668091313474118875081620746919907567682398209044343652147328622866834600839878114285018818463110227111614032671442085465843940709084719667865761125514800243342061732684028802646193202210299179139410607');
          var _smallZpSubgroup =
              new box.commons.mathematical.ZpSubgroup(generator, p, q);

          var messages = [];
          messages.push('197');
          messages.push('199');
          messages.push('211');

          var elements = [];

          var exponents = [];

          var value = new box.forge.jsbn.BigInteger(
              '10813600900774814195443606333960059840422668831651681762833741452209135730650864452130214650095752196571587702814444040062441118243327189166429884552159103387262050740332278254634882734924804644475224947872860032029859548750165768253226371255576644038692950180048665862716256869418687777905840129397057458233193867066348913299433809742746548665012143326171826247072111249488645241753486115635552758795831876906471529661551590917603438603247246774800973230633192834223627007745551660750221242611378094020803537796039757375005102923882805858377750794407562467092342593013192207441450132449006118950054688176587133961409');
          var groupElement = new box.commons.mathematical.ZpGroupElement(
              value, _smallZpSubgroup.getP(), _smallZpSubgroup.getQ());
          elements.push(groupElement);

          value = new box.forge.jsbn.BigInteger(
              '9853567379170719777334566185703517962033907422082302290399539440945952704005195929747815492644386838518442489536168205168599971300023125537903954233589099450715318090993081445448000242320602780219383745672665060815409904594899636902384560406070750905212550608478584637229051043378424454988191292610492156611060987968379597746425215082609688334075995471885941390029962061830957304051378886661637882858376301662242976957205952138813983434210148145737465280463387503441953147312554462853944227422544694220254912841114943534241459615181877522308107316310818302531331583848802731174268791963178918947077447910490965216158');
          groupElement = new box.commons.mathematical.ZpGroupElement(
              value, _smallZpSubgroup.getP(), _smallZpSubgroup.getQ());
          elements.push(groupElement);

          value = new box.forge.jsbn.BigInteger(
              '15794379152029066838713804099734041126263932884995939213200727618402701551640677264676158281588744352993384547095094861783499389509798314014022069284171927996645400863301983568835178765405753107568243261485121885250020254379553972077632932375247364025813645736274557285578258334201060356153010774984159753076425595005818465482275931002143043064230447825744365372619735966376047086115776775312766029914867090765966581873109229928865155261673639010273082385667041760650533946369556580260795606883221254081319827267155147350340767060491743854037592935823565514546490962430394933911057965977363230092438984010824700325167');
          groupElement = new box.commons.mathematical.ZpGroupElement(
              value, _smallZpSubgroup.getP(), _smallZpSubgroup.getQ());
          elements.push(groupElement);

          var elGamalPublicKey =
              keyFactory.createPublicKey(elements, _smallZpSubgroup);

          value = new box.forge.jsbn.BigInteger(
              '19932290298126983822867373246283694543834419444448689387027938121267278297690369083865205185937225553946698937771529278177448965152988087940288333275652867866197468584904193639808285279573404886376170146138923013466997865394411013033589133127405286904582891808686947313183243255910228279949181818238658864287585958169067297934354631387987451313123988211850808977971146508607133777397002899865332248785578309214179182602443053969288258864213973827536851717416916385105956235720163370004298189528382210074328598493302826727281275733318359279641378534728810069395703258431116367239913924616427295756294583567073396132741');
          var exponent = new box.commons.mathematical.Exponent(
              _smallZpSubgroup.getQ(), value);
          exponents.push(exponent);

          value = new box.forge.jsbn.BigInteger(
              '7577689178299053378025254165498777990855334617862256718327226463721764741583484454093269000328148335682856492924773910667544587336325922889256151180332100653754327285144917296147066439365449004239784202513534311409488352456747202671798659147817096528453115200056937800230539886947124207663419998972244638553002561054271980225111687656902641911235998639469734777832498679320977231339705464833025304454996110583985661736573069868478918748203761800351243404612284301662483435138804753635654540381762463438773405967794218429676239599853078295451426556494902828889387202479882230599369972987637455867152360420905963929754');
          exponent = new box.commons.mathematical.Exponent(
              _smallZpSubgroup.getQ(), value);
          exponents.push(exponent);

          value = new box.forge.jsbn.BigInteger(
              '129547331161844130899924173735104728702220700682642138851022406761777994632728337153635583150724889632689084497665212926336088840842855597736181764744122977345480665146165524664735861913761423291109155186483836806324970977794125768575887671259015293979900588616487195411477129839925141145884519189238456836206862720534398725375349481201999965487100563522135988094619173050092410477941592825803862292425503577691441721033806466916501473758812704373810862790693842410609705251142958265348495650713103821889837809522418329172422392659115758749365074645804533890934339070499112165048929164809093509978635413193830265570');
          exponent = new box.commons.mathematical.Exponent(
              _smallZpSubgroup.getQ(), value);
          exponents.push(exponent);

          var elGamalPrivateKey =
              keyFactory.createPrivateKey(exponents, _smallZpSubgroup);

          var encrypter =
              elGamalCipherFactory.createEncrypter(elGamalPublicKey);

          var encryptedMessage =
              encrypter.encryptStrings(messages).getElGamalComputationValues();

          var decrypter =
              elGamalCipherFactory.createDecrypter(elGamalPrivateKey);

          var decryptedMessage = decrypter.decrypt(encryptedMessage, true);

          expect(messages).toBeDefined();
          expect(decryptedMessage).toBeDefined();

          expect(decryptedMessage.length === messages.length);

          for (var k = 0; k < decryptedMessage.length; k++) {
            var keyValue = new box.forge.jsbn.BigInteger(messages[k]);
            var e = new box.commons.mathematical.ZpGroupElement(
                keyValue, _smallZpSubgroup.getP(), _smallZpSubgroup.getQ());
            expect(decryptedMessage[k].equals(e)).toBeTruthy();
          }
        });
  });

  describe('Computation values', function() {
    it('must be deserialized from a provided serialized string', function() {
      cryptolib(
          'homomorphic.cipher', 'commons.utils', 'commons.mathematical',
          function(box) {
            var computationValues =
                box.homomorphic.cipher.deserializeElGamalComputationValues(
                    '{"ciphertext":{"p":"BA==","q":"Ag==","gamma":"Ag==","phis":["Aw=="]}}');

            expect(computationValues.getGamma().getP().toString()).toEqual('4');
            expect(computationValues.getGamma().getQ().toString()).toEqual('2');
            expect(computationValues.getGamma().getElementValue().toString())
                .toEqual('2');
            expect(computationValues.getPhis()[0].getElementValue().toString())
                .toEqual('3');
          });
    });

    it('must be serialized to a string', function() {
      cryptolib(
          'homomorphic.cipher', 'commons.utils', 'commons.mathematical',
          function(box) {
            var serializedObject =
                '{"ciphertext":{"p":"BA==","q":"Ag==","gamma":"Ag==","phis":["Aw=="]}}';
            var computationValues =
                box.homomorphic.cipher.deserializeElGamalComputationValues(
                    serializedObject);

            var jsonAsString = computationValues.stringify();
            expect(jsonAsString).toContain('"ciphertext":{');
            expect(jsonAsString).toContain('"p":"BA=="');
            expect(jsonAsString).toContain('"q":"Ag=="');
            expect(jsonAsString).toContain('"gamma":"Ag=="');
            expect(jsonAsString).toContain('"phis":["Aw=="]');
            expect(jsonAsString.length).toBe(serializedObject.length);
          });
    });
  });
});
