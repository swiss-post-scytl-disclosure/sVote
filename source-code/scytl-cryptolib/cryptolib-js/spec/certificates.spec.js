/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe(
    'Certificates: ', function() {
      'use strict';

      var _rootCertificatePem =
          '-----BEGIN CERTIFICATE-----MIIEKjCCAxKgAwIBAgIQQtWFdPN4NAvUIWyyJyUlbTANBgkqhkiG9w0BAQsFADBUMRAwDgYDVQQDDAdSb290IENOMRYwFAYDVQQLDA1Sb290IE9yZyBVbml0MREwDwYDVQQKDAhSb290IE9yZzEVMBMGA1UEBhMMUm9vdCBDb3VudHJ5MB4XDTE0MDYxODEwMjMyOFoXDTE1MDYxODEwMjMyOFowVDEQMA4GA1UEAwwHUm9vdCBDTjEWMBQGA1UECwwNUm9vdCBPcmcgVW5pdDERMA8GA1UECgwIUm9vdCBPcmcxFTATBgNVBAYTDFJvb3QgQ291bnRyeTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJ4AidkWId1zr4IZgItjE9uv38eB8fIGJGEts2n/XNh/EJ7NYZyBzvKSJR83v7LVJjc4pup5crWyS0B5lQ7uuD3/XZ9QpQFtnGiqlKOKH45zsw3ekaHEAco07L2dZznBuQTGLTlhrmvkCFRa8b8WYC+k90oUPvSd/9S4kA1Jlo9JDKHLer0SbjZCcVRXoLSBKmGWE0xSNfmNuZNONDRbtHvSA8A10AOtxKii9w464MXYzmPil7uM1Og1HC+FXCmkzLNfqQ31Om0jra3nLmrCpBJjRPX7svVnoxajRqpazVQnmJjjpzV7yNLwnR9W8OPwqanXcbxmTkrXMxfLxiVXDFUCAwEAAaOB9zCB9DAPBgNVHRMBAf8EBTADAQH/MDUGCCsGAQUFBwEBAQH/BCYwJDAiBggrBgEFBQcwAYYWaHR0cDovL29jc3AudGhhd3RlLmNvbTA0BgNVHR8BAf8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZlcmlzaWduLmNvbS9wY2EzLmNybDArBgNVHREBAf8EITAfpB0wGzEZMBcGA1UEAwwQUHJpdmF0ZUxhYmVsMy0xNTAOBgNVHQ8BAf8EBAMCAQYwNwYDVR0lAQH/BC0wKwYIKwYBBQUHAwEGCCsGAQUFBwMCBgpghkgBhvhFAQgBBglghkgBhvhCBAEwDQYJKoZIhvcNAQELBQADggEBADmtmjApZAXIkGLaZCdkRnhel53BtEdQnG990Oo/tBBboqy2ipum9ByTj3hNWJB3zuPN77rkrek9rbookNcCgVWhHtTk1lUpUK6ZohDsZh8k0MqIhkz+X+HiWGRsEOptjsCaknyWcWb4aXAevMAQMPm/ktkpQ8AOxAq+gtieewWQZP3kGPhBBCfn8TGjdrn9+ymf8EIbAUFXQ8m+oWeNlrdWhqzRXwQbj4EDds1kZdTo0nCYUdH+XEBF9nMyhAxSQWzCKQQTRFWv1dr3dKapzfgrdH8wEgvptiBYCY62O5+3DxiNK/VWquHz6S5GqIwkmSPDPMUU/qK3SNG3xIL1U1k=-----END CERTIFICATE-----';
      var _rootRsaPublicKeyPem =
          '-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAngCJ2RYh3XOvghmAi2MT26/fx4Hx8gYkYS2zaf9c2H8Qns1hnIHO8pIlHze/stUmNzim6nlytbJLQHmVDu64Pf9dn1ClAW2caKqUo4ofjnOzDd6RocQByjTsvZ1nOcG5BMYtOWGua+QIVFrxvxZgL6T3ShQ+9J3/1LiQDUmWj0kMoct6vRJuNkJxVFegtIEqYZYTTFI1+Y25k040NFu0e9IDwDXQA63EqKL3DjrgxdjOY+KXu4zU6DUcL4VcKaTMs1+pDfU6bSOtrecuasKkEmNE9fuy9WejFqNGqlrNVCeYmOOnNXvI0vCdH1bw4/CpqddxvGZOStczF8vGJVcMVQIDAQAB-----END PUBLIC KEY-----';
      var _rootSerialNumber = '88837713778966677489555326888277517677';
      var _rootNotBefore = '1403087008';
      var _rootNotAfter = '1434623008';
      var _rootCn = 'Root CN';
      // var _rootOrg = 'Root Org';
      // var _rootOrgUnit = 'Root Org Unit';
      var _rootCountry = 'Root Country';
      var _userRsaPublicKeyPem =
          '-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAglypZU45bnf2opnRI+Y51VqouBpvDT33xIB/OtbwKzwVpi+JrjBFtfk33tE9t/dSRs79CK94HRhCWcOiLa2qWPrjeZ9SBiEyScrhIvRZVBF41zBgwQNuRvJCsKmAqlZaFNJDZxEP4repmlBn1CfVFmfrXmOKqwP5F7l9ZtucveRzsfmF1yVPFkW8TMuB3YqMiyymyqHlS8ujCsu5I8tpgPbwuxdMOY94fNhSXrYkY8IuX1g1zdq/Z1jluOaR/UqK4UpnbuJaH/F0VgDNiWh6cTD0DFGEk0b70i5wU4Q3L/S6XZQRvSuADoCbhwBKuFL5pW5n865oLVb5S3wuVdWaGwIDAQAB-----END PUBLIC KEY-----';
      var _userCertificatePem =
          '-----BEGIN CERTIFICATE-----MIIEMzCCAxugAwIBAgIQRbaPaToIM+VS/d6etgYZ4jANBgkqhkiG9w0BAQsFADBUMRAwDgYDVQQDDAdSb290IENOMRYwFAYDVQQLDA1Sb290IE9yZyBVbml0MREwDwYDVQQKDAhSb290IE9yZzEVMBMGA1UEBhMMUm9vdCBDb3VudHJ5MB4XDTE0MDYxODEwMjMyOFoXDTE1MDYxODEwMjMyOFowYDETMBEGA1UEAwwKU3ViamVjdCBDTjEZMBcGA1UECwwQU3ViamVjdCBPcmcgVW5pdDEUMBIGA1UECgwLU3ViamVjdCBPcmcxGDAWBgNVBAYTD1N1YmplY3QgQ291bnRyeTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAIJcqWVOOW539qKZ0SPmOdVaqLgabw0998SAfzrW8Cs8FaYvia4wRbX5N97RPbf3UkbO/QiveB0YQlnDoi2tqlj643mfUgYhMknK4SL0WVQReNcwYMEDbkbyQrCpgKpWWhTSQ2cRD+K3qZpQZ9Qn1RZn615jiqsD+Re5fWbbnL3kc7H5hdclTxZFvEzLgd2KjIsspsqh5UvLowrLuSPLaYD28LsXTDmPeHzYUl62JGPCLl9YNc3av2dY5bjmkf1KiuFKZ27iWh/xdFYAzYloenEw9AxRhJNG+9IucFOENy/0ul2UEb0rgA6Am4cASrhS+aVuZ/OuaC1W+Ut8LlXVmhsCAwEAAaOB9DCB8TAMBgNVHRMBAf8EAjAAMDUGCCsGAQUFBwEBAQH/BCYwJDAiBggrBgEFBQcwAYYWaHR0cDovL29jc3AudGhhd3RlLmNvbTA0BgNVHR8BAf8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZlcmlzaWduLmNvbS9wY2EzLmNybDArBgNVHREBAf8EITAfpB0wGzEZMBcGA1UEAwwQUHJpdmF0ZUxhYmVsMy0xNTAOBgNVHQ8BAf8EBAMCBsAwNwYDVR0lAQH/BC0wKwYIKwYBBQUHAwEGCCsGAQUFBwMCBgpghkgBhvhFAQgBBglghkgBhvhCBAEwDQYJKoZIhvcNAQELBQADggEBAAWZDJD6bg4ohHewszrAbL2tdUNxhrwCgNaHUhwNK43kiLGH0U9innhL1i0jP1VHNkL1G/+ZCo1qzh/Usji/jtlurfAWtrXku6VRF9NP+itKOY5jJ91Ijkc7t4dgoeJq6iMHn6JbDKIQ88r/Ikd0GdF04o5Qjqq1HlUVmqyIOHeHFla4i4tOxTyUBj34eE1No/xmaKYV1QtR1dqSHblR7OagEo7Dd3fXp7iSrKrXaN0Ef/6zeF3zjU5SMKcUcU9d3CbhS/CrGb+UGlqTXgzPXQWESH9AqBNl67+HF3mYktDQOZYPT5WRO5IKSko2cy9pP9UCsLk4oU3xyOxacWDpk1k=-----END CERTIFICATE-----';
      var _userSerialNumber = '92664638458902967092551440174596626914';
      var _userNotBefore = '1403087008';
      var _userNotAfter = '1434623008';
      var _userCn = 'Subject CN';
      // var _userOrg = 'Subject Org';
      // var _userOrgUnit = 'Subject Org Unit';
      var _userCountry = 'Subject Country';

      // Test opening root certificate.
      describe('Should be loaded if cryptolib is called with *: ', function() {
        it('should be defined a certificates.service objects', function() {
          cryptolib('*', function(box) {
            expect(box.certificates.service).toBeDefined();
          });
        });
      });

      describe('Root Certificate ...', function() {
        describe('should be able to ..', function() {
          it('retrieve its fields and public key, when certificate was generated using Java.',
             function() {
               cryptolib('certificates', function(box) {
                 // Retrieve root certificate.
                 var rootCertificate =
                     new box.certificates.service.load(_rootCertificatePem);

                 // Check whether expected fields can be retrieved from root
                 // certificate.
                 expect(
                     new forge.jsbn.BigInteger(
                         rootCertificate.getSerialNumber(), box.BASE_16_RADIX))
                     .toEqual(new forge.jsbn.BigInteger(_rootSerialNumber));
                 var notBefore = rootCertificate.getNotBefore()
                                     .getTime()
                                     .toString()
                                     .substring(0, box.EPOCH_TIME_LENGTH);
                 expect(notBefore).toBe(_rootNotBefore);
                 var notAfter = rootCertificate.getNotAfter()
                                    .getTime()
                                    .toString()
                                    .substring(0, box.EPOCH_TIME_LENGTH);
                 expect(notAfter).toBe(_rootNotAfter);
                 expect(rootCertificate.getSubjectCN()).toBe(_rootCn);
                 expect(rootCertificate.getSubjectCountry()).toBe(_rootCountry);
                 expect(rootCertificate.getIssuerCN()).toBe(_rootCn);
                 expect(rootCertificate.getIssuerCountry()).toBe(_rootCountry);

                 // Retrieve public key from root certificate.
                 var rootPublicKeyPem =
                     rootCertificate.getPublicKey().toString().replace(
                         /(\r\n|\n|\r)/gm, '');
                 var rootPublicKey =
                     forge.pki.publicKeyFromPem(rootPublicKeyPem);

                 // Check whether public key retrieved from root certificate is
                 // same as original.
                 expect(rootPublicKeyPem).toBe(_rootRsaPublicKeyPem);
                 var rootPublicKeyOrig =
                     forge.pki.publicKeyFromPem(_rootRsaPublicKeyPem);
                 expect(rootPublicKey.e).toEqual(rootPublicKeyOrig.e);
                 expect(rootPublicKey.n).toEqual(rootPublicKeyOrig.n);

                 // Retrieve key usage extension fields.
                 var keyUsageExtension = rootCertificate.getKeyUsageExtension();

                 // Check whether expected key usage flags can be retrieved from
                 // certificate.
                 expect(keyUsageExtension.digitalSignature()).toBe(false);
                 expect(keyUsageExtension.nonRepudiation()).toBe(false);
                 expect(keyUsageExtension.keyEncipherment()).toBe(false);
                 expect(keyUsageExtension.dataEncipherment()).toBe(false);
                 expect(keyUsageExtension.keyAgreement()).toBe(false);
                 expect(keyUsageExtension.keyCertSign()).toBe(true);
                 expect(keyUsageExtension.crlSign()).toBe(true);
                 expect(keyUsageExtension.encipherOnly()).toBe(false);
                 expect(keyUsageExtension.decipherOnly()).toBe(false);

                 // Check whether signature of root certificate with its own
                 // public key (since it is self-signed).
                 expect(rootCertificate.verify(_rootCertificatePem)).toBe(true);
               });
             });
        });
      });

      // Test opening user certificate.
      describe('User Certificate ...', function() {
        describe('should be able to ..', function() {
          it('retrieve its fields and public key, when certificate was generated using Java.',
             function() {
               cryptolib('certificates', function(box) {

                 // Retrieve user certificate.
                 var userCertificate =
                     new box.certificates.CryptoX509Certificate(
                         _userCertificatePem);

                 // Check whether expected fields can be retrieved from user
                 // certificate.
                 expect(
                     new forge.jsbn.BigInteger(
                         userCertificate.getSerialNumber(), box.BASE_16_RADIX))
                     .toEqual(new forge.jsbn.BigInteger(_userSerialNumber));
                 var notBefore = userCertificate.getNotBefore()
                                     .getTime()
                                     .toString()
                                     .substring(0, box.EPOCH_TIME_LENGTH);
                 expect(notBefore).toBe(_userNotBefore);
                 var notAfter = userCertificate.getNotAfter()
                                    .getTime()
                                    .toString()
                                    .substring(0, box.EPOCH_TIME_LENGTH);
                 expect(notAfter).toBe(_userNotAfter);
                 expect(userCertificate.getSubjectCN()).toBe(_userCn);
                 expect(userCertificate.getSubjectCountry()).toBe(_userCountry);
                 expect(userCertificate.getIssuerCN()).toBe(_rootCn);
                 expect(userCertificate.getIssuerCountry()).toBe(_rootCountry);

                 // Retrieve public key from user certificate.
                 var userPublicKeyPem =
                     userCertificate.getPublicKey().toString().replace(
                         /(\r\n|\n|\r)/gm, '');
                 var userPublicKey =
                     forge.pki.publicKeyFromPem(userPublicKeyPem);

                 // Check whether public key retrieved from certificate is same
                 // as original.
                 expect(userPublicKeyPem).toBe(_userRsaPublicKeyPem);
                 var userPublicKeyOrig =
                     forge.pki.publicKeyFromPem(_userRsaPublicKeyPem);
                 expect(userPublicKey.e).toEqual(userPublicKeyOrig.e);
                 expect(userPublicKey.n).toEqual(userPublicKeyOrig.n);

                 // Retrieve key usage extension fields.
                 var keyUsageExtension = userCertificate.getKeyUsageExtension();

                 // Check whether expected key usage flags can be retrieved from
                 // certificate.
                 expect(keyUsageExtension.digitalSignature()).toBe(true);
                 expect(keyUsageExtension.nonRepudiation()).toBe(true);
                 expect(keyUsageExtension.keyEncipherment()).toBe(false);
                 expect(keyUsageExtension.dataEncipherment()).toBe(false);
                 expect(keyUsageExtension.keyAgreement()).toBe(false);
                 expect(keyUsageExtension.keyCertSign()).toBe(false);
                 expect(keyUsageExtension.crlSign()).toBe(false);
                 expect(keyUsageExtension.encipherOnly()).toBe(false);
                 expect(keyUsageExtension.decipherOnly()).toBe(false);

                 // Verify signature of user certificate with root certificate's
                 // public key.
                 var rootCertificate =
                     new box.certificates.CryptoX509Certificate(
                         _rootCertificatePem);
                 expect(rootCertificate.verify(_userCertificatePem)).toBe(true);
               });
             });
        });
      });

      it('should exist a validator object', function() {
        cryptolib('certificates', function(box) {
          var validator = new box.certificates.CryptoX509CertificateValidator();

          expect(validator).toBeDefined();
        });
      });

      describe('Validator', function() {
        it('should validate the subject DN', function() {
          cryptolib('certificates', function(box) {

            var validationData = {
              subject: {
                commonName: 'Subject CN',
                organization: 'Subject Org',
                organizationUnit: 'Subject Org Unit',
                country: 'Subject Country'
              }
            };

            var validator = new box.certificates.CryptoX509CertificateValidator(
                validationData);
            var validations = validator.validate(_userCertificatePem);
            expect(validations.length).toBe(0);
          });
        });

        it('should fail the validation if subject CN is different from what expected',
           function() {
             cryptolib('certificates', function(box) {

               var validationData = {
                 subject: {
                   commonName: 'Subject CN1',
                   organization: 'Subject Org',
                   organizationUnit: 'Subject Org Unit',
                   country: 'Subject Country'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('SUBJECT');
             });
           });

        it('should fail the validation if subject O is different from what expected',
           function() {
             cryptolib('certificates', function(box) {

               var validationData = {
                 subject: {
                   commonName: 'Subject CN',
                   organization: 'Subject Org1',
                   organizationUnit: 'Subject Org Unit',
                   country: 'Subject Country'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('SUBJECT');
             });
           });

        it('should fail the validation if subject OU is different from what expected',
           function() {
             cryptolib('certificates', function(box) {

               var validationData = {
                 subject: {
                   commonName: 'Subject CN',
                   organization: 'Subject Org',
                   organizationUnit: 'Subject Org Unit1',
                   country: 'Subject Country'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('SUBJECT');
             });
           });

        it('should fail the validation if subject C is different from what expected',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {
                 subject: {
                   commonName: 'Subject CN',
                   organization: 'Subject Org',
                   organizationUnit: 'Subject Org Unit',
                   country: 'Subject CountryC'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('SUBJECT');
             });
           });

        it('should fail the validation if subject CN is different from what expected',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {
                 issuer: {
                   commonName: 'Subject CN1',
                   organization: 'Subject Org',
                   organizationUnit: 'Subject Org Unit',
                   country: 'Subject Country'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('ISSUER');
             });
           });

        it('should fail the validation if issuer is different from what expected',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {
                 issuer: {
                   commonName: 'Subject CN',
                   organization: 'Subject Org1',
                   organizationUnit: 'Subject Org Unit',
                   country: 'Subject Country'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('ISSUER');
             });
           });

        it('should fail the validation if subject is different from what expected',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {
                 issuer: {
                   commonName: 'Subject CN',
                   organization: 'Subject Org',
                   organizationUnit: 'Subject Org Unit1',
                   country: 'Subject Country'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('ISSUER');
             });
           });

        it('should fail the validation if issuer is different from what expected',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {
                 issuer: {
                   commonName: 'Subject CN',
                   organization: 'Subject Org',
                   organizationUnit: 'Subject Org Unit',
                   country: 'Subject CountryC'
                 }
               };

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('ISSUER');
             });
           });

        it('should validate that time is between notBefore and notAfter',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: '2014-12-25\'T\'00:00:00Z'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations.length).toBe(0);
             });
           });

        it('should fail the validation if time is not between notBefore and notAfter',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: '2016-12-25\'T\'00:00:00Z'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('TIME');
             });
           });

        it('106356 should not fail the validation if time equals notBefore',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: '2014-06-18\'T\'10:23:28Z'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).not.toContain('TIME');
             });
           });

        it('106356 should not fail the validation if time equals notAfter',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: '2015-06-18\'T\'10:23:28Z'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).not.toContain('TIME');
             });
           });

        it('should fail the validation if time is invalid - 1 character',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: 'a'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('TIME');
             });
           });

        it('should fail the validation if time is invalid - 3 characters',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: 'abc'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('TIME');
             });
           });

        it('should fail the validation if time is invalid 20 characters',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: 'aaaaaaaaaaaaaaaaaaaa'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('TIME');
             });
           });

        it('should fail if that the flag CA is set to false if keyType validation parameter is CA',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {keyType: 'CA'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('KEY_TYPE');
             });
           });

        it('should validate that if the Key Type is CA, then the key usage should be set to keycertSign, cRLSign',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {keyType: 'CA'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('KEY_TYPE');
             });
           });

        it('should validate that if the Key Type is Sign, then the key usage should be set to digitalSignature, nonRepudiation',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {keyType: 'Sign'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).not.toContain('KEY_TYPE');
             });
           });

        it('should validate that if the Key Type is Encryption, then the key usage should be set to keyEncipherment, dataEncipherment',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {keyType: 'Encryption'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).toContain('KEY_TYPE');
             });
           });

        it('should validate that if the Key Type is Signature, then the certificate has to be verified with the public provided',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {caCertificatePem: _rootCertificatePem};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);
               expect(validations).not.toContain('SIGNATURE');
             });
           });
      });

      it('should exist a validator chain method', function() {
        cryptolib('certificates', function(box) {
          expect(box.certificates.service.validateX509CertificateChain)
              .toBeDefined();
        });
      });

      describe('Chain validator', function() {
        it('should validate a chain of certificates - valid dates', function() {
          cryptolib('certificates', function(box) {
            var chain = {
              leaf: {
                pem:
                    '-----BEGIN CERTIFICATE-----MIICCzCCAXSgAwIBAgIBATANBgkqhkiG9w0BAQUFADBHMRswGQYDVQQDDBJ3aGl0ZSBpbnRlcm1lZGlhdGUxDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwHhcNMTQwOTA1MTAxNDEzWhcNMjQwODEzMTAxNDEzWjA/MRMwEQYDVQQDDAp3aGl0ZSBsZWFmMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3yWZQRc5xk14TIL3/2UGVgNz94dHq7vg4ORduUvtio4nVMSriQNSPnEbAfV9FR8qA6UCzqXQroNX6eSpQfJ4MblyUWUj7VYtY/jC2UxGcE4GJxhngiwfO+It2ejTr6I7EPlj7pwPOVTDWByaDsYWtJ3JeaVhR9Tcf5L+H5k7HTQIDAQABow8wDTALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQEFBQADgYEAQNoV1G2zwceE1/4VppH3eJYi8u1CUWUoaQe1rH3owm9CH9/OfS+MicUIW/7yRi86rtATnhTcfN34E18AwbJsmESBnCkx+WaVZ/WC1jGJ/Dd5rgxvRfZpwd1RKivKzZsoYm19PVFRDg9K9qqA6dkuq29wTnwMV48rQPkfix9lWjg=-----END CERTIFICATE-----',
                keytype: 'Sign',
                subject: {
                  commonName: 'white leaf',
                  organization: 'scytl',
                  organizationUnit: 'se',
                  country: 'ES'
                },
                time: '2014-12-25\'T\'00:00:00Z'
              },
              root:
                  '-----BEGIN CERTIFICATE-----MIICSDCCAbGgAwIBAgIJAPJ2LYuwqa81MA0GCSqGSIb3DQEBBQUAMD0xETAPBgNVBAMMCHdoaXRlIGNhMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMB4XDTE0MDkwNTA5MzQwNloXDTI0MDkwMjA5MzQwNlowPTERMA8GA1UEAwwId2hpdGUgY2ExDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnpKp2eeY1hC4V8E1nUIZJpJVhAZk3oJeEbq0Xi2uSYrh6wmfuQIHFs5rYF4yHoGvf+PM66od8ccHa1GNGB/b6lFb4/+2vEvYWA51OWY9BBdgjhAsqoks9ANNDQ1oxX4dqLwjM/GDDg2v9xoT9tj0CaJnieQbhMkH8oQveBBR57AgMBAAGjUDBOMB0GA1UdDgQWBBRwKg/RntWH8EvCkEX3jtHuvSpbwTAfBgNVHSMEGDAWgBRwKg/RntWH8EvCkEX3jtHuvSpbwTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBADk/OOoc2tbfxja95RqH22EcAVJ/mDin2ev9cXhZ2A2xh4SrUad0fNsyFnY+m3EzfA0KPrI46sEPDeuWqwnGAYccmp7ylVIS6edYRvnI5mdLze2CnuDNJYQzZXjASQF2ih5wTNm3VMZkN0SqGQZiOCa9NQHPfmNj8mJe0Ooj2b3o-----END CERTIFICATE-----',
              certificates: {
                pems: [
                  '-----BEGIN CERTIFICATE-----MIICGjCCAYOgAwIBAgIBATANBgkqhkiG9w0BAQUFADA9MREwDwYDVQQDDAh3aGl0ZSBjYTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzAeFw0xNDA5MDUwOTM1MThaFw0yNDA4MjMwOTM1MThaMEcxGzAZBgNVBAMMEndoaXRlIGludGVybWVkaWF0ZTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUmpmT/rNcxIP8Xulj0RcbdvaNGJNoEPMzEGztguB+mKsTPgJIlrKvdskY5NIPd1+hzS+HjvW11uwyT2FnmyfKBqND4EH5BqU8Ph4bAyrT950DPcOGE4vG0fLO0jTD3h0n4d4yugWmrND2sScFZQsGLbgOYX4QM8vV+owoKNRxMCAwEAAaMgMB4wDwYDVR0TBAgwBgEB/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEFBQADgYEAF0B5LQDo3uRs9dSkxp39ER64XIa6XYV0zdbq+fqHg8Qcoggpox7vwmc6YoCNQpfY2zLiIUMbb6VMP4pkp7jZMju9TBKPOjMFrQKom0cowT/gYdokgnsk8yUYOdo46GJR43PjFLTiQk5JDawWaECbhcgyLf59AalX9z9dbY0AwqI=-----END CERTIFICATE-----'
                ],
                subjects: [{
                  commonName: 'white intermediate',
                  organization: 'scytl',
                  organizationUnit: 'se',
                  country: 'ES'
                }]
              }
            };
            var validation =
                box.certificates.service.validateX509CertificateChain(chain);
            expect(validation).toEqual([[], []]);
          });
        });

        it('should validate a chain of certificates generated by java', function() {
          cryptolib('certificates.service', function(box) {
            var chain = {
              leaf: {
                pem:
                    '-----BEGIN CERTIFICATE-----MIIDeDCCAmCgAwIBAgIVAJ81fEEjKAKK8Yg5D654L3suMkbVMA0GCSqGSIb3DQEBCwUAMFExEjAQBgNVBAMMCVZvdGVycyBDQTETMBEGA1UECwwKc2cxMDAxLTAwMDEOMAwGA1UECgwFTlNXRUMxCTAHBgNVBAcMADELMAkGA1UEBhMCQVUwHhcNMTQwOTA4MDc0NDExWhcNMTUwOTA4MDc0MzU0WjB4MTkwNwYDVQQDDDBWb3RlclNpZ24gNDExNTQ5NzQxMjM5NzMxMjgwNDQ0MjI3NzQxNzUxODczMDY5OTcxEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqYeNTbS15+IpxgXEXivFK5Rt3I5du1NDgYdxRRebYqWi/0t2LsyAljZkAqWbgyQswJbXwpuhJmlSlz2RNr04PBxxm93BMoVGXlbVGEGAmk3ZkwVB0VjSFZkJHvARmJ9oudYC7uUyxtr8brPB16Q8FIv2j9CSPMWUXa0YbxLFgdZmJ6Ulu5B3RToBvpXL0aFNiIpIm4Vz5f0N4f+AxV1CV8vwzoQpefuGuss32WIi1vrhx0j1+K6hWGx8xgID/yKEIBPsKupjjcPhNxdzZepI0FOSpPnjjVMHHWgEaiRloRk66T7FiKOPkanazQBbo/zULRrKfvXkQ4NAnoV29T7GBwIDAQABoyAwHjAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIGwDANBgkqhkiG9w0BAQsFAAOCAQEADghhwPmZ2LTPhtVyKtBa1ljNcNODVRrLFX05h9ZIVojteI6BGE+xM/IagfsaOrK804T0psbdWEpWQJqXJtbI5TEiFAbliVdFOKv7Al5jnd2HNqauU3YWoZ2jy32wbtBnTpR8E8ILJLsMMyvy+ZSRUKyHaboOsphy1dtyncACNnuLhLtSC1IeB2LbFOLZWnU7dnKETAwMCBJ3ELlhFIPPwLLQjkLtX3Z4kYWRNUB/ePoKGfXPhBYXaY4KE135kA+5GNpIRwzZmh8VrDo3aTLEAJZD0xDv4ohir5FWcnMPdUvmwfya/mZZhns94y5aiql/kTJPl0f0G1t0AOkRYqbB/w==-----END CERTIFICATE-----',
                keytype: 'Sign',
                subject: {
                  commonName:
                      'VoterSign 41154974123973128044422774175187306997',
                  organization: 'NSWEC',
                  organizationUnit: 'sg1001-000',
                  country: 'AU'
                },
                time: '2014-12-25\'T\'00:00:00Z'
              },
              root:
                  '-----BEGIN CERTIFICATE-----MIIDYzCCAkugAwIBAgIUax82wdIaGWRKCH3J1h1u4Ij4F7AwDQYJKoZIhvcNAQELBQAwWTEaMBgGA1UEAwwRRWxlY3Rpb24gRXZlbnQgQ0ExEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMB4XDTE0MDkwODA3NDM1NloXDTE1MDkwODA3NDM1NFowWTEaMBgGA1UEAwwRRWxlY3Rpb24gRXZlbnQgQ0ExEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgXYi9cTerqmun+SS56tByn9Z9OYwP2cS8NkvHO0Pv04lTEJ//1jo8uUpn3kza4m5lgUFEr6hl+e+G21yk/NrieD4Ke008Be0F9arSedsYyQvqcg1VcssY3+mtbz8osOH8f/jB6cMkASrzESvDOIM45MkaFlAYqkxN4q819qaOgWvlotkQKUKu+Dnb59Cpqf8MQ2nVv+mEwXVZiVCKquOnEN29vqppI8GigE1jqRkKx5mIwmUBYWrOalWVexMNJVSAqyxlJ1ozgKC6Z/o37l/pRFJMG1iIiAiMRucokWn7p9414O+3O4OuDP1lkBG7PxzmOHEl3WrJ7hseGLWIF/mcwIDAQABoyMwITAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBBjANBgkqhkiG9w0BAQsFAAOCAQEAO89PQSEeZkGxydWgFJ61DKq6eiaNjCRXKorH3yDCd7wwuOiKamJRojk29zrLZR+0BrYGVLV4JPj/0ZStNtbQfz1jeadEVdrpjlk9NfNgPcYIbb9tENNNFRqeQR8JB1K+qOELZt87jX9wg1jzgAo0kSHDAIIADDOhX//lzeE2amaXgYYHmLtdKpZI2lXKj1VmdeeR/wGtpkXGkDO65FkvcY65lj25wzksanpk/juy4ENLoY1AvC6f30Kw7CSduoctsB58MOPPtlR6rA7EmSWUAdfTmobB3kHOm6OZN+nikUhC73l1kS9mZXg/oabO7yoOmRQ7ttqqNmecaB7EnS5nHg==-----END CERTIFICATE-----',
              certificates: {
                pems: [
                  '-----BEGIN CERTIFICATE-----MIIDWzCCAkOgAwIBAgIULFolrOnoUb8h6GroW8JoRZtvSrcwDQYJKoZIhvcNAQELBQAwWTEaMBgGA1UEAwwRRWxlY3Rpb24gRXZlbnQgQ0ExEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMB4XDTE0MDkwODA3NDM1N1oXDTE1MDkwODA3NDM1NFowUTESMBAGA1UEAwwJVm90ZXJzIENBMRMwEQYDVQQLDApzZzEwMDEtMDAwMQ4wDAYDVQQKDAVOU1dFQzEJMAcGA1UEBwwAMQswCQYDVQQGEwJBVTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALAjG+fLhtK6FS0iLdAPscY9jCGYjROueU0F+ans8a5e214RDNS2WdnYhJSuPRcbftztncze6vCD0/KSZO6MTwagqx7iudFhSITJA96tTDT0pEQ/fInkcJbOh8LYAHukEXYPxQSiHTD52UTLwPpAVd/DhBflf7LKL0VEjXuDifU/EvVDp96g6yoyHMiv6REsIrr8o2cSI5UbrxPIX7V/fAwfHKDyRp2ECrqaMmz/7dKWjhB0zoBQGI7r4WCIDmUbU9psn90a5y6xj5XhuF6HzozaZH1IibCTULdTQEUfs4dPaF+5oWlCZrF+dkBTXQh4Pm1h+iOboCk+8FcZtt4ijC0CAwEAAaMjMCEwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYwDQYJKoZIhvcNAQELBQADggEBAD96TL6FvCMtoRJqjowqVRSQTAhnCcPesmG6+yzz9YXWu46koEM/XkeZm+wtIF6YC2R59vAvbFNtrnkThvFG3sDXDUAeDS4vrVr8S0BApbHDa94ri7Km8aYrPhwy5sDOsq5QR//RZORPOJaMeZHiJA0MP31GJJjwcJJs4uOm3sQHEdNArSEp8+Dzim3X/EY6NWo8COeKEwcPSm0c/jMung1NYKFV6CGF/jOOEcrESNdnBWi7LYAWn/ba9XiyuKhD26bLX1L20IFEO+EvcnEvLrhulmRe/bZsLkyZMieZqh+w3KFUe2F452J6o+/i2TbHcn8/c2B9T490/dV5w8sB1bM=-----END CERTIFICATE-----'
                ],
                subjects: [{
                  commonName: 'Voters CA',
                  organization: 'NSWEC',
                  organizationUnit: 'sg1001-000',
                  country: 'AU'
                }]
              }
            };
            var validation =
                box.certificates.service.validateX509CertificateChain(chain);
            expect(validation).toEqual([[], []]);
          });
        });

        it('should have a function that flatten the failed validation array',
           function() {
             cryptolib('certificates.service', function(box) {
               var validationFailed =
                   [['SIGNATURE', 'NOT_AFTER'], ['NOT_BEFORE']];
               var validationFailedFlattened =
                   ['signature_0', 'not_after_0', 'not_before_1'];

               expect(box.certificates.service.flattenFailedValidations(
                          validationFailed))
                   .toEqual(validationFailedFlattened);
             });
           });
      });

      // commented when validations were commented
      describe('Bug #97033', function() {
        it('should not validate a chain of certificates because of an invalid not after in one certificate',
           function() {
             cryptolib('certificates', function(box) {
               var chain = {
                 leaf: {
                   pem:
                       '-----BEGIN CERTIFICATE-----MIIDszCCApugAwIBAgIJALAhpG3UDbqKMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMB4XDTE0MTAyMzE1MTMxMVoXDTE1MTAyMzE1MTMxMVowVjELMAkGA1UEBhMCRVMxEzARBgNVBAgMClRlc3QtU3RhdGUxDjAMBgNVBAoMBVNjeXRsMQswCQYDVQQLDAJRQTEVMBMGA1UEAwwMU2VjRW5kRW50aXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1isXtrHkDvChE1jGCSnleAc547Hnswxhuf3XcTs6cG4y5oOgLgEZrbzHAe6Z47wBmt/35OC5oIa5tgmWBs7QVnAZ4PZWAdZTAq2J5TBQCcpilw2Kc5NHp231Ckw52pHT6/O4VieeZdOA/+PCbL7/4PrQ/Uq/23W1kHvUqToJTRwrOZFMg8QfTlo3RYN6xFmlW6hXCCHkolqihbK44jX6FELCLwbE1dw43eao5Pnbxw//2jyqReC2qrpcKQ4MdgJJYgK9rA+zCCj0m7H+LsNyiat4OTYvDVx9Lg7nmjgD2cFWewLDE9t4QVST41vYgWObJjisLyGsbEwLfOjXkf8jrQIDAQABo4GKMIGHMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgbAMC0GCWCGSAGG+EIBDQQgFh5TY3l0bCBRQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFLBtuBGwweIxC3h8ILFblDSg2x8SMB8GA1UdIwQYMBaAFL0z1LklZYtSH+ayTVfCjxPfiDf4MA0GCSqGSIb3DQEBCwUAA4IBAQBDTsidJItoNaPPp+idbBCLm8VrNxlVpWYQ4KmA6glCw5z7TcH1JW+GsfSxXfXo2bFGtxDFsjZCV76fBwoQVAvJ6ntCyn/vrslSQy6RvArM2e7Di6h6ND500so0a5mfycN7rauhj0tWcmZv3zExJcwgtar/P3BHYpfN9/eG46NpxRq4Sq9uwfYEH9lzAfZ3xgLWgIRAEoF6b2VRMU9ApVX/Ifai17+0JKhNkmBp63EUJCcXNSjTV4UtKTO2QFply4hOqluhMkFZDqFsbzB+mrSQcCget+6ymCVdTaN3GwVuW4CKIoR85Nr+3OV1VupDz3bnzqDjeiTNIJEV6aONP2zD-----END CERTIFICATE-----',
                   keytype: 'Sign',
                   subject: {
                     commonName: 'SecEndEntity',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   }
                 },
                 root:
                     '-----BEGIN CERTIFICATE-----MIIDgDCCAmigAwIBAgIJALAhpG3UDbqIMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTExMTJaFw0yNDEwMjAxNTExMTJaMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKanbx9BsuYLjq4RVAS269FD2Gtpp3YkcvKHwHO5XI89+pJELO9CVnyIvEmon3ndbvPTqM3D/HOvl0M4/kPFeHij8d24iM48GVbYfLVBrV3BtKe24pHeIagSn6hl9ouiknbvDBqF8qZcBTNhnH8yX7isoJf3TShBpTOdyJ6XkDOQ+R0GtS1M4NN9BS74oGt0REC17VlBrO6JyR9rNswk4abqqpF7ekw1yXxcnrqa480+oyEcY9ji5QNoP9kGyibeB08seeB+wlt0fCWcJXtecsnPRpBNJmhLr4ziNyXoIOCEeVMA/MFx7vrrl+pkyhd49rQybbctv/mQGUM4QG6IZ3cCAwEAAaNdMFswHQYDVR0OBBYEFA56IIf2S8G0LjsIlSASdPBgMi2kMB8GA1UdIwQYMBaAFA56IIf2S8G0LjsIlSASdPBgMi2kMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQCE15Vvk+I+iR2RutD1+Mm8Yx1J8dtObqUYeIVh/xJlCA2zTBB2MPdNz6IroIHqb4/ZYd2l9ZtNkIyCZ3PV5ulV1ShQD6H2gXmE1AnwQeojqrdKQYAehcDOYWxMAZNBK1piimEpQCCSiBlNKG4f14GTjqpI4WuqcZ7DGQtWqR3LVpNsKCdE9OHZo/Ep4Kp1AwQ+qhmLl0wA1SmHP7+0nxRI5i5w0Yl8NQx/v6W4j7ohWOwzpRRZ4rCz856oOVJqDDOMWqQfqYKQGY4WROpM447OsQ+svno3JVbqtvMDoRLBu6GEJtSl2DOvccPSqI2cxotsBzF+Ppz9EpeWtW+Wut6k-----END CERTIFICATE-----',
                 certificates: {
                   pems: [
                     '-----BEGIN CERTIFICATE-----MIIDsDCCApigAwIBAgIJALAhpG3UDbqJMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTEyMTZaFw0xNTEwMjMxNTEyMTZaME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyR4qNaHpEoj/48+iJtJUJuXv4i/KEKRKwrTYOVbDplyfTg0KMO+zeDd5SyE/aBnANetlZo8Ku0m1LPbUdge67Jzj3jinZ+iUQbkKr15YgMjehb3Y0V/02ImIr4+w8oXBGIN6eDpRt7tKrCPuyeRiWOZqIae7t5KY5enQ2NC6YbteTwjO/O1ldTnFo3xeDiuQ7P3zBmkAXaAxDxEE9dh61mB/QXVeJpphTgF8yoRWeZhl1S9YnNfTvMyXih8WzY/DlRTKs9JnqRmI7YhLDV95v3Aah1FhelG+l3FsWciE3brcZDa0Y+zumck2sy35PU3TF7RxifUumCEiNHEaawV7xQIDAQABo4GNMIGKMAwGA1UdEwQFMAMBAf8wLQYJYIZIAYb4QgENBCAWHlNjeXRsIFFBIEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUvTPUuSVli1If5rJNV8KPE9+IN/gwHwYDVR0jBBgwFoAUDnogh/ZLwbQuOwiVIBJ08GAyLaQwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQB/R7kXa2NblyepSBkdjIQNEvxmaryje9MQLJg+kRaWjpkWz0OW+a9vYoK+6hAt15RYFWNASTHF6StSe0En9F35fzaU8/XUxoKH5pmm7aypVIN7mWWV+XMv8iao1kP2tcpnk5tsvnuqBfqcU22r+oLLcnJ1hWy2kijtIBGEe+kxhZi2twlDG66RQpEZA2CmDjLHYbAXF7wnqiY9PSKwXcKPjcHXxs8LgPbbZjLRZ2FSvfgya/9drHfu7QYHRBFFHYljdgcpF/Z/nerocmvTgX8iuOu133JA3JmMOy3E7riVUfRkKh4Nnivh9S5mCKSff0yW4i+kJJ9TuAuDi30FN3tR-----END CERTIFICATE-----'
                   ],
                   subjects: [{
                     commonName: 'SecQA',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   }]
                 }
               };
               var validations =
                   box.certificates.service.validateX509CertificateChain(chain);
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .toEqual(['not_after_0']);
             });
           });
      });

      describe('Bug #97011', function() {
        it('certificate: no failed validations if add Time Reference validation for Leaf Certificate, set it the first day certificate is valid (at exact valid time or a few hours after to skip time zone conversion effect) and run validation',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: '2014-07-18\'T\'12:23:28Z'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);

               expect(validations).toEqual([]);
             });
           });

        it('chain: no failed validations if add Time Reference validation for Leaf Certificate, set it the first day certificate is valid (at exact valid time or a few hours after to skip time zone conversion effect) and run validation',
           function() {
             cryptolib('certificates', function(box) {
               var chain = {
                 leaf: {
                   pem:
                       '-----BEGIN CERTIFICATE-----MIIDszCCApugAwIBAgIJALAhpG3UDbqKMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMB4XDTE0MTAyMzE1MTMxMVoXDTE1MTAyMzE1MTMxMVowVjELMAkGA1UEBhMCRVMxEzARBgNVBAgMClRlc3QtU3RhdGUxDjAMBgNVBAoMBVNjeXRsMQswCQYDVQQLDAJRQTEVMBMGA1UEAwwMU2VjRW5kRW50aXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1isXtrHkDvChE1jGCSnleAc547Hnswxhuf3XcTs6cG4y5oOgLgEZrbzHAe6Z47wBmt/35OC5oIa5tgmWBs7QVnAZ4PZWAdZTAq2J5TBQCcpilw2Kc5NHp231Ckw52pHT6/O4VieeZdOA/+PCbL7/4PrQ/Uq/23W1kHvUqToJTRwrOZFMg8QfTlo3RYN6xFmlW6hXCCHkolqihbK44jX6FELCLwbE1dw43eao5Pnbxw//2jyqReC2qrpcKQ4MdgJJYgK9rA+zCCj0m7H+LsNyiat4OTYvDVx9Lg7nmjgD2cFWewLDE9t4QVST41vYgWObJjisLyGsbEwLfOjXkf8jrQIDAQABo4GKMIGHMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgbAMC0GCWCGSAGG+EIBDQQgFh5TY3l0bCBRQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFLBtuBGwweIxC3h8ILFblDSg2x8SMB8GA1UdIwQYMBaAFL0z1LklZYtSH+ayTVfCjxPfiDf4MA0GCSqGSIb3DQEBCwUAA4IBAQBDTsidJItoNaPPp+idbBCLm8VrNxlVpWYQ4KmA6glCw5z7TcH1JW+GsfSxXfXo2bFGtxDFsjZCV76fBwoQVAvJ6ntCyn/vrslSQy6RvArM2e7Di6h6ND500so0a5mfycN7rauhj0tWcmZv3zExJcwgtar/P3BHYpfN9/eG46NpxRq4Sq9uwfYEH9lzAfZ3xgLWgIRAEoF6b2VRMU9ApVX/Ifai17+0JKhNkmBp63EUJCcXNSjTV4UtKTO2QFply4hOqluhMkFZDqFsbzB+mrSQcCget+6ymCVdTaN3GwVuW4CKIoR85Nr+3OV1VupDz3bnzqDjeiTNIJEV6aONP2zD-----END CERTIFICATE-----',
                   keytype: 'Sign',
                   subject: {
                     commonName: 'SecEndEntity',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   },
                   time: '2014-10-23\'T\'19:11:28Z'
                 },
                 root:
                     '-----BEGIN CERTIFICATE-----MIIDgDCCAmigAwIBAgIJALAhpG3UDbqIMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTExMTJaFw0yNDEwMjAxNTExMTJaMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKanbx9BsuYLjq4RVAS269FD2Gtpp3YkcvKHwHO5XI89+pJELO9CVnyIvEmon3ndbvPTqM3D/HOvl0M4/kPFeHij8d24iM48GVbYfLVBrV3BtKe24pHeIagSn6hl9ouiknbvDBqF8qZcBTNhnH8yX7isoJf3TShBpTOdyJ6XkDOQ+R0GtS1M4NN9BS74oGt0REC17VlBrO6JyR9rNswk4abqqpF7ekw1yXxcnrqa480+oyEcY9ji5QNoP9kGyibeB08seeB+wlt0fCWcJXtecsnPRpBNJmhLr4ziNyXoIOCEeVMA/MFx7vrrl+pkyhd49rQybbctv/mQGUM4QG6IZ3cCAwEAAaNdMFswHQYDVR0OBBYEFA56IIf2S8G0LjsIlSASdPBgMi2kMB8GA1UdIwQYMBaAFA56IIf2S8G0LjsIlSASdPBgMi2kMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQCE15Vvk+I+iR2RutD1+Mm8Yx1J8dtObqUYeIVh/xJlCA2zTBB2MPdNz6IroIHqb4/ZYd2l9ZtNkIyCZ3PV5ulV1ShQD6H2gXmE1AnwQeojqrdKQYAehcDOYWxMAZNBK1piimEpQCCSiBlNKG4f14GTjqpI4WuqcZ7DGQtWqR3LVpNsKCdE9OHZo/Ep4Kp1AwQ+qhmLl0wA1SmHP7+0nxRI5i5w0Yl8NQx/v6W4j7ohWOwzpRRZ4rCz856oOVJqDDOMWqQfqYKQGY4WROpM447OsQ+svno3JVbqtvMDoRLBu6GEJtSl2DOvccPSqI2cxotsBzF+Ppz9EpeWtW+Wut6k-----END CERTIFICATE-----',
                 certificates: {
                   pems: [
                     '-----BEGIN CERTIFICATE-----MIIDsDCCApigAwIBAgIJALAhpG3UDbqJMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTEyMTZaFw0xNTEwMjMxNTEyMTZaME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyR4qNaHpEoj/48+iJtJUJuXv4i/KEKRKwrTYOVbDplyfTg0KMO+zeDd5SyE/aBnANetlZo8Ku0m1LPbUdge67Jzj3jinZ+iUQbkKr15YgMjehb3Y0V/02ImIr4+w8oXBGIN6eDpRt7tKrCPuyeRiWOZqIae7t5KY5enQ2NC6YbteTwjO/O1ldTnFo3xeDiuQ7P3zBmkAXaAxDxEE9dh61mB/QXVeJpphTgF8yoRWeZhl1S9YnNfTvMyXih8WzY/DlRTKs9JnqRmI7YhLDV95v3Aah1FhelG+l3FsWciE3brcZDa0Y+zumck2sy35PU3TF7RxifUumCEiNHEaawV7xQIDAQABo4GNMIGKMAwGA1UdEwQFMAMBAf8wLQYJYIZIAYb4QgENBCAWHlNjeXRsIFFBIEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUvTPUuSVli1If5rJNV8KPE9+IN/gwHwYDVR0jBBgwFoAUDnogh/ZLwbQuOwiVIBJ08GAyLaQwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQB/R7kXa2NblyepSBkdjIQNEvxmaryje9MQLJg+kRaWjpkWz0OW+a9vYoK+6hAt15RYFWNASTHF6StSe0En9F35fzaU8/XUxoKH5pmm7aypVIN7mWWV+XMv8iao1kP2tcpnk5tsvnuqBfqcU22r+oLLcnJ1hWy2kijtIBGEe+kxhZi2twlDG66RQpEZA2CmDjLHYbAXF7wnqiY9PSKwXcKPjcHXxs8LgPbbZjLRZ2FSvfgya/9drHfu7QYHRBFFHYljdgcpF/Z/nerocmvTgX8iuOu133JA3JmMOy3E7riVUfRkKh4Nnivh9S5mCKSff0yW4i+kJJ9TuAuDi30FN3tR-----END CERTIFICATE-----'
                   ],
                   subjects: [{
                     commonName: 'SecQA',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   }]
                 }
               };
               var validations =
                   box.certificates.service.validateX509CertificateChain(chain);
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .not.toContain('time_0');
             });
           });

        it('certificate: failed validations if add Time Reference validation for Leaf Certificate, set it the last day certificate is valid (a minut after valid time expires or a few hours after to skip time zone conversion effect) and run validation',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {time: '2015-07-18\'T\'12:24:28Z'};

               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var validations = validator.validate(_userCertificatePem);

               expect(validations).toEqual(['TIME']);
             });
           });

        it('chain: failed validations if add Time Reference validation for Leaf Certificate, set it the last day certificate is valid (a minut after valid time expires or a few hours after to skip time zone conversion effect) and run validation',
           function() {
             cryptolib('certificates', function(box) {
               var chain = {
                 leaf: {
                   pem:
                       '-----BEGIN CERTIFICATE-----MIIDszCCApugAwIBAgIJALAhpG3UDbqKMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMB4XDTE0MTAyMzE1MTMxMVoXDTE1MTAyMzE1MTMxMVowVjELMAkGA1UEBhMCRVMxEzARBgNVBAgMClRlc3QtU3RhdGUxDjAMBgNVBAoMBVNjeXRsMQswCQYDVQQLDAJRQTEVMBMGA1UEAwwMU2VjRW5kRW50aXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1isXtrHkDvChE1jGCSnleAc547Hnswxhuf3XcTs6cG4y5oOgLgEZrbzHAe6Z47wBmt/35OC5oIa5tgmWBs7QVnAZ4PZWAdZTAq2J5TBQCcpilw2Kc5NHp231Ckw52pHT6/O4VieeZdOA/+PCbL7/4PrQ/Uq/23W1kHvUqToJTRwrOZFMg8QfTlo3RYN6xFmlW6hXCCHkolqihbK44jX6FELCLwbE1dw43eao5Pnbxw//2jyqReC2qrpcKQ4MdgJJYgK9rA+zCCj0m7H+LsNyiat4OTYvDVx9Lg7nmjgD2cFWewLDE9t4QVST41vYgWObJjisLyGsbEwLfOjXkf8jrQIDAQABo4GKMIGHMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgbAMC0GCWCGSAGG+EIBDQQgFh5TY3l0bCBRQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFLBtuBGwweIxC3h8ILFblDSg2x8SMB8GA1UdIwQYMBaAFL0z1LklZYtSH+ayTVfCjxPfiDf4MA0GCSqGSIb3DQEBCwUAA4IBAQBDTsidJItoNaPPp+idbBCLm8VrNxlVpWYQ4KmA6glCw5z7TcH1JW+GsfSxXfXo2bFGtxDFsjZCV76fBwoQVAvJ6ntCyn/vrslSQy6RvArM2e7Di6h6ND500so0a5mfycN7rauhj0tWcmZv3zExJcwgtar/P3BHYpfN9/eG46NpxRq4Sq9uwfYEH9lzAfZ3xgLWgIRAEoF6b2VRMU9ApVX/Ifai17+0JKhNkmBp63EUJCcXNSjTV4UtKTO2QFply4hOqluhMkFZDqFsbzB+mrSQcCget+6ymCVdTaN3GwVuW4CKIoR85Nr+3OV1VupDz3bnzqDjeiTNIJEV6aONP2zD-----END CERTIFICATE-----',
                   keytype: 'Sign',
                   subject: {
                     commonName: 'SecEndEntity',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   },
                   time: '2015-10-23\'T\'19:12:28Z'
                 },
                 root:
                     '-----BEGIN CERTIFICATE-----MIIDgDCCAmigAwIBAgIJALAhpG3UDbqIMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTExMTJaFw0yNDEwMjAxNTExMTJaMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKanbx9BsuYLjq4RVAS269FD2Gtpp3YkcvKHwHO5XI89+pJELO9CVnyIvEmon3ndbvPTqM3D/HOvl0M4/kPFeHij8d24iM48GVbYfLVBrV3BtKe24pHeIagSn6hl9ouiknbvDBqF8qZcBTNhnH8yX7isoJf3TShBpTOdyJ6XkDOQ+R0GtS1M4NN9BS74oGt0REC17VlBrO6JyR9rNswk4abqqpF7ekw1yXxcnrqa480+oyEcY9ji5QNoP9kGyibeB08seeB+wlt0fCWcJXtecsnPRpBNJmhLr4ziNyXoIOCEeVMA/MFx7vrrl+pkyhd49rQybbctv/mQGUM4QG6IZ3cCAwEAAaNdMFswHQYDVR0OBBYEFA56IIf2S8G0LjsIlSASdPBgMi2kMB8GA1UdIwQYMBaAFA56IIf2S8G0LjsIlSASdPBgMi2kMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQCE15Vvk+I+iR2RutD1+Mm8Yx1J8dtObqUYeIVh/xJlCA2zTBB2MPdNz6IroIHqb4/ZYd2l9ZtNkIyCZ3PV5ulV1ShQD6H2gXmE1AnwQeojqrdKQYAehcDOYWxMAZNBK1piimEpQCCSiBlNKG4f14GTjqpI4WuqcZ7DGQtWqR3LVpNsKCdE9OHZo/Ep4Kp1AwQ+qhmLl0wA1SmHP7+0nxRI5i5w0Yl8NQx/v6W4j7ohWOwzpRRZ4rCz856oOVJqDDOMWqQfqYKQGY4WROpM447OsQ+svno3JVbqtvMDoRLBu6GEJtSl2DOvccPSqI2cxotsBzF+Ppz9EpeWtW+Wut6k-----END CERTIFICATE-----',
                 certificates: {
                   pems: [
                     '-----BEGIN CERTIFICATE-----MIIDsDCCApigAwIBAgIJALAhpG3UDbqJMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTEyMTZaFw0xNTEwMjMxNTEyMTZaME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyR4qNaHpEoj/48+iJtJUJuXv4i/KEKRKwrTYOVbDplyfTg0KMO+zeDd5SyE/aBnANetlZo8Ku0m1LPbUdge67Jzj3jinZ+iUQbkKr15YgMjehb3Y0V/02ImIr4+w8oXBGIN6eDpRt7tKrCPuyeRiWOZqIae7t5KY5enQ2NC6YbteTwjO/O1ldTnFo3xeDiuQ7P3zBmkAXaAxDxEE9dh61mB/QXVeJpphTgF8yoRWeZhl1S9YnNfTvMyXih8WzY/DlRTKs9JnqRmI7YhLDV95v3Aah1FhelG+l3FsWciE3brcZDa0Y+zumck2sy35PU3TF7RxifUumCEiNHEaawV7xQIDAQABo4GNMIGKMAwGA1UdEwQFMAMBAf8wLQYJYIZIAYb4QgENBCAWHlNjeXRsIFFBIEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUvTPUuSVli1If5rJNV8KPE9+IN/gwHwYDVR0jBBgwFoAUDnogh/ZLwbQuOwiVIBJ08GAyLaQwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQB/R7kXa2NblyepSBkdjIQNEvxmaryje9MQLJg+kRaWjpkWz0OW+a9vYoK+6hAt15RYFWNASTHF6StSe0En9F35fzaU8/XUxoKH5pmm7aypVIN7mWWV+XMv8iao1kP2tcpnk5tsvnuqBfqcU22r+oLLcnJ1hWy2kijtIBGEe+kxhZi2twlDG66RQpEZA2CmDjLHYbAXF7wnqiY9PSKwXcKPjcHXxs8LgPbbZjLRZ2FSvfgya/9drHfu7QYHRBFFHYljdgcpF/Z/nerocmvTgX8iuOu133JA3JmMOy3E7riVUfRkKh4Nnivh9S5mCKSff0yW4i+kJJ9TuAuDi30FN3tR-----END CERTIFICATE-----'
                   ],
                   subjects: [{
                     commonName: 'SecQA',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   }]
                 }
               };
               var validations =
                   box.certificates.service.validateX509CertificateChain(chain);
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .toContain('time_0');
             });
           });
      });

      // commented when validations were commented
      describe('Bug #97028', function() {
        it('it should not be not_before_2 missing if there is an error in the second certicate of the chain',
           function() {
             cryptolib('certificates', function(box) {
               var chain = {
                 leaf: {
                   pem:
                       '-----BEGIN CERTIFICATE-----MIIDszCCApugAwIBAgIJAJZdx2FxigSQMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMB4XDTE0MTAyMzE1MzYxOVoXDTE1MTAyMzE1MzYxOVowVjELMAkGA1UEBhMCRVMxEzARBgNVBAgMClRlc3QtU3RhdGUxDjAMBgNVBAoMBVNjeXRsMQswCQYDVQQLDAJRQTEVMBMGA1UEAwwMU2VjRW5kRW50aXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwFcomvOSzm2HkrOodZ93pyCxkpW7CVUMYC9zusJ7SdHEhcMib+44+rcfjnxPLmSO9xZTRYQo1DXwktEqkMutNih+YB51MPddKFylvMP9cUKkT1sIJzoIhRXC+xOtPAILfYx2XsNJCuRrEKM27JrC2xIVCBIiJuNz6UyFe7ZvtKmt+n40Nsi4Mcx0DhGRuZ3uhuA5OEotbG6VZZVhAovYFh09x+GyT1Dpug8NRD+mM+g1qJkhNZ+MST/rtw5DB++S+VwVLyVfxrTNycUafWWHAIMsjRlp6qlgfK32yY6N0haZXvP4RcXYSIWZK5ON+6r5lIY/FBR2m7eiGxlxdPkr7QIDAQABo4GKMIGHMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgbAMC0GCWCGSAGG+EIBDQQgFh5TY3l0bCBRQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFAOC8miWiUpLGw0TQzpjMUhAyYtvMB8GA1UdIwQYMBaAFDK0r97umjjsFvU6NAkgEvKeaIMVMA0GCSqGSIb3DQEBCwUAA4IBAQAQH64q51KivF3lUd78nOvHzuv04pUGihcVnYm3MTnXiODpwj4f6s5sZnAUWB+BOCJ1btaJvNAWUpnOH36nLr+qKV5mIxOyiKQWxY0tunySctsOpSscW/OfatvOUdkjshC7QuNq0fhjYh8SGO9Bk4ePO0uaIEWWywaH0wHVJPFaETklFXg3TZr61T5scrDEfs+lk+fQjkJ0YiS4e5VKfS7uVPLtDLgT9sx3l1QlVV2xZ8jF7hPpTy0JX8adp0NEQd+kfFFQuuSnY0QfLKtut9LHIjrl2q2sLfi0ibJ+uokCY85XZeyPzovIaSu3b2941E3dxL6UbDCEJyjlffybakyN-----END CERTIFICATE-----',
                   keytype: 'Sign',
                   subject: {
                     commonName: 'SecEndEntity',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   },
                 },
                 root:
                     '-----BEGIN CERTIFICATE-----MIIDgDCCAmigAwIBAgIJAJZdx2FxigSOMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTQ0MzdaFw0yNDEwMjAxNTQ0MzdaMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL4xWFqJpuQtcbywy9aYeH1X8cvlueJNPG/ixZsZjXY5KUdq7bjBLDlns49UWw96uxbU+JEL5aKMibtA8sJP9ZhDKY15ps9+wtceFnkT0g0IZ1shupQPEOKH2UfIS5/2teL7vjCplX3mDX8YOgzLqlqFrVtifejH2ETRzpDFc7K/P3KtSQqGKYUEZw5olCdLcg9Fp+I9QVkNGGr6Ds16BGWnb6Pgm5NOegdoOJLF6vZkhvyTYz9/Tg2men/HJLCVCXhbYwfUcu0ezj8w5G6zCdxVrqSTvjHVzp0kskVGRZjlrEOFLvswOGTjAon/U4iuBtqR+pLZg4Law+ETujPJPs0CAwEAAaNdMFswHQYDVR0OBBYEFKh4F63r8K3uM97VcdOYg3vylSHzMB8GA1UdIwQYMBaAFKh4F63r8K3uM97VcdOYg3vylSHzMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQAq2bLqJ+cLtnivXrNP5j9YL031zAO6xQDehxqasKOb7e6pdB8NKAOwa7smXCRsJxphT2vTv+VsecD/b8D3irEV000pbemeVmyrnOyaJxTCjBRvhjIbOmaBZ5k0KfxDubT8xx9H8Im+jxX0tZU5IQuTBG+Izd+5Dm5kxAhh7JDzclUYfUMDYrv43/7zm5wPkRqqK5SY7J4CPMiW3Uzxpc+uPCwydMSTYbDTdNN5nf0eDGepm2h/iewfickgkCFoNe6b3nxlQiK8EL9FUSKeq+1AWVnT3riC8C/CXTZwKo3ULXkKe3Jl3mvVYjkz/O/3NzeTfFdktuNaSh28Wrzlb9h4-----END CERTIFICATE-----',
                 certificates: {
                   pems: [
                     '-----BEGIN CERTIFICATE-----MIIDsDCCApigAwIBAgIJAJZdx2FxigSPMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTQyMDJaFw0xNTEwMjMxNTQyMDJaME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlvRn5OjWMaQfWCknqnsTzupcpx6zPQCLDe9DbizYRBghJcr4UlxI1uLsCHHzEMXCta5rJ+PSQ+TZ71PP7Ep3JyLAYObhVM8ltQyAKm8fgZa/FhCer46O3CohiD0Cdjg3r+qGOB+OKcGigXnNSoEsBlJ7HTzi5egiSg6D1Mx4uNw35F15ahTDxzIT/HN/Dnl3fEGzuJFebhSxj3u/+EEFJQrYvl62bUzzpXKlg/1C8BUYUXcPqhuUwZhyxE/DKyunA489XwpvN11y9NgygKb45qFAi9WmXil7q5JkW8iBqhXWbAoGiYf8nG5OanQBHERmAYAzJBI3QCK+6WiDXCRT/wIDAQABo4GNMIGKMAwGA1UdEwQFMAMBAf8wLQYJYIZIAYb4QgENBCAWHlNjeXRsIFFBIEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUMrSv3u6aOOwW9To0CSAS8p5ogxUwHwYDVR0jBBgwFoAUqHgXrevwre4z3tVx05iDe/KVIfMwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQA/Jxb3/gHb6mGyTEti+g3Zs5nrX64dtbFPWEgpyKu2jZ0ZoyllAUThIBjwvPw0hI3LqTOG06CCrgrPmndakasc5bJTEokshoI/IxTp/itME3ZBx/TUpu6v3baHYQ5C3HcKGhJxScikeO/Gh17A71Q4wA1PbLQ8jHCgfAFIODbXNRcYj8BVpv+7rXY/px4ai2UEf6ZAQZ3H6OyN9PGRA4EABJEiBsRNJvzcUcfq1JHF02FNlhFn8ZZXxOzpSc9bYX8trJGgB6GXDj2BPnFzsz1+ksNW6CaLU9AuuS2Bj/NseF2WVRwi3nCIvAPbpgAhivIHZOxY/eNRjJijo7+OKG40-----END CERTIFICATE-----'
                   ],
                   subjects: [{
                     commonName: 'SecQA',
                     organization: 'Scytl',
                     organizationUnit: 'QA',
                     country: 'ES'
                   }]
                 }
               };
               var validations =
                   box.certificates.service.validateX509CertificateChain(chain);
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .toContain('not_before_0');
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .toContain('not_before_1');
             });
           });
      });

      describe('Bug #96959', function() {
        it('The subject error should be related to the right certificate in the chain',
           function() {
             cryptolib('certificates', function(box) {
               var chain = {
                 leaf: {
                   pem:
                       '-----BEGIN CERTIFICATE-----MIICCzCCAXSgAwIBAgIBATANBgkqhkiG9w0BAQUFADBHMRswGQYDVQQDDBJ3aGl0ZSBpbnRlcm1lZGlhdGUxDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwHhcNMTQwOTA1MTAxNDEzWhcNMjQwODEzMTAxNDEzWjA/MRMwEQYDVQQDDAp3aGl0ZSBsZWFmMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3yWZQRc5xk14TIL3/2UGVgNz94dHq7vg4ORduUvtio4nVMSriQNSPnEbAfV9FR8qA6UCzqXQroNX6eSpQfJ4MblyUWUj7VYtY/jC2UxGcE4GJxhngiwfO+It2ejTr6I7EPlj7pwPOVTDWByaDsYWtJ3JeaVhR9Tcf5L+H5k7HTQIDAQABow8wDTALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQEFBQADgYEAQNoV1G2zwceE1/4VppH3eJYi8u1CUWUoaQe1rH3owm9CH9/OfS+MicUIW/7yRi86rtATnhTcfN34E18AwbJsmESBnCkx+WaVZ/WC1jGJ/Dd5rgxvRfZpwd1RKivKzZsoYm19PVFRDg9K9qqA6dkuq29wTnwMV48rQPkfix9lWjg=-----END CERTIFICATE-----',
                   keytype: 'Sign',
                   subject: {
                     commonName: 'white leaf',
                     organization: 'scytl',
                     organizationUnit: 'se',
                     country: 'ES'
                   },
                   time: '2014-12-25\'T\'00:00:00Z'
                 },
                 root:
                     '-----BEGIN CERTIFICATE-----MIICSDCCAbGgAwIBAgIJAPJ2LYuwqa81MA0GCSqGSIb3DQEBBQUAMD0xETAPBgNVBAMMCHdoaXRlIGNhMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMB4XDTE0MDkwNTA5MzQwNloXDTI0MDkwMjA5MzQwNlowPTERMA8GA1UEAwwId2hpdGUgY2ExDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnpKp2eeY1hC4V8E1nUIZJpJVhAZk3oJeEbq0Xi2uSYrh6wmfuQIHFs5rYF4yHoGvf+PM66od8ccHa1GNGB/b6lFb4/+2vEvYWA51OWY9BBdgjhAsqoks9ANNDQ1oxX4dqLwjM/GDDg2v9xoT9tj0CaJnieQbhMkH8oQveBBR57AgMBAAGjUDBOMB0GA1UdDgQWBBRwKg/RntWH8EvCkEX3jtHuvSpbwTAfBgNVHSMEGDAWgBRwKg/RntWH8EvCkEX3jtHuvSpbwTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBADk/OOoc2tbfxja95RqH22EcAVJ/mDin2ev9cXhZ2A2xh4SrUad0fNsyFnY+m3EzfA0KPrI46sEPDeuWqwnGAYccmp7ylVIS6edYRvnI5mdLze2CnuDNJYQzZXjASQF2ih5wTNm3VMZkN0SqGQZiOCa9NQHPfmNj8mJe0Ooj2b3o-----END CERTIFICATE-----',
                 certificates: {
                   pems: [
                     '-----BEGIN CERTIFICATE-----MIICGjCCAYOgAwIBAgIBATANBgkqhkiG9w0BAQUFADA9MREwDwYDVQQDDAh3aGl0ZSBjYTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzAeFw0xNDA5MDUwOTM1MThaFw0yNDA4MjMwOTM1MThaMEcxGzAZBgNVBAMMEndoaXRlIGludGVybWVkaWF0ZTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUmpmT/rNcxIP8Xulj0RcbdvaNGJNoEPMzEGztguB+mKsTPgJIlrKvdskY5NIPd1+hzS+HjvW11uwyT2FnmyfKBqND4EH5BqU8Ph4bAyrT950DPcOGE4vG0fLO0jTD3h0n4d4yugWmrND2sScFZQsGLbgOYX4QM8vV+owoKNRxMCAwEAAaMgMB4wDwYDVR0TBAgwBgEB/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEFBQADgYEAF0B5LQDo3uRs9dSkxp39ER64XIa6XYV0zdbq+fqHg8Qcoggpox7vwmc6YoCNQpfY2zLiIUMbb6VMP4pkp7jZMju9TBKPOjMFrQKom0cowT/gYdokgnsk8yUYOdo46GJR43PjFLTiQk5JDawWaECbhcgyLf59AalX9z9dbY0AwqI=-----END CERTIFICATE-----'
                   ],
                   subjects: [{
                     commonName: 'white intermediate wrong',
                     organization: 'scytl',
                     organizationUnit: 'se',
                     country: 'ES'
                   }]
                 }
               };
               var validations =
                   box.certificates.service.validateX509CertificateChain(chain);
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .toContain('subject_1');
             });
           });
      });

      describe('Bug #96958', function() {
        it('a Sign KeyType for Leaf Certificate must relult in a KEY_TYPE error if checked with CA',
           function() {
             cryptolib('certificates', function(box) {
               var chain = {
                 leaf: {
                   pem:
                       '-----BEGIN CERTIFICATE-----MIICCzCCAXSgAwIBAgIBATANBgkqhkiG9w0BAQUFADBHMRswGQYDVQQDDBJ3aGl0ZSBpbnRlcm1lZGlhdGUxDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwHhcNMTQwOTA1MTAxNDEzWhcNMjQwODEzMTAxNDEzWjA/MRMwEQYDVQQDDAp3aGl0ZSBsZWFmMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3yWZQRc5xk14TIL3/2UGVgNz94dHq7vg4ORduUvtio4nVMSriQNSPnEbAfV9FR8qA6UCzqXQroNX6eSpQfJ4MblyUWUj7VYtY/jC2UxGcE4GJxhngiwfO+It2ejTr6I7EPlj7pwPOVTDWByaDsYWtJ3JeaVhR9Tcf5L+H5k7HTQIDAQABow8wDTALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQEFBQADgYEAQNoV1G2zwceE1/4VppH3eJYi8u1CUWUoaQe1rH3owm9CH9/OfS+MicUIW/7yRi86rtATnhTcfN34E18AwbJsmESBnCkx+WaVZ/WC1jGJ/Dd5rgxvRfZpwd1RKivKzZsoYm19PVFRDg9K9qqA6dkuq29wTnwMV48rQPkfix9lWjg=-----END CERTIFICATE-----',
                   keyType: 'CA',
                   subject: {
                     commonName: 'white leaf',
                     organization: 'scytl',
                     organizationUnit: 'se',
                     country: 'ES'
                   },
                   time: '2014-12-25\'T\'00:00:00Z'
                 },
                 root:
                     '-----BEGIN CERTIFICATE-----MIICSDCCAbGgAwIBAgIJAPJ2LYuwqa81MA0GCSqGSIb3DQEBBQUAMD0xETAPBgNVBAMMCHdoaXRlIGNhMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMB4XDTE0MDkwNTA5MzQwNloXDTI0MDkwMjA5MzQwNlowPTERMA8GA1UEAwwId2hpdGUgY2ExDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnpKp2eeY1hC4V8E1nUIZJpJVhAZk3oJeEbq0Xi2uSYrh6wmfuQIHFs5rYF4yHoGvf+PM66od8ccHa1GNGB/b6lFb4/+2vEvYWA51OWY9BBdgjhAsqoks9ANNDQ1oxX4dqLwjM/GDDg2v9xoT9tj0CaJnieQbhMkH8oQveBBR57AgMBAAGjUDBOMB0GA1UdDgQWBBRwKg/RntWH8EvCkEX3jtHuvSpbwTAfBgNVHSMEGDAWgBRwKg/RntWH8EvCkEX3jtHuvSpbwTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBADk/OOoc2tbfxja95RqH22EcAVJ/mDin2ev9cXhZ2A2xh4SrUad0fNsyFnY+m3EzfA0KPrI46sEPDeuWqwnGAYccmp7ylVIS6edYRvnI5mdLze2CnuDNJYQzZXjASQF2ih5wTNm3VMZkN0SqGQZiOCa9NQHPfmNj8mJe0Ooj2b3o-----END CERTIFICATE-----',
                 certificates: {
                   pems: [
                     '-----BEGIN CERTIFICATE-----MIICGjCCAYOgAwIBAgIBATANBgkqhkiG9w0BAQUFADA9MREwDwYDVQQDDAh3aGl0ZSBjYTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzAeFw0xNDA5MDUwOTM1MThaFw0yNDA4MjMwOTM1MThaMEcxGzAZBgNVBAMMEndoaXRlIGludGVybWVkaWF0ZTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUmpmT/rNcxIP8Xulj0RcbdvaNGJNoEPMzEGztguB+mKsTPgJIlrKvdskY5NIPd1+hzS+HjvW11uwyT2FnmyfKBqND4EH5BqU8Ph4bAyrT950DPcOGE4vG0fLO0jTD3h0n4d4yugWmrND2sScFZQsGLbgOYX4QM8vV+owoKNRxMCAwEAAaMgMB4wDwYDVR0TBAgwBgEB/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEFBQADgYEAF0B5LQDo3uRs9dSkxp39ER64XIa6XYV0zdbq+fqHg8Qcoggpox7vwmc6YoCNQpfY2zLiIUMbb6VMP4pkp7jZMju9TBKPOjMFrQKom0cowT/gYdokgnsk8yUYOdo46GJR43PjFLTiQk5JDawWaECbhcgyLf59AalX9z9dbY0AwqI=-----END CERTIFICATE-----'
                   ],
                   subjects: [{
                     commonName: 'white intermediate',
                     organization: 'scytl',
                     organizationUnit: 'se',
                     country: 'ES'
                   }]
                 }
               };
               var validations =
                   box.certificates.service.validateX509CertificateChain(chain);
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .toContain('key_type_0');
             });
           });

        it('a Sign KeyType for Leaf Certificate must result in a KEY_TYPE error if checked with Encryption',
           function() {
             cryptolib('certificates', function(box) {
               var chain = {
                 leaf: {
                   pem:
                       '-----BEGIN CERTIFICATE-----MIICCzCCAXSgAwIBAgIBATANBgkqhkiG9w0BAQUFADBHMRswGQYDVQQDDBJ3aGl0ZSBpbnRlcm1lZGlhdGUxDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwHhcNMTQwOTA1MTAxNDEzWhcNMjQwODEzMTAxNDEzWjA/MRMwEQYDVQQDDAp3aGl0ZSBsZWFmMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3yWZQRc5xk14TIL3/2UGVgNz94dHq7vg4ORduUvtio4nVMSriQNSPnEbAfV9FR8qA6UCzqXQroNX6eSpQfJ4MblyUWUj7VYtY/jC2UxGcE4GJxhngiwfO+It2ejTr6I7EPlj7pwPOVTDWByaDsYWtJ3JeaVhR9Tcf5L+H5k7HTQIDAQABow8wDTALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQEFBQADgYEAQNoV1G2zwceE1/4VppH3eJYi8u1CUWUoaQe1rH3owm9CH9/OfS+MicUIW/7yRi86rtATnhTcfN34E18AwbJsmESBnCkx+WaVZ/WC1jGJ/Dd5rgxvRfZpwd1RKivKzZsoYm19PVFRDg9K9qqA6dkuq29wTnwMV48rQPkfix9lWjg=-----END CERTIFICATE-----',
                   keyType: 'Encryption',
                   subject: {
                     commonName: 'white leaf',
                     organization: 'scytl',
                     organizationUnit: 'se',
                     country: 'ES'
                   },
                   time: '2014-12-25\'T\'00:00:00Z'
                 },
                 root:
                     '-----BEGIN CERTIFICATE-----MIICSDCCAbGgAwIBAgIJAPJ2LYuwqa81MA0GCSqGSIb3DQEBBQUAMD0xETAPBgNVBAMMCHdoaXRlIGNhMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMB4XDTE0MDkwNTA5MzQwNloXDTI0MDkwMjA5MzQwNlowPTERMA8GA1UEAwwId2hpdGUgY2ExDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnpKp2eeY1hC4V8E1nUIZJpJVhAZk3oJeEbq0Xi2uSYrh6wmfuQIHFs5rYF4yHoGvf+PM66od8ccHa1GNGB/b6lFb4/+2vEvYWA51OWY9BBdgjhAsqoks9ANNDQ1oxX4dqLwjM/GDDg2v9xoT9tj0CaJnieQbhMkH8oQveBBR57AgMBAAGjUDBOMB0GA1UdDgQWBBRwKg/RntWH8EvCkEX3jtHuvSpbwTAfBgNVHSMEGDAWgBRwKg/RntWH8EvCkEX3jtHuvSpbwTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBADk/OOoc2tbfxja95RqH22EcAVJ/mDin2ev9cXhZ2A2xh4SrUad0fNsyFnY+m3EzfA0KPrI46sEPDeuWqwnGAYccmp7ylVIS6edYRvnI5mdLze2CnuDNJYQzZXjASQF2ih5wTNm3VMZkN0SqGQZiOCa9NQHPfmNj8mJe0Ooj2b3o-----END CERTIFICATE-----',
                 certificates: {
                   pems: [
                     '-----BEGIN CERTIFICATE-----MIICGjCCAYOgAwIBAgIBATANBgkqhkiG9w0BAQUFADA9MREwDwYDVQQDDAh3aGl0ZSBjYTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzAeFw0xNDA5MDUwOTM1MThaFw0yNDA4MjMwOTM1MThaMEcxGzAZBgNVBAMMEndoaXRlIGludGVybWVkaWF0ZTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUmpmT/rNcxIP8Xulj0RcbdvaNGJNoEPMzEGztguB+mKsTPgJIlrKvdskY5NIPd1+hzS+HjvW11uwyT2FnmyfKBqND4EH5BqU8Ph4bAyrT950DPcOGE4vG0fLO0jTD3h0n4d4yugWmrND2sScFZQsGLbgOYX4QM8vV+owoKNRxMCAwEAAaMgMB4wDwYDVR0TBAgwBgEB/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEFBQADgYEAF0B5LQDo3uRs9dSkxp39ER64XIa6XYV0zdbq+fqHg8Qcoggpox7vwmc6YoCNQpfY2zLiIUMbb6VMP4pkp7jZMju9TBKPOjMFrQKom0cowT/gYdokgnsk8yUYOdo46GJR43PjFLTiQk5JDawWaECbhcgyLf59AalX9z9dbY0AwqI=-----END CERTIFICATE-----'
                   ],
                   subjects: [{
                     commonName: 'white intermediate',
                     organization: 'scytl',
                     organizationUnit: 'se',
                     country: 'ES'
                   }]
                 }
               };
               var validations =
                   box.certificates.service.validateX509CertificateChain(chain);
               expect(box.certificates.service.flattenFailedValidations(
                          validations))
                   .toContain('key_type_0');
             });
           });

        it('if a certificate KeyType is CA en you test for Sign the validation must fail',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {keyType: 'Sign'};
               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var ca =
                   '-----BEGIN CERTIFICATE-----MIICSDCCAbGgAwIBAgIJAPJ2LYuwqa81MA0GCSqGSIb3DQEBBQUAMD0xETAPBgNVBAMMCHdoaXRlIGNhMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMB4XDTE0MDkwNTA5MzQwNloXDTI0MDkwMjA5MzQwNlowPTERMA8GA1UEAwwId2hpdGUgY2ExDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnpKp2eeY1hC4V8E1nUIZJpJVhAZk3oJeEbq0Xi2uSYrh6wmfuQIHFs5rYF4yHoGvf+PM66od8ccHa1GNGB/b6lFb4/+2vEvYWA51OWY9BBdgjhAsqoks9ANNDQ1oxX4dqLwjM/GDDg2v9xoT9tj0CaJnieQbhMkH8oQveBBR57AgMBAAGjUDBOMB0GA1UdDgQWBBRwKg/RntWH8EvCkEX3jtHuvSpbwTAfBgNVHSMEGDAWgBRwKg/RntWH8EvCkEX3jtHuvSpbwTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBADk/OOoc2tbfxja95RqH22EcAVJ/mDin2ev9cXhZ2A2xh4SrUad0fNsyFnY+m3EzfA0KPrI46sEPDeuWqwnGAYccmp7ylVIS6edYRvnI5mdLze2CnuDNJYQzZXjASQF2ih5wTNm3VMZkN0SqGQZiOCa9NQHPfmNj8mJe0Ooj2b3o-----END CERTIFICATE-----';
               var validations = validator.validate(ca);
               expect(validations).toContain('KEY_TYPE');
             });
           });

        it('if a certificate KeyType is CA en you test for Encryption the validation must fail',
           function() {
             cryptolib('certificates', function(box) {
               var validationData = {keyType: 'Encryption'};
               var validator =
                   new box.certificates.CryptoX509CertificateValidator(
                       validationData);
               var ca =
                   '-----BEGIN CERTIFICATE-----MIICSDCCAbGgAwIBAgIJAPJ2LYuwqa81MA0GCSqGSIb3DQEBBQUAMD0xETAPBgNVBAMMCHdoaXRlIGNhMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMB4XDTE0MDkwNTA5MzQwNloXDTI0MDkwMjA5MzQwNlowPTERMA8GA1UEAwwId2hpdGUgY2ExDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnpKp2eeY1hC4V8E1nUIZJpJVhAZk3oJeEbq0Xi2uSYrh6wmfuQIHFs5rYF4yHoGvf+PM66od8ccHa1GNGB/b6lFb4/+2vEvYWA51OWY9BBdgjhAsqoks9ANNDQ1oxX4dqLwjM/GDDg2v9xoT9tj0CaJnieQbhMkH8oQveBBR57AgMBAAGjUDBOMB0GA1UdDgQWBBRwKg/RntWH8EvCkEX3jtHuvSpbwTAfBgNVHSMEGDAWgBRwKg/RntWH8EvCkEX3jtHuvSpbwTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBADk/OOoc2tbfxja95RqH22EcAVJ/mDin2ev9cXhZ2A2xh4SrUad0fNsyFnY+m3EzfA0KPrI46sEPDeuWqwnGAYccmp7ylVIS6edYRvnI5mdLze2CnuDNJYQzZXjASQF2ih5wTNm3VMZkN0SqGQZiOCa9NQHPfmNj8mJe0Ooj2b3o-----END CERTIFICATE-----';
               var validations = validator.validate(ca);
               expect(validations).toContain('KEY_TYPE');
             });
           });
      });

      describe('Bug #102292', function() {
        it('keyType error message must not be dublicate', function() {
          cryptolib('certificates', function(box) {
            var validationData = {keyType: 'CA'};
            var pem =
                '-----BEGIN CERTIFICATE-----MIIDszCCApugAwIBAgIJAMGIeWNyGpIWMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMB4XDTE0MTAyMzE2MTQxMFoXDTI0MTAyMDE2MTQxMFowVjELMAkGA1UEBhMCRVMxEzARBgNVBAgMClRlc3QtU3RhdGUxDjAMBgNVBAoMBVNjeXRsMQswCQYDVQQLDAJRQTEVMBMGA1UEAwwMU2VjRW5kRW50aXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2/vvhZLckyuypcLLcMS1c7+lsWsvievgEBXV7IvmUXTF62Sbr/9tVmj3z1aMICd+i8PSV8i61bhY19XZgJP5Lt9CrdrXd7zPXb+Mh75isxkckSFeW3umHhiYdviQ+z85bf8BQ9IT0HVZBep4gD2Xh+cXucGt6Hp/toYyTHx84oS9tmtgFpfYrwSXH1+HrXtNpsXJDTzMzj45H6XjrGHFtR9lWq3ZietGjtqA2fA0uu+PGg2NwLWtYhS20RsJxa2whvxQKJ+1DCddhXR0d4ZC+Cz7TKk29YGlthCeoIpReUjwirLwLsL4j2oFovTHwa0+BkpCDncnGehz7nuZ6V+mHQIDAQABo4GKMIGHMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgbAMC0GCWCGSAGG+EIBDQQgFh5TY3l0bCBRQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFGySZYMtItrtgB5vJdvOX7DSg/MhMB8GA1UdIwQYMBaAFJ2ki7GE9c6+FxQs/6x2JplMoFmiMA0GCSqGSIb3DQEBCwUAA4IBAQAuUsrw61q07SzYUI6BSLXAsICX3kMWiSr6RrQ2c7z1h1dcUbfdm51Vxu9W8BBt/FB4CUa24H6MFb2WTfuSeBVF9OF9Cj5B6WVgsd+fSmmbS0/VF9TYrRKzF59BAkfYcFChwIc70nBpbablqEpNhNmO/45ffoEt5eTitWdB3tyyfa+fgZuMZLQEv+W+Lq355WPmXD6poVEvLv1LmDYZIc89FrZE9raxbyQWJJ/S5f+DkbTIwHad5bvxQsvJ8VRf5ADdfyeAL1qJNgEXAqLbAnWd7hT8dT2oDfE4Ug5kDyxRQZzf2qCR0BEvGDHx5ykXUFMk2IO3nJ5j9m1aBm0qZ9+-----END CERTIFICATE-----';

            var validator = new box.certificates.CryptoX509CertificateValidator(
                validationData);
            var validations = validator.validate(pem);

            expect(validations).toEqual(['KEY_TYPE']);
          });
        });
      });
    });
