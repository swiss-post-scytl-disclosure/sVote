/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('ElGamalKeyFactory', function() {
  'use strict';

  it('should be able to create Key Pairs', function() {
    cryptolib('homomorphic.keypair', 'commons.utils', function(box) {

      var converters = new box.commons.utils.Converters();
      var keyFactory = new box.homomorphic.keypair.factory.KeyFactory();

      var serializedElGamalPublicKey =
          '{"publicKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"elements":["Ag==","Ag=="]}}';

      var serializedElGamalPublicKeyB64 =
          converters.base64Encode(serializedElGamalPublicKey);

      var elGamalPublicKey =
          keyFactory.createPublicKey(serializedElGamalPublicKeyB64);

      var serializedElGamalPrivateKey =
          '{"privateKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"exponents":["BA==","BQ=="]}}';

      var serializedElGamalPrivateKeyB64 =
          converters.base64Encode(serializedElGamalPrivateKey);

      var elGamalPrivateKey =
          keyFactory.createPrivateKey(serializedElGamalPrivateKeyB64);

      var elGamalKeyPair =
          keyFactory.createKeyPair(elGamalPublicKey, elGamalPrivateKey);
      expect(elGamalKeyPair).toBeDefined();
    });
  });

  it('should be able to deserialize an ElGamalPublicKey', function() {
    cryptolib('homomorphic.keypair', 'commons', function(box) {

      var converters = new box.commons.utils.Converters();
      var keyFactory = new box.homomorphic.keypair.factory.KeyFactory();

      var p = new box.forge.jsbn.BigInteger('23');
      var generator = new box.forge.jsbn.BigInteger('2');
      var zpSubgroup =
          box.commons.mathematical.groupUtils.buildZpSubgroupFromPAndG(
              p, generator);
      var serializedElGamalPublicKey =
          '{"publicKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"elements":["Ag==","Ag=="]}}';

      var value = new box.forge.jsbn.BigInteger('' + 2);
      var groupElement = new box.commons.mathematical.ZpGroupElement(
          value, zpSubgroup.getP(), zpSubgroup.getQ());

      var serializedElGamalPublicKeyB64 =
          converters.base64Encode(serializedElGamalPublicKey);

      var elGamalPublicKey =
          keyFactory.createPublicKey(serializedElGamalPublicKeyB64);
      expect(elGamalPublicKey).toBeDefined();

      expect(elGamalPublicKey.getGroup().equals(zpSubgroup)).toBeTruthy();
      expect(elGamalPublicKey.getGroupElementsArray().length).toBe(2);
      expect(elGamalPublicKey.getGroupElementsArray()[0].equals(groupElement))
          .toBeTruthy();

    });
  });

  it('should be able to deserialize an ElGamalPrivateKey', function() {
    cryptolib('homomorphic.keypair', 'commons', function(box) {

      var converters = new box.commons.utils.Converters();
      var keyFactory = new box.homomorphic.keypair.factory.KeyFactory();

      var p = new box.forge.jsbn.BigInteger('23');
      var generator = new box.forge.jsbn.BigInteger('2');
      var zpSubgroup =
          box.commons.mathematical.groupUtils.buildZpSubgroupFromPAndG(
              p, generator);

      var serializedElGamalPrivateKey =
          '{"privateKey":{"zpSubgroup":{"p":"Fw==","q":"Cw==","g":"Ag=="},"exponents":["Ag==","Ag=="]}}';

      var serializedElGamalPrivateKeyB64 =
          converters.base64Encode(serializedElGamalPrivateKey);

      var elGamalPrivateKey =
          keyFactory.createPrivateKey(serializedElGamalPrivateKeyB64);
      expect(elGamalPrivateKey).toBeDefined();

      expect(elGamalPrivateKey.getGroup().equals(zpSubgroup)).toBeTruthy();
      expect(elGamalPrivateKey.getExponentsArray().length).toBe(2);
      expect(elGamalPrivateKey.getExponentsArray()[0].getValue().toString())
          .toBe('2');
    });
  });
});
