/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Stores', function() {
  'use strict';

  var testKeystore = {
    'salt': 'AWnL9mtD1UoAQ58l+8GH9aHPlrDMgeMF8m+0wHyCPAI=',
    'secrets': {
      'symmetricalias1':
          '7mgsaxqwL1oBASLQ40bcP5btCcLF1kgo+382PsIeqkyw3JeQIYsEIdRuUxdmP/9lUqQj+2sxxlCk6XU=',
      'symmetricalias2':
          'hF+vhbcCy0odpXrpPAohUKrnX3LP1sLv9t7QCaBgdStg8rvx55opMrV+l3+ywOwx6u3jKfhTbmL4tnM='
    },
    'store':
        'MIIo1QIBAzCCKI8GCSqGSIb3DQEHAaCCKIAEgih8MIIoeDCCCrUGCSqGSIb3DQEHAaCCCqYEggqiMIIKnjCCBUsGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBQBIb74sxGRGMWR72h36eEyizGvmgICBAAEggTIpNG0MPgmBQSrCCP4Bz+S2WRuB1PmT+2HGFxQyfe39dUZTIIBVEurr7/F1emudb6kGO1YnNXvpVMQ4n18PQS7RFZdTWOUnaDi5ZZSsB+7m+cth8hJuYNsxt8mJs5e8sNQ6HtXdPh78XLxBLple9hWEoeJjUPQ8tkcacBGH/ePs5dANEvIsiNTXcH/Qb33GmprpIAAX1sAcewmWkxVHpy50b7VxHZMM+tz0475lFO8Fs4NYwplIP5FSbsqq2P8JD8dsntdc02rEaLjbIy2coWkUG6bi79wdA8IaZ7PdNlcTWs0navU3LaAC9rznqdUzqmfzA4zDFAVjG2iGzSvmZX1bFGs2ucWQWp01/eNXEb6oLRLkwNXfnEGxBUhAtBBG2kYf+qu9wES6U3e1lTz3JySzIoJoFPVvOUXcpsaQCPvRVqajqZtZEZ2vCq4gQE83e/5/8/shXN8FJLk2hynshgOXeCc/0YbLf29oWXWmu7YR6dc67btlC3kfsZIXBM1e8Egluun8iZwNquPHH/8usGgbHTSbjK50i009x3zIzZH9rzTSxalqkSrT4uLKTnq4OpNEuVm1+nWFpBZmr89Kv4qQvgI1zQiP4zjJD2npxBLMq8iVgNXAMfXmtjkbXZHWFDda2zs6QSg+2wFnfpo4CWs71fvTr5U+J1mr5ojZKQ+/M5nT/R8H7F3anPe/znPiKshp8w6dI70dlnCx1Pes0wy7tnr8JBvQ0tudYPIErYIaaWTDOVd5ceAwp4IEiMJ+/4iRyuSXLZZwHQFGn1RDjGnni248W/gtL9f6+9FR/Zf++RniH/jMcLmZM4Et0sEJ2XaMJKHfLeGFhVCUpKTSaLTXP63pRAq0HZ9eyeY7M9EFUxgxDfXRzwr8DKaSNNi5vXcgf7Gdl2NyjnSugT7sWrzgvp7S2fUpgF7dfIkjIKW9n1VzuvDUlWr3RiodLjc5+6RYj0GcC0iS11WjlSHtDcc3M25iPMztP+MF9KfxF01nSLMbGkUrm/x1zeT2h2uO+0JqKxuBz1VtpF5Rp6Mc4bvgO7Wuu3JC0WG9flGjR8WjSyggzQDlGpGeENm1uadGODxg+EPHsUsEoNaC5scgg2oCQkt+aX+qr5+mzHgqXEEVMfi0mCHEh18GQb9dAKBomFC6TLSC2W5NMvMh5K8piNaLfMZGWjlMpJ2MIiVdT/rS7gOoP2iC21TFK1r3FqQPGYOKx+U162t8diGKNu1Z7EV+n7rUp78UVOJ5IVbvvdUGMifnjAc5dsospWit4hDlMKYYoY57vYo5z+mkFISguLXFZSxjs5Dm5xclqWVqUlz+EHm2D3Hn1PUtd04h/X98sUuQP9uaUHTqjX/L7FPLurIOqAFM0Qv12GO6c/hMCt5BMNbe+N6bsiX26Gb3yDoUfSUl5ZR0H2xhSpAByEc1DTKcKf2S/tGiO6f1lV+/na0plnFOl32d6SL3q31bz/qz9zAayAhGDkG7uBtWxc5wC5rV2eK230HeyPvWdmnWNpTrTHHygvusaYNKr61S7boX5JHrINjPpu4iXuSXtdrojebcFVfVTCVH78cFx9dChxCgmpdVUOugElVvcyhWTv0SQemmFsAOoY3F4OnU1NTVxIJkJLRhUg0Pw49MT4wGQYJKoZIhvcNAQkUMQweCgB1AHMAZQByADIwIQYJKoZIhvcNAQkVMRQEElRpbWUgMTQ5NTc4NzE5OTcxMDCCBUsGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBQ8DLvHwD8yD5ZmHk1IKkdMP2O0OQICBAAEggTI61JN3s5gw0Fe46U7G5C8XW52HwxA/gA5Kz+12pWyg1TZuNgkhVhFIsqv9yQzscYWwRxkJc5pxovOzr50YaGKcTUt1tIRTK8+uzNJoaVpQ+YT+ERHPhLTcxxC0JIRoyY5jfyrMP29GZEYO3EvZNuj81DeI+dsnsLiLPuURmv1O6tg4GdsEQqsCftq/c34ige8IJ4XIOU3nGdnE4my/zr4AcSld8Iub3izt5/5TmpFWwy4wJgAgLS8pbghO+I1dt6ptFBz/b5dGZtlCUam8TCXVnPFGmNQg8flP5DZlZIj0AqgdG1Vci5zETZ+NkU9J/6Q/NTMugA+pBq3txcX2NiAeeERTwflQtFGesB0kcacVRmXXvp6aEfUwiOI5M/ORnF6liKXcIdrUVw12mtJxFqVOI/Udeho0wcymnHaB2rNNaNXVUJdIG5TC+XTmP2xOKQYi/+hsyN6JZTmG4Vhg2UU4zotWQZb069d3CH40nY6QiKRC1Bvh03k60ZctPWIX34jqwQO5OMX4LsfW2MAixMJGVCnxD994mWB8QICbBdnOa5wG6shdM+TjKt+Gx+wCh+4sMGYXVliloOtnxh1HKtVOJBMSDHUcPvfP9FKzo78+3zzagAOTdItYkP6WTSp86pBrjaE03VdFerFnedu7OwXTX2b7C1H29HOueuT5YKgM8nwNPWbf2sCjIZ4XEHbpHpOUxDj1soXcWm0xT1znMdGS5Zdrd49/Suj8KFeOugJSTxJJClTLfdv9K/cKg5cDrFcIGz1WeC7hr4rBgQ5ZTnKh1OYP5O1yVX3KJPleHZyewLzn3jZGhfvJM7CZdd/Sg7maYhY17diWhEtTqJI6cp3hP1xSGZTjwg4K9fAdEc/6MpCh/an1cD1bRDegXZbAo3OTssxK+lo9ckjtvcSt/PXD3dubfQJ6KowtTtXwYZrJHE/XtSoh0puWSKx1z7zqKeHP4AtdxHP0N6n+nIrIzeWU6LLuis+VLVr8r6vne5mCGXFM0LI8VL4gS/c50ZuKycTOObTREi8IGFS58TNrtl1nwg6MgLL2mwdhtINhPDvQyRgZb/W16IRHgbSX9obZvZ17XnkX5OU7T9apmILL+3A3e6tdpoVOOdE5GjvE5ZJqUwEIzRq4ECo8c2+7ltdc8fe9JrTwnUBvgaWkxOEhLw/afmVvwHMWAB2lbEwt6vLWRs9Ppo8m8lq8Cxoa1C00Y6Oh92H9iIEltTB+iws+YFpkfVOCmzhL1CNVO78zyYAROY0Ysqkymwps5mRkCtNbusCjnmxIVgYeVUQiF/DvH2Q7grn0zaVYIC1kQsZeMYJCLhtgOs+/V8QZ5Qj/mHynK0AapUaBdvzib+547E/JeIGnd379PlYOaokOdc8epw8ZPAeuqchtiusDUd9frrx18dgiyLnKN4vnrp83ySiZ/VgEW894KqsU0wFRFOvuPhPE53oK/hlyTYB4xJiaiMA3kTxRckybqIHHDqfqonVGBoqkRiBLKwFzsgrzqxHvoV9D3lFLZ9gOPZ+8l2LHDxLXXezoub1bWofirPauMFicxznQNZQ4BIm+C9MeKsRM6SUyDNvr1c6WsNxzvj13ko5EH0fcYVKyzGdl1npZfuAjNdLcJJx9kd/ppKvMT4wGQYJKoZIhvcNAQkUMQweCgB1AHMAZQByADEwIQYJKoZIhvcNAQkVMRQEElRpbWUgMTQ5NTc4NzE5OTg1MTCCHbsGCSqGSIb3DQEHBqCCHawwgh2oAgEAMIIdoQYJKoZIhvcNAQcBMCgGCiqGSIb3DQEMAQYwGgQUAXxjyvvwa/jD+2rWxKzfYLSCWSICAgQAgIIdaGkGvtHCOhKmLg5YKV8UCOZrfTm6ms0vyt1rwUfmuI4r5Dco6rldw+47C+kFdYWLOP0uHQeha6UBYslKOtTleYzEY26rtwWccVhWmMJOAeoMEyTZxmv0JMWTLFP15nVqSD0h01iZzb2DLhxQRbRfjN4Uy3Gma0P3Db6YtdU8x/NPUwZPVTshywPr3PcnkwlzK98A24KgFWYdAnp2dcDZmi3L7ubQgKsOEBvZvOfixD2SzYDvRFXY0IMsVLWuD7IG4hkRmtPOtqKDnAujJTU/+UqMwJlRl7d/qDsXwMQf9fbpn8hDxeZCiVSLpFkMRTF0pDPGWCqofIPvqEa0snAl7SFV3NUu54YsOiFhCSnroNYdJs/leFOYVO3+M0jH95AmEpBOg9f5n5pllPHKg1ZzroAVOMWbb2rlosWU0Ep99qTtjfaydk1qjksG338DxJzZQQxVqecSxesUf5QVu+Q+xu/yCvR5HM6+wCSUq2WZqSV9fNYncHWIA5yAn4QSmf/3owBtd0bFS1y7ZVyBm2OKWGO7Y28sUlr5nx/Z1tYOnXwFeYch2f/z2onTtGA3Bhu6thU/yGrs7Jg0wjYGhjivDaa82ykYbTin1IGB7jPdm60p1ie7kIvdYOhTD1SjSIxve9ueTX0n9aW9kxOURJpDClAAwTh0dPmmJjiapLiBU1K5/sy0TtfS6OQl3zhbrzf+DlCcVae94SI6PxTDxt9EZUtxbv+i5eQxTF35/yURq5xYcjiMtVk9kFoTMqxTfdRyrzbGV8xPZSXdNxjyEKMlWsWL39RpI0RaQNk+7GbUOlXLUpyDVWca7+Xuh1UxY4gr52wRgy4eVj8XTYfkhAMZ59/wTyZZ40tSko1uRyEBFgqNS8TF5W2yjOOwXyVNGXJZ3shBAnVsJ/1kO1d90kqcH43t4lgSxeBPlbv7+CpNefLRkHKPA4WlqOpUPuWHsRrfejzvSswi763scvamQpqMHjpcI+aYKA/49X3IIGaemHiZiCCSvcd+yMLgkTMvR372BjFRclxYKlUnB1cUCKJ0Zq6TrRHnwRFjyKqFSERgGoUwFtX4I8WbmKXtqjik36kFfnVqcIS5lFy5osJfJd/L3MD8Ny74j2aYa/ECvdC7xY73CfxB0OfVxaDjCufPoYzXP/Oxps0oTahfyxvgJByI36/9rsrqBxb7t/y9Kvwg5pcNvXN74/uf+jPbNA80jF0dAAFgBN0x+RT05dxmsqO5C5MnT//Djmhm6/zsLCk1JqmPtA6ceOqV9a7N2yvIBLAXaFIc2gVka2PixUtsl0wJdItLL4vVj4z6c5OL/tSbkpNq5mDpHSQ88V59w0wjshiyKk+cMuf3OQGxfShVt1ird9vcE5PAwdkoh4gWda3wMhaaQjjK2Q1t6RFb4Ojv6tSfYYfUmnVcRQIkksR4nY7AZ2nqN533Uh9qTOX9+BHQP2ccUpBr4Qa/mVqhWWVPVlDh49/Wn/GdgU0CI36bBdx6FzYsUqPNo3d+YXTpNMCnh1SJ0glEbV7VDP0HmclBQXx7Ik1RJ4V1FHonF3qIEYOHky0GDqeqA9Wms+uluxUzBEMqInTN02C6iV29ONlkwy16k9xIf2JI84SkaChKupRGH2yhGMQpEkrcku6mp9fzPy1d6NDbCHedBVuAfyVqTK7Ob57mN128XU/5LKjiLJ3PqhFwGymqr5XByNfJ2nt++Y7WU5AZi04JR6POlVK9voiE0O9hxAxu26rogOZJv/n9pPFYOB48zW55npeOnZ6n02syJZ6o1aFx7EC3ac56xD0URiBZc7PqZTAm8+mH2y2Lnp/80tcC6CdwAcuK8ss51WB3iwODSd6I1ad2C5ldcROaiqTKEvg95TnGC07JLLLzSXesh1gzqXdxeUK2pkflxDNnJZksZpMfkpgJX9SKOowL0Z6GNJXyoz9uzUxklvQkC8ZeViEJ7rvQADdDMvxN2BtJG3aqiBfldK4J/Rcj/A/ZOy9gqQi6SoDJSBhMtEftnV5vMymOMLTfH79Nv4kYvg7KSrZe4KgsYbbo8t+IGVIOpb65Sl2kewm+OJsORpzqfWOn3Q2qBy1ptHalDas0LXInXF3zCgNi35lSO6SBYuSwvU2VXsXHvoqftO7stXta90hwiYCXLMeRmU5wAQIRZVVkb6VyGMe/G3ASzm9D66qYLeJ/wajpP8irpqVYiYlYz57fOsN/Iv/UhezIGRI9ngUW7mE6duirBdjDkL+MYj+pM7m/hC4iCpvSnhGY3kZqCOovWEEAl7RxIgRkwI+CehZXn2IXa4Ly2wnC47jAI+QTDdvhfYV5JYb6pNRJzthlkdLXnZjrRN2KMZtW3L0qOvMjij/vCxawlh/azP5Oa5lnRESUA0BG+1NBuuJjdBeeyaAeZNXh6/pGZiFuKWwap5RFyRnbXINqZ+/2O6fSNtyNiJwXdyy1r5vNNk08tBsND+25vsm3yt1Up8IWPlTH786tbSW55WoRs9kZK3n1W0U4HAJktWc6HXIoXd70d4opqPZy+sXkypt1OX/EGi/PJLjZOl6/0c8HhO8QLGazFVDKI8bVqF1knlZRMww3j17QZkpy1aVU65fMD5aPaTJsJNBmrtl65Y3G76hDyJ4zyPU0m1fcnvCC8wMTO2UhshCE3NB52gN+Wd10gbaEO/yRNucQC5xAM339J+gAJg5+aP2y+jwnSGcAcmbjdESwjxxJDyyf7i0yf8fZp3IhXB4rV7EMZMc7Aw2ASxaK08AhXh00H2P8TVpwUYX1YeuXcWo1UJ2EHh++c7XG7yKiwXVqgB3kCGYxIlyvzBVmf/7LZdBipE84cveZlkUahedQxBnmmA10EXLjb79KV7FZC4Kh3SxgRBxCgicGvKgwoeWyEqXLc3fkjmV+l6pd9XvSE31r2bNcR/HVwentc09NHKR7tgJngjetmQoRnZitzYSD/82/wpYOp6bRCLAJClhTsHBTkDcHN5KuBxQZIR3hWoB8X/5vDlOxLoJDWDhLxbMsalNj+BJC/9AqnuO1jeSwMywfaTeH3oK6nvn8leOXWayvqtngseV8njPxHUtjAf+yquq9AaaYQzigQCO+l0Jl+HbyZ0NTjzkagGuv8kAfhyJMto4WhZxvFKxB31GVNzzFx2K/KoZLBrw0Ym+uxBDGzFGXGBzO8PQ2LOdYzYwJI9UFG0u0mPEk09rNkmLe80al43j/yEkpe8BOjBnEeqlZZFoY/Pt7F7Vw38Nztxu8PRyWYQUjFEghPdgiO18SuNY0xa0syFo3aoVfZevxVhsWDR7ee+d7gEGzF7McUytlmFLo4peu02iwHvA9aIX4lXgLh4L1nEDteAXmN5RfB9Lie6M9qgxIaLY1XRTA5XoIF+595Y4u3anRkLldHIZlPVHlKBag7+Bl/IEmjBdARQE36dv4804hZ0nZjK5VFSFFFejbSyrB4IEt/gJw5WPxfuX+ADlTNw+NfsEwrt9y36oTQ2fB5afKmYv69q5mxv9MqyWp8+o6RgcgvayiVlWS6mXRQPL0LsorYpkPMRiMjF7pyyPt3DYFC8yhOTx6XUNyqFOK+MRP50bNtH7YGNpnsICHhEw78J5WRIJZyxIaMwnsOEJSVzyR0YbfpaOCcLJFszSijcOXpG38Pie9RZJ3TGp33tMRH8ToWOUSBNDv5KzzSYx46s10xuGIykCThvETYT/i28SwwcdxYXYaPglwdH35Z/ySyCnaeljvC3AbI8L4/KLMpFAcGoN1XNs23Sd2RezdH77OUJq/FXZMfs0dZ/RhFXwLqiL8G+fOQUmbnJZS7Im3p8D8s2ynCRs2ER58PqPbAlB/Ms1nwZvCLbWiM2AyvXj/6vxoWp9etHaC2VvD1Sk87Jf/feJpbvnWI44nhpZU7ZpLtYu4FyJIlBzYNAOjtxzKWBNbUkWxLF0iid56sdWi1q9mKATRK02fLGuDARUhWIb2ijwUDmnddHNJrTuo0LjBSLz+p3vzd3+UFubcJaNbSg2Wj42QPDEr6CxVB63m+8iezhns8RJojpCATCxGRANLOJebQ71lE0VkwlTMfVsiJUqn2P/RZyloDSI4PSLrPF6gx2ZYyz/4cj33qw0XqqhRPR48JjtfMLiwbp5iBNf1pcGs0V5W67is1SKodGw5vxK8MA/0qICDrSzCEVUYDCdMeyeqe2Rfr/f26X0ZnTUfFTeyS36Mr/MzUXtlDbnQ2QrNPFRdstDn6OuyegAPkfFfwagrRDsYGLq6waFq8jMM5vcftvQ8hxSEkp3BoqWtuLYwGpK1skee+rv8GAxiEVkI6Iu+kgZ4Mjcr4KsF7hlV20cSOeRiaqbydYK2a1yzuHp1dolkHCOcj4MGE0JCJGk7FeaGKGk4jHgB324nKwshOKUjsMCnsMMypxZ6A2UQaQDMW6KQJTBj8yKS+m0GyG/zxl56Tcu9bTRVEHwx5i7PvTFSBxySUBVgSxQL+DVSkdUjffRuvLG2uMAX/BI12UgiNPLsuKUMEyLFlWLJXx0AbhsCY5tYPpN4+U1DRubMrZvnOVHSYlaS1BjF6qr64upjeEzYLR53w93tVXvg0wV16U3JCzu0Q7MK961U/EEx41ggcbT1gmOm/IHjMz9r21Nk/i27AiTHdlBUDXqRDCinFJAmP8NuF01A/ZADLFv0ExkTx6pbwOZJD0fOPCj3uF4PHaHm0qZmnhtgDuzTBh7W5T1rtNa97cY79MR53cbvdIbqKNaxpCXYEoszgjL80uK1e41+VWRrLpUI5xs+2lD+t06ehK9E5UaWFimPFJQVKeni+pktXPLUv+cXQ5iHhBhcb02uEVITa7Kb8+ZQjmG1tUffPvonZedqsqcz26hG108ZX322WFoh0IXgKm59HTr4gor/N3CRMh7Jag53iMSmpCoMHjH3IgB5bmnAlLPXt1PacA/bKx1e/uFBXyBlYFaJP5+luNKEyyuAlffwDPJ8HoNB05iI0kpdZZ1XtXHDhelWe/5ZjlEQAYP+MR5vnKygLQqq+TnpIhlGhDbctCof2rZA/u96aOxm5hk98cUPIxcICt06NtB1b8zQOpW250hv+jtcoZyLQavoMS/lQgmbMFD/SoobSVBH5POT79Iq++mPC522x1hXL31g2ZnHVYVJ8RwCB78iiy4PLFxES58fTFcL8/9ez3zMMgyXNQbb9LR6XBk18PesO3TaCYIyekNaV8PdVFbL1JwkoOyc/cFcrLXZ97BKhfjDf30sG2i+C5OKyhzVZoWaeTFjmd/FMjlW3qF3PRTltKAbeXRn9lD80ZCF6V7ZPxDrd1CAanFiqJ7lLDa7edyVchxmST6inucWfqUThhenIIfp5MyAjxh35cGA19uXGKBbMd5JbzpmmbSj6r2zZ4zLvnR0wVdRdk2jNruuwBkxF0Ga7UGNpZTrXG6FAK1o7fiY9M4WSLQ2Mnfm4Y8e5B0JEr+krWVYLiL0w+RMiswKJBpt94dqL5eZyNV7+6WXLqPkx3ZGfzbou6g02zfczYY6TY/5YvASFM6z/WeJHjRR70B4Nraq1aIniMgnfIL6yFeMy1++mCq+Oj+rk/rQLS5LDeH6wWisKFu9R0NmTWnH3AZbA4Lece3YAXXLShUPEPUIB4Vm5nD6oIhV2ZOl8xS8SCfpIY5qXrIr7i9i7B/BO3KcbDFamfwYLc+1e56VJzb/Z8CgepLEqXdu4uVqY3DDtQTlhn9WPVpimYPQvYITsk5jmC6E8ytVjLkV1xT2nCJWV5W0TiZW7i2QH82E+FDzOxjXPlzGJ2+CiztqZmBJq2j2J3r1v4vmBlNgMh6k7AoOMjidDRIc+X3m1COLzGbjVt/tu2W/sNCTzaluOyhMXv+qexRX9N63tDPx23QRlget/ZKN21iPPG0lkIjybY545oMRO/ATtkOdnLXhFZjjv8Lh3NEJJuZkYD/eU1XtdnqjgId/x9jH4RswuKpTll1GFOZ8lagjBvPTYt41IcNwS47KRbTJpyphKetoYhiATx4qLZyjqcCHhBm8Mym4EWg5IYWAJCmGgbgLSbrbKXf6qSx7lBs31kUMyIhDH32YBlTshRzEl/466BFpID3U1kz5ANGCXpDJ1IPMKI/jbOiPfE4Fg0/PctWNNSkFGQgmiZD07uazYGeuGrr+d38zaRcm4LDqcpO9MB5ZoL+sHERNQehXXsJGWxiFSBogwNPMK/zHC/FyU8Djd0LzYLzJHAMKBMi210JUcpdfpj5Nba8s7qBZyH6fcri2CK/BBYa8fY4l+mipqbaX8pZKbldbdY6DYTvQASxlKG+mojzV579Q/U3NTZ3YtcBz8xhYmuWNGhJyruAat2VaN3lusoi6aIOltwsxbUqF8AYNzYg9++umwDzXxDEMNs6STgNuS+65MFtcJtByNheDLoRgnPQxRaDhOXGwS+5FAEhit94vC0zVWIBDaYEDCLjbAHLqQGKmb6opr8LUPUCvyDFllsJUb64s6rl9ywd3HM16AVx5HVqVeq/8cj+jDWr68XaJxqksZuJUiTIiIJ+nJINMF1OYHsDzX6IynJhjJoJD3sYjewYOOHf1Qus8FY5827tzglliVcxmHHeMLgBONjFVZ9eFszHiObzmhUv5I0BA2/HAZw9gege5EPR/c0qWsCUP8pxF63CIIoDoxcOp+KJbIMAKxR2dPxY1Tp2nc7xK6IysAA+Fb2Y6KHCk+FQiNnSw7Et0jh9hRcxu8/WZVT8+AR0rjObIr5aOuig3Vygksor4vU5M9q7hiCmfy80EnrYUBIY3QNIEWLHO3qG3PYKySTaZ5I2wRWI2c4FmnjOmTTGq2e2SLcXVC9o4s2+gJT+ZXXBu3cMmgQXyB2iuYeFl8jGK2Mf32lh7yCKIr6ynEDQ7mLfdNLYcguhZuDQss8Zvh1o872keU230w7da/U5NVkvkHuBF/WEubEzB6f8b/2xnh/5nWGqUe1GfZdAzZiGfcST4ocjS9UvqkLmKQfn/2ZCNrELTUtx60JlCiwWHZYRSRfTqjoIdYQKWgAsInlhDSGfDvc8/443reiyyoUSFqCvVvxoWMb0TsCkBmRlVcsD9MyKlK9H5QqDCNvgPXxhmlYzUUXQGSH1j1Ebf7Fs54NHxPOv8Ozguuu3na0KqIEZ520zK1K/QD9mEjqRNWJuvpy12s05coFcqK09ra253UdQ+rF69UYrNncPZFFvXbnQWQOwHGc3MfUjHACY2cxKn9MysWU6a6fpK63tPAd581aeL2S6mt+XUTpmf40dFxuI7p19U8JzK2OSHxopF5era35achPifJkkUUncNlla3UU3g0O9OunkxQ6HuwNndaxYrHAtRgRBr7Ltf7C3anCObpYwJx+2TLfFBnJ86OcwSySvn0Zl+11WyPAvQPZKrTpWsPYGJlc/dlc4/XahZncXu5bwblLWLnUdSt7eRWozv8BtPtzvl9bydSSOzM9fRgRGbR+7dqdV+lNuqu3uiZO1vkejZufGSQxeD7PDIEpb7ZRL6DTqkN5plZXBe0/S+8BlJCLDYhhBBtT+SaNsLYv29TQhQ7jpDq12tztm1D390v0sG0toTvHbpzcipL9JFoml+9+kJplz1eH9YrVMNHlsd+Azb/ULZMnMr8sCBX4fLbOL2lLzfV73JFBwtAJ+uEy3DvSgormZDAPqf5TzASFl5ryyt/Ek9ogn3VysGwKDPtl/neYn/NmLXjT06rlv29zHT0qVFBWqEIvKUz+W8uhDA4cX/ONK+FB/Z6t9Qr1OTtAu7ILr2XCbEMTfq1mtvePPK5AAP00fSEOCLZQ5JD1Lk4gq5m6kYVR1bYNkxZCnglxaSyf8XfVJIYQkmJreilBSpHKEp38q/Fu1EQf8VMIiv7HlGstuhiVJ7i+DARtguCem3xm/KRXq/v+3qex+aq1AHrl6GJbO4S1EnY2goS5+fvhJoAfLHx9On70JU1UA9t87fhAaKLU0IK0PNV+izHn/SlLUtFKuXIV2a7CAKSoqrR15x36FaPnowdPDR6CtzHJMtjhiLlAjUTZ4UtcEwc3MtHmol3XkIdahDXkczt8Z4hTPbBhHpuIKAVWFrDgshyuuCt+MPBvTh5EtZhAeF+u7DtgWVER+fxRK4XRSqwtLWXyJNpE1KD9NiUTF/ifDTjdZlWyGOsM2ctDC7NnCBMBC4+djN3uWWc9zd3ZoG3KwMWPYynMBXu1AYXAIYNtrJ52bCuC3ksjSS+de0mx+Vx4sVhgw9O5JfvnzQGkjnX3OnEATKo3pu/J/8qgXExXLcyv/TMf38pnm9cY3bO8U134i83ojJB5rcFR/m3OVNxmM/f/crT3spQb9u9ZEfXJnkz3opCBaX5o/WWijXltI9ggPsZFsUDkfbxgS/YKMGrifGs9M9t2yNVAtkvEkqzeWZz9rmOnm+uBvXeCbhOcvPHBI/Jna89XHr5wo7FQGMmLLgMIInhgOa6ONydg6qWwqpVLTWG7zJHqv2JbiRoyi84PTSt+ect89RlV58CiakL6NOI2SmThm6JT+kb9rggTDCjtU7VAUZiAbsmWaptsk8+OHNfV1kc8qbFj8uvTx9cpPHOgzCDfg6Q+Azm0SphH63HF4j1sTV336rZp9IhdbF68d8IjExgyncn88cfxFGHOZ4s1PGIJZsvvXvhp8ztjkTPJcTGLVL0m5AxlWXEYnIWI69s+pGtoGVyqoj41CFCvQkt5x1HSiHJQ5GenAd/AM1AtwjktGjyq7U2OthwLAOTwOIbJgkFRVGPp3OsenOMzVE8smzQ/gteqLYZYQzQ3R3d7dYpXqMfBkh4qfoTXsaF+3M+WUX6bfy9bGSKxSVfNM6QsXOKm+XK4Mrgw61EkMBQuxH5nsMqR8j9fsSOU9HaLsfndwJlZzuD5fP8W65BlJemeLYRtY0thhjkbNblaEyqLMVuvwqESCdldoLqjwG299wJRTNrqKTlY8duj4KE84e7NWLuTiLB7KQpx0P41YSGyX9/ZRyg1/HvnF7jA3Z+VH9ZmN6hDycAiVCp3RbYcTiJpc82liwWcbPeHz7vs0F+B5KOFCiunLqjK+tQ6cutFytIIYsrFhnJdYr4u5gCQ2fO6I9SnH4Gl6v/z+87IdcE271Ixw9cYZMZBBsR1KX0Pt2ycbhaGd9y6d46ehtep7G/QFm148Xs7PNEK1/IFJ2OULYKAMDeJCScZCjMw2GTW/p5u/kV7kZ6NuLKN70w+Qym10Qq2NlA8phbwG/M2vP7pVwqxlV9ZpnJzlbWSec69SujR8HRPFQx0fBVsjdcL/g1MeX8Y9FzPRwQuTZiAmc320HBJO5bgixmkZgWrfESqQQx96mXZR///Cic423+D8LViSqUO4pfX1XSttCLnJtFKGvWpaOmfEK6OsVFOeP24Cf3vXtbtNqev/84zTDs2+DB+FEJq1mFZCV5O4iob76nF214rzZlpc5G66q1ZQ6hWPu14h2VdSe9opp6rVxk4dRg1JdceNZ9PmvjaiX5RtS0THKZdzNgWbj57bX1TPxFwprHREGMQWgX0ioK/ZI+K6ZfmLtuv/9Vh0FEYuWps2ACSaTE+bR4JmonEJPg15ANjOzug5AtyVIGum1fMKp0jupyK1K8qvXZ7uk6vLpHvEcwIhPqC9luipgJjbVTHGE0cPNc+EFfdnM2QcbcSPiZosZlFxB0M6XqnL1KGPkUBuDZQuvAfC057g4OjU8Xu+5OuruXgcMnMQCYhCj00C2EvlbUc/C/+iWLRwQWuMJMwVqxNwf8gvBCLaiTQOPlUIIFn7JtFnqxb4wR/eV0nMbcKEbsmCVDICnwCNp0uhMivje0ECg8rYzUJnPIRj9OpYwUCO2/t/npKwCA78oW5Au5N1wlgh3zOOigUHQuLOp4XCJckJev/yKCehbyWFGysupEXw9X3r2NiumDm6Q5u8deARy9N6oHZpkNkr1z3uQhgn0A/q7jjuE/v2q98nhJPCxShkfJjW0O72O+bwgPmCL6+75Q0NNu71/7F+Ex4tfxZy+IJEkU1NdNBztdxPC4whxxAx2FHlzwNj6BS/8rZVcmSOzfiqIdTpxrvDHy7KDtw5EIipTY5uZIZ0wPTAhMAkGBSsOAwIaBQAEFN8/+hqTNNrLi6iVazDqnxResURRBBRe3Z0mBQG7oRJl7k0p9XHUJLJI5QICBAA='
  };

  describe('sks', function() {
    var data = {
      salt: 'CpHq+38iqF3Ls7j97qBQ2A9Diy8=',
      secrets: {},
      store:
          'MIIW1QIBAzCCFo8GCSqGSIb3DQEHAaCCFoAEghZ8MIIWeDCCCrUGCSqGSIb3DQEHAaCCCqYEggqiMIIKnjCCBUsGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBTi2se0MOLF/EwAkroUjfqP3o13LQICBAAEggTI3MsaE5UDYPZQi8//fWVQEu/Bs/qP7sMmFxey6nABtBjCx0Y5CNYyNXPbujpqGKSpuycw3cgMshhY6KCDsOezaWPJ9Fg2gxjsnoTibdM7G3BnKTkdF/FBYPZaa4Fn0DX9mOZ4/3dLlbID9Bapp1xtF04SBuf6f+Blv6tXXkzUqTMr2cXhVOuQFnT/48XqA9j3k+hFXwdjqH5C1mBX216Se0Rpwf/2YJweaW7z21w0BfcXeNUQB2jfHV11C016XmMVK4RI+gsicwCI3QK0FfmKkPdpuDJw8fVBPykHFN2YHa3k+jlD9UrAOb5MvgcV2EysYKPdfAux7Ih/vwWocAD0j5gIAbKiH4rpTr9l41lQbbi3VpTRGttrIr2rWB8E+ETnE19/ywmhqT+hslZQaW9H5hALKPqeC+JMYo7Kf9Y8YOPHUTYUtklnrmzM/nKEcZ0G+c5uoqiI6imZDypyxDeUdci5ZskNun7w/VV5NdqMOxTvPS1AJ1xo47PrSx41vXFrzTTNvHP1Ma1h02XACTL37bfM37kB5pBKFQbm6aAQkAzvkwoS3qFJDutAO096EVqJbtMtslW3kwh/CF7Eu/h+bHiGLCB3kklOA6T4zNhYAgJKWzFpI0opqIS/zRO36UNyLqwl7xHT38mlgao+5Mngsq+p8cYxSPc13aaPbB3O7o7ofFKUTAPiRDTXAWFw8mUQFbFX7TDpAhVci6SOA2lk15w0QRsrT1E/nWouZClCkPJ6BEGLFIh7MmQHqLxuPS2PchJfzfVlQ5s3vohz3dVtmlM7jEN/uJcZlYL7ZiAQh3KwgrYtV8SSSRkGKPJnATmkvVFyW25C7jn1dF1afC9lvsXKo/7Cd144u5UiAm3V7ovltFD7W6uCJXex7agwBMwx0HsDrW8zTV0Etl4KjQmKlQBaiilpIvQC3Dx8X15bZp6tOgixdLK6e5LQCS04IZxGSsWUWS8mmQnTXPxfbTTREesqGyR0BEeypQkdS2OMd2jcdfmp8FQ91JcB0l+FfUWgT9llqRO2dNxtU/sNQ42sHOFTxTM6/sS5fJNnEXspFP+RExp0ulMNARl+Fdt3wE9Va7rKXSvZjnDvYPh0t+q4yTGWC7cs5JxMMkM/Bz92rh3Bd7Rocj2lY4S0st3jFJtiUAiPzndxNqCVXVGXAxB8VSkbU+y0W1YWFLsON47O0xy7kBPrAKVoxL5rOxoaxYSJ+zek2F93bvZPSB01u7DRfejeYVCcrWFpvEkoREL53OYQo68pdDZ/fpmo8gA/CNZJOZZzpuEF3o5uEJhHEFDEvtOTDTBN/bZEl3pWeHUFrd5TfmKol/hDEBSJFSavwHtpG15xH5AUmSs2q6f99NQG+XmPuitx3LtRRM2FAP8lxkzwzXxlYMRU5Xo9sd6G3c2T+XRVylglEvuKs0vXeFLanHVjAYQs+k/0XUg3bkiEas0SrJky50O9qbm+Hra6pRNnDpd5q5yVryiFIeazf0PdICVPZYLfD6G2VSqnaXh67e27I+I8mNw34sN5/UeE69qFZs7NnSa1o69X1hI2JCTzumBckMGiy/V1jrgtRdx0Yk6709a0ohif3Un31pBJi2+5vB7SG0xdWgXevBov6mXunAfs08fvTVxJMT4wFwYJKoZIhvcNAQkUMQoeCABzAGkAZwBuMCMGCSqGSIb3DQEJFTEWBBT30Y2c9DlDJOloZti2gLN4BqL68jCCBUsGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBSaG2nVpGYo4ktGgMtzrxNfWqa0wgICBAAEggTIbwPtlmL2AMtW6zLD1+4Z7gkIsFflDU1KlELK9cPxJb6KPK0hGZseHPzgBVLJhRuc08O3WQdJfQ9+fNUedJOJ6EaLoFdY6/L0wexox7M0f+OHLGU6rjpYwnQhFQdpKfi+692FVfxYsdo4cqm2rl2jTSb6yxS/Uyd2EPZrmEdzUMbnaCbAr3mH/KPr19is4pnZVk90tuxbFZpziRAr9AD6NW5yLoIZ1AgP3Zktu+7J9S78rVp0GKzTop+Mda1A20Di4ZKn9zGLUVtZASslVBaT9dQlrb8hVUxGTTqwqDDVfxjqs4TFaolqwnZgLiH5rwK44q4mphJuV+1NxpqTpcmRiLGukwonXpe54leWUXZegAV65lh3QNmkcmvh4Fl/lSw4pkeQ8zmk4KWvHz+Dupbxc6OxP3DkU9Z1ol3iY/ax5fQec8Ffbl+OXmByzs+hM9NDsx65Re2sNUXdgNxYe7SNi2fOizUYQ4tTsexfv2moJYZkQwGfxPlvhm7VeikZ7Rlxy7nJ6iE2VjUKJte3dCmT+yxU6Hdk83vyXEQFT27NbKGnAkdBL9r+65oLj2zTNHXx3fDgPRIHSzpizZwarceMsovfb0wWzSX78NUCQLPtYEUOXdTcR/ATcSXj81MasxKKhCxgKDn+M67xhOO6Lp+VPzYMLHG0lAP4r4rScia4OQ/W82ukuaCErCQi1wzkRHPnQe3hryxjFAAnsCcHPH2d/4N5BD4oxV+qStZj0GTIBpmG+xvlJVNdmU9v9qX76BQCJltZ/qfxeFNaYVFxIdNyaRn8OK9HcLOJ9Rt4XNXu6BUeQjB3CXptZCgiO1VWVpkkkgyfEY5i7symkYa4Ratc3xDyLdgSpb3XIS+joaWhJ3fBAOsCTOZg4fbgBL0DbqiZ+CuBDiZMazZ/Dtu7Z2j/BIy8hThn9FBXg+UiVcL8ahDbs4fHTmr67Se9f4J+sppQBYGFI+uy08gWYS58RuCpyuv7SJbCT0TzidVhF54dRhsH/NsMMS/iA521lvoTSXBWygMH+b7SX2vRXmYdTsYNJCN3eqAdbdiBsrbdN3K8hXClzWPbQxAnRiv+C9Uao/grTjPwkVn1yo6ZmsX2DZgcoRcMnZDPoYbgBY7wprM2231gpQwUanPaZts2901Sjljk5VlDiA76CCya0UD4KFOlwjDEUvy329H15KzyGQAEuq2NUTm+t9nwl/wkqBar+4nQKaAsw730eUMS7vjRL8xR4Vibat+JyEl0NqTjJTht3zpkZJGDPe9g6k8UMI9tD4/i+SBA9t6NmN4b69k7ccwzSohb1D9fCM5L8itS24spxZC6ZWuUiwVl8uEqOfTCK1f+qFDCsNqhW2PmjhuyVmwBulsKmZ4EaJ9K2vlUlavVXVcYJYeo+FQvqjpBKk/2rmuvP1JMfcEDfhMwwnvc8+cnDwyF2yUASiTKs4LQACXVBID+UiTRsXQn8Ehtcax1R9be/L5ieEj1YM8xoUNHL0+9zJ/cz2xVLptjAT+H0HzZjS21bylwzpi34z8LoR9mr6wiHrjjSnv35HfsuoZlsQRz9eDU1wqmrNRyB/CRbpxOKPfS3/Iz0W8yNi7rNQSGovji7Oq3ga8Zfr9dlVDZCK0grgOSx1maw8UAMT4wFwYJKoZIhvcNAQkUMQoeCABhAHUAdABoMCMGCSqGSIb3DQEJFTEWBBR8jDd0NRd1Jy4NBY8mvr6QLOyc7zCCC7sGCSqGSIb3DQEHBqCCC6wwgguoAgEAMIILoQYJKoZIhvcNAQcBMCgGCiqGSIb3DQEMAQYwGgQUM4okkGUcsM2Ra1bDvAvfYgJ/uX8CAgQAgIILaP7zBhm2wmmUCTNM5y6XgAi79qM1FZBctY0GqQziVb2fSX0cYMP7rUUuoVbywJ+ZXFbG194vjbEXhAWISMhNqv7bdYdlZgkFU/JRgN0PBOk5APykMtNxgDbHNeSVsGYW4Zzym2v2JSAqronJwso0TrU4JZPDT4SrZrNt8alyNo/uva5OIq6NAB942jg1/YY/FrbsE4x8UB4Jb3kKtGmisPHLjpLS2akOC0wlb8CDxgV8+jQPVRtY5BVL1G7qlJWmy8ohkhsM5EqW8xX8QyW1vQXTfTnEeLi/NV+H9g0ODbvmG9MwKZVuN4Ofb1wZ476K6nQm/XhtUfSbTaAoGu4FR6Pt+qNvZq4CkzPQQnWHYDynGTl+LumAp3978q2Ukm5j0FUtfGJYE3T3wqFgGfyer2cOm65ti2/FohWTafD9wy5ecuq1qiXCySQfiCLj3o0eLKK8qaLu2OMUWzZ2Ntw9w/GnXa4nbuaOAUGBSOfSO28j808ucxQuFWIY7yRxGoGbu4aLIIMbinu7OUpElacOMZ1kiDfCjmXKmfD8uodpEXN6AnJAQVkOMOSOjS9mf83xG+3ZjwcKoMikM9GjQj+/x5xugSpX7FGBeHJ2b5Zzeg59uYoyh+KCXev0FQEvdge+/RG2dBrwpAdMx8+dMaev8Xxt2+phlLIhdJhNGlHW18tmqVegIr9sTLdyy4uOcG1ivZ+lr1q/n8GkehdHN08Z3rJmGY/Es+D7CEcSvzRSMS98d9za1jT4Xk5gZTDHGNHCo0KqhPdVJ4LiBUv11UTe9rOXOV2l3CmTMSAh5Fy2sblCSkFq2+KJsWNgcSOmYNrXM71TihqqdvltK3xMQugXzSgKD5riaocmQ1LIwmFaWx1vZUQy+XjQkvtHf5jROWrWG3AFHCRq7W6gcvZHlWc8U5r9TMrc44+ZHUcb0cCNHO1TB2EYEyZDP+6FTOSdtZORDAq1oaOPQxT9/A/XmTVsHgHR1jUXvwZK4tXaEKFQlZKHWdC3OtjSpcMZ8qMKCyzhXJB/Za0hKEjqNEAUZixfn21HoX7tEyImsAIzYRGlxSSwFEazE9BBXVB+t0DKsBu+jNL17dOEDUs43rY43nZbbgPszH+IAnjjD8Qgx9DQ7Lnt6dD+spIPCvXXl+vlmcKvTDhSEJ1KM5tX+YhgFwI/eBAO0s8qE+GVr+Yba15gvdy27nWeo1GZlnlPx1FIWacbqBIbDJf32zFV3P6xSeYa5k3eo2va31KzD0LzqISK+ygVsjVr6GD0ZRU3GIEoUeem2G5r94UhLlC91nST7R44ZFXiiTj8e+gI7n+sdCIrZIU1SxE7Hft7aP38JLByqnVtwmJ/DCIig8eGm4rNlWYXPyc2nJ9F0yQOfFJLcBOZjhz8uW396IpgauCRYe9uW+2m71XBPb5/YePkdcvhbXEvhJR7shP5mktF+ycdJV1AJSM8OotlnVEZfqGwB4Yr1QnbE+FpQ7IA0tIi/awIEaVYdT9eD7z4mYyKBH3Gdv/+P0w3Tv7+tsX6hsabQqChTKM85mIZTp3YyjNNq8hJOa03Q9/F1uFTzrQMfnH2mgm1q34j0LiopjQVwWp/nSVSfkxx1kzAALEMYuROlCxeECX06xujZMH86bKEedA7aaUsNsSw8tQUTrquacdBBVDCYc+5quOoOe7zbCsieiexgxXmjCJiUh9EEQORqgiKYnZrp1Pv/pERNn5lM5AaOcljQ8msGxvzzxtdRKmaDjXvnPAJBs7qigITqmigCv8+27PxsGXusXSRLsTZF9qKvDjEHE1B31LCqRN1xWM9XmvYeSWy+wteJRNpBTflAOw7dVyXbT9CYuBMYOyifwYsUS672duwyNpbLq+0UrSbK+OQ+vpx5wqHf6NrWgtpcw99QcwZlRhUKZrjJloML80+5Rnbamh9er7XL3nyovJ7sn9GEWYBSkXTg3bfsKEnAqnplfYchPZkDqFehOAQFicx8VqUFuKqxcP2FYrx/D1VAMZMFl/KkrM4Ua8zLLUnWtaAd5zqb+9GLZi6IPa0fqRXsymNl+w8BwBBXWxE7HfJTZLJe4BAQxFtPsVtERLOuQ7mz9yaIhvt/Alp2zsABVRJvBQpvhzD6Z84JP8VytE8hJyy2DBYe3GBfUBSSqGxR7iB1SxLAapSX8TZvN0jjdNYzUqxk1YMhjcCjnJ3cNe70+Y4da6pbJsNj/ma3t+6us7+aw3uA/PmJUOvuLvu7PBM0djM+iK3rE0JezhbmhbjaerZ1aY3Bp3NU2x2AZfEuAKBye59skRokF5rgGGGzlp7Ajx8I/fNHnAvMp897jv15zj+EX6Uov3B29vD5Fit2hRTo1j/gTCCi6VWBAg41oueBxwk5onZ/HYrqJuwk7RITJ532qo98ly0jycgYrkUIYbg8VLB5s3IpBlHG0NdMUZ11Xz22mwEoVTJNLQYLXOv3WhwbqX8F+JyEHIPGQjaAUJtp6gk2U4CakQWAfumatU16tWt4JN8Ty0fASBlMjIR17f9lFCI9Ijy4fT1R3z9meDitU6ST2yVIvk/jFAAWPj4xHTV/BbO0ilzTU1hqrkjDf83tHzCnRzzFcAbMdtdE5kYo17MMBdnlFakJroKcqMhaPyz0rOtL8hjbypccXMbj/ULYOM2FbvMxBSoinRSCGtijj4YN2eP2pTO5Mm1TBe1IeBEqYrmx7oKSo+BauQDFDttvW3eOsN8qSIFUYH1oBykOfIchLd+L67hy4ZEr5eY3INNNzR+MbwAtH1D+yC1UfNwLrrwZNHjorRhgmRk3/Fml/1Si/4ciWkYDCBdnG/ZLTPTBmqTBCJCimjpziLGIEGlJDUx+JDKpbqm2PpLXAJSPzkJt3A8RtXt6P/nhiyEXHGLAAQC/Ug/0xuSwzyC8K2o7xTq0fl4kde1uT/j9pLQXX10ONRr0LzYtq1W2UfGL8JLot6Pu2rFNPS2PhJceePnmm52jasvQD+BuZVKTWtZAGaEyzGZxoAzShm4h3BX9i6QgzX6U3GkzfrbJjTo6SlxoSIwX76+Q70+gbLPcHvnY7bijCm8VXTrdh5q5bNvRGqxmyjSsk7nB374e0dP7W9WSYaW24HQ/C8zy5/+SqmHl9GEAw1ZnLK2D95rav/9zwPqZn2X4hwMxHvVWa4sRcJ2DDou4WM/0U5psck+EFcGgT/SQCi/oGBIsI/QOPhEUoCa5qA7/WiJ9LT0Q5G1bC7A/QW3Qs5nB0CIe/q5Zic2E9brGMHOdpEDG5phzsAEikO304h7xYHOmXZ7pgynFONmANlfPMHGGKjyLcHl2gzMmtIP8e3YHVXlkSfTDjKL7a2YOH/qbi2Dd1Vn5oYidhhRFpdBAEyXpevey6bRTCc7CK6I6pdMf9GunGJ1eUIey9KtyyrDoWlGPJUvNLN4jT53cqpnmAbX3CUoE/EQ2bu4O5+1AGXuPuH7mfbgf4S2Eb5mNjfBeMR1UkL6yqnWkybl9fKkAG3ofn0+8AwnLh8TzL129555/lE+9jOVzh2zxz+nsDpdvjB4e9YIfhCZ1qY2e3Yc6ofkH6ZMZx2/yDSRWqePH8ncI89u27i9PyeWsnu3WS2DC05MYuGvbuaa6PXhYh49GTpJHWbO7bYNnk7IfUDm5PLQ0CQWM7dHcJY/NlaqmXpjNOARI1DOwlyH5wvFuUShN0szBgVcm2gcJ1Pkl+nlFRespcirC4aORHnXPY5zbUQdkPQVFxwxakGHznQtCgq3fHCDONtgVm/n8gsDbgAQIcIlpPkPjV8OiwzTTuQmn+La5EEl0JQ0ZmtvC3l93sUxvev39FSe0BbqWRhPG3JJPSLCddPN/bIF+eaYRsL2xcGyQHyZ92ZTP/4OKb9tzMwHqp9dIBuOKZa7TlgMIAzV1+f1YS7stq31jqYwPTAhMAkGBSsOAwIaBQAEFIL4ft7A4v6F7xHtL5968evRiWjOBBQsjehM4e7vvVXPa3SEHJ3pPWItIAICBAA='
    };
    describe('sks content', function() {
      it('throws an error if it does not have a property called salt',
         function() {
           cryptolib('stores.sks', function(box) {
             expect(function() {
               box.stores.sks.SksReader(Object.create(data, {salt: undefined}));
             }).toThrow();
           });
         });
    });
    describe('should have a sks keystore reader that', function() {
      it('opens a sks json object', function() {
        cryptolib('stores.sks', function(box) {
          var sksReader = new box.stores.sks.SksReader(data);
          expect(sksReader.keyStore).toBeDefined();
        });
      });

      it('opens a string containing a stringyfied sks keystore', function() {
        cryptolib('stores.sks', function(box) {
          var sksReader = new box.stores.sks.SksReader(JSON.stringify(data));
          expect(sksReader.keyStore).toBeDefined();
        });
      });

      it('throws an error when trying to open a string containing an invalid or corrupted stringyfied sks keystore',
         function() {
           cryptolib('stores.sks', function(box) {
             expect(function() {
               box.stores.sks.SksReader('corrupted keystore');
             }).toThrow();
           });
         });

      it('retrieves a secret key associated with a specified alias and password',
         function() {

           var f_main = function(box) {

             var secretKeyFactory =
                 new box.symmetric.secretkey.factory.SecretKeyFactory();

             var secretKeyB64 =
                 secretKeyFactory.getCryptoSecretKeyGeneratorForEncryption()
                     .generate();

             var passwordBase64 =
                 secretKeyFactory.getCryptoSecretKeyGeneratorForEncryption()
                     .generate();

             var prepareKeyStore = function() {
               var data = {
                 salt: 'CpHq+38iqF3Ls7j97qBQ2A9Diy8=',
                 secrets: {},
                 store:
                     'MIIW1QIBAzCCFo8GCSqGSIb3DQEHAaCCFoAEghZ8MIIWeDCCCrUGCSqGSIb3DQEHAaCCCqYEggqiMIIKnjCCBUsGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBTi2se0MOLF/EwAkroUjfqP3o13LQICBAAEggTI3MsaE5UDYPZQi8//fWVQEu/Bs/qP7sMmFxey6nABtBjCx0Y5CNYyNXPbujpqGKSpuycw3cgMshhY6KCDsOezaWPJ9Fg2gxjsnoTibdM7G3BnKTkdF/FBYPZaa4Fn0DX9mOZ4/3dLlbID9Bapp1xtF04SBuf6f+Blv6tXXkzUqTMr2cXhVOuQFnT/48XqA9j3k+hFXwdjqH5C1mBX216Se0Rpwf/2YJweaW7z21w0BfcXeNUQB2jfHV11C016XmMVK4RI+gsicwCI3QK0FfmKkPdpuDJw8fVBPykHFN2YHa3k+jlD9UrAOb5MvgcV2EysYKPdfAux7Ih/vwWocAD0j5gIAbKiH4rpTr9l41lQbbi3VpTRGttrIr2rWB8E+ETnE19/ywmhqT+hslZQaW9H5hALKPqeC+JMYo7Kf9Y8YOPHUTYUtklnrmzM/nKEcZ0G+c5uoqiI6imZDypyxDeUdci5ZskNun7w/VV5NdqMOxTvPS1AJ1xo47PrSx41vXFrzTTNvHP1Ma1h02XACTL37bfM37kB5pBKFQbm6aAQkAzvkwoS3qFJDutAO096EVqJbtMtslW3kwh/CF7Eu/h+bHiGLCB3kklOA6T4zNhYAgJKWzFpI0opqIS/zRO36UNyLqwl7xHT38mlgao+5Mngsq+p8cYxSPc13aaPbB3O7o7ofFKUTAPiRDTXAWFw8mUQFbFX7TDpAhVci6SOA2lk15w0QRsrT1E/nWouZClCkPJ6BEGLFIh7MmQHqLxuPS2PchJfzfVlQ5s3vohz3dVtmlM7jEN/uJcZlYL7ZiAQh3KwgrYtV8SSSRkGKPJnATmkvVFyW25C7jn1dF1afC9lvsXKo/7Cd144u5UiAm3V7ovltFD7W6uCJXex7agwBMwx0HsDrW8zTV0Etl4KjQmKlQBaiilpIvQC3Dx8X15bZp6tOgixdLK6e5LQCS04IZxGSsWUWS8mmQnTXPxfbTTREesqGyR0BEeypQkdS2OMd2jcdfmp8FQ91JcB0l+FfUWgT9llqRO2dNxtU/sNQ42sHOFTxTM6/sS5fJNnEXspFP+RExp0ulMNARl+Fdt3wE9Va7rKXSvZjnDvYPh0t+q4yTGWC7cs5JxMMkM/Bz92rh3Bd7Rocj2lY4S0st3jFJtiUAiPzndxNqCVXVGXAxB8VSkbU+y0W1YWFLsON47O0xy7kBPrAKVoxL5rOxoaxYSJ+zek2F93bvZPSB01u7DRfejeYVCcrWFpvEkoREL53OYQo68pdDZ/fpmo8gA/CNZJOZZzpuEF3o5uEJhHEFDEvtOTDTBN/bZEl3pWeHUFrd5TfmKol/hDEBSJFSavwHtpG15xH5AUmSs2q6f99NQG+XmPuitx3LtRRM2FAP8lxkzwzXxlYMRU5Xo9sd6G3c2T+XRVylglEvuKs0vXeFLanHVjAYQs+k/0XUg3bkiEas0SrJky50O9qbm+Hra6pRNnDpd5q5yVryiFIeazf0PdICVPZYLfD6G2VSqnaXh67e27I+I8mNw34sN5/UeE69qFZs7NnSa1o69X1hI2JCTzumBckMGiy/V1jrgtRdx0Yk6709a0ohif3Un31pBJi2+5vB7SG0xdWgXevBov6mXunAfs08fvTVxJMT4wFwYJKoZIhvcNAQkUMQoeCABzAGkAZwBuMCMGCSqGSIb3DQEJFTEWBBT30Y2c9DlDJOloZti2gLN4BqL68jCCBUsGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBSaG2nVpGYo4ktGgMtzrxNfWqa0wgICBAAEggTIbwPtlmL2AMtW6zLD1+4Z7gkIsFflDU1KlELK9cPxJb6KPK0hGZseHPzgBVLJhRuc08O3WQdJfQ9+fNUedJOJ6EaLoFdY6/L0wexox7M0f+OHLGU6rjpYwnQhFQdpKfi+692FVfxYsdo4cqm2rl2jTSb6yxS/Uyd2EPZrmEdzUMbnaCbAr3mH/KPr19is4pnZVk90tuxbFZpziRAr9AD6NW5yLoIZ1AgP3Zktu+7J9S78rVp0GKzTop+Mda1A20Di4ZKn9zGLUVtZASslVBaT9dQlrb8hVUxGTTqwqDDVfxjqs4TFaolqwnZgLiH5rwK44q4mphJuV+1NxpqTpcmRiLGukwonXpe54leWUXZegAV65lh3QNmkcmvh4Fl/lSw4pkeQ8zmk4KWvHz+Dupbxc6OxP3DkU9Z1ol3iY/ax5fQec8Ffbl+OXmByzs+hM9NDsx65Re2sNUXdgNxYe7SNi2fOizUYQ4tTsexfv2moJYZkQwGfxPlvhm7VeikZ7Rlxy7nJ6iE2VjUKJte3dCmT+yxU6Hdk83vyXEQFT27NbKGnAkdBL9r+65oLj2zTNHXx3fDgPRIHSzpizZwarceMsovfb0wWzSX78NUCQLPtYEUOXdTcR/ATcSXj81MasxKKhCxgKDn+M67xhOO6Lp+VPzYMLHG0lAP4r4rScia4OQ/W82ukuaCErCQi1wzkRHPnQe3hryxjFAAnsCcHPH2d/4N5BD4oxV+qStZj0GTIBpmG+xvlJVNdmU9v9qX76BQCJltZ/qfxeFNaYVFxIdNyaRn8OK9HcLOJ9Rt4XNXu6BUeQjB3CXptZCgiO1VWVpkkkgyfEY5i7symkYa4Ratc3xDyLdgSpb3XIS+joaWhJ3fBAOsCTOZg4fbgBL0DbqiZ+CuBDiZMazZ/Dtu7Z2j/BIy8hThn9FBXg+UiVcL8ahDbs4fHTmr67Se9f4J+sppQBYGFI+uy08gWYS58RuCpyuv7SJbCT0TzidVhF54dRhsH/NsMMS/iA521lvoTSXBWygMH+b7SX2vRXmYdTsYNJCN3eqAdbdiBsrbdN3K8hXClzWPbQxAnRiv+C9Uao/grTjPwkVn1yo6ZmsX2DZgcoRcMnZDPoYbgBY7wprM2231gpQwUanPaZts2901Sjljk5VlDiA76CCya0UD4KFOlwjDEUvy329H15KzyGQAEuq2NUTm+t9nwl/wkqBar+4nQKaAsw730eUMS7vjRL8xR4Vibat+JyEl0NqTjJTht3zpkZJGDPe9g6k8UMI9tD4/i+SBA9t6NmN4b69k7ccwzSohb1D9fCM5L8itS24spxZC6ZWuUiwVl8uEqOfTCK1f+qFDCsNqhW2PmjhuyVmwBulsKmZ4EaJ9K2vlUlavVXVcYJYeo+FQvqjpBKk/2rmuvP1JMfcEDfhMwwnvc8+cnDwyF2yUASiTKs4LQACXVBID+UiTRsXQn8Ehtcax1R9be/L5ieEj1YM8xoUNHL0+9zJ/cz2xVLptjAT+H0HzZjS21bylwzpi34z8LoR9mr6wiHrjjSnv35HfsuoZlsQRz9eDU1wqmrNRyB/CRbpxOKPfS3/Iz0W8yNi7rNQSGovji7Oq3ga8Zfr9dlVDZCK0grgOSx1maw8UAMT4wFwYJKoZIhvcNAQkUMQoeCABhAHUAdABoMCMGCSqGSIb3DQEJFTEWBBR8jDd0NRd1Jy4NBY8mvr6QLOyc7zCCC7sGCSqGSIb3DQEHBqCCC6wwgguoAgEAMIILoQYJKoZIhvcNAQcBMCgGCiqGSIb3DQEMAQYwGgQUM4okkGUcsM2Ra1bDvAvfYgJ/uX8CAgQAgIILaP7zBhm2wmmUCTNM5y6XgAi79qM1FZBctY0GqQziVb2fSX0cYMP7rUUuoVbywJ+ZXFbG194vjbEXhAWISMhNqv7bdYdlZgkFU/JRgN0PBOk5APykMtNxgDbHNeSVsGYW4Zzym2v2JSAqronJwso0TrU4JZPDT4SrZrNt8alyNo/uva5OIq6NAB942jg1/YY/FrbsE4x8UB4Jb3kKtGmisPHLjpLS2akOC0wlb8CDxgV8+jQPVRtY5BVL1G7qlJWmy8ohkhsM5EqW8xX8QyW1vQXTfTnEeLi/NV+H9g0ODbvmG9MwKZVuN4Ofb1wZ476K6nQm/XhtUfSbTaAoGu4FR6Pt+qNvZq4CkzPQQnWHYDynGTl+LumAp3978q2Ukm5j0FUtfGJYE3T3wqFgGfyer2cOm65ti2/FohWTafD9wy5ecuq1qiXCySQfiCLj3o0eLKK8qaLu2OMUWzZ2Ntw9w/GnXa4nbuaOAUGBSOfSO28j808ucxQuFWIY7yRxGoGbu4aLIIMbinu7OUpElacOMZ1kiDfCjmXKmfD8uodpEXN6AnJAQVkOMOSOjS9mf83xG+3ZjwcKoMikM9GjQj+/x5xugSpX7FGBeHJ2b5Zzeg59uYoyh+KCXev0FQEvdge+/RG2dBrwpAdMx8+dMaev8Xxt2+phlLIhdJhNGlHW18tmqVegIr9sTLdyy4uOcG1ivZ+lr1q/n8GkehdHN08Z3rJmGY/Es+D7CEcSvzRSMS98d9za1jT4Xk5gZTDHGNHCo0KqhPdVJ4LiBUv11UTe9rOXOV2l3CmTMSAh5Fy2sblCSkFq2+KJsWNgcSOmYNrXM71TihqqdvltK3xMQugXzSgKD5riaocmQ1LIwmFaWx1vZUQy+XjQkvtHf5jROWrWG3AFHCRq7W6gcvZHlWc8U5r9TMrc44+ZHUcb0cCNHO1TB2EYEyZDP+6FTOSdtZORDAq1oaOPQxT9/A/XmTVsHgHR1jUXvwZK4tXaEKFQlZKHWdC3OtjSpcMZ8qMKCyzhXJB/Za0hKEjqNEAUZixfn21HoX7tEyImsAIzYRGlxSSwFEazE9BBXVB+t0DKsBu+jNL17dOEDUs43rY43nZbbgPszH+IAnjjD8Qgx9DQ7Lnt6dD+spIPCvXXl+vlmcKvTDhSEJ1KM5tX+YhgFwI/eBAO0s8qE+GVr+Yba15gvdy27nWeo1GZlnlPx1FIWacbqBIbDJf32zFV3P6xSeYa5k3eo2va31KzD0LzqISK+ygVsjVr6GD0ZRU3GIEoUeem2G5r94UhLlC91nST7R44ZFXiiTj8e+gI7n+sdCIrZIU1SxE7Hft7aP38JLByqnVtwmJ/DCIig8eGm4rNlWYXPyc2nJ9F0yQOfFJLcBOZjhz8uW396IpgauCRYe9uW+2m71XBPb5/YePkdcvhbXEvhJR7shP5mktF+ycdJV1AJSM8OotlnVEZfqGwB4Yr1QnbE+FpQ7IA0tIi/awIEaVYdT9eD7z4mYyKBH3Gdv/+P0w3Tv7+tsX6hsabQqChTKM85mIZTp3YyjNNq8hJOa03Q9/F1uFTzrQMfnH2mgm1q34j0LiopjQVwWp/nSVSfkxx1kzAALEMYuROlCxeECX06xujZMH86bKEedA7aaUsNsSw8tQUTrquacdBBVDCYc+5quOoOe7zbCsieiexgxXmjCJiUh9EEQORqgiKYnZrp1Pv/pERNn5lM5AaOcljQ8msGxvzzxtdRKmaDjXvnPAJBs7qigITqmigCv8+27PxsGXusXSRLsTZF9qKvDjEHE1B31LCqRN1xWM9XmvYeSWy+wteJRNpBTflAOw7dVyXbT9CYuBMYOyifwYsUS672duwyNpbLq+0UrSbK+OQ+vpx5wqHf6NrWgtpcw99QcwZlRhUKZrjJloML80+5Rnbamh9er7XL3nyovJ7sn9GEWYBSkXTg3bfsKEnAqnplfYchPZkDqFehOAQFicx8VqUFuKqxcP2FYrx/D1VAMZMFl/KkrM4Ua8zLLUnWtaAd5zqb+9GLZi6IPa0fqRXsymNl+w8BwBBXWxE7HfJTZLJe4BAQxFtPsVtERLOuQ7mz9yaIhvt/Alp2zsABVRJvBQpvhzD6Z84JP8VytE8hJyy2DBYe3GBfUBSSqGxR7iB1SxLAapSX8TZvN0jjdNYzUqxk1YMhjcCjnJ3cNe70+Y4da6pbJsNj/ma3t+6us7+aw3uA/PmJUOvuLvu7PBM0djM+iK3rE0JezhbmhbjaerZ1aY3Bp3NU2x2AZfEuAKBye59skRokF5rgGGGzlp7Ajx8I/fNHnAvMp897jv15zj+EX6Uov3B29vD5Fit2hRTo1j/gTCCi6VWBAg41oueBxwk5onZ/HYrqJuwk7RITJ532qo98ly0jycgYrkUIYbg8VLB5s3IpBlHG0NdMUZ11Xz22mwEoVTJNLQYLXOv3WhwbqX8F+JyEHIPGQjaAUJtp6gk2U4CakQWAfumatU16tWt4JN8Ty0fASBlMjIR17f9lFCI9Ijy4fT1R3z9meDitU6ST2yVIvk/jFAAWPj4xHTV/BbO0ilzTU1hqrkjDf83tHzCnRzzFcAbMdtdE5kYo17MMBdnlFakJroKcqMhaPyz0rOtL8hjbypccXMbj/ULYOM2FbvMxBSoinRSCGtijj4YN2eP2pTO5Mm1TBe1IeBEqYrmx7oKSo+BauQDFDttvW3eOsN8qSIFUYH1oBykOfIchLd+L67hy4ZEr5eY3INNNzR+MbwAtH1D+yC1UfNwLrrwZNHjorRhgmRk3/Fml/1Si/4ciWkYDCBdnG/ZLTPTBmqTBCJCimjpziLGIEGlJDUx+JDKpbqm2PpLXAJSPzkJt3A8RtXt6P/nhiyEXHGLAAQC/Ug/0xuSwzyC8K2o7xTq0fl4kde1uT/j9pLQXX10ONRr0LzYtq1W2UfGL8JLot6Pu2rFNPS2PhJceePnmm52jasvQD+BuZVKTWtZAGaEyzGZxoAzShm4h3BX9i6QgzX6U3GkzfrbJjTo6SlxoSIwX76+Q70+gbLPcHvnY7bijCm8VXTrdh5q5bNvRGqxmyjSsk7nB374e0dP7W9WSYaW24HQ/C8zy5/+SqmHl9GEAw1ZnLK2D95rav/9zwPqZn2X4hwMxHvVWa4sRcJ2DDou4WM/0U5psck+EFcGgT/SQCi/oGBIsI/QOPhEUoCa5qA7/WiJ9LT0Q5G1bC7A/QW3Qs5nB0CIe/q5Zic2E9brGMHOdpEDG5phzsAEikO304h7xYHOmXZ7pgynFONmANlfPMHGGKjyLcHl2gzMmtIP8e3YHVXlkSfTDjKL7a2YOH/qbi2Dd1Vn5oYidhhRFpdBAEyXpevey6bRTCc7CK6I6pdMf9GunGJ1eUIey9KtyyrDoWlGPJUvNLN4jT53cqpnmAbX3CUoE/EQ2bu4O5+1AGXuPuH7mfbgf4S2Eb5mNjfBeMR1UkL6yqnWkybl9fKkAG3ofn0+8AwnLh8TzL129555/lE+9jOVzh2zxz+nsDpdvjB4e9YIfhCZ1qY2e3Yc6ofkH6ZMZx2/yDSRWqePH8ncI89u27i9PyeWsnu3WS2DC05MYuGvbuaa6PXhYh49GTpJHWbO7bYNnk7IfUDm5PLQ0CQWM7dHcJY/NlaqmXpjNOARI1DOwlyH5wvFuUShN0szBgVcm2gcJ1Pkl+nlFRespcirC4aORHnXPY5zbUQdkPQVFxwxakGHznQtCgq3fHCDONtgVm/n8gsDbgAQIcIlpPkPjV8OiwzTTuQmn+La5EEl0JQ0ZmtvC3l93sUxvev39FSe0BbqWRhPG3JJPSLCddPN/bIF+eaYRsL2xcGyQHyZ92ZTP/4OKb9tzMwHqp9dIBuOKZa7TlgMIAzV1+f1YS7stq31jqYwPTAhMAkGBSsOAwIaBQAEFIL4ft7A4v6F7xHtL5968evRiWjOBBQsjehM4e7vvVXPa3SEHJ3pPWItIAICBAA='
               };

               var f = function(box) {

                 var cipherFactory =
                     new box.symmetric.cipher.factory.SymmetricCipherFactory();
                 var pbkdfGeneratorfactory =
                     new box.primitives.derivation.factory
                         .SecretKeyGeneratorFactory();
                 var converters = new box.commons.utils.Converters();

                 var aesCipher = cipherFactory.getCryptoSymmetricCipher();

                 var cryptoPbkdfSecretKeyGenerator =
                     pbkdfGeneratorfactory.createPBKDF();
                 var saltB64 = 'CpHq+38iqF3Ls7j97qBQ2A9Diy8=';
                 var passwordB64 = cryptoPbkdfSecretKeyGenerator.generateSecret(
                     passwordBase64, saltB64);

                 var aliasAndSecretKey =
                     'alias1' + converters.base64Decode(secretKeyB64);
                 var aliasAndSecretKeyB64 =
                     converters.base64Encode(aliasAndSecretKey);

                 data.secrets.alias1 =
                     aesCipher.encrypt(passwordB64, aliasAndSecretKeyB64);
               };

               f.policies = {
                 primitives: {
                   derivation: {
                     pbkdf: {
                       provider:
                           Config.primitives.derivation.pbkdf.provider.FORGE,
                       hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                       saltLengthBytes: 20,
                       keyLengthBytes: 32,
                       iterations:
                           Config.primitives.derivation.pbkdf.iterations.I_1
                     }
                   }
                 },
                 symmetric: {
                   cipher: {
                     provider: Config.symmetric.cipher.provider.FORGE,
                     algorithm: {
                       AES128_GCM: {
                         name:
                             Config.symmetric.cipher.algorithm.AES128_GCM.name,
                         keyLengthBytes: 32,
                         tagLengthBytes: Config.symmetric.cipher.algorithm
                                             .AES128_GCM.tagLengthBytes
                       }
                     },
                     initializationVectorLengthBytes:
                         Config.symmetric.cipher.initializationVectorLengthBytes
                             .IV_12,
                     secureRandom: {
                       provider:
                           Config.symmetric.cipher.secureRandom.provider.SCYTL
                     }
                   }
                 }
               };

               cryptolib(
                   'symmetric.cipher', 'commons.utils', 'primitives.derivation',
                   f);

               return data;
             };

             var data = prepareKeyStore();
             var sksReader = new box.stores.sks.SksReader(data);
             var retrievedSecretKey =
                 sksReader.getSecretKey('alias1', passwordBase64);

             expect(retrievedSecretKey).toEqual(secretKeyB64);
           };

           f_main.policies = {
             symmetric: {
               secretkey: {
                 encryption: {lengthBytes: 32},
                 mac: {
                   lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
                 },
                 secureRandom: {
                   provider:
                       Config.symmetric.secretkey.secureRandom.provider.SCYTL
                 }
               },
               cipher: {
                 provider: Config.symmetric.cipher.provider.FORGE,
                 algorithm: {
                   AES128_GCM: {
                     name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                     keyLengthBytes: 32,
                     tagLengthBytes: Config.symmetric.cipher.algorithm
                                         .AES128_GCM.tagLengthBytes
                   }
                 },
                 initializationVectorLengthBytes:
                     Config.symmetric.cipher.initializationVectorLengthBytes
                         .IV_12,
                 secureRandom: {
                   provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
                 }
               }
             },
             primitives: {
               derivation: {
                 pbkdf: {
                   provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                   hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                   saltLengthBytes: 20,
                   keyLengthBytes:
                       Config.primitives.derivation.pbkdf.keyLengthBytes.KL_32,
                   iterations: Config.primitives.derivation.pbkdf.iterations.I_1
                 }
               }
             }
           };

           cryptolib(
               'stores.sks', 'commons.utils', 'symmetric.secretkey', f_main);
         });

      it('throws an error if you try to retrieve a secret key providing an alias not defined in the keyStore',
         function() {
           cryptolib('stores.sks', function(box) {
             var sksReader = new box.stores.sks.SksReader(data);

             var thrownError = false;
             try {
               sksReader.getSecretKey('alias2');
             } catch (error) {
               thrownError = true;
             }

             expect(thrownError).toBeTruthy();
           });
         });

      it('retrieves a secret key and a private key from a real .sks key store',
         function() {

           var f_main = function(box) {
             var data = {
               'salt': 'xwZO9BDmu+rREWpt3lGWsWL8Pp9lHYf+5upIllTmWKw=',
               'secrets': {
                 'symmetric':
                     '4ok9VK1L+DNRYACKC2CpyEFaIr8HDAA8L4UFCO/luSjO/5O8XFPeYMx03PUEHZ3kVy/+3vM='
               },
               'store':
                   'MIIbKQIBAzCCGuMGCSqGSIb3DQEHAaCCGtQEghrQMIIazDCCCrEGCSqGSIb3DQEHAaCCCqIEggqeMIIKmjCCBUkGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBT/H16QimzbCtIynQNTqFntOVZKOwICBAAEggTIMxv+kiaAseUg7dMpfI7UDN5VtnqIUKUkcDq5YcOIPlvOqteeHN6mp8LU+6Ur6k3mQNDcCIu2FMLXaLatmsvhSUHrzCf3h+10vjAaE8CenfZFmL14P/OJZDb6aa1YfTcdzlIBLYg4MO0ET4UGIpCODjz007QFTWgatKKOcBQifepjw8U5AhPWywUmamL+oXfBQqynOsrvx1FzML5ElnCicle7y0Hwii+p56R4gzUI6PLwcvcEZjbq0fyagNyTMUUH2OD26Uk2KoWe6KVrdsdY7lWj3pX7gE1LeVFUKkgizfekKh0iHka7R3dkFaOIcQRugTyLkRkUN3O4yNqJcf50LyuOqtWb0sBvagy0U1Nz2PK1QD/UOMO4eOv7hZFaHfw9u93tS4wD9aC0eUU+sjyC76uI4sPrnGgt52WKj9Sfx4NPEV5ZnCq0+S05jpdTbV0Zv1jq+gLHlFaAOD6dzw67G+1L2TzrEY5selBjkjGbOiblV+xbBkgpYUocI+bn3SXKRJTcYGy+oGAvwg9Ut+tbIQ9r/wXO1RonjwDMKFx4ankiK8RKc0xAPmABNslrRqkUkz8N1CHdi4cau00+Db2JJg0w2iEFNctPZt0Kg7/PMT112yvU7qOZm7+NIjVAstE4DCBAFFAMqsTg7aUIC+ZaxTfQN4q86J5a8uD81fqmL2SO1W7PYCog9rFjHh5sYPLmiDUJ2exOTy9M4J/DSgju5loOr11bv0D4WHQsbdYA0zuQkNx9j72JNMbgFojHttEIFumz8lZXkyC615CuiQPszW0CCdY99re5nZLBnQF0b/pYnSc8r7xyW9AIOD9GliqIaqlktDU8Q89ERFKQRYAXdQt+q1RZYXAkgquYK2I7loov7bbWct8Pb4i/Ie4YL6FBUwgDstR6d0VKtLdzjktIN43uIPq8MAp3CL0sEb0Ml1aJLpU9ob8t44kCB8DK1yU7E2j5rIFYbpGb75HqhQmnBL0y2JrHbLJtfikoowzrStIriPvTkvHNoRhwGkKk35bm1x1u8qaeQDwFKTDmMrtX5683Nm3tefyeY+8F1cUTVMzGd1dzgPHHqvDQkul9Z4yUfMrNxpYM2/MVdF+R5/W5YgbM5CyP6RDWfrpp2o0axEKPpz7FUX+AvttPaW+9ws10vZARr6NZB+3Kuv6up5pxynH4EpeNuPED38/ZYWW2bdYil8LhKo9JVH3ue3Zbhfxv0DBdMqOIulEbuBKD3xda/jh5shheUpvcDPCJcqkxKlbBhb6PIFDapPua/epbrHPIVCgyl2j2909egdKLkHsIJt4FpF6wKGNp2fhqYT47SA301+BxBf0BHrAbG8abtGUBBTbPZnoGLLag1Tj5qVocckssyDvKhRJSi/DNv89Hhml0rlikIAA3ttzCrHJaTe/S3t+0kADlI1P55GAm7UjAgRu5wAgbvBQoFijhktLRQPkglVDvNqhqQi1aiSeh1CqYXN8HCTocYfpOACzd844xNTFGM5ke3XHWDBvehAtuC/Duuln40vT03swtk6jE+NEQ1nr7GunjsNIhNd7Mm6w3lazokhfRNrMyXazgSta5Rlz/CgL7P88hNOEtnUnl+Kwfo5NPtDpbvlL/ESWCbQX+BfRmSEnxsV6DMTwwFwYJKoZIhvcNAQkUMQoeCABzAGkAZwBuMCEGCSqGSIb3DQEJFTEUBBJUaW1lIDE0OTU1NDgxNDY1MTgwggVJBgsqhkiG9w0BDAoBAqCCBPowggT2MCgGCiqGSIb3DQEMAQMwGgQU/vRAWLnK2AQIFLXF+mzvfANOVIgCAgQABIIEyCIYdB/X0xqVxkjsuDgs3DWY3hTgUIaeR5/87EE7vKPuHLBf551XjP4UTuwIb5Ijwt26aLfcM6TTPtGQwA/u0avunfqRcjUPQf9h2JJAQw5Te5i/V69cuyH4NNJCkpPqI4D5tEju1+XJ6AzHuJNtvgxojF2Q+1JYZsZyIu5haJpdQsjA4iOdZ42K6QCPIpY2XmCD9+uh2otC3zkAZs7Ve92/vhjqOo366rLb3xYfzR+K3uy4RfeUx97IR1TZTAuBh14Ua8v/P1l9MK4WnIUc7oUXFvz7Sz0spYUzguW6U/SAtx0FYnudmZnDa5F3GidMkvMmelLtvcuWeOwvpDi/o1olUr+YMUPnlkEYeDUzFkEjmaHfv3ARDXkT8h8gHU7Nx+gtLygSuCNFkAOl1XZGyK3B6tDn5ZTX6eNNC/vhptXbxpAQK3WZVc/DeKBg3tJ+WLetI4X69cZzzEzowhYyMSveOsxvpQL2HhLxrKyjN+EMip7o7fxGSqYwiUWXXv7PYYnCLxchqJLmljdgIlaidwXfouTl15Rc843ZI8/oH7Ic7js6DaftCkDuicVoBn845GbiTcI/IV3zciJ53m3MIHC5T67rHD/AZgOpCeS4X/AOoy+ADSINvWkzs9AzE+2ciXhKPMc8lib/LN1sQXJp/miKSXtJXNcptzp4cI+1Tfy7sF6fRPbMDdb7C7NitI/eKmFWmD44V2kS3uhrf9AEYVVvXhWlAu32QyBBbP/XGEZbeaBa6BV0maZZaveBXQXOe/EB7bBxroUJ9Wy6Cko8RxTCPsEVAAvnMfuuSBLgQ9ECfQHpqLwYjWQ+6Cr76v7KhVcVVL9DJt80sMGVdZsR/MqX2g/6kllVzhFWqpeteJjmeW0HM8iYjvroskiujoMPfj3JPWjI9P/fuv7haS/JN+DNqCUu6rVn2stPlDYO7ZGbm7ISpKaVJoBvs9cZuyBbnkzNj1o4WAmA7ex1zgXZgz2JgGZVrCOgYPHD6Dq7W4TkzjIwIb/Q3u+QBmLy09E6vi5KzV7geREXBR9DUi/xedi8nmyd2zNWzZ2nYfMCo/PEhWJRNnJrwcVTvQUXGS1CnCa5BN4D96QUyrQIiKvllt4XtAYGLF65Uz/6B4cf2DK0VY0DBRyHhGH5IFGXnqetEvJgQzZYERGpbu7IrK76ul6DjjbunEntYkLNobiMFYKe9XPVviADsQTCMBgkyesYxk83D7/fK2+WbtnQg9IgiFqSMYIhXLaAkRk4Zzdx5+05imxXx1aYDnLechch0/EZbqJKHplEmDGX6CtS5WApKnEZ7nAoHUHAwE+UlrxgEH1MEZWvCSHDfkMUOIV7b6OVzWQbgbusJOkA7/zr9Pd51gFnSq3ItJjhxox4NmuW05mlYHpKcnUoEMG7Qt6q8G0kHI3ljZ9pJYZyrtQYDw9G+lWFU1lIuu4xINR2fgJ9DT4PFaYHGxJeF+/uISWHdO0LJfxY/w7DO42h3nforThOkQkO6rOb41U43T3PKefwlTQkvFD3aoBKmh+EfkYj9vgX3Nzj3CXkVU6FTNp4yYRHYr33QGEzohiidSjXuwRylkM3RGZieD7koYTM4a5E2VmoKdG4ERJcCEr9zNyA0aZGABBkMBb/jcidIDE8MBcGCSqGSIb3DQEJFDEKHggAYQB1AHQAaDAhBgkqhkiG9w0BCRUxFAQSVGltZSAxNDk1NTQ4MTQ2NjkxMIIQEwYJKoZIhvcNAQcGoIIQBDCCEAACAQAwgg/5BgkqhkiG9w0BBwEwKAYKKoZIhvcNAQwBBjAaBBTa3LVg7ocRK6x1T0s+WE64oW9f1wICBACAgg/AWRCwohy9S5+teMpChvQ9BurWKEEp8UP4tTxk6pmOuO4TExInXRWWsdSt4jul2OF789ICKWCI5+6as2Uf5YFVdZeQlAGQHU7q0giAJ+wA9yA2GWAHOFsK0CSmlyO5mcR0eFS0clNVkZRvQC4WTrHSlZCr7gTrqF5efZn3j0UMVKw3xmyquR8PKzm0sshYEjjWnae7dOaRXLnKUqZ6gOaOUxLhTTs32kKkVxpx3FBkrPA/YePeKpFFU4C9Pi4DMzkRm4KERr9t3WVRhkkb/+3DmNSdr8J9I7REWzIHDDw4qEn2gcrJKPVIiA7kcCGUxDPaZhjQo0933UD4RZIeXDfbKp08bf9VB02HYdcUqKmviKHdrbp1hhKc9ZWyZxVuDqUDSY3qxmiBbt9HnNAVC7kXp9UutZaQMHA/hDUBPGn8667n5uiaPWZjSQTWillkq/4W54WdoWjokpj8mFt2b3fa2y5hhOb993Vh8yQOB7PqbpAXV9y7q8P1vPAypcS7tmOLXvfNuOPrIMdkOK+F7ASGfi59ofB9Q7E15QJAiUl2cl646JYt9cjPh0mBVWJ7UVrrrBcsVWUpK93BCOwNjzO6NI1TB6Z051q3qKkKeWFa9XQR6xZD3idcYjr4IISV2y5jJ7qO9xpCSak/SzlBSDUlwe7KX2ikwbs/6k5Any2ZZnpKsX9Kf1CrdivUFmte0acqVOugjIQIdiS6FqHgGb6+7sYL3VkxKjSsj6QZ1cQ148SCuYPgig5xFCdG/Ms8cOXld38tcbJBXuyzsZ359MEdYUF5gz8E3rAgNDlpttiykQ04G0Vb1c7wKZqYqUXXhPlnZ9K9H745lgO4JzhiFgIsbqNd5++7I4Xpzp66Sqw8FfY+kwtgFcpO6+/GA+JCbMIs3HloCtvctKZQrdz78sYdre0vkhYdQVQ7YMuSe0NdE2LPHJ5JzGHABDKDG15bPo8enq4S6g69LBkZvtY8Viy8/myxyTLpaTF0BUZQqvEpmiBtIzlX3zoZ8X+pUaUhixK560nhPzBVRjZS6iRjkW1WvNZFHKFRyTQGVNVkcMiaoFhWuIXCiJ/x7rbEM6rFax9WL6/D35c52Z+hbxfMT70iX0OgGUl2l83ZTK20Kx4nFmZr7buL8bipCTyyDzJSfTOV86JtaeHIEvX9tWN3P5OYW8vyuIRQBezwhnC1BpZjJpUdsPMeFqxEB5ITNaY5WLzKMK6GpJEZndA/ypVFMlVHjyskurz9DzJkn07bDrLTlVmhNhuxBZ0L53q5QrDr3EFuno4xqYdB3AECz2TXqMpFUciyY4KWa4lKYFOmvXIUyTPlGNr4GuIk0rS9g4Cm/eigRDXa/rI/uwwVsto7uQztxKrwJikNRTVu7vMYZQwiygwhQ6F3FJM5OCtLymNmop1vd0jPbaGvAaRMxmP623GjSobu2MywLMZoRBaJYb3lalOonGfkWZzZEcFz5NmYfcMR2CZl3rFHT/R+OTstktz26qtoz5BDL/uc1pFuJshV2px5XtOL+tlHTbEb9/Aoj1+4tHtLlREBuk3blRJ+DionEpCz6zPAsRXvWj1YqMcBsd9ZMvWX+RtDh4SnWnb+e7DVqIGSTIaA6dR6bhCxZaHHqLeFZO3RL9dMjaufuQ1lVq9G7i6kFSOaYPHMBxhcqgh3hc3I8YQ9kiOPUYRUSmLhzL6PwWwDNT+dE7+t/U0xpDn/cwUgNSMdd2R7ZqB2ZO3vXOUGszZmmtoiOojziQUZr7N2Zkn5e2WPn6dQSLuzVye1nBxLP51CZZ6OqPxqI60HbIfcyGGep2dXowrThQPUaHHLRglGXpbf+JG6QlqVeHQckYtM0GpI6hX0Xf9vCdBQ4ctInflJvYHWaEgQSBBoe5JD7jRAVpuJgUTikAWfF6EzduDdj30SuNHiA+0iGaIHmkrFf9fECaxmAOx+Mb01RXzrdaPLa8bLK4hfe598M8dI8ungYfq6dbQSThR/o08XSfR1e9l8ZP6dpkCnAf7wl0Lw1Xh2cdzIcLbe0l7ZQVqUxu7E9hiAWbaV8aKw7xTWEOVFASIIZ7YlJoclRi1S49SFxYq8ajoRQ3hjaq6dQy3DplncnxL9M4jHeAW1fPJQuFhznWSon/4kJj6fN5c4QugKrVLiuxMoJW6a8Bqa7CNY+p7BIrTYgSc3x4JDRNRhnb1gDlnsgvi9M8jjkj4gF5H4A8rf+RNMVBZTBmrtf0yXmmRFQVD7v0srJ0nFdKxZBwYC33HoMrmxKlI/6iy6pqsBTWVNJcqWjfby0dnj+0Erv1PhTsVRC8siZwjVxcoHpWYnJGZeCPioVHyuQ6Q4gj7Ahgv2b7GBjhAUV0JFjTWrP3QtAKhawrCDrSY6gkfM1imZwMKrJ8sKU3yGLvRxDDGisrK98Byhg3YonlGuCJcixmFPfTwiGxraaffVDF5VsL4fEMYi3hkg4YXZrwsDIuoyIm/yiffvvhdX2DPxfv2Q0xxRMfwwqyXFpD7mj5f2KouNx6iC4QHSCIRoXEOU9uaRqgXoh2VWErSTLAZnDEf12+7F2gqF/nvgHpQvu68wUuCAC5v9WBxuQksxGxKYfv9mT7wN51n7SqsSrXuyS/6NVPB8SE61I5eUCA2erikpHx4Vnl7DAiH/Ei7HqECkpMS9EO813H++k3KgFzZCvxNtoBSQxcvLzEClVc6cZ/oW/TNxVNJQL+UcDyo5pInEkkih3QUOAC4qIWlpKi3HdJcu8YfiCF6rXBJwiiuapz0z1c7rOqe8tgRpvqxruOp05k9+OTgCuFF40VL4wVHYW240S7RPVRFnGk6a4QwxZl9WwJmyUD5n8BOrnRBMhcZbfaecddhhXemRo0NFNC7wmZql0xDdt0XUZfVNTa2o0fBLJ1rE8o4krNN60HosqVDI+NgzltIZEqp6fnjf06P5omN6zrAzwAPYMbNtZviSxIqxw3MKCZVu3QRXEbtdxysXtjprcBW6iNYkY37opJZbMCfZfm/SoLUO1Foekwp4jPze310gweaJpwtl4BT4E5MvmhZui9Y9FIyYIBFMyvA4iKWkO8OrZb7cIkp4xdmpVL7WuC1+FeJTlxFmfTAfK9DU7MbPExaKVjT8+4SSOqD6rO2g+R9NdoAQnesXr/HW/PE4ty239H9Z83ZCBpPlYonxjqLEcDzKVIiUXFT/LkVqNQKXvd7cr56lbnq+b15hPMc1+W869mx/rX1mytVosySQyUQVWY8bv4fFhaxRniQXGivCOzt5K0BJRjOsJpTOPIM5HCcJOqI+MlzlIzV5K2QJirNe+uB1DhRKBPBtxa+yqXHMYHBz8Ky28BEUAERpLg0g+vcOy8wswWFhbyPG/LvxELlue+g/Huw4OYROjS9nug+zFSqPJc3re7e2eXGoubDn6D3XYzyoMNWRcaLDnGEgP/wf4ydul5n1JAvwcfEk6WLF3mlHDMtKrFodqFVeudEhVrzs7M1sIT9myepHIPcfMPSa4Fq6AEvIQON60Afqf7wcj3ap1+sxdBRuQ3qDoUsiCESGt74XAmIKLP85ZOAt0231wJzFHv9Dl64yVboCJ4wZPViYkDOELSJsv/QzmaV0C6VaJximprWNMpJCWW+q3Ff2rDqpP9CyfGT6ybNIQr5Ab9I1m+VXBEOz017Zb4NAYj2BvpUuehrrna8mvo5Qwqamm2cJuGvqrjCFG4ovT0h2hmFW1pE8e31da7mGgPAXpXrYXHx6VKdJvNHS1m0/PrrojbfYQIvuyAuYJRz3ywKAeeAUEudzOR6PaiD394E2Z35imMgmhRPosULbgF2RPChRBvVxPTK7daId7TuScy/Nt1Gr2tRV0WVOnbXIXw/w5ZXu3Thu8W6GQam6elA2Dyb34s14QGEPsQWltRe/pdJHXBUvlpxHduSoGrt3jAMaBExbXsYIQx3vk9IJD6yl4hBtW3Rt643LWHdi9Db5+GLl3LcCHwOA4T52NHdm/v2M2jVeUaKTGaKsbu3zt6ENcyFzKRm4iQqcivDVvmaHOfVdHrAYtwn+yjnf//xsjTU1Dc2S/df1wr0+0jDs4SW458tG24b4cOxdTujwgatDpuOEb7s7P4dMQstZH0mNDPbIgLwo2QDPhFXKZH72R84v2/TMK7gZp7T6RMCT10C7Um6S3V/ccGEeU2CRIPcEb9kR4AxGrgjU2VUei7gKYzmqkg+OfBl1z95VUGjkycJmM53JMeGP0s+SkRQPWsOSDXAXwZiMnILef7heE3mh4+QCSwTxzD0RV7tpTAxazhftkJ3/cfXZKfV94rf1dAe4vxB+LBrHsPdOw9kWKhRjq5evLpRDw9ZnBhBcikISUO6kMmjmLU7+yw8VXCovGYqzJD8AyhVxPehaVMQxCpTTNNLScKd05rPywAHPYvxVzccL/5CiJR2SrbHBmcsIRE8+odT8TKCdXD5tv4a+EpWv2a13UplihG7dtWq1r/u9L/o9HasgbJkL/r50OuS0XyvXnUD10EE8RplbDaHqR3m6AYzTo+lkIBo5skF/k7WNZt4k3S44cvgDmggbvtwM+l67mTJCCiUGC38gI0MHmdfF3cuUrzDrneDo3KIOY1wUBqynMpkUFU/jq8Bte+ls465zF+uQM6jRv0iZj1CZG5Ucxadkj5mh4AWCIA1EkO3jm8ux8B/N9LTNvRrUYgXIc222qRALIvP1tfUW0KgyBdiEScjIlDk6+IdUNI9gQlWvDXWJhm0RYbOLKdLujoohm33qqLG9h63B7uydEJhDz7wQF7k8iAKkzVsE+5kw5YvlJvnpXwiFV2etQ8MgksD+LIy6nowAvvXyy5Kfgl1JWx7Hs6jJY5wpjEfosA93jYlgCJzE9rV4VBSWIQ7sdKzgJKC9IwBec2eCNGPgaTS27lP+GrG5haIJcurAumQMNhXCSHTEe9fyyLGZk+ULSXo/mAjdQb+37f05kmfYy3al/8oH25dQinyVCWJcpIXi5zW7NfIAan3dfFHjUQVyrpm4d4xMuny3xK2890QBJ8j/FLzSTYbKrAJLicWDhLZA430x0Ws9I9R3S1Yq60aDN5pbtf9+rA21ST5H9yRS/12cBIN4MenPJ6LycQlgTGLF4RgUW6/k6PD5h5sGfYgUeRZmtGISySntuOBqVx9qjrSr5DL/KRIkLr4vSjumFa995k0t9lNh4Fh4TgDSdVnTnGd/7U8ZxWXDVEiGPXzIenQHvi6n+/AsOwYKe4wHbs3GNyoytvvupzwF6h9SSD6kyLObzDhcXyw3F0BnIbhoOlcADB2MFEnHuFiiA2FjakYwTJ4gx9sXGdGpyldnnLprKyDmscU21EgK4+qO8pOQe2ThrknqgzRHgViOjcaTzJ1+OGYA2zEaQuTNMAC50EVZkGwBMkPcWe/AMD0wITAJBgUrDgMCGgUABBSB1iwNkYzcmk6DSmuVuT42y9I+KwQUBqnV3PqGKa2wYwazLM/OwCCc3P0CAgQA'
             };

             var sksReader = new box.stores.sks.SksReader(data);

             var passwordBase64 = 'UUtKTzZLT1ZCQkY0VzNaWVpPN0M2V1IzRzY=';

             var retrievedSecretKeyB64 =
                 sksReader.getSecretKey('symmetric', passwordBase64);

             expect(retrievedSecretKeyB64).toBeDefined();

             var converters = new box.commons.utils.Converters();

             var secretKey = converters.base64Decode(retrievedSecretKeyB64);

             expect(secretKey.length === 16).toBeTruthy();

             var privateKeyChain = sksReader.getPrivateKeyChain(passwordBase64);

             expect(privateKeyChain).toBeDefined();

             for (var key in privateKeyChain.map) {
               if (typeof key !== 'undefined') {
                 expect(privateKeyChain.map[key]).toBeDefined();
               }
             }
           };

           f_main.policies = {
             symmetric: {
               secretkey: {
                 encryption: {lengthBytes: 16},
                 mac: {
                   lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
                 },
                 secureRandom: {
                   provider:
                       Config.symmetric.secretkey.secureRandom.provider.SCYTL
                 }
               },
               cipher: {
                 provider: Config.symmetric.cipher.provider.FORGE,
                 algorithm: {
                   AES128_GCM: {
                     name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                     keyLengthBytes: 16,
                     tagLengthBytes: Config.symmetric.cipher.algorithm
                                         .AES128_GCM.tagLengthBytes
                   }
                 },
                 initializationVectorLengthBytes:
                     Config.symmetric.cipher.initializationVectorLengthBytes
                         .IV_12,
                 secureRandom: {
                   provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
                 }
               }
             },
             primitives: {
               derivation: {
                 pbkdf: {
                   provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                   hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                   saltLengthBytes: 32,
                   keyLengthBytes:
                       Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
                   iterations:
                       Config.primitives.derivation.pbkdf.iterations.I_32000
                 }
               }
             }
           };

           cryptolib('stores.sks', 'commons.utils', f_main);
         });

      it('retrieves an ElGamal private key from a real .sks key store JSON', function() {

        var f_main = function(box) {
          var data = {
            'salt': '7Q951QtBynbVfljodmtQmg351nq/kmxcHfCwdrTUdng=',
            'egPrivKeys': {
              'elgamalprivatekeyalias1':
                  'HA5aI6+JCB1/AIeb70bcMetz+1AuY9zTh4GJAS2Xr9A1EN6N5qj9pLCwXneozTyuOi8mp0sHg1bZfWKuehBH+8kjYDlKzexbqmXiDhf2jw7mX991x0KVpeQ7oJJMGIPAv+Ff3lGHZxZYEdAy72Nttt5HZWkX7jPGI9ZeqT8aQhB3mNuGMhFnfMFMdEFGr9AVa8ZeXfLVpuVkGoHVj8mkN9PHUy+NLQc3dyh1ggfHNrUlYD21SOEp+4GTNxVNjsMPiOhi7LTHabXfliq3KdVltpo2IAZ0uvlmIj2cxZC+qFXv1BNiypPYkPRYSCfthWjhkz8iUSJUek5ve5sewVAMFZc5cRXEesvP5AH7tRwmv+63Ht7ktv8W5SKCm+ycVmqx3Tzo4NEEsxDqwBilTLhbcokANljO3iyR3HgVIrvwEJGMm79AfH8RYe5rhCUbLZ/KF+XCVm3TZ2ux0zWgynZi7d/1rzqae9nM5vBfdK6YMmT+AuEoXxKDzvV/9cqlRNg+EWF+vpfIit203fSiSv8fF6WZ3TSSUQ/oR6I56i5PWO/MBT3iYz++iwSO1faKNG762p7OY7gnG0V/EICTVtXn2pg+p78beAISpNL/FNxntVKIdg2dAXwaRcSWb9V5/zrN8FTQgIKEMzFI/lWZUT4N0R+S08gdYfRIkxc3Olp2vXUV/rt5b7oc6jhHJo4Z37m3FSm7LFsrmVvzh5PGA0/gFfWbuZyPngjqoPBNJFtbFI4dqG+H8hOzOg9/KAx41qNPuUGWjuVatr0nE2OdWLxaOe8eQ4exykL4xsS/s2bkrmynXJTCVKm9mNudmMUfdWE7Y3PxWtx3TnQpHGqIb0VbQRJabTzwHyyYBTqAmxsLO20VoCagPDJscPJNcBvGgYyKcjzjjPo+aaAHSfHiqUdOluN5+sVNfy6ifUkIh5VBeoH6+s3Xhj0xPLLIpFIz8RA3y5lB7l19mFjw/T+pyq8dCWlx3xD8XgY9K/kWxQF8ERvZ2Qu/zHySV8ybXB3TKXJsmZNeNyFFCSZs6uvw+XhupaG+LuOO4+FWTD3PpCMkeGXGJa4zCsnkMu/Hgi4ZTVvOxO27BwZz08/x9y76dpQcE1nt0IB2KMNuwD82m9cO77meHBp/LgsMQmA9G/+5i/mW6oq9ZlxlsE4FoA7ElJR2X7V2DXMwDSQPSuOScUjZCfsd4hPHW7+Q/INzBmIbBqvlaE7yjJ6Rt23UbfKgjnNL8891KE9qgnlxAZ3M77YuQpMLKWTmYpQH3fcrBFv6QRV1aUaMhDL1Fjnzn6J/sbM4l0JysJrMigRmHy6I5PWJDQTeuzEDtFAyd9E7sOv5/rdIBHvtQlUBy+4PTPPoGv+idkIKKZ04yglwGTWxtkKsAvt1CyQExj/q7Wghf1xWLoW3/hpoYqRtSOSIgQUpFEsjW2SY1kR/5EdudS66gfFbsABrNheNHSddXUIfB1QlqYBdIarUq1Np8rlZ0XywLxrVlqUmnLI9oXQbkvkq1NhD9XlbEYu+ROZFXZADytu9K4KInkqAZXz9HB/QBncTRmPRS6onM+kbjKLImtvlEJ6EJt2I46nIUVM2d73MTPLNWaUNkuFq9u+oZ/Dn1ozgEoeqCJtHtMtl33FUu1Nsl6LtJ6bbKtNR6XlCgedbWtUX0Qli8RDw6sb/zrRZ9lBqNw/8Nfg5zn6CBdcqwRgVwNc48VJ+g6X35quwKFlUyLEFi4DvUN46n1FbE09PACVJu2uYZ8AI3bF1jpPZbMVcPcQEHI0WhUCg9+HyoQrOl8X7h2uwnxxaClGujvT1INXLWwIosqg/73WvC+kl78lOC4cczc6lzuKZoWLAxpOZ8gpRxWF+BT2mo63Ulr9aIhoeh9rTxecWyk2NVQ+tPm6WfMW7Uq21H5sLDFRKE1zkQJsMZiJDfqyY0osrCJsY/Y9wq/HrlEf/GTe9zrMEC1ZZheDqKO7zwdIs8iubKv6n+pSJSifjXjAsv29HDpF/vycVy7HzlouloQEJFZqpXxTawVcOpf6/pwxuO3CbTtpAJFdsI2hf84/bV5ZhW+vAvIlGkINMr+oshNA4FKvDr1nC6bXAU9swfZoGyJUuvVm6vLlAL49JZGWpkfcrOyaSFP15p4kPRfzv63hV57QPImx55QPfpbPUXQeW512TcZTsNX8UYNVNny2FSZXckQQaArkrLwqVmjKhNwrzEVc2WuAcbthcm8dsSpjsDSaJDmS2xVK7flmQTTou5xHJppWyUeeY/bo/vBE6oG6v5QFzwDcnyPXXy7cZnUvEzk+IVNZCqMQ4ZNjMW4mMiR+TJKCQ4ro5QEqCWFkSGtOrerZTkthacwqS0UTVorFxrZEPZwKGsMSmMEA44KhcxN2jgiyc/Aed3oyEKWbmtMcd8eaqiKEFCaU+v31/WL1vBi5zxEOCsJUPjbgcESlVzwSttE9RRVwoljMoZDoMDPEC3x9aOWz03bGn2Ky7XiLgPzAi2FN6EIWoPnqI1KylTdN51gSqv8P79CzhSQzROFpB4AunA6K7qPCM4UYvuPReKmkNJzON5FnMBJTUWT6PNISXdKdbFIgdRkF0d7rwF1a3H13fXG4NginqYtzNoVpPfBHlXAWU8dKOyIb0tvTPeqAfUZ0iXTXsTuVmqSuhaPANWPYOuFZwB84TvUhq+Azj4S1kJN1K0Fo3DwLaRYTcAkbZOEuNR33e8kRGsmfH7aU3JsKZJJBffY88CbY5fclNG7HUn59kI3L8Mxuvntb7KGed3VNqNxKaCwT7XJJcIo4XBl6xdzuwzlaSsdsd30Ci6uFip2ootnemCNJwtgoVLXFJe0pJ+JVOU1LMcFJdWTf6fuwDndzI+RWFwcoc30FxmEBJ2fF/vBvlSqgtRpISAeq/y7auPFn7+GKJZspuIDJqkXdOKxCm2ChmiqZ0ZPZRX6vU+zHxCN536eauE7IY1khuAOBZsxPweJg0RKm3+rB3fc4amlneTV/Pwc0CCLEFY1tVouaCe9GSzy1tzNCN8c5j5YVUWAjsgvXcmnzSAsQYvn1cjt+BPr3B+nnbx1oEJgi1dmF8NXm7qCg0KGTPqc5+kKpJh/egtsbxBNVeb12MmFPXzrockM6+ZlyOSdHxdFYUxlFaBnEs8MHTOXgoCpU93U/3uu7l+Kqm+i4ErpGliyYqguvMizxAHk8jckmzoMkcztvFFv18qlgG22Nz1BqwCobAc6DT7QQIlAQsQMY0d1QMlcxH35aTN5kS0/G2EDZ3ZG5AQEwQmczf4kNVWwk62XhTRcKcqdRRX/7h6OOMDc1nP5/GmcderDbaRFV28eFrzi4Mjjcjzpz889ttmHXPN7D8AvbTtHqRm4ujAFAvYbhU5hguY7x7BP+OOB01jvOM/F1U7Zzgg12FQoaMvcacJECaMR1Alz22htPaK0VIxCmxZd+q/T1xAJYPNwver4QbKJX6Pg==',
              'elgamalprivatekeyalias2':
                  'vezAhuUI9Rqb9SNp9QXGpdiiKjJCGjK3MWmQbSC+NV+4HYEKKuCP6N9RuKAprssqmGYDWUc3ecYD6JsRlW2PNnbAv0oK/aGGogh3gqrVDLqBqSet1hmnN3NImAmXscpioAAnfPd1RR4V2lv8+Lc3aUPKTvNwl1P3jATM3TViGzb0hxMhsCuUwFOX'
            },
            'store':
                'MFUCAQMwEQYJKoZIhvcNAQcBoAQEAjAAMD0wITAJBgUrDgMCGgUABBR+zoG7NsLvE8zqUFX7HXeP/yK07gQUQaC4BDlbcjaPJxp4KTcJh9wSzMMCAgQA'
          };
          var sksReader = new box.stores.sks.SksReader(data);

          var converters = new box.commons.utils.Converters();
          var passwordBase64 =
              converters.base64Encode('01234567890abcdefghijk');

          var retrievedElGamalPrivateKey = sksReader.getElGamalPrivateKey(
              'elgamalprivatekeyalias1', passwordBase64);

          expect(retrievedElGamalPrivateKey).toBeDefined();

          var group = retrievedElGamalPrivateKey.getGroup();

          var elGamalPrivKeyP =
              '25878792566670842099842137716422866466252991028815773139028451679515364679624923581358662655689289205766441980239548823737806954397019411202244121935752456749381769565031670387914863935577896116425654849306598185507995737892509839616944496073707445338806101425467388977937489020456783676102620561970644684015868766028080049372849872115052208214439472603355483095640041515460851475971118272125133224007949688443680429668091313474118875081620746919907567682398209044343652147328622866834600839878114285018818463110227111614032671442085465843940709084719667865761125514800243342061732684028802646193202210299179139410607';
          var elGamalPrivKeyQ =
              '12939396283335421049921068858211433233126495514407886569514225839757682339812461790679331327844644602883220990119774411868903477198509705601122060967876228374690884782515835193957431967788948058212827424653299092753997868946254919808472248036853722669403050712733694488968744510228391838051310280985322342007934383014040024686424936057526104107219736301677741547820020757730425737985559136062566612003974844221840214834045656737059437540810373459953783841199104522171826073664311433417300419939057142509409231555113555807016335721042732921970354542359833932880562757400121671030866342014401323096601105149589569705303';
          var elGamalPrivKeyG =
              '23337993065784550228812110720552652305178266477392633588884900695706615523553977368516877521940228584865573144621632575456086035440118913707895716109366641541746808409917179478292952139273396531060021729985473121368590574110220870149822495151519706210399569901298027813383104891697930149341258267962490850297875794622068418425473578455187344232698462829084010585324877420343904740081787639502967515631687068869545665294697583750184911025514712871193837246483893950501015755683415509019863976071649325968623617568219864744389709563087949389080252971419711636380986100047871404548371112472694814597772988558887480308242';
          var elGamalPrivKeyExponent =
              '4859259447320592668819568962669961246561051865212874540681243424367801394774393725734829250015941495215558771019728908939132002055237814853650010734496771955581048260699707147620967124841995608460226547395857304134089405473568889471419858106512603895297560717994246768950866491384823569118637582497519403351819609184883390558852906911976387433283350856334618005481106148226480670760580586739976814586223883428847209532777965666220529265035157816712732743895240103023779784827727286610206045924297284258748757461471125260095895029712193814187300268672295894546694634423997510695194818714918502470470193799175083856878';

          expect(group.getP().toString())
              .toEqual(
                  converters.base64ToBigInteger(elGamalPrivKeyP).toString());
          expect(group.getQ().toString())
              .toEqual(
                  converters.base64ToBigInteger(elGamalPrivKeyQ).toString());
          expect(group.getGenerator().getElementValue().toString())
              .toEqual(
                  converters.base64ToBigInteger(elGamalPrivKeyG).toString());

          var exponents = retrievedElGamalPrivateKey.getExponentsArray();
          expect(exponents.length).toBe(1);
          expect(exponents[0].getValue().toString())
              .toEqual(converters.base64ToBigInteger(elGamalPrivKeyExponent)
                           .toString());
        };

        f_main.policies = {
          symmetric: {
            secretkey: {
              encryption: {lengthBytes: 16},
              mac: {
                lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
              },
              secureRandom: {
                provider: Config.symmetric.secretkey.secureRandom.provider.SCYTL
              }
            },
            cipher: {
              provider: Config.symmetric.cipher.provider.FORGE,
              algorithm: {
                AES128_GCM: {
                  name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                  keyLengthBytes: 16,
                  tagLengthBytes: Config.symmetric.cipher.algorithm.AES128_GCM
                                      .tagLengthBytes
                }
              },
              initializationVectorLengthBytes:
                  Config.symmetric.cipher.initializationVectorLengthBytes.IV_12,
              secureRandom: {
                provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
              }
            }
          },
          primitives: {
            derivation: {
              pbkdf: {
                provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                saltLengthBytes: 32,
                keyLengthBytes:
                    Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
                iterations:
                    Config.primitives.derivation.pbkdf.iterations.I_32000
              }
            }
          }
        };

        cryptolib('stores.sks', 'commons.utils', f_main);
      });

      it('opens a keystore and gets the full certificate chain', function() {

        var f_main = function(box) {
          var data = testKeystore;

          // open the keystore
          var sksReader = new box.stores.sks.SksReader(data);

          var converters = new box.commons.utils.Converters();
          var passwordBase64 =
              converters.base64Encode('01234567890abcdefghijk');

          // get all certificate chains
          var certificateChain = sksReader.getCertificateChain(passwordBase64);

          expect(certificateChain.length).toEqual(7);
        };

        f_main.policies = {
          symmetric: {
            secretkey: {
              encryption: {lengthBytes: 32},
              mac: {
                lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
              },
              secureRandom: {
                provider: Config.symmetric.secretkey.secureRandom.provider.SCYTL
              }
            },
            cipher: {
              provider: Config.symmetric.cipher.provider.FORGE,
              algorithm: {
                AES128_GCM: {
                  name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                  keyLengthBytes: 16,
                  tagLengthBytes: Config.symmetric.cipher.algorithm.AES128_GCM
                                      .tagLengthBytes
                }
              },
              initializationVectorLengthBytes:
                  Config.symmetric.cipher.initializationVectorLengthBytes.IV_12,
              secureRandom: {
                provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
              }
            }
          },
          primitives: {
            derivation: {
              pbkdf: {
                provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                saltLengthBytes: 32,
                keyLengthBytes:
                    Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
                iterations: Config.primitives.derivation.pbkdf.iterations.I_1
              }
            }
          }
        };

        cryptolib('stores.sks', 'commons.utils', f_main);
      });

      it('opens a keystore and gets a certificate chain, by private key alias',
         function() {

           var f_main = function(box) {
             var data = testKeystore;

             // open the keystore
             var sksReader = new box.stores.sks.SksReader(data);

             var converters = new box.commons.utils.Converters();
             var passwordBase64 =
                 converters.base64Encode('01234567890abcdefghijk');

             // get all certificate chains
             var user1CertificateChain =
                 sksReader.getCertificateChainByAlias(passwordBase64, 'user1');
             var user2CertificateChain =
                 sksReader.getCertificateChainByAlias(passwordBase64, 'user2');

             expect(user1CertificateChain.length).toEqual(3);
             expect(user2CertificateChain.length).toEqual(4);
           };

           f_main.policies = {
             symmetric: {
               secretkey: {
                 encryption: {lengthBytes: 32},
                 mac: {
                   lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
                 },
                 secureRandom: {
                   provider:
                       Config.symmetric.secretkey.secureRandom.provider.SCYTL
                 }
               },
               cipher: {
                 provider: Config.symmetric.cipher.provider.FORGE,
                 algorithm: {
                   AES128_GCM: {
                     name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                     keyLengthBytes: 16,
                     tagLengthBytes: Config.symmetric.cipher.algorithm
                                         .AES128_GCM.tagLengthBytes
                   }
                 },
                 initializationVectorLengthBytes:
                     Config.symmetric.cipher.initializationVectorLengthBytes
                         .IV_12,
                 secureRandom: {
                   provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
                 }
               }
             },
             primitives: {
               derivation: {
                 pbkdf: {
                   provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                   hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                   saltLengthBytes: 20,
                   keyLengthBytes:
                       Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
                   iterations: Config.primitives.derivation.pbkdf.iterations.I_1
                 }
               }
             }
           };
           cryptolib('stores.sks', 'commons.utils', f_main);
         });

      it('opens a keystore and gets a certificate, by subject common name',
         function() {

           var f_main = function(box) {
             var data = testKeystore;

             var sksReader = new box.stores.sks.SksReader(data);

             var converters = new box.commons.utils.Converters();
             var passwordBase64 =
                 converters.base64Encode('01234567890abcdefghijk');

             // retrieve certificate from key store, with subject common name
             // 'SecDev'.
             var certificatePem =
                 sksReader.getCertificateBySubject(passwordBase64, 'SecDev');

             // verify contents of certificate (which is self-signed in this
             // example).
             var certificate =
                 new box.certificates.service.load(certificatePem);
             expect(certificate.getSubjectCN()).toBe('SecDev');
             expect(certificate.getSubjectOrgUnit()).toBe('QA');
             expect(certificate.getSubjectOrg()).toBe('Scytl');
             expect(certificate.getSubjectCountry()).toBe('ES');
             expect(certificate.verify(certificatePem)).toBe(true);
           };

           f_main.policies = {
             symmetric: {
               secretkey: {
                 encryption: {lengthBytes: 32},
                 mac: {
                   lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
                 },
                 secureRandom: {
                   provider:
                       Config.symmetric.secretkey.secureRandom.provider.SCYTL
                 }
               },
               cipher: {
                 provider: Config.symmetric.cipher.provider.FORGE,
                 algorithm: {
                   AES128_GCM: {
                     name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                     keyLengthBytes: 16,
                     tagLengthBytes: Config.symmetric.cipher.algorithm
                                         .AES128_GCM.tagLengthBytes
                   }
                 },
                 initializationVectorLengthBytes:
                     Config.symmetric.cipher.initializationVectorLengthBytes
                         .IV_12,
                 secureRandom: {
                   provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
                 }
               }
             },
             primitives: {
               derivation: {
                 pbkdf: {
                   provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                   hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                   saltLengthBytes: 20,
                   keyLengthBytes:
                       Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
                   iterations: Config.primitives.derivation.pbkdf.iterations.I_1
                 }
               }
             }
           };

           cryptolib(
               'stores.sks', 'certificates.service', 'commons.utils', f_main);
         });

      it('should derive the key only once', function() {

        var f_main = function(box) {
          var data = testKeystore;

          var sksReader = new box.stores.sks.SksReader(data);

          var converters = new box.commons.utils.Converters();
          var passwordBase64 =
              converters.base64Encode('01234567890abcdefghijk');

          var spy = spyOn(sksReader, '_deriveKey').and.callThrough();
          // Retrieve a certificate a few times.
          for (var i = 0; i < 5; i++) {
            sksReader.getCertificateBySubject(passwordBase64, 'SecDev');
          }
          expect(spy).toHaveBeenCalledTimes(1);
        };

        f_main.policies = {
          symmetric: {
            secretkey: {
              encryption: {lengthBytes: 32},
              mac: {
                lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
              },
              secureRandom: {
                provider: Config.symmetric.secretkey.secureRandom.provider.SCYTL
              }
            },
            cipher: {
              provider: Config.symmetric.cipher.provider.FORGE,
              algorithm: {
                AES128_GCM: {
                  name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                  keyLengthBytes: 16,
                  tagLengthBytes: Config.symmetric.cipher.algorithm.AES128_GCM
                                      .tagLengthBytes
                }
              },
              initializationVectorLengthBytes:
                  Config.symmetric.cipher.initializationVectorLengthBytes.IV_12,
              secureRandom: {
                provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
              }
            }
          },
          primitives: {
            derivation: {
              pbkdf: {
                provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                saltLengthBytes: 20,
                keyLengthBytes:
                    Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
                iterations: Config.primitives.derivation.pbkdf.iterations.I_1
              }
            }
          }
        };

        cryptolib('stores.sks', 'commons.utils', f_main);
      });
    });
  });

  describe('bugfix 102362', function() {
    var sksBug102362 = testKeystore;

    it('must retrieve a private key by alias', function() {
      cryptolibPolicies = {
        symmetric: {
          secretkey: {
            encryption: {lengthBytes: 32},
            mac:
                {lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32},
            secureRandom: {
              provider: Config.symmetric.secretkey.secureRandom.provider.SCYTL
            }
          },
          cipher: {
            provider: Config.symmetric.cipher.provider.FORGE,
            algorithm: {
              AES128_GCM: {
                name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                keyLengthBytes: 16,
                tagLengthBytes:
                    Config.symmetric.cipher.algorithm.AES128_GCM.tagLengthBytes
              }
            },
            initializationVectorLengthBytes:
                Config.symmetric.cipher.initializationVectorLengthBytes.IV_12,
            secureRandom: {
              provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
            }
          }
        },
        primitives: {
          derivation: {
            pbkdf: {
              provider: Config.primitives.derivation.pbkdf.provider.FORGE,
              hash: Config.primitives.derivation.pbkdf.hash.SHA256,
              saltLengthBytes: 20,
              keyLengthBytes:
                  Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
              iterations: 1
            }
          }
        }
      };

      cryptolib('stores.sks', 'commons.utils', function(box) {
        var converters = new box.commons.utils.Converters();

        var sksReader = new box.stores.sks.SksReader(sksBug102362);
        var passwordBase64 = converters.base64Encode('01234567890abcdefghijk');


        var privateKey = sksReader.getPrivateKeys(passwordBase64, 'user1');

        expect(privateKey.user1).toBeDefined();
      });
    });

    it('must retrieve only the requested key/keys', function() {
      cryptolibPolicies = {
        symmetric: {
          secretkey: {
            encryption: {lengthBytes: 32},
            mac:
                {lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32},
            secureRandom: {
              provider: Config.symmetric.secretkey.secureRandom.provider.SCYTL
            }
          },
          cipher: {
            provider: Config.symmetric.cipher.provider.FORGE,
            algorithm: {
              AES128_GCM: {
                name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                keyLengthBytes: 16,
                tagLengthBytes:
                    Config.symmetric.cipher.algorithm.AES128_GCM.tagLengthBytes
              }
            },
            initializationVectorLengthBytes:
                Config.symmetric.cipher.initializationVectorLengthBytes.IV_12,
            secureRandom: {
              provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
            }
          }
        },
        primitives: {
          derivation: {
            pbkdf: {
              provider: Config.primitives.derivation.pbkdf.provider.FORGE,
              hash: Config.primitives.derivation.pbkdf.hash.SHA256,
              saltLengthBytes: 20,
              keyLengthBytes:
                  Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
              iterations: 1
            }
          }
        }
      };

      cryptolib('stores.sks', 'commons.utils', function(box) {
        var converters = new box.commons.utils.Converters();

        var sksReader = new box.stores.sks.SksReader(sksBug102362);
        var passwordBase64 = converters.base64Encode('01234567890abcdefghijk');


        var privateKey = sksReader.getPrivateKeys(passwordBase64, 'user2');

        expect(privateKey.user1).not.toBeDefined();
      });
    });

    it('if not alias is specified, it must retrieve all the secret key in the keystore',
       function() {
         cryptolibPolicies = {
           symmetric: {
             secretkey: {
               encryption: {lengthBytes: 32},
               mac: {
                 lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32
               },
               secureRandom: {
                 provider:
                     Config.symmetric.secretkey.secureRandom.provider.SCYTL
               }
             },
             cipher: {
               provider: Config.symmetric.cipher.provider.FORGE,
               algorithm: {
                 AES128_GCM: {
                   name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                   keyLengthBytes: 16,
                   tagLengthBytes: Config.symmetric.cipher.algorithm.AES128_GCM
                                       .tagLengthBytes
                 }
               },
               initializationVectorLengthBytes:
                   Config.symmetric.cipher.initializationVectorLengthBytes
                       .IV_12,
               secureRandom: {
                 provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
               }
             }
           },
           primitives: {
             derivation: {
               pbkdf: {
                 provider: Config.primitives.derivation.pbkdf.provider.FORGE,
                 hash: Config.primitives.derivation.pbkdf.hash.SHA256,
                 saltLengthBytes: 20,
                 keyLengthBytes:
                     Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
                 iterations: 1
               }
             }
           }
         };

         cryptolib('stores.sks', 'commons.utils', function(box) {
           var converters = new box.commons.utils.Converters();

           var sksReader = new box.stores.sks.SksReader(sksBug102362);
           var passwordBase64 =
               converters.base64Encode('01234567890abcdefghijk');


           var privateKey = sksReader.getPrivateKeys(passwordBase64);

           expect(privateKey.user1).toBeDefined();
           expect(privateKey.user2).toBeDefined();
         });
       });
  });

  describe('bugfix #102457', function() {
    it('sks should manage invalid symmetric aliases', function() {
      cryptolibPolicies = {
        symmetric: {
          secretkey: {
            encryption: {lengthBytes: 32},
            mac:
                {lengthBytes: Config.symmetric.secretkey.mac.lengthBytes.SK_32},
            secureRandom: {
              provider: Config.symmetric.secretkey.secureRandom.provider.SCYTL
            }
          },
          cipher: {
            provider: Config.symmetric.cipher.provider.FORGE,
            algorithm: {
              AES128_GCM: {
                name: Config.symmetric.cipher.algorithm.AES128_GCM.name,
                keyLengthBytes: 16,
                tagLengthBytes:
                    Config.symmetric.cipher.algorithm.AES128_GCM.tagLengthBytes
              }
            },
            initializationVectorLengthBytes:
                Config.symmetric.cipher.initializationVectorLengthBytes.IV_12,
            secureRandom: {
              provider: Config.symmetric.cipher.secureRandom.provider.SCYTL
            }
          }
        },
        primitives: {
          derivation: {
            pbkdf: {
              provider: Config.primitives.derivation.pbkdf.provider.FORGE,
              hash: Config.primitives.derivation.pbkdf.hash.SHA256,
              saltLengthBytes: 20,
              keyLengthBytes:
                  Config.primitives.derivation.pbkdf.keyLengthBytes.KL_16,
              iterations: Config.primitives.derivation.pbkdf.iterations.I_1
            }
          }
        }
      };

      cryptolib('stores.sks', 'commons.utils', function(box) {
        var data = testKeystore;
        var sksReader = new box.stores.sks.SksReader(data);

        var converters = new box.commons.utils.Converters();
        var passwordBase64 = converters.base64Encode('01234567890abcdefghijk');

        expect(function() {
          sksReader.getSecretKey('werypvqwenprqv', passwordBase64);
        }).toThrow();
      });
    });
  });
});
