/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe('Cryptolib: ', function() {
  'use strict';

  it('Should be possible to create a CryptoException object', function() {
    cryptolib('commons', function(box) {
      var exception = new box.commons.exceptions.CryptoLibException(
          'customMessage', 'originalMessage');
      expect(exception).toBeDefined();
    });
  });

  it('Should be possible to create have a string representation of the exception',
     function() {
       cryptolib('commons', function(box) {
         var exception = new box.commons.exceptions.CryptoLibException(
             'customMessage', 'originalMessage');
         expect(exception.toString()).toBeDefined();
       });
     });
});
