# WARNING!
As of June, 2018, this library has become **DEPRECATED**. There are no plans to remove it, but it will no longer be maintained. It has been replaced by an `npm` publishable version of `cryptolib-js` whose source code is also located in the project `scytl-cryptolib`, with folder names that follow the pattern `cryptolib-js-*` (for example, `cryptolib-js-securerandom`). The module `cryptolib-js-buildtools` is used to publish these modules to Nexus. The URL where the library is published is <https://nexus.scytl.net/content/groups/public-npm/> and the library may be `npm` installed via the following module names: `scytl-*` (where: `*`=`codec`, `bitwise`, `cryptopolicy`, `securerandom`, `messagedigest`, `pbkdf`, `symmetric`, `asymmetric`, `certificate`, `digitalenvelope`, `keystore`, `mathematical`, `elgamal`, `zkproof`). Please use this version of `crytolib-js` for all of your future projects and convert any of your existing projects to this version, when time allows.

# Cryptolib-JS - A quick start to the Scytl javascript cryptography library

# Released artifacts
CryptoLib JS and Forge are released in a zip file, located at
http://bcngpp1vsprdcitest.scytl.net:8080/job/COMM-alice-and-bobs-scytl-cryptoLib-js-master/lastSuccessfulBuild/artifact/dist/

Folder labeled ‘lib’ has crypto-lib.js and ‘sample/vendor/forge.min’ includes index.js
# Html setup
Html page used to render JS code should declare Forge and CryptoLib JS:

```html
<head>
  <script type="text/javascript" src="../sample/vendor/forge.min/index.js"></script><!-->From released zip file<-->
  <script type="text/javascript" src="../lib/crypto-lib.js"></script><!-->From released zip file<-->
  <script type="text/javascript" src="myTestPolicyProperties.js"></script><!-->From https://scm.scytl.net/stash/projects/COMM/repos/scytl-cryptolib-js/browse/src/lib/cryptolib.spec.js<-->
  <script type="text/javascript" src="myTestCode.js"></script><!-->Your code goes there<-->
</head>
```
3rd resource declared here refers to properties file for policy, that should be based on https://scm.scytl.net/stash/projects/COMM/repos/scytl-cryptolib-js/browse/src/lib/cryptolib.spec.js

Last one refers to usage example detailed at continuation.
So with such an html header set, javascript functions from 'myTestCode.js' can be called when an html event happens (e.g. clicking a button).
Custom functions are then in charge of calling CryptoLib JS modules.

# Symmetric service
Unit tests (files named '*.spec.js') are another good starting point in order to find examples of JS code snippets involving CryptoLib JS, at location: https://scm.scytl.net/stash/projects/COMM/repos/scytl-cryptolib-js/browse/src/lib

## How to getSecretKeyForEncryption (full example)

``` javascript
function testGetSecretKeyForEncryption() {
    cryptoPRNG.startEntropyCollection();
    //in a production environment, entropy source events take place before stopping entropy collection
    cryptoPRNG.stopEntropyCollectionAndCreatePRNG();
    cryptolib('symmetric.service', function(box) {
        var cryptoSecretKey = box.symmetric.service.getSecretKeyForEncryption();
        //send SecretKey to html
        document.form1.symmetricKey.value = cryptoSecretKey;
    });
}
```

Note that html would need the following to trigger this function:

``` html
<input type="button" value="Symmetric Key Generator" onClick="testGetSecretKeyForEncryption();"/><br/>
[...]
<textarea name="symmetricKey" rows="10" cols="72">
</textarea><br/>
```

## How to getSecretKeyForMac (from unit test)
``` javascript
cryptolib('symmetric.service', function(box) {
  var secretKey = box.symmetric.service.getSecretKeyForMac();
});
```

## How to encrypt and decrypt (from unit test)
``` javascript
cryptolib('symmetric.service', 'commons.utils', function(box) {
    // generate a secret key
    var secretKeyB64 = box.symmetric.service.getSecretKeyForEncryption();
    // encrypt and decrypt data
    var converters = new box.commons.utils.Converters();
    var dataB64 = converters.base64Encode("anyString");
    var encryptedDataBase64 = box.symmetric.service.encrypt(secretKeyB64, dataB64);
    var decryptedDataBase64 = box.symmetric.service.decrypt(secretKeyB64, encryptedDataBase64);
});
```

## How to getMac and verifyMac (from unit test)
``` javascript
cryptolib('symmetric.service', 'commons.utils', function(box) {
    // generate a secret key
    var secretKeyB64 = box.symmetric.service.getSecretKeyForMac();
    // encrypt and decrypt data
    var converters = new box.commons.utils.Converters();
    var dataB64 = converters.base64Encode("anyString");
    var macB64 = box.symmetric.service.getMac(secretKeyB64, [dataB64]);
    var verified = box.symmetric.service.verifyMac(macB64, secretKeyB64, [dataB64]);
});
```


# Asymmetric service

## How to getKeyPairForEncryption (from unit test)
``` javascript
cryptolib('asymmetric.service', function(box) {
    var encryptionKeyPair = box.asymmetric.service.getKeyPairForEncryption();
});
```

## How to encrypt and decrypt (from unit test)
``` javascript
cryptolib('asymmetric.service', 'commons.utils', function(box) {
    var converters = new box.commons.utils.Converters();
    var dataB64= converters.base64Encode("anyString");
    var publicKeyB64="placeYourPublicKeyHere";
    var privateKeyB64="placeYourPrivateKeyHere";
    var encryptedDataB64 = box.asymmetric.service.encrypt(publicKeyB64, dataB64);
    var decryptedDataB64 = box.asymmetric.service.decrypt(privateKeyB64, encryptedDataB64);
});
```

## How to sign and verifySignature (from unit test)
``` javascript
cryptolib('asymmetric.service', 'commons.utils', function(box) {
    var converters = new box.commons.utils.Converters();
    var dataB64 = converters.base64Encode("anyString");
    var arrayDataBase64 = [dataB64];
    var publicKeyB64="placeYourPublicKeyHere";
    var privateKeyB64="placeYourPrivateKeyHere";
    var signatureBase64 = box.asymmetric.service.sign(privateKeyB64, arrayDataBase64);
    var verified = box.asymmetric.service.verifySignature(signatureBase64, publicKeyB64, arrayDataBase64);
});
```

## How to verifyXmlSignature (from QA interface)
``` javascript
function xmlSigVerification() {
    cryptoPRNG.startEntropyCollection();
    var htmlpublicKey = document.form1.publicKey.value.toString();
    var htmlsignatureParentNode = document.form1.signatureParentNode.value.toString();
    var htmlxmlfile = document.form1.xmlfile.value.toString();
    cryptoPRNG.stopEntropyCollectionAndCreatePRNG();
    cryptolib('*', function(box) {
      var abCert = box.forge.pki.certificateFromPem(htmlpublicKey, true);
      var publicKey = abCert.publicKey;
      verificationResult = box.asymmetric.service.verifyXmlSignature(publicKey, htmlxmlfile, htmlsignatureParentNode);
    });
}
```


# Certificates service

## How to load (from unit test)
``` javascript
cryptolib('certificates', function (box) {
    var myX509pemCertificate = "placeYourX509PemCertificateHere";
    var loadedCertificate = new box.certificates.service.load(myX509pemCertificate);
});
```

## How to validateCertificate (from unit test)
``` javascript
var myX509pemCertificate = "placeYourX509PemCertificateHere";
cryptolib('certificates', function(box) {
    var validationData = {
        subject: {
            commonName: "Subject CN",
            organization: "Subject Org",
            organizationUnit: "Subject Org Unit",
            country: "Subject Country"
        }
    };
    var validator = new box.certificates.CryptoX509CertificateValidator(validationData);
    var failedValidations = validator.validate(myX509pemCertificate);
});
```

## How to validateX509CertificateChain (from unit test)
``` javascript
cryptolib('certificates', function(box) {
    var certificateChain = {
      leaf: {
          pem: "-----BEGIN CERTIFICATE----------END CERTIFICATE-----",
          keytype: "Sign",
          subject: {
              commonName: "leafCommonName",
              organization: "leafOrganization",
              organizationUnit: "leafOrganizationUnit",
              country: "ES"
          },
          time: "2014-12-25'T'00:00:00Z"
      },
      root: "-----BEGIN CERTIFICATE----------END CERTIFICATE-----",
      certificates: {
          pems: [
              "-----BEGIN CERTIFICATE----------END CERTIFICATE-----"
          ],
          subjects: [
              {
                  commonName: "intermediateCommonName",
                  organization: "intermediateOrganization",
                  organizationUnit: "intermediateOrganizationUnit",
                  country: "ES"
              }
          ]
      }
    };
    var validationFailed = box.certificates.service.validateX509CertificateChain(certificateChain);
});
```

## How to flattenFailedValidations (from unit test)
``` javascript
cryptolib('certificates', function(box) {
    var failedValidations = box.certificates.service.validateX509CertificateChain(certificateChain);
    var flattenFailedValidations = box.certificates.service.flattenFailedValidations(failedValidations);
});
```

# Proof service
Unit tests (files named '*.spec.js') are another good starting point in order to find examples of JS code snippets involving CryptoLib JS, at location: https://scm.scytl.net/stash/projects/COMM/repos/scytl-cryptolib-js/browse/src/lib

## How to createSimplePlaintextEqualityProof:

``` javascript
var proof = box.proofs.service.createSimplePlaintextEqualityProof(
                    primaryCiphertext, primaryPublicKey, witness, secondaryCiphertext,
                    secondaryPublicKey, zpSubgroup);
```

## How to createSimplePlaintextEqualityProof with precomputed values:

``` javascript
 var preComputedValues = box.proofs.service.preComputeSimplePlaintextEqualityProof(
                    primaryPublicKey, secondaryPublicKey, zpSubgroup);
 var proof = box.proofs.service.createSimplePlaintextEqualityProof(
                    primaryCiphertext, primaryPublicKey, witness, secondaryCiphertext,
                    secondaryPublicKey, zpSubgroup, preComputedValues);
```

preComputedValues´s type is box.proofs.ProofPreComputedValues.

## How to createSimplePlaintextEqualityProof with progress meter:

``` javascript
var proof = box.proofs.service.createSimplePlaintextEqualityProof(
                    primaryCiphertext, primaryPublicKey, witness, secondaryCiphertext,
                    secondaryPublicKey, zpSubgroup, progressCallback, progressPercentMinCheckInterval);
```

## How to createSimplePlaintextEqualityProof with precomputed values with progress meter:

``` javascript
 var preComputedValues = box.proofs.service.preComputeSimplePlaintextEqualityProof(
                    primaryPublicKey, secondaryPublicKey, zpSubgroup, progressCallback, progressPercentMinCheckInterval);
 var proof = box.proofs.service.createSimplePlaintextEqualityProof(
                    primaryCiphertext, primaryPublicKey, witness, secondaryCiphertext,
                    secondaryPublicKey, zpSubgroup, preComputedValues);
```

# More examples

More examples of html pages used by QA can be found at 2 places:

- integrated level, ready for testing (this depends on jenkins job execution status and can be out of order) : http://bcngpp1vsprdcitest.scytl.net:8080/view/Alice%20and%20Bobs/job/COMM-alice-and-bobs-scytl-qa-cryptoLibClient-master/ws/from-scratch/target/appassembler/webapp/
- repository level: https://scm.scytl.net/stash/projects/COMM/repos/scytl-qa-cryptolibclient/browse/from-scratch/src/main/webapp/WEB-INF

# Library technical design

[Read more &raquo;](libraryDesign.md)


# Versions

[Read more &raquo;](versioning-scytl-projects.md)

# Changelog

Use of conventional-changelog grunt plugin [Read more &raquo;](conventional-changelog.md), based by this commit conventions [Read more &raquo;](commit-conventions.md)
