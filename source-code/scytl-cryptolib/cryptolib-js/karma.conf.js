/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
module.exports = function(config) {
  'use strict';
  config.set({
    basePath: '.',
    singleRun: true,
    browserNoActivityTimeout: 60 * 60 * 1000,
    files: [
      'node_modules/node-forge/dist/forge.min.js', 'node_modules/sjcl/sjcl.js',
      'lib/cryptolib.js', 'lib/*.js', 'spec/*.js'
    ],
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    preprocessors: {'lib/*.js': ['coverage']},
    reporters: ['progress', 'junit', 'coverage'],
    junitReporter: {
      useBrowserName: false,
      outputDir: 'target/reports/junit',
      outputFile: 'TESTS-xunit.xml'
    },
    coverageReporter:
        {type: 'lcov', dir: 'target/reports', subdir: 'coverage'}
  });
};
