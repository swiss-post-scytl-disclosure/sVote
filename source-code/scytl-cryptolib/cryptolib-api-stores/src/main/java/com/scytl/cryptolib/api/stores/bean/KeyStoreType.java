/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.api.stores.bean;

/**
 * Enum which defines the key store types.
 */
public enum KeyStoreType {

    PKCS12("PKCS12"), JCEKS("JCEKS");

    private final String _keyStoreTypeName;

    KeyStoreType(final String keyStoreTypeName) {
        _keyStoreTypeName = keyStoreTypeName;
    }

    public String getKeyStoreTypeName() {
        return _keyStoreTypeName;
    }
}
