/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = TouchEndCollector;

var collectEntropy_;

function TouchEndCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

TouchEndCollector.prototype = {
  handleEvent: function() {
    collectEntropy_((+new Date()), 1);
  }
};