/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = MouseWheelCollector;

var collectEntropy_;

function MouseWheelCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

MouseWheelCollector.prototype = {
  handleEvent: function(ev) {
    ev = ev || window.event;
    collectEntropy_((ev.offsetY || 0) + (+new Date()), 3);
  }
};