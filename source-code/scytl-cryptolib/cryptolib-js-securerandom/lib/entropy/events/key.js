/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = KeyCollector;

var collectEntropy_;

function KeyCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

KeyCollector.prototype = {
  handleEvent: function(ev) {
    ev = ev || window.event;
    collectEntropy_((ev.keyCode) + (+new Date()), 3);
  }
};