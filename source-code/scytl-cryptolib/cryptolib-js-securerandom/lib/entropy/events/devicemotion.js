/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var math = require('./math');

module.exports = DeviceMotionCollector;

var motionX_, motionY_, motionZ_;
var collectEntropy_ = function() {};

function DeviceMotionCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

DeviceMotionCollector.prototype = {
  handleEvent: function(ev) {
    ev = ev || window.event;
    var acceleration = ev.accelerationIncludingGravity;
    var currentX = (math.entropyRound(acceleration.x) || 0);
    var currentY = (math.entropyRound(acceleration.y) || 0);
    var currentZ = (math.entropyRound(acceleration.z) || 0);
    var rotation = ev.rotationRate;
    if (rotation !== null) {
      currentX += math.entropyRound(rotation.alpha);
      currentY += math.entropyRound(rotation.beta);
      currentZ += math.entropyRound(rotation.gamma);
    }
    // The API detects and delivers accelerometer data 50 times per
    // second
    // even if there is any event related, so this is
    // a way to control if it really changed or not
    if ((motionX_ === null) || (motionY_ === null) || (motionZ_ === null) ||
        (motionX_ !== currentX) || (motionY_ !== currentY) ||
        (motionZ_ !== currentZ)) {
      motionX_ = currentX;
      motionY_ = currentY;
      motionZ_ = currentZ;
      collectEntropy_(currentX + currentY + currentZ + (+new Date()), 1);
    }
  }
};