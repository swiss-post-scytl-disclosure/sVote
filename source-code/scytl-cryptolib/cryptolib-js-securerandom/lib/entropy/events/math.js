/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var MIN_MATH_ROUND_ENTROPY_FACTOR = 1000000;

module.exports = {
  entropyRound: function(data) {
    return Math.round(data * MIN_MATH_ROUND_ENTROPY_FACTOR);
  }
};