/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = MouseMoveEventCollector;

var collectEntropy_;

function MouseMoveEventCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

MouseMoveEventCollector.prototype = {
  handleEvent: function(ev) {
    // to ensure compatibility with IE 8
    ev = ev || window.event;
    collectEntropy_(
        (ev.x || ev.clientX || ev.offsetX || 0) +
            (ev.y || ev.clientY || ev.offsetY || 0) + (+new Date()),
        3);
  }
};
