/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = MouseButtonCollector;

var collectEntropy_;

function MouseButtonCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

MouseButtonCollector.prototype = {
  handleEvent: function(ev) {
    ev = ev || window.event;
    collectEntropy_(
        (ev.x || ev.clientX || ev.offsetX || 0) +
            (ev.y || ev.clientY || ev.offsetY || 0) + (+new Date()),
        3);
  }
};