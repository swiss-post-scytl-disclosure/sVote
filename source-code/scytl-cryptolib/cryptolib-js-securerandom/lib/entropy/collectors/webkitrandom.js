/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = WebKitRandomCollector;

var collectEntropy_, globalCrypto_, entropyQuantity_;

function WebKitRandomCollector(
    collectEntropyCallback, cryptoApi, entropyQuantity) {
  collectEntropy_ = collectEntropyCallback;
  globalCrypto_ = cryptoApi;
  entropyQuantity_ = entropyQuantity;
}

WebKitRandomCollector.prototype = {
  startCollector: function() {
    var numPositions = entropyQuantity_ / 32;
    // get cryptographically strong entropy in Webkit
    var ab = new Uint32Array(numPositions);
    globalCrypto_.getRandomValues(ab);
    var data = '';
    for (var i = 0; i < ab.length; i++) {
      data += '' + (ab[i]);
    }
    collectEntropy_(data, entropyQuantity_);
  },

  stopCollector: function() {
    // just executed once, so no need to execute anything else
    return true;
  }
};
