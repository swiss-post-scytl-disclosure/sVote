/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = AjaxCollector;

var collectEntropy_;

function AjaxCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

AjaxCollector.prototype = {
  startCollector: function() {
    // if jQuery is included
    if (window.jQuery !== undefined) {
      $(window)
          .ajaxStart(function() {
            collectEntropy_((+new Date()), 1);
          })
          .ajaxComplete(function() {
            collectEntropy_((+new Date()), 1);
          });
    }
  },

  stopCollector: function() {
    // if jQuery is included
    if (window.jQuery !== undefined) {
      $(window).unbind('ajaxStart');
      $(window).unbind('ajaxComplete');
    }
    return true;
  }
};