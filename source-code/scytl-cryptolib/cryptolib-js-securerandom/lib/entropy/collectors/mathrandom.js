/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = MathRandomCollector;

var collectEntropy_ = function() {};

function MathRandomCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

MathRandomCollector.prototype = {
  startCollector: function() {
    collectEntropy_(Math.random(), 0);
  },

  stopCollector: function() {
    // just executed once, so no need to execute anything else
    return true;
  }
};
