/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = JSExecutionCollector;

var collectEntropy_ = function() {};

function JSExecutionCollector(collectEntropyCallback) {
  collectEntropy_ = collectEntropyCallback;
}

JSExecutionCollector.prototype = {
  _collectTimeout: null,

  startCollector: function() {
    var timer_start = (+new Date()), timer_end;

    this._collectTimeout = (function collect() {
      timer_end = (+new Date());
      var total_time = timer_end - timer_start;
      // because of browser baseline time checks we limit it
      if (total_time > 20) {
        collectEntropy_((+new Date()), 1);
      }
      timer_start = timer_end;
      return setTimeout(collect, 0);
    })();
  },

  stopCollector: function() {
    clearTimeout(this._collectTimeout);
  }
};