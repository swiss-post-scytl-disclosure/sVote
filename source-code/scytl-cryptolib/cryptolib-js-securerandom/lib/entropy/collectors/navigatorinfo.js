/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = NavigatorInfoCollector;

var collectEntropy_ = function() {};

function NavigatorInfoCollector(collectEntropyCallback) {
    collectEntropy_ = collectEntropyCallback;
}

NavigatorInfoCollector.prototype = {
  startCollector: function() {
    if (typeof(navigator) !== 'undefined') {
      var _navString = '';
      for (var key in navigator) {
        if (typeof key !== 'undefined') {
          try {
            if (typeof(navigator[key]) === 'string') {
              _navString += navigator[key];
            }
          } catch (e) {
            // ignore any kind of exception
          }
        }
      }
      collectEntropy_(_navString, 1);
    }
  },

  stopCollector: function() {
    // just executed once, so no need to execute anything else
    return true;
  }
};
