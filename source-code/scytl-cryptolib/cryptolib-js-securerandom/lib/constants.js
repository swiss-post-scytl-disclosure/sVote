/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

/**
 * Defines constants for the secure random service.
 */
module.exports = Object.freeze({
  SECURE_RANDOM_MAX_NUM_BYTES: 512,
  SECURE_RANDOM_MAX_NUM_DIGITS: 512,
  SECURE_RANDOM_DEFAULT_NUM_SEED_BYTES: 32
});
