/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var secureRandom = require('../lib/index');
var constants = require('../lib/constants');

describe('The secure random module should be able to ...', function() {
  var PRIVATE_KEY_PEM =
      '-----BEGIN RSA PRIVATE KEY-----MIIEowIBAAKCAQEAglypZU45bnf2opnRI+Y51VqouBpvDT33xIB/OtbwKzwVpi+JrjBFtfk33tE9t/dSRs79CK94HRhCWcOiLa2qWPrjeZ9SBiEyScrhIvRZVBF41zBgwQNuRvJCsKmAqlZaFNJDZxEP4repmlBn1CfVFmfrXmOKqwP5F7l9ZtucveRzsfmF1yVPFkW8TMuB3YqMiyymyqHlS8ujCsu5I8tpgPbwuxdMOY94fNhSXrYkY8IuX1g1zdq/Z1jluOaR/UqK4UpnbuJaH/F0VgDNiWh6cTD0DFGEk0b70i5wU4Q3L/S6XZQRvSuADoCbhwBKuFL5pW5n865oLVb5S3wuVdWaGwIDAQABAoIBAC/tn34Wf3kE9BGeGc1oFLVDaqqdVVz5/oEpeR2J7q0GnzMFYUpAhzC7WvY52cYsUPyll1Q9Jx0TUTmteo/uvKWQQFfz4nVMeS+2PoXabolBDzuWlsv/1eiRo0FOYHa/3siu8YcQN9X0DpAkpbfTmT1uoZOHZ3EuucMmOFu7vGn38Grw8bSxpR0uvTtnb8ygC+aB51y38RMyhzQQanrM8FMeAfDAy6IB0Yo7b0c50Cxa6Ax4nqn9LXyGakr5WeAMkgTIOA/GId9SZD4e5eRpq+628pOeR4O9datFltgl6r1+A4ii2VrJsDqeatGtODlX6KRKqwFHoGIa2TjgSZLuorECgYEAxeSZDOOgFsI5mB7RkRzZaQ9znJ15sgdyZiAFZAOUah4hSGdAXNAnZTlrdacduXEu3EfkpuPToX7xZSv5FRYwfBwMwCLeytlGLPjQzWejZGbo4+KqgzWb9fECDYVtDPlJ/+yLih9nt67BHweJKxYydl18rVigdVyy22X86NijSykCgYEAqKPUrXZAo+TJvmTw4tgsibJgvXBYBhmsej8mGNQw+Nyp2gV28sgm61ifIeXKS8teq+MFwGA6cHQedbsCqhMHokdhESZmlbWxhSFLihQcewBxwvrBwbaxI23yXRzwMewznZFL032PpcbqrmwFmcSSEZ3nmbvTH6ShqLW+pzDNp6MCgYBQLzdgxJ7qedqSa/JohTMG4e7rh9d2rpPJE7J7ewPZF8pOpx+qO+Gqn2COdJ+Ts2vUcAETKn9nEaPIZc/wnmQY9dioxbhWo0FPGaaphBPtq9Ez/XUv4zoFppk5V1X/isdUPsmvttf00oeIBiqrXbwmv+yz5JRn2Z7TTXjz9Ev+OQKBgQCUuoCMRzl1EgcXIqEL/0kwW6BUEqufHa9u1Ri9Vw6lvL8T6DPipMEmWK9nzuid9gtVns/ovTVtDgv7GuabplLaPQePf4WDzY11c0rSyS/hDyBFrK+LL5uEOqhAlJAGB2HyOj1clWVF+GvrTpuV5LZKUS/79pmZU7G7QCaX/0Ow7wKBgC/kDH7cmWQnWvvJ5izrx/7PogQVPOLELeUIGLu/hjsSdDKiFCxCUZ948+9NuG+DnpXDWzw//r8mPBRRGGsqFws5Aipp7yjQ3kRDCCzGelPCVhHyfmKqA+8ewXPulKS3/wIyHIvaXmsuAtTfurHtpRyzjKmCBK1Y6WQ3trIXvo7s-----END RSA PRIVATE KEY-----';

  var PIN_ENTROPY = 100;

  var randomGenerator_;
  var nonUint8Array_ = [1, 2, 3];
  var emptyString_ = '';
  var nonString_ = 999;
  var nonNumber_ = 'not a number';
  var nonPositiveNumber_ = 0;
  var nonObject_ = 0;
  var nonBoolean_ = 'not a boolean';

  beforeEach(function() {
    randomGenerator_ = secureRandom.newService().newRandomGenerator();
  });

  describe('create a secure random service that should be able to ..', function() {
    it('throw an exception when being created, using an invalid seed',
       function() {
         expect(function() {
           secureRandom.newService({prngSeed: null});
         }).toThrow();

         expect(function() {
           secureRandom.newService({prngSeed: nonUint8Array_});
         }).toThrow();
       });

    it('throw an exception when being created, using an invalid maximum entropy counter',
       function() {
         expect(function() {
           secureRandom.newService({maxEntropyCounter: null});
         }).toThrow();

         expect(function() {
           secureRandom.newService({maxEntropyCounter: nonNumber_});
         }).toThrow();

         expect(function() {
           secureRandom.newService({maxEntropyCounter: nonPositiveNumber_});
         }).toThrow();
       });

    it('throw an exception when being created, using an invalid hash updater interval',
       function() {
         expect(function() {
           secureRandom.newService({hashUpdaterInterval: null});
         }).toThrow();

         expect(function() {
           secureRandom.newService({hashUpdaterInterval: nonNumber_});
         }).toThrow();

         expect(function() {
           secureRandom.newService({hashUpdaterInterval: nonPositiveNumber_});
         }).toThrow();
       });

    it('throw an exception when being created, using an invalid Window object',
       function() {
         expect(function() {
           secureRandom.newService({windowObject: null});
         }).toThrow();

         expect(function() {
           secureRandom.newService({windowObject: nonObject_});
         }).toThrow();
       });

    it('throw an exception when being created, using an invalid Crypto API object',
       function() {
         expect(function() {
           secureRandom.newService({cryptoApi: null});
         }).toThrow();

         expect(function() {
           secureRandom.newService({cryptoApi: nonObject_});
         }).toThrow();
       });

    it('throw an exception when being created, using private key entropy collector data containing an invalid private key',
       function() {
         expect(function() {
           secureRandom.newService({privateKeyData: {pinEntropy: PIN_ENTROPY}});
         }).toThrow();

         expect(function() {
           secureRandom.newService({
             privateKeyData: {privateKey: undefined, pinEntropy: PIN_ENTROPY}
           });
         }).toThrow();

         expect(function() {
           secureRandom.newService(
               {privateKeyData: {privateKey: null, pinEntropy: PIN_ENTROPY}});
         }).toThrow();

         expect(function() {
           secureRandom.newService({
             privateKeyData:
                 {privateKey: nonString_, pinEntropy: PIN_ENTROPY}
           });
         }).toThrow();

         expect(function() {
           secureRandom.newService({
             privateKeyData:
                 {privateKey: emptyString_, pinEntropy: PIN_ENTROPY}
           });
         }).toThrow();
       });

    it('throw an exception when being created, using private key entropy collector data containing invalid PIN entropy',
       function() {
         expect(function() {
           secureRandom.newService(
               {privateKeyData: {privateKey: PRIVATE_KEY_PEM}});
         }).toThrow();

         expect(function() {
           secureRandom.newService({
             privateKeyData:
                 {privateKey: PRIVATE_KEY_PEM, pinEntropy: undefined}
           });
         }).toThrow();

         expect(function() {
           secureRandom.newService({
             privateKeyData: {privateKey: PRIVATE_KEY_PEM, pinEntropy: null}
           });
         }).toThrow();

         expect(function() {
           secureRandom.newService({
             privateKeyData:
                 {privateKey: PRIVATE_KEY_PEM, pinEntropy: nonNumber_}
           });
         }).toThrow();

         expect(function() {
           secureRandom.newService({
             privateKeyData: {
               privateKey: PRIVATE_KEY_PEM,
               pinEntropy: nonPositiveNumber_
             }
           });
         }).toThrow();
       });

    it('throw an exception when being created, usinsg an invalid noDefaultCollectors flag',
       function() {
         expect(function() {
           secureRandom.newService({noDefaultCollectors: null});
         }).toThrow();

         expect(function() {
           secureRandom.newService({noDefaultCollectors: nonBoolean_});
         }).toThrow();
       });

    describe('create a secure random generator that should be able to', function() {
      it('throw an exception when generating random bytes, using invalid input data',
         function() {
           expect(function() {
             randomGenerator_.nextBytes();
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBytes(undefined);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBytes(null);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBytes(nonNumber_);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBytes(nonPositiveNumber_);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBytes(
                 constants.SECURE_RANDOM_MAX_NUM_BYTES + 1);
           }).toThrow();
         });

      it('throw an exception when generating a random BigInteger by number of bits, using invalid input data',
         function() {
           expect(function() {
             randomGenerator_.nextBigInteger();
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigInteger(undefined);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigInteger(null);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigInteger(nonNumber_);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigInteger(nonPositiveNumber_);
           }).toThrow();
         });

      it('throw an exception when generating a random BigInteger by number of digits, using invalid input data',
         function() {
           expect(function() {
             randomGenerator_.nextBigIntegerByDigits();
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigIntegerByDigits(undefined);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigIntegerByDigits(null);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigIntegerByDigits(nonNumber_);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigIntegerByDigits(nonPositiveNumber_);
           }).toThrow();

           expect(function() {
             randomGenerator_.nextBigIntegerByDigits(
                 constants.SECURE_RANDOM_MAX_NUM_DIGITS + 1);
           }).toThrow();
         });
    });
  });
});
