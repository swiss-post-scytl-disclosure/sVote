/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/** A mock entropy collector for test purposes */

/*jshint node:true */
'use strict';

module.exports = TestEntropyCollector;

/**
 * The test entropy collector allows direct assigning of its data and entropy
 * counter. The collector is set to update entropy with the current data every
 * half second.
 *
 * @param {any} collectEntropyCallback
 */
function TestEntropyCollector(collectEntropyCallback) {
  var that = this;
  var collectionTimer_;

  var collectEntropyCallback_ = collectEntropyCallback;
  this.entropyCounter = 0;
  this.data = '';

  var previousCounter_ = 0;
  var previousData_ = '';

  this.startCollector = function() {
    collectionTimer_ = setInterval(function() {
      // Only call the entropy collector if data has changed.
      if ((that.data !== previousData_) ||
          (that.entropyCounter !== previousCounter_)) {
        console.log(
            'Collecting test entropy -- counter is now ' + that.entropyCounter);
        collectEntropyCallback_(that.data, that.entropyCounter);
        previousData_ = that.data;
        previousCounter_ = that.entropyCounter;
      }
    }, 500);
  };

  this.stopCollector = function() {
    clearInterval(collectionTimer_);
  };
}
