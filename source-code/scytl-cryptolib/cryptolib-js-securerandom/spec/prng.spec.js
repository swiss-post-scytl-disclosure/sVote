/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine: true */
'use strict';

var Prng = require('../lib/prng.js');
var codec = require('scytl-codec');

describe('The pseudo-random number generator should be able to ..', function() {
  it('generate pseudo-random numbers', function() {
    var prng = new Prng(codec.utf8Encode('123abc'));

    // Generate a 4-byte random number.
    var byteArray = prng.nextBytes(4);

    expect(byteArray).toBeDefined();
    expect(byteArray.length).toBe(4);
  });

  it('enforce a minimum length', function() {
    var prng = new Prng('123abc');

    expect(function() {
      prng.nextBytes();
    }).toThrow();

    expect(function() {
      prng.nextBytes(0);
    }).toThrow();

    expect(function() {
      prng.nextBytes(1);
    }).toBeDefined();
  });
});
