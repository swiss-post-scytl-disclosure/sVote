/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var secureRandom = require('../lib/index');

try {
  var service_ = secureRandom.newService();
  var randomGenerator_ = service_.newRandomGenerator();

  randomGenerator_.nextBytes(10);
  randomGenerator_.nextBigIntegerByDigits(10);
  randomGenerator_.nextBigIntegerByDigits(10);
} catch (e) {
  throw new Error('Secure random module cannot be used with node.js: ' + e);
}
