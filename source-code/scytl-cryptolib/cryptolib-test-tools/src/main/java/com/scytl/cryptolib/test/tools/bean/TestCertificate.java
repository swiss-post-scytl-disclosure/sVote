/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.test.tools.bean;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;

/**
 * Implementation of class {@link java.security.cert.Certificate} to be used for
 * testing purposes.
 */
public class TestCertificate extends Certificate {
    private static final long serialVersionUID = -8278774001493088558L;

    private PublicKey _publicKey;

    protected TestCertificate(final String type) {

        super(type);
    }

    /**
     * Constructor.
     *
     * @param type
     *            the type of the certificate.
     * @param publicKey
     *            the public key of the certificate.
     */
    public TestCertificate(final String type, final PublicKey publicKey) {

        super(type);

        _publicKey = publicKey;
    }

    @Override
    public byte[] getEncoded() throws CertificateEncodingException {

        return _publicKey.getEncoded();
    }

    @Override
    public PublicKey getPublicKey() {

        return _publicKey;
    }

    @Override
    public void verify(final PublicKey key)
            throws CertificateException, NoSuchAlgorithmException,
            InvalidKeyException, NoSuchProviderException,
            SignatureException {
    }

    @Override
    public void verify(final PublicKey key, final String sigProvider)
            throws CertificateException, NoSuchAlgorithmException,
            InvalidKeyException, NoSuchProviderException,
            SignatureException {
    }

    @Override
    public String toString() {

        return TestCertificate.class.getSimpleName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TestCertificate other = (TestCertificate) obj;
        if (_publicKey == null) {
            if (other._publicKey != null) {
                return false;
            }
        } else if (!_publicKey.equals(other._publicKey)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
            + ((_publicKey == null) ? 0 : _publicKey.hashCode());
        return result;
    }
}
