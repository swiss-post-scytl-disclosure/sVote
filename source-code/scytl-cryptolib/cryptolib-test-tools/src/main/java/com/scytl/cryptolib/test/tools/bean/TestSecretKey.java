/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.test.tools.bean;

import java.util.Arrays;

import javax.crypto.SecretKey;

/**
 * Implementation of interface {@link javax.crypto.SecretKey} to be used for
 * testing purposes.
 */
public class TestSecretKey implements SecretKey {

    private static final long serialVersionUID = 1L;

    private final byte[] _keyBytes;

    public TestSecretKey(final byte[] keyBytes) {

        if (keyBytes != null) {
            _keyBytes = keyBytes.clone();
        } else {
            _keyBytes = null;
        }
    }

    @Override
    public byte[] getEncoded() {

        if (_keyBytes != null) {
            return _keyBytes.clone();
        } else {
            return null;
        }
    }

    @Override
    public String getAlgorithm() {

        return "TestSecretKey Algorithm";
    }

    @Override
    public String getFormat() {

        return "TestSecretKey Format";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TestSecretKey other = (TestSecretKey) obj;
        if (!Arrays.equals(_keyBytes, other._keyBytes)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(_keyBytes);
        return result;
    }
}
