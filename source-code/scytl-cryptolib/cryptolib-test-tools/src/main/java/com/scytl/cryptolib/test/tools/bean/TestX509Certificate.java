/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.test.tools.bean;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

/**
 * Implementation of class {@link java.security.cert.X509Certificate} to be used
 * for testing purposes.
 */
public class TestX509Certificate extends X509Certificate {
    private static final long serialVersionUID = -5494125476368422902L;

    private final PublicKey _publicKey;

    private final Date _notBefore;

    private final Date _notAfter;

    public TestX509Certificate(final PublicKey publicKey) {

        _publicKey = publicKey;

        _notBefore = new Date(System.currentTimeMillis());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        _notAfter = calendar.getTime();
    }

    @Override
    public byte[] getEncoded() throws CertificateEncodingException {

        return _publicKey.getEncoded();
    }

    @Override
    public PublicKey getPublicKey() {

        return _publicKey;
    }

    @Override
    public Date getNotBefore() {

        return new Date(_notBefore.getTime());
    }

    @Override
    public Date getNotAfter() {

        return new Date(_notAfter.getTime());
    }

    @Override
    public boolean hasUnsupportedCriticalExtension() {

        return false;
    }

    @Override
    public Set<String> getCriticalExtensionOIDs() {

        return new HashSet<String>();
    }

    @Override
    public Set<String> getNonCriticalExtensionOIDs() {

        return new HashSet<String>();
    }

    @Override
    public byte[] getExtensionValue(final String oid) {

        return new byte[0];
    }

    @Override
    public void checkValidity()
            throws CertificateExpiredException,
            CertificateNotYetValidException {
    }

    @Override
    public void checkValidity(final Date date)
            throws CertificateExpiredException,
            CertificateNotYetValidException {
    }

    @Override
    public int getVersion() {

        return 0;
    }

    @Override
    public BigInteger getSerialNumber() {

        return BigInteger.ZERO;
    }

    @Override
    public Principal getIssuerDN() {

        return new X500Principal("TestX509Certificate Issuer DN");
    }

    @Override
    public Principal getSubjectDN() {

        return new X500Principal("TestX509Certificate Subject DN");
    }

    @Override
    public byte[] getTBSCertificate() throws CertificateEncodingException {

        return new byte[0];
    }

    @Override
    public byte[] getSignature() {

        return new byte[0];
    }

    @Override
    public String getSigAlgName() {

        return "Test Signature Algorithm Name";
    }

    @Override
    public String getSigAlgOID() {
        return "Test Signature Algorithm OID";
    }

    @Override
    public byte[] getSigAlgParams() {

        return new byte[0];
    }

    @Override
    public boolean[] getIssuerUniqueID() {

        return new boolean[0];
    }

    @Override
    public boolean[] getSubjectUniqueID() {

        return new boolean[0];
    }

    @Override
    public boolean[] getKeyUsage() {

        return new boolean[0];
    }

    @Override
    public int getBasicConstraints() {

        return 0;
    }

    @Override
    public void verify(final PublicKey key)
            throws CertificateException, NoSuchAlgorithmException,
            InvalidKeyException, NoSuchProviderException,
            SignatureException {
    }

    @Override
    public void verify(final PublicKey key, final String sigProvider)
            throws CertificateException, NoSuchAlgorithmException,
            InvalidKeyException, NoSuchProviderException,
            SignatureException {
    }

    @Override
    public String toString() {

        return TestX509Certificate.class.getSimpleName();
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TestX509Certificate other = (TestX509Certificate) obj;
        if (_publicKey == null) {
            if (other._publicKey != null) {
                return false;
            }
        } else if (!_publicKey.equals(other._publicKey)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
            + ((_publicKey == null) ? 0 : _publicKey.hashCode());
        return result;
    }
}
