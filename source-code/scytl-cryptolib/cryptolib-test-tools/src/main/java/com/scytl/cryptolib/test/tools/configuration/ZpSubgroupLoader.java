/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.test.tools.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Properties;

/**
 * Utility to retrieve the properties of a pre-generated Zp subgroup so that the
 * unit tests that need this information do not have to generate it themselves,
 * which is a time consuming operation.
 */
public class ZpSubgroupLoader {

    /**
     * The relative path of the properties file that should be used for the Zp
     * subgroup configuration.
     */
    private static final String PROPERTIES_FILE_PATH =
        "/properties/zpSubgroup.properties";

    private static final String P_PARAMETER_PROPERTY_KEY = "p";

    private static final String Q_PARAMETER_PROPERTY_KEY = "q";

    private static final String GENERATOR_PROPERTY_KEY = "g";

    private final BigInteger _p;

    private final BigInteger _q;

    private final BigInteger _g;

    /**
     * Loads the pre-generated Zp subgroup parameters from a file.
     */
    public ZpSubgroupLoader() {

        Properties config = new Properties();
        try (InputStream stream = getClass().getResourceAsStream(PROPERTIES_FILE_PATH)) {
            config.load(stream);
        } catch (IOException e) {
            throw new IllegalStateException(
                "Failed to load Zp subgroup configuration.", e);
        }

        _p = new BigInteger(config.getProperty(P_PARAMETER_PROPERTY_KEY));
        _q = new BigInteger(config.getProperty(Q_PARAMETER_PROPERTY_KEY));
        _g = new BigInteger(config.getProperty(GENERATOR_PROPERTY_KEY));
    }

    public BigInteger getP() {
        return _p;
    }

    public BigInteger getQ() {
        return _q;
    }

    public BigInteger getG() {
        return _g;
    }
}
