/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.ffc.cryptoapi.FFCPrimes;

/**
 * Factory of {@link FFCPrimes}.
 * <p>
 * Implementation must be thread-safe.
 */
interface FFCPrimesFactory {
    /**
     * Creates a new {@link FFCPrimes} for given prime lengths and a
     * pseudo-random generator.
     * 
     * @param l
     *            the length of P in bits
     * @param n
     *            the length of Q in bits
     * @param random
     *            the generator
     * @return the primes
     * @throws GeneralCryptoLibException
     *             failed to create the primes.
     */
    FFCPrimes newFFCPrimes(int l, int n, PseudoRandom random)
            throws GeneralCryptoLibException;
}
