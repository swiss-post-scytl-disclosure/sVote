/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.securerandom.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;

/**
 * Implementation of the {@link SecureRandomPolicy} interface, which reads
 * values from a properties input.
 * <P>
 * Instances of this class are immutable.
 */
public final class SecureRandomPolicyFromProperties
        implements SecureRandomPolicy {

    private final ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmAndProvider;

    /**
     * Instantiates a new {@code SecureRandomPolicyFromProperties}.
     * 
     * @param path
     *            The path where the properties are read from.
     * @throws CryptoLibException
     *             if the path of properties file or properties names are
     *             invalid.
     */
    public SecureRandomPolicyFromProperties(final String path) {

        try {

            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _secureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider
                    .valueOf(helper.getNotBlankOSDependentPropertyValue(
                        "primitives.securerandom"));

        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {

        return _secureRandomAlgorithmAndProvider;
    }

}
