/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.configuration;

/**
 * Configuration policy for creating a message digest.
 * <P>
 * Implementations must be immutable.
 */
public interface MessageDigestPolicy {

    /**
     * Returns the {@link ConfigMessageDigestAlgorithmAndProvider} that should
     * be used when creating a message digest from some data.
     * 
     * @return The {@link ConfigMessageDigestAlgorithmAndProvider}.
     */
    ConfigMessageDigestAlgorithmAndProvider getMessageDigestAlgorithmAndProvider();

}
