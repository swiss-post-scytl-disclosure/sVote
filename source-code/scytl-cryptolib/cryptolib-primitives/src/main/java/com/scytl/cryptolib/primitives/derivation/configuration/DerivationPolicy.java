/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;

/**
 * An interface which extends derivation policies as well as the
 * {@link SecureRandomPolicy}.
 */
public interface DerivationPolicy extends KDFDerivationPolicy,
        PBKDFDerivationPolicy, SecureRandomPolicy {

}
