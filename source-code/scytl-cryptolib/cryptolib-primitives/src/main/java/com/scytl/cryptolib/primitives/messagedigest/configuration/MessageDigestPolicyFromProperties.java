/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;

/**
 * Implementation of the {@link MessageDigestPolicy} interface, which reads
 * values from a properties input.
 * <P>
 * Instances of this class are immutable.
 */
public class MessageDigestPolicyFromProperties
        implements MessageDigestPolicy {

    private final ConfigMessageDigestAlgorithmAndProvider _messageDigestAlgorithmAndProvider;

    /**
     * Instantiates a provider of properties from the specified path.
     * 
     * @param path
     *            The path where the properties are read from.
     * @throws CryptoLibException
     *             if the path or property key is invalid.
     */
    public MessageDigestPolicyFromProperties(final String path) {

        try {

            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _messageDigestAlgorithmAndProvider =
                ConfigMessageDigestAlgorithmAndProvider.valueOf(helper
                    .getNotBlankPropertyValue("primitives.messagedigest"));

        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigMessageDigestAlgorithmAndProvider getMessageDigestAlgorithmAndProvider() {
        return _messageDigestAlgorithmAndProvider;
    }

}
