/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.factory;

import com.scytl.cryptolib.derivation.cryptoapi.CryptoAPIDerivedKey;

/**
 * Class to encapsulate a derived key.
 */
class CryptoDerivedKey implements CryptoAPIDerivedKey {

    private final byte[] _key;

    /**
     * Creates an instance of derived key initialized by provided {@code key}.
     * 
     * @param key
     *            The derived key to encapsulate, as a byte[].
     */
    public CryptoDerivedKey(final byte[] key) {
        _key = key.clone();
    }

    @Override
    public byte[] getEncoded() {
        return _key.clone();
    }
}
