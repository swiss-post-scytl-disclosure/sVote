/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

/**
 * Provider to obtain small primes up to 2^16.
 * <p>
 * Implementation must be thread-safe.
 */
interface SmallPrimesProvider {
    /**
     * Returns an array of small primes. The returned primes have ascending
     * order, i.e. {@code 2,3,5,7,...}. Client must not modify the content of
     * the returned array.
     * 
     * @return the small primes.
     */
    int[] getSmallPrimes();
}
