/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import java.math.BigInteger;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Factory of {@link PseudoRandom}.
 * <p>
 * Implementation must be thread-safe.
 */
interface PseudoRandomFactory {
    /**
     * Creates a new {@link PseudoRandom} with the specified seed.
     * 
     * @param seed
     *            the seed
     * @return the instance.
     */
    PseudoRandom newPseudoRandom(BigInteger seed);

    /**
     * Creates a new {@link PseudoRandom} with a random seed having the
     * specified number of bits.
     * 
     * @param seedLength
     *            the seed length in bits
     * @return the instance.
     * @throws GeneralCryptoLibException
     *             failed to create instance.
     */
    PseudoRandom newPseudoRandom(int seedLength)
            throws GeneralCryptoLibException;
}
