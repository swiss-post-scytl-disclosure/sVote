/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import java.math.BigInteger;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.ffc.cryptoapi.FFCPrimes;

/**
 * Factory for creating generator.
 * <p>
 * Implementation must be thread-safe.
 */
interface GFactory {
    /**
     * Creates a new generator for given primes and index
     * 
     * @param primes
     *            the primes
     * @param index
     *            the index
     * @return the generator
     * @throws GeneralCryptoLibException
     *             failed to create the generator.
     */
    BigInteger newG(FFCPrimes primes, int index)
            throws GeneralCryptoLibException;
}
