/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.service;

import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.ServiceFactoryHelper;

/**
 * This class sets up default factories of {@link PrimitivesServiceAPI} objects.
 */
public final class PrimitivesServiceFactoryHelper {

    /**
     * This is a helper class. It cannot be instantiated.
     */
    private PrimitivesServiceFactoryHelper() {
    }

    /**
     * Retrieves a new factory of non thread-safe services.
     * <p>
     * Default factory is {@link BasicPrimitivesServiceFactory}.
     * 
     * @param params
     *            List of parameters used in the creation of the default
     *            factory.
     * @return The new factory.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<PrimitivesServiceAPI> getInstance(
            final Object... params) {
        return ServiceFactoryHelper.get(
            BasicPrimitivesServiceFactory.class, params);

    }

    /**
     * Retrieves a new factory of thread-safe services.
     * <p>
     * Default factory is {link {@link PollingPrimitivesServiceFactory}
     * 
     * @param params
     *            List of parameters used in the creation of the default
     *            factory.
     * @return The new factory of thread-safe services.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<PrimitivesServiceAPI> getFactoryOfThreadSafeServices(
            final Object... params) {
        return ServiceFactoryHelper.get(
            PollingPrimitivesServiceFactory.class, params);
    }

}
