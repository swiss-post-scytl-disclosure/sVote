/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.primitives.derivation.constants.DerivationConstants;
import com.scytl.cryptolib.primitives.messagedigest.configuration.HashAlgorithm;

/**
 * Enum which defines the password derivation algorithm and its parameters.
 * <p>
 * Each element of the enum contains the following attributes:
 * <ol>
 * <li>An algorithm.</li>
 * <li>A {@link Provider}.</li>
 * <li>A number of iterations.</li>
 * <li>A {@link HashAlgorithm}.</li>
 * <li>The length in bits of the salt.</li>
 * <li>The length in bits of the expected output.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigPBKDFDerivationParameters {

    PBKDF2_1_SHA256_256_SUNJCE_KL128(
            DerivationConstants.PBKDF2_HMAC_SHA256, Provider.SUN_JCE,
            1, HashAlgorithm.SHA256, 256, 128),

    PBKDF2_32000_SHA256_256_SUNJCE_KL128(
            DerivationConstants.PBKDF2_HMAC_SHA256, Provider.SUN_JCE,
            32000, HashAlgorithm.SHA256, 256, 128),
    
    PBKDF2_1_SHA256_256_BC_KL128(
        DerivationConstants.PBKDF2_HMAC_SHA256, Provider.BOUNCY_CASTLE,
        1, HashAlgorithm.SHA256, 256, 128),

    PBKDF2_32000_SHA256_256_BC_KL128(
        DerivationConstants.PBKDF2_HMAC_SHA256, Provider.BOUNCY_CASTLE,
        32000, HashAlgorithm.SHA256, 256, 128);

    private final String _algorithm;

    private final Provider _provider;

    private final int _iterations;

    private final String _hashAlgorithm;

    private final int _saltBitLength;

    private final int _keyBitLength;

    ConfigPBKDFDerivationParameters(final String algorithm,
            final Provider provider, final int iterations,
            final HashAlgorithm hashAlgorithm, final int saltBitLength,
            final int keyBitLength) {

        _algorithm = algorithm;
        _provider = provider;
        _iterations = iterations;
        _hashAlgorithm = hashAlgorithm.getAlgorithm();
        _saltBitLength = saltBitLength;
        _keyBitLength = keyBitLength;
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public Provider getProvider() {
        return _provider;
    }

    public int getIterations() {
        return _iterations;
    }

    public String getHashAlgorithm() {
        return _hashAlgorithm;
    }

    public int getSaltBitLength() {
        return _saltBitLength;
    }

    public int getKeyBitLength() {
        return _keyBitLength;
    }
}
