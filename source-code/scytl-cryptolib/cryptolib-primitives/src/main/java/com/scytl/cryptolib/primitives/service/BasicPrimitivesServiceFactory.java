/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.service;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link PrimitivesService} objects which are non thread-safe.
 */
class BasicPrimitivesServiceFactory extends
        PropertiesConfiguredFactory<PrimitivesServiceAPI> implements
        ServiceFactory<PrimitivesServiceAPI> {
    /**
     * This factory will create services configured with default values.
     */
    public BasicPrimitivesServiceFactory() {
        super(PrimitivesServiceAPI.class);
    }

    /**
     * This factory will create services configured with the given configuration
     * file.
     * 
     * @param path
     *            The path of the properties file to be used to configure the
     *            service.
     */
    public BasicPrimitivesServiceFactory(final String path) {
        super(PrimitivesServiceAPI.class, path);
    }

    @Override
    protected PrimitivesServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new PrimitivesService(_path);
    }

    @Override
    protected PrimitivesServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new PrimitivesService();
    }
}
