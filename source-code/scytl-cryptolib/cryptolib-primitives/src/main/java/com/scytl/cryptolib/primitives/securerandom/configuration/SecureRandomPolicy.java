/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.securerandom.configuration;

/**
 * Configuration policy for creating instances of generators
 * {@link java.security.SecureRandom}.
 * <P>
 * Implementations must be immutable.
 */
public interface SecureRandomPolicy {

    /**
     * Returns the {@link ConfigSecureRandomAlgorithmAndProvider} that should be
     * used when getting instances of {@link java.security.SecureRandom}.
     * 
     * @return The {@link ConfigSecureRandomAlgorithmAndProvider}.
     */
    ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider();
}
