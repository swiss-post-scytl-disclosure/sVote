/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.configuration;

/**
 * Enum which defines the key derivation function hash algorithm.
 */
public enum HashAlgorithm {

    SHA256("SHA-256"),

    /**
     * This option is only available for the BouncyCastle cryptographic service
     * provider.
     */
    SHA512_224("SHA-512/224");

    private final String _algorithm;

    HashAlgorithm(final String algorithm) {
        _algorithm = algorithm;
    }

    public String getAlgorithm() {
        return _algorithm;
    }
}
