/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import java.math.BigInteger;

/**
 * Pseudo-random generator.
 * <p>
 * Typical use of PseudoRandom looks like the following <pre>
 * <code>
 * PseudoRandomFactory randomFactory = ...
 * BigInteger initialSeed = ...
 * PseudoRandom random = randomFactory.newPseudoRandom(initialSeed);
 * BigInteger foo = random.next();
 * BigInteger bar = random.next();
 * BigInteger finalSeed = random.seed();
 * </code> </pre>
 */
interface PseudoRandom {
    /**
     * Returns the next pseudo-random value. This operation changes the seed.
     * The returned number is nonnegative.
     * 
     * @return the next pseudo-random value.
     */
    BigInteger next();

    /**
     * Returns the next pseudo-random value for a given preferred length.
     * <p>
     * Preferred here means that the returned value does not always have the
     * specified length. The actual length can be greater or less than the
     * specified one. It is only guaranteed that the lowest {@code length} bits
     * of the returned value are distributed more or less uniformly.
     * <p>
     * This operation changes the seed. The returned number is nonnegative.
     * 
     * @param length
     *            the preferred length in bits
     * @return the next pseudo-random value.
     */
    BigInteger next(int length);

    /**
     * Returns the current value of the seed. This operation does not changes
     * the seed.
     * 
     * @return the current value of the seed.
     */
    BigInteger seed();
}
