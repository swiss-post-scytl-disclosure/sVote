/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum representing a set of parameters that can be used when requesting a
 * message digest generator.
 * <P>
 * These parameters are:
 * <ol>
 * <li>An algorithm.</li>
 * <li>A provider.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigMessageDigestAlgorithmAndProvider {

    SHA256_SUN(HashAlgorithm.SHA256, Provider.SUN),

    SHA256_BC(HashAlgorithm.SHA256, Provider.BOUNCY_CASTLE),

    SHA256_DEFAULT(HashAlgorithm.SHA256, Provider.DEFAULT),

    SHA512_224_BC(HashAlgorithm.SHA512_224, Provider.BOUNCY_CASTLE),

    SHA512_224_DEFAULT(HashAlgorithm.SHA512_224, Provider.DEFAULT);

    private final HashAlgorithm _algorithm;

    private final Provider _provider;

    private ConfigMessageDigestAlgorithmAndProvider(
            final HashAlgorithm algorithm, final Provider provider) {
        _algorithm = algorithm;
        _provider = provider;
    }

    /**
     * Returns the configured algorithm.
     * 
     * @return The algorithm.
     */
    public String getAlgorithm() {
        return _algorithm.getAlgorithm();
    }

    /**
     * Returns the configured provider.
     * 
     * @return The provider.
     */
    public Provider getProvider() {
        return _provider;
    }
}
