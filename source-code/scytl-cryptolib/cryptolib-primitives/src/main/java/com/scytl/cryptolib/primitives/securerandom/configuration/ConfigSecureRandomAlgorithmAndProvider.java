/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.securerandom.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;

/**
 * Enum representing a set of parameters that can be used when requesting an
 * instance of SecureRandom.
 * <P>
 * These parameters are:
 * <ol>
 * <li>An algorithm.</li>
 * <li>A {@link Provider}.</li>
 * <li>A list of operating systems that support the algorithm and provider.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigSecureRandomAlgorithmAndProvider {

    NATIVE_PRNG_SUN("NativePRNG", Provider.SUN, OperatingSystem.LINUX),

    PRNG_SUN_MSCAPI("Windows-PRNG", Provider.SUN_MSCAPI,
            OperatingSystem.WINDOWS);

    private final String _algorithm;

    private final Provider _provider;

    private final OperatingSystem[] _operatingSystems;

    ConfigSecureRandomAlgorithmAndProvider(final String algorithm,
            final Provider provider,
            final OperatingSystem... operatingSystems) {
        _algorithm = algorithm;
        _provider = provider;
        _operatingSystems = operatingSystems;
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public Provider getProvider() {
        return _provider;
    }

    /**
     * Checks if the algorithm and provider are OS-compliant.
     * 
     * @param operatingSystem
     *            The current operation system.
     * @return {@code true} if the algorithm and provider are OS-compliant.
     */
    public boolean isOSCompliant(final OperatingSystem operatingSystem) {

        for (OperatingSystem oS : _operatingSystems) {
            if (operatingSystem == oS) {
                return true;
            }
        }

        return false;
    }

}
