/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

import static com.scytl.cryptolib.primitives.derivation.constants.DerivationConstants.MGF1;

import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.primitives.messagedigest.configuration.HashAlgorithm;

/**
 * Enum which defines the key derivation function parameters.
 * <p>
 * Each element of the enum contains the following attributes:
 * <ol>
 * <li>An algorithm.</li>
 * <li>A {@link HashAlgorithm}.</li>
 * <li>A {@link Provider}.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigKDFDerivationParameters {

    MGF1_SHA256_BC(MGF1, HashAlgorithm.SHA256, Provider.BOUNCY_CASTLE),

    MGF1_SHA256_DEFAULT(MGF1, HashAlgorithm.SHA256, Provider.DEFAULT);

    private final String _algorithm;

    private final String _hashAlgorithm;

    private final Provider _provider;

    private ConfigKDFDerivationParameters(final String algorithm,
            final HashAlgorithm hashAlgorithm, final Provider provider) {

        _algorithm = algorithm;
        _hashAlgorithm = hashAlgorithm.getAlgorithm();
        _provider = provider;
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public String getHashAlgorithm() {
        return _hashAlgorithm;
    }

    public Provider getProvider() {
        return _provider;
    }
}
