/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.ffc.cryptoapi.FFCPrime;

/**
 * Factory of {@link FFCPrime}.
 * <p>
 * Implementation must be thread-safe.
 */
interface FFCPrimeFactory {
    /**
     * Creates a new {@link FFCPrime} for a given length and a pseudo-random
     * generator.
     * 
     * @param length
     *            the length in bits
     * @param random
     *            the generator
     * @return the prime
     * @throws GeneralCryptoLibException
     *             failed to create the prime.
     */
    FFCPrime newFFCPrime(int length, PseudoRandom random)
            throws GeneralCryptoLibException;
}
