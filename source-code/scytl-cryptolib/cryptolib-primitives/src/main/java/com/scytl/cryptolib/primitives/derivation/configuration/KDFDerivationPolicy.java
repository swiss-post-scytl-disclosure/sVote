/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

/**
 * Interface which specifies the methods that should be implemented by a
 * specific {@link KDFDerivationPolicy}.
 */
public interface KDFDerivationPolicy {

    /**
     * Returns the key derivation function parameters.
     * 
     * @return the key derivation function parameters.
     */
    ConfigKDFDerivationParameters getKDFDerivationParameters();

}
