/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.factory;

import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicy;

/**
 * A factory class for creating a message digest generator and verifier.
 */
public class MessageDigestFactory {

    private final MessageDigestPolicy _messageDigestPolicy;

    /**
     * Constructs a MessageDigestFactory using the provided
     * {@link MessageDigestPolicy}.
     * 
     * @param messageDigestPolicy
     *            The MessageDigestPolicy to be used to configure this
     *            MessageDigestFactory.
     *            <P>
     *            NOTE: The received {@link MessageDigestPolicy} should be an
     *            immutable object. If this is the case, then the entire class
     *            is thread safe.
     */
    public MessageDigestFactory(
            final MessageDigestPolicy messageDigestPolicy) {

        _messageDigestPolicy = messageDigestPolicy;
    }

    /**
     * Create a {@link CryptoMessageDigest} according to the given policy.
     * 
     * @return A {@link CryptoMessageDigest} object.
     */
    public CryptoMessageDigest create() {

        return new CryptoMessageDigest(_messageDigestPolicy);
    }
}
