/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

/**
 * Interface which specifies the methods that should be implemented by a
 * specific {@link PBKDFDerivationPolicy}.
 */
public interface PBKDFDerivationPolicy {

    /**
     * Returns the password derivation algorithm and its parameters.
     * 
     * @return The password derivation algorithm and its parameters.
     */
    ConfigPBKDFDerivationParameters getPBKDFDerivationParameters();

}
