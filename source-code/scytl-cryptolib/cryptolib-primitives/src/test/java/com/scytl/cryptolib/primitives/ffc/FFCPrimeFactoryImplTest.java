/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import static com.scytl.cryptolib.primitives.ffc.FFCData.N;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.ffc.cryptoapi.FFCPrime;

/**
 * Tests of {@link FFCPrimeFactoryImpl}.
 */
public class FFCPrimeFactoryImplTest {
    private PseudoRandom random;

    private FFCPrimeFactoryImpl factory;

    @Before
    public void setUp() {
        random = FakePseudoRandom.newInstance(N);
        SmallPrimesProvider provider =
            SmallPrimesProviderImpl.getInstance();
        factory = new FFCPrimeFactoryImpl(provider);
    }

    @Test
    public void testNewFFCPrime() throws GeneralCryptoLibException {
        FFCPrime prime = factory.newFFCPrime(N, random);
        assertTrue(prime.value().isProbablePrime(80));
        assertEquals(N, prime.value().bitLength());
        assertEquals(random.seed(), prime.seed());
    }
}
