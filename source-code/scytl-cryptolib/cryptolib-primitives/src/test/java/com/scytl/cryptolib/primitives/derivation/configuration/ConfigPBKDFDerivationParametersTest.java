/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.primitives.derivation.constants.DerivationConstants;
import com.scytl.cryptolib.primitives.messagedigest.configuration.HashAlgorithm;

public class ConfigPBKDFDerivationParametersTest {

    public static final int KEY_BITS_LENGTH_128 = 128;

    public static final int ITERATIONS_32000 = 32000;

    @Test
    public void whenGivenPBKBF2WithSunJCEProviderAndHashFunctionKL128() {

        assertEquals(
            DerivationConstants.PBKDF2_HMAC_SHA256,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_SUNJCE_KL128
                .getAlgorithm());

        assertEquals(
            Provider.SUN_JCE,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_SUNJCE_KL128
                .getProvider());

        assertEquals(
            ITERATIONS_32000,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_SUNJCE_KL128
                .getIterations());

        assertEquals(
            HashAlgorithm.SHA256.getAlgorithm(),
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_SUNJCE_KL128
                .getHashAlgorithm());

        assertEquals(
            KEY_BITS_LENGTH_128,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_SUNJCE_KL128
                .getKeyBitLength());
    }
    
    @Test
    public void whenGivenPBKBF2WithBCProviderAndHashFunctionKL128() {

        assertEquals(
            DerivationConstants.PBKDF2_HMAC_SHA256,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_BC_KL128
                .getAlgorithm());

        assertEquals(
            Provider.BOUNCY_CASTLE,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_BC_KL128
                .getProvider());

        assertEquals(
            ITERATIONS_32000,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_BC_KL128
                .getIterations());

        assertEquals(
            HashAlgorithm.SHA256.getAlgorithm(),
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_BC_KL128
                .getHashAlgorithm());

        assertEquals(
            KEY_BITS_LENGTH_128,
            ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_BC_KL128
                .getKeyBitLength());
    }
}
