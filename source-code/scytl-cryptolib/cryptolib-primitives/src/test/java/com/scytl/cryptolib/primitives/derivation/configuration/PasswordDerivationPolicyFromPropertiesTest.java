/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

public class PasswordDerivationPolicyFromPropertiesTest {

    private static DerivationPolicyFromProperties
            _passwordDerivationPolicyFromFile;

    @BeforeClass
    public static void init() throws GeneralCryptoLibException {
        _passwordDerivationPolicyFromFile =
                new DerivationPolicyFromProperties(Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);

    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePasswordDerivationPolicyPassingAnIncorrectPath() {

        new DerivationPolicyFromProperties("somePath");

    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePasswordDerivationPolicyPassingAFileWithInvalidLabels()
            throws GeneralCryptoLibException {
        new DerivationPolicyFromProperties(
                "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePasswordDerivationPolicyPassingAFileWithBlankLabels()
            throws GeneralCryptoLibException {
        new DerivationPolicyFromProperties(
                "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Test
    public void whenGetPasswordDerivationPolicyFromLinux() {

        Assume.assumeTrue(OperatingSystem.LINUX.isCurrent());

        assertEquals(_passwordDerivationPolicyFromFile.getPBKDFDerivationParameters(),
                getPasswordDerivationPolicyForLinux().getPBKDFDerivationParameters()
        );

        assertEquals(_passwordDerivationPolicyFromFile.getSecureRandomAlgorithmAndProvider(),
                getPasswordDerivationPolicyForLinux().getSecureRandomAlgorithmAndProvider()
        );

    }

    @Test
    public void whenGetPasswordDerivationPolicyFromWindows() {

        Assume.assumeTrue(OperatingSystem.WINDOWS.isCurrent());

        assertEquals(getPasswordDerivationPolicyForWindows().getPBKDFDerivationParameters(),
                _passwordDerivationPolicyFromFile.getPBKDFDerivationParameters()
        );

        assertEquals(getPasswordDerivationPolicyForWindows().getSecureRandomAlgorithmAndProvider(),
                _passwordDerivationPolicyFromFile.getSecureRandomAlgorithmAndProvider()
        );

    }

    private DerivationPolicy getPasswordDerivationPolicyForLinux() {
        return new DerivationPolicy() {

            @Override
            public ConfigKDFDerivationParameters getKDFDerivationParameters() {
                return ConfigKDFDerivationParameters.MGF1_SHA256_DEFAULT;
            }

            @Override
            public ConfigPBKDFDerivationParameters getPBKDFDerivationParameters() {
                return ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_BC_KL128;
            }

            @Override
            public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
                return ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN;
            }
        };
    }

    private DerivationPolicy getPasswordDerivationPolicyForWindows() {
        return new DerivationPolicy() {

            @Override
            public ConfigKDFDerivationParameters getKDFDerivationParameters() {
                return ConfigKDFDerivationParameters.MGF1_SHA256_DEFAULT;
            }

            @Override
            public ConfigPBKDFDerivationParameters getPBKDFDerivationParameters() {
                return ConfigPBKDFDerivationParameters.PBKDF2_32000_SHA256_256_BC_KL128;
            }

            @Override
            public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
                return ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI;
            }
        };
    }

}
