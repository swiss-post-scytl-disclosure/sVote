/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.configuration;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

public class MessageDigestPolicyFromPropertiesTest {

    private final String INVALID_PATH = "test path";

    @Test(expected = CryptoLibException.class)
    public void whenCreateMessageDigestPolicyPassingAnIncorrectPath() {

        new MessageDigestPolicyFromProperties(INVALID_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateMessageDigestPolicyPassingAFileWithInvalidLabels() {

        new MessageDigestPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateMessageDigestPolicyPassingAFileWithBlankLabels()
            throws GeneralCryptoLibException {

        new MessageDigestPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

}
