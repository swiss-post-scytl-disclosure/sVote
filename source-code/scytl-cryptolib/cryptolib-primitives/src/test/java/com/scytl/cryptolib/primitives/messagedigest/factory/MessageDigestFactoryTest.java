/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.factory;

import static mockit.Deencapsulation.getField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicy;

public class MessageDigestFactoryTest {

    @BeforeClass
    public static void setUp() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @AfterClass
    public static void removeBouncyCastleProvider() {
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Test
    public void whenCreateMessageDigestFactoryUsingPolicySha256AndSun() {

        MessageDigestFactory messageDigestFactoryByPolicy =
            new MessageDigestFactory(getMessageDigestPolicySha256AndSun());

        MessageDigestPolicy expectedMessageDigestPolicy =
            getField(messageDigestFactoryByPolicy, "_messageDigestPolicy");

        assertEquals(getMessageDigestPolicySha256AndSun()
            .getMessageDigestAlgorithmAndProvider(),
            expectedMessageDigestPolicy
                .getMessageDigestAlgorithmAndProvider());
    }

    @Test
    public void whenCreateMessageDigestFactoryUsingPolicySha256AndBc() {

        MessageDigestFactory messageDigestFactoryByPolicy =
            new MessageDigestFactory(getMessageDigestPolicySha256AndBc());

        MessageDigestPolicy expectedMessageDigestPolicy =
            getField(messageDigestFactoryByPolicy, "_messageDigestPolicy");

        assertEquals(getMessageDigestPolicySha256AndBc()
            .getMessageDigestAlgorithmAndProvider(),
            expectedMessageDigestPolicy
                .getMessageDigestAlgorithmAndProvider());
    }

    @Test
    public void whenCreateMessageDigestGeneratorUsingPolicySha256AndSun()
            throws NoSuchAlgorithmException, NoSuchProviderException {

        MessageDigestFactory messageDigestFactory =
            new MessageDigestFactory(getMessageDigestPolicySha256AndSun());

        CryptoMessageDigest cryptoMessageDigest =
            messageDigestFactory.create();

        assertNotNull(cryptoMessageDigest);
    }

    @Test
    public void whenCreateMessageDigestGeneratorUsingPolicySha256AndBc()
            throws NoSuchAlgorithmException, NoSuchProviderException {

        MessageDigestFactory messageDigestFactory =
            new MessageDigestFactory(getMessageDigestPolicySha256AndBc());

        CryptoMessageDigest cryptoMessageDigest =
            messageDigestFactory.create();

        assertNotNull(cryptoMessageDigest);
    }

    private MessageDigestPolicy getMessageDigestPolicySha256AndSun() {
        return new MessageDigestPolicy() {

            @Override
            public ConfigMessageDigestAlgorithmAndProvider getMessageDigestAlgorithmAndProvider() {
                return ConfigMessageDigestAlgorithmAndProvider.SHA256_SUN;
            }
        };
    }

    private MessageDigestPolicy getMessageDigestPolicySha256AndBc() {
        return new MessageDigestPolicy() {

            @Override
            public ConfigMessageDigestAlgorithmAndProvider getMessageDigestAlgorithmAndProvider() {
                return ConfigMessageDigestAlgorithmAndProvider.SHA256_BC;
            }
        };
    }
}
