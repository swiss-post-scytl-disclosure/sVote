/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicy;
import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicyFromProperties;
import com.scytl.cryptolib.primitives.messagedigest.factory.MessageDigestFactory;
import com.scytl.cryptolib.securerandom.cryptoapi.CryptoAPIRandomBytes;

/**
 * Tests of {@link PseudoRandomFactoryImpl}.
 */
public class PseudoRandomFactoryImplTest {
    private static final BigInteger TWO = BigInteger.valueOf(2);

    private static final int LENGTH = 2;

    private PseudoRandomFactoryImpl factory;

    @Before
    public void setUp() throws GeneralCryptoLibException {
        MessageDigestPolicy policy = new MessageDigestPolicyFromProperties(
            Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
        MessageDigestFactory digestFactory =
            new MessageDigestFactory(policy);
        CryptoAPIRandomBytes random = mock(CryptoAPIRandomBytes.class);
        when(random.nextRandom(1)).thenReturn(
            BigInteger.ZERO.toByteArray(), BigInteger.ONE.toByteArray(),
            TWO.toByteArray());
        factory = new PseudoRandomFactoryImpl(digestFactory, random);
    }

    @Test
    public void testNewPseudoRandomBigInteger() {
        PseudoRandom random = factory.newPseudoRandom(TWO);
        assertEquals(TWO, random.seed());
    }

    @Test
    public void testNewPseudoRandomInt() throws GeneralCryptoLibException {
        PseudoRandom random = factory.newPseudoRandom(LENGTH);
        assertEquals(TWO, random.seed());
    }

}
