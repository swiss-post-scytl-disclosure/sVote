/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.primes.bean;

import java.util.Arrays;

import com.scytl.cryptolib.derivation.cryptoapi.CryptoAPIDerivedKey;

/**
 * Implementation of interface
 * {@link com.scytl.cryptolib.derivation.cryptoapi.CryptoAPIDerivedKey} to be
 * used for testing purposes.
 */
public class TestCryptoDerivedKey implements CryptoAPIDerivedKey {

    private byte[] _keyBytes;

    public TestCryptoDerivedKey(final byte[] keyBytes) {

        _keyBytes = keyBytes;
    }

    @Override
    public byte[] getEncoded() {

        return _keyBytes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TestCryptoDerivedKey other = (TestCryptoDerivedKey) obj;
        if (!Arrays.equals(_keyBytes, other._keyBytes)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(_keyBytes);
        return result;
    }
}
