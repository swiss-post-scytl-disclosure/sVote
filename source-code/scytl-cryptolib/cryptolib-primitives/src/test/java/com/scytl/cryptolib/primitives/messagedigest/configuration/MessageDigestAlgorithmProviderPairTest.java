/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.messagedigest.configuration;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;

public class MessageDigestAlgorithmProviderPairTest {

    @Test
    public void givenAlgorithmSha256WhenPairIsSha256AndSun() {

        assertAlgorithm("SHA-256",
            ConfigMessageDigestAlgorithmAndProvider.SHA256_SUN);
    }

    @Test
    public void givenAlgorithmSha256WhenPairIsSha256AndBc() {

        assertAlgorithm("SHA-256",
            ConfigMessageDigestAlgorithmAndProvider.SHA256_BC);
    }

    @Test
    public void givenAlgorithmSha512With224WhenPairIsSha512With224AndBc() {

        assertAlgorithm("SHA-512/224",
            ConfigMessageDigestAlgorithmAndProvider.SHA512_224_BC);
    }

    @Test
    public void givenProviderSunWhenPairIsSha256AndSun() {

        assertProvider(Provider.SUN,
            ConfigMessageDigestAlgorithmAndProvider.SHA256_SUN);
    }

    @Test
    public void givenProviderBcWhenPairIsSha256AndBc() {

        assertProvider(Provider.BOUNCY_CASTLE,
            ConfigMessageDigestAlgorithmAndProvider.SHA256_BC);
    }

    @Test
    public void givenProviderBcWhenPairIsSha512With224AndBc() {

        assertProvider(Provider.BOUNCY_CASTLE,
            ConfigMessageDigestAlgorithmAndProvider.SHA512_224_BC);
    }

    private void assertAlgorithm(
            final String expectedAlgorithm,
            final ConfigMessageDigestAlgorithmAndProvider MessageDigestAlgorithmAndProvider) {

        Assert.assertEquals("Algorithm name was not the expected value",
            expectedAlgorithm,
            MessageDigestAlgorithmAndProvider.getAlgorithm());
    }

    private void assertProvider(
            final Provider expectedProvider,
            final ConfigMessageDigestAlgorithmAndProvider MessageDigestAlgorithmAndProvider) {

        Assert.assertEquals("Provider was not the expected value",
            expectedProvider,
            MessageDigestAlgorithmAndProvider.getProvider());
    }
}
