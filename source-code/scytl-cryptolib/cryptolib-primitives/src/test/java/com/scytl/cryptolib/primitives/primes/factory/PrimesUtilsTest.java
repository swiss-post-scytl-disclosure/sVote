/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.primes.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.List;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.primitives.primes.utils.PrimesUtils;

public class PrimesUtilsTest {

    @Test
    public void whenCheckIfAnIntegerIsProbablePrime() {

        assertTrue(PrimesUtils.isProbablePrime(2));
        assertTrue(PrimesUtils.isProbablePrime(3));
        assertTrue(PrimesUtils.isProbablePrime(17));
        assertTrue(PrimesUtils.isProbablePrime(1009));
        assertTrue(PrimesUtils.isProbablePrime(3067));
        assertTrue(PrimesUtils.isProbablePrime(4813));
        assertTrue(PrimesUtils.isProbablePrime(6217));

        assertFalse(PrimesUtils.isProbablePrime(4));
        assertFalse(PrimesUtils.isProbablePrime(10));
        assertFalse(PrimesUtils.isProbablePrime(100));
        assertFalse(PrimesUtils.isProbablePrime(99999));
        assertFalse(PrimesUtils.isProbablePrime(99999999));

    }

    @Test
    public void whenCheckIfABigIntegerIsProbablePrime() {

        assertTrue(PrimesUtils.isProbablePrime(BigInteger.valueOf(2)));
        assertTrue(PrimesUtils.isProbablePrime(BigInteger.valueOf(3)));
        assertTrue(PrimesUtils.isProbablePrime(BigInteger.valueOf(17)));
        assertTrue(PrimesUtils.isProbablePrime(BigInteger.valueOf(1009)));
        assertTrue(PrimesUtils.isProbablePrime(BigInteger.valueOf(3067)));
        assertTrue(PrimesUtils.isProbablePrime(BigInteger.valueOf(4813)));
        assertTrue(PrimesUtils.isProbablePrime(BigInteger.valueOf(6217)));

        assertFalse(PrimesUtils.isProbablePrime(BigInteger.valueOf(4)));
        assertFalse(PrimesUtils.isProbablePrime(BigInteger.valueOf(10)));
        assertFalse(PrimesUtils.isProbablePrime(BigInteger.valueOf(100)));
        assertFalse(PrimesUtils.isProbablePrime(BigInteger.valueOf(99999)));
        assertFalse(PrimesUtils.isProbablePrime(BigInteger
            .valueOf(99999999)));

    }

    @Test
    public void whenCheckIfOrderedPrimeListOnlyContainsPrimes() {

        List<BigInteger> listBigInteger = PrimesUtils.getPrimesList();

        for (BigInteger bigInt : listBigInteger) {
            assertTrue(PrimesUtils.isProbablePrime(bigInt));
        }

    }

    @Test
    public void whenGetNumberOfPrimesFromFile() {

        List<BigInteger> listBigInteger = PrimesUtils.getPrimesList(10);

        assertEquals(10, listBigInteger.size());
    }

    @Test
    public void whenGetAllPrimesListFromFile() {

        List<BigInteger> listBigInteger = PrimesUtils.getPrimesList();

        assertEquals(10000, listBigInteger.size());
    }

    @Test(expected = CryptoLibException.class)
    public void whenGetTooManyPrimesListFromFile() {

        PrimesUtils.getPrimesList(20000);
    }

}
