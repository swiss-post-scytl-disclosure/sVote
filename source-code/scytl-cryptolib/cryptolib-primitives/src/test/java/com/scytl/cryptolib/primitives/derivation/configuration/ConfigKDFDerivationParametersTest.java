/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.derivation.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.primitives.derivation.constants.DerivationConstants;
import com.scytl.cryptolib.primitives.messagedigest.configuration.HashAlgorithm;

public class ConfigKDFDerivationParametersTest {

    @Test
    public void whenGivenMGF1WithBCProviderAndHashFunctionSha256() {

        assertEquals(DerivationConstants.MGF1,
            ConfigKDFDerivationParameters.MGF1_SHA256_BC.getAlgorithm());

        assertEquals(HashAlgorithm.SHA256.getAlgorithm(),
            ConfigKDFDerivationParameters.MGF1_SHA256_BC
                .getHashAlgorithm());

        assertEquals(Provider.BOUNCY_CASTLE,
            ConfigKDFDerivationParameters.MGF1_SHA256_BC.getProvider());
    }
}
