/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import static com.scytl.math.BigIntegers.modPow;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.ffc.cryptoapi.FFCPrime;
import com.scytl.cryptolib.ffc.cryptoapi.FFCPrimes;
import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicy;
import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicyFromProperties;
import com.scytl.cryptolib.primitives.messagedigest.factory.MessageDigestFactory;

/**
 * Tests of {@link GFactoryImpl}.
 */
public class GFactoryImplTest {
    private GFactoryImpl gFactory;

    @Before
    public void setUp() {
        MessageDigestPolicy policy = new MessageDigestPolicyFromProperties(
            Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
        MessageDigestFactory digestFactory =
            new MessageDigestFactory(policy);
        gFactory = new GFactoryImpl(digestFactory);
    }

    @Test
    public void testNewG() throws GeneralCryptoLibException {
        FFCPrime p = new FFCPrime(FFCData.P, BigInteger.ZERO, 1);
        FFCPrime q = new FFCPrime(FFCData.Q, BigInteger.ONE, 1);
        FFCPrimes primes = new FFCPrimes(p, q, BigInteger.valueOf(2));
        BigInteger g = gFactory.newG(primes, 1);
        assertTrue(g.compareTo(p.value()) < 0);
        BigInteger e =
            p.value().subtract(BigInteger.ONE).divide(q.value());
        assertTrue(BigInteger.ONE.compareTo(modPow(g, e, p.value())) < 0);
        assertEquals(BigInteger.ONE, modPow(g, q.value(), p.value()));
    }
}
