/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.primitives.messagedigest.factory.CryptoMessageDigest;

/**
 * Tests of {@link PseudoRandomImpl}.
 */
public class PseudoRandomImplTest {
    private PseudoRandomImpl random;

    @Before
    public void setUp() {
        CryptoMessageDigest digest = mock(CryptoMessageDigest.class);
        when(digest.getDigestLength()).thenReturn(1);
        when(digest.generate(any(byte[].class)))
            .thenReturn(new byte[] {0 }, new byte[] {1 }, new byte[] {2 });
        random = new PseudoRandomImpl(digest, BigInteger.ZERO);
    }

    @Test
    public void testNext() {
        assertEquals(new BigInteger(1, new byte[] {0 }), random.next());
        assertEquals(BigInteger.ONE, random.seed());
        assertEquals(new BigInteger(1, new byte[] {1 }), random.next());
        assertEquals(BigInteger.valueOf(2), random.seed());
        assertEquals(new BigInteger(1, new byte[] {2 }), random.next());
        assertEquals(BigInteger.valueOf(3), random.seed());
    }

    @Test
    public void testNextInt() {
        assertEquals(new BigInteger(1, new byte[] {2, 1, 0 }),
            random.next(20));
        assertEquals(BigInteger.valueOf(3), random.seed());
    }

    @Test
    public void testSeed() {
        assertEquals(BigInteger.ZERO, random.seed());
    }
}
