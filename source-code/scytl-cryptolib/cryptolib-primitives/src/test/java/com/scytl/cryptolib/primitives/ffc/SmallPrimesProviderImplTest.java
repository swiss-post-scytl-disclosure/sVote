/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.ffc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests of {@link SmallPrimesProviderImpl}.
 */
public class SmallPrimesProviderImplTest {

    @Test
    public void testGetSmallPrimes() {
        SmallPrimesProviderImpl provider =
            SmallPrimesProviderImpl.getInstance();
        int[] primes = provider.getSmallPrimes();
        assertEquals(6542, primes.length);
        assertEquals(2, primes[0]);
        assertEquals(65521, primes[primes.length - 1]);
    }
}
