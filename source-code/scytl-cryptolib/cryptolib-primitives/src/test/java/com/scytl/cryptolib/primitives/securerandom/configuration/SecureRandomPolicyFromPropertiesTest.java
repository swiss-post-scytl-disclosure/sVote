/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.securerandom.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;

import mockit.Deencapsulation;

public class SecureRandomPolicyFromPropertiesTest {

    private static OperatingSystem _os;

    private SecureRandomPolicyFromProperties _secureRandomPolicyFromFile;

    @BeforeClass
    public static void setUp() {
        _os = OperatingSystem.current();
    }

    @Before
    public void setup() throws GeneralCryptoLibException {
        _secureRandomPolicyFromFile =
            new SecureRandomPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @After
    public void revertSystemHelper() {
        Deencapsulation.setField(OperatingSystem.class, "current",
            _os);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateSecureRandomPolicyPassingAnIncorrectPath() {

        new SecureRandomPolicyFromProperties("somePath");

    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateSecureRandomPolicyPassingAFileWithInvalidLabels() {
        new SecureRandomPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateSecureRandomPolicyPassingAFileWithBlankLabels()
            throws GeneralCryptoLibException {
        new SecureRandomPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Test
    public void whenGetSecureRandomPolicyFromLinux() {

        Assume.assumeTrue(OperatingSystem.LINUX.isCurrent());

        assertEquals(getSecureRandomPolicyNativePrngSun()
            .getSecureRandomAlgorithmAndProvider(),
            _secureRandomPolicyFromFile
                .getSecureRandomAlgorithmAndProvider());
    }

    @Test
    public void whenGetSecureRandomPolicyFromWindows() {

        Assume.assumeTrue(OperatingSystem.WINDOWS.isCurrent());

        assertEquals(getSecureRandomPolicyWindowsPrngSunMSCAPI()
            .getSecureRandomAlgorithmAndProvider(),
            _secureRandomPolicyFromFile
                .getSecureRandomAlgorithmAndProvider());
    }

    private SecureRandomPolicy getSecureRandomPolicyNativePrngSun() {
        return new SecureRandomPolicy() {

            @Override
            public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
                return ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN;
            }
        };
    }

    private SecureRandomPolicy getSecureRandomPolicyWindowsPrngSunMSCAPI() {
        return new SecureRandomPolicy() {

            @Override
            public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
                return ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI;
            }
        };
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

}
