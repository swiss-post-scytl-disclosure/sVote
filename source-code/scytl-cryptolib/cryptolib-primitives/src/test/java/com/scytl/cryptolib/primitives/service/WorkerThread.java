/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.service;

import java.util.Base64;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.derivation.cryptoapi.CryptoAPIDerivedKey;
import com.scytl.cryptolib.derivation.cryptoapi.CryptoAPIPBKDFDeriver;

public class WorkerThread implements Runnable {

    private final PrimitivesServiceAPI _service;

    public WorkerThread(final PrimitivesServiceAPI service) {
        _service = service;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()
            + " Start. Command = " + _service);
        processCommand();
        System.out.println(Thread.currentThread().getName() + " End.");
    }

    private void processCommand() throws CryptoLibException {
        try {

            CryptoAPIPBKDFDeriver pbkdfDeriver =
                _service.getPBKDFDeriver();
            System.out.println("pbkdfDeriver:" + pbkdfDeriver);

            CryptoAPIDerivedKey deriveKey =
                pbkdfDeriver.deriveKey(_service
                    .get64CharAlphabetCryptoRandomString().nextRandom(24)
                    .toCharArray());
            System.out.println(Base64.getEncoder()
                .encodeToString(deriveKey.getEncoded()));

        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);

        }
    }
}
