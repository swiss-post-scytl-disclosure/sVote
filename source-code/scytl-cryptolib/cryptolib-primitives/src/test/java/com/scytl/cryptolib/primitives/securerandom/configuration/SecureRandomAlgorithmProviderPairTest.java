/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.primitives.securerandom.configuration;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;

public class SecureRandomAlgorithmProviderPairTest {

    @Test
    public void givenNativePrngSunWhenGetNameThenExpectedValues() {

        assertAlgorithm("NativePRNG",
            ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN);
    }

    @Test
    public void givenWindowsPrngSunMSCAPIWhenGetNameThenExpectedValues() {

        assertAlgorithm("Windows-PRNG",
            ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI);
    }

    @Test
    public void givenNativePrngSunWhenGetProviderThenExpectedValues() {

        assertProvider(Provider.SUN,
            ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN);
    }

    @Test
    public void givenWindowsPrngSunMSCAPIWhenGetProviderThenExpectedValues() {

        assertProvider(Provider.SUN_MSCAPI,
            ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI);
    }

    @Test
    public void givenNativePrngSunWhenCheckingOSCompliance() {

        Assume.assumeTrue(OperatingSystem.LINUX.isCurrent());

        ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN
            .isOSCompliant(OperatingSystem.LINUX);
    }

    @Test
    public void givenWindowsPrngSunMSCAPIWhenCheckingOSCompliance() {

        Assume.assumeTrue(OperatingSystem.WINDOWS.isCurrent());

        ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI
            .isOSCompliant(OperatingSystem.WINDOWS);
    }

    private void assertAlgorithm(
            final String expectedAlgorithm,
            final ConfigSecureRandomAlgorithmAndProvider secureRandomAlgorithmAndProvider) {

        Assert.assertEquals("Algorithm name was not the expected value",
            expectedAlgorithm,
            secureRandomAlgorithmAndProvider.getAlgorithm());
    }

    private void assertProvider(
            final Provider expectedProvider,
            final ConfigSecureRandomAlgorithmAndProvider secureRandomAlgorithmAndProvider) {

        Assert.assertEquals("Provider was not the expected value",
            expectedProvider,
            secureRandomAlgorithmAndProvider.getProvider());
    }
}
