/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, mocha:true */
'use strict';

var bitwise = require('../lib/index.js');

describe('The bitwise utility ...', function() {
  var BYTE_ARRAY_1 = [105, 73, 99];
  var BYTE_ARRAY_2 = [86, 43, 215];
  var BYTE_ARRAY_3 = [33, 163, 41];
  var BYTE = 0x05;

  var allBytesArray_;
  var prependedArray_;
  var appendedArray_;
  var bytes1_;
  var bytes2_;
  var bytes3_;
  var allBytes_;

  beforeAll(function() {
    allBytesArray_ = BYTE_ARRAY_1.concat(BYTE_ARRAY_2, BYTE_ARRAY_3);

    prependedArray_ = [BYTE].concat(allBytesArray_);

    appendedArray_ = allBytesArray_.concat([BYTE]);

    bytes1_ = new Uint8Array(BYTE_ARRAY_1);
    bytes2_ = new Uint8Array(BYTE_ARRAY_2);
    bytes3_ = new Uint8Array(BYTE_ARRAY_3);
    allBytes_ = new Uint8Array(allBytesArray_);
  });

  describe('should be able to ..', function() {
    it('concatenate some bytes to some existing bytes', function() {
      var concatenated = bitwise.concatenate(bytes1_, bytes2_, bytes3_);

      expect(Array.apply([], concatenated)).toEqual(allBytesArray_);
    });

    it('slice a subset of bytes from a concatenation of bytes', function() {
      var sliced = bitwise.slice(
          allBytes_, bytes1_.length, bytes1_.length + bytes2_.length);

      expect(Array.apply([], sliced)).toEqual(BYTE_ARRAY_2);

      sliced = bitwise.slice(allBytes_, bytes1_.length);

      expect(Array.apply([], sliced))
          .toEqual(BYTE_ARRAY_2.concat(BYTE_ARRAY_3));
    });

    it('prepend a byte to some existing bytes', function() {
      var prepended = bitwise.prepend(BYTE, allBytes_);

      expect(Array.apply([], prepended)).toEqual(prependedArray_);
    });

    it('apppend a byte to some existing bytes', function() {
      var appended = bitwise.append(allBytes_, BYTE);

      expect(Array.apply([], appended)).toEqual(appendedArray_);
    });
  });
});
