# Bitwise Module

This module defines the bitwise utilities API for Scytl's JavaScript cryptographic library.

## How to view, build and test the source code of this module

Start by git cloning the project `comm/scytl-cryptolib` from `Stash`.

The source code of this module will be found in the directory `scytl-cryptolib/cryptolib-js-bitwise/lib`.

To build this module, change to the directory `scytl-cryptolib/cryptolib-js-bitwise` and do the following:

```java
npm install
--or--
mvn clean install -DskipTests
```
The unit tests of this module will be found in the directory `scytl-cryptolib/cryptolib-js-bitwise/spec`. To run the tests, change to the directory `scytl-cryptolib/cryptolib-js-bitwise` and do the following:

```javascript
karma test
--or--
mvn test
```
To only run a chosen test, add an `f` in front of the corresponding `it` statement. For example:

```javascript
fit('concatenate some bytes to some existing bytes', function()
...
```
**Note:** To build and test the entire `scytl-cryptolib` project, change to the directory `scytl-cryptolib` and do the following:

```javascript
mvn clean install
```
## How to generate the JSDoc for this module

To generate the JSDoc for this module, change to the directory `scytl-cryptolib/cryptolib-js-bitwise`, build the module (if not already done) and do the following:

```javascript
node_modules/.bin/jsdoc lib
```
This will generate a file called `out`. Double click on the file `out/index.html` to view the JSDoc.

## How to `npm` install this module

To `npm` install this module in standalone mode, do the following:

```javascript
npm install --registry https://nexus.scytl.net/content/groups/public-npm/ scytl-bitwise
```
To `npm` install this module as a dependency of your own module, do the following:

1. Add the following line to the `dependencies` section of your module's `package.json` file.
    ```javascript
    "scytl-bitwise": "^2.1.0",
    ```
2. Make sure that the `.npmrc` file of your module contains the following line.
    ```javascript
    registry=https://nexus.scytl.net/content/groups/public-npm/
    ```
3. Install all dependencies.
    ```javascript
    npm install
    ```
## How to concatenate bytes

The following example shows how to bitwise concatenate three byte arrays together. Both the input and the output will be in the form of byte arrays, wrapped in `Uint8Array` objects.

```javascript
var bitwise = require('scytl-bitwise');

var bytes1 = new Uint8Arra([105, 73, 99]);
var bytes2 = new Uint8Array([86, 43, 215]);
var bytes3 = new Uint8Array([33, 163, 41]);

var concatenated = bitwise.concatenate(bytes1, bytes2, bytes3);
```
## How to slice a subset of bytes from a concatenation of bytes

The following example shows how to slice the first bytes from the concatenation produced in the previous example. Both the input and output will be in the form of byte arrays, wrapped in `Uint8Array` objects.

```javascript
var bitwise = require('scytl-bitwise');

var sliced = bitwise.slice(concatenated, bytes1.length);
```
## How to prepend a byte to a some bytes

The following example shows how prepend a byte to some existing bytes. The bytes I/O will be in the form of byte arrays, wrapped in `Uint8Array` objects.

```javascript
var bitwise = require('scytl-bitwise');

var bytes = new Uint8Arra([30, 2, 58]);
var byte = 0x05;

var prepended = bitwise.prepend(byte, bytes);
```
## How to append a byte to some bytes

The following example shows how append a byte to some existing bytes. The bytes I/O will be in the form of byte arrays, wrapped in `Uint8Array` objects.

```javascript
var bitwise = require('scytl-bitwise');

var bytes = new Uint8Arra([30, 2, 58]);
var byte = 0x05;

var appended = bitwise.append(bytes, byte);
```
