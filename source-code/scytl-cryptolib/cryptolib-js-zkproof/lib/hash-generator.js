/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = HashGenerator;

/**
 * @class HashGenerator
 * @classdesc Encapsulates a hash generator needed for zero-knowledge proof
 *            generation and verification.
 * @private
 * @param {MessageDigestService}
 *            messageDigestService The message digest service to use.
 */
function HashGenerator(messageDigestService) {
  var digester_ = messageDigestService.newDigester();

  /**
   * Generates the hash.
   *
   * @function generate
   * @memberof HashGenerator
   * @param {ZpGroupElement[]}
   *            publicValues The public values.
   * @param {ZpGroupElement[]}
   *            generatedValues The generated values.
   * @param {Uint8Array|string}
   *            data Auxiliary data.
   * @returns {Uint8Array} The generated hash.
   */
  this.generate = function(publicValues, generatedValues, data) {
    digester_.update(elementsToString(publicValues));
    digester_.update(elementsToString(generatedValues));
    digester_.update(data);

    return digester_.digest();
  };

  function elementsToString(elements) {
    var str = '';
    for (var i = 0; i < elements.length; i++) {
      str += elements[i].value;
    }

    return str;
  }
}
