/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var ZeroKnowledgeProofService = require('./service');
var validator = require('./input-validator');

module.exports = {
  /**
   * Creates a new ZeroKnowledgeProofService object, which encapsulates a
   * zero-knowledge proof service.
   *
   * @function newService
   * @global
   * @param {Object}
   *            [options] An object containing optional arguments.
   * @param {Policy}
   *            [options.policy=Default policy] The cryptographic policy to
   *            use.
   * @param {MessageDigestService}
   *            [options.messageDigestService=Created internally] The message
   *            digest service to use.
   * @param {SecureRandomService}
   *            [options.secureRandomService=Created internally] The secure
   *            random service to use.
   * @param {MathematicalService}
   *            [options.mathematicalService=Created internally] The
   *            mathematical service to use.
   * @returns {ZeroKnowledgeProofService} The new ZeroKnowledgeProofService
   *          object.
   * @throws {Error}
   *             If the input data validation fails.
   * @example <caption> How to use a cryptographic policy that sets the
   *          message digest algorithm to <code>SHA-512</code></caption>
   *
   * var cryptoPolicy = require('scytl-cryptopolicy');
   * var zkProof = require('scytl-zkproof');
   *
   * var myPolicy = cryptoPolicy.newInstance();
   *
   * myPolicy.proofs.messagedigest.algorithm =
   * cryptoPolicy.options.proofs.messageDigest.algorithm.SHA512_224;
   *
   * var proofService = zkProof.newService({policy: myPolicy});
   */
  newService: function(options) {
    checkData(options);

    return new ZeroKnowledgeProofService(options);
  }
};

function checkData(options) {
  options = options || {};

  if (typeof options.policy !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.policy,
        'Cryptographic policy provided to zero-knowledge proof service');
  }

  if (typeof options.messageDigestService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.messageDigestService,
        'Message digest service object provided to zero-knowledge proof service');
  }

  if (typeof options.secureRandomService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.secureRandomService,
        'Secure random service object provided to zero-knowledge proof service');
  }

  if (typeof options.mathematicalService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.mathematicalService,
        'Mathematical service object provided to zero-knowledge proof service');
  }
}
