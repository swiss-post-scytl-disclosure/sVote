/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var ValidationTestData = require('../data/validation-data');
var zkProof = require('../../lib/index');

describe('The zero-knowledge proof module should be able to ...', function() {
  var proofService_;

  var nonObject_;
  var emptyObject_;
  var nonPolicy_;
  var nonSecureRandomService_;
  var nonMathematicalService_;
  var nonMessageDigestService_;
  var nonString_;
  var nonJsonString_;
  var nonArray_;
  var emptyArray_;
  var nonObjectArray_;
  var nonProofObject_;
  var hash_;
  var values_;
  var exponents_;
  var phiOutputs_;

  beforeAll(function() {
    proofService_ = zkProof.newService();

    var testData = new ValidationTestData();
    nonObject_ = testData.getNonObject();
    emptyObject_ = testData.getEmptyObject();
    nonPolicy_ = testData.getNonPolicy();
    nonSecureRandomService_ = testData.getNonSecureRandomService();
    nonMathematicalService_ = testData.getNonMathematicalService();
    nonMessageDigestService_ = testData.getNonMessageDigestService();
    nonString_ = testData.getNonString();
    nonJsonString_ = testData.getNonJsonString();
    nonArray_ = testData.getNonArray();
    emptyArray_ = testData.getEmptyArray();
    nonObjectArray_ = testData.getNonObjectArray();
    nonProofObject_ = testData.getNonProofObject();
    hash_ = testData.getProofHash();
    values_ = testData.getProofValues();
    exponents_ = testData.getPreComputationExponents();
    phiOutputs_ = testData.getPreComputationPhiOutputs();
  });

  describe('create a zero-knowledge proof service that should be able to ..', function() {
    it('throw an error when being created, using an invalid cryptographic policy',
       function() {
         expect(function() {
           Object.create(zkProof.newService({policy: null}));
         }).toThrow();

         expect(function() {
           Object.create(zkProof.newService({policy: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(zkProof.newService({policy: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when being created, using an invalid secure random service object',
       function() {
         expect(function() {
           Object.create(zkProof.newService({secureRandomService: null}));
         }).toThrow();

         expect(function() {
           Object.create(zkProof.newService({secureRandomService: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(
               zkProof.newService({secureRandomService: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when being created, using an invalid mathematical service object',
       function() {
         expect(function() {
           Object.create(zkProof.newService({mathematicalService: null}));
         }).toThrow();

         expect(function() {
           Object.create(zkProof.newService({mathematicalService: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(
               zkProof.newService({mathematicalService: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when being created, using an invalid message digest service object',
       function() {
         expect(function() {
           Object.create(zkProof.newService({messageDigestService: null}));
         }).toThrow();

         expect(function() {
           Object.create(
               zkProof.newService({messageDigestService: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(
               zkProof.newService({messageDigestService: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when creating a new ZeroKnowledgeProof object, using invalid input data',
       function() {
         expect(function() {
           proofService_.newProof(undefined, values_);
         }).toThrow();

         expect(function() {
           proofService_.newProof(null, values_);
         }).toThrow();

         expect(function() {
           proofService_.newProof(nonObject_, values_);
         }).toThrow();

         expect(function() {
           proofService_.newProof(emptyObject_, values_);
         }).toThrow();

         expect(function() {
           proofService_.newProof(hash_);
         }).toThrow();

         expect(function() {
           proofService_.newProof(hash_, undefined);
         }).toThrow();

         expect(function() {
           proofService_.newProof(hash_, null);
         }).toThrow();

         expect(function() {
           proofService_.newProof(hash_, nonArray_);
         }).toThrow();

         expect(function() {
           proofService_.newProof(hash_, emptyArray_);
         }).toThrow();

         expect(function() {
           proofService_.newProof(hash_, nonObjectArray_);
         }).toThrow();
       });

    it('throw an error when creating a new ZeroKnowledgeProofPreComputation object, using invalid input data',
       function() {
         expect(function() {
           proofService_.newPreComputation(undefined, phiOutputs_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(null, phiOutputs_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(nonArray_, phiOutputs_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(emptyArray_, phiOutputs_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(nonObjectArray_, phiOutputs_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(exponents_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(exponents_, undefined);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(exponents_, null);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(exponents_, nonArray_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(exponents_, emptyArray_);
         }).toThrow();

         expect(function() {
           proofService_.newPreComputation(exponents_, nonObjectArray_);
         }).toThrow();
       });

    it('throw an error when serializing or deserializing a ZeroKnowledgeProof object, using invalid input data',
       function() {
         expect(function() {
           proofService_.proofToJson();
         }).toThrow();

         expect(function() {
           proofService_.proofToJson(undefined);
         }).toThrow();

         expect(function() {
           proofService_.proofToJson(null);
         }).toThrow();

         expect(function() {
           proofService_.proofToJson(nonProofObject_);
         }).toThrow();

         expect(function() {
           proofService_.jsonToProof(undefined);
         }).toThrow();

         expect(function() {
           proofService_.jsonToProof(null);
         }).toThrow();

         expect(function() {
           proofService_.jsonToProof(nonString_);
         }).toThrow();

         expect(function() {
           proofService_.jsonToProof(nonJsonString_);
         }).toThrow();
       });

    it('throw an error when serializing or deserializing a ZeroKnowledgeProofPreComputation object, using invalid input data',
       function() {
         expect(function() {
           proofService_.preComputationToJson();
         }).toThrow();

         expect(function() {
           proofService_.preComputationToJson(undefined);
         }).toThrow();

         expect(function() {
           proofService_.preComputationToJson(null);
         }).toThrow();

         expect(function() {
           proofService_.preComputationToJson(nonProofObject_);
         }).toThrow();

         expect(function() {
           proofService_.jsonToPreComputation(undefined);
         }).toThrow();

         expect(function() {
           proofService_.jsonToPreComputation(null);
         }).toThrow();

         expect(function() {
           proofService_.jsonToPreComputation(nonString_);
         }).toThrow();

         expect(function() {
           proofService_.jsonToPreComputation(nonJsonString_);
         }).toThrow();
       });
  });
});
