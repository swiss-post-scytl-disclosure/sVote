/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var PlaintextExponentEqualityProofTestData =
    require('./data/plaintext-exponent-equality-proof-data');
var cryptoPolicy = require('scytl-cryptopolicy');
var zkProof = require('../lib/index');
var messageDigest = require('scytl-messagedigest');
var mathematical = require('scytl-mathematical');
var secureRandom = require('scytl-securerandom');
var codec = require('scytl-codec');

describe('The zero-knowledge proof module should be able to ...', function() {
  var proofService_;
  var group_;
  var firstSecret_;
  var secondSecret_;
  var anotherFirstSecret_;
  var anotherSecondSecret_;
  var baseElements_;
  var otherBaseElements_;
  var ciphertext_;
  var anotherCiphertext_;
  var data_;
  var otherData_;
  var proofHandler_;
  var proof_;
  var preComputation_;

  beforeAll(function() {
    proofService_ = zkProof.newService();

    var testData = new PlaintextExponentEqualityProofTestData();
    group_ = testData.getGroup();
    firstSecret_ = testData.getFirstSecret();
    secondSecret_ = testData.getSecondSecret();
    anotherFirstSecret_ = testData.getAnotherFirstSecret();
    anotherSecondSecret_ = testData.getAnotherSecondSecret();
    baseElements_ = testData.getBaseElements();
    otherBaseElements_ = testData.getOtherBaseElements();
    ciphertext_ = testData.getCiphertext();
    anotherCiphertext_ = testData.getAnotherCiphertext();
    data_ = testData.getStringData();
    otherData_ = testData.getOtherStringData();

    proofHandler_ =
        proofService_.newPlaintextExponentEqualityProofHandler(group_).init(
            baseElements_);
    proof_ = proofHandler_.generate(firstSecret_, secondSecret_, ciphertext_);
    preComputation_ = proofHandler_.preCompute();
  });

  beforeEach(function() {
    proofHandler_ =
        proofService_.newPlaintextExponentEqualityProofHandler(group_).init(
            baseElements_);
  });

  describe('create a zero-knowledge proof service that should be able to ..', function() {
    describe('create a plaintext exponent equality proof handler that should be able to', function() {
      it('generate and verify a plaintext exponent equality proof', function() {
        var verified = proofHandler_.verify(proof_, ciphertext_);
        expect(verified).toBeTruthy();
      });

      it('generate and verify a plaintext exponent equality proof, using a specified cryptographic policy',
         function() {
           var policy = cryptoPolicy.newInstance();
           policy.proofs.messageDigest.algorithm =
               cryptoPolicy.options.proofs.messageDigest.algorithm.SHA512_224;

           var proofService = zkProof.newService({policy: policy});
           var proofHandler =
               proofService.newPlaintextExponentEqualityProofHandler(group_)
                   .init(baseElements_);

           proof_ =
               proofHandler.generate(firstSecret_, secondSecret_, ciphertext_);

           var verified = proofHandler.verify(proof_, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext exponent equality proof, using a specified message digest service object',
         function() {
           var policy = cryptoPolicy.newInstance();
           policy.proofs.messageDigest.algorithm =
               cryptoPolicy.options.proofs.messageDigest.algorithm.SHA512_224;
           var messageDigestService =
               messageDigest.newService({policy: policy});

           var proofService =
               zkProof.newService({messageDigestService: messageDigestService});
           var proofHandler =
               proofService.newPlaintextExponentEqualityProofHandler(group_)
                   .init(baseElements_);

           proof_ =
               proofHandler.generate(firstSecret_, secondSecret_, ciphertext_);

           var verified = proofHandler.verify(proof_, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext exponent equality proof, using a specified secure random service object',
         function() {
           var proofService = zkProof.newService(
               {secureRandomService: secureRandom.newService()});
           var proofHandler =
               proofService.newPlaintextExponentEqualityProofHandler(group_)
                   .init(baseElements_);

           proof_ =
               proofHandler.generate(firstSecret_, secondSecret_, ciphertext_);

           var verified = proofHandler.verify(proof_, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext exponent equality proof, using a specified mathematical service object',
         function() {
           var proofService = zkProof.newService(
               {mathematicalService: mathematical.newService()});
           var proofHandler =
               proofService.newPlaintextExponentEqualityProofHandler(group_)
                   .init(baseElements_);

           proof_ =
               proofHandler.generate(firstSecret_, secondSecret_, ciphertext_);

           var verified = proofHandler.verify(proof_, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext proof, using provided auxiliary data',
         function() {
           // Data as string.
           var proof = proofHandler_.generate(
               firstSecret_, secondSecret_, ciphertext_, {data: data_});
           var verified =
               proofHandler_.verify(proof, ciphertext_, {data: data_});
           expect(verified).toBeTruthy();

           // Data as bytes.
           proof = proofHandler_.generate(
               firstSecret_, secondSecret_, ciphertext_,
               {data: codec.utf8Encode(data_)});
           verified = proofHandler_.verify(
               proof, ciphertext_, {data: codec.utf8Encode(data_)});
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext exponent equality proof, using a pre-computation',
         function() {
           var proof = proofHandler_.generate(
               firstSecret_, secondSecret_, ciphertext_,
               {preComputation: preComputation_});

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext exponent equality proof, using empty auxiliary data',
         function() {
           var proof = proofHandler_.generate(
               firstSecret_, secondSecret_, ciphertext_, {data: ''});

           var verified = proofHandler_.verify(proof, ciphertext_, {data: ''});
           expect(verified).toBeTruthy();
         });

      it('pre-compute, generate and verify a plaintext exponent equality proof, using method chaining',
         function() {
           var preComputation =
               proofService_.newPlaintextExponentEqualityProofHandler(group_)
                   .init(baseElements_)
                   .preCompute();

           var proof =
               proofService_.newPlaintextExponentEqualityProofHandler(group_)
                   .init(baseElements_)
                   .generate(
                       firstSecret_, secondSecret_, ciphertext_,
                       {data: data_, preComputation: preComputation});

           var verified =
               proofService_.newPlaintextExponentEqualityProofHandler(group_)
                   .init(baseElements_)
                   .verify(proof, ciphertext_, {data: data_});
           expect(verified).toBeTruthy();
         });

      it('create a new plaintext exponent equality ZeroKnowledgeProof object',
         function() {
           var hash = proof_.hash;
           var values = proof_.values;
           var proof = proofService_.newProof(hash, values);

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('create a new plaintext exponent equality ZeroKnowledgeProofPreComputation object',
         function() {
           var exponents = preComputation_.exponents;
           var phiOutputs = preComputation_.phiOutputs;
           var preComputation =
               proofService_.newPreComputation(exponents, phiOutputs);

           var proof = proofHandler_.generate(
               firstSecret_, secondSecret_, ciphertext_,
               {preComputation: preComputation});

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('serialize and deserialize a plaintext exponent equality proof',
         function() {
           var proofJson = proof_.toJson();

           var proof = proofService_.newProof(proofJson);

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('serialize and deserialize a plaintext exponent equality proof pre-computation',
         function() {
           var preComputationJson = preComputation_.toJson();

           var preComputation =
               proofService_.newPreComputation(preComputationJson);

           var proof = proofHandler_.generate(
               firstSecret_, secondSecret_, ciphertext_,
               {preComputation: preComputation});

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeTruthy();
         });

      it('fail to verify a plaintext exponent equality proof that was generated with a first secret not used to generate the ciphertext',
         function() {
           var proof = proofHandler_.generate(
               anotherFirstSecret_, secondSecret_, ciphertext_);

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext exponent equality proof that was generated with a second secret not used to generate the ciphertext',
         function() {
           var proof = proofHandler_.generate(
               firstSecret_, anotherSecondSecret_, ciphertext_);

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext exponent equality proof that was generated with base elements not used to generate the ciphertext',
         function() {
           proofHandler_.init(otherBaseElements_);

           var proof =
               proofHandler_.generate(firstSecret_, secondSecret_, ciphertext_);

           var verified = proofHandler_.verify(proof, ciphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext exponent equality proof that was generated with ciphertext not generated from the base elements',
         function() {
           var proof = proofHandler_.generate(
               firstSecret_, secondSecret_, anotherCiphertext_);

           var verified = proofHandler_.verify(proof, anotherCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext exponent equality proof when using base elements not used to generate the proof',
         function() {
           var verified = proofHandler_.init(otherBaseElements_)
                              .verify(proof_, ciphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext exponent equality proof when using ciphertext not used to generate the proof',
         function() {
           var verified = proofHandler_.verify(proof_, anotherCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext exponent equality proof when using auxiliary data not used to generate the proof',
         function() {
           var proof = proofHandler_.generate(
               firstSecret_, secondSecret_, ciphertext_, {data: data_});

           var verified =
               proofHandler_.verify(proof, ciphertext_, {data: otherData_});
           expect(verified).toBeFalsy();
         });

      it('throw an error when generating a proof before the handler has been initialized with base elements',
         function() {
           var proofHandler =
               proofService_.newPlaintextExponentEqualityProofHandler(group_);

           expect(function() {
             proofHandler.generate(firstSecret_, secondSecret_, ciphertext_);
           }).toThrow();
         });

      it('throw an error when pre-computing a proof before the handler has been initialized with base elements',
         function() {
           var proofHandler =
               proofService_.newPlaintextExponentEqualityProofHandler(group_);

           expect(function() {
             proofHandler.preCompute();
           }).toThrow();
         });

      it('throw an error when verifying a proof before the handler has been initialized with base elements',
         function() {
           var proofHandler =
               proofService_.newPlaintextExponentEqualityProofHandler(group_);

           expect(function() {
             proofHandler.verify(proof_, ciphertext_);
           }).toThrow();
         });
    });
  });
});
