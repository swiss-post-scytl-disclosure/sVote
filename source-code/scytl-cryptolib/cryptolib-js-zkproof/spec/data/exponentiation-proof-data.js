/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var CommonTestData = require('./common-data');
var mathematical = require('scytl-mathematical');

module.exports = ExponentiationProofTestData;

/**
 * Provides the data needed by the exponentiation zero-knowledge proof of
 * knowledge unit tests.
 */
function ExponentiationProofTestData() {
  var NUM_BASE_ELEMENTS = 3;
  var EXPONENTIATION_PROOF_STRING_DATA =
      'Test Exponentation Proof Auxilary Data';
  var OTHER_EXPONENTIATION_PROOF_STRING_DATA =
      EXPONENTIATION_PROOF_STRING_DATA + '0';
  var NUM_EXPONENTIATION_PROOF_PROGRESS_CHECKS = NUM_BASE_ELEMENTS;

  var mathRandomGenerator = mathematical.newService().newRandomGenerator();

  var commonTestData = new CommonTestData();
  var group_ = commonTestData.getGroup();
  var anotherGroup_ = commonTestData.getAnotherGroup();

  var secret_ = mathRandomGenerator.nextExponent(group_);
  var anotherSecret_;
  do {
    anotherSecret_ = mathRandomGenerator.nextExponent(group_);
  } while (anotherSecret_.value.equals(secret_.value));
  var secretFromAnotherGroup_ = mathRandomGenerator.nextExponent(anotherGroup_);

  var baseElements_ =
      commonTestData.generateRandomGroupElements(group_, NUM_BASE_ELEMENTS);
  var otherBaseElements_;
  do {
    otherBaseElements_ =
        commonTestData.generateRandomGroupElements(group_, NUM_BASE_ELEMENTS);
  } while (otherBaseElements_.equals(baseElements_));
  var lessBaseElements_ = baseElements_.slice(1);
  var baseElementsFromAnotherGroup_ =
      commonTestData.generateRandomGroupElements(
          anotherGroup_, NUM_BASE_ELEMENTS);

  var exponentiatedElements_ =
      commonTestData.exponentiateElements(baseElements_, secret_);
  var otherExponentiatedElements_ =
      commonTestData.exponentiateElements(baseElements_, anotherSecret_);
  var tooManyBaseElements =
      commonTestData.generateRandomGroupElements(group_, NUM_BASE_ELEMENTS + 1);
  var tooManyExponentiatedElements_ =
      commonTestData.exponentiateElements(tooManyBaseElements, secret_);
  var exponentiatedElementsFromAnotherGroup_ =
      commonTestData.exponentiateElements(
          baseElementsFromAnotherGroup_, secretFromAnotherGroup_);

  this.getGroup = function() {
    return group_;
  };

  this.getAnotherGroup = function() {
    return anotherGroup_;
  };

  this.getSecret = function() {
    return secret_;
  };

  this.getAnotherSecret = function() {
    return anotherSecret_;
  };

  this.getSecretFromAnotherGroup = function() {
    return secretFromAnotherGroup_;
  };

  this.getBaseElements = function() {
    return baseElements_;
  };

  this.getOtherBaseElements = function() {
    return otherBaseElements_;
  };

  this.getLessBaseElements = function() {
    return lessBaseElements_;
  };

  this.getBaseElementsFromAnotherGroup = function() {
    return baseElementsFromAnotherGroup_;
  };

  this.getExponentiatedElements = function() {
    return exponentiatedElements_;
  };

  this.getOtherExponentiatedElements = function() {
    return otherExponentiatedElements_;
  };

  this.getTooManyExponentiatedElements = function() {
    return tooManyExponentiatedElements_;
  };

  this.getExponentiatedElementsFromAnotherGroup = function() {
    return exponentiatedElementsFromAnotherGroup_;
  };

  this.getStringData = function() {
    return EXPONENTIATION_PROOF_STRING_DATA;
  };

  this.getOtherStringData = function() {
    return OTHER_EXPONENTIATION_PROOF_STRING_DATA;
  };

  this.getNumProgressChecks = function() {
    return NUM_EXPONENTIATION_PROOF_PROGRESS_CHECKS;
  };
}
