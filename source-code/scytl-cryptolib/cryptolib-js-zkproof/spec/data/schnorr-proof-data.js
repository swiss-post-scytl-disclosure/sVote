/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var CommonTestData = require('./common-data');
var mathematical = require('scytl-mathematical');

module.exports = SchnorrProofTestData;

/**
 * Provides the data needed by the Schnorr zero-knowledge proof of knowledge
 * unit tests.
 */
function SchnorrProofTestData() {
  var SCHNORR_PROOF_STRING_DATA = 'Test Schnorr Proof Auxilary Data';
  var OTHER_SCHNORR_PROOF_STRING_DATA = SCHNORR_PROOF_STRING_DATA + '0';
  var VOTER_ID = '11111';
  var ELECTION_ID = '22222';
  var NUM_SCHNORR_PROOF_PROGRESS_CHECKS = 1;

  var mathRandomGenerator = mathematical.newService().newRandomGenerator();

  var commonTestData = new CommonTestData();
  var group_ = commonTestData.getGroup();
  var anotherGroup_ = commonTestData.getAnotherGroup();

  var secret_ = mathRandomGenerator.nextExponent(group_);
  var anotherSecret_;
  do {
    anotherSecret_ = mathRandomGenerator.nextExponent(group_);
  } while (anotherSecret_.value.equals(secret_.value));
  var secretFromAnotherGroup_ = mathRandomGenerator.nextExponent(anotherGroup_);

  var gamma_ = group_.generator.exponentiate(secret_);
  var gammaForAnotherSecret_ = group_.generator.exponentiate(anotherSecret_);
  var gammaFromAnotherGroup_ = anotherGroup_.generator.exponentiate(
      mathRandomGenerator.nextExponent(anotherGroup_));

  this.getGroup = function() {
    return group_;
  };

  this.getAnotherGroup = function() {
    return anotherGroup_;
  };

  this.getSecret = function() {
    return secret_;
  };

  this.getAnotherSecret = function() {
    return anotherSecret_;
  };

  this.getSecretFromAnotherGroup = function() {
    return secretFromAnotherGroup_;
  };

  this.getGamma = function() {
    return gamma_;
  };

  this.getGammaForAnotherSecret = function() {
    return gammaForAnotherSecret_;
  };

  this.getGammaFromAnotherGroup = function() {
    return gammaFromAnotherGroup_;
  };

  this.getStringData = function() {
    return SCHNORR_PROOF_STRING_DATA;
  };

  this.getOtherStringData = function() {
    return OTHER_SCHNORR_PROOF_STRING_DATA;
  };

  this.getVoterId = function() {
    return VOTER_ID;
  };

  this.getElectionId = function() {
    return ELECTION_ID;
  };

  this.getNumProgressChecks = function() {
    return NUM_SCHNORR_PROOF_PROGRESS_CHECKS;
  };
}
