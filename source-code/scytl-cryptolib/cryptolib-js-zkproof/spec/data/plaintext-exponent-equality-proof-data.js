/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var CommonTestData = require('./common-data');
var mathematical = require('scytl-mathematical');

module.exports = PlaintextExponentEqualityProofTestData;

/**
 * Provides the data needed by the plaintext exponent equality zero-knowledge
 * proof of knowledge unit tests.
 */
function PlaintextExponentEqualityProofTestData() {
  var NUM_BASE_ELEMENTS = 5;
  var PLAINTEXT_EXPONENT_EQUALITY_PROOF_STRING_DATA =
      'Test Plaintext Exponent Equality Proof Auxilary Data';
  var OTHER_PLAINTEXT_EXPONENT_EQUALITY_PROOF_STRING_DATA =
      PLAINTEXT_EXPONENT_EQUALITY_PROOF_STRING_DATA + '0';
  var NUM_PLAINTEXT_EXPONENT_EQUALITY_PROOF_PROGRESS_CHECKS = NUM_BASE_ELEMENTS;

  var mathRandomGenerator = mathematical.newService().newRandomGenerator();

  var commonTestData = new CommonTestData();
  var group_ = commonTestData.getGroup();
  var anotherGroup_ = commonTestData.getAnotherGroup();

  var firstSecret_ = mathRandomGenerator.nextExponent(group_);
  var secondSecret_ = mathRandomGenerator.nextExponent(group_);
  var anotherFirstSecret_;
  do {
    anotherFirstSecret_ = mathRandomGenerator.nextExponent(group_);
  } while (anotherFirstSecret_.value.equals(firstSecret_.value));
  var anotherSecondSecret_;
  do {
    anotherSecondSecret_ = mathRandomGenerator.nextExponent(group_);
  } while (anotherSecondSecret_.value.equals(secondSecret_.value));
  var firstSecretFromAnotherGroup_ =
      mathRandomGenerator.nextExponent(anotherGroup_);
  var secondSecretFromAnotherGroup_ =
      mathRandomGenerator.nextExponent(anotherGroup_);

  var baseElements_ =
      commonTestData.generateRandomGroupElements(group_, NUM_BASE_ELEMENTS);
  var otherBaseElements_;
  do {
    otherBaseElements_ =
        commonTestData.generateRandomGroupElements(group_, NUM_BASE_ELEMENTS);
  } while (otherBaseElements_.equals(baseElements_));
  var lessBaseElements_ = commonTestData.generateRandomGroupElements(
      group_, (NUM_BASE_ELEMENTS - 2));
  var baseElementsFromAnotherGroup_ =
      commonTestData.generateRandomGroupElements(
          anotherGroup_, NUM_BASE_ELEMENTS);

  var ciphertext_ =
      commonTestData.generatePlaintextExponentEqualityProofCiphertext(
          baseElements_, firstSecret_, secondSecret_);
  var anotherCiphertext_ =
      commonTestData.generatePlaintextExponentEqualityProofCiphertext(
          otherBaseElements_, firstSecret_, secondSecret_);
  var ciphertextWithLessElements_ =
      commonTestData.generatePlaintextExponentEqualityProofCiphertext(
          lessBaseElements_, firstSecret_, secondSecret_);
  var ciphertextFromAnotherGroup_ =
      commonTestData.generatePlaintextExponentEqualityProofCiphertext(
          baseElementsFromAnotherGroup_, firstSecretFromAnotherGroup_,
          secondSecretFromAnotherGroup_);

  this.getGroup = function() {
    return group_;
  };

  this.getAnotherGroup = function() {
    return anotherGroup_;
  };

  this.getFirstSecret = function() {
    return firstSecret_;
  };

  this.getSecondSecret = function() {
    return secondSecret_;
  };

  this.getAnotherFirstSecret = function() {
    return anotherFirstSecret_;
  };

  this.getAnotherSecondSecret = function() {
    return anotherSecondSecret_;
  };

  this.getFirstSecretFromAnotherGroup = function() {
    return firstSecretFromAnotherGroup_;
  };

  this.getSecondSecretFromAnotherGroup = function() {
    return secondSecretFromAnotherGroup_;
  };

  this.getBaseElements = function() {
    return baseElements_;
  };

  this.getOtherBaseElements = function() {
    return otherBaseElements_;
  };

  this.getLessBaseElements = function() {
    return lessBaseElements_;
  };

  this.getBaseElementsFromAnotherGroup = function() {
    return baseElementsFromAnotherGroup_;
  };

  this.getCiphertext = function() {
    return ciphertext_;
  };

  this.getAnotherCiphertext = function() {
    return anotherCiphertext_;
  };

  this.getCiphertextWithLessElements = function() {
    return ciphertextWithLessElements_;
  };

  this.getCiphertextFromAnotherGroup = function() {
    return ciphertextFromAnotherGroup_;
  };

  this.getStringData = function() {
    return PLAINTEXT_EXPONENT_EQUALITY_PROOF_STRING_DATA;
  };

  this.getOtherStringData = function() {
    return OTHER_PLAINTEXT_EXPONENT_EQUALITY_PROOF_STRING_DATA;
  };

  this.getNumProgressChecks = function() {
    return NUM_PLAINTEXT_EXPONENT_EQUALITY_PROOF_PROGRESS_CHECKS;
  };
}
