/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = ProgressMeterTestData;

/**
 * Provides the data and functions needed by the zero-knowledge proof of
 * knowledge progress meter unit tests.
 */
function ProgressMeterTestData() {
  var DEFAULT_CUSTOM_PROGRESS_PERCENT_MIN_CHECK_INTERVAL = 10;
  var CUSTOM_PROGRESS_PERCENT_MIN_CHECK_INTERVAL = 5;

  var progressPercentSum_;
  var progressPercentLatest_;

  this.initializeProgressMeterData = function() {
    progressPercentSum_ = 0;
    progressPercentLatest_ = 0;
  };

  this.getDefaultProgressPercentMinCheckInterval = function() {
    return DEFAULT_CUSTOM_PROGRESS_PERCENT_MIN_CHECK_INTERVAL;
  };

  this.getCustomProgressPercentMinCheckInterval = function() {
    return CUSTOM_PROGRESS_PERCENT_MIN_CHECK_INTERVAL;
  };

  this.getProgressPercentSum = function() {
    return progressPercentSum_;
  };

  this.getProgressPercentLatest = function() {
    return progressPercentLatest_;
  };

  this.progressCallback = function(progressPercent) {
    progressPercentSum_ += progressPercent;
    progressPercentLatest_ = progressPercent;
  };

  this.calculateProgressPercentSum = function(numChecks, minCheckInterval) {
    var percent = 0;
    var lastPercent = 0;
    var percentChange = 0;
    var incrementPercentSum = false;
    var percentSum = 0;

    for (var i = 0; i < numChecks; i++) {
      percent = Math.floor(((i + 1) / numChecks) * 100);
      percent = Math.min(percent, 100);

      percentChange = percent - lastPercent;
      if (percentChange > 0) {
        incrementPercentSum =
            (percentChange >= minCheckInterval) || (percent === 100);
        if (incrementPercentSum) {
          lastPercent = percent;
          percentSum += percent;
        }
      }
    }

    return percentSum;
  };
}
