/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var CommonTestData = require('./common-data');
var zkProof = require('../../lib/index');
var mathematical = require('scytl-mathematical');

module.exports = ValidationTestData;

/**
 * Provides the input validation data needed by the zero-knowledge proof service
 * unit tests.
 */
function ValidationTestData() {
  var proofService = zkProof.newService();
  var mathRandomGenerator = mathematical.newService().newRandomGenerator();
  var commonTestData = new CommonTestData();

  var nonObject_ = 999;
  var emptyObject_ = {};
  var nonPolicy_ = zkProof.newService();
  var nonSecureRandomService_ = zkProof.newService();
  var nonMathematicalService_ = zkProof.newService();
  var nonMessageDigestService_ = zkProof.newService();

  var nonBoolean_ = '';
  var nonNumber_ = '';
  var nonPositiveNumber_ = 0;
  var nonString_ = 999;
  var emptyString_ = '';
  var nonJsonString_ = 'Not a JSON string';
  var nonArray_ = '';
  var emptyArray_ = [];
  var nonObjectArray_ = ['1', '2', '3'];
  var nonStringArray_ = [];
  nonStringArray_.push(0);
  nonStringArray_.push(1);
  nonStringArray_.push(2);
  var nonFunction_ = '';
  var nonAuxiliaryData_ = 999;
  var nonProofObject_ = mathematical.newService();

  var group = commonTestData.getGroup();
  var secret = mathRandomGenerator.nextExponent(group);
  var gamma = group.generator.exponentiate(secret);
  var proof =
      proofService.newSchnorrProofHandler(group).generate(secret, gamma, '');
  var proofHash_ = proof.hash;
  var proofValues_ = proof.values;
  var preComputation = proofService.newSchnorrProofHandler(group).preCompute();
  var preComputationExponents_ = preComputation.exponents;
  var preComputationPhiOutputs_ = preComputation.phiOutputs;

  this.getNonObject = function() {
    return nonObject_;
  };

  this.getEmptyObject = function() {
    return emptyObject_;
  };

  this.getNonPolicy = function() {
    return nonPolicy_;
  };

  this.getNonSecureRandomService = function() {
    return nonSecureRandomService_;
  };

  this.getNonMathematicalService = function() {
    return nonMathematicalService_;
  };

  this.getNonMessageDigestService = function() {
    return nonMessageDigestService_;
  };

  this.getNonBoolean = function() {
    return nonBoolean_;
  };

  this.getNonNumber = function() {
    return nonNumber_;
  };

  this.getNonPositiveNumber = function() {
    return nonPositiveNumber_;
  };

  this.getNonString = function() {
    return nonString_;
  };

  this.getEmptyString = function() {
    return emptyString_;
  };

  this.getNonJsonString = function() {
    return nonJsonString_;
  };

  this.getNonArray = function() {
    return nonArray_;
  };

  this.getEmptyArray = function() {
    return emptyArray_;
  };

  this.getNonObjectArray = function() {
    return nonObjectArray_;
  };

  this.getNonStringArray = function() {
    return nonStringArray_;
  };

  this.getNonFunction = function() {
    return nonFunction_;
  };

  this.getNonAuxiliaryData = function() {
    return nonAuxiliaryData_;
  };

  this.getNonProofObject = function() {
    return nonProofObject_;
  };

  this.getProofHash = function() {
    return proofHash_;
  };

  this.getProofValues = function() {
    return proofValues_;
  };

  this.getPreComputationExponents = function() {
    return preComputationExponents_;
  };

  this.getPreComputationPhiOutputs = function() {
    return preComputationPhiOutputs_;
  };
}
