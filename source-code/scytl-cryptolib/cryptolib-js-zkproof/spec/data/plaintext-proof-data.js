/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var CommonTestData = require('./common-data');
var elGamal = require('scytl-elgamal');

module.exports = PlaintextProofTestData;

/**
 * Provides the data needed by the plaintext zero-knowledge proof of knowledge
 * unit tests.
 */
function PlaintextProofTestData() {
  var NUM_PLAINTEXT_ELEMENTS = 3;
  var PLAINTEXT_PROOF_STRING_DATA = 'Test Plaintext Proof Auxilary Data';
  var OTHER_PLAINTEXT_PROOF_STRING_DATA = PLAINTEXT_PROOF_STRING_DATA + '0';
  var NUM_PLAINTEXT_PROOF_PROGRESS_CHECKS = NUM_PLAINTEXT_ELEMENTS + 1;

  var elGamalEncrypter = elGamal.newService().newEncrypter();

  var commonTestData = new CommonTestData();
  var group_ = commonTestData.getGroup();
  var anotherGroup_ = commonTestData.getAnotherGroup();

  var publicKey_ =
      commonTestData.generateKeyPair(group_, NUM_PLAINTEXT_ELEMENTS).publicKey;
  var anotherPublicKey_;
  do {
    anotherPublicKey_ =
        commonTestData.generateKeyPair(group_, NUM_PLAINTEXT_ELEMENTS)
            .publicKey;
  } while (anotherPublicKey_.elements.equals(publicKey_.elements));
  var publicKeyWithLessElements_ =
      commonTestData.generateKeyPair(group_, (NUM_PLAINTEXT_ELEMENTS - 1))
          .publicKey;
  var publicKeyFromAnotherGroup_ =
      commonTestData.generateKeyPair(anotherGroup_, NUM_PLAINTEXT_ELEMENTS)
          .publicKey;

  var plaintext_ = commonTestData.generateRandomGroupElements(
      group_, NUM_PLAINTEXT_ELEMENTS);
  var anotherPlaintext_;
  do {
    anotherPlaintext_ = commonTestData.generateRandomGroupElements(
        group_, NUM_PLAINTEXT_ELEMENTS);
  } while (anotherPlaintext_.equals(plaintext_));
  var plaintextWithLessElements_ = commonTestData.generateRandomGroupElements(
      group_, (NUM_PLAINTEXT_ELEMENTS - 1));
  var plaintextFromAnotherGroup_ = commonTestData.generateRandomGroupElements(
      anotherGroup_, NUM_PLAINTEXT_ELEMENTS);

  var encryptedElements_ =
      elGamalEncrypter.init(publicKey_).encrypt(plaintext_, {saveSecret: true});
  var otherEncryptedElements_;
  do {
    otherEncryptedElements_ =
        elGamalEncrypter.init(publicKey_).encrypt(plaintext_, {
          saveSecret: true
        });
  } while (otherEncryptedElements_.secret.equals(encryptedElements_.secret) ||
           otherEncryptedElements_.phis.equals(encryptedElements_.phis));
  var encryptedElementsWithLessElements_ =
      elGamalEncrypter.init(publicKeyWithLessElements_)
          .encrypt(plaintextWithLessElements_, {saveSecret: true});
  var encryptedElementsFromAnotherGroup_ =
      elGamalEncrypter.init(publicKeyFromAnotherGroup_)
          .encrypt(plaintextFromAnotherGroup_, {saveSecret: true});

  this.getGroup = function() {
    return group_;
  };

  this.getAnotherGroup = function() {
    return anotherGroup_;
  };

  this.getPublicKey = function() {
    return publicKey_;
  };

  this.getAnotherPublicKey = function() {
    return anotherPublicKey_;
  };

  this.getPublicKeyWithLessElements = function() {
    return publicKeyWithLessElements_;
  };

  this.getPublicKeyFromAnotherGroup = function() {
    return publicKeyFromAnotherGroup_;
  };

  this.getPlaintext = function() {
    return plaintext_;
  };

  this.getAnotherPlaintext = function() {
    return anotherPlaintext_;
  };

  this.getPlaintextWithLessElements = function() {
    return plaintextWithLessElements_;
  };

  this.getPlaintextFromAnotherGroup = function() {
    return plaintextFromAnotherGroup_;
  };

  this.getEncryptedElements = function() {
    return encryptedElements_;
  };

  this.getOtherEncryptedElements = function() {
    return otherEncryptedElements_;
  };

  this.getEncryptedElementsWithLessElements = function() {
    return encryptedElementsWithLessElements_;
  };

  this.getEncryptedElementsFromAnotherGroup = function() {
    return encryptedElementsFromAnotherGroup_;
  };

  this.getStringData = function() {
    return PLAINTEXT_PROOF_STRING_DATA;
  };

  this.getOtherStringData = function() {
    return OTHER_PLAINTEXT_PROOF_STRING_DATA;
  };

  this.getNumProgressChecks = function() {
    return NUM_PLAINTEXT_PROOF_PROGRESS_CHECKS;
  };
}
