/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var ExponentiationProofTestData = require('./data/exponentiation-proof-data');
var cryptoPolicy = require('scytl-cryptopolicy');
var zkProof = require('../lib/index');
var messageDigest = require('scytl-messagedigest');
var mathematical = require('scytl-mathematical');
var secureRandom = require('scytl-securerandom');
var codec = require('scytl-codec');

describe('The zero-knowledge proof module should be able to ...', function() {
  var proofService_;
  var group_;
  var anotherGroup_;
  var secret_;
  var anotherSecret_;
  var baseElements_;
  var otherBaseElements_;
  var exponentiatedElements_;
  var otherExponentiatedElements_;
  var data_;
  var otherData_;
  var proofHandler_;
  var proof_;
  var preComputation_;

  beforeAll(function() {
    proofService_ = zkProof.newService();

    var testData = new ExponentiationProofTestData();
    group_ = testData.getGroup();
    anotherGroup_ = testData.getAnotherGroup();
    secret_ = testData.getSecret();
    anotherSecret_ = testData.getAnotherSecret();
    baseElements_ = testData.getBaseElements();
    otherBaseElements_ = testData.getOtherBaseElements();
    exponentiatedElements_ = testData.getExponentiatedElements();
    otherExponentiatedElements_ = testData.getOtherExponentiatedElements();
    data_ = testData.getStringData();
    otherData_ = testData.getOtherStringData();

    proofHandler_ =
        proofService_.newExponentiationProofHandler(group_).init(baseElements_);
    proof_ = proofHandler_.generate(secret_, exponentiatedElements_);
    preComputation_ = proofHandler_.preCompute();
  });

  describe('create a zero-knowledge proof service that should be able to ..', function() {
    describe('create an exponentiation proof handler that should be able to', function() {
      it('generate and verify an exponentiation proof', function() {
        var verified = proofHandler_.verify(proof_, exponentiatedElements_);
        expect(verified).toBeTruthy();
      });

      it('generate and verify an exponentiation proof, using a specified cryptographic policy',
         function() {
           var policy = cryptoPolicy.newInstance();
           policy.proofs.messageDigest.algorithm =
               cryptoPolicy.options.proofs.messageDigest.algorithm.SHA512_224;

           var proofService = zkProof.newService({policy: policy});
           var proofHandler =
               proofService.newExponentiationProofHandler(group_).init(
                   baseElements_);

           var proof = proofHandler.generate(secret_, exponentiatedElements_);

           var verified = proofHandler.verify(proof, exponentiatedElements_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify an exponentiation proof, using a specified message digest service object',
         function() {
           var policy = cryptoPolicy.newInstance();
           policy.proofs.messageDigest.algorithm =
               cryptoPolicy.options.proofs.messageDigest.algorithm.SHA512_224;
           var messageDigestService =
               messageDigest.newService({policy: policy});

           var proofService =
               zkProof.newService({messageDigestService: messageDigestService});
           var proofHandler =
               proofService.newExponentiationProofHandler(group_).init(
                   baseElements_);

           var proof = proofHandler.generate(secret_, exponentiatedElements_);

           var verified = proofHandler.verify(proof, exponentiatedElements_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify an exponentiation proof, using a specified secure random service object',
         function() {
           var proofService = zkProof.newService(
               {secureRandomService: secureRandom.newService()});
           var proofHandler =
               proofService.newExponentiationProofHandler(group_).init(
                   baseElements_);

           var proof = proofHandler.generate(secret_, exponentiatedElements_);

           var verified = proofHandler.verify(proof, exponentiatedElements_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify an exponentiation proof, using a specified mathematical service object',
         function() {
           var proofService = zkProof.newService(
               {mathematicalService: mathematical.newService()});
           var proofHandler =
               proofService.newExponentiationProofHandler(group_).init(
                   baseElements_);

           var proof = proofHandler.generate(secret_, exponentiatedElements_);

           var verified = proofHandler.verify(proof, exponentiatedElements_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext proof, using provided auxiliary data',
         function() {
           // Data as string.
           var proof = proofHandler_.generate(
               secret_, exponentiatedElements_, {data: data_});
           var verified = proofHandler_.verify(
               proof, exponentiatedElements_, {data: data_});
           expect(verified).toBeTruthy();

           // Data as bytes.
           proof = proofHandler_.generate(
               secret_, exponentiatedElements_,
               {data: codec.utf8Encode(data_)});
           verified = proofHandler_.verify(
               proof, exponentiatedElements_, {data: codec.utf8Encode(data_)});
           expect(verified).toBeTruthy();
         });

      it('generate and verify an exponentiation proof, using a pre-computation',
         function() {
           var proof = proofHandler_.generate(
               secret_, exponentiatedElements_,
               {preComputation: preComputation_});

           var verified = proofHandler_.verify(proof, exponentiatedElements_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify an exponentiation proof, using empty string auxiliary data',
         function() {
           var proof = proofHandler_.generate(
               secret_, exponentiatedElements_, {data: ''});

           var verified =
               proofHandler_.verify(proof, exponentiatedElements_, {data: ''});
           expect(verified).toBeTruthy();
         });

      it('pre-compute, generate and verify a plaintext proof, using method chaining',
         function() {
           var preComputation =
               proofService_.newExponentiationProofHandler(group_)
                   .init(baseElements_)
                   .preCompute();

           var proof = proofService_.newExponentiationProofHandler(group_)
                           .init(baseElements_)
                           .generate(
                               secret_, exponentiatedElements_,
                               {data: data_, preComputation: preComputation});

           var verified =
               proofService_.newExponentiationProofHandler(group_)
                   .init(baseElements_)
                   .verify(proof, exponentiatedElements_, {data: data_});
           expect(verified).toBeTruthy();
         });

      it('create a new exponentiation ZeroKnowledgeProof object', function() {
        var hash = proof_.hash;
        var values = proof_.values;
        var proof = proofService_.newProof(hash, values);

        var verified = proofHandler_.verify(proof, exponentiatedElements_);
        expect(verified).toBeTruthy();
      });

      it('create a new exponentiation ZeroKnowledgeProofPreComputation object',
         function() {
           var exponents = preComputation_.exponents;
           var phiOutputs = preComputation_.phiOutputs;
           var preComputation =
               proofService_.newPreComputation(exponents, phiOutputs);

           var proof = proofHandler_.generate(
               secret_, exponentiatedElements_,
               {preComputation: preComputation});

           var verified = proofHandler_.verify(proof, exponentiatedElements_);
           expect(verified).toBeTruthy();
         });

      it('serialize and deserialize an exponentiation proof', function() {
        var proofJson = proof_.toJson();

        var proof = proofService_.newProof(proofJson);

        var verified = proofHandler_.verify(proof, exponentiatedElements_);
        expect(verified).toBeTruthy();
      });

      it('serialize and deserialize an exponentiation proof pre-computation',
         function() {
           var preComputationJson = preComputation_.toJson();

           var preComputation =
               proofService_.newPreComputation(preComputationJson);

           var proof = proofHandler_.generate(
               secret_, exponentiatedElements_,
               {preComputation: preComputation});

           var verified = proofHandler_.verify(proof, exponentiatedElements_);
           expect(verified).toBeTruthy();
         });

      it('fail to verify an exponentiation proof that was generated with a secret not used to exponentiate',
         function() {
           var proof =
               proofHandler_.generate(anotherSecret_, exponentiatedElements_);

           var verified = proofHandler_.verify(proof, exponentiatedElements_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify an exponentiation proof that was generated with base elements not used to exponentiate',
         function() {
           proofHandler_.init(otherBaseElements_);

           var proof = proofHandler_.generate(secret_, exponentiatedElements_);

           var verified = proofHandler_.verify(proof, exponentiatedElements_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify an exponentiation proof that was generated with base elements exponentiated to another secret',
         function() {
           var proof =
               proofHandler_.generate(secret_, otherExponentiatedElements_);

           var verified =
               proofHandler_.verify(proof, otherExponentiatedElements_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify an exponentiation proof when using base elements not used to exponentiate',
         function() {
           var verified = proofHandler_.init(otherBaseElements_)
                              .verify(proof_, exponentiatedElements_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify an exponentiation proof when using elements exponentiated to the wrong secret',
         function() {
           var verified =
               proofHandler_.verify(proof_, otherExponentiatedElements_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify an exponentiation proof when using the wrong data',
         function() {
           var proof = proofHandler_.generate(
               secret_, exponentiatedElements_, {data: data_});

           var verified = proofHandler_.verify(
               proof, exponentiatedElements_, {data: otherData_});
           expect(verified).toBeFalsy();
         });

      it('throw an error when generating a proof before the handler has been initialized with base elements',
         function() {
           var proofHandler =
               proofService_.newExponentiationProofHandler(group_);

           expect(function() {
             proofHandler.generate(secret_, exponentiatedElements_);
           }).toThrow();
         });

      it('throw an error when pre-computing a proof before the handler has been initialized with base elements',
         function() {
           var proofHandler =
               proofService_.newExponentiationProofHandler(group_);

           expect(function() {
             proofHandler.preCompute();
           }).toThrow();
         });

      it('throw an error when verifying a proof before the handler has been initialized with base elements',
         function() {
           var proofHandler =
               proofService_.newExponentiationProofHandler(group_);

           expect(function() {
             proofHandler.verify(proof_, exponentiatedElements_);
           }).toThrow();
         });
    });
  });
});
