/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var PlaintextEqualityProofTestData =
    require('./data/plaintext-equality-proof-data');
var cryptoPolicy = require('scytl-cryptopolicy');
var zkProof = require('../lib/index');
var messageDigest = require('scytl-messagedigest');
var mathematical = require('scytl-mathematical');
var secureRandom = require('scytl-securerandom');
var elGamal = require('scytl-elgamal');
var codec = require('scytl-codec');

describe('The zero-knowledge proof module should be able to ...', function() {
  var proofService_;
  var group_;
  var primarySecret_;
  var primaryCiphertext_;
  var secondarySecret_;
  var secondaryCiphertext_;
  var anotherPrimarySecret_;
  var anotherPrimaryCiphertext_;
  var anotherSecondarySecret_;
  var anotherSecondaryCiphertext_;
  var primaryPublicKey_;
  var secondaryPublicKey_;
  var anotherPrimaryPublicKey_;
  var anotherSecondaryPublicKey_;
  var data_;
  var otherData_;
  var proofHandler_;
  var proof_;
  var preComputation_;

  beforeAll(function() {
    proofService_ = zkProof.newService();

    var testData = new PlaintextEqualityProofTestData();
    var elGamalService = elGamal.newService();
    group_ = testData.getGroup();
    var primaryEncryptedElements = testData.getPrimaryEncryptedElements();
    primarySecret_ = primaryEncryptedElements.secret;
    primaryCiphertext_ = elGamalService.newEncryptedElements(
        primaryEncryptedElements.gamma, primaryEncryptedElements.phis);
    var secondaryEncryptedElements = testData.getSecondaryEncryptedElements();
    secondarySecret_ = secondaryEncryptedElements.secret;
    secondaryCiphertext_ = elGamalService.newEncryptedElements(
        secondaryEncryptedElements.gamma, secondaryEncryptedElements.phis);
    var otherPrimaryEncryptedElements =
        testData.getOtherPrimaryEncryptedElements();
    anotherPrimarySecret_ = otherPrimaryEncryptedElements.secret;
    anotherPrimaryCiphertext_ = elGamalService.newEncryptedElements(
        otherPrimaryEncryptedElements.gamma,
        otherPrimaryEncryptedElements.phis);
    var otherSecondaryEncryptedElements =
        testData.getOtherSecondaryEncryptedElements();
    anotherSecondarySecret_ = otherSecondaryEncryptedElements.secret;
    anotherSecondaryCiphertext_ = elGamalService.newEncryptedElements(
        otherSecondaryEncryptedElements.gamma,
        otherSecondaryEncryptedElements.phis);
    primaryPublicKey_ = testData.getPrimaryPublicKey();
    secondaryPublicKey_ = testData.getSecondaryPublicKey();
    anotherPrimaryPublicKey_ = testData.getAnotherPrimaryPublicKey();
    anotherSecondaryPublicKey_ = testData.getAnotherSecondaryPublicKey();
    data_ = testData.getStringData();
    otherData_ = testData.getOtherStringData();

    proofHandler_ = proofService_.newPlaintextEqualityProofHandler(group_).init(
        primaryPublicKey_, secondaryPublicKey_);
    proof_ = proofHandler_.generate(
        primarySecret_, secondarySecret_, primaryCiphertext_,
        secondaryCiphertext_);
    preComputation_ = proofHandler_.preCompute();
  });

  beforeEach(function() {
    proofHandler_ = proofService_.newPlaintextEqualityProofHandler(group_).init(
        primaryPublicKey_, secondaryPublicKey_);
  });

  describe('create a zero-knowledge proof service that should be able to ..', function() {
    describe('create a plaintext equality proof handler that should be able to', function() {
      it('generate and verify a plaintext equality proof', function() {
        var verified = proofHandler_.verify(
            proof_, primaryCiphertext_, secondaryCiphertext_);
        expect(verified).toBeTruthy();
      });

      it('generate and verify a plaintext equality proof, using a specified cryptographic policy',
         function() {
           var policy = cryptoPolicy.newInstance();
           policy.proofs.messageDigest.algorithm =
               cryptoPolicy.options.proofs.messageDigest.algorithm.SHA512_224;

           var proofService = zkProof.newService({policy: policy});
           var proofHandler =
               proofService.newPlaintextEqualityProofHandler(group_).init(
                   primaryPublicKey_, secondaryPublicKey_);

           proof_ = proofHandler.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler.verify(
               proof_, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext equality proof, using a specified message digest service',
         function() {
           var policy = cryptoPolicy.newInstance();
           policy.proofs.messageDigest.algorithm =
               cryptoPolicy.options.proofs.messageDigest.algorithm.SHA512_224;
           var messageDigestService =
               messageDigest.newService({policy: policy});

           var proofService =
               zkProof.newService({messageDigestService: messageDigestService});
           var proofHandler =
               proofService.newPlaintextEqualityProofHandler(group_).init(
                   primaryPublicKey_, secondaryPublicKey_);

           proof_ = proofHandler.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler.verify(
               proof_, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext equality proof, using a specified secure random service object',
         function() {
           var proofService = zkProof.newService(
               {secureRandomService: secureRandom.newService()});
           var proofHandler =
               proofService.newPlaintextEqualityProofHandler(group_).init(
                   primaryPublicKey_, secondaryPublicKey_);

           proof_ = proofHandler.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler.verify(
               proof_, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext equality proof, using a specified mathematical service object',
         function() {
           var proofService = zkProof.newService(
               {mathematicalService: mathematical.newService()});
           var proofHandler =
               proofService.newPlaintextEqualityProofHandler(group_).init(
                   primaryPublicKey_, secondaryPublicKey_);

           proof_ = proofHandler.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler.verify(
               proof_, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext proof, using provided auxiliary data',
         function() {
           // Data as string.
           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_, {data: data_});
           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_, {data: data_});
           expect(verified).toBeTruthy();

           // Data as bytes.
           proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_, {data: codec.utf8Encode(data_)});
           verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_,
               {data: codec.utf8Encode(data_)});
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext equality proof, using a pre-computation',
         function() {
           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_, {preComputation: preComputation_});

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('generate and verify a plaintext equality proof, using empty auxiliary data',
         function() {
           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_, {data: ''});

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_, {data: ''});
           expect(verified).toBeTruthy();
         });

      it('pre-compute, generate and verify a plaintext proof, using method chaining',
         function() {
           var preComputation =
               proofService_.newPlaintextEqualityProofHandler(group_)
                   .init(primaryPublicKey_, secondaryPublicKey_)
                   .preCompute();

           var proof = proofService_.newPlaintextEqualityProofHandler(group_)
                           .init(primaryPublicKey_, secondaryPublicKey_)
                           .generate(
                               primarySecret_, secondarySecret_,
                               primaryCiphertext_, secondaryCiphertext_,
                               {data: data_, preComputation: preComputation});

           var verified = proofService_.newPlaintextEqualityProofHandler(group_)
                              .init(primaryPublicKey_, secondaryPublicKey_)
                              .verify(
                                  proof, primaryCiphertext_,
                                  secondaryCiphertext_, {data: data_});
           expect(verified).toBeTruthy();
         });

      it('create a new plaintext equality ZeroKnowledgeProof object',
         function() {
           var hash = proof_.hash;
           var values = proof_.values;
           var proof = proofService_.newProof(hash, values);

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('create a new plaintext equality ZeroKnowledgeProofPreComputation object',
         function() {
           var exponents = preComputation_.exponents;
           var phiOutputs = preComputation_.phiOutputs;
           var preComputation =
               proofService_.newPreComputation(exponents, phiOutputs);

           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_, {preComputation: preComputation});

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('serialize and deserialize a plaintext equality proof', function() {
        var proofJson = proof_.toJson();

        var proof = proofService_.newProof(proofJson);

        var verified = proofHandler_.verify(
            proof, primaryCiphertext_, secondaryCiphertext_);
        expect(verified).toBeTruthy();
      });

      it('serialize and deserialize a plaintext equality proof pre-computation',
         function() {
           var preComputationJson = preComputation_.toJson();

           var preComputation =
               proofService_.newPreComputation(preComputationJson);

           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_, {preComputation: preComputation});

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeTruthy();
         });

      it('fail to verify a plaintext equality proof that was generated with a primary secret not used to generate the primary ciphertext',
         function() {
           var proof = proofHandler_.generate(
               anotherPrimarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof that was generated with a secondary secret not used to generate the secondary ciphertext',
         function() {
           var proof = proofHandler_.generate(
               primarySecret_, anotherSecondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof that was generated with a primary publicKey not used to generate the primary ciphertext',
         function() {
           proofHandler_.init(anotherPrimaryPublicKey_, secondaryPublicKey_);

           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof that was generated with a secondary publicKey not used to generate the secondary ciphertext',
         function() {
           proofHandler_.init(primaryPublicKey_, anotherSecondaryPublicKey_);

           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof that was generated with a primary ciphertext not generated from the primary secret',
         function() {
           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, anotherPrimaryCiphertext_,
               secondaryCiphertext_);

           var verified = proofHandler_.verify(
               proof, anotherPrimaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof that was generated with a secondary ciphertext not generated from the secondary secret',
         function() {
           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               anotherSecondaryCiphertext_);

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, anotherSecondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof when using a primary public key not used to generate the proof',
         function() {
           var verified =
               proofHandler_.init(anotherPrimaryPublicKey_, secondaryPublicKey_)
                   .verify(proof_, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof when using a secondary public key not used to generate the proof',
         function() {
           var verified =
               proofHandler_.init(primaryPublicKey_, anotherSecondaryPublicKey_)
                   .verify(proof_, primaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof when using a primary ciphertext not used to generate the proof',
         function() {
           var verified = proofHandler_.verify(
               proof_, anotherPrimaryCiphertext_, secondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof when using a secondary ciphertext not used to generate the proof',
         function() {
           var verified = proofHandler_.verify(
               proof_, primaryCiphertext_, anotherSecondaryCiphertext_);
           expect(verified).toBeFalsy();
         });

      it('fail to verify a plaintext equality proof when using auxiliary data not used to generate the proof',
         function() {
           var proof = proofHandler_.generate(
               primarySecret_, secondarySecret_, primaryCiphertext_,
               secondaryCiphertext_, {data: data_});

           var verified = proofHandler_.verify(
               proof, primaryCiphertext_, secondaryCiphertext_,
               {data: otherData_});
           expect(verified).toBeFalsy();
         });

      it('throw an error when generating a proof before the handler has been initialized with any pubilc key',
         function() {
           var proofHandler =
               proofService_.newPlaintextEqualityProofHandler(group_);

           expect(function() {
             proofHandler.generate(
                 primarySecret_, secondarySecret_, primaryCiphertext_,
                 secondaryCiphertext_);
           }).toThrow();
         });

      it('throw an error when pre-computing a proof before the handler has been initialized with any pubilc key',
         function() {
           var proofHandler =
               proofService_.newPlaintextEqualityProofHandler(group_);

           expect(function() {
             proofHandler.preCompute();
           }).toThrow();
         });

      it('throw an error when verifying a proof before the handler has been initialized with any pubilc key',
         function() {
           var proofHandler =
               proofService_.newPlaintextEqualityProofHandler(group_);

           expect(function() {
             proofHandler.verify(
                 proof_, primaryCiphertext_, secondaryCiphertext_);
           }).toThrow();
         });
    });
  });
});
