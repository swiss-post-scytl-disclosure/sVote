# Cryptolib-Java - A quick start to the Scytl cryptographic library implemented in Java

# Released artifacts
Stored in Nexus at https://nexus.scytl.net/content/repositories/releases/com/scytl/cryptolib

# Maven setup
Dependency on cryptoLib can be specific to a module:

``` xml
<dependency>
  <artifactId>cryptolib-api-symmetric</artifactId>
  <groupId>com.scytl.cryptolib</groupId>
  <version>${cryptolib.version}</version>
</dependency>
```

 or generic:

``` xml
<dependency>
  <artifactId>cryptolib-all</artifactId>
  <groupId>com.scytl.cryptolib</groupId>
  <version>${cryptolib.version}</version>
</dependency>
```

# Java imports
Theses examples apply to subsequent code snippets provided.

``` java
import java.security.Security;
import java.security.KeyPair;
import java.io.FileInputStream;
import javax.crypto.SecretKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import com.scytl.cryptolib.symmetric.service.SymmetricService;
import com.scytl.cryptolib.asymmetric.service.AsymmetricService;
import com.scytl.cryptolib.certificates.service.CertificatesService;
import com.scytl.cryptolib.certificates.bean.CertificateData;
import com.scytl.cryptolib.certificates.bean.RootCertificateData;
```

# Symmetric service

## How to getSecretKeyForEncryption

``` java
public static void generateSecretKey() {
  Security.addProvider(new BouncyCastleProvider());
  SymmetricService symmetricService = new SymmetricService();
  SecretKey secretKeyForEncryption = symmetricService.getSecretKeyForEncryption();
}
```

## How to getSecretKeyForHmac

``` java
public static void generateSecretKeyForHMAC() {
  Security.addProvider(new BouncyCastleProvider());
  SymmetricService symmetricService = new SymmetricService();
  SecretKey secretKeyForHMAC = symmetricService.getSecretKeyForHmac();
}
```

## How to encrypt and decrypt

``` java
public static void encryptAndDecryptSymmetric() {
  Security.addProvider(new BouncyCastleProvider());
  SymmetricService symmetricService = new SymmetricService();
  byte[] encryptedData = symmetricService.encrypt(secretKeyForEncryption, plainText.getBytes());
  byte[] decryptedData = symmetricService.decrypt(secretKeyForEncryption, encryptedData);
}
```

## How to getMac and verifyMac

``` java
public static void getAndVerifyHMAC() {
  Security.addProvider(new BouncyCastleProvider());
  SymmetricService symmetricService = new SymmetricService();
  byte[] mac = symmetricService.getMac(secretKeyForHMAC, plainText.getBytes());
  boolean verificationResult = symmetricService.verifyMac(secretKeyForHMAC, mac, plainText.getBytes());
}
```

# Asymmetric service

## How to getKeyPairForEncryption

``` java
public static void createKeypair() {
  Security.addProvider(new BouncyCastleProvider());
  AsymmetricService asymmetricService = new AsymmetricService();
  KeyPair keyPairForEncryption = asymmetricService.getKeyPairForEncryption();
}
```

## How to encrypt and decrypt

``` java
public static void encryptAndDecryptAsymmetric() {
  Security.addProvider(new BouncyCastleProvider());
  AsymmetricService asymmetricService = new AsymmetricService();
  byte[] encryptedData = asymmetricService.encrypt(keyPairForEncryption.getPublic(), plainText.getBytes());
  byte[] decryptedData = asymmetricService.decrypt(keyPairForEncryption.getPrivate(), encryptedData);
}
```

## How to sign and verifySignature

``` java
public static void signAndVerifySignature() {
  Security.addProvider(new BouncyCastleProvider());
  AsymmetricService asymmetricService = new AsymmetricService();
  KeyPair keyPairForSigning = asymmetricService.getKeyPairForSigning();
  byte[] signatureBytes = asymmetricService.sign(keyPairForSigning.getPrivate(), plainText.getBytes());
  boolean verificationResult = asymmetricService.verifySignature(signatureBytes, keyPairForSigning.getPublic(), plainText.getBytes());
}
```

## How to verifyXmlSignature
``` java
public static void verifyXmlSignature() {
  Security.addProvider(new BouncyCastleProvider());
  AsymmetricService asymmetricService = new AsymmetricService();
  FileInputStream signedXmlInStream = new FileInputStream(signedXmlFile);
  boolean verificationResult = asymmetricService.verifyXmlSignature(keyPairForSigning.getPublic(), signedXmlInStream, "xmlNodeNameSigned");
}
```

# Certificates service

## How to createSignX509Certificate
``` java
public static void createSignX509Certificate() {
  CertificatesService certificatesService = new CertificatesService();
  CertificateData userCertificate = new CertificateData();
  userCertificate.setSubjectPublicKey(userKeyPair.getPublic());
  certificatesService.createSignX509Certificate(userCertificate, rootKeyPair.getPrivate());
}
```

## How to createEncryptionX509Certificate
``` java
public static void createEncryptionX509Certificate() {
  certificatesService.createEncryptionX509Certificate(userCertificate, rootKeyPair.getPrivate());
}
```

## How to createRootAuthorityX509Certificate
``` java
public static void createRootAuthorityX509Certificate() {
  RootCertificateData rootCertificateData = new RootCertificateData();
  rootCertificateData.setSubjectPublicKey(rootKeyPair.getPublic());
  certificatesService.createRootAuthorityX509Certificate(rootCertificateData, rootKeyPair.getPrivate());
}
```

## How to createIntermediateAuthorityX509Certificate
``` java
public static void createIntermediateAuthorityX509Certificate() {
  CertificateData intermediateAuthorityCertificateData = new CertificateData();
  intermediateAuthorityCertificateData.setSubjectPublicKey(intermediateAuthorityKeyPair.getPublic());
  certificatesService.createIntermediateAuthorityX509Certificate(intermediateAuthorityCertificateData, rootKeyPair.getPrivate());
}
```