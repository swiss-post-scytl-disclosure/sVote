/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var mathematical = require('../../lib/index');
var CommonTestData = require('./common-data');
var secureRandom = require('scytl-securerandom');
var forge = require('node-forge');

module.exports = ValidationTestData;

var BigInteger = forge.jsbn.BigInteger;

/**
 * Provides the input validation data needed by the mathematical service unit
 * tests.
 */
function ValidationTestData() {
  var commonTestData = new CommonTestData();

  var p = commonTestData.getP();
  var minCertaintyLevel = commonTestData.getMinimumCertaintyLevel();
  var multiGroupElementValues = commonTestData.multiGroupElementValues();

  var nonObject_ = 999;
  var emptyObject_ = {};
  var nonSecureRandomService_ = mathematical.newService();

  var nonBoolean_ = '';
  var nonNumber_ = '';
  var nonPositiveNumber_ = 0;
  var nonString_ = 0;
  var nonJsonString_ = 'Not a JSON string';
  var nonArray_ = '';

  var nonMathObject_ = secureRandom.newService();
  var nonMathObjectArray_ = [];
  nonMathObjectArray_.push(nonMathObject_ + '1');
  nonMathObjectArray_.push(nonMathObject_ + '2');
  nonMathObjectArray_.push(nonMathObject_ + '3');

  var tooSmallModulus_ = new BigInteger('2');
  var tooSmallOrder_ = BigInteger.ZERO;
  var tooLargeOrder_ = p;
  var tooSmallGenerator_ = BigInteger.ONE;
  var tooLargeGenerator_ = p;
  var tooSmallElementValue_ = BigInteger.ZERO;
  var tooLargeElementValue_ = p;

  var tooSmallPBitLength_ = 1;
  var tooLowCertaintyLevel_ = minCertaintyLevel - 1;
  var tooLargeNumMembersRequired_ = multiGroupElementValues.length + 1;

  this.getNonObject = function() {
    return nonObject_;
  };

  this.getEmptyObject = function() {
    return emptyObject_;
  };

  this.getNonSecureRandomService = function() {
    return nonSecureRandomService_;
  };

  this.getNonBoolean = function() {
    return nonBoolean_;
  };

  this.getNonNumber = function() {
    return nonNumber_;
  };

  this.getNonPositiveNumber = function() {
    return nonPositiveNumber_;
  };

  this.getNonString = function() {
    return nonString_;
  };

  this.getNonJsonString = function() {
    return nonJsonString_;
  };

  this.getNonArray = function() {
    return nonArray_;
  };

  this.getNonMathObject = function() {
    return nonMathObject_;
  };

  this.getNonMathObjectArray = function() {
    return nonMathObjectArray_;
  };

  this.getTooSmallModulus = function() {
    return tooSmallModulus_;
  };

  this.getTooSmallOrder = function() {
    return tooSmallOrder_;
  };

  this.getTooLargeOrder = function() {
    return tooLargeOrder_;
  };

  this.getTooSmallGenerator = function() {
    return tooSmallGenerator_;
  };

  this.getTooLargeGenerator = function() {
    return tooLargeGenerator_;
  };

  this.getTooSmallElementValue = function() {
    return tooSmallElementValue_;
  };

  this.getTooLargeElementValue = function() {
    return tooLargeElementValue_;
  };

  this.getTooSmallModulusBitLength = function() {
    return tooSmallPBitLength_;
  };

  this.getTooLowCertaintyLevel = function() {
    return tooLowCertaintyLevel_;
  };

  this.getTooLargeNumMembersRequired = function() {
    return tooLargeNumMembersRequired_;
  };
}
