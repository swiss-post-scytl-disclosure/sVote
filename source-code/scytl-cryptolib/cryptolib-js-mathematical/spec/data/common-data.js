/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var mathematical = require('../../lib/index');
var constants = require('../../lib/constants');
var forge = require('node-forge');

module.exports = CommonTestData;

var BigInteger = forge.jsbn.BigInteger;

/**
 * Provides the common data needed by the mathematical service unit tests.
 */
function CommonTestData() {
  var mathService = mathematical.newService();

  var p_ = new BigInteger('23');
  var q_ = new BigInteger('11');
  var g_ = new BigInteger('2');
  var group_ = mathService.newZpSubgroup(p_, q_, g_);
  var qrGroup_ = mathService.newQuadraticResidueGroup(p_, g_);

  var anotherP_ = new BigInteger(
      '22115396014581083143553195792384915983375949993753365299824374182778648887213105675373952839695834312125954513956465041609718392131788741228259266178434294275575978809365653903991346366383637500069010795276889266879661859362340655085050484891149389971190945080643692669920059928787982790808824970597361825060408436004650739931286605415552480363242364319544423959620330010413847743583301091182870021747196927889763205955078275353307312408480155002017730305672359814713050632922694161739281353676187308501682728826507718002850698488786266115311952718697872338756199429262811547458969260835283688979875294699655014592833');
  var anotherQ_ = new BigInteger(
      '79783631514788897688995869254536231266441581267248557197866166084121363399063');
  var anotherG_ = new BigInteger(
      '9484337033848536557358945372517536146444081505783459082983213315915020922388822299017798461933010391712259501201285084235930755858547311934865298396110947362913733900057881561201738951725697128822802249693116611294569213895789823297341865759919045291915705783128173481781221176839434866976235031358969583527093743179257827355453820268832913547364509208164670680381449515691300440305333919098105147193087681183020489724214850151272955879869448467832934822639783688344322183540621808345771717172404141548242866493539424279296365911355186761897533654055060787119955689115570505208857288933136456026541073605147828202749');
  var anotherGroup_ =
      mathService.newZpSubgroup(anotherP_, anotherQ_, anotherG_);

  var elementValue_ = new BigInteger('3');
  var element_ = mathService.newZpGroupElement(p_, q_, elementValue_);

  var exponentValue_ = new BigInteger('2');
  var exponent_ = mathService.newExponent(q_, exponentValue_);

  var element1Value_ = new BigInteger('2');
  var element2Value_ = new BigInteger('3');
  var element3Value_ = new BigInteger('9');
  var elements_ = [];
  elements_[0] = mathService.newZpGroupElement(p_, q_, element1Value_);
  elements_[1] = mathService.newZpGroupElement(p_, q_, element2Value_);
  elements_[2] = mathService.newZpGroupElement(p_, q_, element3Value_);

  var exponent1Value = new BigInteger('2');
  var exponent2Value = new BigInteger('4');
  var exponent3Value = new BigInteger('12');
  var exponents_ = [];
  exponents_[0] = mathService.newExponent(q_, exponent1Value);
  exponents_[1] = mathService.newExponent(q_, exponent2Value);
  exponents_[2] = mathService.newExponent(q_, exponent3Value);

  var elementFromAnotherGroup_ =
      mathService.newZpGroupElement(anotherP_, anotherQ_, elementValue_);
  var exponentFromAnotherGroup_ =
      mathService.newExponent(anotherQ_, exponentValue_);

  var pLarge = new BigInteger(
      '22588801568735561413035633152679913053449200833478689904902877673687016391844561133376032309307885537704777240609087377993341380751697605235541131273868440070920362148431866829787784445019147999379498503693247429579480289226602748397335327890884464685051682703709742724121783217827040722415360103179289160056581759372475845985438977307323570530753362027145384124771826114651710264766437273044759690955051982839684910462609395741692689616014805965573558015387956017183286848440036954926101719205598449898400180082053755864070690174202432196678045052744337832802051787273056312757384654145455745603262082348042780103679');
  var qrGroupLarge_ = mathService.newQuadraticResidueGroup(pLarge, g_);

  var exponent1LargeValue_ = new BigInteger(
      '129547331161844130899924173735104728702220700682642138851022406761777994632728337153635583150724889632689084497665212926336088840842855597736181764744122977345480665146165524664735861913761423291109155186483836806324970977794125768575887671259015293979900588616487195411477129839925141145884519189238456836206862720534398725375349481201999965487100563522135988094619173050092410477941592825803862292425503577691441721033806466916501473758812704373810862790693842410609705251142958265348495650713103821889837809522418329172422392659115758749365074645804533890934339070499112165048929164809093509978635413193830265570');
  var exponent1Large_ = mathService.newExponent(q_, exponent1LargeValue_);
  var exponent2LargeValue_ = new BigInteger(
      '7533608948657130117198364792462971241599837777629317286887202126604432685322243335715285630236984093165224740099563982076007648728594011814311120331313200895892632245003875959905589367483155794698953057915454892457379023058909518440394905695255170407948701433659375513788930419544151891518689331636455316691827048845766063478445484089208223819510250476623436782404385896353292653750744111841510466992101967325081746747885221256692886521245630131046183525331085171958919416648097910028887247120874015121272894655357197879292253781160838729753609784455892951965051119314203469298666741439497193677501351952497150273389');
  var exponent2Large_ = mathService.newExponent(q_, exponent2LargeValue_);

  var multiGroupElementValueArray_ = [3, 4, 5, 6, 7, 8, 9, 12, 13, 16, 18];
  var multiGroupElementValues_ = [];
  var multiGroupElements_ = [];
  var value;
  var element;
  for (var i = 0; i < multiGroupElementValueArray_.length; i++) {
    value = new BigInteger('' + multiGroupElementValueArray_[i]);
    element = mathService.newZpGroupElement(p_, q_, value);
    multiGroupElementValues_.push(value);
    multiGroupElements_.push(element);
  }

  this.getP = function() {
    return p_;
  };

  this.getQ = function() {
    return q_;
  };

  this.getG = function() {
    return g_;
  };

  this.getGroup = function() {
    return group_;
  };

  this.getAnotherP = function() {
    return anotherP_;
  };

  this.getAnotherQ = function() {
    return anotherQ_;
  };

  this.getAnotherG = function() {
    return anotherG_;
  };

  this.getAnotherGroup = function() {
    return anotherGroup_;
  };

  this.getQuadraticResidueGroup = function() {
    return qrGroup_;
  };

  this.getElementValue = function() {
    return elementValue_;
  };

  this.getElement = function() {
    return element_;
  };

  this.getExponentValue = function() {
    return exponentValue_;
  };

  this.getExponent = function() {
    return exponent_;
  };

  this.getElement1Value = function() {
    return element1Value_;
  };

  this.getElement2Value = function() {
    return element2Value_;
  };

  this.getElement3Value = function() {
    return element3Value_;
  };

  this.getElements = function() {
    return elements_;
  };

  this.getExponents = function() {
    return exponents_;
  };

  this.getElementFromAnotherGroup = function() {
    return elementFromAnotherGroup_;
  };

  this.getExponentFromAnotherGroup = function() {
    return exponentFromAnotherGroup_;
  };

  this.getLargeQuadraticResidueGroup = function() {
    return qrGroupLarge_;
  };

  this.getExponent1LargeValue = function() {
    return exponent1LargeValue_;
  };

  this.getExponent1Large = function() {
    return exponent1Large_;
  };

  this.getExponent2LargeValue = function() {
    return exponent2LargeValue_;
  };

  this.getExponent2Large = function() {
    return exponent2Large_;
  };

  this.getMinimumCertaintyLevel = function() {
    return constants.MINIMUM_PRIME_CERTAINTY_LEVEL;
  };

  this.getMultiGroupElementValueArray = function() {
    return multiGroupElementValueArray_;
  };

  this.multiGroupElementValues = function() {
    return multiGroupElementValues_;
  };

  this.getMultiGroupElements = function() {
    return multiGroupElements_;
  };
}
