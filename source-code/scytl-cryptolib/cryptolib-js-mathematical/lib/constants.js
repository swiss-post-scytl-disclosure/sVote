/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

/**
 * Defines constants for the mathematical module.
 */
module.exports = Object.freeze(
    {MINIMUM_PRIME_CERTAINTY_LEVEL: 80, SHORT_EXPONENT_BIT_LENGTH: 256});
