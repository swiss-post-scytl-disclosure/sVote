/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var MathematicalService = require('./service');
var validator = require('./input-validator');

module.exports = {
  /**
   * Creates a new MathematicalService object, which encapsulates a
   * mathematical service.
   *
   * @function newService
   * @global
   * @param {Object}
   *            [options] An object containing optional arguments.
   * @param {SecureRandomService}
   *            [options.secureRandomService=Created internally] The secure
   *            random service to use.
   * @returns {MathematicalService} The new MathematicalService object.
   * @throws {Error}
   *             If the input data validation fails.
   * @example <caption> How to use a provided secure random service, that was
   *          initialized with a chosen seed</caption>
   *
   * var mathematical = require('scytl-mathematical');
   * var secureRandom = require('scytl-securerandom');
   *
   * var mySecureRandomService = secureRandom.newService({prngSeed: mySeed});
   *
   * var mathService_ = mathematical.newService({secureRandomService:
   * mySecureRandomService});
   */
  newService: function(options) {
    checkData(options);

    return new MathematicalService(options);
  }
};

function checkData(options) {
  options = options || {};

  if (typeof options.secureRandomService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.secureRandomService,
        'Secure random service object provided to mathematical service');
  }
}
