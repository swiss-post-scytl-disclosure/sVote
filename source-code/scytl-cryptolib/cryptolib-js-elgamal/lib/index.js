/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var ElGamalCryptographyService = require('./service');
var validator = require('./input-validator');

module.exports = {
  /**
   * Creates a new ElGamalCryptographyService object, which encapsulates an
   * ElGamal cryptography service.
   *
   * @function newService
   * @global
   * @param {Object}
   *            [options] An object containing optional arguments.
   * @param {SecureRandomService}
   *            [options.secureRandomService=Created internally] The secure
   *            random service to use.
   * @param {MathematicalService}
   *            [options.mathematicalService=Created internally] The
   *            mathematical service to use.
   * @returns {ElGamalCryptographyService} The new ElGamalCryptographyService
   *          object.
   * @throws {Error}
   *             If the input data validation fails.
   * @example <caption> How to use a provided secure random service, that was
   *          initialized with a chosen seed</caption>
   *
   * var elGamal = require('scytl-elgamal');
   * var secureRandom = require('scytl-securerandom');
   *
   * var mySecureRandomService = secureRandom.newService({prngSeed: mySeed});
   *
   * var elGamalService = elGamal.newService({secureRandomService:
   * mySecureRandomService});
   */
  newService: function(options) {
    checkData(options);

    return new ElGamalCryptographyService(options);
  }
};

function checkData(options) {
  options = options || {};

  if (typeof options.secureRandomService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.secureRandomService,
        'Secure random service object provided to ElGamal cryptography service');
  }

  if (typeof options.mathematicalService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.mathematicalService,
        'Mathematical service object provided to ElGamal cryptography service');
  }
}
