/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var codec = require('scytl-codec');

module.exports = ElGamalPublicKey;

/**
 * @class ElGamalPublicKey
 * @classdesc Encapsulates an ElGamal public key. To instantiate this object,
 *            use the method {@link ElGamalCryptographyService.newPublicKey}.
 * @property {ZpSubgroup} group The Zp subgroup to which the Zp group elements
 *           of this public key belong.
 * @property {ZpGroupElement[]} elements The Zp group elements that comprise
 *           this public key.
 */
function ElGamalPublicKey(group, elements) {
  this.group = group;
  Object.freeze(this.elements = elements);

  Object.freeze(this);
}

ElGamalPublicKey.prototype = {
  /**
   * Serializes this object into a JSON string representation.
   * <p>
   * <b>IMPORTANT:</b> This serialization must be exactly the same as the
   * corresponding serialization in the library <code>cryptoLib</code>,
   * implemented in Java, since the two libraries are expected to communicates
   * with each other via these serializations.
   *
   * @function toJson
   * @memberof ElGamalPublicKey
   * @returns {string} The JSON string representation of this object.
   */
  toJson: function() {
    var gB64 = codec.base64Encode(this.group.generator.value);
    var pB64 = codec.base64Encode(this.group.p);
    var qB64 = codec.base64Encode(this.group.q);

    var elementsB64 = [];
    for (var i = 0; i < this.elements.length; i++) {
      elementsB64.push(codec.base64Encode(this.elements[i].value));
    }

    return JSON.stringify({
      publicKey:
          {zpSubgroup: {g: gB64, p: pB64, q: qB64}, elements: elementsB64}
    });
  }
};
