/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var CommonTestData = require('./common-data');
var elGamal = require('../../lib/index');
var mathematical = require('scytl-mathematical');

module.exports = ValidationTestData;

/**
 * Provides the input validation data needed by the ElGamal service unit tests.
 */
function ValidationTestData() {
  var commonTestData = new CommonTestData();

  var nonObject_ = 999;
  var emptyObject_ = {};
  var nonSecureRandomService_ = elGamal.newService();
  var nonSecureRandomGenerator_ = elGamal.newService();
  var nonMathematicalService_ = elGamal.newService();

  var nonBoolean_ = '';
  var nonString_ = 999;
  var nonJsonString_ = 'Not a JSON string';
  var nonArray_ = '';
  var nonStringArray_ = [];
  nonStringArray_.push(1);
  nonStringArray_.push(2);
  nonStringArray_.push(3);

  var nonMathObject_ = elGamal.newService();
  var nonMathObjectArray_ = [];
  nonMathObjectArray_.push(nonMathObject_ + '1');
  nonMathObjectArray_.push(nonMathObject_ + '2');
  nonMathObjectArray_.push(nonMathObject_ + '3');
  var nonElGamalObject_ = mathematical.newService();

  var publicKeyFromAnotherGroup_ = commonTestData.getLargePublicKey();
  var privateKeyFromAnotherGroup_ = commonTestData.getLargePrivateKey();
  var elementsFromAnotherGroup_ =
      commonTestData.getElementsFromLargeZpSubgroup();
  var exponentsFromAnotherGroup_ =
      commonTestData.getExponentsFromLargeZpSubgroup();

  this.getNonObject = function() {
    return nonObject_;
  };

  this.getEmptyObject = function() {
    return emptyObject_;
  };

  this.getNonSecureRandomService = function() {
    return nonSecureRandomService_;
  };

  this.getNonSecureRandomGenerator = function() {
    return nonSecureRandomGenerator_;
  };

  this.getNonMathematicalService = function() {
    return nonMathematicalService_;
  };

  this.getNonBoolean = function() {
    return nonBoolean_;
  };

  this.getNonString = function() {
    return nonString_;
  };

  this.getNonJsonString = function() {
    return nonJsonString_;
  };

  this.getNonArray = function() {
    return nonArray_;
  };

  this.getNonStringArray = function() {
    return nonStringArray_;
  };

  this.getNonMathObject = function() {
    return nonMathObject_;
  };

  this.getNonMathObjectArray = function() {
    return nonMathObjectArray_;
  };

  this.getNonElGamalObject = function() {
    return nonElGamalObject_;
  };

  this.getPublicKeyFromAnotherGroup = function() {
    return publicKeyFromAnotherGroup_;
  };

  this.getPrivateKeyFromAnotherGroup = function() {
    return privateKeyFromAnotherGroup_;
  };

  this.getElementsFromAnotherGroup = function() {
    return elementsFromAnotherGroup_;
  };

  this.getExponentsFromAnotherGroup = function() {
    return exponentsFromAnotherGroup_;
  };
}
