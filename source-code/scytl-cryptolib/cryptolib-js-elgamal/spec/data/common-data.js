/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var elGamal = require('../../lib/index');
var mathematical = require('scytl-mathematical');
var forge = require('node-forge');

module.exports = CommonTestData;

var BigInteger = forge.jsbn.BigInteger;

/**
 * Provides the common data needed by the ElGamal service unit tests.
 */
function CommonTestData() {
  var SHORT_EXPONENT_BIT_LENGTH = 256;

  var NUM_GROUP_ELEMENTS = 2;
  var NUM_SINGLE_GROUP_ELEMENTS = 5;
  var P_STR = '23';
  var Q_STR = '11';
  var G_STR = '2';
  var KEY_ELEMENT_1_VALUE_STR = '2';
  var KEY_ELEMENT_2_VALUE_STR = '4';
  var KEY_EXPONENT_1_VALUE_STR = '1';
  var KEY_EXPONENT_2_VALUE_STR = '2';
  var P_LARGE_STR =
      '22588801568735561413035633152679913053449200833478689904902877673687016391844561133376032309307885537704777240609087377993341380751697605235541131273868440070920362148431866829787784445019147999379498503693247429579480289226602748397335327890884464685051682703709742724121783217827040722415360103179289160056581759372475845985438977307323570530753362027145384124771826114651710264766437273044759690955051982839684910462609395741692689616014805965573558015387956017183286848440036954926101719205598449898400180082053755864070690174202432196678045052744337832802051787273056312757384654145455745603262082348042780103679';
  var Q_LARGE_STR =
      '11294400784367780706517816576339956526724600416739344952451438836843508195922280566688016154653942768852388620304543688996670690375848802617770565636934220035460181074215933414893892222509573999689749251846623714789740144613301374198667663945442232342525841351854871362060891608913520361207680051589644580028290879686237922992719488653661785265376681013572692062385913057325855132383218636522379845477525991419842455231304697870846344808007402982786779007693978008591643424220018477463050859602799224949200090041026877932035345087101216098339022526372168916401025893636528156378692327072727872801631041174021390051839';

  var KEY_ELEMENT_1_LARGE_VALUE_STR =
      '8008650650117182055231947796743510595354735701773678127718063162818861428894482760351040442014062206238677757186380515026469824595870224783896120627455841205517047520359887236518609328142965980653129280864098892996273964740090483597375107437139254412471355470548193136206727124381287524666926794272344682267309880188101372142121235759763117992716726731820970068223350454790098901032607754554402031099573358614926437378953856998041036773132081351057601842441454945388068039731483320814325657715553744820319265534754357498649904400594493834709761403358903505501640705996603619499187129497007221246019054462834616450362';
  var KEY_ELEMENT_2_LARGE_VALUE_STR =
      '11633656285653208443919333339470607846301429932894308783457570029107286147936455427826899303856584843544699293627942560893304455970515082299498680026759665698080990494885992738624255552297867735585728427220214607212173788862760053487198296321130486607904876481443447070538549541504734530736112052175501932133880661864111558469545768002897528612084034669926036569715266022136440126595140089621370735885927146016552642348282420224106653601826235849885956583427798077317966671703306684815012981357245529568157986211578924579799810305712551845266909314082456551659795959581952216679219662533353585637849875985836675331118';
  var KEY_EXPONENT_1_LARGE_VALUE_STR =
      '2310427452287266604038074581830299484831755686607108413899820257798165596547227594518989253178264380941033095268581246346863002280809127490132335557543545847332023813499454886051935043363376517658050024793517465349562106228739828060169613880190064891580719513382177220752409776280150551117185655930834371803832590064152233876708731218969195345490200219499344738664373593917843352485459832349992908261365788172930979823955846952949156058247067965643074532679047274706566088888021355575983368719069681293419346162608049979977471053590745499346591475991673763640645139343249862033377744682868210594034732021779460567412';
  var KEY_EXPONENT_2_LARGE_VALUE_STR =
      '7533608948657130117198364792462971241599837777629317286887202126604432685322243335715285630236984093165224740099563982076007648728594011814311120331313200895892632245003875959905589367483155794698953057915454892457379023058909518440394905695255170407948701433659375513788930419544151891518689331636455316691827048845766063478445484089208223819510250476623436782404385896353292653750744111841510466992101967325081746747885221256692886521245630131046183525331085171958919416648097910028887247120874015121272894655357197879292253781160838729753609784455892951965051119314203469298666741439497193677501351952497150273389';

  var mathService_ = mathematical.newService();
  var elGamalService = elGamal.newService();

  var mathRandomGenerator_ = mathService_.newRandomGenerator();

  var p = new BigInteger(P_STR);
  var q = new BigInteger(Q_STR);
  var g = new BigInteger(G_STR);
  var group_ = mathService_.newZpSubgroup(p, q, g);

  var publicKeyElements_ = [];
  publicKeyElements_[0] = mathService_.newZpGroupElement(
      p, q, new BigInteger(KEY_ELEMENT_1_VALUE_STR));
  publicKeyElements_[1] = mathService_.newZpGroupElement(
      p, q, new BigInteger(KEY_ELEMENT_2_VALUE_STR));
  var publicKey_ = elGamalService.newPublicKey(group_, publicKeyElements_);

  var privateKeyExponents_ = [];
  privateKeyExponents_[0] =
      mathService_.newExponent(q, new BigInteger(KEY_EXPONENT_1_VALUE_STR));
  privateKeyExponents_[1] =
      mathService_.newExponent(q, new BigInteger(KEY_EXPONENT_2_VALUE_STR));
  var privateKey_ = elGamalService.newPrivateKey(group_, privateKeyExponents_);

  var keyPair_ = elGamalService.newKeyPair(publicKey_, privateKey_);

  var elements_ = generateGroupElements(group_, NUM_GROUP_ELEMENTS);
  var elementValueStrings_ = getGroupElementValueStrings(elements_);

  p = new BigInteger(P_LARGE_STR);
  q = new BigInteger(Q_LARGE_STR);
  g = new BigInteger(G_STR);
  var largeGroup = mathService_.newZpSubgroup(p, q, g);

  var publicKeyElements = [];
  publicKeyElements[0] = mathService_.newZpGroupElement(
      p, q, new BigInteger(KEY_ELEMENT_1_LARGE_VALUE_STR));
  publicKeyElements[1] = mathService_.newZpGroupElement(
      p, q, new BigInteger(KEY_ELEMENT_2_LARGE_VALUE_STR));
  var largePublicKey_ =
      elGamalService.newPublicKey(largeGroup, publicKeyElements);

  var privateKeyExponents = [];
  privateKeyExponents[0] = mathService_.newExponent(
      q, new BigInteger(KEY_EXPONENT_1_LARGE_VALUE_STR));
  privateKeyExponents[1] = mathService_.newExponent(
      q, new BigInteger(KEY_EXPONENT_2_LARGE_VALUE_STR));
  var largePrivateKey_ =
      elGamalService.newPrivateKey(largeGroup, privateKeyExponents);

  var elementsFromLargeGroup_ =
      generateGroupElements(largeGroup, NUM_GROUP_ELEMENTS);
  var exponentsFromLargeGroup_ =
      generateRandomExponents(largeGroup, NUM_GROUP_ELEMENTS);

  p = new BigInteger(P_LARGE_STR);
  q = new BigInteger(Q_LARGE_STR).subtract(BigInteger.ONE);
  g = new BigInteger(G_STR);
  var elementsFromLargeNonQrGroup_ = generateGroupElements(
      mathService_.newZpSubgroup(p, q, g), NUM_GROUP_ELEMENTS);

  p = new BigInteger(P_STR);
  q = new BigInteger(Q_STR);
  var identityElements_ = [];
  for (var i = 0; i < NUM_GROUP_ELEMENTS; i++) {
    identityElements_[i] = mathService_.newZpGroupElement(p, q, BigInteger.ONE);
  }

  var singleElements_ =
      generateGroupElements(largeGroup, NUM_SINGLE_GROUP_ELEMENTS);

  this.getGroup = function() {
    return group_;
  };

  this.getPublicKey = function() {
    return publicKey_;
  };

  this.getPrivateKey = function() {
    return privateKey_;
  };

  this.getKeyPair = function() {
    return keyPair_;
  };

  this.getPublicKeyElements = function() {
    return publicKeyElements_;
  };

  this.getPrivateKeyExponents = function() {
    return privateKeyExponents_;
  };

  this.getZpGroupElements = function() {
    return elements_;
  };

  this.getZpGroupElementValueStrings = function() {
    return elementValueStrings_;
  };

  this.getLargePublicKey = function() {
    return largePublicKey_;
  };

  this.getLargePrivateKey = function() {
    return largePrivateKey_;
  };

  this.getElementsFromLargeZpSubgroup = function() {
    return elementsFromLargeGroup_;
  };

  this.getExponentsFromLargeZpSubgroup = function() {
    return exponentsFromLargeGroup_;
  };

  this.getElementsFromLargeNonQrZpSubgroup = function() {
    return elementsFromLargeNonQrGroup_;
  };

  this.getIdentityElements = function() {
    return identityElements_;
  };

  this.getSingleElements = function() {
    return singleElements_;
  };

  this.getShortExponentBitLength = function() {
    return SHORT_EXPONENT_BIT_LENGTH;
  };

  function generateRandomExponents(group, numExponents) {
    var exponents = [];
    for (var i = 0; i < numExponents; i++) {
      exponents.push(mathRandomGenerator_.nextExponent(group));
    }

    return exponents;
  }

  function generateGroupElements(group, numElements) {
    var elements = [];
    for (var i = 0; i < numElements; i++) {
      elements.push(mathRandomGenerator_.nextZpGroupElement(group));
    }

    return elements;
  }

  function getGroupElementValueStrings(elements) {
    var elementValueStrings = [];
    for (var i = 0; i < elements.length; i++) {
      elementValueStrings.push(elements[i].value.toString());
    }

    return elementValueStrings;
  }
}
