# Quick Guide to the Module *cryptolib-interoperation*

## Description
The purpose of the module *cryptolib-interoperation* is to test the interoperation between cryptographic applications implemented in *cryptoLib* and those same applications implemented in libraries external to *cryptoLib*, the latter possibly being implemented in languages different from Java. Presently, this module tests the cryptographic interoperation between *cryptoLib* and *cryptoLib-js*, the latter library being implemented in JavaScript.

## Nashorn JavaScript Engine
The interoperation tests make use of the Nashorn JavaScript engine by Oracle, which was released with Java 8. This engine allows the tests implemented in JavaScript to be invoked directly from the tests implemented in Java so that no external framework (e.g. *Node.js*) or file IO is needed for the tests implemented in JavaScript.

## Dependencies
The interoperation tests implemented in Java access their dependencies on *cryptoLib* via the Maven POM file of the module *cryptolib-interoperation*. The tests implemented in JavaScript rely on a copy of *cryptoLib-js* found in the file
*main/resources/js/lib/cryptolib-min.js*. Therefore, this file must be updated everytime that a new release of *cryptoLib-js* is made and *cryptoLib* is required to be compatible with these changes. The folder 
*main/resources/js/lib* also contains a copy of the third party cryptographic library *forge*, which is needed by *cryptoLib-js*. Therefore, this library must be updated everytime the version of it used by *cryptoLib-js* changes, which occurs quite seldom.

## Cryptographic Policy
All interoperation tests make use of the common default cryptographic policy used by the *cryptoLib* and *cryptoLib-js* libraries. The tests implemented in Java access this policy via a Maven dependency to the module *cryptolib-default-policies* of *cryptoLib*. The tests implemented in JavaScript access the default policy from the file *main/resources/js/config/cryptolib.default.policy.js*. In order for the interoperation tests to work correctly, this file must always define the same policy as that maintained in the module *cryptolib-default-policies*. Note that the ability to configure these policy files can also be useful to quickly test the interoperation when a new and more secure cryptographic property must be adopted by both libraries.

## Data Generation
The input data needed for each cryptographic application being tested is generated in the constructor of the associated test implemented in Java. When this input data is needed by an associated test implemented in JavaScript, it is serialized into a format (e.g. JSON or Base64 encoded) that is suitable for use with *cryptoLib-js*. Likewise, the data returned by an associated test implemented in JavaScript is deserialized back into a format that is suitable for use with *cryptoLib*. Note that no file IO is needed to exchange data between the tests implemented in Java and JavaScript.

## Test Location
The interoperation test classes implemented in Java are found in the *main* folder of the module *cryptolib-interoperation*. These classes are implemented using the command design pattern so that they may be added to a command runner, in the order in which they are to be run. The command runner is setup and called by the the *main* program, which itself is implemented in the class *JavaJsInteroperationTester*. The interoperation tests implemented in JavaScript are found in the folder *main/resources/js/test*. This folder also contains JavaScript files to initialize the tests and define any additional utilities that they need. As mentioned above, these tests are invoked by the tests implemented in Java, via the Nashorn JavaScript engine.

## Test Description
For each cryptographic application being tested (e.g. asymmetric cipher), the Java-JavaScript interoperation test process consists of the following 5 steps:

    1 - Use cryptoLib to run a given cryptographic operation (e.g. asymmetric encryption) of the given application and generate a result.
    
    2 - Use cryptolib-js to run the reverse cryptographic operation of step 1 (e.g. asymmetric decryption), taking the result generated in step 1 as input. Throw an exception if the result obtained by this step is not what was expected (e.g. retrieval of the data that was asymmetrically encrypted in step 1).
    
    3 - Use cryptolib-js to run the same cryptographic operation as in step 1 and and generate a result.
    
    4 - Use cryptolib to run the reverse cryptographic operation of step 4, taking the result generated in step 4 as input. Throw an exception if the result obtained by this step is not what was expected.
    
    5 - If no exception was thrown in step 5, print a message stating that the Java-JavaScript interoperation test for this cryptographic application was successful. 
    
For example, a successful run of the interoperation tests for the asymmetric cipher application would print the following output to the screen:

    INFO - [JAVA JS INTEROP] Asymmetrically encrypting data, using Java...
    INFO - [JAVA JS INTEROP] Asymmetrically decrypting data, using JavaScript...
    INFO - [JAVA JS INTEROP] Asymmetrically encrypting data, using JavaScript...
    INFO - [JAVA JS INTEROP] Asymmetrically decrypting data, using Java...
    INFO - [JAVA JS INTEROP] INTEROPERATION TEST FOR ASYMMETRIC CIPHER WAS SUCCESSFUL.
    
Note that for some cryptographic applications, the reverse operation is not yet implemented in JavaScript, in which case only the first half of the interoperation test cycle described above would be executed and printed to screen.

## How to Run Tests
To build the module and run the tests, change to the directory *cryptolib-interoperation* and type the following:

    mvn clean install -P cryptolib-interoperation-tests
    
Note: This command can also be run from the top level directory of *cryptoLib*, in which case all other modules of the library will also be built and have their unit tests run.
