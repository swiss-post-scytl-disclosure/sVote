/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
var digitalEnvelopeTest = {

  /**
   * Opens a digital envelope that was generated using Java and checks if the
   * original data could be retrieved.
   *
   * @param {String}
   *            digitalEnvelopeJson the digital envelope, in JSON format.
   * @param {String[]}
   *            privateKeyPems the array of PEM formatted private keys, each
   *            of which can be used to retrieve the data from the digital
   *            envelope.
   * @param {String}
   *            dataB64 the data that was stored in the digital envelope, in
   *            Base64 encoded format.
   * @throws {Error}
   *             if the digital envelope opening operation fails or the
   *             original data could not be retrieved.
   */
  open: function(digitalEnvelopeJson, privateKeyPems, dataB64) {

    'use strict';

    privateKeyPems = Java.from(privateKeyPems);

    var errorData = {};
    errorData['Digital envelope (JSON format) [Java]'] = digitalEnvelopeJson;
    errorData
        ['Private key(s) used to retrieve data from digital envelope (PEM format) [Java]'] =
            privateKeyPems;
    errorData['Data stored in digital envelope (Base64 encoded) [Java]'] =
        dataB64;

    try {
      cryptolib('digitalenvelope', 'digitalenvelope.service', function(box) {
        var digitalEnvelope =
            box.digitalenvelope.deserialize(digitalEnvelopeJson);

        for (var i = 0; i < privateKeyPems.length; i++) {
          var privateKeyPem = privateKeyPems[i];

          var dataB64Found = box.digitalenvelope.service.openDigitalEnvelope(
              digitalEnvelope, privateKeyPem);

          if (dataB64Found !== dataB64) {
            errorData
                ['Faulty Data retrieved from digital envelope (Base64 encoded) [JS]'] =
                    dataB64Found;
            errorData
                ['Private key used to retrieve faulty data from digital envelope (PEM format) [Java]'] =
                    privateKeyPem;

            throw 'Expected data retrieved from digital envelope (Base64 encoded): ' +
                dataB64 + ' ; Found: ' + dataB64Found;
          }
        }
      });
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  },

  /**
   * Generates a digital envelope that will be opened using Java.
   *
   * @param {String}
   *            dataB64 the data to store in the digital envelope, in Base64
   *            encoded format.
   * @param {String[]}
   *            publicKeyPems the array of PEM formatted public keys used to
   *            generate the digital envelope.
   * @returns {String} the generated digital envelope, in JSON format.
   * @throws {Error}
   *             if the digital envelope generation operation fails.
   */
  generate: function(dataB64, publicKeyPems) {

    'use strict';

    publicKeyPems = Java.from(publicKeyPems);

    var errorData = {};
    errorData['Data to store in digital envelope (Base64 encoded) [Java]'] =
        dataB64;
    errorData
        ['Public key(s) used to store data in digital envelope (PEM format) [Java]'] =
            publicKeyPems;

    try {
      var digitalEnvelopeJson;
      cryptolib('digitalenvelope.service', function(box) {
        var digitalEnvelope = box.digitalenvelope.service.createDigitalEnvelope(
            dataB64, publicKeyPems);

        digitalEnvelopeJson = digitalEnvelope.stringify();
      });

      return digitalEnvelopeJson;
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  }
};