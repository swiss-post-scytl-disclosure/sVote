/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Generation of entropy required by cryptoLib-js.
 */
cryptoPRNG.startEntropyCollection();
cryptoPRNG.stopEntropyCollectionAndCreatePRNG();

/**
 * Placeholder definition of function setInterval to avoid errors due to
 * 'window' being undefined for interoperation tests.
 */
function setInterval() {}

String.prototype.escapeNewlineChars = function() {
  'use strict';

  return this.replace(/\\n/g, '\n').replace(/\\r/g, '\r');
};

String.prototype.escapeDoubleQuoteChars = function() {
  'use strict';

  return this.replace(/\\"/g, '"');
};

/**
 * Definition of global variables for interoperation tests.
 */
var _converters;
var _parsers;
cryptolib('commons.utils', function(box) {
  'use strict';

  _converters = new box.commons.utils.Converters();
  _parsers = new box.commons.utils.Parsers();
});