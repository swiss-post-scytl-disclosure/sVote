/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
var plaintextProofTest = {

  /**
   * Verifies a plaintext zero knowledge proof of knowledge that was generated
   * using Java and checks if the verification was true.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            publicKeyJson the public key, in JSON format.
   * @param {String}
   *            plaintextJson the plaintext, in JSON format.
   * @param {String}
   *            ciphertextJson the ciphertext, in JSON format.
   * @param {String}
   *            proofJson the proof to verify, in JSON format.
   * @throws {Error}
   *             if the proof verification operation fails or the verification
   *             is false.
   */
  verify: function(
      zpSubgroupJson, publicKeyJson, plaintextJson, ciphertextJson, proofJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['ElGamal public key (JSON format) [Java]'] = publicKeyJson;
    errorData['Plaintext (JSON format) [Java]'] = plaintextJson;
    errorData['Ciphertext (JSON format) [Java]'] = ciphertextJson;
    errorData['Plaintext zero knowledge proof (JSON format) [Java]'] =
        proofJson;

    try {
      cryptolib(
          'commons.mathematical', 'homomorphic.cipher', 'homomorphic.service',
          'proofs', 'proofs.service', function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var publicKeyJsonB64 = _converters.base64Encode(publicKeyJson);
            var publicKey = box.homomorphic.service.createElGamalPublicKey(
                publicKeyJsonB64);

            var plaintext = testUtils.deserializeZpGroupElements(plaintextJson);

            var computationValues =
                box.homomorphic.cipher.deserializeElGamalComputationValues(
                    ciphertextJson);
            var ciphertext = new box.homomorphic.cipher.ElGamalEncrypterValues(
                null, computationValues.getGamma(),
                computationValues.getPhis());

            var proof = box.proofs.utils.deserializeProof(proofJson);

            var verified = box.proofs.service.verifyPlaintextProof(
                publicKey, ciphertext, plaintext, proof, zpSubgroup);

            if (!verified) {
              throw 'Verification of plaintext zero knowledge proof was false.';
            }
          });
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  },

  /**
   * Generates a plaintext zero knowledge proof of knowledge that will be
   * verified using Java.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            publicKeyJson the public key, in JSON format.
   * @param {String}
   *            plaintextJson the plaintext, in JSON format.
   * @param {String}
   *            witnessJson the witness, in JSON format.
   * @param {String}
   *            ciphertextJson the ciphertext, in JSON format.
   * @param {String}
   *            preComputedValuesJson the pre-computed plaintext proof values,
   *            in JSON format (if the pre-computation step was performed).
   * @returns {String} the generated plaintext zero knowledge proof, in JSON
   *          format.
   * @throws {Error}
   *             if the proof generation operation fails.
   */
  generate: function(
      zpSubgroupJson, publicKeyJson, plaintextJson, witnessJson, ciphertextJson,
      preComputedValuesJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['ElGamal public key (JSON format) [Java]'] = publicKeyJson;
    errorData['Plaintext (JSON format) [Java]'] = plaintextJson;
    errorData['Witness (JSON format) [Java]'] = witnessJson;
    errorData['Ciphertext (JSON format) [Java]'] = ciphertextJson;
    if (typeof preComputedValuesJson !== 'undefined') {
      errorData['Pre-computed plaintext proof values (JSON format) [JS]'] =
          preComputedValuesJson;
    }

    try {
      var proofJson;
      cryptolib(
          'commons.mathematical', 'homomorphic.cipher', 'homomorphic.service',
          'proofs', 'proofs.service', function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var publicKeyJsonB64 = _converters.base64Encode(publicKeyJson);
            var publicKey = box.homomorphic.service.createElGamalPublicKey(
                publicKeyJsonB64);

            var plaintext = testUtils.deserializeZpGroupElements(plaintextJson);

            var witness =
                box.commons.mathematical.groupUtils.deserializeExponent(
                    witnessJson);

            var computationValues =
                box.homomorphic.cipher.deserializeElGamalComputationValues(
                    ciphertextJson);
            var ciphertext = new box.homomorphic.cipher.ElGamalEncrypterValues(
                null, computationValues.getGamma(),
                computationValues.getPhis());

            var proof;
            if (typeof preComputedValuesJson !== 'undefined') {
              var preComputedValues =
                  box.proofs.utils.deserializeProofPreComputedValues(
                      preComputedValuesJson);
              proof = box.proofs.service.createPlaintextProof(
                  publicKey, ciphertext, plaintext, witness, zpSubgroup,
                  preComputedValues);
            } else {
              proof = box.proofs.service.createPlaintextProof(
                  publicKey, ciphertext, plaintext, witness, zpSubgroup);
            }

            proofJson = proof.stringify();
          });

      return proofJson;
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  },

  /**
   * Generates pre-computed values that can be used to generate a plaintext
   * zero knowledge proof of knowledge, using Java or JavaScript.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            publicKeyJson the public key, in JSON format.
   * @returns {String} the pre-computed plaintext proof values, in JSON
   *          format.
   * @throws {Error}
   *             if the pre-computed values generation operation fails.
   */
  generatePreComputedValues: function(zpSubgroupJson, publicKeyJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['ElGamal public key (JSON format) [Java]'] = publicKeyJson;

    try {
      var preComputedValuesJson;
      cryptolib(
          'commons.mathematical', 'homomorphic.service', 'proofs.service',
          function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var publicKeyJsonB64 = _converters.base64Encode(publicKeyJson);
            var publicKey = box.homomorphic.service.createElGamalPublicKey(
                publicKeyJsonB64);

            var preComputedValues = box.proofs.service.preComputePlaintextProof(
                publicKey, zpSubgroup);

            preComputedValuesJson = preComputedValues.stringify();
          });

      return preComputedValuesJson;
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  }
};