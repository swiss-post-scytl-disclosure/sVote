/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
var plaintextExponentEqualityProofTest = {

  /**
   * Verifies a plaintext exponent equality zero knowledge proof of knowledge
   * that was generated using Java and checks if the verification was true.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            baseElementsJson the base elements, in JSON format.
   * @param {String}
   *            ciphertextJson the ciphertext, in JSON format.
   * @param {String}
   *            proofJson the plaintext exponent equality zero knowledge
   *            proof, in JSON format.
   * @throws {Error}
   *             if the proof verification operation fails or the verification
   *             is false.
   */
  verify: function(
      zpSubgroupJson, baseElementsJson, ciphertextJson, proofJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['Base elements (JSON format) [Java]'] = baseElementsJson;
    errorData['Ciphertext (JSON format) [Java]'] = ciphertextJson;
    errorData
        ['Plaintext exponent equality zero knowledge proof (JSON format) [Java]'] =
            proofJson;

    try {
      cryptolib(
          'commons.mathematical', 'homomorphic.cipher', 'proofs',
          'proofs.service', function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var baseElements =
                testUtils.deserializeZpGroupElements(baseElementsJson);

            var computationValues =
                box.homomorphic.cipher.deserializeElGamalComputationValues(
                    ciphertextJson);
            var ciphertext = new box.homomorphic.cipher.ElGamalEncrypterValues(
                null, computationValues.getGamma(),
                computationValues.getPhis());

            var proof = box.proofs.utils.deserializeProof(proofJson);

            var verified =
                box.proofs.service.verifyPlaintextExponentEqualityProof(
                    ciphertext, baseElements, proof, zpSubgroup);

            if (!verified) {
              throw 'Verification of plaintext exponent equality zero knowledge proof was false.';
            }
          });
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  },

  /**
   * Generates a plaintext exponent equality zero knowledge proof of knowledge
   * that will be verified using Java.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            baseElementsJson the base elements, in JSON format.
   * @param {String}
   *            ciphertextJson the ciphertext, in JSON format.
   * @param {String}
   *            witness1Json the witness that wraps the ephemeral key, in JSON
   *            format.
   * @param {String}
   *            witness2Json the witness that wraps the message key, in JSON
   *            format.
   * @param {String}
   *            preComputedValuesJson the pre-computed plaintext exponent
   *            equality proof values, in JSON format (if the pre-computation
   *            step was performed).
   * @returns {String} the generated plaintext exponent equality zero
   *          knowledge proof, in JSON format.
   * @throws {Error}
   *             if the proof generation operation fails.
   */
  generate: function(
      zpSubgroupJson, baseElementsJson, ciphertextJson, witness1Json,
      witness2Json, preComputedValuesJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['Base elements (JSON format) [Java]'] = baseElementsJson;
    errorData['Ciphertext (JSON format) [Java]'] = ciphertextJson;
    errorData['Witness 1 (JSON format) [Java]'] = witness1Json;
    errorData['Witness 2 (JSON format) [Java]'] = witness2Json;
    if (typeof preComputedValuesJson !== 'undefined') {
      errorData
          ['Pre-computed plaintext exponent equality proof values (JSON format) [JS]'] =
              preComputedValuesJson;
    }

    try {
      var proofJson;
      cryptolib(
          'commons.mathematical', 'homomorphic.cipher', 'proofs',
          'proofs.service', function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var baseElements =
                testUtils.deserializeZpGroupElements(baseElementsJson);

            var computationValues =
                box.homomorphic.cipher.deserializeElGamalComputationValues(
                    ciphertextJson);
            var ciphertext = new box.homomorphic.cipher.ElGamalEncrypterValues(
                null, computationValues.getGamma(),
                computationValues.getPhis());

            var witness1 =
                box.commons.mathematical.groupUtils.deserializeExponent(
                    witness1Json);

            var witness2 =
                box.commons.mathematical.groupUtils.deserializeExponent(
                    witness2Json);

            var proof;
            if (typeof preComputedValuesJson !== 'undefined') {
              var preComputedValues =
                  box.proofs.utils.deserializeProofPreComputedValues(
                      preComputedValuesJson);
              proof = box.proofs.service.createPlaintextExponentEqualityProof(
                  ciphertext, baseElements, witness1, witness2, zpSubgroup,
                  preComputedValues);
            } else {
              proof = box.proofs.service.createPlaintextExponentEqualityProof(
                  ciphertext, baseElements, witness1, witness2, zpSubgroup);
            }

            proofJson = proof.stringify();
          });

      return proofJson;
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  },

  /**
   * Generates pre-computed values that can be used to generate a plaintext
   * exponent equality zero knowledge proof of knowledge, using Java or
   * JavaScript.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            baseElementsJson the base elements, in JSON format.
   * @returns {String} the pre-computed plaintext exponent equality proof
   *          values, in JSON format.
   * @throws {Error}
   *             if the pre-computed values generation operation fails.
   */
  generatePreComputedValues: function(zpSubgroupJson, baseElementsJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['Base elements (JSON format) [Java]'] = baseElementsJson;

    try {
      var preComputedValuesJson;
      cryptolib(
          'commons.mathematical', 'homomorphic.service', 'proofs.service',
          function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var baseElements =
                testUtils.deserializeZpGroupElements(baseElementsJson);

            var preComputedValues =
                box.proofs.service.preComputePlaintextExponentEqualityProof(
                    baseElements, zpSubgroup);

            preComputedValuesJson = preComputedValues.stringify();
          });

      return preComputedValuesJson;
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  }
};