/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
var schnorrProofTest = {

  /**
   * Verifies a Schnorr zero knowledge proof of knowledge that was generated
   * using Java and checks if the verification was true.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            voterId the voter identifier.
   * @param {String}
   *            electionEventId the election event identifier.
   * @param {String}
   *            exponentiatedGeneratorJson the exponentiated generator, in
   *            JSON format.
   * @param {String}
   *            proofJson, the Schnorr proof, in JSON format.
   * @throws {Error}
   *             if the proof verification operation fails or the verification
   *             is false.
   */
  verify: function(
      zpSubgroupJson, voterId, electionEventId, exponentiatedGeneratorJson,
      proofJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['Voter ID [Java]'] = voterId;
    errorData['Election event ID [Java]'] = electionEventId;
    errorData['Exponentiated generator (JSON format) [Java]'] =
        exponentiatedGeneratorJson;
    errorData['Schnorr zero knowledge proof (JSON format) [Java]'] = proofJson;

    try {
      cryptolib(
          'commons.mathematical', 'proofs', 'proofs.service', function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var exponentiatedGenerator =
                box.commons.mathematical.groupUtils.deserializeGroupElement(
                    exponentiatedGeneratorJson);

            var proof = box.proofs.utils.deserializeProof(proofJson);

            var verified = box.proofs.service.verifySchnorrProof(
                exponentiatedGenerator, voterId, electionEventId, proof,
                zpSubgroup);

            if (!verified) {
              throw 'Verification of Schnorr zero knowledge proof was false.';
            }
          });
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  },

  /**
   * Generates a Schnorr zero knowledge proof of knowledge that will be
   * verified using Java.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            voterId the voter identifier.
   * @param {String}
   *            electionEventId the election event identifier.
   * @param {String}
   *            witnessJson the witness, in JSON format.
   * @param {String}
   *            exponentiatedGeneratorJson the exponentiated generator, in
   *            JSON format.
   * @param {String}
   *            preComputedValuesJson the pre-computed Schnorr proof values,
   *            in JSON format (if the pre-computation step was performed).
   * @returns {String} the generated Schnorr zero knowledge proof, in JSON
   *          format.
   * @throws {Error}
   *             if the proof generation operation fails.
   */
  generate: function(
      zpSubgroupJson, voterId, electionEventId, witnessJson,
      exponentiatedGeneratorJson, preComputedValuesJson) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['Voter ID [Java]'] = voterId;
    errorData['Election event ID [Java]'] = electionEventId;
    errorData['Witness (JSON format) [Java]'] = witnessJson;
    errorData['Exponentiated generator (JSON format) [Java]'] =
        exponentiatedGeneratorJson;
    if (typeof preComputedValuesJson !== 'undefined') {
      errorData['Pre-computed Schnorr proof values (JSON format) [JS]'] =
          preComputedValuesJson;
    }

    try {
      var proofJson;
      cryptolib(
          'commons.mathematical', 'proofs', 'proofs.service', function(box) {
            var zpSubgroup =
                box.commons.mathematical.groupUtils.deserializeGroup(
                    zpSubgroupJson);

            var witness =
                box.commons.mathematical.groupUtils.deserializeExponent(
                    witnessJson);

            var exponentiatedGenerator =
                box.commons.mathematical.groupUtils.deserializeGroupElement(
                    exponentiatedGeneratorJson);

            var proof;
            if (typeof preComputedValuesJson !== 'undefined') {
              var preComputedValues =
                  box.proofs.utils.deserializeProofPreComputedValues(
                      preComputedValuesJson);
              proof = box.proofs.service.createSchnorrProof(
                  voterId, electionEventId, exponentiatedGenerator, witness,
                  zpSubgroup, preComputedValues);
            } else {
              proof = box.proofs.service.createSchnorrProof(
                  voterId, electionEventId, exponentiatedGenerator, witness,
                  zpSubgroup);
            }

            proofJson = proof.stringify();
          });

      return proofJson;
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  },

  /**
   * Generates pre-computed values that can be used to generate a Schnorr zero
   * knowledge proof of knowledge, using Java or JavaScript.
   *
   * @param {String}
   *            zpSubgroupJson the Zp subgroup, in JSON format.
   * @param {String}
   *            voterId the voter identifier.
   * @param {String}
   *            electionEventId the election event identifier.
   * @returns {String} the pre-computed Schnorr proof values, in JSON format.
   * @throws {Error}
   *             if the pre-computed values generation operation fails.
   */
  generatePreComputedValues: function(
      zpSubgroupJson, voterId, electionEventId) {

    'use strict';

    var errorData = {};
    errorData['Zp Subgroup (JSON format) [Java]'] = zpSubgroupJson;
    errorData['Voter ID [Java]'] = voterId;
    errorData['Election event ID [Java]'] = electionEventId;

    try {
      var preComputedValuesJson;
      cryptolib('commons.mathematical', 'proofs.service', function(box) {
        var zpSubgroup = box.commons.mathematical.groupUtils.deserializeGroup(
            zpSubgroupJson);

        var preComputedValues = box.proofs.service.preComputeSchnorrProof(
            voterId, electionEventId, zpSubgroup);

        preComputedValuesJson = preComputedValues.stringify();
      });

      return preComputedValuesJson;
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  }
};