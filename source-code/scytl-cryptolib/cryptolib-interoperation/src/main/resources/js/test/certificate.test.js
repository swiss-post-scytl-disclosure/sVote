/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
var certificateTest = {

  /**
   * Opens a certificate that was generated using Java and checks that its
   * signature can be verified and that its content can be retrieved.
   *
   * @param {String}
   *            certificatePem the certificate to open, in PEM format.
   * @param {String}
   *            rootCertificatePem the root certificate, in PEM format.
   * @param {String}
   *            publicKeyPem the expected public key of the certificate to
   *            open, in PEM format.
   * @param {Array}
   *            issuerDnFields the expected issuer DN fields of the
   *            certificate to open, as an array of Strings.
   * @param {Array}
   *            subjectDnFields the expected subject DN fields of the
   *            certificate to open, as an array of Strings.
   * @param {String}
   *            notBefore the expected starting date of validity of the
   *            certificate to open, , in Epoch time format.
   * @param {String}
   *            notAfter the expected ending date of validity of the
   *            certificate to open, in Epoch time format.
   * @throws {Error}
   *             if the certificate opening operation fails, the certificate's
   *             signature cannot be verified or its content cannot be
   *             retrieved.
   */
  open: function(
      certificatePem, rootCertificatePem, publicKeyPem, issuerDnFields,
      subjectDnFields, notBefore, notAfter) {

    'use strict';

    issuerDnFields = Java.from(issuerDnFields);
    subjectDnFields = Java.from(subjectDnFields);

    var errorData = {};
    errorData['Certificate (PEM format)  [Java]'] = certificatePem;
    errorData['Root certificate (PEM format)  [Java]'] = rootCertificatePem;
    errorData['Public key (PEM format)  [Java]'] = publicKeyPem;
    errorData['Issuer distinguished name fields  [Java]'] = issuerDnFields;
    errorData['Subject distinguished name fields  [Java]'] = subjectDnFields;
    errorData['Starting date of validity (Epoch time format)  [Java]'] =
        notBefore;
    errorData['Ending date of validity (Epoch time format) [Java]'] = notAfter;

    try {
      cryptolib('certificates.service', function(box) {

        var rootCertificate =
            new box.certificates.service.load(rootCertificatePem);
        if (!rootCertificate.verify(certificatePem)) {
          throw 'Verification of certificate by root certificate was false.';
        }

        var certificate = new box.certificates.service.load(certificatePem);
        testUtils.checkPublicKey(certificate.getPublicKey(), publicKeyPem);
        testUtils.checkCertificateIssuerDn(certificate, issuerDnFields);
        testUtils.checkCertificateSubjectDn(certificate, subjectDnFields);
        testUtils.checkCertificateValidityDates(
            certificate, notBefore, notAfter);
      });
    } catch (error) {
      throw testUtils.serializeError(error, errorData);
    }
  }
};