/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the digital envelope tests.
 */
public final class DigitalEnvelopeConstants {

    private DigitalEnvelopeConstants() {
    }

    public static final int NUM_ASYMMETRIC_KEY_PAIRS = 3;

    public static final String JS_DIGITAL_ENVELOPE_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/digital.envelope.test.js";

    public static final String JS_DIGITAL_ENVELOPE_TEST_NAMESPACE =
        "digitalEnvelopeTest";

    public static final String JS_DIGITAL_ENVELOPE_GENERATION_FUNCTION =
        "generate";

    public static final String JS_DIGITAL_ENVELOPE_OPENING_FUNCTION =
        "open";
}
