/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the ElGamal tests.
 */
public final class ElGamalConstants {

    private ElGamalConstants() {
    }

    public static final int NUM_ZP_GROUP_ELEMENTS = 6;

    public static final String JS_ELGAMAL_CIPHER_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/elgamal.cipher.test.js";

    public static final String JS_ELGAMAL_CIPHER_TEST_NAMESPACE =
        "elGamalCipherTest";

    public static final String JS_ELGAMAL_CIPHER_SHORT_EXPONENT_TEST_PATH =
        CommonConstants.JS_TEST_PATH
            + "/elgamal.cipher.short.exponent.test.js";

    public static final String JS_ELGAMAL_CIPHER_SHORT_EXPONENT_TEST_NAMESPACE =
        "elGamalCipherShortExponentTest";

    public static final String JS_ELGAMAL_ENCRYPTION_FUNCTION = "encrypt";

    public static final String JS_ELGAMAL_DECRYPTION_FUNCTION = "decrypt";

    public static final String JS_ELGAMAL_PRE_COMPUTED_VALUES_GENERATION_FUNCTION =
        "generatePreComputedValues";

    public static final String JS_ELGAMAL_JAVA_SHORT_EXPONENT_ENCRYPTION_FUNCTION =
        "encryptWithJavaShortExponent";
}
