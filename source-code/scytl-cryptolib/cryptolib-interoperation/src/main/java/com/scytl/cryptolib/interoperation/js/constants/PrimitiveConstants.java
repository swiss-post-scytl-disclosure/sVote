/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the primitive tests.
 */
public final class PrimitiveConstants {

    private PrimitiveConstants() {
    }

    public static final String JS_HASH_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/hash.test.js";

    public static final String JS_HASH_TEST_NAMESPACE = "hashTest";

    public static final String JS_HASH_GENERATION_FUNCTION = "generate";

    public static final String JS_HASH_VERIFICATION_FUNCTION = "verify";

    public static final String JS_PBKDF_KEY_DERIVER_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/pbkdf.key.deriver.test.js";

    public static final String JS_PBKDF_KEY_DERIVER_TEST_NAMESPACE =
        "pbkdfKeyDeriverTest";

    public static final String JS_PBKDF_KEY_DERIVER_DERIVATION_FUNCTION =
        "derive";

    public static final String JS_PBKDF_KEY_DERIVER_VERIFICATION_FUNCTION =
        "verify";

    public static final String JS_PBKDF_KEY_DERIVER_DERIVATION_WITH_FORGE_FUNCTION =
        "deriveWithForge";

    public static final String JS_PBKDF_KEY_DERIVER_VERIFICATION_WITH_FORGE_FUNCTION =
        "verifyWithForge";
}
