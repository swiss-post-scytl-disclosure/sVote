/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the asymmetric tests.
 */
public final class AsymmetricConstants {

    private AsymmetricConstants() {
    }

    public static final int MAXIMUM_DATA_ARRAY_LENGTH = 10;

    public static final String JS_ASYMMETRIC_CIPHER_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/asymmetric.cipher.test.js";

    public static final String JS_ASYMMETRIC_CIPHER_TEST_NAMESPACE =
        "asymmetricCipherTest";

    public static final String JS_ASYMMETRIC_ENCRYPTION_FUNCTION =
        "encrypt";

    public static final String JS_ASYMMETRIC_DECRYPTION_FUNCTION =
        "decrypt";

    public static final String JS_ASYMMETRIC_SIGNER_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/asymmetric.signer.test.js";

    public static final String JS_ASYMMETRIC_SIGNER_TEST_NAMESPACE =
        "asymmetricSignerTest";

    public static final String JS_ASYMMETRIC_SIGNING_FUNCTION = "sign";

    public static final String JS_ASYMMETRIC_SIGNATURE_VERIFICATION_FUNCTION =
        "verify";

    public static final String ASYMMETRIC_XML_SIGNER_TEST_FILE_TO_SIGN_PATH =
        CommonConstants.DATA_RESOURCES_PATH + "/xmlTestFileToSign.xml";

    public static final String ASYMMETRIC_XML_SIGNER_TEST_FILE_SIGNATURE_PARENT_NODE =
        "element1";

    public static final String JS_ASYMMETRIC_XML_SIGNER_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/asymmetric.xml.signer.test.js";

    public static final String JS_ASYMMETRIC_XML_SIGNER_TEST_NAMESPACE =
        "asymmetricXmlSignerTest";

    public static final String JS_ASYMMETRIC_XML_SIGNATURE_VERIFICATION_FUNCTION =
        "verify";
}
