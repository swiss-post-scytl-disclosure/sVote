/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the PKCS12 tests.
 */
public final class StoreConstants {

    private StoreConstants() {

    }

    public static final String SECRET_KEY_FOR_ENCRYPTION_ALIAS =
        "test_secret_key_for_encryption";

    public static final String SECRET_KEY_FOR_HMAC_ALIAS =
        "test_secret_key_for_hmac";

    public static final String ROOT_PRIVATE_KEY_ALIAS =
        "test_root_private_key";

    public static final String INTERMEDIATE_PRIVATE_KEY_ALIAS =
        "test_intermediate_private_key";

    public static final String LEAF_PRIVATE_KEY_ALIAS =
        "test_leaf_private_key";

    public static final String ELGAMAL_PRIVATE_KEY_ALIAS =
        "test_elgamal_private_key";

    public static final String JS_PKCS12_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/pkcs12.test.js";

    public static final String JS_PKCS12_TEST_NAMESPACE = "pkcs12Test";

    public static final String JS_PKCS12_OPENING_FUNCTION = "open";

    public static final String JS_SCYTL_KEY_STORE_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/scytl.keystore.test.js";

    public static final String JS_SCYTL_KEY_STORE_TEST_NAMESPACE =
        "scytlKeyStoreTest";

    public static final String JS_SCYTL_KEY_STORE_OPENING_FUNCTION =
        "open";
}
