/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the zero knowledge proof of knowledge tests.
 */
public final class ProofConstants {

    private ProofConstants() {
    }

    public static final String JS_SCHNORR_PROOF_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/schnorr.proof.test.js";

    public static final String JS_SCHNORR_PROOF_TEST_NAMESPACE =
        "schnorrProofTest";

    public static final int EXPONENTIATION_PROOF_NUM_BASE_ELEMENTS = 6;

    public static final String JS_EXPONENTIATION_PROOF_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/exponentiation.proof.test.js";

    public static final String JS_EXPONENTIATION_PROOF_TEST_NAMESPACE =
        "exponentiationProofTest";

    public static final int PLAINTEXT_PROOF_PLAINTEXT_LENGTH = 6;

    public static final String JS_PLAINTEXT_PROOF_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/plaintext.proof.test.js";

    public static final String JS_PLAINTEXT_PROOF_TEST_NAMESPACE =
        "plaintextProofTest";

    public static final int SIMPLE_PLAINTEXT_EQUALITY_PROOF_PLAINTEXT_LENGTH =
        6;

    public static final String JS_SIMPLE_PLAINTEXT_EQUALITY_PROOF_TEST_PATH =
        CommonConstants.JS_TEST_PATH
            + "/simple.plaintext.equality.proof.test.js";

    public static final String JS_SIMPLE_PLAINTEXT_EQUALITY_PROOF_TEST_NAMESPACE =
        "simplePlaintextEqualityProofTest";

    public static final int PLAINTEXT_EQUALITY_PROOF_PLAINTEXT_LENGTH = 6;

    public static final int OR_PROOF_NUMBER_OF_ELEMENTS = 6;

    public static final String JS_PLAINTEXT_EQUALITY_PROOF_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/plaintext.equality.proof.test.js";

    public static final String JS_PLAINTEXT_EQUALITY_PROOF_TEST_NAMESPACE =
        "plaintextEqualityProofTest";

    public static final int PLAINTEXT_EXPONENT_EQUALITY_PROOF_PLAINTEXT_LENGTH =
        5;

    public static final String JS_PLAINTEXT_EXPONENT_EQUALITY_PROOF_TEST_PATH =
        CommonConstants.JS_TEST_PATH
            + "/plaintext.exponent.equality.proof.test.js";

    public static final String JS_PLAINTEXT_EXPONENT_EQUALITY_PROOF_TEST_NAMESPACE =
        "plaintextExponentEqualityProofTest";

    public static final String JS_PROOF_GENERATION_FUNCTION = "generate";

    public static final String JS_PROOF_VERIFICATION_FUNCTION = "verify";

    public static final String JS_PROOF_PRE_COMPUTED_VALUES_GENERATION_FUNCTION =
        "generatePreComputedValues";

    public static final String JS_OR_PROOF_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/or.proof.test.js";

    public static final String JS_OR_PROOF_TEST_NAMESPACE = "orProofTest";
}
