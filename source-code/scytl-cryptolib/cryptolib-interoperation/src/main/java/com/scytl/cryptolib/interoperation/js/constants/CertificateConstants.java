/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the certificate tests.
 */
public final class CertificateConstants {

    private CertificateConstants() {
    }

    public static final String JS_CERTIFICATE_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/certificate.test.js";

    public static final String JS_CERTIFICATE_TEST_NAMESPACE =
        "certificateTest";

    public static final String JS_CERTIFICATE_OPENING_FUNCTION = "open";

    public static final String JS_CERTIFICATE_CHAIN_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/certificate.chain.test.js";

    public static final String JS_CERTIFICATE_CHAIN_TEST_NAMESPACE =
        "certificateChainTest";

    public static final String JS_CERTIFICATE_CHAIN_VALIDATION_FUNCTION =
        "validate";
}
