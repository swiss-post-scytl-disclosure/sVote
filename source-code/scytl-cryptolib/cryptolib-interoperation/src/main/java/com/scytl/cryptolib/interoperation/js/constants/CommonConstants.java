/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants common to this module.
 */
public final class CommonConstants {

    private CommonConstants() {
    }

    public static final String LOGGER_PROMPT = "[JAVA JS INTEROP] ";

    public static final String SCRIPT_ENGINE_NAME = "nashorn";

    public static final String DATA_RESOURCES_PATH = "/data";

    public static final String JS_RESOURCES_PATH = "/js";

    public static final String JS_LIBRARY_PATH =
        JS_RESOURCES_PATH + "/lib";

    public static final String JS_CONFIGURATION_PATH =
        JS_RESOURCES_PATH + "/config";

    public static final String JS_TEST_PATH = JS_RESOURCES_PATH + "/test";

    public static final String FORGE_PATH =
        JS_LIBRARY_PATH + "/forge.min.js";

    public static final String SJCL_PATH = JS_LIBRARY_PATH + "/sjcl.js";

    public static final String CRYPTOLIB_JS_PATH =
        JS_LIBRARY_PATH + "/cryptolib.min.js";

    public static final String CRYPTOLIB_DEFAULT_POLICY_JS_PATH =
        JS_CONFIGURATION_PATH + "/cryptolib.default.policy.js";

    public static final String JS_TESTS_INITIALIZE_PATH =
        JS_TEST_PATH + "/initialize.js";

    public static final String JS_TESTS_UTILITIES_PATH =
        JS_TEST_PATH + "/utils.js";

    public static final String TEST_DATA_SERIALIZATION_DELIMITER = ";";
}
