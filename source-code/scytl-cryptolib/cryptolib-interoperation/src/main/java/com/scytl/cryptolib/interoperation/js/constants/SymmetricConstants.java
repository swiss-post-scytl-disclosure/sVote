/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.interoperation.js.constants;

/**
 * Constants for the symmetric tests.
 */
public final class SymmetricConstants {

    private SymmetricConstants() {
    }

    public static final String JS_SYMMETRIC_CIPHER_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/symmetric.cipher.test.js";

    public static final String JS_SYMMETRIC_CIPHER_TEST_NAMESPACE =
        "symmetricCipherTest";

    public static final String JS_SYMMETRIC_ENCRYPTION_FUNCTION =
        "encrypt";

    public static final String JS_SYMMETRIC_DECRYPTION_FUNCTION =
        "decrypt";

    public static final String JS_MAC_TEST_PATH =
        CommonConstants.JS_TEST_PATH + "/mac.test.js";

    public static final String JS_MAC_TEST_NAMESPACE = "macTest";

    public static final String JS_MAC_GENERATION_FUNCTION = "generate";

    public static final String JS_MAC_VERIFICATION_FUNCTION = "verify";
}
