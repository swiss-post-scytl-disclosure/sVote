/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.factory.builders;

import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.scytl.cryptolib.certificates.bean.extensions.BasicConstraintsExtension;
import com.scytl.cryptolib.certificates.bean.extensions.ExtensionType;

import mockit.Injectable;
import mockit.NonStrictExpectations;
import mockit.integration.junit4.JMockit;

/**
 * Tests {@link ExtensionBuilder} class
 */
@RunWith(JMockit.class)
public class ExtensionBuilderTest {

    @Injectable
    private X509v3CertificateBuilder _certificateBuilder;

    @Injectable
    private BasicConstraintsExtension _certificateExtension;

    /**
     * Test method for
     * {@link com.scytl.cryptolib.certificates.factory.builders.ExtensionBuilder#addExtension(com.scytl.cryptolib.certificates.bean.extensions.AbstractCertificateExtension)}
     * .
     * 
     * @throws CertIOException
     */
    @Test
    public void testAddExtension() throws CertIOException {
        new NonStrictExpectations() {
            {
                _certificateExtension.getExtensionType();
                returns(ExtensionType.BASIC_CONSTRAINTS);

                _certificateExtension.isCritical();
                returns(true);

                _certificateExtension.isCertificateAuthority();
                returns(true);

                _certificateBuilder.addExtension(
                    Extension.basicConstraints,
                    _certificateExtension.isCritical(),
                    new BasicConstraints(
                        _certificateExtension.isCertificateAuthority()));

            }
        };

        new ExtensionBuilder(_certificateBuilder)
            .addExtension(_certificateExtension);
    }

}
