/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.service;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.util.Date;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.utils.AsymmetricTestDataGenerator;
import com.scytl.cryptolib.certificates.bean.CertificateData;
import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.cryptolib.certificates.utils.X509CertificateTestDataGenerator;

/**
 * Tests of the certificates service API for negative cases.
 */
public class CertificatesServiceNegativeTest {

    private static CertificatesService _certificatesServiceForDefaultPolicy;

    private static KeyPair _rootKeyPair;

    private static PrivateKey _rootPrivateKey;

    private static PublicKey _publicKey;

    private static CryptoAPIX509Certificate _certificate;

    @BeforeClass
    public static void setUp() throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        _certificatesServiceForDefaultPolicy = new CertificatesService();

        _rootKeyPair = AsymmetricTestDataGenerator.getKeyPairForSigning();
        _rootPrivateKey = _rootKeyPair.getPrivate();

        KeyPair keyPair =
            AsymmetricTestDataGenerator.getKeyPairForSigning();
        _publicKey = keyPair.getPublic();

        CertificateData certificateData =
            X509CertificateTestDataGenerator.getCertificateData(keyPair);
        _certificate =
             _certificatesServiceForDefaultPolicy
                .createSignX509Certificate(certificateData,
                    _rootPrivateKey);
    }

    @AfterClass
    public static void tearDown() {

        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Test
    public void testCertificateVerificationWithInvalidPublicKey()
            throws GeneralCryptoLibException {

        Assert.assertFalse(_certificate.verify(_publicKey));
    }

    @Test
    public void testCertificateDateValidityForInvalidCertificate()
            throws GeneralCryptoLibException {

        KeyPair keyPair =
            AsymmetricTestDataGenerator.getKeyPairForSigning();
        CertificateData invalidCertificateData =
            X509CertificateTestDataGenerator
                .getExpiredCertificateData(keyPair);
        CryptoAPIX509Certificate invalidCertificate =
                _certificatesServiceForDefaultPolicy
                        .createSignX509Certificate(invalidCertificateData,
                                _rootPrivateKey);

        Assert.assertFalse(invalidCertificate.checkValidity());
    }

    @Test
    public void testCertificateDateValidityCheckWithInvalidDate()
            throws GeneralCryptoLibException {

        Date invalidDate =
            X509CertificateTestDataGenerator
                .getDateOutsideValidityPeriod();

        Assert.assertFalse(_certificate.checkValidity(invalidDate));
    }
}
