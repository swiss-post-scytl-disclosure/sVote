/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates;

import com.scytl.cryptolib.asymmetric.keypair.configuration.ConfigEncryptionKeyPairAlgorithmAndSpec;
import com.scytl.cryptolib.asymmetric.keypair.configuration.ConfigSigningKeyPairAlgorithmAndSpec;
import com.scytl.cryptolib.asymmetric.keypair.configuration.KeyPairPolicy;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * An implementation of the KeyPairPolicy interface which specifies RSA as the
 * key pair algorithm and cryptographic service provider.
 */
public class RsaSigningKeyPairPolicy implements KeyPairPolicy {

    /**
     * @see com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy#getSecureRandomAlgorithmAndProvider()
     */
    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
        return ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN;
    }

    /**
     * @see com.scytl.cryptolib.asymmetric.keypair.configuration.SigningKeyPairPolicy#getSigningKeyPairAlgorithmAndSpec()
     */
    @Override
    public ConfigSigningKeyPairAlgorithmAndSpec getSigningKeyPairAlgorithmAndSpec() {
        return ConfigSigningKeyPairAlgorithmAndSpec.RSA_2048_F4_SUN_RSA_SIGN;
    }

    /**
     * @see com.scytl.cryptolib.asymmetric.keypair.configuration.EncryptionKeyPairPolicy#getEncryptingKeyPairAlgorithmAndSpec()
     */
    @Override
    public ConfigEncryptionKeyPairAlgorithmAndSpec getEncryptingKeyPairAlgorithmAndSpec() {
        return ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_2048_F4_BC;
    }
}
