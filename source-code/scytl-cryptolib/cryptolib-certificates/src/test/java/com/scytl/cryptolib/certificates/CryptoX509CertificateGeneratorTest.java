/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.utils.AsymmetricTestDataGenerator;
import com.scytl.cryptolib.certificates.bean.CertificateData;
import com.scytl.cryptolib.certificates.bean.RootCertificateData;
import com.scytl.cryptolib.certificates.bean.X509CertificateType;
import com.scytl.cryptolib.certificates.configuration.X509CertificateGeneratorPolicyFromProperties;
import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.cryptolib.certificates.factory.CryptoX509CertificateGenerator;
import com.scytl.cryptolib.certificates.factory.X509CertificateGeneratorFactory;
import com.scytl.cryptolib.certificates.utils.X509CertificateTestDataChecker;
import com.scytl.cryptolib.certificates.utils.X509CertificateTestDataGenerator;
import com.scytl.cryptolib.commons.constants.Constants;

/**
 *
 */
public class CryptoX509CertificateGeneratorTest {

    private static CryptoX509CertificateGenerator _x509certificateGenerator;

    private static KeyPair _rootKeyPair;

    private static PublicKey _rootPublicKey;

    private static PrivateKey _rootPrivateKey;

    private static CertificateData _certificateData;

    @BeforeClass
    public static void setUp() throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        X509CertificateGeneratorFactory x509CertificateGeneratorFactory =
            new X509CertificateGeneratorFactory(
                new X509CertificateGeneratorPolicyFromProperties(
                    Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH));

        _x509certificateGenerator =
            x509CertificateGeneratorFactory.create();

        _rootKeyPair = AsymmetricTestDataGenerator.getKeyPairForSigning();
        _rootPublicKey = _rootKeyPair.getPublic();
        _rootPrivateKey = _rootKeyPair.getPrivate();

        _certificateData =
            X509CertificateTestDataGenerator
                .getCertificateData(AsymmetricTestDataGenerator
                    .getKeyPairForSigning());
    }

    @AfterClass
    public static void tearDown() {

        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Test
    public final void testWhenCreateRootAuthorityCertificateFromServiceThenOk()
            throws GeneralCryptoLibException {

        RootCertificateData rootCertificateData =
            X509CertificateTestDataGenerator
                .getRootCertificateData(_rootKeyPair);

        CryptoAPIX509Certificate rootCertificate =
            _x509certificateGenerator.generate(rootCertificateData,
                X509CertificateType.CERTIFICATE_AUTHORITY.getExtensions(),
                _rootPrivateKey);

        X509CertificateTestDataChecker.assertCertificateContentCorrect(
            rootCertificate, rootCertificateData,
            X509CertificateType.CERTIFICATE_AUTHORITY, _rootPublicKey);
    }

    @Test
    public final void testWhenCreateIntermediateAuthorityCertificateFromServiceThenOk()
            throws GeneralCryptoLibException {

        CryptoAPIX509Certificate intermediateCertificate =
            _x509certificateGenerator.generate(_certificateData,
                X509CertificateType.CERTIFICATE_AUTHORITY.getExtensions(),
                _rootPrivateKey);

        X509CertificateTestDataChecker.assertCertificateContentCorrect(
            intermediateCertificate, _certificateData,
            X509CertificateType.CERTIFICATE_AUTHORITY, _rootPublicKey);
    }

    @Test
    public final void testWhenCreateSignCertificateFromServiceThenOk()
            throws GeneralCryptoLibException {

        CryptoAPIX509Certificate signCertificate =
            _x509certificateGenerator.generate(_certificateData,
                X509CertificateType.SIGN.getExtensions(), _rootPrivateKey);

        X509CertificateTestDataChecker.assertCertificateContentCorrect(
            signCertificate, _certificateData, X509CertificateType.SIGN,
            _rootPublicKey);
    }

    @Test
    public final void testWhenCreateEncryptionCertificateFromServiceThenOk()
            throws GeneralCryptoLibException {

        CryptoAPIX509Certificate encryptionCertificate =
            _x509certificateGenerator.generate(_certificateData,
                X509CertificateType.ENCRYPT.getExtensions(),
                _rootPrivateKey);

        X509CertificateTestDataChecker.assertCertificateContentCorrect(
            encryptionCertificate, _certificateData,
            X509CertificateType.ENCRYPT, _rootPublicKey);
    }
}
