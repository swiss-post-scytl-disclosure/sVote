/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.service;

import com.scytl.cryptolib.api.certificates.CertificatesServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.ServiceFactoryHelper;

/**
 * This class sets up default factories of {@link CertificatesServiceAPI}
 * objects.
 */
public final class CertificatesServiceFactoryHelper {
    /**
     * This is a helper class. It cannot be instantiated.
     */
    private CertificatesServiceFactoryHelper() {
    }

    /**
     * Retrieves a new factory of non thread-safe services.
     * <p>
     * Default factory is {@link BasicCertificatesServiceFactory}.
     * 
     * @param params
     *            list of parameters used in the creation of the default
     *            factory.
     * @return the new factory.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<CertificatesServiceAPI> getInstance(
            final Object... params) {
        return ServiceFactoryHelper.get(
            BasicCertificatesServiceFactory.class, params);

    }

    /**
     * Retrieves a new factory of thread-safe services.
     * <p>
     * Default factory is {link {@link PollingCertificatesServiceFactory}
     * 
     * @param params
     *            list of parameters used in the creation of the default
     *            factory.
     * @return the new factory of thread-safe services.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<CertificatesServiceAPI> getFactoryOfThreadSafeServices(
            final Object... params) {
        return ServiceFactoryHelper.get(
            PollingCertificatesServiceFactory.class, params);
    }

}
