/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.service;

import com.scytl.cryptolib.api.certificates.CertificatesServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link CertificatesService} objects which are non thread-safe.
 */
public class BasicCertificatesServiceFactory extends
        PropertiesConfiguredFactory<CertificatesServiceAPI> implements
        ServiceFactory<CertificatesServiceAPI> {
    /**
     * Default constructor. This factory will create services configured with
     * default values.
     */
    public BasicCertificatesServiceFactory() {
        super(CertificatesServiceAPI.class);
    }

    /**
     * This factory will create services configured with the given configuration
     * file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     */
    public BasicCertificatesServiceFactory(final String path) {
        super(CertificatesServiceAPI.class, path);
    }

    @Override
    protected CertificatesServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new CertificatesService(_path);
    }

    @Override
    protected CertificatesServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new CertificatesService();
    }

}
