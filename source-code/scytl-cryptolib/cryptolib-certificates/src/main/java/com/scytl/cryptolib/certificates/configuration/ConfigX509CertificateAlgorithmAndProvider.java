/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum which defines the algorithm and {@link Provider} of an X509 certificate.
 */
public enum ConfigX509CertificateAlgorithmAndProvider {

    SHA256_WITH_RSA_BC("SHA256withRSA", Provider.BOUNCY_CASTLE),

    SHA256_WITH_RSA_DEFAULT("SHA256withRSA", Provider.DEFAULT);

    private final String _algorithm;

    private final Provider _provider;

    private ConfigX509CertificateAlgorithmAndProvider(
            final String algorithm, final Provider provider) {
        _algorithm = algorithm;
        _provider = provider;
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public Provider getProvider() {
        return _provider;
    }
}
