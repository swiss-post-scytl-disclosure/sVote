/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;

/**
 * Interface to retrieve the X509 certificate generation cryptographic policy
 * settings.
 */
public interface X509CertificateGeneratorPolicy extends SecureRandomPolicy {

    /**
     * Retrieves the algorithm used to sign the certificate with the issuer
     * private key and the cryptographic service provider of the certificate.
     * 
     * @return signature algorithm and provider of certificate.
     */
    ConfigX509CertificateAlgorithmAndProvider getCertificateAlgorithmAndProvider();

}
