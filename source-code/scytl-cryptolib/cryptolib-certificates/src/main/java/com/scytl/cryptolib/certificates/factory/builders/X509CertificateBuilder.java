/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.factory.builders;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import com.scytl.cryptolib.certificates.bean.extensions.AbstractCertificateExtension;

/**
 * Interface for the X509 certificate builders.
 */
public interface X509CertificateBuilder {

    /**
     * Builds the certificate.
     * 
     * @param issuerPrivateKey
     *            private key of certificate issuer.
     * @return the X509 certificate.
     */
    X509Certificate build(final PrivateKey issuerPrivateKey);

    /**
     * Adds a certificate extension to the certificate builder.
     * 
     * @param extension
     *            certificate extension to add.
     */
    void addExtension(final AbstractCertificateExtension extension);
}
