/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.configuration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Class that contains the results of the validation of a
 * {@link java.security.cert.X509Certificate}.
 */
public class X509CertificateValidationResult {

    private final boolean _isValidated;

    private final Set<X509CertificateValidationType> _failedValidationTypes;

    /**
     * Create a validation result based on previous validation taken from
     * arguments.
     * 
     * @param isValidated
     *            indicates whether {@link java.security.cert.X509Certificate}
     *            is valid.
     * @param failedValidationTypes
     *            array of {@link X509CertificateValidationType} that failed.
     * @throws GeneralCryptoLibException
     *             if the validation was failed.
     */
    public X509CertificateValidationResult(final boolean isValidated,
            final X509CertificateValidationType... failedValidationTypes)
            throws GeneralCryptoLibException {

        _isValidated = isValidated;

        _failedValidationTypes = new HashSet<>();
        for (X509CertificateValidationType failedValidationType : failedValidationTypes) {
            _failedValidationTypes.add(failedValidationType);
        }
    }

    /**
     * Checks if the {@link java.security.cert.X509Certificate} is valid.
     *
     * @return true if {@link java.security.cert.X509Certificate} was validated
     *         successfully and false otherwise.
     */
    public boolean isValidated() {

        return _isValidated;
    }

    /**
     * Returns a list of {@link X509CertificateValidationType} that failed the
     * {@link java.security.cert.X509Certificate} validation.
     * 
     * @return a list of the failed {@link X509CertificateValidationType}.
     */
    public List<X509CertificateValidationType> getFailedValidationTypes() {

        return new ArrayList<>(_failedValidationTypes);
    }
}
