/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.factory;

import com.scytl.cryptolib.certificates.configuration.X509CertificateGeneratorPolicy;
import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.primitives.securerandom.factory.SecureRandomFactory;

/**
 * Factory class for creating {@link CryptoX509CertificateGenerator}.
 */
public final class X509CertificateGeneratorFactory {

    private final X509CertificateGeneratorPolicy _x509CertGeneratorPolicy;

    /**
     * Constructor which uses the specified
     * {@link X509CertificateGeneratorPolicy}.
     * 
     * @param x509CertGeneratorPolicy
     *            X509 certificate generator policy.
     */
    public X509CertificateGeneratorFactory(
            final X509CertificateGeneratorPolicy x509CertGeneratorPolicy) {

        _x509CertGeneratorPolicy = x509CertGeneratorPolicy;
    }

    /**
     * Creates a certificate generator.
     * @return a {@link CryptoX509CertificateGenerator}.
     */
    public CryptoX509CertificateGenerator create() {

        String algorithm =
            _x509CertGeneratorPolicy.getCertificateAlgorithmAndProvider()
                .getAlgorithm();
        Provider provider =
            _x509CertGeneratorPolicy.getCertificateAlgorithmAndProvider()
                .getProvider();

        SecureRandomFactory secureRandomFactory =
            new SecureRandomFactory(_x509CertGeneratorPolicy);

        return new CryptoX509CertificateGenerator(algorithm, provider,
            secureRandomFactory.createIntegerRandom());
    }
}
