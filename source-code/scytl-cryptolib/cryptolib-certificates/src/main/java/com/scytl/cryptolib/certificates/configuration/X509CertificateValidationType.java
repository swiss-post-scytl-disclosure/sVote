/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.configuration;

/**
 * Enum which defines the X509 certificate validation types.
 */
public enum X509CertificateValidationType {

    DATE,

    SUBJECT,

    ISSUER,

    KEY_TYPE,

    SIGNATURE
}
