/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.service;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.certificates.CertificatesServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.PooledProxiedServiceFactory;

/**
 * This Service factory creates thread-safe services. Thread-safe services proxy
 * all requests to a pool of non thread-safe service instances.
 */
public class PollingCertificatesServiceFactory extends
        PooledProxiedServiceFactory<CertificatesServiceAPI> implements
        ServiceFactory<CertificatesServiceAPI> {
    /**
     * Constructor that uses default values.
     */
    public PollingCertificatesServiceFactory() {
        super(new BasicCertificatesServiceFactory(),
            new GenericObjectPoolConfig());
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param path
     *            the path where the properties file is located.
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingCertificatesServiceFactory(final String path,
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicCertificatesServiceFactory(path), poolConfig);
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingCertificatesServiceFactory(
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicCertificatesServiceFactory(), poolConfig);
    }

}
