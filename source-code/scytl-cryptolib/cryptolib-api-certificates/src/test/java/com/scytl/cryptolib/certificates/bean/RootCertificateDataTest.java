/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean;

import java.security.PublicKey;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.test.tools.bean.TestPublicKey;

import mockit.Deencapsulation;
import mockit.Injectable;

public class RootCertificateDataTest {

    private RootCertificateData _target;

    @Before
    public void setUp() {
        _target = new RootCertificateData();
    }

    @Test
    public void getPublicKeyTest(@Injectable final PublicKey value) {

        Assert.assertNotEquals(value, _target.getSubjectPublicKey());
        Deencapsulation.setField(_target, value);
        Assert.assertEquals(value, _target.getSubjectPublicKey());
    }

    @Test
    public void getSubjectDNTest(
            @Injectable final X509DistinguishedName value) {

        Assert.assertNotEquals(value, _target.getSubjectDn());
        Deencapsulation.setField(_target, value);
        Assert.assertEquals(value, _target.getSubjectDn());
    }

    @Test
    public void getValidityDaysTest(
            @Injectable final ValidityDates value) {

        Assert.assertNotEquals(value, _target.getValidityDates());
        Deencapsulation.setField(_target, value);
        Assert.assertEquals(value, _target.getValidityDates());
    }

    @Test
    public void getIssuerTest(
            @Injectable final X509DistinguishedName value) {

        Assert.assertNotEquals(value, _target.getIssuerDn());
        Deencapsulation.setField(_target, value);
        Assert.assertEquals(value, _target.getIssuerDn());
    }

    @Test
    public void setPublicKeyTest() throws GeneralCryptoLibException {

        PublicKey publicKey = new TestPublicKey(new byte[1]);

        Assert.assertNotEquals(publicKey,
            Deencapsulation.getField(_target, PublicKey.class));
        _target.setSubjectPublicKey(publicKey);

        Assert.assertEquals(publicKey,
            Deencapsulation.getField(_target, PublicKey.class));
    }

    @Test
    public void setSubjectDNTest(
            @Injectable final X509DistinguishedName value)
            throws GeneralCryptoLibException {

        Assert.assertNotEquals(value, Deencapsulation.getField(_target,
            X509DistinguishedName.class));
        _target.setSubjectDn(value);

        Assert.assertEquals(value, Deencapsulation.getField(_target,
            X509DistinguishedName.class));

    }

    @Test
    public void setValidityDaysTest(@Injectable final ValidityDates value)
            throws GeneralCryptoLibException {

        Assert.assertNotEquals(value,
            Deencapsulation.getField(_target, ValidityDates.class));
        _target.setValidityDates(value);

        Assert.assertEquals(value,
            Deencapsulation.getField(_target, ValidityDates.class));
    }
}
