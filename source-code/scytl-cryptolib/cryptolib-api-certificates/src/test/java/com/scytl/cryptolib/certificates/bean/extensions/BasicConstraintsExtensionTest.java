/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean.extensions;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.certificates.bean.extensions.BasicConstraintsExtension;
import com.scytl.cryptolib.certificates.bean.extensions.ExtensionType;
import com.scytl.cryptolib.certificates.constants.X509CertificateConstants;

/**
 * Test of the class BasicConstraintsExtension.
 */
public class BasicConstraintsExtensionTest {

    private static final boolean ROOT_BASIC_CONSTRAINTS_EXTENSION_IS_CRITICAL =
        true;

    private static final boolean USER_BASIS_CONSTRAINTS_EXTENSION_IS_CRITICAL =
        false;

    private static final boolean ROOT_IS_CERTIFICATE_AUTHORITY = true;

    private static final boolean USER_IS_CERTIFICATE_AUTHORITY = false;

    private static BasicConstraintsExtension _rootBasicConstraintsExtension;

    private static BasicConstraintsExtension _userBasicConstraintsExtension;

    @BeforeClass
    public static void setUp() {

        _rootBasicConstraintsExtension =
            new BasicConstraintsExtension(
                ROOT_BASIC_CONSTRAINTS_EXTENSION_IS_CRITICAL,
                ROOT_IS_CERTIFICATE_AUTHORITY);

        _userBasicConstraintsExtension =
            new BasicConstraintsExtension(
                USER_BASIS_CONSTRAINTS_EXTENSION_IS_CRITICAL,
                USER_IS_CERTIFICATE_AUTHORITY);
    }

    @Test
    public void retrieveExtensionType() {

        boolean rootExtensionTypeRetrieved =
            _rootBasicConstraintsExtension.getExtensionType() == ExtensionType.BASIC_CONSTRAINTS;

        boolean userExtensionTypeRetrieved =
            _userBasicConstraintsExtension.getExtensionType() == ExtensionType.BASIC_CONSTRAINTS;

        assert (rootExtensionTypeRetrieved && userExtensionTypeRetrieved);
    }

    @Test
    public void retrieveIsCriticalFlag() {

        boolean rootIsCriticalFlagRetrieved =
            _rootBasicConstraintsExtension.isCritical() == ROOT_BASIC_CONSTRAINTS_EXTENSION_IS_CRITICAL;

        boolean userIsCriticalFlagRetrieved =
            _userBasicConstraintsExtension.isCritical() == USER_BASIS_CONSTRAINTS_EXTENSION_IS_CRITICAL;

        assert (rootIsCriticalFlagRetrieved && userIsCriticalFlagRetrieved);
    }

    @Test
    public void retrieveInternallySetIsCriticalFlag() {

        BasicConstraintsExtension rootBasicConstraintsExtension =
            new BasicConstraintsExtension(ROOT_IS_CERTIFICATE_AUTHORITY);
        boolean rootIsCriticalFlagRetrieved =
            rootBasicConstraintsExtension.isCritical() == X509CertificateConstants.CERTIFICATE_EXTENSION_IS_CRITICAL_FLAG_DEFAULT;
        ;

        BasicConstraintsExtension userBasicConstraintsExtension =
            new BasicConstraintsExtension(USER_IS_CERTIFICATE_AUTHORITY);
        boolean userIsCriticalFlagRetrieved =
            userBasicConstraintsExtension.isCritical() == X509CertificateConstants.CERTIFICATE_EXTENSION_IS_CRITICAL_FLAG_DEFAULT;
        ;

        assert (rootIsCriticalFlagRetrieved && userIsCriticalFlagRetrieved);
    }

    @Test
    public void retrieveIsCertificateAuthorityFlag() {

        boolean rootIsCertificateAuthorityFlagRetrieved =
            _rootBasicConstraintsExtension.isCertificateAuthority() == ROOT_IS_CERTIFICATE_AUTHORITY;

        boolean userIsCertificateAuthorityFlagRetrieved =
            _userBasicConstraintsExtension.isCertificateAuthority() == USER_IS_CERTIFICATE_AUTHORITY;

        assert (rootIsCertificateAuthorityFlagRetrieved && userIsCertificateAuthorityFlagRetrieved);
    }

}
