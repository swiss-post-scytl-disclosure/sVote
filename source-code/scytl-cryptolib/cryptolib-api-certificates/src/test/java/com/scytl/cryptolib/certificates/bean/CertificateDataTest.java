/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

import mockit.Deencapsulation;
import mockit.Injectable;
import mockit.Tested;

/**
 * Tests of CertificateData.
 */
public class CertificateDataTest {

    @Tested
    CertificateData _target;

    @Test
    public void getIssuerDnTest(
            @Injectable final X509DistinguishedName value) {

        Assert.assertNotEquals(value, _target.getIssuerDn());
        Deencapsulation.setField(_target, value);
        Assert.assertEquals(value, _target.getIssuerDn());
    }

    @Test
    public void setIssuerDnTest(
            @Injectable final X509DistinguishedName value)
            throws GeneralCryptoLibException {

        Assert.assertNotEquals(value, Deencapsulation.getField(_target,
            X509DistinguishedName.class));
        _target.setIssuerDn(value);

        Assert.assertEquals(value, Deencapsulation.getField(_target,
            X509DistinguishedName.class));
    }

}
