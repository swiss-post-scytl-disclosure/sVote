/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.certificates.constants.X509CertificateConstants;

/**
 * Tests of ValidityDates.
 */
public class ValidityDatesTest {

    @Test
    public void whenSetValidityDatesThenOK()
            throws GeneralCryptoLibException {

        Date notBefore = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        Date notAfter = calendar.getTime();

        ValidityDates validityDates =
            new ValidityDates(notBefore, notAfter);

        boolean startingDateRetrieved =
            validityDates.getNotBefore().equals(notBefore);

        boolean endingDateRetrieved =
            validityDates.getNotAfter().equals(notAfter);

        boolean startingDateBeforeEndingDate =
            notBefore.compareTo(notAfter) < 0;

        assert (startingDateRetrieved && endingDateRetrieved && startingDateBeforeEndingDate);
    }

    @Test
    public void whenSetMaximumAllowedStartingDateThenOK()
            throws GeneralCryptoLibException {

        Calendar calendar = Calendar.getInstance();
        calendar
            .set(
                X509CertificateConstants.X509_CERTIFICATE_MAXIMUM_VALIDITY_DATE_YEAR,
                Calendar.JANUARY, 0);
        Date notBefore = calendar.getTime();
        calendar
            .set(
                X509CertificateConstants.X509_CERTIFICATE_MAXIMUM_VALIDITY_DATE_YEAR,
                Calendar.FEBRUARY, 0);
        Date notAfter = calendar.getTime();

        ValidityDates validityDates =
            new ValidityDates(notBefore, notAfter);

        boolean startingDateRetrieved =
            validityDates.getNotBefore().equals(notBefore);

        boolean endingDateRetrieved =
            validityDates.getNotAfter().equals(notAfter);

        boolean startingDateBeforeEndingDate =
            notBefore.compareTo(notAfter) < 0;

        assert (startingDateRetrieved && endingDateRetrieved && startingDateBeforeEndingDate);
    }

    @Test
    public void whenSetMaximumAllowedEndingDateThenOK()
            throws GeneralCryptoLibException {

        Date notBefore = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar
            .set(
                X509CertificateConstants.X509_CERTIFICATE_MAXIMUM_VALIDITY_DATE_YEAR,
                Calendar.JANUARY, 1);
        Date notAfter = calendar.getTime();

        ValidityDates validityDates =
            new ValidityDates(notBefore, notAfter);

        boolean startingDateRetrieved =
            validityDates.getNotBefore().equals(notBefore);

        boolean endingDateRetrieved =
            validityDates.getNotAfter().equals(notAfter);

        boolean startingDateBeforeEndingDate =
            notBefore.compareTo(notAfter) < 0;

        assert (startingDateRetrieved && endingDateRetrieved && startingDateBeforeEndingDate);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenNullStartingDateThenException()
            throws GeneralCryptoLibException {

        Date notBefore = null;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        Date notAfter = calendar.getTime();

        new ValidityDates(notBefore, notAfter);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenNullEndingDateThenException()
            throws GeneralCryptoLibException {

        Date notBefore = new Date(System.currentTimeMillis());
        Date notAfter = null;

        new ValidityDates(notBefore, notAfter);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenStartingDateAfterEndingDateThenException()
            throws GeneralCryptoLibException {

        Date notAfter = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        Date notBefore = calendar.getTime();

        new ValidityDates(notBefore, notAfter);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenStartingDateEqualsEndingDateThenException()
            throws GeneralCryptoLibException {

        Date notBefore = new Date(System.currentTimeMillis());
        Date notAfter = new Date(notBefore.getTime());

        new ValidityDates(notBefore, notAfter);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenTooLargeStartingDateThenException()
            throws GeneralCryptoLibException {

        Calendar calendar = Calendar.getInstance();
        calendar
            .set(
                X509CertificateConstants.X509_CERTIFICATE_MAXIMUM_VALIDITY_DATE_YEAR + 1,
                Calendar.JANUARY, 1);
        Date notBefore = calendar.getTime();
        calendar
            .set(
                X509CertificateConstants.X509_CERTIFICATE_MAXIMUM_VALIDITY_DATE_YEAR + 1,
                Calendar.FEBRUARY, 0);
        Date notAfter = calendar.getTime();

        new ValidityDates(notBefore, notAfter);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenTooLargeEndingDateThenException()
            throws GeneralCryptoLibException {

        Date notBefore = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar
            .set(
                X509CertificateConstants.X509_CERTIFICATE_MAXIMUM_VALIDITY_DATE_YEAR + 1,
                Calendar.JANUARY, 1);
        Date notAfter = calendar.getTime();

        new ValidityDates(notBefore, notAfter);
    }
}
