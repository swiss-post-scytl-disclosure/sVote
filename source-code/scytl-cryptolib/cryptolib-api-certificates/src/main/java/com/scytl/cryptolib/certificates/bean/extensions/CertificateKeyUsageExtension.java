/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean.extensions;

import java.util.EnumSet;

import com.scytl.cryptolib.certificates.constants.X509CertificateConstants;

/**
 * Key usage certificate extension.
 */
public class CertificateKeyUsageExtension extends
        AbstractCertificateExtension {

    private final boolean _isCritical;

    private final EnumSet<CertificateKeyUsage> _certificateKeyUsages;

    /**
     * Creates a certificate extension with provided arguments.
     *
     * @param isCritical
     *            flag indicating whether extension is critical.
     * @param certificateKeyUsages
     *            key usages of extension.
     */
    public CertificateKeyUsageExtension(final boolean isCritical,
            final EnumSet<CertificateKeyUsage> certificateKeyUsages) {

        super();

        _isCritical = isCritical;
        _certificateKeyUsages = certificateKeyUsages.clone();
    }

    /**
     * Constructor, with {@code isCritical} flag set to default value defined in
     * {@link com.scytl.cryptolib.certificates.constants.X509CertificateConstants#CERTIFICATE_EXTENSION_IS_CRITICAL_FLAG_DEFAULT}
     * .
     * 
     * @param certificateKeyUsages
     *            key usages of extension.
     */
    public CertificateKeyUsageExtension(
            final EnumSet<CertificateKeyUsage> certificateKeyUsages) {

        super();

        _isCritical =
            X509CertificateConstants.CERTIFICATE_EXTENSION_IS_CRITICAL_FLAG_DEFAULT;
        _certificateKeyUsages = certificateKeyUsages.clone();
    }

    @Override
    public ExtensionType getExtensionType() {

        return ExtensionType.KEY_USAGE;
    }

    @Override
    public boolean isCritical() {

        return _isCritical;
    }

    /**
     * Returns a list of of the key usages.
     * 
     * @return a list of of the key usages.
     */
    public EnumSet<CertificateKeyUsage> getKeyUsages() {

        return _certificateKeyUsages.clone();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((_certificateKeyUsages == null) ? 0 : _certificateKeyUsages.hashCode());
        result = prime * result + (_isCritical ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CertificateKeyUsageExtension other = (CertificateKeyUsageExtension) obj;
        if (_certificateKeyUsages == null) {
            if (other._certificateKeyUsages != null)
                return false;
        } else if (!_certificateKeyUsages.equals(other._certificateKeyUsages))
            return false;
        if (_isCritical != other._isCritical)
            return false;
        return true;
    }
}
