/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean.extensions;

/**
 * Base class for the X509 certificate extensions.
 */
public abstract class AbstractCertificateExtension {

    /**
     * Returns the extension type of the certificate.
     * 
     * @return the extension type.
     */
    public abstract ExtensionType getExtensionType();

    /**
     * Indicates whether extension is critical.
     * 
     * @return true if the extension is critical and false otherwise.
     */
    public abstract boolean isCritical();

    @Override
    public boolean equals(final Object obj) {

        if (obj instanceof AbstractCertificateExtension) {
            AbstractCertificateExtension certExtensionObj =
                (AbstractCertificateExtension) obj;

            return getExtensionType() == certExtensionObj
                .getExtensionType();
        }

        return false;
    }

    @Override
    public int hashCode() {

        return getExtensionType().hashCode();
    }
}
