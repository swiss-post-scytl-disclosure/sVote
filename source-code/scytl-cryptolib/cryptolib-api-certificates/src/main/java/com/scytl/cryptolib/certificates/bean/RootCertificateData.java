/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean;

import java.security.PublicKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.utils.validations.Validate;

/**
 * Contains all necessary certificate data to build a root certificate.
 */
public class RootCertificateData implements CertificatePublicData {

    private PublicKey _subjectPublicKey;

    private X509DistinguishedName _subjectDn;

    private ValidityDates _validityDates;

    @Override
    public PublicKey getSubjectPublicKey() {

        return _subjectPublicKey;
    }

    public void setSubjectPublicKey(final PublicKey subjectPublicKey)
            throws GeneralCryptoLibException {

        Validate.notNull(subjectPublicKey, "Subject public key");
        Validate.notNullOrEmpty(subjectPublicKey.getEncoded(),
            "Subject public key content");

        _subjectPublicKey = subjectPublicKey;
    }

    @Override
    public X509DistinguishedName getSubjectDn() {

        return _subjectDn;
    }

    public void setSubjectDn(final X509DistinguishedName subjectDn)
            throws GeneralCryptoLibException {

        Validate.notNull(subjectDn, "Subject distinguished name");

        _subjectDn = subjectDn;
    }

    /**
     * Root Authorities are self-issued. So issuer is the same as the subject.
     */
    @Override
    public X509DistinguishedName getIssuerDn() {

        return getSubjectDn();
    }

    @Override
    public ValidityDates getValidityDates() {

        return _validityDates;
    }

    public void setValidityDates(final ValidityDates validityDates)
            throws GeneralCryptoLibException {

        Validate.notNull(validityDates, "Validity dates object");

        _validityDates = validityDates;
    }
}
