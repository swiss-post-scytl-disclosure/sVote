/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean.extensions;

/**
 * Enum which defines the certificate extension types.
 */
public enum ExtensionType {

    BASIC_CONSTRAINTS,

    KEY_USAGE
}
