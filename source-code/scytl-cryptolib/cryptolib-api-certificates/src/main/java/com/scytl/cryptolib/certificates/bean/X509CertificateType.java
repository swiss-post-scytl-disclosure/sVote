/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean;

import java.util.EnumSet;

import com.scytl.cryptolib.certificates.bean.extensions.AbstractCertificateExtension;
import com.scytl.cryptolib.certificates.bean.extensions.BasicConstraintsExtension;
import com.scytl.cryptolib.certificates.bean.extensions.CertificateKeyUsage;
import com.scytl.cryptolib.certificates.bean.extensions.CertificateKeyUsageExtension;

/**
 * Enum that defines the supported certificate types.
 */
public enum X509CertificateType {

    /**
     * Certificate authority certificate type.
     * <ul>
     * <li>Basic Constraints extension with the cA flag set to TRUE</li>
     * <li>Certificate signing key usage extension</li>
     * <li>CRL signing key usage extension</li>
     * </ul>
     */
    CERTIFICATE_AUTHORITY(true, EnumSet.of(
        CertificateKeyUsage.KEY_CERT_SIGN, CertificateKeyUsage.CRL_SIGN)),

    /**
     * Sign certificate type.
     * <ul>
     * <li>Digital signature key usage extension</li>
     * <li>Non-repudiation key usage extension</li>
     * </ul>
     */
    SIGN(false, EnumSet.of(CertificateKeyUsage.DIGITAL_SIGNATURE,
        CertificateKeyUsage.NON_REPUDIATION)),

    /**
     * Encrypt certificate type.
     * <ul>
     * <li>Key encipherment key usage extension</li>
     * <li>Data encipherment key usage extension</li>
     * </ul>
     */
    ENCRYPT(false, EnumSet.of(CertificateKeyUsage.KEY_ENCIPHERMENT,
        CertificateKeyUsage.DATA_ENCIPHERMENT));

    private final EnumSet<CertificateKeyUsage> _certificateKeyUsages;

    private final boolean _isCertAuthority;

    private X509CertificateType(final boolean isCertAuthority,
            final EnumSet<CertificateKeyUsage> certificateKeyUsage) {

        _isCertAuthority = isCertAuthority;
        _certificateKeyUsages = certificateKeyUsage;
    }

    public AbstractCertificateExtension[] getExtensions() {

        return new AbstractCertificateExtension[] {
                new BasicConstraintsExtension(_isCertAuthority),
                new CertificateKeyUsageExtension(_certificateKeyUsages) };
    }
}
