/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean.extensions;

/**
 * Enum which defines the usages of the extended key usage certificate
 * extension.
 */
public enum ExtendedKeyUsage {

    SERVER_AUTH,

    CLIENT_AUTH
}
