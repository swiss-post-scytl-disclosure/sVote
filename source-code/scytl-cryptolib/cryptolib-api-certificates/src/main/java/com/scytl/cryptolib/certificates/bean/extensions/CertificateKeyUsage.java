/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean.extensions;

/**
 * Enum which defines the usages of the key usage certificate extension.
 */
public enum CertificateKeyUsage {

    DIGITAL_SIGNATURE,

    NON_REPUDIATION,

    KEY_ENCIPHERMENT,

    DATA_ENCIPHERMENT,

    KEY_AGREEMENT,

    KEY_CERT_SIGN,

    CRL_SIGN,

    ENCIPHER_ONLY,

    DECIPHER_ONLY
}
