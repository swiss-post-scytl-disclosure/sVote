/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.utils.validations.Validate;

/**
 * Class which contains the basic certificate data.
 */
public class CertificateData extends RootCertificateData implements
        CertificatePublicData {

    private X509DistinguishedName _issuerDn;

    @Override
    public X509DistinguishedName getIssuerDn() {
        return _issuerDn;
    }

    /**
     * Assigns the issue to the certificate.
     * 
     * @param issuerDn
     *            the issuer {@link X509DistinguishedName} to be assigned to the
     *            certificate.
     * @throws GeneralCryptoLibException
     */
    public void setIssuerDn(final X509DistinguishedName issuerDn)
            throws GeneralCryptoLibException {

        Validate.notNull(issuerDn, "Issuer distinguished name");

        _issuerDn = issuerDn;
    }
}
