/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean;

import java.security.PublicKey;

/**
 * Provides public data that is used to build a certificate.
 */
public interface CertificatePublicData {

    PublicKey getSubjectPublicKey();

    X509DistinguishedName getSubjectDn();

    X509DistinguishedName getIssuerDn();

    ValidityDates getValidityDates();

}
