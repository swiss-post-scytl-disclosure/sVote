/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.certificates.bean.extensions;

import com.scytl.cryptolib.certificates.constants.X509CertificateConstants;

/**
 * Basic constraints certificate extension.
 */
public class BasicConstraintsExtension extends
        AbstractCertificateExtension {

    private final boolean _isCritical;

    private final boolean _isCertificateAuthority;

    /**
     * Creates a certificate extension with provided arguments.
     * 
     * @param isCritical
     *            Flag indicating whether extension is critical.
     * @param isCertificateAuthority
     *            Flag indicating whether certificate is a certificate
     *            authority.
     */
    public BasicConstraintsExtension(final boolean isCritical,
            final boolean isCertificateAuthority) {

        super();

        _isCritical = isCritical;
        _isCertificateAuthority = isCertificateAuthority;
    }

    /**
     * Constructor, with {@code isCritical} flag set to default value defined in
     * {@link X509CertificateConstants#CERTIFICATE_EXTENSION_IS_CRITICAL_FLAG_DEFAULT}
     * .
     * 
     * @param isCertificateAuthority
     *            flag indicating whether certificate is a certificate
     *            authority.
     */
    public BasicConstraintsExtension(final boolean isCertificateAuthority) {

        super();

        _isCritical =
            X509CertificateConstants.CERTIFICATE_EXTENSION_IS_CRITICAL_FLAG_DEFAULT;
        _isCertificateAuthority = isCertificateAuthority;
    }

    @Override
    public ExtensionType getExtensionType() {

        return ExtensionType.BASIC_CONSTRAINTS;
    }

    @Override
    public boolean isCritical() {

        return _isCritical;
    }

    public boolean isCertificateAuthority() {

        return _isCertificateAuthority;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (_isCertificateAuthority ? 1231 : 1237);
        result = prime * result + (_isCritical ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        BasicConstraintsExtension other = (BasicConstraintsExtension) obj;
        if (_isCertificateAuthority != other._isCertificateAuthority)
            return false;
        if (_isCritical != other._isCritical)
            return false;
        return true;
    }
}
