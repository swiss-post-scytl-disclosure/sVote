/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.proof;

import static junitparams.JUnitParamsRunner.$;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

/**
 * Tests of the Proof API input validation.
 */
@RunWith(JUnitParamsRunner.class)
public class ProofValidationTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @Parameters(method = "createProof")
    public void testProofCreationValidation(Exponent hashValue,
            List<Exponent> proofValues, String errorMsg)
            throws GeneralCryptoLibException {

        thrown.expect(GeneralCryptoLibException.class);
        thrown.expectMessage(errorMsg);
        new Proof(hashValue, proofValues);
    }

    public static Object[] createProof() throws GeneralCryptoLibException {

        BigInteger q = new BigInteger("11");

        Exponent hashValue = new Exponent(q, BigInteger.TEN);

        List<Exponent> exponentList = new ArrayList<Exponent>();
        exponentList.add(new Exponent(q, BigInteger.ONE));
        exponentList.add(new Exponent(q, new BigInteger("4")));

        List<Exponent> exponentListWithNullElement =
            new ArrayList<>(exponentList);
        exponentListWithNullElement.add(null);

        BigInteger differentQ = new BigInteger("12");
        List<Exponent> exponentListWithdifferentQExponent =
            new ArrayList<>(exponentList);
        exponentListWithdifferentQExponent.add(new Exponent(differentQ,
            BigInteger.TEN));

        System.out.println(new Exponent(differentQ, BigInteger.TEN)
            .toJson());

        return $(
            $(null, exponentList, "Hash value of proof is null."),
            $(hashValue, null, "List of proof exponents is null."),
            $(hashValue, new ArrayList<Exponent>(),
                "List of proof exponents is empty."),
            $(hashValue, exponentListWithNullElement,
                "List of proof exponents contains one or more null elements."),
            $(hashValue,
                exponentListWithdifferentQExponent,
                "Zp subgroup q parameter of proof exponent value must be equal to Zp subgroup q parameter of proof hash value: "
                    + hashValue.getQ() + "; Found " + differentQ));
    }
}
