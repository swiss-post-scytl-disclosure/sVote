/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.proof;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

public class ProofTest {

    private Proof _target;

    @Before
    public void setup() throws GeneralCryptoLibException {

        BigInteger q = new BigInteger("11");

        Exponent exp = new Exponent(q, BigInteger.TEN);

        List<Exponent> expList = new ArrayList<Exponent>();
        expList.add(new Exponent(q, BigInteger.ONE));
        expList.add(new Exponent(q, new BigInteger("4")));

        _target = new Proof(exp, expList);

    }

    @Test
    public void whenCreateProofFromJsonStringNewProofWillCreateSameJsonString()
            throws GeneralCryptoLibException {

        Proof proof = Proof.fromJson(_target.toJson());

        Assert.assertTrue(_target.toJson().equals(proof.toJson()));
    }

    @Test
    public void whenWriteProofToJsonStringCanReconstructOriginalProof()
            throws GeneralCryptoLibException {

        Proof proof = Proof.fromJson(_target.toJson());

        Assert.assertTrue(_target.equals(proof));
    }
}
