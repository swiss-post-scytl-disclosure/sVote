/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.bean;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.utils.validations.Validate;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;

/**
 * Container class for the pre-computed values of a zero knowledge proof.
 */
public final class ProofPreComputedValues {

    private final List<Exponent> _exponents;

    private final List<ZpGroupElement> _phiOutputs;

    /**
     * Default constructor.
     * 
     * @param exponents
     *            the randomly generated exponents, as a list of
     *            {@link com.scytl.cryptolib.mathematical.groups.impl.Exponent}
     *            objects.
     * @param phiOutputs
     *            the outputs of the phi function, as a list of
     *            {@link com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement}
     *            objects.
     * @throws GeneralCryptoLibException
     *             if the list of random exponents or phi function outputs is
     *             null, empty or contains one or more null elements.
     */
    public ProofPreComputedValues(final List<Exponent> exponents,
            final List<ZpGroupElement> phiOutputs)
            throws GeneralCryptoLibException {

        Validate.notNullOrEmptyAndNoNulls(exponents,
            "List of random exponents");
        Validate.notNullOrEmptyAndNoNulls(phiOutputs,
            "List of phi function outputs");

        _exponents = new ArrayList<>(exponents);
        _phiOutputs = new ArrayList<>(phiOutputs);
    }

    public List<Exponent> getExponents() {

        return new ArrayList<>(_exponents);
    }

    public List<ZpGroupElement> getPhiOutputs() {

        return new ArrayList<>(_phiOutputs);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((_exponents == null) ? 0 : _exponents.hashCode());
        result = prime * result
            + ((_phiOutputs == null) ? 0 : _phiOutputs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProofPreComputedValues other = (ProofPreComputedValues) obj;
        if (_exponents == null) {
            if (other._exponents != null) {
                return false;
            }
        } else if (!_exponents.equals(other._exponents)) {
            return false;
        }
        if (_phiOutputs == null) {
            if (other._phiOutputs != null) {
                return false;
            }
        } else if (!_phiOutputs.equals(other._phiOutputs)) {
            return false;
        }
        return true;
    }
}
