/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.factory;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.scytl.keystore.configuration.ScytlKeyStoreP12PolicyFromProperties;
import com.scytl.cryptolib.scytl.keystore.configuration.ScytlKeyStorePolicyFromProperties;
import com.scytl.cryptolib.scytl.keystore.tests.BaseScytlKeyStoreTests;

public class ScytlKeyStoreWithPBKDFGeneratorFactoryTest extends BaseScytlKeyStoreTests {

    ScytlKeyStoreWithPBKDFGeneratorFactory _target;

    @Before
    public void setup() throws GeneralCryptoLibException {
        ScytlKeyStorePolicyFromProperties storePolicy =
            new ScytlKeyStoreP12PolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
        _target = new ScytlKeyStoreWithPBKDFGeneratorFactory(storePolicy);
    }

    @Test
    public void createTest() throws GeneralSecurityException, IOException {

        ScytlKeyStoreWithPBKDFGenerator obj = _target.create();
        Assert
            .assertNotNull("CryptoStore created should not be null", obj);
    }
}
