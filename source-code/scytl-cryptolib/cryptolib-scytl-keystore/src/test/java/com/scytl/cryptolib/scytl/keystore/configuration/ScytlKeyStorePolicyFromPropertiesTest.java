/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.configuration;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;

import mockit.Deencapsulation;

public class ScytlKeyStorePolicyFromPropertiesTest {

    private static OperatingSystem _os;

    @BeforeClass
    public static void setUp()
            throws NoSuchAlgorithmException, NoSuchProviderException {
        _os = OperatingSystem.current();

    }

    @AfterClass
    public static void tearDown() {
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @After
    public void revertSystemHelper()
            throws NoSuchAlgorithmException, NoSuchProviderException {
        Deencapsulation.setField(OperatingSystem.class, "current", _os);

    }

    @Test
    public void okTest() throws GeneralCryptoLibException {
        ScytlKeyStorePolicyFromProperties policy =
            new ScytlKeyStorePolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH, "p12");
        Assert.assertEquals("created policy from file ok test",
            Provider.SUN_JSSE, policy.getStoreTypeAndProvider()
                .getProvider());
    }

    @Test
    public void okTrimKeyTest() throws GeneralCryptoLibException {
        ScytlKeyStorePolicyFromProperties policy =
            new ScytlKeyStorePolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH, " p12 ");
        Assert.assertEquals("created policy from file ok test",
            Provider.SUN_JSSE, policy.getStoreTypeAndProvider()
                .getProvider());

    }

    @Test(expected = CryptoLibException.class)
    public void nullPathTest() throws GeneralCryptoLibException {
        new ScytlKeyStorePolicyFromProperties(null, "p12");
    }

    @Test(expected = CryptoLibException.class)
    public void invalidPathTest() throws GeneralCryptoLibException {
        new ScytlKeyStorePolicyFromProperties("INVALID!", "myKey");
    }

    @Test(expected = CryptoLibException.class)
    public void invalidKeyTest() throws GeneralCryptoLibException {
        new ScytlKeyStorePolicyFromProperties(
            Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH, "myKey");
    }

    @Test(expected = CryptoLibException.class)
    public void blankKeyTest() throws GeneralCryptoLibException {
        new ScytlKeyStorePolicyFromProperties(
            Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH, " ");
    }

    @Test
    public void getOsLinuxTest() throws GeneralCryptoLibException {

        Deencapsulation.setField(OperatingSystem.class, "current",
            OperatingSystem.LINUX);
        new ScytlKeyStorePolicyFromProperties(
            Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH, "p12");
    }

    @Test
    public void getOsWindowsTest() throws GeneralCryptoLibException {

        Deencapsulation.setField(OperatingSystem.class, "current",
            OperatingSystem.WINDOWS);
        new ScytlKeyStorePolicyFromProperties(
            Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH, "p12");
        System.err.println(OperatingSystem.current());
    }

    @Test(expected = CryptoLibException.class)
    public void getOsOtherTest() throws GeneralCryptoLibException {

        Deencapsulation.setField(OperatingSystem.class, "current",
            OperatingSystem.OTHER);
        new ScytlKeyStorePolicyFromProperties(
            Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH, "p12");
        System.err.println(OperatingSystem.current());
    }
}
