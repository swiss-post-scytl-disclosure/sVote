/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.factory;

import java.util.Base64;
import java.util.Map;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.utils.validations.Validate;

/**
 * Utils class.
 */
public final class CryptoScytlKeyStoreWithPBKDFHelper {
    enum ENTRY_TYPE {
        SALT, SECRET, ELGAMAL, STORE
    }

    /**
     * This is a helper class. It cannot be instantiated.
     */
    private CryptoScytlKeyStoreWithPBKDFHelper() {
    }

    /**
     * Formats key store to JSON.
     *
     * @param salt
     *            the salt.
     * @param secretKeys
     *            the secret key.
     * @param keyStoreJS
     *            the sey store JS.
     * @return the key store formatted to JSON.
     * @throws GeneralCryptoLibException
     */
    public static String toJSON(final byte[] salt,
            final Map<String, byte[]> secretKeys,
            final Map<String, byte[]> elGamalPrivateKeys,
            final byte[] keyStoreJS) throws GeneralCryptoLibException {

        Validate.notNullOrEmpty(salt, "Key store salt");
        Validate.notNull(secretKeys, "Secret key map");
        Validate.notNull(elGamalPrivateKeys, "ElGamal private key map");

        StringBuilder sb = new StringBuilder("{");
        sb.append(String.format("\"salt\":\"%s\"",
            Base64.getEncoder().encodeToString(salt)));
        if (!secretKeys.isEmpty()) {
            StringBuilder nsb = new StringBuilder();

            for (Map.Entry<String, byte[]> entry : secretKeys.entrySet()) {
                nsb.append(String.format("\"%s\":\"%s\",", entry.getKey(),
                    Base64.getEncoder().encodeToString(entry.getValue())));
            }

            sb.append(String.format(",\"secrets\":{%s}",
                nsb.substring(0, nsb.length() - 1)));
        }
        if (!elGamalPrivateKeys.isEmpty()) {
            StringBuilder nsb = new StringBuilder();

            for (Map.Entry<String, byte[]> entry : elGamalPrivateKeys
                .entrySet()) {
                nsb.append(String.format("\"%s\":\"%s\",", entry.getKey(),
                    Base64.getEncoder().encodeToString(entry.getValue())));
            }

            sb.append(String.format(",\"egPrivKeys\":{%s}",
                nsb.substring(0, nsb.length() - 1)));
        }
        if (keyStoreJS != null && keyStoreJS.length != 0) {
            sb.append(String.format(",\"store\":\"%s\"",
                Base64.getEncoder().encodeToString(keyStoreJS)));
        }
        sb.append('}');

        return sb.toString();
    }

}
