/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.service;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link ScytlKeyStoreService} objects which are non thread-safe.
 */
public class BasicScytlKeyStoreServiceFactory extends
        PropertiesConfiguredFactory<ScytlKeyStoreServiceAPI> {
    /**
     * Default constructor. This factory will create services configured with
     * default values.
     */
    public BasicScytlKeyStoreServiceFactory() {
        super(ScytlKeyStoreServiceAPI.class);
    }

    /**
     * This factory will create services configured with the given configuration
     * file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     */
    public BasicScytlKeyStoreServiceFactory(final String path) {
        super(ScytlKeyStoreServiceAPI.class, path);
    }

    @Override
    protected ScytlKeyStoreServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new ScytlKeyStoreService(_path);
    }

    @Override
    protected ScytlKeyStoreServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new ScytlKeyStoreService();
    }

}
