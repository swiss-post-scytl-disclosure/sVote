/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.configuration;

import static com.scytl.cryptolib.commons.configuration.Provider.BOUNCY_CASTLE;
import static com.scytl.cryptolib.commons.configuration.Provider.DEFAULT;
import static com.scytl.cryptolib.commons.configuration.Provider.SUN_JSSE;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum representing a set of parameters that can be used when requesting an
 * instance of Store.
 * <P>
 * These parameters are:
 * <ol>
 * <li>A type.</li>
 * <li>A {@link Provider}.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigScytlKeyStoreTypeAndProvider {

    PKCS12_BC("PKCS12", BOUNCY_CASTLE),

    PKCS12_DEFAULT("PKCS12", DEFAULT),

    PKCS12_SUN_JSSE("PKCS12", SUN_JSSE);

    private final String _type;

    private final Provider _provider;

    ConfigScytlKeyStoreTypeAndProvider(final String type,
            final Provider provider) {
        _type = type;
        _provider = provider;
    }

    public String getType() {
        return _type;
    }

    public Provider getProvider() {
        return _provider;
    }
}
