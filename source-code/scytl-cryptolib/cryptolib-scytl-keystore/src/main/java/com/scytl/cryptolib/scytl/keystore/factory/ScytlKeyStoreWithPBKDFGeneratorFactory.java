/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.factory;

import com.scytl.cryptolib.primitives.derivation.factory.CryptoKeyDeriverFactory;
import com.scytl.cryptolib.scytl.keystore.configuration.ScytlKeyStorePolicy;
import com.scytl.cryptolib.symmetric.cipher.factory.SymmetricCipherFactory;

/**
 * A factory class to create {@link CryptoScytlKeyStoreWithPBKDF} generators.
 * <p>
 * This factory receives as a parameter a {@link ScytlKeyStorePolicy}.
 * <p>
 * Instances of this class are immutable.
 */
public final class ScytlKeyStoreWithPBKDFGeneratorFactory {

    private final ScytlKeyStorePolicy _storePolicy;

    /**
     * Constructor which receives a {@link ScytlKeyStorePolicy}.
     * 
     * @param storePolicy
     *            the policy to be used by this factory.
     */
    public ScytlKeyStoreWithPBKDFGeneratorFactory(
            final ScytlKeyStorePolicy storePolicy) {

        _storePolicy = storePolicy;

    }

    /**
     * Creates a {@link ScytlKeyStoreWithPBKDFGenerator}.
     * 
     * @return {@link ScytlKeyStoreWithPBKDFGenerator}
     */
    public ScytlKeyStoreWithPBKDFGenerator create() {
        CryptoKeyDeriverFactory cryptoPBKDFDeriver =
            new CryptoKeyDeriverFactory(_storePolicy);
        SymmetricCipherFactory symmetricCipherFactory =
            new SymmetricCipherFactory(_storePolicy);

        return new ScytlKeyStoreWithPBKDFGenerator(cryptoPBKDFDeriver,
            _storePolicy.getStoreTypeAndProvider().getType(), _storePolicy
                .getStoreTypeAndProvider().getProvider(),
            symmetricCipherFactory,
            _storePolicy.getSecretKeyAlgorithmAndSpec());
    }

}
