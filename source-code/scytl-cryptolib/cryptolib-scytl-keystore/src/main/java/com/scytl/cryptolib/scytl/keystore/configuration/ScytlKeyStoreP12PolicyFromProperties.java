/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.configuration;

/**
 * Implementation of the {@link ScytlKeyStorePolicy} interface, which reads
 * values from a properties file.
 * <P>
 * Instances of this class are immutable.
 */
public final class ScytlKeyStoreP12PolicyFromProperties extends
        ScytlKeyStorePolicyFromProperties implements ScytlKeyStorePolicy {

    /**
     * Creates an instance of the class with p12 from the specified properties
     * class.
     * 
     * @param path
     *            the path which specifies the properties file.
     */
    public ScytlKeyStoreP12PolicyFromProperties(final String path) {
        super(path, "p12");
    }
}
