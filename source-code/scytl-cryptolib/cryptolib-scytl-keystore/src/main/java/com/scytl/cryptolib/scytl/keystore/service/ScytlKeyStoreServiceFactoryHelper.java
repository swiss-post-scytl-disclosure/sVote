/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.service;

import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.ServiceFactoryHelper;

/**
 * This class sets up default factories of {@link ScytlKeyStoreServiceAPI} objects.
 */
public final class ScytlKeyStoreServiceFactoryHelper {

    /**
     * This is a helper class. It cannot be instantiated.
     */
    private ScytlKeyStoreServiceFactoryHelper() {
        super();
    }

    /**
     * Retrieves a new factory of non thread-safe services.
     * <p>
     * Default factory is {@link BasicScytlKeyStoreServiceFactory}.
     * 
     * @param params
     *            a list of parameters used in the creation of the default
     *            factory.
     * @return the new factory.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<ScytlKeyStoreServiceAPI> getInstance(
            final Object... params) {
        return ServiceFactoryHelper.get(BasicScytlKeyStoreServiceFactory.class,
            params);

    }

    /**
     * Retrieves a new factory of thread-safe services.
     * <p>
     * Default factory is {link {@link PollingScytlKeyStoreServiceFactory}.
     * 
     * @param params
     *            a list of parameters used in the creation of the default
     *            factory.
     * @return the new factory of thread-safe services.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<ScytlKeyStoreServiceAPI> getFactoryOfThreadSafeServices(
            final Object... params) {
        return ServiceFactoryHelper.get(PollingScytlKeyStoreServiceFactory.class,
            params);
    }

}
