/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.configuration;

import com.scytl.cryptolib.primitives.derivation.configuration.DerivationPolicy;
import com.scytl.cryptolib.symmetric.cipher.configuration.SymmetricCipherPolicy;
import com.scytl.cryptolib.symmetric.key.configuration.SecretKeyPolicy;

/**
 * Interface which specifies the methods that should be implemented by specific
 * {@code StorePolicy}.
 */
public interface ScytlKeyStorePolicy extends DerivationPolicy,
        SymmetricCipherPolicy, SecretKeyPolicy {

    ConfigScytlKeyStoreTypeAndProvider getStoreTypeAndProvider();

}
