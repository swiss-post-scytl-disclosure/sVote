/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.scytl.keystore.constants;

/**
 * Contains constants used in Stores.
 */
public final class ScytlKeyStoreConstants {

    /**
     * This class cannot be instantiated.
     */
    private ScytlKeyStoreConstants() {
    }

    /**
     * Minimum alias length for a Scytl key store entry.
     */
    public static final int MINIMUM_SKS_ENTRY_ALIAS_LENGTH = 1;

    /**
     * Maximum alias length for a Scytl key store entry.
     */
    public static final int MAXIMUM_SKS_ENTRY_ALIAS_LENGTH = 50;

    /**
     * Minimum password length for a Scytl key store entry.
     */
    public static final int MINIMUM_SKS_PASSWORD_LENGTH = 16;

    /**
     * Maximum password length for a Scytl key store entry.
     */
    public static final int MAXIMUM_SKS_PASSWORD_LENGTH = 1000;

    /**
     * Allowed set of Scytl key store entry alias characters (specified as a
     * regular expression).
     */
    public static final String ALLOWED_SKS_ENTRY_ALIAS_CHARACTERS =
        "[a-z0-9_-]+";
}
