/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * 
 *  This package defines the api for basic operations of creating and loading stores. *  
 *  <p>  
 *  <strong>Opening a store to retrieve a key (private or secret):</strong>
 *  <p>  
 *  <pre class="java"> 
 *  ....
 *  ScytlKeyStoreService _storeService
 *  PasswordProtection password
 *  ....
 *  FileInputStream in = new FileInputStream("/keystore.sks");
 *  Key key;
 *  try {
 *      CryptoScytlKeyStore cryptoKeyStore = _storeService.load(in, password);
 *      key = cryptoKeyStore.getKey("myAlias", password.getPassword());
 *  } finally {
 *      IOUtils.closeQuietly(in);
 *  }
 *  </pre>
 *
 *  <strong>Creating and storing a store after setting some data:</strong>
 * 
 *  <pre class="java">
 *  ....
 *  ScytlKeyStoresService _storeService
 *  PasswordProtection password
 *  X509Certificate cert
 *  PrivateKey privateKey
 *  Certificate[] chain
 *  ...
 *  //create a new store
 *  CryptoAPIScytlKeyStore cryptoKeyStore = _storeService.create();
 *  
 *  //Adding a private key with its chain of certificates
 *  cryptoKeyStore.setPrivateKeyEntry(alias1, privateKey,password.getPassword(), chain);
 *  
 *  //addinf a secret key
 *  cryptoKeyStore.setSymmetricKeyEntry(alias2, secretKey,password.getPassword());
 *  
 *  //finally storing it to a file
 *  FileOutputStream outKeyStore = new FileOutputStream("/keystore.sks");
 *  
 *  IOUtils.closeQuietly(outKeyStore);
 *  try {
 *      cryptoKeyStore.store(outKeyStore, password.getPassword());
 *  } finally {
 *  IOUtils.closeQuietly(outKeyStore);
 *  }
 *  </pre>
 */
package com.scytl.cryptolib.api.scytl.keystore;

