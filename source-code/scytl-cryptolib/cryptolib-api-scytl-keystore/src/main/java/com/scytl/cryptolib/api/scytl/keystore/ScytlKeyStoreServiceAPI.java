/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.api.scytl.keystore;

import java.io.InputStream;
import java.security.KeyStore.PasswordProtection;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.scytl.keystore.cryptoapi.CryptoAPIScytlKeyStore;

/**
 * This interface defines public operations that can be done with stores.
 */
public interface ScytlKeyStoreServiceAPI {

    /**
     * Creates an empty Key store ready to fill up with data.
     * <p>
     * In order to physically store the returned key store a further call to
     * {@link CryptoAPIScytlKeyStore#store(java.io.OutputStream, char[])} method
     * should be done.
     * 
     * @return the key store created.
     */
    com.scytl.cryptolib.scytl.keystore.cryptoapi.CryptoAPIScytlKeyStore createKeyStore();

    /**
     * Loads a key store.
     * <p>
     * A password must be given to unlock the store.
     *
     * @param in
     *            the stream from the {@link CryptoAPIScytlKeyStore} will be
     *            read.
     * @param password
     *            the password used to check the integrity of the store. For
     *            security reasons, the password must contain a minimum of 16
     *            characters.
     * @return the key store loaded.
     * @throws GeneralCryptoLibException
     *             if there is a problem reading the store or if there is an I/O
     *             or format problem with the key store data.
     */
    CryptoAPIScytlKeyStore loadKeyStore(InputStream in,
            final PasswordProtection password)
            throws GeneralCryptoLibException;

    /**
     * Loads a key store. The store is expected in JSON format, see
     * {@link CryptoAPIScytlKeyStore#toJSON(char[])}.
     * 
     * @param in
     *            the stream from the {@link CryptoAPIScytlKeyStore} will be
     *            read.
     * @param password
     *            the password used to check the integrity of the store. For
     *            security reasons, the password must contain a minimum of 16
     *            characters.
     * @return the key store loaded.
     * @throws GeneralCryptoLibException
     *             if there is a problem reading the store or if there is an I/O
     *             or format problem with the key store data.
     */
    CryptoAPIScytlKeyStore loadKeyStoreFromJSON(InputStream in,
            PasswordProtection password) throws GeneralCryptoLibException;

    /**
     * Formats the given {@link CryptoAPIScytlKeyStore} to JSON format.
     * 
     * @param in
     *            the stream from the {@link CryptoAPIScytlKeyStore} will be
     *            read.
     * @return the given {@link CryptoAPIScytlKeyStore} in JSON format.
     * @throws GeneralCryptoLibException
     *             if there is a problem reading the store or if there is an I/O
     *             or format problem with the key store data.
     */
    String formatKeyStoreToJSON(InputStream in)
            throws GeneralCryptoLibException;

}
