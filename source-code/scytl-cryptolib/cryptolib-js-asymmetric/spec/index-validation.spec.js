/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var asymmetric = require('../lib/index');
var codec = require('scytl-codec');
var forge = require('node-forge');

var BigInteger = forge.jsbn.BigInteger;

describe('The asymmetric cryptography module should be able to ...', function() {
  var DATA = 'anystring';

  var PUBLIC_KEY_PEM =
      '-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAglypZU45bnf2opnRI+Y51VqouBpvDT33xIB/OtbwKzwVpi+JrjBFtfk33tE9t/dSRs79CK94HRhCWcOiLa2qWPrjeZ9SBiEyScrhIvRZVBF41zBgwQNuRvJCsKmAqlZaFNJDZxEP4repmlBn1CfVFmfrXmOKqwP5F7l9ZtucveRzsfmF1yVPFkW8TMuB3YqMiyymyqHlS8ujCsu5I8tpgPbwuxdMOY94fNhSXrYkY8IuX1g1zdq/Z1jluOaR/UqK4UpnbuJaH/F0VgDNiWh6cTD0DFGEk0b70i5wU4Q3L/S6XZQRvSuADoCbhwBKuFL5pW5n865oLVb5S3wuVdWaGwIDAQAB-----END PUBLIC KEY-----';
  var PRIVATE_KEY_PEM =
      '-----BEGIN RSA PRIVATE KEY-----MIIEowIBAAKCAQEAglypZU45bnf2opnRI+Y51VqouBpvDT33xIB/OtbwKzwVpi+JrjBFtfk33tE9t/dSRs79CK94HRhCWcOiLa2qWPrjeZ9SBiEyScrhIvRZVBF41zBgwQNuRvJCsKmAqlZaFNJDZxEP4repmlBn1CfVFmfrXmOKqwP5F7l9ZtucveRzsfmF1yVPFkW8TMuB3YqMiyymyqHlS8ujCsu5I8tpgPbwuxdMOY94fNhSXrYkY8IuX1g1zdq/Z1jluOaR/UqK4UpnbuJaH/F0VgDNiWh6cTD0DFGEk0b70i5wU4Q3L/S6XZQRvSuADoCbhwBKuFL5pW5n865oLVb5S3wuVdWaGwIDAQABAoIBAC/tn34Wf3kE9BGeGc1oFLVDaqqdVVz5/oEpeR2J7q0GnzMFYUpAhzC7WvY52cYsUPyll1Q9Jx0TUTmteo/uvKWQQFfz4nVMeS+2PoXabolBDzuWlsv/1eiRo0FOYHa/3siu8YcQN9X0DpAkpbfTmT1uoZOHZ3EuucMmOFu7vGn38Grw8bSxpR0uvTtnb8ygC+aB51y38RMyhzQQanrM8FMeAfDAy6IB0Yo7b0c50Cxa6Ax4nqn9LXyGakr5WeAMkgTIOA/GId9SZD4e5eRpq+628pOeR4O9datFltgl6r1+A4ii2VrJsDqeatGtODlX6KRKqwFHoGIa2TjgSZLuorECgYEAxeSZDOOgFsI5mB7RkRzZaQ9znJ15sgdyZiAFZAOUah4hSGdAXNAnZTlrdacduXEu3EfkpuPToX7xZSv5FRYwfBwMwCLeytlGLPjQzWejZGbo4+KqgzWb9fECDYVtDPlJ/+yLih9nt67BHweJKxYydl18rVigdVyy22X86NijSykCgYEAqKPUrXZAo+TJvmTw4tgsibJgvXBYBhmsej8mGNQw+Nyp2gV28sgm61ifIeXKS8teq+MFwGA6cHQedbsCqhMHokdhESZmlbWxhSFLihQcewBxwvrBwbaxI23yXRzwMewznZFL032PpcbqrmwFmcSSEZ3nmbvTH6ShqLW+pzDNp6MCgYBQLzdgxJ7qedqSa/JohTMG4e7rh9d2rpPJE7J7ewPZF8pOpx+qO+Gqn2COdJ+Ts2vUcAETKn9nEaPIZc/wnmQY9dioxbhWo0FPGaaphBPtq9Ez/XUv4zoFppk5V1X/isdUPsmvttf00oeIBiqrXbwmv+yz5JRn2Z7TTXjz9Ev+OQKBgQCUuoCMRzl1EgcXIqEL/0kwW6BUEqufHa9u1Ri9Vw6lvL8T6DPipMEmWK9nzuid9gtVns/ovTVtDgv7GuabplLaPQePf4WDzY11c0rSyS/hDyBFrK+LL5uEOqhAlJAGB2HyOj1clWVF+GvrTpuV5LZKUS/79pmZU7G7QCaX/0Ow7wKBgC/kDH7cmWQnWvvJ5izrx/7PogQVPOLELeUIGLu/hjsSdDKiFCxCUZ948+9NuG+DnpXDWzw//r8mPBRRGGsqFws5Aipp7yjQ3kRDCCzGelPCVhHyfmKqA+8ewXPulKS3/wIyHIvaXmsuAtTfurHtpRyzjKmCBK1Y6WQ3trIXvo7s-----END RSA PRIVATE KEY-----';

  var data_;
  var service_;
  var signer_;
  var sigVerifier_;
  var encrypter_;
  var decrypter_;
  var signature_;
  var encryptedData_;
  var keyArg_;
  var nonObject_;
  var emptyObject_;
  var nonString_;
  var emptyString_;
  var nonUint8Array_;
  var nonBigInteger_;
  var nonPositiveBigInteger_;

  beforeAll(function() {
    data_ = codec.utf8Encode(DATA);
    service_ = asymmetric.newService();
    signer_ = service_.newSigner();
    sigVerifier_ = service_.newSignatureVerifier();
    encrypter_ = service_.newEncrypter();
    decrypter_ = service_.newDecrypter();
    keyArg_ = BigInteger.ONE;
    nonObject_ = 999;
    emptyObject_ = {};
    nonString_ = 999;
    emptyString_ = '';
    nonUint8Array_ = [];
    nonBigInteger_ = asymmetric.newService();
    nonPositiveBigInteger_ = BigInteger.ZERO;
  });

  beforeEach(function() {
    signer_.init(PRIVATE_KEY_PEM);
    sigVerifier_.init(PUBLIC_KEY_PEM);
    encrypter_.init(PUBLIC_KEY_PEM);
    decrypter_.init(PRIVATE_KEY_PEM);
    encryptedData_ = encrypter_.encrypt(data_);
    signature_ = signer_.sign(data_);
  });

  describe('create an asymmetric cryptography service that should be able to ..', function() {
    it('throw an error when being created, using invalid cryptographic policy',
       function() {
         expect(function() {
           Object.create(asymmetric.newService({policy: null}));
         }).toThrow();

         expect(function() {
           Object.create(asymmetric.newService({policy: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(asymmetric.newService({policy: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when being created, using invalid secure random service object',
       function() {
         expect(function() {
           Object.create(asymmetric.newService({secureRandomService: null}));
         }).toThrow();

         expect(function() {
           Object.create(
               asymmetric.newService({secureRandomService: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(
               asymmetric.newService({secureRandomService: emptyObject_}));
         }).toThrow();
       });

    it('throw an error when creating a new RsaPublicKey object, using invalid input data',
       function() {
         expect(function() {
           service_.newRsaPublicKey();
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey(undefined);
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey(null);
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey(nonObject_);
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey(emptyObject_);
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey({n: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey({n: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey({n: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey({n: nonPositiveBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey({e: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey({e: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey({e: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPublicKey(keyArg_, {e: nonPositiveBigInteger_});
         }).toThrow();
       });

    it('throw an error when creating a new RsaPrivateKey object, using an invalid params object, PEM string, modulus, public exponent or private exponent',
       function() {
         expect(function() {
           service_.newRsaPrivateKey();
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey(undefined);
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey(null);
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey(nonObject_);
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey(emptyObject_);
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({pem: emptyString_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({n: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({n: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({n: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({n: nonPositiveBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({e: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({e: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({e: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({e: nonPositiveBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({d: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({d: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({d: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({d: nonPositiveBigInteger_});
         }).toThrow();
       });

    it('throw an error when creating a new RsaPrivateKey object, using an invalid first or second prime',
       function() {
         expect(function() {
           service_.newRsaPrivateKey({p: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({p: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({p: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({p: nonPositiveBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({q: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({q: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({q: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({q: nonPositiveBigInteger_});
         }).toThrow();
       });

    it('throw an error when creating a new RsaPrivateKey object, using an invalid first exponent, second exponent or coefficient',
       function() {
         expect(function() {
           service_.newRsaPrivateKey({dP: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({dP: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({dP: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({dP: nonPositiveBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({dQ: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({dQ: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({dQ: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({dQ: nonPositiveBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({qInv: undefined});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({qInv: null});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({qInv: nonBigInteger_});
         }).toThrow();

         expect(function() {
           service_.newRsaPrivateKey({qInv: nonPositiveBigInteger_});
         }).toThrow();
       });

    it('throw an error when creating a new KeyPair object, using invalid input data',
       function() {
         expect(function() {
           service_.newKeyPair();
         }).toThrow();

         expect(function() {
           service_.newKeyPair(undefined, PRIVATE_KEY_PEM);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(null, PRIVATE_KEY_PEM);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(nonString_, PRIVATE_KEY_PEM);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(emptyString_, PRIVATE_KEY_PEM);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(PUBLIC_KEY_PEM);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(PUBLIC_KEY_PEM, undefined);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(PUBLIC_KEY_PEM, null);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(PUBLIC_KEY_PEM, nonString_);
         }).toThrow();

         expect(function() {
           service_.newKeyPair(PUBLIC_KEY_PEM, emptyString_);
         }).toThrow();
       });

    describe('create a signer that should be able to ...', function() {
      it('throw an error when being initialized, using invalid input data',
         function() {
           expect(function() {
             signer_.init();
           }).toThrow();

           expect(function() {
             signer_.init(undefined);
           }).toThrow();

           expect(function() {
             signer_.init(null);
           }).toThrow();

           expect(function() {
             signer_.init(nonString_);
           }).toThrow();

           expect(function() {
             signer_.init(emptyString_);
           }).toThrow();
         });

      it('throw an error when being updated, using invalid input data',
         function() {
           expect(function() {
             signer_.update();
           }).toThrow();

           expect(function() {
             signer_.update(undefined);
           }).toThrow();

           expect(function() {
             signer_.update(null);
           }).toThrow();

           expect(function() {
             signer_.update(nonUint8Array_);
           }).toThrow();
         });

      it('throw an error when signing, using invalid input data', function() {
        expect(function() {
          signer_.sign(null);
        }).toThrow();

        expect(function() {
          signer_.sign(nonUint8Array_);
        }).toThrow();
      });
    });

    describe('create a signature verifier that should be able to ...', function() {
      it('throw an error when being initialized, using invalid input data',
         function() {
           expect(function() {
             sigVerifier_.init();
           }).toThrow();

           expect(function() {
             sigVerifier_.init(undefined);
           }).toThrow();

           expect(function() {
             sigVerifier_.init(null);
           }).toThrow();

           expect(function() {
             sigVerifier_.init(nonString_);
           }).toThrow();

           expect(function() {
             sigVerifier_.init(emptyString_);
           }).toThrow();
         });

      it('throw an error when being updated, using invalid input data',
         function() {
           expect(function() {
             sigVerifier_.update();
           }).toThrow();

           expect(function() {
             sigVerifier_.update(undefined);
           }).toThrow();

           expect(function() {
             sigVerifier_.update(null);
           }).toThrow();

           expect(function() {
             sigVerifier_.update(nonUint8Array_);
           }).toThrow();
         });

      it('throw an error when verifying a signature, using invalid signature',
         function() {
           expect(function() {
             sigVerifier_.verify(undefined, data_);
           }).toThrow();

           expect(function() {
             sigVerifier_.verify(null, data_);
           }).toThrow();

           expect(function() {
             sigVerifier_.verify(nonUint8Array_, data_);
           }).toThrow();
         });

      it('throw an error when verifying a signature, using invalid data that was signed',
         function() {
           expect(function() {
             sigVerifier_.verify(signature_, null);
           }).toThrow();

           expect(function() {
             sigVerifier_.verify(signature_, nonUint8Array_);
           }).toThrow();
         });
    });

    describe('create an encrypter that should be able to ...', function() {
      it('throw an error when being initialized, using invalid input data',
         function() {
           expect(function() {
             encrypter_.init();
           }).toThrow();

           expect(function() {
             encrypter_.init(undefined);
           }).toThrow();

           expect(function() {
             encrypter_.init(null);
           }).toThrow();

           expect(function() {
             encrypter_.init(nonString_);
           }).toThrow();

           expect(function() {
             encrypter_.init(emptyString_);
           }).toThrow();
         });

      it('throw an error when encrypting, using invalid input data',
         function() {
           expect(function() {
             encrypter_.encrypt();
           }).toThrow();

           expect(function() {
             encrypter_.encrypt(undefined);
           }).toThrow();

           expect(function() {
             encrypter_.encrypt(null);
           }).toThrow();

           expect(function() {
             encrypter_.encrypt(nonUint8Array_);
           }).toThrow();
         });
    });

    describe('create a decrypter that should be able to ...', function() {
      it('throw an error when initializing, using invalid input data',
         function() {
           expect(function() {
             decrypter_.init();
           }).toThrow();

           expect(function() {
             decrypter_.init(undefined);
           }).toThrow();

           expect(function() {
             decrypter_.init(null);
           }).toThrow();

           expect(function() {
             decrypter_.init(nonString_);
           }).toThrow();

           expect(function() {
             decrypter_.init(emptyString_);
           }).toThrow();
         });

      it('throw an error when decrypting, using invalid input data',
         function() {
           expect(function() {
             decrypter_.decrypt();
           }).toThrow();

           expect(function() {
             decrypter_.decrypt(undefined);
           }).toThrow();

           expect(function() {
             decrypter_.decrypt(null);
           }).toThrow();

           expect(function() {
             decrypter_.decrypt(nonUint8Array_);
           }).toThrow();
         });
    });
  });
});
