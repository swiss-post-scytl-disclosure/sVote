/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

module.exports = KeyPair;

/**
 * @class KeyPair
 * @classdesc Encapsulates a key pair. To instantiate this object, use the
 *            method {@link AsymmetricCryptographyService.newKeyPair}.
 * @property {string} publicKey The public key, in PEM format.
 * @property {string} privateKey The private key, in PEM format.
 */
function KeyPair(publicKey, privateKey) {
  return Object.freeze({publicKey: publicKey, privateKey: privateKey});
}
