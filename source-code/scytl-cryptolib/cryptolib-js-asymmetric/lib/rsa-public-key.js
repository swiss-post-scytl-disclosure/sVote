/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var constants = require('./constants');
var forge = require('node-forge');

module.exports = RsaPublicKey;

/**
 * @class RsaPublicKey
 * @classdesc Encapsulates an RSA public key. To instantiate this object, use
 *            the method {@link AsymmetricCryptographyService.newRsaPublicKey}.
 * @property {number} n The modulus.
 * @property {number} e The public exponent.
 */
function RsaPublicKey(params) {
  this.n = params.n;
  this.e = params.e;

  return Object.freeze(this);
}

RsaPublicKey.prototype = {
  /**
   * Serializes this key into its PEM string representation.
   *
   * @function toPem
   * @memberof RsaPublicKey
   * @returns {string} The PEM string representation of this key.
   */
  toPem: function() {
    var forgePublicKey = forge.pki.rsa.setPublicKey(this.n, this.e);

    return forge.pki.publicKeyToPem(forgePublicKey, constants.PEM_LINE_LENGTH);
  }
};
