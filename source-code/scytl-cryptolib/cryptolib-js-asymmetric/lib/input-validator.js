/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var forge = require('node-forge');

var BigInteger = forge.jsbn.BigInteger;

/**
 * Input data validation utility for this module. Only intended for internal
 * use.
 */
module.exports = {
  /**
   * Checks if a value is defined.
   *
   * @function checkIsDefined
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is undefined.
   */
  checkIsDefined: function(value, label) {
    if (typeof value === 'undefined') {
      throw new TypeError(label + ' is undefined.');
    }
  },

  /**
   * Checks if a value is not null.
   *
   * @function checkIsNotNull
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is null.
   */
  checkIsNotNull: function(value, label) {
    if (value === null) {
      throw new TypeError(label + ' is null.');
    }
  },

  /**
   * Checks if a value is defined and not null.
   *
   * @function checkIsDefinedAndNotNull
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not defined or it is null.
   */
  checkIsDefinedAndNotNull: function(value, label) {
    this.checkIsDefined(value, label);
    this.checkIsNotNull(value, label);
  },

  /**
   * Checks if a value is of an expected type.
   *
   * @function checkIsType
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            type The expected type of the value.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not of the expected type.
   */
  checkIsType: function(value, type, label) {
    var typeFound = typeof value;
    if (typeFound !== type) {
      throw new TypeError(
          'Expected ' + label + ' to have type \'' + type + '\' ; Found: \'' +
          typeFound + '\'');
    }
  },

  /**
   * Checks if a value is an instance of an Object.
   *
   * @function checkIsInstanceOf
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {Object}
   *            obj The Object to check against.
   * @param {string}
   *            objName The Object name, for error handling purposes.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the object is undefined, null, or it is not an instance of
   * the Object.
   */
  checkIsInstanceOf: function(value, obj, objName, label) {
    this.checkIsDefinedAndNotNull(value, label);

    if (!(value instanceof obj)) {
      throw new TypeError(label + ' is not an instance of Object ' + objName);
    }
  },

  /**
   * Checks if a value is an object.
   *
   * @function checkIsObject
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not an object.
   */
  checkIsObject: function(value, label) {
    if (typeof value !== 'object') {
      throw new TypeError(label + ' is not an object.');
    }
  },

  /**
   * Checks if a value is an object and has properties.
   *
   * @function checkIsObjectWithProperties
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not an object or it has no properties.
   */
  checkIsObjectWithProperties: function(value, label) {
    this.checkIsDefinedAndNotNull(value, label);

    this.checkIsObject(value, label);

    if (!Object.getOwnPropertyNames(value).length) {
      throw new TypeError(label + ' does not have any properties.');
    }
  },

  /**
   * Checks if a value is a non-empty string.
   *
   * @function checkIsNonEmptyString
   * @private
   * @param {string}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not a non-empty string.
   */
  checkIsNonEmptyString: function(value, label) {
    this.checkIsType(value, 'string', label);

    if (value.length === 0) {
      throw new TypeError(label + ' is empty.');
    }
  },

  /**
   * Checks if a value is a BigInteger object.
   *
   * @function checkIsBigInteger
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not a BigInteger object.
   */
  checkIsBigInteger: function(value, label) {
    this.checkIsDefinedAndNotNull(value, label);

    if (typeof value.abs === 'undefined') {
      throw new TypeError(label + ' is not a BigInteger object.');
    }
  },

  /**
   * Checks if a value is a positive BigInteger object.
   *
   * @function checkIsPositiveBigInteger
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not a positive BigInteger object.
   */
  checkIsPositiveBigInteger: function(value, label) {
    this.checkIsBigInteger(value, label);

    if (value.compareTo(BigInteger.ONE) < 0) {
      throw new TypeError(label + ' must not be less than 1; Found: ' + value);
    }
  }
};
