/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var AsymmetricCryptographyService = require('./service');
var validator = require('./input-validator');

module.exports = {
  /**
   * Creates a new AsymmetricCryptographyService object, which encapsulates an
   * asymmetric cryptography service.
   *
   * @function newService
   * @global
   * @param {Object}
   *            [options] An object containing optional arguments.
   * @param {Policy}
   *            [options.policy=Default policy] The cryptographic policy to
   *            use.
   * @param {SecureRandomService}
   *            [options.secureRandomService=Created internally] The secure
   *            random service to use.
   * @returns {AsymmetricCryptographyService} The new
   *          AsymmetricCryptographyService object.
   * @throws {Error}
   *             If the input data validation fails.
   * @example <caption> How to use a cryptographic policy that sets the key
   *          length of the asymmetric cipher to 3072 bits</caption>
   *
   * var cryptoPolicy = require('scytl-cryptopolicy');
   * var asymmetric = require('scytl-asymmetric');
   *
   * var myPolicy = cryptoPolicy.newInstance();
   *
   * myPolicy.asymmetric.keyPair.encryption.keyLength =
   * cryptoPolicy.options.asymmetric.keyPair.encryption.keyLength.KL_3072;
   *
   * var asymmetricService = asymmetric.newService({policy: myPolicy});
   */
  newService: function(options) {
    checkData(options);

    return new AsymmetricCryptographyService(options);
  }
};

function checkData(options) {
  options = options || {};

  if (typeof options.policy !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.policy,
        'Cryptographic policy provided to asymmetric cryptography service');
  }

  if (typeof options.secureRandomService !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.secureRandomService,
        'Secure random service object provided to asymmetric cryptography service');
  }
}
