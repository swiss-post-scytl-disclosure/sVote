# PBKDF Module

This module defines the password based key derivation function (PBKDF) API for Scytl's JavaScript cryptographic library. The starting point for using this module is the instantiation of a PBKDF service.

## How to view, build and test the source code of this module

Start by git cloning the project `comm/scytl-cryptolib` from `Stash`.

The source code of this module will be found in the directory `scytl-cryptolib/cryptolib-js-pbkdf/lib`.

To build this module, change to the directory `scytl-cryptolib/cryptolib-js-pbkdf` and do the following:

```java
npm install
--or--
mvn clean install -DskipTests
```
The unit tests of this module will be found in the directory `scytl-cryptolib/cryptolib-js-pbkdf/spec`. To run the tests, change to the directory `scytl-cryptolib/cryptolib-js-pbkdf` and do the following:

```javascript
karma test
--or--
mvn test
```
To only run a chosen test suite, add an `f` in front of the corresponding `describe` statement. For example:

```javascript
fdescribe('create a PBKDF deriver that should be able to', function()
...
```
To only run a chosen test, add an `f` in front of the corresponding `it` statement. For example:

```javascript
fit('derive a key of the expected length, when using the default cryptographic policy.',
...
```
**Note:** To build and test the entire `scytl-cryptolib` project, change to the directory `scytl-cryptolib` and do the following:

```javascript
mvn clean install
```
## How to generate the JSDoc for this module

To generate the JSDoc for this module, change to the directory `scytl-cryptolib/cryptolib-js-pbkdf`, build the module (if not already done) and do the following:

```javascript
node_modules/.bin/jsdoc lib
```
This will generate a file called `out`. Double click on the file `out/index.html` to view the JSDoc.

## How to `npm` install this module

To `npm` install this module in standalone mode, do the following:

```javascript
npm install --registry https://nexus.scytl.net/content/groups/public-npm/ scytl-pbkdf
```
To `npm` install this module as a dependency of your own module, do the following:

1. Add the following lines to the `dependencies` section of your module's `package.json` file.
    ```javascript
    "scytl-codec": "^2.1.0",
    "scytl-cryptopolicy": "^2.1.0",
    "scytl-pbkdf": "^2.1.0",
    ```
2. Make sure that the `.npmrc` file of your module contains the following line.
    ```javascript
    registry=https://nexus.scytl.net/content/groups/public-npm/
    ```
3. Install all dependencies.
    ```javascript
    npm install
    ```
## How to instantiate a PBKDF service

The following example shows the default way to create a new instance of a PBKDF service.

```java
var pbkdf = require('scytl-pbkdf');

var pbkdfService = pbkdf.newService();
```
The PBKDF service accepts an optional input argument that can be used to override the default cryptographic policy used
for PBKDF operations. For example, to set the length of the derived key to 16 bytes, one could do the following:

```java
var cryptoPolicy = require('scytl-cryptopolicy');
var pbkdf = require('scytl-pbkdf');

var myPolicy = cryptoPolicy.newInstance();

myPolicy.primitives.keyDerivation.pbkdf.keyLengthBytes =
  cryptoPolicy.options.primitives.keyDerivation.pbkdf.keyLengthBytes.KL_16;

var pbkdfService = pbkdf.newService({policy: myPolicy});
```
**Note** It is desirable to use the property `CryptographicPolicy.options` to override the default policy, as shown above, in order to ensure that one is using an option that is allowed by the cryptographic policy.

## How to instantiate a PBKDF deriver

The following example shows how to create a new PBKDF deriver object.

```java
var deriver = pbkdfService.newDeriver();
```

## How to derive a PBKDF key

The following examples show how to derive a PBKDF key from a given password and salt. The password is provided as a `string` and the salt can be provided as a `string` or as a byte array, wrapped in a `Uint8Array` object). The output will be in the form of a byte array, wrapped in a `Uint8Array` object.

```java
var codec = require('scytl-codec');
...

var derivedKey = deriver.derive('myPassword', 'mySalt');
var derivedKey = deriver.derive('myPassword', new Uint8Array([0x01, 0x02, 0x03]));
```
