/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var pbkdf = require('../lib/index');
var codec = require('scytl-codec');

describe('The PBKDF module should be able to ...', function() {
  var PASSWORD = 'Test';
  var SALT = 'TestTestTestTestTestTestTestTest';

  var salt_;
  var deriver_;
  var nonObject_ = 999;
  var emptyObject_ = {};
  var nonString_ = 999;

  beforeAll(function() {
    salt_ = codec.utf8Encode(SALT);
    deriver_ = pbkdf.newService().newDeriver();
  });

  describe('create a PBKDF service that should be able to ..', function() {
    it('throw an error when being created, using an invalid cryptographic policy',
       function() {
         expect(function() {
           Object.create(pbkdf.newService({policy: null}));
         }).toThrow();

         expect(function() {
           Object.create(pbkdf.newService({policy: nonObject_}));
         }).toThrow();

         expect(function() {
           Object.create(pbkdf.newService({policy: emptyObject_}));
         }).toThrow();
       });

    describe('create a PBKDF deriver that should be able to', function() {
      it('throw an error when deriving a key, using an invalid password',
         function() {
           expect(function() {
             deriver_.derive(undefined, salt_);
           }).toThrow();

           expect(function() {
             deriver_.derive(null, salt_);
           }).toThrow();

           expect(function() {
             deriver_.derive(nonString_, salt_);
           }).toThrow();
         });

      it('throw an error when deriving a key, using invalid salt', function() {
        expect(function() {
          deriver_.derive(PASSWORD);
        }).toThrow();

        expect(function() {
          deriver_.derive(PASSWORD, undefined);
        }).toThrow();

        expect(function() {
          deriver_.derive(PASSWORD, null);
        }).toThrow();

        expect(function() {
          deriver_.derive(PASSWORD, '');
        }).toThrow();

        expect(function() {
          deriver_.derive(PASSWORD, nonString_);
        }).toThrow();
      });
    });
  });
});
