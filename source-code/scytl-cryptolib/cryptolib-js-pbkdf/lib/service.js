/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var cryptoPolicy = require('scytl-cryptopolicy');
var PbkdfDeriver = require('./deriver');

module.exports = PbkdfService;

/**
 * @class PbkdfService
 * @classdesc The PBKDF service API. To instantiate this object, use the method
 * {@link newService}.
 * @hideconstructor
 * @param {Object}
 *            [options] An object containing optional arguments.
 * @param {Policy}
 *            [options.policy=Default policy] The cryptographic policy to use.
 */
function PbkdfService(options) {
  options = options || {};

  var policy;
  if (options.policy) {
    policy = options.policy;
  } else {
    policy = cryptoPolicy.newInstance();
  }

  /**
   * Creates a new PbkdfDeriver object for deriving keys using a PBKDF.
   *
   * @function newDeriver
   * @memberof PbkdfService
   * @returns {PbkdfDeriver} The new PbkdfDeriver object.
   */
  this.newDeriver = function() {
    return new PbkdfDeriver(policy);
  };
}
