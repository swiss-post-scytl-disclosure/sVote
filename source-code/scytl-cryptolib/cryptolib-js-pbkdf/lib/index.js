/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var PbkdfService = require('./service');
var validator = require('./input-validator');

module.exports = {
  /**
   * Creates a new PbkdfService object, which encapsulates a PBKDF service.
   *
   * @function newService
   * @global
   * @param {Object}
   *            [options] An object containing optional arguments.
   * @param {Policy}
   *            [options.policy=Default policy] The cryptographic policy to
   *            use.
   * @returns {PbkdfService} The new PbkdfService object.
   * @throws {Error}
   *             If the input data validation fails.
   * @example <caption> How to use a cryptographic policy that sets the PBKDF
   *          derivation key length to 16 bytes</code></caption>
   *
   * var cryptoPolicy = require('scytl-cryptopolicy');
   * var pbkdf = require('scytl-pbkdf');
   *
   * var myPolicy = cryptoPolicy.newInstance();
   *
   * myPolicy.primitives.keyDerivation.pbkdf.keyLengthBytes =
   * cryptoPolicy.options.primitives.keyDerivation.pbkdf.keyLengthBytes.KL_16;
   *
   * var pbkdfService = pbkdf.newService({policy: myPolicy});
   */
  newService: function(options) {
    checkData(options);

    return new PbkdfService(options);
  }
};

function checkData(options) {
  options = options || {};

  if (typeof options.policy !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.policy, 'Cryptographic policy provided to PBKDF service');
  }
}
