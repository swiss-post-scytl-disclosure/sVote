/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

import mockit.Deencapsulation;

public class KeyPairPolicyFromPropertiesTest {

    private static KeyPairPolicyFromProperties _keyPairPolicyFromProperties;

    private static OperatingSystem _os;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _keyPairPolicyFromProperties =
            new KeyPairPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);

        _os = OperatingSystem.current();
    }

    @After
    public void revertSystemHelper() {
        Deencapsulation.setField(OperatingSystem.class, "current",
            _os);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateKeyPairPolicyPassingAnIncorrectPath()
            throws GeneralCryptoLibException {

        new KeyPairPolicyFromProperties("somePath");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateKeyPairPolicyPassingInvalidLabels() {
        new KeyPairPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateKeyPairPolicyPassingBlankLabels() {
        new KeyPairPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Test
    public void whenGetSigningKeyAlgorithmAndSpecThenExpectedValue() {

        String errorMsg =
            "The returned signing key algorithm and spec type is not the expected value";
        assertEquals(errorMsg,
            ConfigSigningKeyPairAlgorithmAndSpec.RSA_2048_F4_SUN_RSA_SIGN,
            _keyPairPolicyFromProperties
                .getSigningKeyPairAlgorithmAndSpec());
    }

    @Test
    public void whenGetEncryptionKeyAlgorithmAndSpecThenExpectedValue() {

        String errorMsg =
            "The returned encryption key algorithm and spec type is not the expected value";
        assertEquals(errorMsg,
            ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_2048_F4_SUN_RSA_SIGN,
            _keyPairPolicyFromProperties
                .getEncryptingKeyPairAlgorithmAndSpec());
    }

    @Test
    public void whenGetSecureRandomThenExpectedValue() {

        ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmAndProvider;
        if (OperatingSystem.WINDOWS.isCurrent()) {
            _secureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI;
        } else {
            _secureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN;
        }

        String errorMsg =
            "The returned secure random algorithm and provider type is not the expected value";
        assertEquals(errorMsg, _secureRandomAlgorithmAndProvider,
            _keyPairPolicyFromProperties
                .getSecureRandomAlgorithmAndProvider());
    }

}
