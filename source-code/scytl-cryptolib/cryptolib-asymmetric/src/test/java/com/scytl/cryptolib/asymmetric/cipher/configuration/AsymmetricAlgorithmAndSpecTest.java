/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.cipher.configuration;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;

public class AsymmetricAlgorithmAndSpecTest {

    @Test
    public void givenAlgorithmModePaddingWhenSpecIsRsaWithRsaKemAndKdf1AndSha256AndBc() {

        assertAlgorithmModePadding(
            "RSA/RSA-KEMWITHKDF1ANDSHA-256/NOPADDING",
            ConfigAsymmetricCipherAlgorithmAndSpec.RSAwithRSAKEMANDKDF1ANDSHA256_BC);
    }

    @Test
    public void givenAlgorithmModePaddingWhenSpecIsRsaWithRsaKemAndKdf2AndSha256AndBc() {

        assertAlgorithmModePadding(
            "RSA/RSA-KEMWITHKDF2ANDSHA-256/NOPADDING",
            ConfigAsymmetricCipherAlgorithmAndSpec.RSAwithRSAKEMANDKDF2ANDSHA256_BC);
    }

    @Test
    public void givenProviderWhenSpecIsRsaWithRsaKemAndKdf1AndSha256AndBc() {

        assertProvider(
            Provider.BOUNCY_CASTLE,
            ConfigAsymmetricCipherAlgorithmAndSpec.RSAwithRSAKEMANDKDF1ANDSHA256_BC);
    }

    @Test
    public void givenProviderWhenSpecIsRsaWithRsaKemAndKdf2AndSha256AndBc() {

        assertProvider(
            Provider.BOUNCY_CASTLE,
            ConfigAsymmetricCipherAlgorithmAndSpec.RSAwithRSAKEMANDKDF2ANDSHA256_BC);
    }

    private void assertAlgorithmModePadding(
            final String expectedAlgorithm,
            final ConfigAsymmetricCipherAlgorithmAndSpec cipherAlgorithmAndSpec) {

        Assert.assertEquals(
            "Algorithm/mode/padding was not the expected value.",
            expectedAlgorithm,
            cipherAlgorithmAndSpec.getAlgorithmModePadding());
    }

    private void assertProvider(
            final Provider expectedProvider,
            final ConfigAsymmetricCipherAlgorithmAndSpec cipherAlgorithmAndSpec) {

        Assert.assertEquals("Provider was not the expected value.",
            expectedProvider, cipherAlgorithmAndSpec.getProvider());
    }

}
