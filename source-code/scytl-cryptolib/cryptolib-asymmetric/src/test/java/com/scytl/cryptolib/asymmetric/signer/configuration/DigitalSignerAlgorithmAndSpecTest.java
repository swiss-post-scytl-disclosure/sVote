/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.configuration;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;

public class DigitalSignerAlgorithmAndSpecTest {

    private static final int PADDING_SALT_BIT_LENGTH = 32;

    private static final int PADDING_TRAILER_FIELD = 1;

    @Test
    public void givenAlgorithmWhenSpecIsSha256WithRsaAndPssAndBc() {

        Assert
            .assertEquals(
                "Algorithm/padding was not the expected value",
                "SHA256withRSA/PSS",
                ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC
                    .getAlgorithmAndPadding());
    }

    @Test
    public void givenPaddingMessageDigestAlgorithmWhenSpecIsSha256WithRsaAndPssAndBc() {

        Assert
            .assertEquals(
                "Padding message digest algorithm was not the expected value",
                "SHA-256",
                ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC
                    .getPaddingMessageDigestAlgorithm());
    }

    @Test
    public void givenPaddingMaskingFunctionAlgorithmWhenSpecIsSha256WithRsaAndPssAndBc() {

        Assert
            .assertEquals(
                "Padding masking generation function algorithm was not the expected value",
                "MGF1",
                ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC
                    .getPaddingInfo()
                    .getPaddingMaskingGenerationFunctionAlgorithm());
    }

    @Test
    public void givenPaddingMaskingFunctionMessageDigestAlgorithmWhenSpecIsSha256WithRsaAndPssAndBc() {

        Assert
            .assertEquals(
                "Padding masking generation function message digest algorithm was not the expected value",
                "SHA-256",
                ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC
                    .getPaddingInfo()
                    .getPaddingMaskingGenerationFunctionMessageDigestAlgorithm());
    }

    @Test
    public void givenPaddingSaltLengthWhenSpecIsSha256WithRsaAndPssAndBc() {

        Assert
            .assertEquals(
                "Padding salt bit length was not the expected value",
                PADDING_SALT_BIT_LENGTH,
                ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC
                    .getPaddingInfo()
                    .getPaddingSaltBitLength());
    }

    @Test
    public void givenPaddingTrailerFieldWhenSpecIsSha256WithRsaAndPssAndBc() {

        Assert
            .assertEquals(
                "Padding trailer field was not the expected value",
                PADDING_TRAILER_FIELD,
                ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC
                    .getPaddingInfo()
                    .getPaddingTrailerField());
    }

    @Test
    public void givenProviderWhenSpecIsSha256WithRsaAndPssAndBc() {

        Assert
            .assertEquals(
                "Provider was not the expected value",
                Provider.BOUNCY_CASTLE,
                ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC
                    .getProvider());
    }

}
