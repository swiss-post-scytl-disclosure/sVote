/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.configuration;

import java.security.spec.RSAKeyGenParameterSpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests of ConfigEncryptionKeyPairAlgorithmAndSpec.
 */
public class ConfigEncryptionKeyPairAlgorithmAndSpecTest {

    @Test
    public void givenRSA2048SunRsaSignThenExpectedValues() {

        assertRSAEquals(
            "RSA",
            new RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4),
            "SunRsaSign",
            ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_2048_F4_SUN_RSA_SIGN);
    }

    @Test
    public void givenRSA3072SunRsaSignThenExpectedValues() {

        assertRSAEquals(
            "RSA",
            new RSAKeyGenParameterSpec(3072, RSAKeyGenParameterSpec.F4),
            "SunRsaSign",
            ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_3072_F4_SUN_RSA_SIGN);
    }

    @Test
    public void givenRSA4096SunRsaSignThenExpectedValues() {

        assertRSAEquals(
            "RSA",
            new RSAKeyGenParameterSpec(4096, RSAKeyGenParameterSpec.F4),
            "SunRsaSign",
            ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_4096_F4_SUN_RSA_SIGN);
    }

    @Test
    public void givenRSA2048BCThenExpectedValues() {

        assertRSAEquals("RSA", new RSAKeyGenParameterSpec(2048,
            RSAKeyGenParameterSpec.F4), BouncyCastleProvider.PROVIDER_NAME,
            ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_2048_F4_BC);
    }

    @Test
    public void givenRSA3072BCThenExpectedValues() {

        assertRSAEquals("RSA", new RSAKeyGenParameterSpec(3072,
            RSAKeyGenParameterSpec.F4), BouncyCastleProvider.PROVIDER_NAME,
            ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_3072_F4_BC);
    }

    @Test
    public void givenRSA4096BCThenExpectedValues() {

        assertRSAEquals("RSA", new RSAKeyGenParameterSpec(4096,
            RSAKeyGenParameterSpec.F4), BouncyCastleProvider.PROVIDER_NAME,
            ConfigEncryptionKeyPairAlgorithmAndSpec.RSA_4096_F4_BC);
    }

    private void assertRSAEquals(
            final String expectedAlgorithm,
            final RSAKeyGenParameterSpec expectedSpec,
            final String expectedProvider,
            final ConfigEncryptionKeyPairAlgorithmAndSpec keyPairAlgorithmAndSpec) {

        String errorMsg =
            "Encryption algorithm was not the expected value";
        Assert.assertTrue(errorMsg, expectedAlgorithm
            .equals(keyPairAlgorithmAndSpec.getAlgorithm()));

        errorMsg = "Encryption key size";
        Assert
            .assertTrue(
                errorMsg,
                expectedSpec.getKeysize() == ((RSAKeyGenParameterSpec) keyPairAlgorithmAndSpec
                    .getSpec()).getKeysize());

        errorMsg = "Encryption spec public exponent";
        Assert.assertTrue(
            errorMsg,
            expectedSpec.getPublicExponent().compareTo(
                ((RSAKeyGenParameterSpec) keyPairAlgorithmAndSpec
                    .getSpec()).getPublicExponent()) == 0);

        errorMsg = "Provider was not the expected value";
        Assert.assertTrue(errorMsg, expectedProvider
            .equals(keyPairAlgorithmAndSpec.getProvider()
                .getProviderName()));
    }
}
