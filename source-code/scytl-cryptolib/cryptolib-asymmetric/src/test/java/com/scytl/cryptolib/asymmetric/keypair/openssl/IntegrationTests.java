/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;

/**
 * Integration tests.
 */
public class IntegrationTests {
    private IntegrationTests() {
    }

    /**
     * Main method.
     *
     * @param args
     */
    public static void main(final String[] args) {
        try {
            OpenSSLKeyPairGenerator generator =
                OpenSSLKeyPairGenerator.getInstance("RSA");
            AlgorithmParameterSpec spec = new RSAKeyGenParameterSpec(2048,
                RSAKeyGenParameterSpec.F4);
            generator.initialize(spec);
            KeyPair pair = generator.genKeyPair();
            System.err.println(pair.getPrivate());
            System.err.println(pair.getPublic());
        } catch (InvalidAlgorithmParameterException
                | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
