/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.xml.signer.configuration;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Transform;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.signer.configuration.DigitalSignerPolicyFromProperties;
import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.commons.constants.Constants;

public class XmlDigitalSignerFromPropertiesTest {

    private static String INVALID_PATH = "Invalid path";

    private static XmlDigitalSignerPolicyFromProperties _xmlSignerPolicyFromProperties;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _xmlSignerPolicyFromProperties =
            new XmlDigitalSignerPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @Test
    public void givenPolicyWhenGetXmlDigitalSignerConfigThenExpectedValues() {

        ConfigXmlDigitalSignerAlgorithmAndSpec config =
            _xmlSignerPolicyFromProperties
                .getXmlDigitalSignerAlgorithmAndSpec();

        boolean signatureAlgorithmCorrect =
            config.getDigitalSignatureAlgorithm().equals(
                "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        boolean messageDigestAlgorithmCorrect =
            config.getMessageDigestAlgorithm().equals(DigestMethod.SHA256);
        boolean transformMethodCorrect =
            config.getTransformMethod().equals(Transform.ENVELOPED);
        boolean canonicalizationMethodCorrect =
            config.getCanonicalizationMethod().equals(
                CanonicalizationMethod.EXCLUSIVE);
        boolean mechanismTypeCorrect =
            config.getMechanismType().equals("DOM");
        boolean providerCorrect =
            config.getProvider().equals(Provider.XML_DSIG);

        assert (signatureAlgorithmCorrect && messageDigestAlgorithmCorrect
            && transformMethodCorrect && canonicalizationMethodCorrect
            && mechanismTypeCorrect && providerCorrect);
    }

    @Test(expected = CryptoLibException.class)
    public void whenPolicyFromIncorrectPathThenException()
            throws GeneralCryptoLibException {

        new DigitalSignerPolicyFromProperties(INVALID_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyBlankLabels() {

        new DigitalSignerPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyInvalidLabels()
            throws GeneralCryptoLibException {

        new DigitalSignerPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

}
