/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.xml.signer.configuration;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Transform;

import org.junit.Assert;
import org.junit.Test;

public class XmlDigitalSignerAlgorithmAndSpecTest {

    @Test
    public void givenSignatureAlgorithmWhenSpecIsSha256WithRsaSha256EnvelopedExclusiveDom() {

        Assert
            .assertEquals(
                "Digital signature algorithm was not the expected value",
                "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
                ConfigXmlDigitalSignerAlgorithmAndSpec.SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG
                    .getDigitalSignatureAlgorithm());
    }

    @Test
    public void givenDigestAlgorithmWhenSpecIsSha256WithRsaSha256EnvelopedExclusiveDom() {

        Assert
            .assertEquals(
                "Message digest algorithm was not the expected value",
                DigestMethod.SHA256,
                ConfigXmlDigitalSignerAlgorithmAndSpec.SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG
                    .getMessageDigestAlgorithm());
    }

    @Test
    public void givenTransformMethodWhenSpecIsSha256WithRsaSha256EnvelopedExclusiveDom() {

        Assert
            .assertEquals(
                "Transform method was not the expected value",
                Transform.ENVELOPED,
                ConfigXmlDigitalSignerAlgorithmAndSpec.SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG
                    .getTransformMethod());
    }

    @Test
    public void givenCanonicalizationMethodWhenSpecIsSha256WithRsaSha256EnvelopedExclusiveDom() {

        Assert
            .assertEquals(
                "Canonicalization method was not the expected value",
                CanonicalizationMethod.EXCLUSIVE,
                ConfigXmlDigitalSignerAlgorithmAndSpec.SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG
                    .getCanonicalizationMethod());
    }

    @Test
    public void givenMechanismTypeWhenSpecIsSha256WithRsaSha256EnvelopedExclusiveDom() {

        Assert
            .assertEquals(
                "Mechanism type was not the expected value",
                "DOM",
                ConfigXmlDigitalSignerAlgorithmAndSpec.SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG
                    .getMechanismType());
    }

}
