/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.cipher.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;

public class AsymmetricCipherFromPropertiesTest {

    private static String INVALID_PATH = "Invalid path";

    private static AsymmetricCipherPolicyFromProperties _cipherPolicyFromProperties;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _cipherPolicyFromProperties =
            new AsymmetricCipherPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenPolicyFromIncorrectPathThenException()
            throws GeneralCryptoLibException {

        new AsymmetricCipherPolicyFromProperties(INVALID_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyInvalidLabels()
            throws GeneralCryptoLibException {

        new AsymmetricCipherPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyBlankLabels()
            throws GeneralCryptoLibException {

        new AsymmetricCipherPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Test
    public void givenPolicyWhenGetSymmetricCipherConfigThenExpectedValues() {

        ConfigAsymmetricCipherAlgorithmAndSpec config =
            _cipherPolicyFromProperties
                .getAsymmetricCipherAlgorithmAndSpec();

        assertEquals("RSA/RSA-KEMWITHKDF1ANDSHA-256/NOPADDING",
            config.getAlgorithmModePadding());
    }
}
