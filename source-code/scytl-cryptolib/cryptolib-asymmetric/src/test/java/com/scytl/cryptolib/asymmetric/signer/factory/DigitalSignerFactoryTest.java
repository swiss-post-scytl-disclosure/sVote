/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.factory;

import static mockit.Deencapsulation.getField;
import static org.junit.Assert.assertEquals;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.asymmetric.signer.configuration.ConfigDigitalSignerAlgorithmAndSpec;
import com.scytl.cryptolib.asymmetric.signer.configuration.DigitalSignerPolicy;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

public class DigitalSignerFactoryTest {

    @BeforeClass
    public static void setUp() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @AfterClass
    public static void removeBouncyCastleProvider() {
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Test
    public void whenCreateDigitalSignerFactoryUsingSha256WithRsaAndPssAndBc() {

        DigitalSignerFactory DigitalSignerFactoryByPolicy =
            new DigitalSignerFactory(
                getDigitalSignerPolicySha256WithRsaAndPssAndBc());

        DigitalSignerPolicy expectedDigitalSignerPolicy =
            getField(DigitalSignerFactoryByPolicy, "_digitalSignerPolicy");

        assertEquals(getDigitalSignerPolicySha256WithRsaAndPssAndBc()
            .getDigitalSignerAlgorithmAndSpec(),
            expectedDigitalSignerPolicy.getDigitalSignerAlgorithmAndSpec());
    }

    private DigitalSignerPolicy getDigitalSignerPolicySha256WithRsaAndPssAndBc() {
        return new DigitalSignerPolicy() {

            @Override
            public ConfigDigitalSignerAlgorithmAndSpec getDigitalSignerAlgorithmAndSpec() {
                return ConfigDigitalSignerAlgorithmAndSpec.SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC;
            }

            @Override
            public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {

                switch (OperatingSystem.current()) {

                case WINDOWS:
                    return ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI;
                case LINUX:
                    return ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN;
                default:
                    throw new CryptoLibException("OS not supported");
                }
            }

        };
    }

}
