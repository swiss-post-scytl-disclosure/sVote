/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.xml.signer.factory;

import static mockit.Deencapsulation.getField;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.asymmetric.xml.signer.configuration.ConfigXmlDigitalSignerAlgorithmAndSpec;
import com.scytl.cryptolib.asymmetric.xml.signer.configuration.XmlDigitalSignerPolicy;

public class XmlDigitalSignerFactoryTest {

    @Test
    public void whenCreateXmlDigitalSignerFactoryUsingSha256WithRsaSha256EnvelopedExclusiveDom() {

        XmlDigitalSignerFactory xmlDigitalSignerFactoryByPolicy =
            new XmlDigitalSignerFactory(
                getDigitalSignerPolicySha256WithRsaSha256EnvelopedExclusiveDom());

        XmlDigitalSignerPolicy expectedXmlDigitalSignerPolicy =
            getField(xmlDigitalSignerFactoryByPolicy,
                "_xmlDigitalSignerPolicy");

        Assert
            .assertEquals(
                getDigitalSignerPolicySha256WithRsaSha256EnvelopedExclusiveDom()
                    .getXmlDigitalSignerAlgorithmAndSpec(),
                expectedXmlDigitalSignerPolicy
                    .getXmlDigitalSignerAlgorithmAndSpec());
    }

    private XmlDigitalSignerPolicy getDigitalSignerPolicySha256WithRsaSha256EnvelopedExclusiveDom() {
        return new XmlDigitalSignerPolicy() {

            @Override
            public ConfigXmlDigitalSignerAlgorithmAndSpec getXmlDigitalSignerAlgorithmAndSpec() {
                return ConfigXmlDigitalSignerAlgorithmAndSpec.SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG;
            }
        };
    }

}
