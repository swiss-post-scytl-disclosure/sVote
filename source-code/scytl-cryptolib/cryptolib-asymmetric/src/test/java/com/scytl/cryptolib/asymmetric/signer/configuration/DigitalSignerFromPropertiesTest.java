/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.configuration;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;

public class DigitalSignerFromPropertiesTest {

    private static final int PADDING_SALT_BIT_LENGTH = 32;

    private static final int PADDING_TRAILER_FIELD = 1;

    private static String INVALID_PATH = "Invalid path";

    private static DigitalSignerPolicyFromProperties _signerPolicyFromProperties;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _signerPolicyFromProperties =
            new DigitalSignerPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @Test
    public void givenPolicyWhenGetDigitalSignerConfigThenExpectedValues() {

        ConfigDigitalSignerAlgorithmAndSpec config =
            _signerPolicyFromProperties.getDigitalSignerAlgorithmAndSpec();

        boolean algorithmAndPaddingCorrect =
            config.getAlgorithmAndPadding().equals("SHA256withRSA/PSS");
        boolean paddingMessageDigestAlgorithmCorrect =
            config.getPaddingMessageDigestAlgorithm().equals("SHA-256");
        boolean paddingMaskingGenerationFunctionAlgorithmCorrect =
            config.getPaddingInfo()
                .getPaddingMaskingGenerationFunctionAlgorithm()
                .equals("MGF1");
        boolean paddingMaskingGenerationFunctionMessageDigestAlgorithmCorrect =
            config
                .getPaddingInfo()
                .getPaddingMaskingGenerationFunctionMessageDigestAlgorithm()
                .equals("SHA-256");
        boolean paddingSaltBitLengthCorrect =
            config.getPaddingInfo()
                .getPaddingSaltBitLength() == PADDING_SALT_BIT_LENGTH;
        boolean paddingTrailerFieldCorrect =
            config.getPaddingInfo()
                .getPaddingTrailerField() == PADDING_TRAILER_FIELD;

        assert (algorithmAndPaddingCorrect
            && paddingMessageDigestAlgorithmCorrect
            && paddingMaskingGenerationFunctionAlgorithmCorrect
            && paddingMaskingGenerationFunctionMessageDigestAlgorithmCorrect
            && paddingSaltBitLengthCorrect && paddingTrailerFieldCorrect);
    }

    @Test(expected = CryptoLibException.class)
    public void whenPolicyFromIncorrectPathThenException()
            throws GeneralCryptoLibException {

        new DigitalSignerPolicyFromProperties(INVALID_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyBlankLabels() {

        new DigitalSignerPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyInvalidLabels()
            throws GeneralCryptoLibException {

        new DigitalSignerPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

}
