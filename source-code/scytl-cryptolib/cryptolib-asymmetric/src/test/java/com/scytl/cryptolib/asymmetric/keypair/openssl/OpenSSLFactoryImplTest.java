/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import mockit.Expectations;
import mockit.Injectable;

/**
 * Tests of {@link OpenSSLFactory}.
 */
public class OpenSSLFactoryImplTest {
    @Injectable
    private ProcessFactory processFactory;

    @Injectable
    private Process process;

    private OpenSSLFactoryImpl factory;

    @Before
    public void setUp() {
        factory = new OpenSSLFactoryImpl(processFactory);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetOpenSSL()
            throws IOException, InterruptedException, OpenSSLException {
        new Expectations() {
            {
                processFactory.createProcess(withInstanceOf(List.class));
                result = process;
                process.getInputStream();
                result =
                    new ByteArrayInputStream("OpenSSL 1.1.0g  2 Nov 2017"
                        .getBytes(StandardCharsets.UTF_8));
                process.waitFor();
                result = 0;
            }
        };
        OpenSSL openSSL = factory.getOpenSSL();
        assertEquals(openSSL, factory.getOpenSSL());
    }

    @SuppressWarnings("unchecked")
    @Test(expected = OpenSSLException.class)
    public void testGetOpenSSLInvalidVersion098zh()
            throws IOException, InterruptedException, OpenSSLException {
        new Expectations() {
            {
                processFactory.createProcess(withInstanceOf(List.class));
                result = process;
                process.getInputStream();
                result = new ByteArrayInputStream(
                    "OpenSSL 0.9.8zh 3 Dec 2015".getBytes(StandardCharsets.UTF_8));
                process.waitFor();
                result = 0;
            }
        };
        factory.getOpenSSL();
    }

    @SuppressWarnings("unchecked")
    @Test(expected = OpenSSLException.class)
    public void testGetOpenSSLInvalidVersion101f()
            throws IOException, InterruptedException, OpenSSLException {
        new Expectations() {
            {
                processFactory.createProcess(withInstanceOf(List.class));
                result = process;
                process.getInputStream();
                result = new ByteArrayInputStream(
                        "OpenSSL 1.0.1f 6 Jan 2014".getBytes(StandardCharsets.UTF_8));
                process.waitFor();
                result = 0;
            }
        };
        factory.getOpenSSL();
    }

    @SuppressWarnings("unchecked")
    @Test(expected = OpenSSLException.class)
    public void testGetOpenSSLUnavailable()
            throws IOException, OpenSSLException {
        new Expectations() {
            {
                processFactory.createProcess(withInstanceOf(List.class));
                result = new IOException("test");
            }
        };
        factory.getOpenSSL();
    }
}
