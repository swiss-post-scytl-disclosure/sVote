/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;

/**
 * Configuration policy to construct objects of type
 * {@link com.scytl.cryptolib.asymmetric.keypair.factory.CryptoKeyPairGenerator}
 * <P>
 * Implementations must be immutable.
 */
public interface KeyPairPolicy extends SecureRandomPolicy,
        SigningKeyPairPolicy, EncryptionKeyPairPolicy {

}
