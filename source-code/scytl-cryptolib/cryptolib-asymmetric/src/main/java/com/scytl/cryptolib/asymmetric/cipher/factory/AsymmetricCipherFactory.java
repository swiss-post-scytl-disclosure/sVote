/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.cipher.factory;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.cipher.configuration.AsymmetricCipherPolicy;

/**
 * A factory class for creating an asymmetric cipher.
 */
public class AsymmetricCipherFactory {

    private final AsymmetricCipherPolicy _asymmetricCipherPolicy;

    /**
     * Constructs a {@code AsymmetricCipherFactory} using the provided
     * {@link AsymmetricCipherPolicy}.
     * 
     * @param asymmetricCipherPolicy
     *            the {@code AsymmetricCipherPolicy} to be used to configure this
     *            AsymmetricCipherFactory.
     *            <P>
     *            NOTE: The received {@link AsymmetricCipherPolicy} should be an
     *            immutable object. If this is the case, then the entire
     *            {@code AsymmetricCipherFactory} class is thread safe.
     */
    public AsymmetricCipherFactory(
            final AsymmetricCipherPolicy asymmetricCipherPolicy) {

        _asymmetricCipherPolicy = asymmetricCipherPolicy;
    }

    /**
     * Creates a {@link CryptoAsymmetricCipher} according to the given asymmetric
     * cipher policy.
     * 
     * @return a {@link CryptoAsymmetricCipher} object.
     */
    public CryptoAsymmetricCipher create() {

        try {
            return new CryptoAsymmetricCipher(_asymmetricCipherPolicy);
        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }
}
