/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.cipher.configuration;

/**
 * Enum which defines the paddings.
 */
public enum Padding {

    PKCS1("PKCS1Padding"), PKCS5("PKCS5Padding"), PSS("PSS"), NONE(
            "NoPadding");

    private final String _paddingName;

    private Padding(final String paddingName) {
        _paddingName = paddingName;
    }

    public String getPaddingName() {
        return _paddingName;
    }
}
