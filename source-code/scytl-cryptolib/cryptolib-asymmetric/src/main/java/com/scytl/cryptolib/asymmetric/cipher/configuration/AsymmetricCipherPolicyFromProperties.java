/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.cipher.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;
import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;
import com.scytl.cryptolib.symmetric.cipher.configuration.ConfigSymmetricCipherAlgorithmAndSpec;
import com.scytl.cryptolib.symmetric.cipher.configuration.SymmetricCipherPolicy;
import com.scytl.cryptolib.symmetric.key.configuration.ConfigSecretKeyAlgorithmAndSpec;
import com.scytl.cryptolib.symmetric.key.configuration.SecretKeyPolicy;

/**
 * Implementation of the {@link AsymmetricCipherPolicy} interface, which reads
 * values from a properties input.
 * <P>
 * Instances of this class are immutable.
 */
public class AsymmetricCipherPolicyFromProperties implements
        AsymmetricCipherPolicy, SecureRandomPolicy, SecretKeyPolicy,
        SymmetricCipherPolicy {

    private final ConfigAsymmetricCipherAlgorithmAndSpec _asymmetricCipherAlgorithmAndSpec;

    private final ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmAndProvider;

    private final ConfigSecretKeyAlgorithmAndSpec _secretKeyAlgorithmAndSpec;

    private final ConfigSymmetricCipherAlgorithmAndSpec _symmetricCipherAlgorithmAndSpec;

    /**
     * Creates an asymmetric cipher policy using properties which are read from
     * the properties file at the specified path.
     * 
     * @param path
     *            the path where the properties are read from.
     * @throws CryptoLibException
     *             if {@code path} or properties file is invalid.
     */
    public AsymmetricCipherPolicyFromProperties(final String path) {

        try {
            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _asymmetricCipherAlgorithmAndSpec =
                ConfigAsymmetricCipherAlgorithmAndSpec.valueOf(helper
                    .getNotBlankPropertyValue("asymmetric.cipher"));

            _secureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider
                    .valueOf(helper
                        .getNotBlankOSDependentPropertyValue("asymmetric.cipher.securerandom"));

            _secretKeyAlgorithmAndSpec = ConfigSecretKeyAlgorithmAndSpec
                .valueOf(helper.getNotBlankPropertyValue("asymmetric.cipher.symmetric.encryptionsecretkey"));

            _symmetricCipherAlgorithmAndSpec =
                ConfigSymmetricCipherAlgorithmAndSpec
                    .valueOf(helper
                        .getNotBlankPropertyValue("asymmetric.cipher.symmetric.cipher"));
        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigAsymmetricCipherAlgorithmAndSpec getAsymmetricCipherAlgorithmAndSpec() {

        return _asymmetricCipherAlgorithmAndSpec;
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {

        return _secureRandomAlgorithmAndProvider;
    }

    @Override
    public ConfigSecretKeyAlgorithmAndSpec getSecretKeyAlgorithmAndSpec() {

        return _secretKeyAlgorithmAndSpec;
    }

    @Override
    public ConfigSymmetricCipherAlgorithmAndSpec getSymmetricCipherAlgorithmAndSpec() {

        return _symmetricCipherAlgorithmAndSpec;
    }
}
