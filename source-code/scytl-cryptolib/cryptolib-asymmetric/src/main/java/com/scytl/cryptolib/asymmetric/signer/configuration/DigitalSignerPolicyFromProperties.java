/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * Implementation of the {@link DigitalSignerPolicy} interface, which reads
 * values from a properties input.
 * <P>
 * Instances of this class are immutable.
 */
public class DigitalSignerPolicyFromProperties implements
        DigitalSignerPolicy {

    private final ConfigDigitalSignerAlgorithmAndSpec _signerAlgorithmAndSpec;

    private final ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmAndProvider;

    /**
     * Creates a policy configured with the given configuration file.
     * @param path
     *            the path where the properties are read from.
     * @throws CryptoLibException
     *             if {@code path} is invalid.
     */
    public DigitalSignerPolicyFromProperties(final String path) {

        try {
            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);
            _signerAlgorithmAndSpec =
                ConfigDigitalSignerAlgorithmAndSpec.valueOf(helper
                    .getNotBlankPropertyValue("asymmetric.signer"));

            // Pick up the PRNG configuration.
            _secureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider.valueOf(helper
                    .getNotBlankOSDependentPropertyValue("asymmetric.signer.securerandom"));

        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigDigitalSignerAlgorithmAndSpec getDigitalSignerAlgorithmAndSpec() {

        return _signerAlgorithmAndSpec;
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
        return _secureRandomAlgorithmAndProvider;
    }

}
