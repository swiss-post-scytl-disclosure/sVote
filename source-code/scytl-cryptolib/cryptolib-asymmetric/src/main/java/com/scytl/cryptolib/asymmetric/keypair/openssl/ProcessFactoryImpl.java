/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;

import java.io.IOException;
import java.util.List;

/**
 * Implementation of {@link ProcessFactory}.
 */
class ProcessFactoryImpl implements ProcessFactory {
    private static final ProcessFactoryImpl INSTANCE =
        new ProcessFactoryImpl();

    private ProcessFactoryImpl() {
    }

    /**
     * Returns the instance.
     *
     * @return the instance.
     */
    public static ProcessFactoryImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public Process createProcess(final List<String> command)
            throws IOException {
        return new ProcessBuilder(command).start();
    }
}
