/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.constants;

/**
 * Contains constants used in KeyPair.
 */
public final class KeyPairConstants {

    /**
     * This class cannot be instantiated.
     */
    private KeyPairConstants() {
    }

    /**
     * The RSA algorithm.
     */
    public static final String RSA_ALG = "RSA";
}
