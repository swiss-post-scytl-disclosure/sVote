/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum representing a set of parameters that can be used when requesting a
 * digital signer. These are:
 * <P>
 * <ol>
 * <li>An algorithm/padding</li>
 * <li>A padding message digest algorithm</li>
 * <li>Padding info</li>
 * <li>A provider</li>
 * </ol>
 * Instances of this enum are immutable.
 */
public enum ConfigDigitalSignerAlgorithmAndSpec {

    SHA256withRSA_SHA256_BC("SHA256withRSA", "SHA-256",
            PaddingInfo.NO_PADDING_INFO, Provider.BOUNCY_CASTLE),

    SHA256withRSA_SHA256_DEFAULT("SHA256withRSA", "SHA-256",
            PaddingInfo.NO_PADDING_INFO, Provider.DEFAULT),

    SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC("SHA256withRSA/PSS",
            "SHA-256", PaddingInfo.PSS_PADDING_INFO, Provider.BOUNCY_CASTLE),

    SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_DEFAULT(
            "SHA256withRSA/PSS", "SHA-256", PaddingInfo.PSS_PADDING_INFO,
            Provider.DEFAULT);

    private final String _algorithmAndPadding;

    private final String _paddingMessageDigestAlgorithm;

    private final PaddingInfo _paddingInfo;

    private final Provider _provider;

    private ConfigDigitalSignerAlgorithmAndSpec(
            final String algorithmAndPadding,
            final String paddingMessageDigest, final PaddingInfo paddingInfo,
            final Provider provider) {

        _algorithmAndPadding = algorithmAndPadding;
        _paddingMessageDigestAlgorithm = paddingMessageDigest;
        _paddingInfo = paddingInfo;
        _provider = provider;
    }

    /**
     * @return The algorithm and padding scheme of this
     *         {@link ConfigDigitalSignerAlgorithmAndSpec}.
     */
    public String getAlgorithmAndPadding() {

        return _algorithmAndPadding;
    }

    /**
     * @return The provider of this {@link ConfigDigitalSignerAlgorithmAndSpec}.
     */
    public String getPaddingMessageDigestAlgorithm() {

        return _paddingMessageDigestAlgorithm;
    }

    /**
     * @return Additional padding information, this depends on the particular
     *         algorithm and spec. {@link ConfigDigitalSignerAlgorithmAndSpec}.
     */
    public PaddingInfo getPaddingInfo() {

        return _paddingInfo;
    }

    /**
     * @return The provider of this {@link ConfigDigitalSignerAlgorithmAndSpec}.
     */
    public Provider getProvider() {

        return _provider;
    }

}
