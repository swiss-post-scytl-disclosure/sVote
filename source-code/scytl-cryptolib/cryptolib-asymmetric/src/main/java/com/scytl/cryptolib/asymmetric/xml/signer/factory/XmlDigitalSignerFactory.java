/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.xml.signer.factory;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.asymmetric.xml.signer.configuration.XmlDigitalSignerPolicy;

/**
 * A factory class for creating an XML digital signer and an XML digital
 * signature verifier.
 */
public class XmlDigitalSignerFactory {

    private final XmlDigitalSignerPolicy _xmlDigitalSignerPolicy;

    /**
     * Constructs a {@code XmlDigitalSignerFactory} using the provided
     * {@link XmlDigitalSignerPolicy}.
     * 
     * @param xmlDigitalSignerPolicy
     *            The XmlDigitalSignerPolicy to be used to configure this
     *            XmlDigitalSignerFactory.
     *            <P>
     *            NOTE: The received {@link XmlDigitalSignerPolicy} should be an
     *            immutable object. If this is the case, then the entire
     *            {@code XmlDigitalSignerFactory} class is thread safe.
     */
    public XmlDigitalSignerFactory(
            final XmlDigitalSignerPolicy xmlDigitalSignerPolicy) {

        _xmlDigitalSignerPolicy = xmlDigitalSignerPolicy;
    }

    /**
     * Creates a {@link CryptoXmlDigitalSigner} according to the given digital
     * signer policy.
     * 
     * @return a {@link CryptoXmlDigitalSigner} object.
     * @throws CryptoLibException
     *             if the creation of {@link CryptoXmlDigitalSigner} was failed.
     */
    public CryptoXmlDigitalSigner create() throws CryptoLibException {

        return new CryptoXmlDigitalSigner(_xmlDigitalSignerPolicy);
    }
}
