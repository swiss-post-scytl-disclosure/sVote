/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;

/**
 * Factory of {@link OpenSSL}.
 * <p>
 * Implementation must be thread-safe
 */
interface OpenSSLFactory {
    /**
     * Returns an {@link OpenSSL} instance.
     *
     * @return the instance.
     * @throws OpenSSLException
     *             OpenSSL is not available.
     */
    OpenSSL getOpenSSL() throws OpenSSLException;
}
