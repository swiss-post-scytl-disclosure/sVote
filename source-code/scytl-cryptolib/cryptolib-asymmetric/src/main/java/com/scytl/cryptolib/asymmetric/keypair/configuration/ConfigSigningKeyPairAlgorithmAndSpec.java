/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.configuration;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;

import com.scytl.cryptolib.asymmetric.keypair.constants.KeyPairConstants;
import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum which defines the signing key pair algorithms and their specification.
 * <p>
 * Each element of the enum contains the following attributes:
 * <ol>
 * <li>An algorithm.</li>
 * <li>A {@link RSAKeyGenParameterSpec}, which is composed of: (a) a key size;
 * and (b) an exponent, which is set to 65537 by default.</li>
 * <li>A {@link Provider}.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */

public enum ConfigSigningKeyPairAlgorithmAndSpec {

    RSA_2048_F4_SUN_RSA_SIGN(KeyPairConstants.RSA_ALG,
            new RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4),
            Provider.SUN_RSA_SIGN),

    RSA_3072_F4_SUN_RSA_SIGN(KeyPairConstants.RSA_ALG,
            new RSAKeyGenParameterSpec(3072, RSAKeyGenParameterSpec.F4),
            Provider.SUN_RSA_SIGN),

    RSA_4096_F4_SUN_RSA_SIGN(KeyPairConstants.RSA_ALG,
            new RSAKeyGenParameterSpec(4096, RSAKeyGenParameterSpec.F4),
            Provider.SUN_RSA_SIGN),

    RSA_2048_F4_BC(KeyPairConstants.RSA_ALG, new RSAKeyGenParameterSpec(
        2048, RSAKeyGenParameterSpec.F4), Provider.BOUNCY_CASTLE),

    RSA_3072_F4_BC(KeyPairConstants.RSA_ALG, new RSAKeyGenParameterSpec(
        3072, RSAKeyGenParameterSpec.F4), Provider.BOUNCY_CASTLE),

    RSA_4096_F4_BC(KeyPairConstants.RSA_ALG, new RSAKeyGenParameterSpec(
        4096, RSAKeyGenParameterSpec.F4), Provider.BOUNCY_CASTLE),

    RSA_2048_OPENSSL_RSA_SIGN(KeyPairConstants.RSA_ALG,
            new RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4),
            Provider.OPENSSL),

    RSA_3072_OPENSSL_RSA_SIGN(KeyPairConstants.RSA_ALG,
            new RSAKeyGenParameterSpec(3072, RSAKeyGenParameterSpec.F4),
            Provider.OPENSSL),

    RSA_4096_OPENSSL_RSA_SIGN(KeyPairConstants.RSA_ALG,
            new RSAKeyGenParameterSpec(4096, RSAKeyGenParameterSpec.F4),
            Provider.OPENSSL);

    private final String _algorithm;

    private final Provider _provider;

    private final AlgorithmParameterSpec _algorithmParameterSpec;

    private ConfigSigningKeyPairAlgorithmAndSpec(final String name,
            final AlgorithmParameterSpec algorithmParameterSpec,
            final Provider provider) {

        _algorithm = name;
        _algorithmParameterSpec = algorithmParameterSpec;
        _provider = provider;
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public AlgorithmParameterSpec getSpec() {
        return _algorithmParameterSpec;
    }

    public Provider getProvider() {
        return _provider;
    }
}
