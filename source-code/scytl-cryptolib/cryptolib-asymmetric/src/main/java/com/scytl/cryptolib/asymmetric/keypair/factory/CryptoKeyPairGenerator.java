/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.factory;

import java.security.KeyPair;
import java.security.KeyPairGenerator;

/**
 * Instances of this class are immutable.
 */
public final class CryptoKeyPairGenerator {

    private final KeyPairGenerator _keyPairGenerator;

    /**
     * Constructor which stores a {@link KeyPairGenerator}.
     * <p>
     * Note: depending on how the {@link KeyPairGenerator} has been configured,
     * this class will generate the corresponding types of {@link KeyPair}.
     * 
     * @param keyPairGenerator
     *            the {@link KeyPairGenerator} that should be already
     *            initialized.
     */
    CryptoKeyPairGenerator(final KeyPairGenerator keyPairGenerator) {
        _keyPairGenerator = keyPairGenerator;
    }

    /**
     * Generates a key pair.
     * @return the generated key pair.
     * @see KeyPairGenerator#genKeyPair()
     */
    public KeyPair genKeyPair() {
        return _keyPairGenerator.genKeyPair();
    }

}
