/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.configuration;

/**
 * Interface which specifies the methods that must be implemented when creating
 * signing key pairs.
 */
public interface SigningKeyPairPolicy {

    /**
     * Returns the
     *         {@link com.scytl.cryptolib.asymmetric.keypair.configuration.ConfigSigningKeyPairAlgorithmAndSpec}
     *         (which encapsulates an algorithm, a provider and an
     *         {@link java.security.spec.AlgorithmParameterSpec}) that should be
     *         used to construct signing key pairs.
     * @return {@code com.scytl.cryptolib.asymmetric.keypair.configuration.ConfigSigningKeyPairAlgorithmAndSpec}
     */

    ConfigSigningKeyPairAlgorithmAndSpec getSigningKeyPairAlgorithmAndSpec();
}
