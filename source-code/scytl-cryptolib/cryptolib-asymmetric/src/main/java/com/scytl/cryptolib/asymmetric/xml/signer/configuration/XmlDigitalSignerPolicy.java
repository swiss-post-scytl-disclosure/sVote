/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.xml.signer.configuration;

/**
 * Configuration policy for signing and verifying XML.
 * <P>
 * Implementations must be immutable.
 */
public interface XmlDigitalSignerPolicy {

    /**
     * Returns configuration for signing and verifying XML.
     * @return configuration.
     */
    ConfigXmlDigitalSignerAlgorithmAndSpec getXmlDigitalSignerAlgorithmAndSpec();

}
