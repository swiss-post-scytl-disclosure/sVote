/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;

/**
 * Generic exception for errors of {@link OpenSSL}.
 */
class OpenSSLException extends Exception {
    private static final long serialVersionUID = -6820283716295702958L;

    /**
     * Constructor.
     *
     * @param message
     */
    public OpenSSLException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param message
     * @param cause
     */
    public OpenSSLException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
