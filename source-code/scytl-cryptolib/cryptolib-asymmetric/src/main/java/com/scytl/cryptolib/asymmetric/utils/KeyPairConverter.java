/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.utils;

import java.security.PrivateKey;
import java.security.PublicKey;

import com.scytl.cryptolib.api.asymmetric.utils.KeyPairConverterAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.utils.validations.Validate;
import com.scytl.cryptolib.primitives.primes.utils.PemUtils;

/**
 * This class implements {@link KeyPairConverterAPI}.
 */
public class KeyPairConverter implements KeyPairConverterAPI {

    /**
     * Default constructor.
     */
    public KeyPairConverter() {
    }

    @Override
    public PublicKey getPublicKeyForSigningFromPem(
            final String publicKeyPem) throws GeneralCryptoLibException {

        Validate.notNullOrBlank(publicKeyPem, "Public key PEM string");

        return PemUtils.publicKeyFromPem(publicKeyPem);
    }

    @Override
    public PublicKey getPublicKeyForEncryptingFromPem(
            final String publicKeyPem) throws GeneralCryptoLibException {

        Validate.notNullOrBlank(publicKeyPem, "Public key PEM string");

        return PemUtils.publicKeyFromPem(publicKeyPem);
    }

    @Override
    public PrivateKey getPrivateKeyForSigningFromPem(
            final String privateKeyPem) throws GeneralCryptoLibException {

        Validate.notNullOrBlank(privateKeyPem, "Private key PEM string");

        return PemUtils.privateKeyFromPem(privateKeyPem);
    }

    @Override
    public PrivateKey getPrivateKeyForEncryptingFromPem(
            final String privateKeyPem) throws GeneralCryptoLibException {

        Validate.notNullOrBlank(privateKeyPem, "Private key PEM string");

        return PemUtils.privateKeyFromPem(privateKeyPem);
    }

    @Override
    public String exportPublicKeyForSigningToPem(final PublicKey publicKey)
            throws GeneralCryptoLibException {

        Validate.notNull(publicKey, "Public key");
        Validate.notNullOrEmpty(publicKey.getEncoded(),
            "Public key content");

        return PemUtils.publicKeyToPem(publicKey);
    }

    @Override
    public String exportPublicKeyForEncryptingToPem(
            final PublicKey publicKey) throws GeneralCryptoLibException {

        Validate.notNull(publicKey, "Public key");
        Validate.notNullOrEmpty(publicKey.getEncoded(),
            "Public key content");

        return PemUtils.publicKeyToPem(publicKey);
    }

    @Override
    public String exportPrivateKeyForSigningToPem(
            final PrivateKey privateKey) throws GeneralCryptoLibException {

        Validate.notNull(privateKey, "Private key");
        Validate.notNullOrEmpty(privateKey.getEncoded(),
            "Private key content");

        return PemUtils.privateKeyToPem(privateKey);
    }

    @Override
    public String exportPrivateKeyForEncryptingToPem(
            final PrivateKey privateKey) throws GeneralCryptoLibException {

        Validate.notNull(privateKey, "Private key");
        Validate.notNullOrEmpty(privateKey.getEncoded(),
            "Private key content");

        return PemUtils.privateKeyToPem(privateKey);
    }
}
