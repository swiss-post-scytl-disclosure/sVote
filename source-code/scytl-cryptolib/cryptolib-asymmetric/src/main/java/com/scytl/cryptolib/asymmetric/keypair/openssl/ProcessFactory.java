/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;

import java.io.IOException;
import java.util.List;

/**
 * Factory of {@link Process}. This is a part of {@link OpenSSLImpl}.
 * <p>
 * Implementation must be thread-safe.
 */
interface ProcessFactory {
    /**
     * Creates a new process for the specified command.
     *
     * @param command
     *            the command
     * @return the running process
     * @throws IOException
     *             failed to create the process.
     */
    Process createProcess(List<String> command) throws IOException;
}
