/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.configuration;

/**
 * Enum representing padding information to be used by a signing algorithm.
 * <P>
 * <ol>
 * <li>A padding masking generation function algorithm</li>
 * <li>A padding masking generation function message digest algorithm</li>
 * <li>A padding salt bit length</li>
 * <li>A padding trailer field</li>
 * </ol>
 * Instances of this enum are immutable.
 */
public enum PaddingInfo {

    NO_PADDING_INFO(),

    PSS_PADDING_INFO("MGF1", "SHA-256", 32, 1);

    private final String _paddingMaskingGenerationFunctionAlgorithm;

    private final String _paddingMaskingGenerationFunctionMessageDigestAlgorithm;

    private final int _paddingSaltBitLength;

    private final int _paddingTrailerField;

    PaddingInfo() {

        _paddingMaskingGenerationFunctionAlgorithm = null;
        _paddingMaskingGenerationFunctionMessageDigestAlgorithm = null;
        _paddingSaltBitLength = 0;
        _paddingTrailerField = 0;
    }

    PaddingInfo(final String paddingMaskingGenerationFunctionAlgorithm,
            final String paddingMaskingGenerationFunctionMessageDigest,
            final int paddingSaltBitLength, final int paddingTrailerField) {

        _paddingMaskingGenerationFunctionAlgorithm =
            paddingMaskingGenerationFunctionAlgorithm;
        _paddingMaskingGenerationFunctionMessageDigestAlgorithm =
            paddingMaskingGenerationFunctionMessageDigest;
        _paddingSaltBitLength = paddingSaltBitLength;
        _paddingTrailerField = paddingTrailerField;
    }

    /**
     * @return The padding masking generation function algorithm of this
     *         {@link PaddingInfo}.
     */
    public String getPaddingMaskingGenerationFunctionAlgorithm() {

        return _paddingMaskingGenerationFunctionAlgorithm;
    }

    /**
     * @return The padding masking generation function message digest algorithm
     *         of this {@link PaddingInfo}.
     */
    public String getPaddingMaskingGenerationFunctionMessageDigestAlgorithm() {

        return _paddingMaskingGenerationFunctionMessageDigestAlgorithm;
    }

    /**
     * @return The padding salt bit length of this {@link PaddingInfo}.
     */
    public int getPaddingSaltBitLength() {

        return _paddingSaltBitLength;
    }

    /**
     * @return The padding trailer field of this {@link PaddingInfo}.
     */
    public int getPaddingTrailerField() {

        return _paddingTrailerField;
    }
}
