/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * An implementation of the {@link KeyPairPolicy} interface, which reads values
 * from a properties input.
 * <P>
 * Instances of this policy fully configure the behavior of
 * {@link com.scytl.cryptolib.asymmetric.keypair.factory.KeyPairGeneratorFactory}.
 * <P>
 * Instances of this class are immutable.
 */
public final class KeyPairPolicyFromProperties implements KeyPairPolicy {

    private final ConfigSigningKeyPairAlgorithmAndSpec _signingAlgorithm;

    private final ConfigEncryptionKeyPairAlgorithmAndSpec _encryptionAlgorithm;

    private final ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmAndProvider;

    /**
     * Constructs an instance of KeyPairPolicyFromProperties from the given
     * path.
     * <P>
     * Note: a single property is used to configure the secure random algorithm
     * and provider that is used when creating both signing and encryption
     * asymmetric keys.
     * 
     * @param path
     *            the path where the properties are read from.
     * @throws CryptoLibException
     *            it path or properties file is invalid.
     */
    public KeyPairPolicyFromProperties(final String path) {

        try {

            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _signingAlgorithm =
                ConfigSigningKeyPairAlgorithmAndSpec
                    .valueOf(helper
                        .getNotBlankPropertyValue("asymmetric.signingkeypair"));

            _encryptionAlgorithm =
                ConfigEncryptionKeyPairAlgorithmAndSpec
                    .valueOf(helper
                        .getNotBlankPropertyValue("asymmetric.encryptionkeypair"));

            _secureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider
                    .valueOf(helper
                        .getNotBlankOSDependentPropertyValue("asymmetric.keypair.securerandom"));

        } catch (IllegalArgumentException e) {
            throw new CryptoLibException(
                "An IllegalArgumentException exception was thrown while trying to obtain a secure random property from a properties source.",
                e);
        }
    }

    @Override
    public ConfigSigningKeyPairAlgorithmAndSpec getSigningKeyPairAlgorithmAndSpec() {
        return _signingAlgorithm;
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
        return _secureRandomAlgorithmAndProvider;
    }

    @Override
    public ConfigEncryptionKeyPairAlgorithmAndSpec getEncryptingKeyPairAlgorithmAndSpec() {
        return _encryptionAlgorithm;
    }

}
