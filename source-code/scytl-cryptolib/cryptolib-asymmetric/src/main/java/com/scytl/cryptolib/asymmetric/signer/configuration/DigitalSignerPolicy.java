/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * Configuration policy for digitally signing and verifying a digital signature.
 * <P>
 * Implementations must be immutable.
 */
public interface DigitalSignerPolicy {

    /**
     * Returns configuration for digitally signing and verifying a digital
     *         signature.
     * @return configuration.
     */
    ConfigDigitalSignerAlgorithmAndSpec getDigitalSignerAlgorithmAndSpec();

    /**
     * Returns the {@link ConfigSecureRandomAlgorithmAndProvider} that should be
     * used when getting instances of {@link java.security.SecureRandom}.
     *
     * @return The {@link ConfigSecureRandomAlgorithmAndProvider}.
     */
    ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider();
}
