/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.signer.factory;

import com.scytl.cryptolib.asymmetric.signer.configuration.DigitalSignerPolicy;

/**
 * A factory class for creating a digital signer and a digital signature
 * verifier.
 */
public class DigitalSignerFactory {

    private final DigitalSignerPolicy _digitalSignerPolicy;

    /**
     * Constructs a {@code DigitalSignerFactory} using the provided
     * {@link DigitalSignerPolicy}.
     * 
     * @param digitalSignerPolicy
     *            the digital signer policy to be used to configure this
     *            {@code DigitalSignerFactory}.
     *            <P>
     *            NOTE: The received {@link DigitalSignerPolicy} should be an
     *            immutable object. If this is the case, then the entire
     *            {@code DigitalSignerFactory} class is thread safe.
     */
    public DigitalSignerFactory(
            final DigitalSignerPolicy digitalSignerPolicy) {

        _digitalSignerPolicy = digitalSignerPolicy;
    }

    /**
     * Creates a {@link CryptoDigitalSigner} according to the given digital
     * signer policy.
     * 
     * @return a {@link CryptoDigitalSigner} object.
     */
    public CryptoDigitalSigner create() {

        return new CryptoDigitalSigner(_digitalSignerPolicy);
    }

}
