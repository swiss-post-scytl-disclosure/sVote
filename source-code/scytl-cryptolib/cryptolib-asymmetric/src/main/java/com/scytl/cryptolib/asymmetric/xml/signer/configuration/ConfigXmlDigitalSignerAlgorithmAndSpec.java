/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.xml.signer.configuration;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Transform;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum representing a set of parameters that can be used when requesting an XML
 * digital signer.
 * <P>
 * These parameters are:
 * <ol>
 * <li>A digital signature algorithm.</li>
 * <li>A message digest algorithm.</li>
 * <li>A transform method.</li>
 * <li>A canonicalization method.</li>
 * <li>A mechanism type.</li>
 * <li>A provider.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigXmlDigitalSignerAlgorithmAndSpec {

    SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG(
            "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
            DigestMethod.SHA256, Transform.ENVELOPED,
            CanonicalizationMethod.EXCLUSIVE, "DOM", Provider.XML_DSIG),

    SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_DEFAULT(
            "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
            DigestMethod.SHA256, Transform.ENVELOPED,
            CanonicalizationMethod.EXCLUSIVE, "DOM", Provider.DEFAULT);

    private final String _digitalSignatureAlgorithm;

    private final String _messageDigestAlgorithm;

    private final String _transformMethod;

    private final String _canonicalizationMethod;

    private final String _mechanismType;

    private final Provider _provider;

    private ConfigXmlDigitalSignerAlgorithmAndSpec(
            final String digitalSignatureAlgorithm,
            final String messageDigestAlgorithm,
            final String transformMethod,
            final String canonicalizationMethod,
            final String mechanismType, final Provider provider) {

        _digitalSignatureAlgorithm = digitalSignatureAlgorithm;
        _messageDigestAlgorithm = messageDigestAlgorithm;
        _transformMethod = transformMethod;
        _canonicalizationMethod = canonicalizationMethod;
        _mechanismType = mechanismType;
        _provider = provider;
    }

    public String getDigitalSignatureAlgorithm() {

        return _digitalSignatureAlgorithm;
    }

    public String getMessageDigestAlgorithm() {

        return _messageDigestAlgorithm;
    }

    public String getTransformMethod() {

        return _transformMethod;
    }

    public String getCanonicalizationMethod() {

        return _canonicalizationMethod;
    }

    public String getMechanismType() {

        return _mechanismType;
    }

    public Provider getProvider() {
        return _provider;
    }

}
