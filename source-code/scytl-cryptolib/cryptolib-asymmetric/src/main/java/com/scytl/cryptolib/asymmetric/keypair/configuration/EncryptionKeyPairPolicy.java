/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.configuration;

/**
 * Interface which specifies the methods that must be implemented when creating
 * encryption key pairs.
 */
public interface EncryptionKeyPairPolicy {

    /**
     * Returns configuration for creating encryption key pairs.
     * @return configuration.
     */
    ConfigEncryptionKeyPairAlgorithmAndSpec getEncryptingKeyPairAlgorithmAndSpec();
}
