/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.xml.signer.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;

/**
 * Implementation of the {@link XmlDigitalSignerPolicy} interface, which reads
 * values from a properties input.
 * <P>
 * Instances of this class are immutable.
 */
public class XmlDigitalSignerPolicyFromProperties implements
        XmlDigitalSignerPolicy {

    private final ConfigXmlDigitalSignerAlgorithmAndSpec _xmlSignerAlgorithmAndSpec;

    /**
     * Creates a digital signer policy using properties which are read from
     * the properties file at the specified path.
     * @param path
     *            the path where the properties are read from.
     * @throws CryptoLibException
     *             if path or properties are invalid.
     */
    public XmlDigitalSignerPolicyFromProperties(final String path) {

        PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);
        
        _xmlSignerAlgorithmAndSpec =
                ConfigXmlDigitalSignerAlgorithmAndSpec.valueOf(helper
                    .getNotBlankPropertyValue("asymmetric.xml.signer"));
        
    }

    @Override
    public ConfigXmlDigitalSignerAlgorithmAndSpec getXmlDigitalSignerAlgorithmAndSpec() {

        return _xmlSignerAlgorithmAndSpec;
    }

}
