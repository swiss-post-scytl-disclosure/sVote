/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.cipher.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum representing a set of parameters that can be used when requesting an
 * asymmetric cipher.
 * <P>
 * These parameters are:
 * <ol>
 * <li>An algorithm/mode/padding.</li>
 * <li>A provider.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigAsymmetricCipherAlgorithmAndSpec {

    RSAwithRSAKEMANDKDF1ANDSHA256_BC(
            "RSA/RSA-KEMWITHKDF1ANDSHA-256/NOPADDING",
            Provider.BOUNCY_CASTLE),

    RSAwithRSAKEMANDKDF1ANDSHA256_DEFAULT(
            "RSA/RSA-KEMWITHKDF1ANDSHA-256/NOPADDING", Provider.DEFAULT),

    RSAwithRSAKEMANDKDF2ANDSHA256_BC(
            "RSA/RSA-KEMWITHKDF2ANDSHA-256/NOPADDING",
            Provider.BOUNCY_CASTLE),

    RSAwithRSAKEMANDKDF2ANDSHA256_DEFAULT(
            "RSA/RSA-KEMWITHKDF2ANDSHA-256/NOPADDING", Provider.DEFAULT);

    private final String _algorithmModePadding;

    private final Provider _provider;

    ConfigAsymmetricCipherAlgorithmAndSpec(final String algorithm,
            final Provider provider) {
        _algorithmModePadding = algorithm;
        _provider = provider;
    }

    public String getAlgorithmModePadding() {
        return _algorithmModePadding;
    }

    public Provider getProvider() {
        return _provider;
    }

}
