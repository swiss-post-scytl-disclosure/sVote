/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;

import java.util.Map;

/**
 * This interface represents commands of external OpenSSL.
 * <p>
 * Implementation must be thread-safe.
 */
interface OpenSSL {
    /**
     * Invokes {@code openssl genpkey -outform PEM -algorithm
     * <algorithm> -pkeyopt <options>} command. The returned out is a byte array
     * representing the generated private key in PEM format. The caller must
     * cleanup the returned array after use.
     *
     * @param algorithm
     *            the algorithm
     * @param options
     *            the options
     * @return the output
     * @throws OpenSSLException
     *             failed to invoke command.
     */
    byte[] genpkey(String algorithm, Map<String, String> options)
            throws OpenSSLException;

    /**
     * Invokes {@code openssl version} command.
     *
     * @return the output
     * @throws OpenSSLException
     *             failed to invoke command.
     */
    String version() throws OpenSSLException;
}
