/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.service;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.PooledProxiedServiceFactory;

/**
 * This Service factory creates thread-safe services. Thread-safe services proxy
 * all requests to a pool of non thread-safe service instances.
 */
public class PollingAsymmetricServiceFactory extends
        PooledProxiedServiceFactory<AsymmetricServiceAPI> implements
        ServiceFactory<AsymmetricServiceAPI> {
    /**
     * Default constructor. This factory will create services configured with
     * default values and a default pool.
     */
    public PollingAsymmetricServiceFactory() {
        super(new BasicAsymmetricServiceFactory(),
            new GenericObjectPoolConfig());
    }

    /**
     * Creates services configured with the given configuration file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingAsymmetricServiceFactory(final String path,
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicAsymmetricServiceFactory(path), poolConfig);
    }

    /**
     * Creates an instance of service configured with
     * default values and the given pool configuration.
     * 
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingAsymmetricServiceFactory(
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicAsymmetricServiceFactory(), poolConfig);
    }

}
