/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * This package provides key pair generator implementation which uses external
 * OpenSSL.
 * <p>
 * The public API consists of
 * <ul>
 * <li>{@link OpenSSLKeyPairGenerator} - implementation of
 * {@link java.security.KeyPairGenerator}.</li>
 * </ul>
 * <p>
 * Currently only {@code RSA} algorithm is supported.
 */
package com.scytl.cryptolib.asymmetric.keypair.openssl;
