/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.service;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link AsymmetricService} objects which are non thread-safe.
 */
public class BasicAsymmetricServiceFactory extends
        PropertiesConfiguredFactory<AsymmetricServiceAPI> implements
        ServiceFactory<AsymmetricServiceAPI> {

    /**
     * Default constructor. This factory will create services configured with
     * default values.
     */
    public BasicAsymmetricServiceFactory() {
        super(AsymmetricServiceAPI.class);
    }

    /**
     * This factory will create services configured with the given configuration
     * file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     */
    public BasicAsymmetricServiceFactory(final String path) {
        super(AsymmetricServiceAPI.class, path);
    }

    @Override
    protected AsymmetricServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new AsymmetricService(_path);
    }

    @Override
    protected AsymmetricServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new AsymmetricService();
    }

}
