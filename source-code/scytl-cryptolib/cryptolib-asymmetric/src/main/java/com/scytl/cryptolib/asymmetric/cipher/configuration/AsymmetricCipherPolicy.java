/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.asymmetric.cipher.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;
import com.scytl.cryptolib.symmetric.cipher.configuration.SymmetricCipherPolicy;
import com.scytl.cryptolib.symmetric.key.configuration.SecretKeyPolicy;

/**
 * Configuration policy for asymmetrically encrypting and decrypting.
 * <P>
 * Implementations must be immutable.
 */
public interface AsymmetricCipherPolicy extends SecureRandomPolicy,
        SecretKeyPolicy, SymmetricCipherPolicy {

    /**
     * Returns configuration for symmetric encryption.
     * 
     * @return configuration.
     */
    ConfigAsymmetricCipherAlgorithmAndSpec getAsymmetricCipherAlgorithmAndSpec();
}
