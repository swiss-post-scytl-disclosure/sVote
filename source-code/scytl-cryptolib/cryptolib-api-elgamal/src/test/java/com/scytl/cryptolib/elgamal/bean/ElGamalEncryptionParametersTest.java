/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.bean;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

public class ElGamalEncryptionParametersTest {

    private static BigInteger _p;

    private static BigInteger _q;

    private static BigInteger _g;

    private static ElGamalEncryptionParameters _elGamalEncryptionParameters;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _p = new BigInteger("23");

        _q = new BigInteger("11");

        _g = new BigInteger("2");

        _elGamalEncryptionParameters =
            new ElGamalEncryptionParameters(_p, _q, _g);
    }

    @Test
    public void givenParametersWhenToJsonThenExpectedValues()
            throws GeneralCryptoLibException {

        String jsonStr = _elGamalEncryptionParameters.toJson();

        ElGamalEncryptionParameters reconstructedElGamalEncryptionParameters =
            ElGamalEncryptionParameters.fromJson(jsonStr);

        String errorMsg =
            "The reconstructed ElGamal encryption parameters are not equal to the expected parameters";
        assertEquals(errorMsg, _elGamalEncryptionParameters,
            reconstructedElGamalEncryptionParameters);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenJsonStringWithLessComponentsThenRequiredThenExceptionThrown()
            throws GeneralCryptoLibException {

        String jsonStr = "{\"encryptionParams\":{\"q\":11,\"g\":2}}";

        ElGamalEncryptionParameters.fromJson(jsonStr);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenJsonStringWithCharactersThatAreNotDigitsThenExceptionThrown()
            throws GeneralCryptoLibException {

        String jsonStr =
            "{\"encryptionParams\":{\"p\":a,\"q\":11,\"g\":2}}";

        ElGamalEncryptionParameters.fromJson(jsonStr);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenJsonStringWithCharactersAndNumbersMixThenExceptionThrown()
            throws GeneralCryptoLibException {

        String jsonStr =
            "{\"encryptionParams\":{\"p\":1ab123,\"q\":11,\"g\":2}}";

        ElGamalEncryptionParameters.fromJson(jsonStr);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenJsonStringWithEmptyComponentThenExceptionThrown()
            throws GeneralCryptoLibException {

        String jsonStr = "{\"encryptionParams\":{\"p\":23,,\"g\":2}}";

        ElGamalEncryptionParameters.fromJson(jsonStr);
    }

    @Test(expected = GeneralCryptoLibException.class)
    public void givenJsonStringWithExtraCommaThenExceptionThrown()
            throws GeneralCryptoLibException {

        String jsonStr =
            "{\"encryptionParams\":{\"p\":23,\"q\":11,\"g\":2,}}";

        ElGamalEncryptionParameters.fromJson(jsonStr);
    }
}
