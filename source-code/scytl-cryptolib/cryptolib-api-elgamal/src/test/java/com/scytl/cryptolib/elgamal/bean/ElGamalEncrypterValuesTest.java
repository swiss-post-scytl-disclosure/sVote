/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.bean;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;

/**
 * Tests of ElGamalEncrypterValues.
 */
public class ElGamalEncrypterValuesTest {

    private static BigInteger _p;

    private static BigInteger _q;

    private static BigInteger _g;

    private static ZpSubgroup _group;

    private static ElGamalEncrypterValues _elGamalEncrypterValues;

    private static Exponent _r;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _p = new BigInteger("23");

        _q = new BigInteger("11");

        _g = new BigInteger("2");

        _group = new ZpSubgroup(_g, _p, _q);

        ElGamalComputationsValues values = buildComputationValues();

        _r = new Exponent(_q, new BigInteger("4"));

        _elGamalEncrypterValues = new ElGamalEncrypterValues(_r, values);
    }

    @Test
    public void testGetR() throws GeneralCryptoLibException {

        Exponent returnedR = _elGamalEncrypterValues.getR();

        Exponent expectedR = new Exponent(_q, new BigInteger("4"));

        String errorMsg = "The returned r was not the expected value";
        assertEquals(errorMsg, expectedR, returnedR);
    }

    @Test
    public void testGetValues() throws GeneralCryptoLibException {

        ElGamalComputationsValues returnedValues =
            _elGamalEncrypterValues.getComputationValues();

        ElGamalComputationsValues expectedValues =
            buildComputationValues();

        String errorMsg =
            "The returned encryption values were not the expected values";
        assertEquals(errorMsg, expectedValues.getValues(),
            returnedValues.getValues());
    }

    private static ElGamalComputationsValues buildComputationValues()
            throws GeneralCryptoLibException {

        ZpGroupElement ge1 = new ZpGroupElement(BigInteger.ONE, _group);
        ZpGroupElement ge2 =
            new ZpGroupElement(new BigInteger("2"), _group);
        List<ZpGroupElement> phis = new ArrayList<ZpGroupElement>();
        phis.add(ge1);
        phis.add(ge2);

        ZpGroupElement gamma = new ZpGroupElement(BigInteger.ONE, _group);

        ElGamalComputationsValues values =
            new ElGamalComputationsValues(gamma, phis);

        return values;
    }
}
