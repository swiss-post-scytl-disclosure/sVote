/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.bean;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;

/**
 * Tests of {@link Codec}.
 */
public class CodecTest {
    private ZpSubgroup group;

    @Before
    public void setUp() throws GeneralCryptoLibException {
        group = new ZpSubgroup(BigInteger.valueOf(2),
            BigInteger.valueOf(23), BigInteger.valueOf(11));
    }

    @Test
    public void testDecodePrivateKey() throws GeneralCryptoLibException {
        List<Exponent> exponents =
            asList(new Exponent(group.getQ(), BigInteger.valueOf(4)),
                new Exponent(group.getQ(), BigInteger.valueOf(5)));
        ElGamalPrivateKey expected =
            new ElGamalPrivateKey(exponents, group);
        byte[] bytes = Codec.encode(expected);
        ElGamalPrivateKey actual = Codec.decodePrivateKey(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void testDecodePublicKey() throws GeneralCryptoLibException {
        List<ZpGroupElement> elements =
            asList(new ZpGroupElement(BigInteger.valueOf(4), group),
                new ZpGroupElement(BigInteger.valueOf(8), group));
        ElGamalPublicKey expected = new ElGamalPublicKey(elements, group);
        byte[] bytes = Codec.encode(expected);
        ElGamalPublicKey actual = Codec.decodePublicKey(bytes);
        assertEquals(expected, actual);
    }
}
