/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.cryptoapi;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.cryptolib.elgamal.bean.ElGamalKeyPair;

/**
 * Defines the methods provided by ElGamal Key pair generators.
 */
public interface CryptoAPIElGamalKeyPairGenerator {

    /**
     * Generates an {@link com.scytl.cryptolib.elgamal.bean.ElGamalKeyPair} with
     * the specified Zp subgroup and number of keys.
     * <P>
     * The specified Zp subgroup must be an initialized and valid group.
     *
     * @param encryptionParameters
     *            The {@link ElGamalEncryptionParameters} that should should be
     *            used for generating the ElGamal key pair.
     * @param length
     *            The number of components that each key (the public key and the
     *            private key) should be composed of. This value must be 1 or
     *            more.
     * @return The generated ElGamal key pair, encapsulated within a
     *         {@link ElGamalKeyPair} object.
     * @throws GeneralCryptoLibException
     *             if the {@link ElGamalEncryptionParameters} object or the
     *             mathematical group that it contains is null, or if the
     *             specified length of the key pair to generate is less than 1.
     */
    ElGamalKeyPair generateKeys(
            final ElGamalEncryptionParameters encryptionParameters,
            final int length) throws GeneralCryptoLibException;
}
