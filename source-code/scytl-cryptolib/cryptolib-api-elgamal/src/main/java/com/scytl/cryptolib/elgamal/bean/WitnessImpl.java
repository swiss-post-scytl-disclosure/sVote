/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.bean;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.utils.validations.Validate;
import com.scytl.cryptolib.elgamal.cryptoapi.Witness;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

/**
 * Implementation of {@link Witness}, instances of this class are used during
 * the generation of certain Zero Knowledge Proofs (ZKPs).
 */
public class WitnessImpl implements Witness {

    private final Exponent _exponent;

    /**
     * Constructor.
     * 
     * @param exponent
     *            used to set in this Witness.
     * @throws GeneralCryptoLibException
     *             failed to create instance.
     */
    public WitnessImpl(final Exponent exponent)
            throws GeneralCryptoLibException {
        Validate.notNull(exponent, "Exponent");

        _exponent = exponent;
    }

    @Override
    public Exponent getExponent() {
        return _exponent;
    }
}
