/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.cryptoapi;

import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalComputationsValues;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;

/**
 * Defines the methods supported by a CryptoAPIElGamalDecrypter.
 */
public interface CryptoAPIElGamalDecrypter {

    /**
     * Decrypt a ciphertext.
     * <P>
     * The encrypted message parameter will be a list of group elements,
     * encapsulated within an {@link ElGamalComputationsValues} object.
     * <P>
     * The length of the received ciphertext (number of group elements contained
     * within it) must be equal to, or less than, the length of the private key
     * of this decrypter. If this condition is not met, then an exception will
     * be thrown.
     * 
     * @param ciphertext
     *            the encrypted message to be decrypted.
     * @param validateGroupMembership
     *            if true, validate that each element in {@code ciphertext} is a
     *            member of the mathematical group of the decrypter's ElGamal
     *            private key.
     * @return the decrypted message.
     * @throws GeneralCryptoLibException
     *             if the ciphetext is invalid.
     */
    List<ZpGroupElement> decrypt(ElGamalComputationsValues ciphertext,
            boolean validateGroupMembership)
            throws GeneralCryptoLibException;
}
