/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.cryptoapi;

import com.scytl.cryptolib.mathematical.groups.impl.Exponent;

/**
 * Defines methods exposed by the output of ElGamal encryption, which are used
 * during the generation of Zero Knowledge Proofs (ZKPs).
 */
public interface Witness {

    /**
     * Returns the exponent that was used during the encryption process.
     * @return the exponent.
     */
    Exponent getExponent();
}
