/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.bean;

import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.utils.validations.Validate;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.elgamal.cryptoapi.Witness;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;

/**
 * Class which encapsulates an 'r' value (random exponent) and a set of ElGamal
 * encryption values (a gamma value and a list of phi values).
 */
public final class ElGamalEncrypterValues implements Witness, Ciphertext {

    private final Exponent _r;

    private final ElGamalComputationsValues _computationsValues;

    /**
     * Creates an ElGamalEncrypterValues, setting the received {@link Exponent}
     * and {@link ElGamalComputationsValues}.
     * 
     * @param exponent
     *            the {@link Exponent}
     * @param values
     *            the {@link ElGamalComputationsValues}
     * @throws GeneralCryptoLibException
     *             if the {@link Exponent} or the
     *             {@link ElGamalComputationsValues} is null.
     */
    public ElGamalEncrypterValues(final Exponent exponent,
            final ElGamalComputationsValues values)
            throws GeneralCryptoLibException {

        Validate.notNull(exponent, "Random exponent");
        Validate.notNull(values, "ElGamal ciphertext");

        _r = exponent;
        _computationsValues = values;
    }

    public Exponent getR() {
        return _r;
    }

    public ElGamalComputationsValues getComputationValues() {
        return _computationsValues;
    }

    @Override
    public List<ZpGroupElement> getElements() {
        return _computationsValues.getValues();
    }

    @Override
    public Exponent getExponent() {
        return _r;
    }
}