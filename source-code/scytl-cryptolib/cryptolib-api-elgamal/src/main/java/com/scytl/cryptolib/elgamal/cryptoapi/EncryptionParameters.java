/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.cryptoapi;

import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;

/**
 * Represents a set of encryption Parameters.
 */
public interface EncryptionParameters {

    /**
     * Returns the mathematical group which comprises the p, q and g of this
     *         EncryptionParameters.
     * @return The mathematical group.
     */
    MathematicalGroup<?> getGroup();

}
