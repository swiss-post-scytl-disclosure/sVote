/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.cryptoapi;

import java.util.List;

import com.scytl.cryptolib.digital.envelope.bean.SecretKeyEncryption;

/**
 * Defines the API for a digital envelope. Instances of this API should only be
 * generated or opened through the digital envelope service interface.
 */
public interface CryptoAPIDigitalEnvelope {

    /**
     * Retrieves the symmetrically encrypted data of the digital envelope.
     * 
     * @return the symmetrically encrypted data.
     */
    byte[] getEncryptedData();

    /**
     * Retrieves the HMAC of the symmetrically encrypted data of the digital
     * envelope.
     * 
     * @return the HMAC of the symmetrically encrypted data.
     */
    byte[] getMac();

    /**
     * Retrieves the list of asymmetric encryptions of the concatenation of the
     * encryption and HMAC secret keys of the digital envelope.
     * 
     * @return the list of asymmetric encryptions of the concatenation of the
     *         encryption and HMAC secret keys.
     */
    List<SecretKeyEncryption> getSecretKeyEncryptions();
}
