/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.api.digital.envelope;

import java.security.PrivateKey;
import java.security.PublicKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.digital.envelope.cryptoapi.CryptoAPIDigitalEnvelope;

/**
 * Defines the API for creating and opening digital envelopes.
 */
public interface DigitalEnvelopeServiceAPI {

    /**
     * Creates a digital envelope, using one or more public keys, each of whose
     * corresponding private key can be used to open the envelope and retrieve
     * the data.
     * 
     * @param data
     *            the data to store in the digital envelope.
     * @param publicKeys
     *            one or more public keys, of type
     *            {@link java.security.PublicKey}, used to create the digital
     *            envelope.
     * @return the newly created digital envelope, as an object of type
     *         {@link CryptoAPIDigitalEnvelope}.
     * @throws GeneralCryptoLibException
     *             if the data or a public key is null or empty or if the
     *             digital envelope creation process fails.
     */
    CryptoAPIDigitalEnvelope createDigitalEnvelope(final byte[] data,
            final PublicKey... publicKeys) throws GeneralCryptoLibException;

    /**
     * Opens an existing digital envelope and retrieves the data that it
     * contains. The envelope can be opened with any private key corresponding
     * to a public key that was used to generate the envelope.
     * 
     * @param digitalEnvelope
     *            the digital envelope to open, as an object of type
     *            {@link CryptoAPIDigitalEnvelope}.
     * @param privateKey
     *            the private key, of type {@link java.security.PrivateKey},
     *            used to open the digital envelope.
     * @return the data contained in the digital envelope.
     * @throws GeneralCryptoLibException
     *             if the digital envelope is null, the private key is null or
     *             empty or the digital envelope opening process fails.
     */
    byte[] openDigitalEnvelope(final CryptoAPIDigitalEnvelope digitalEnvelope,
            final PrivateKey privateKey) throws GeneralCryptoLibException;
}
