/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.digital.envelope.bean;

import java.security.PublicKey;
import java.util.Arrays;

/**
 * Container class for the asymmetric encryption of the bit-wise concatenation
 * of the secret key used to symmetrically encrypt the data of a digital
 * envelope and the secret key used to generate an MAC for checking the
 * integrity of the symmetric encryption. The public key used to perform the
 * asymmetric encryption of the secret key concatenation is also included.
 */
public class SecretKeyEncryption {

    private final byte[] _encryptedSecretKeyConcatenation;

    private final PublicKey _publicKey;

    /**
     * Default constructor.
     * 
     * @param encryptedSecretKeyConcatenation
     *            the asymmetric encryption of the secret key concatenation,
     *            using the public key provided as input.
     * @param publicKey
     *            the public key, of type {@link java.security.PublicKey}, used
     *            to asymmetrically encrypt the secret key concatenation.
     */
    public SecretKeyEncryption(final byte[] encryptedSecretKeyConcatenation,
            final PublicKey publicKey) {

        _encryptedSecretKeyConcatenation =
            encryptedSecretKeyConcatenation.clone();

        _publicKey = publicKey;
    }

    public byte[] getEncryptedSecretKeyConcatenation() {

        return _encryptedSecretKeyConcatenation.clone();
    }

    public PublicKey getPublicKey() {

        return _publicKey;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + Arrays.hashCode(_encryptedSecretKeyConcatenation);
        result = prime * result
            + ((_publicKey == null) ? 0 : _publicKey.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SecretKeyEncryption other = (SecretKeyEncryption) obj;
        if (!Arrays.equals(_encryptedSecretKeyConcatenation,
            other._encryptedSecretKeyConcatenation)) {
            return false;
        }
        if (_publicKey == null) {
            if (other._publicKey != null) {
                return false;
            }
        } else if (!_publicKey.equals(other._publicKey)) {
            return false;
        }
        return true;
    }
}
