/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var MessageDigestService = require('./service');
var validator = require('./input-validator');

module.exports = {
  /**
   * Creates a new MessageDigestService object, which encapsulates a message
   * digiest service.
   *
   * @function newService
   * @global
   * @param {Object}
   *            [options] An object containing optional arguments.
   * @param {Policy}
   *            [options.policy=Default policy] The cryptographic policy to
   *            use.
   * @returns {MessageDigestService} The new MessageDigestService object.
   * @throws {Error}
   *             If the input data validation fails.
   * @example <caption> How to use a cryptographic policy that sets the
   *          message digest hash algorithm to <code>SHA512/224</code></caption>
   *
   * var cryptoPolicy = require('scytl-cryptopolicy'); var messageDigest =
   * require('scytl-messagedigest');
   *
   * var myPolicy = cryptoPolicy.newInstance();
   *
   * myPolicy.primitives.messageDigest.algorithm =
   * cryptoPolicy.options.primitives.messageDigest.algorithm.SHA512_224;
   *
   * var messageDigestService = messageDigest.newService({policy: myPolicy});
   */
  newService: function(options) {
    checkData(options);

    return new MessageDigestService(options);
  }
};

function checkData(options) {
  options = options || {};

  if (typeof options.policy !== 'undefined') {
    validator.checkIsObjectWithProperties(
        options.policy,
        'Cryptographic policy provided to message digest service');
  }
}
