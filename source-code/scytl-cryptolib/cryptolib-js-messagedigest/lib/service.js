/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var MessageDigester = require('./digester');
var cryptoPolicy = require('scytl-cryptopolicy');

module.exports = MessageDigestService;

/**
 * @class MessageDigestService
 * @classdesc The message digest service API. To instantiate this object, use
 * the method {@link newService}.
 * @hideconstructor
 * @param {Object}
 *            [options] An object containing optional arguments.
 * @param {Policy}
 *            [options.policy=Default policy] The cryptographic policy to use.
 */
function MessageDigestService(options) {
  options = options || {};

  var policy;
  if (options.policy) {
    policy = options.policy;
  } else {
    policy = cryptoPolicy.newInstance();
  }

  /**
   * Creates a new MessageDigester object for generating message digests.
   *
   * @function newDigester
   * @memberof MessageDigestService
   * @returns {MessageDigester} The new MessageDigester object.
   */
  this.newDigester = function() {
    return new MessageDigester(policy);
  };
}
