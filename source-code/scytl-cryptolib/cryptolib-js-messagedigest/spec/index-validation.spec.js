/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var messageDigest = require('../lib/index');

describe('The message digest module should be able to ...', function() {
  var digester_;
  var nonUint8Array_;

  beforeAll(function() {
    digester_ = messageDigest.newService().newDigester();
    nonUint8Array_ = 999;
  });

  describe('create a message digest service that should be able to ..', function() {
    it('throw an error when being created, using an invalid cryptographic policy',
       function() {
         var nonObject = 999;
         var emptyObject = {};

         expect(function() {
           Object.create(messageDigest.newService({policy: null}));
         }).toThrow();

         expect(function() {
           Object.create(messageDigest.newService({policy: nonObject}));
         }).toThrow();

         expect(function() {
           Object.create(messageDigest.newService({policy: emptyObject}));
         }).toThrow();
       });

    describe('create a message digester that should be able to', function() {
      it('throw an error when digesting, using invalid input data', function() {
        expect(function() {
          digester_.digest(null);
        }).toThrow();

        expect(function() {
          digester_.digest(nonUint8Array_);
        }).toThrow();
      });

      it('throw an error when updating, using invalid input data', function() {
        expect(function() {
          digester_.update();
        }).toThrow();

        expect(function() {
          digester_.update(undefined);
        }).toThrow();

        expect(function() {
          digester_.update(null);
        }).toThrow();

        expect(function() {
          digester_.update(nonUint8Array_);
        }).toThrow();
      });
    });
  });
});
