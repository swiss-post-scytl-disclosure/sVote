# Message Digest Module

This module defines the message digest API for Scytl's JavaScript cryptographic library. The starting point for using this module is the instantiation of a message digest service.

## How to view, build and test the source code of this module

Start by git cloning the project `comm/scytl-cryptolib` from `Stash`.

The source code of this module will be found in the directory `scytl-cryptolib/cryptolib-js-messagedigest/lib`.

To build this module, change to the directory `scytl-cryptolib/cryptolib-js-messagedigest` and do the following:

```java
npm install
--or--
mvn clean install -DskipTests
```
The unit tests of this module will be found in the directory `scytl-cryptolib/cryptolib-js-messagedigest/spec`. To run the tests, change to the directory `scytl-cryptolib/cryptolib-js-messagedigest` and do the following:

```javascript
karma test
--or--
mvn test
```
To only run a chosen test suite, add an `f` in front of the corresponding `describe` statement. For example:

```javascript
fdescribe('create a message digester that should be able to', function()
...
```
To only run a chosen test, add an `f` in front of the corresponding `it` statement. For example:

```javascript
fit('generate digests that are deterministic', function() {
...
```
**Note:** To build and test the entire `scytl-cryptolib` project, change to the directory `scytl-cryptolib` and do the following:

```javascript
mvn clean install
```
## How to generate the JSDoc for this module

To generate the JSDoc for this module, change to the directory `scytl-cryptolib/cryptolib-js-messagedigest`, build the module (if not already done) and do the following:

```javascript
node_modules/.bin/jsdoc lib
```
This will generate a file called `out`. Double click on the file `out/index.html` to view the JSDoc.

## How to `npm` install this module

To `npm` install this module in standalone mode, do the following:

```javascript
npm install --registry https://nexus.scytl.net/content/groups/public-npm/ scytl-messagedigest
```
To `npm` install this module as a dependency of your own module, do the following:

1. Add the following lines to the `dependencies` section of your module's `package.json` file.
    ```javascript
    "scytl-bitwise": "^2.1.0",
    "scytl-codec": "^2.1.0",
    "scytl-cryptopolicy": "^2.1.0",
    "scytl-messagedigest": "^2.1.0",
    ```
2. Make sure that the `.npmrc` file of your module contains the following line.
    ```javascript
    registry=https://nexus.scytl.net/content/groups/public-npm/
    ```
3. Install all dependencies.
    ```javascript
    npm install
    ```
## How to instantiate a message digest service

The following example shows the default way to create a new instance of a message digest service.

```javascript
var messageDigest = require('scytl-messagedigest');

var messageDigestService = messageDigest.newService();
```
The message digest service accepts an optional input argument that can be used to override the default cryptographic policy used
for message digest operations. For example, to set the message digest hashing algorithm to `SHA512/224`, one could do the following.

```javascript
var cryptoPolicy = require('scytl-cryptopolicy');
var messageDigest = require('scytl-messagedigest');

var myPolicy = cryptoPolicy.newInstance();

myPolicy.primitives.messageDigest.algorithm =
  cryptoPolicy.options.primitives.messageDigest.algorithm.SHA512_224;

var messageDigestService = messageDigest.newService({policy: myPolicy});
```
**Note** It is desirable to use the property `CryptographicPolicy.options` to override the default policy, as shown above, in order to ensure that one is using an option that is allowed by the cryptographic policy.

## How to instantiate a message digester

The following example shows how to create a new message digester. This digester will be initialized internally.

```javascript
var digester = messageDigestService.newDigester();
```
## How to generate a message digest

The following examples show how to generate a message digest from some data. The data can be provided as a `string` or as a byte array, wrapped in a `Uint8Array` object. The output will be in the form of a byte array, wrapped in a `Uint8Array` object. The digester will be automatically reinitialized by the method `digest` after the digest has been generated.

```javascript
var codec = require('scytl-codec');
...

var digest = digester.digest('myData');
var digest = digester.digest(new Uint8Array([0x01, 0x02, 0x03]));
```
## How to generate a message digest from multiple data parts

The following example shows how to generate a message digest from multiple data parts by making a call to the `update` method for each part to add. The digester will be automatically reinitialized by the method `digest` after the digest has been generated.

```javascript
var dataParts = ['myData1', 'myData1', 'myData3'];
...

for (var i = 0; i < dataParts.length; i++) {
  digester.update(dataParts[i]);
}
var digest = digester.digester();

--or--

for (var i = 0; i < (dataParts.length-1); i++) {
  digester.update(dataParts[i]);
}
var digest = digester.digester('myData3');
```
The following example shows how to write the calls shown above in a single line of code, using method chaining:

```javascript
var digest = digester.update('myData1').update('myData2').update('myData3').digest();

--or--

var digest = digester.update(data1).update(data2).digest(data3);
```
