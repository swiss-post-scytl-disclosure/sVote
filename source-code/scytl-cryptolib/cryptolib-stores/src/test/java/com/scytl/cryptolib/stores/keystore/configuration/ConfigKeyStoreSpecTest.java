/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.stores.keystore.configuration;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;

public class ConfigKeyStoreSpecTest {

    @Test
    public void givenKeyStoreSpecForSunThenProviderIsSun() {

        assertProvider(Provider.SUN, ConfigKeyStoreSpec.SUN);
    }

    private void assertProvider(final Provider expectedProvider,
            final ConfigKeyStoreSpec configKeyStoreSpec) {

        Assert.assertEquals(
            "Provider name does not have the expected value",
            expectedProvider, configKeyStoreSpec.getProvider());
    }

}
