/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.stores.keystore.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.configuration.Provider;
import com.scytl.cryptolib.commons.constants.Constants;

public class KeyStorePolicyFromPropertiesTest {

    private static final String INVALID_PATH = "Invalid path";

    private static KeyStorePolicyFromProperties _keyStorePolicyFromProperties;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _keyStorePolicyFromProperties =
            new KeyStorePolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @Test
    public void givenKeyStorePolicyWhenGetSpecThenExpectedValues() {

        ConfigKeyStoreSpec config =
            _keyStorePolicyFromProperties.getKeyStoreSpec();

        assertEquals(Provider.SUN, config.getProvider());
    }

    @Test(expected = CryptoLibException.class)
    public void whenPolicyFromIncorrectPathThenException()
            throws GeneralCryptoLibException {

        new KeyStorePolicyFromProperties(INVALID_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyInvalidLabels()
            throws GeneralCryptoLibException {

        new KeyStorePolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");

    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyBlankLabels()
            throws GeneralCryptoLibException {

        new KeyStorePolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }
}
