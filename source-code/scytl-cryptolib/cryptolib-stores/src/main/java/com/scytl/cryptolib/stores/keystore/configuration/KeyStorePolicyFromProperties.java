/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.stores.keystore.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;

/**
 * Implementation of the {@link KeyStorePolicy} interface, which reads the key
 * store cryptographic policy from a properties file.
 * <p>
 * Instances of this class are immutable.
 */
public class KeyStorePolicyFromProperties implements KeyStorePolicy {

    private final ConfigKeyStoreSpec _keyStoreSpec;

    /**
     * Default constructor.
     *
     * @param path
     *            the path of the key store cryptographic policy properties
     *            file.
     * @throws CryptoLibException
     *             if the properties cannot be retrieved.
     */
    public KeyStorePolicyFromProperties(final String path)
            throws CryptoLibException {

        try {
            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _keyStoreSpec =
                ConfigKeyStoreSpec.valueOf(helper
                    .getNotBlankPropertyValue("stores.keystore"));
        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigKeyStoreSpec getKeyStoreSpec() {

        return _keyStoreSpec;
    }
}
