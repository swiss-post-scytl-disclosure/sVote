/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.stores.service;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.stores.StoresServiceAPI;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link StoresService} objects which are non thread-safe.
 */
public class BasicStoresServiceFactory extends
        PropertiesConfiguredFactory<StoresServiceAPI> {
    /**
     * Default constructor. This factory will create services configured with
     * default values.
     */
    public BasicStoresServiceFactory() {
        super(StoresServiceAPI.class);
    }

    /**
     * This factory will create services configured with the given configuration
     * file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     */
    public BasicStoresServiceFactory(final String path) {
        super(StoresServiceAPI.class, path);
    }

    @Override
    protected StoresServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new StoresService(_path);
    }

    @Override
    protected StoresServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new StoresService();
    }

}
