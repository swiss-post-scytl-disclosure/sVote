/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.stores.keystore.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum which specifies the supported cryptographic properties when requesting
 * an instance of a key store.
 * <p>
 * Instances of this enum are immutable.
 */
public enum ConfigKeyStoreSpec {

    SUN(Provider.SUN), BC(Provider.BOUNCY_CASTLE), DEFAULT(
            Provider.DEFAULT);

    private final Provider _provider;

    ConfigKeyStoreSpec(final Provider provider) {
        _provider = provider;
    }

    public Provider getProvider() {
        return _provider;
    }
}
