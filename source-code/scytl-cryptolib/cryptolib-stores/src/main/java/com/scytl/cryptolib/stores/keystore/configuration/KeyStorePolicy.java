/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.stores.keystore.configuration;

/**
 * Interface which specifies the methods that should be implemented by a
 * specific type of {@link KeyStorePolicy}.
 */
public interface KeyStorePolicy {

    /**
     * Retrieves the {@link ConfigKeyStoreSpec} that encapsulates the key store
     * specification.
     * 
     * @return the {@link ConfigKeyStoreSpec}.
     */
    ConfigKeyStoreSpec getKeyStoreSpec();
}
