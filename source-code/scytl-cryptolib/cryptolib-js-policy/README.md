# Cryptographic Policy Module

This module defines the cryptographic policy configuration for Scytl's JavaScript cryptographic library.

## How to view, build and test the source code of this module

Start by git cloning the project `comm/scytl-cryptolib` from `Stash`.

The source code of this module will be found in the directory `scytl-cryptolib/cryptolib-js-policy/lib`.

To build this module, change to the directory `scytl-cryptolib/cryptolib-js-policy` and do the following:

```java
npm install
--or--
mvn clean install -DskipTests
```
The unit tests of this module will be found in the directory `scytl-cryptolib/cryptolib-js-policy/spec`. To run the tests, change to the directory `scytl-cryptolib/cryptolib-js-policy` and do the following:

```javascript
karma test
--or--
mvn test
```
To only run a chosen test suite, add an `f` in front of the corresponding `describe` statement. For example:

```javascript
fdescribe('create a cryptographic policy instance that should be able to ..',
...
```
To only run a chosen test, add an `f` in front of the corresponding `it` statement. For example:

```javascript
fit('provide access to policy options', function() {
...
```
**Note:** To build and test the entire `scytl-cryptolib` project, change to the directory `scytl-cryptolib` and do the following:

```javascript
mvn clean install
```
## How to generate the JSDoc for this module

To generate the JSDoc for this module, change to the directory `scytl-cryptolib/cryptolib-js-policy`, build the module (if not already done) and do the following:

```javascript
node_modules/.bin/jsdoc lib
```
This will generate a file called `out`. Double click on the file `out/index.html` to view the JSDoc.

## How to `npm` install this module

To `npm` install this module in standalone mode, do the following:

```javascript
npm install --registry https://nexus.scytl.net/content/groups/public-npm/ scytl-cryptopolicy
```
To `npm` install this module as a dependency of your own module, do the following:

1. Add the following line to the `dependencies` section of your module's `package.json` file.
    ```javascript
    "scytl-cryptopolicy": "^2.1.0",
    ```
2. Make sure that the `.npmrc` file of your module contains the following line.
    ```javascript
    registry=https://nexus.scytl.net/content/groups/public-npm/
    ```
3. Install all dependencies.
    ```javascript
    npm install
    ```
## How to instantiate the default cryptographic policy

The following example shows how to create an instance of the default cryptographic policy.

```javascript
var cryptoPolicy = require('scytl-cryptopolicy');

var policy = cryptoPolicy.newInstance();
```
This is the default policy that is used by all of the cryptographic services, if no customized policy is provided to them when they are instantiated.

The configuration for the default cryptographic policy can be found in the file `default-policy.json`

## How to instantiate and use a customized cryptographic policy

The following example shows how to use one's own customized cryptographic policy to instantiate an asymmetric cryptography service. The policy is provided as a JSON object.

```javascript
var cryptoPolicy = require('scytl-cryptopolicy');
var asymmetric = require('scytl-cryptopolicy');

// Create a customized policy (you can use the default cryptographic policy as a template for this).
var myPolicy = cryptoPolicy.newInstance(<policy as JSON object>);

// Instantiate an asymmetric cryptography service object, providing the customized policy as input.
var asymmetricService = asymmetrics.newService({policy: myPolicy});

// Perform asymmetric cryptographic operations with the service object ...
```
The following example shows how to create a customized cryptographic policy for the asymmetric cryptography service that uses the 'RSA' signer algorithm, the 'SHA-512/224' hash algorithm and the 'F4' public exponent for digital signature operations, but uses default values for the other asymmetric cryptographic operations.

```javascript
var cryptoPolicy = require('scytl-cryptopolicy');
var asymmetric = require('scytl-cryptopolicy');

// Create a new instance of the default cryptographic policy.
var myPolicy = cryptoPolicy.newInstance();

// Update the policy for use with digital signature operations.
myPolicy.asymmetric.signer = {
    algorithm: cryptoPolicy.options.asymmetric.signer.algorithm.RSA,
    hashAlgorithm: cryptoPolicy.options.asymmetric.signer.hashAlgorithm.SHA512_224,
    publicExponent: cryptoPolicy.options.asymmetric.signer.publicExponent.F4
};

// Instantiate an asymmetric cryptography service object, providing the customized policy as input.
var asymmetricService = asymmetric.newService({policy: myPolicy});

// Perform asymmetric cryptographic operations with the service object ...
```
As shown above, a policy is customized by overriding one or more of its default values with the options available via the `options` property of the `Policy` object. It is highly recommended to use this nomenclature, rather than hard coded values, to override the default values, in order to ensure that one is only using values that have been approved by the research and security department. An exception to this rule is for testing purposes. For example, if one is testing key pair generation, one can increase the test speed by lowering the key size to a value smaller than that allowed by the cryptographic policy, while still sufficiently testing the desired functionality.

The following example shows how to create a customized cryptographic policy for the asymmetric cryptography service that uses the 'SHA-512/224' hash algorithm for digital signature operations but uses default values for the rest of the policy.

```javascript
var cryptoPolicy = require('scytl-cryptopolicy');
var asymmetric = require('scytl-cryptopolicy');

// Create a new instance of the default cryptographic policy.
var myPolicy = cryptoPolicy.newInstance();

// Update the policy for use with digital signature operations.
myPolicy.asymmetric.signer.hashAlgorithm = cryptoPolicy.options.asymmetric.signer.hashAlgorithm.SHA512_224;

// Instatiate an asymmetric cryptography service object, providing the customized policy as input.
var asymmetricService = asymmetric.newService({policy: myPolicy});

// Perform asymmetric cryptographic operations with the service object ...
