/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var objs = require('../lib/objs');

describe('The object manipulation library', function() {
  it('should allow lean copies', function() {
    var obj = {};
    expect(obj.hasOwnProperty).toBeDefined();
    var leanObj = objs.leanCopy(obj);
    expect(leanObj.hasOwnProperty).not.toBeDefined();
  });

  // Only run in supporting platforms
  if (Object.freeze) {
    it('should freeze objects', function() {
      var obj = objs.freeze({property: true});
      expect(function() {
        obj.property = false;
      }).toThrow();
    });
  }

});