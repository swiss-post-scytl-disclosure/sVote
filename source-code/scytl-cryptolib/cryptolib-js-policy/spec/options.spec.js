/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var options = require('../lib/options');

describe('The policy options object', function() {
  it('should be immutable', function() {
    expect(function() {
      options.newProperty = '';
    }).toThrowError(/Attempted to assign to readonly property/);
  });

  it('should provide options for existing policies', function() {
    expect(function() {
      var value = options.asymmetric.keyPair.encryption;
      expect(value).toBeDefined();
    }).not.toThrow();

    expect(function() {
      var value = options.madeUpSection.subSection.ToMakeItCrash;
      expect(value).toBeDefined();
    }).toThrow();
  });

});
