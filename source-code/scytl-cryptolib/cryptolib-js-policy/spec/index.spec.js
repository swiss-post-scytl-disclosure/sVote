/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var cryptoPolicy = require('../lib/index');

describe('The cryptographic policy module should be able to ...', function() {
  describe(
      'create a cryptographic policy instance that should be able to ..',
      function() {
        it('provide access to policy options', function() {
          expect(function() {
            // It's `strict`, so it will fail if any of the values is not
            // present in the options object.
            var policy = cryptoPolicy.newInstance();
            var value = policy.asymmetric.keyPair.encryption;
            expect(value).toBeDefined();
          }).not.toThrow();
        });

        it('restore default settings', function() {
          // Load the policy
          var policy = cryptoPolicy.newInstance();
          // Check that the policy has default values.
          expect(policy.asymmetric.keyPair.encryption.algorithm).toEqual('RSA');
          // Change a value.
          policy.asymmetric.keyPair.encryption.algorithm = 'RSE';
          // Check that the value has changed.
          expect(policy.asymmetric.keyPair.encryption.algorithm).toEqual('RSE');
          // Get a new default policy.
          policy = cryptoPolicy.newInstance();
          // Check that the policy has default values again.
          expect(policy.asymmetric.keyPair.encryption.algorithm).toEqual('RSA');
        });

        it('provide access to policy options', function() {
          // Load the policy
          var policy = cryptoPolicy.newInstance();

          expect(function() {
            policy.asymmetric.keyPair.encryption.algorithm =
                cryptoPolicy.options.asymmetric.keyPair.encryption.algorithm
                    .RSA;
          }).not.toThrow();
        });

        it('allow overriding of the default policy', function() {
          var otherValue = 'XYZ';
          // Load the policy
          var policy = cryptoPolicy.newInstance({
            asymmetric: {cipher: {algorithm: {name: otherValue}}},
            newOption: otherValue
          });

          expect(policy.asymmetric.cipher.algorithm.name).toEqual(otherValue);

          expect(policy.newOption).toBeDefined();
          expect(policy.newOption).toEqual(otherValue);
        });

        it('throw an error when being created with an invalid policy',
           function() {
             expect(function() {
               Object.create(cryptoPolicy.newInstance(null));
             }).toThrow();

             expect(function() {
               Object.create(cryptoPolicy.newInstance('non object'));
             }).toThrow();

             expect(function() {
               Object.create(cryptoPolicy.newInstance({}));
             }).toThrow();
           });
      });
});
