/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var objs = require('./objs');

// The default policy is created with a null prototype and then frozen, so that
// it cannot be manipulated (accidentally or otherwise).
var defaultPolicy_ = require('./default-policy.json');
defaultPolicy_ = objs.freeze(objs.leanCopy(defaultPolicy_));

module.exports = CryptographicPolicy;

/**
 * @class CryptographicPolicy
 * @classdesc The cryptographic policy API. To instantiate this object, use the
 * method {@link newInstance}.
 * @hideconstructor
 * @param {Object}
 *            [policy=Default policy] The cryptographic policy to use, as a JSON
 *            object.
 */
function CryptographicPolicy(policy) {
  // Get a fresh copy of the default policy.
  objs.leanCopy(defaultPolicy_, this);

  // If there is a policy as a constructor argument, merge it with the default
  // policy. But first make sure the object is lean.
  if (typeof policy === 'object') {
    policy = objs.leanCopy(policy);
    objs.leanCopy(objs.merge(this, policy), this);
  }
}
