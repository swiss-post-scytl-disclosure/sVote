/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

/**
 * Input data validation utility for this module. Only intended for internal
 * use.
 */
module.exports = {
  /**
   * Checks if a value is defined.
   *
   * @function checkIsDefined
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is undefined.
   */
  checkIsDefined: function(value, label) {
    if (typeof value === 'undefined') {
      throw new TypeError(label + ' is undefined.');
    }
  },

  /**
   * Checks if a value is not null.
   *
   * @function checkIsNotNull
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is null.
   */
  checkIsNotNull: function(value, label) {
    if (value === null) {
      throw new TypeError(label + ' is null.');
    }
  },

  /**
   * Checks if a value is defined and not null.
   *
   * @function checkIsDefinedAndNotNull
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not defined or it is null.
   */
  checkIsDefinedAndNotNull: function(value, label) {
    this.checkIsDefined(value, label);
    this.checkIsNotNull(value, label);
  },

  /**
   * Checks if a value is an object.
   *
   * @function checkIsObject
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not an object.
   */
  checkIsObject: function(value, label) {
    if (typeof value !== 'object') {
      throw new TypeError(label + ' is not an object.');
    }
  },

  /**
   * Checks if a value is an object and has properties.
   *
   * @function checkIsObjectWithProperties
   * @private
   * @param {Object}
   *            value The value to check.
   * @param {string}
   *            label The error message label for the value.
   * @throws {TypeError}
   *             If the value is not an object or it has no properties.
   */
  checkIsObjectWithProperties: function(value, label) {
    this.checkIsDefinedAndNotNull(value, label);

    this.checkIsObject(value, label);

    if (!Object.getOwnPropertyNames(value).length) {
      throw new Error(label + ' does not have any properties.');
    }
  }
};
