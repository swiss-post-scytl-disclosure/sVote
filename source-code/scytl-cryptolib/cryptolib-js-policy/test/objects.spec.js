/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var expect = require('chai').expect;
var objects = require('../lib/objects');

describe('The object manipulation library', function() {

  it('should allow lean copies', function() {
    var obj = {
      aFunction: function() {}
    };
    expect(obj.hasOwnProperty).to.exist;
    var leanObj = objects.leanCopy(obj);
    expect(leanObj.hasOwnProperty).to.not.exist;
    expect(leanObj.aFunction).to.not.exist;
  });

  it('should freeze objects', function() {
    var obj = objects.freeze({property: true});
    expect(function() {
      obj.property = false;
    }).to.throw();
  });

});
