/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, mocha:true */
'use strict';

var expect = require('chai').expect;
var Policy = require('../lib/index');

describe('The policy object', function() {

  it('should provide static access to policy options', function() {
    expect(function() {
      // It's `strict`, so it will fail if any of the values is not present in
      // the options object.
      Policy.options.asymmetric.keypair.encryption;
    }).not.to.throw();
  });

  it('should be able to restore default settings', function() {
    // Load the policy
    var policy = new Policy();
    // Check that the policy has default values.
    expect(policy.asymmetric.keypair.encryption.algorithm).to.equal('RSA');
    // Change a value.
    policy.asymmetric.keypair.encryption.algorithm = 'RSE';
    // Check that the value has changed.
    expect(policy.asymmetric.keypair.encryption.algorithm).to.equal('RSE');
    // Get a new default policy.
    policy = new Policy();
    // Check that the policy has default values again.
    expect(policy.asymmetric.keypair.encryption.algorithm).to.equal('RSA');
  });


  it('should provide access to policy options', function() {
    // Load the policy
    var policy = new Policy();
    expect(function() {
      policy.asymmetric.keypair.encryption.algorithm =
          Policy.options.asymmetric.keypair.encryption.algorithm.RSA;
    }).not.to.throw();
  });

  it('should allow overriding of the default policy', function() {
    var otherValue = 'XYZ';
    // Load the policy
    var policy = new Policy({
      asymmetric: {cipher: {algorithm: {name: otherValue}}},
      newOption: otherValue
    });

    expect(policy.asymmetric.cipher.algorithm.name).to.equal(otherValue);
    
    expect(policy.newOption).to.equal(otherValue);
  });
});
