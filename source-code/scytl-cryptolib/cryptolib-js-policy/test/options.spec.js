/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true, jasmine:true */
'use strict';

var expect = require('chai').expect;
var options = require('../lib/options');

describe('The policy options object', function() {

  it('should be immutable', function() {
    expect(function() {
      options.newProperty = '';
    }).to.throw(/Can\'t add property.*is not extensible/);
  });

  it('should provide options for existing policies', function() {
    expect(function() {
      var value = options.asymmetric.keypair.encryption;
    }).not.to.throw();

    expect(function() {
      var value = options.madeUpSection.subSection.ToMakeItCrash;
    }).to.throw();
  });

});
