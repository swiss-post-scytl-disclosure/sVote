/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.service;

import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.api.symmetric.SymmetricServiceAPI;
import com.scytl.cryptolib.commons.concurrent.ServiceFactoryHelper;

/**
 * This class sets up default factories of {@link SymmetricServiceAPI} objects.
 */
public final class SymmetricServiceFactoryHelper {

    /**
     * This is a helper class. It cannot be instantiated.
     */
    private SymmetricServiceFactoryHelper() {
    }

    /**
     * Retrieves a new factory of non thread-safe services.
     * <p>
     * Default factory is {@link BasicSymmetricServiceFactory}.
     * 
     * @param params
     *            list of parameters used in the creation of the default
     *            factory.
     * @return the new factory.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<SymmetricServiceAPI> getInstance(
            final Object... params) {
        return ServiceFactoryHelper.get(
            BasicSymmetricServiceFactory.class, params);

    }

    /**
     * Retrieves a new factory of thread-safe services.
     * <p>
     * Default factory is {link {@link PollingSymmetricServiceFactory}.
     * 
     * @param params
     *            list of parameters used in the creation of the default
     *            factory.
     * @return the new factory of thread-safe services.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<SymmetricServiceAPI> getFactoryOfThreadSafeServices(
            final Object... params) {
        return ServiceFactoryHelper.get(
            PollingSymmetricServiceFactory.class, params);
    }
}
