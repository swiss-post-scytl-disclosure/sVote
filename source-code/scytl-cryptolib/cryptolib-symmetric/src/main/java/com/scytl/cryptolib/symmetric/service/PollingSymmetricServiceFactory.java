/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.service;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.symmetric.SymmetricServiceAPI;
import com.scytl.cryptolib.commons.concurrent.PooledProxiedServiceFactory;

/**
 * This factory creates thread-safe services. Thread-safe services proxy all
 * requests to a pool of non thread-safe service instances.
 */
public class PollingSymmetricServiceFactory extends
        PooledProxiedServiceFactory<SymmetricServiceAPI> {

    /**
     * Constructor that uses default values.
     */
    public PollingSymmetricServiceFactory() {
        super(new BasicSymmetricServiceFactory(),
            new GenericObjectPoolConfig());
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param path
     *            the path where the properties file is located.
     * @param poolConfig
     *            the pool configuration.
     */
    public PollingSymmetricServiceFactory(final String path,
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicSymmetricServiceFactory(path), poolConfig);
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingSymmetricServiceFactory(
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicSymmetricServiceFactory(), poolConfig);
    }
}
