/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.mac.configuration;

/**
 * Configuration policy for creating a MAC.
 * <P>
 * Implementations must be immutable.
 */
public interface MacPolicy {

    /**
     * Returns the {@link ConfigMacAlgorithmAndProvider} that should be used
     *         when creating a MAC from some data.
     * @return a provider.
     */
    ConfigMacAlgorithmAndProvider getMacAlgorithmAndProvider();

}
