/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.factory;

import javax.crypto.SecretKey;

/**
 * Defines the methods supported by any secret key generators.
 */
public interface CryptoSecretKeyGenerator {

    /**
     * Creates a {@link SecretKey} object.
     * <P>
     * The attributes of the returned {@link SecretKey} depend on the attributes
     * of the generator that is used to create the key.
     * 
     * @return A newly created secret key.
     */
    SecretKey genSecretKey();
}
