/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.configuration;

import com.scytl.cryptolib.symmetric.key.constants.SecretKeyConstants;

/**
 * Enum which defines the supported secret keys. A secret key is a key that can
 * be used for symmetric encryption by a particular algorithm.
 * <P>
 * Each enum type defines the following attributes:
 * <ul>
 * <li>Algorithm name.</li>
 * <li>Key length.</li>
 * <li>Provider.</li>
 * </ul>
 */
public enum ConfigSecretKeyAlgorithmAndSpec {

    AES_128_BC(SecretKeyConstants.AES_ALG, 128,
            SecretKeyProvider.BOUNCY_CASTLE),

    AES_128_SUNJCE(SecretKeyConstants.AES_ALG, 128,
            SecretKeyProvider.SUNJCE);

    private final String _algorithm;

    private final int _keyLength;

    private final String _provider;

    private ConfigSecretKeyAlgorithmAndSpec(final String algorithm,
            final int keyLength, final SecretKeyProvider provider) {

        _algorithm = algorithm;
        _keyLength = keyLength;
        _provider = provider.getProvider();
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public int getKeyLength() {
        return _keyLength;
    }

    public String getProvider() {
        return _provider;
    }
}
