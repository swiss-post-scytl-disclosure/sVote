/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.factory;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * A generator of secret keys to be used for encryption.
 * <P>
 * Instances of this class are immutable.
 */
public final class CryptoSecretKeyGeneratorForEncryption implements
        CryptoSecretKeyGenerator {

    private final KeyGenerator _secretKeyGenerator;

    CryptoSecretKeyGeneratorForEncryption(
            final KeyGenerator secretKeyGenerator) {

        _secretKeyGenerator = secretKeyGenerator;
    }

    @Override
    public SecretKey genSecretKey() {
        return _secretKeyGenerator.generateKey();
    }
}
