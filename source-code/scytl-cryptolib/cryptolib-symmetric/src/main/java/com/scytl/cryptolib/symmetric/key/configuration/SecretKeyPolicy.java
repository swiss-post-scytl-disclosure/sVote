/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.configuration;

/**
 * Defines the methods that must be implemented by any secret key policy.
 */
public interface SecretKeyPolicy {

    /**
     * Returns a {@link com.scytl.cryptolib.symmetric.key.configuration.ConfigSecretKeyAlgorithmAndSpec}
     *         which specifies the configuration values to be used when creating
     *         a secret key.
     * @return a configuration values.
     */
    ConfigSecretKeyAlgorithmAndSpec getSecretKeyAlgorithmAndSpec();
}
