/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum representing a set of parameters that can be used when requesting a
 * symmetric cipher.
 * <P>
 * These parameters are:
 * <ol>
 * <li>An algorithm/mode/padding.</li>
 * <li>An initialization vector length in bits.</li>
 * <li>An authentication tag length in bits.</li>
 * <li>A provider.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigSymmetricCipherAlgorithmAndSpec {

    AESwithGCMandNOPADDING_96_128_BC("AES/GCM/NoPadding", 96, 128,
            Provider.BOUNCY_CASTLE),

    AESwithGCMandNOPADDING_96_128_DEFAULT("AES/GCM/NoPadding", 96, 128,
            Provider.DEFAULT);

    private final String _algorithmAndModeAndPadding;

    private final int _initVectorBitLength;

    private final int _authTagBitLength;

    private final Provider _provider;

    private ConfigSymmetricCipherAlgorithmAndSpec(
            final String algorithmModePadding,
            final int initVectorBitLength, final int authTagBitLength,
            final Provider provider) {

        _algorithmAndModeAndPadding = algorithmModePadding;
        _initVectorBitLength = initVectorBitLength;
        _authTagBitLength = authTagBitLength;
        _provider = provider;
    }

    public String getAlgorithmAndModeAndPadding() {

        return _algorithmAndModeAndPadding;
    }

    public int getInitVectorBitLength() {

        return _initVectorBitLength;
    }

    public int getAuthTagBitLength() {

        return _authTagBitLength;
    }

    public Provider getProvider() {

        return _provider;
    }
}
