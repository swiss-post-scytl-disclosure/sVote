/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;

/**
 * Configuration policy for symmetrically encrypting and decrypting.
 * <P>
 * Implementations must be immutable.
 */
public interface SymmetricCipherPolicy extends SecureRandomPolicy {

    /**
     * Provides configuration for symmetrically encrypting and decrypting.
     * @return configuration.
     */
    ConfigSymmetricCipherAlgorithmAndSpec getSymmetricCipherAlgorithmAndSpec();

}
