/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;

/**
 * Defines all of the methods that must be supported by a symmetric key.
 */
public interface SymmetricKeyPolicy extends SecretKeyPolicy,
        HmacSecretKeyPolicy, SecureRandomPolicy {

}
