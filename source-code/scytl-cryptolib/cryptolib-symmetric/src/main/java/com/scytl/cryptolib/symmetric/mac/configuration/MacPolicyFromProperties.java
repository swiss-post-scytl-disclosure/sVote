/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.mac.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;

/**
 * Implementation of the {@link MacPolicy} interface, which reads values from a
 * properties input.
 * <P>
 * Instances of this class are immutable.
 */
public class MacPolicyFromProperties implements MacPolicy {

    private final ConfigMacAlgorithmAndProvider _macAlgorithmAndProvider;

    /**
     * Constructs an object that will provide properties from the specified
     * path.
     * @param path
     *            The path where the properties are read from.
     * @throws CryptoLibException
     *            if path is invalid or properties are invalid.
     */
    public MacPolicyFromProperties(final String path) {

        try {

            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _macAlgorithmAndProvider =
                ConfigMacAlgorithmAndProvider.valueOf(helper
                    .getNotBlankPropertyValue("symmetric.mac"));

        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigMacAlgorithmAndProvider getMacAlgorithmAndProvider() {

        return _macAlgorithmAndProvider;
    }

}
