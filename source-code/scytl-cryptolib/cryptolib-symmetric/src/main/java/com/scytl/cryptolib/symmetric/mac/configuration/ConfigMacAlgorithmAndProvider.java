/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.mac.configuration;

import com.scytl.cryptolib.commons.configuration.Provider;

/**
 * Enum representing a set of parameters that can be used when requesting a MAC
 * generator.
 * <P>
 * These parameters are:
 * <ol>
 * <li>An algorithm.</li>
 * <li>A provider.</li>
 * </ol>
 * <P>
 * Instances of this enum are immutable.
 */
public enum ConfigMacAlgorithmAndProvider {

    HMACwithSHA256_SUN("HmacSHA256", Provider.SUN_JCE),

    HMACwithSHA256_BC("HmacSHA256", Provider.BOUNCY_CASTLE),

    HMACwithSHA256_DEFAULT("HmacSHA256", Provider.DEFAULT);

    private final String _algorithm;

    private final Provider _provider;

    private ConfigMacAlgorithmAndProvider(final String algorithm,
            final Provider provider) {
        _algorithm = algorithm;
        _provider = provider;
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public Provider getProvider() {
        return _provider;
    }
}
