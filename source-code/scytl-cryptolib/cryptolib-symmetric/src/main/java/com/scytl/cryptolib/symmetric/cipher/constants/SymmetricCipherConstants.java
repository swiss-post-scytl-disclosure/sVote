/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.constants;

/**
 * Class which defines some constants that are used by symmetric ciphers.
 * <P>
 * Instances of this class are immutable.
 */
public class SymmetricCipherConstants {

    /**
     * Non-public constructor
     */	
	private SymmetricCipherConstants() {		
	}
	
	/**
     * Delimiter for the fields of the symmetric cipher specification.
     */
    public static final String CIPHER_SPECIFICATION_DELIMITER = "/";

    /**
     * Index of the algorithm field in the symmetric cipher specification.
     */
    public static final int ALGORITHM_FIELD_INDEX = 0;

    /**
     * Index of the encryption operating mode field in the symmetric cipher
     * specification.
     */
    public static final int ENCRYPTION_OPERATING_MODE_FIELD_INDEX = 1;

    /**
     * Index of the padding field in the symmetric cipher specification.
     */
    public static final int PADDING_FIELD_INDEX = 2;

}
