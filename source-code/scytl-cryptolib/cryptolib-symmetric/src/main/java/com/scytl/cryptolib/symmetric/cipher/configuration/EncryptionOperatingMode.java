/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.configuration;

/**
 * Enum which defines the encryption operating modes.
 */
public enum EncryptionOperatingMode {

    ECB("ECB"), CBC("CBC"), GCM("GCM");

    private final String _mode;

    private EncryptionOperatingMode(final String mode) {
        _mode = mode;
    }

    public String getMode() {
        return _mode;
    }

}
