/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.configuration;

/**
 * Defines the providers that are supported for creating secret keys.
 */
public enum SecretKeyProvider {

    SUNJCE("SunJCE"), BOUNCY_CASTLE("BC");

    private final String _provider;

    private SecretKeyProvider(final String name) {
        _provider = name;
    }

    public String getProvider() {
        return _provider;
    }
}
