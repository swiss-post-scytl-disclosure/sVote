/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.factory;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.symmetric.cipher.configuration.SymmetricCipherPolicy;

/**
 * A factory class for creating a symmetric cipher.
 */
public class SymmetricCipherFactory {

    private final SymmetricCipherPolicy _symmetricCipherPolicy;

    /**
     * Constructs a {@code SymmetricCipherFactory} using the provided
     * {@link SymmetricCipherPolicy}.
     * 
     * @param symmetricCipherPolicy
     *            the SymmetricCipherPolicy to be used to configure this
     *            SymmetricCipherFactory.
     *            <P>
     *            NOTE: The received {@link SymmetricCipherPolicy} should be an
     *            immutable object. If this is the case, then the entire
     *            {@code SymmetricCipherFactory} class is thread safe.
     */
    public SymmetricCipherFactory(
            final SymmetricCipherPolicy symmetricCipherPolicy) {

        _symmetricCipherPolicy = symmetricCipherPolicy;
    }

    /**
     * Creates a {@link CryptoSymmetricCipher} according to the given symmetric
     * cipher policy.
     * 
     * @return a {@link CryptoSymmetricCipher} object.
     */
    public CryptoSymmetricCipher create() {

        try {
            return new CryptoSymmetricCipher(_symmetricCipherPolicy);
        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException(e);
        }
    }
}
