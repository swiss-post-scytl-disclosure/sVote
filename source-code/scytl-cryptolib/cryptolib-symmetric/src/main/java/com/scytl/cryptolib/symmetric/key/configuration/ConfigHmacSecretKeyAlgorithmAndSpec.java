/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.configuration;

import com.scytl.cryptolib.symmetric.key.constants.SecretKeyConstants;

/**
 * Enum which defines the supported HMAC secret keys.
 * <P>
 * Each enum type defines the following attributes:
 * <ul>
 * <li>Algorithm name.</li>
 * <li>Key length.</li>
 * </ul>
 */
public enum ConfigHmacSecretKeyAlgorithmAndSpec {

    HMACwithSHA256_256(SecretKeyConstants.HMAC_SHA256_ALG, 256);

    private final String _algorithm;

    private final int _keyLengthInBits;

    private ConfigHmacSecretKeyAlgorithmAndSpec(final String algorithm,
            final int keyLengthInBits) {

        _algorithm = algorithm;
        _keyLengthInBits = keyLengthInBits;
    }

    public String getAlgorithm() {
        return _algorithm;
    }

    public int getKeyLengthInBits() {
        return _keyLengthInBits;
    }
}
