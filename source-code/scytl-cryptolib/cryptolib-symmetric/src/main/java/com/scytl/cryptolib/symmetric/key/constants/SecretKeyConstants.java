/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.key.constants;

/**
 * Class which defines some constants that are used by symmetric keys.
 * <P>
 * Instances of this class are immutable.
 */
public final class SecretKeyConstants {

    /**
     * This class cannot be instantiated.
     */
    private SecretKeyConstants() {
    }

    /**
     * The AES algorithm name.
     */
    public static final String AES_ALG = "AES";

    /**
     * HMAC with SHA256 algorithm name.
     */
    public static final String HMAC_SHA256_ALG = "HmacSHA256";
}
