/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.service;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.symmetric.SymmetricServiceAPI;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link SymmetricService} objects which are non thread-safe.
 */
public class BasicSymmetricServiceFactory extends
        PropertiesConfiguredFactory<SymmetricServiceAPI> {
    /**
     * Creates an instance of factory that will create services configured with
     * default values.
     */
    public BasicSymmetricServiceFactory() {
        super(SymmetricServiceAPI.class);
    }

    /**
     * This factory will create services configured with the given configuration
     * file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     */
    public BasicSymmetricServiceFactory(final String path) {
        super(SymmetricServiceAPI.class, path);
    }

    @Override
    protected SymmetricServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new SymmetricService(_path);
    }

    @Override
    protected SymmetricServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new SymmetricService();
    }

}
