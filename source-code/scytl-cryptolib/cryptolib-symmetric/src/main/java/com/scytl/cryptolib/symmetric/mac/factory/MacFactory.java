/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.mac.factory;

import com.scytl.cryptolib.symmetric.mac.configuration.MacPolicy;

/**
 * A factory class for creating a MAC generator and verifier.
 */
public class MacFactory {

    private final MacPolicy _macPolicy;

    /**
     * Constructs a MacFactory using the provided {@link MacPolicy}.
     * @param macPolicy
     *            The MacPolicy to be used to configure this MacFactory.
     *            <P>
     *            NOTE: The received {@link MacPolicy} should be an immutable
     *            object. If this is the case, then the entire
     *            {@code MacFactory} class is thread safe.
     */
    public MacFactory(final MacPolicy macPolicy) {

        _macPolicy = macPolicy;
    }

    /**
     * Creates a {@link CryptoMac} according to the given MAC policy.
     * 
     * @return A {@link CryptoMac} object.
     */
    public CryptoMac create() {

        return new CryptoMac(_macPolicy);
    }

}
