/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * Implementation of the {@link SymmetricCipherPolicy} interface, which reads
 * values from a properties input.
 * <P>
 * Instances of this class are immutable.
 */
public class SymmetricCipherPolicyFromProperties implements
        SymmetricCipherPolicy {

    private final ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmAndProvider;

    private final ConfigSymmetricCipherAlgorithmAndSpec _cipherAlgorithmAndSpec;

    /**
     * Creates a symmetric cipher policy using properties which are read from
     * the properties file at the specified path.
     * @param path
     *            the path where the properties are read from.
     * @throws CryptoLibException
     *             if the path is blank.
     */
    public SymmetricCipherPolicyFromProperties(final String path) {

        try {
            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _secureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider
                    .valueOf(helper
                        .getNotBlankOSDependentPropertyValue("symmetric.cipher.securerandom"));

            _cipherAlgorithmAndSpec =
                ConfigSymmetricCipherAlgorithmAndSpec.valueOf(helper
                    .getNotBlankPropertyValue("symmetric.cipher"));
        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigSymmetricCipherAlgorithmAndSpec getSymmetricCipherAlgorithmAndSpec() {

        return _cipherAlgorithmAndSpec;
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {

        return _secureRandomAlgorithmAndProvider;
    }
}
