/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.utils;

import javax.crypto.SecretKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.symmetric.service.SymmetricService;

/**
 * Utility to generate various types of symmetric data needed by tests.
 */
public class SymmetricTestDataGenerator {

    /**
     * Generates an secret key for encryption.
     * 
     * @return the generated secret key.
     * @throws GeneralCryptoLibException
     *             if the key generation process fails.
     */
    public static SecretKey getSecretKeyForEncryption()
            throws GeneralCryptoLibException {

        SymmetricService symmetricService = new SymmetricService();

        return symmetricService.getSecretKeyForEncryption();
    }

    /**
     * Generates an secret key for HMAC generation.
     * 
     * @return the generated secret key.
     * @throws GeneralCryptoLibException
     *             if the key generation process fails.
     */
    public static SecretKey getSecretKeyForHmac()
            throws GeneralCryptoLibException {

        SymmetricService symmetricService = new SymmetricService();

        return symmetricService.getSecretKeyForHmac();
    }
}
