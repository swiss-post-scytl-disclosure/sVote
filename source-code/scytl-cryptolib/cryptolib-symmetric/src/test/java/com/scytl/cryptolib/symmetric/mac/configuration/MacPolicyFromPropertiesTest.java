/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.mac.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.symmetric.key.configuration.SymmetricKeyPolicyFromProperties;

public class MacPolicyFromPropertiesTest {

    private static String INVALID_PATH = "Invalid path";

    private static MacPolicyFromProperties _macPolicyFromProperties;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _macPolicyFromProperties =
            new MacPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenPolicyFromIncorrectPathThenException()
            throws GeneralCryptoLibException {

        new MacPolicyFromProperties(INVALID_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyInvalidLabels()
            throws GeneralCryptoLibException {

        new SymmetricKeyPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyBlankLabels()
            throws GeneralCryptoLibException {

        new SymmetricKeyPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void givenPolicyWhenGetMacConfigThenExpectedValues() {

        ConfigMacAlgorithmAndProvider config =
            _macPolicyFromProperties.getMacAlgorithmAndProvider();

        assertEquals("HmacSHA256", config.getAlgorithm());
    }

}
