/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.mac.factory;

import static mockit.Deencapsulation.getField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.symmetric.mac.configuration.ConfigMacAlgorithmAndProvider;
import com.scytl.cryptolib.symmetric.mac.configuration.MacPolicy;

public class MacFactoryTest {

    @BeforeClass
    public static void setUp() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @AfterClass
    public static void removeBouncyCastleProvider() {
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Test
    public void whenCreateMacFactoryUsingPolicyHmac256AndSun() {

        MacFactory MacFactoryByPolicy =
            new MacFactory(getMacPolicyHmac256AndSun());

        MacPolicy expectedMacPolicy =
            getField(MacFactoryByPolicy, "_macPolicy");

        assertEquals(getMacPolicyHmac256AndSun()
            .getMacAlgorithmAndProvider(),
            expectedMacPolicy.getMacAlgorithmAndProvider());
    }

    @Test
    public void whenCreateMacFactoryUsingPolicyHmac256AndBc() {

        MacFactory MacFactoryByPolicy =
            new MacFactory(getMacPolicyHmac256AndBc());

        MacPolicy expectedMacPolicy =
            getField(MacFactoryByPolicy, "_macPolicy");

        assertEquals(getMacPolicyHmac256AndBc()
            .getMacAlgorithmAndProvider(),
            expectedMacPolicy.getMacAlgorithmAndProvider());
    }

    @Test
    public void whenCreateMacGeneratorUsingPolicyHmac256AndSun()
            throws NoSuchAlgorithmException, NoSuchProviderException {

        MacFactory MacFactory =
            new MacFactory(getMacPolicyHmac256AndSun());

        CryptoMac cryptoMac = MacFactory.create();

        assertNotNull(cryptoMac);
    }

    @Test
    public void whenCreateMacGeneratorUsingPolicyHmac256AndBc()
            throws NoSuchAlgorithmException, NoSuchProviderException {

        MacFactory MacFactory = new MacFactory(getMacPolicyHmac256AndBc());

        CryptoMac cryptoMac = MacFactory.create();

        assertNotNull(cryptoMac);
    }

    private MacPolicy getMacPolicyHmac256AndSun() {
        return new MacPolicy() {

            @Override
            public ConfigMacAlgorithmAndProvider getMacAlgorithmAndProvider() {
                return ConfigMacAlgorithmAndProvider.HMACwithSHA256_SUN;
            }
        };
    }

    private MacPolicy getMacPolicyHmac256AndBc() {
        return new MacPolicy() {

            @Override
            public ConfigMacAlgorithmAndProvider getMacAlgorithmAndProvider() {
                return ConfigMacAlgorithmAndProvider.HMACwithSHA256_BC;
            }
        };
    }

}
