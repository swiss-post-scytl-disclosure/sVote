/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.configuration;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;

public class SymmetricCipherAlgorithmAndSpecTest {

    private static final int GCM_AUTHENTICATION_TAG_BIT_LENGTH = 128;

    @Test
    public void givenAlgorithmAesGcmNoPaddingWhenSpecIsAesGcmNoPaddingAndBc() {

        assertAlgorithmModePadding(
            "AES/GCM/NoPadding",
            ConfigSymmetricCipherAlgorithmAndSpec.AESwithGCMandNOPADDING_96_128_BC);
    }

    @Test
    public void givenExpectedAuthTagLengthWhenSpecIsAesGcmNoPaddingAndBc() {

        assertAuthenticationTagLength(
            GCM_AUTHENTICATION_TAG_BIT_LENGTH,
            ConfigSymmetricCipherAlgorithmAndSpec.AESwithGCMandNOPADDING_96_128_BC);
    }

    @Test
    public void givenProviderBcWhenSpecIsAesGcmNoPaddingAndBc() {

        assertProvider(
            Provider.BOUNCY_CASTLE,
            ConfigSymmetricCipherAlgorithmAndSpec.AESwithGCMandNOPADDING_96_128_BC);
    }

    private void assertAlgorithmModePadding(
            final String expectedAlgorithm,
            final ConfigSymmetricCipherAlgorithmAndSpec cipherAlgorithmAndSpec) {

        Assert.assertEquals(
            "Algorithm/mode/padding was not the expected value",
            expectedAlgorithm,
            cipherAlgorithmAndSpec.getAlgorithmAndModeAndPadding());
    }

    private void assertAuthenticationTagLength(
            final int expectedAuthTagLength,
            final ConfigSymmetricCipherAlgorithmAndSpec cipherAlgorithmAndSpec) {

        Assert.assertEquals(
            "Authentication tag length was not the expected value",
            expectedAuthTagLength,
            cipherAlgorithmAndSpec.getAuthTagBitLength());
    }

    private void assertProvider(
            final Provider expectedProvider,
            final ConfigSymmetricCipherAlgorithmAndSpec cipherAlgorithmAndSpec) {

        Assert.assertEquals("Provider was not the expected value",
            expectedProvider, cipherAlgorithmAndSpec.getProvider());
    }

}
