/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.configuration;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;

public class SymmetricCipherFromPropertiesTest {

    private static final int INIT_VECTOR_BIT_LENGTH = 96;

    private static final int AUTHENTICATION_TAG_BIT_LENGTH = 128;

    private static String INVALID_PATH = "Invalid path";

    private static SymmetricCipherPolicyFromProperties _cipherPolicyFromProperties;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _cipherPolicyFromProperties =
            new SymmetricCipherPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @Test
    public void givenPolicyWhenGetSymmetricCipherConfigThenExpectedValues() {

        ConfigSymmetricCipherAlgorithmAndSpec config =
            _cipherPolicyFromProperties
                .getSymmetricCipherAlgorithmAndSpec();

        boolean algorithmAndModeAndPaddingCorrect =
            config.getAlgorithmAndModeAndPadding().equals(
                "AES/GCM/NoPadding");
        boolean initVectorLengthCorrect =
            config.getInitVectorBitLength() == INIT_VECTOR_BIT_LENGTH;
        boolean authTagLengthCorrect =
            config.getAuthTagBitLength() == AUTHENTICATION_TAG_BIT_LENGTH;

        assert (algorithmAndModeAndPaddingCorrect
            && initVectorLengthCorrect && authTagLengthCorrect);
    }

    @Test(expected = CryptoLibException.class)
    public void whenPolicyFromIncorrectPathThenException()
            throws GeneralCryptoLibException {

        new SymmetricCipherPolicyFromProperties(INVALID_PATH);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyInvalidLabels()
            throws GeneralCryptoLibException {

        new SymmetricCipherPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreatePolicyBlankLabels()
            throws GeneralCryptoLibException {

        new SymmetricCipherPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

}
