/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.mac.configuration;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.commons.configuration.Provider;

public class MacAlgorithmProviderPairTest {

    @Test
    public void givenAlgorithmSha256WhenPairIsHmac256AndSun() {

        assertAlgorithm("HmacSHA256",
            ConfigMacAlgorithmAndProvider.HMACwithSHA256_SUN);
    }

    @Test
    public void givenAlgorithmSha256WhenPairIsHmac256AndBc() {

        assertAlgorithm("HmacSHA256",
            ConfigMacAlgorithmAndProvider.HMACwithSHA256_BC);
    }

    @Test
    public void givenProviderSunWhenPairIsHmac256AndSun() {

        assertProvider(Provider.SUN_JCE,
            ConfigMacAlgorithmAndProvider.HMACwithSHA256_SUN);
    }

    @Test
    public void givenProviderBcWhenPairIsHmac256AndBc() {

        assertProvider(Provider.BOUNCY_CASTLE,
            ConfigMacAlgorithmAndProvider.HMACwithSHA256_BC);
    }

    private void assertAlgorithm(final String expectedAlgorithm,
            final ConfigMacAlgorithmAndProvider macAlgorithmAndProvider) {

        Assert.assertEquals("Algorithm name was not the expected value",
            expectedAlgorithm, macAlgorithmAndProvider.getAlgorithm());
    }

    private void assertProvider(final Provider expectedProvider,
            final ConfigMacAlgorithmAndProvider macAlgorithmAndProvider) {

        Assert.assertEquals("Provider was not the expected value",
            expectedProvider, macAlgorithmAndProvider.getProvider());
    }

}
