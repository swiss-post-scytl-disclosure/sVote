/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.symmetric.cipher.factory;

import java.security.Security;
import java.util.Arrays;

import javax.crypto.SecretKey;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicyFromProperties;
import com.scytl.cryptolib.primitives.securerandom.factory.CryptoRandomBytes;
import com.scytl.cryptolib.primitives.securerandom.factory.SecureRandomFactory;
import com.scytl.cryptolib.symmetric.cipher.configuration.SymmetricCipherPolicy;
import com.scytl.cryptolib.symmetric.cipher.configuration.SymmetricCipherPolicyFromProperties;
import com.scytl.cryptolib.symmetric.service.SymmetricService;

public class SymmetricCipherTest {

    private static final int DATA_BYTE_LENGTH = 100;

    private static SecretKey _key;

    private static CryptoRandomBytes _randomByteGenerator;

    private static byte[] _data;

    private static CryptoSymmetricCipher _cryptoSymmetricCipher;

    private static SymmetricCipherPolicy _symmetricCipherPolicy;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        Security.addProvider(new BouncyCastleProvider());

        SymmetricService symmetricServiceFromDefaultConstructor =
            new SymmetricService();

        _key =
            symmetricServiceFromDefaultConstructor
                .getSecretKeyForEncryption();

        _randomByteGenerator =
            new SecureRandomFactory(new SecureRandomPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH))
                .createByteRandom();

        _data = _randomByteGenerator.nextRandom(DATA_BYTE_LENGTH);

        _symmetricCipherPolicy =
            new SymmetricCipherPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);

        _cryptoSymmetricCipher =
            new SymmetricCipherFactory(_symmetricCipherPolicy).create();
    }

    @AfterClass
    public static void tearDown() {
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Test
    public void testEncryptAndDecryptDataByteArray()
            throws GeneralCryptoLibException {

        byte[] encryptedData = _cryptoSymmetricCipher.encrypt(_key, _data);

        byte[] initVector = _cryptoSymmetricCipher.getInitVector(_data);

        byte[] decryptedData =
            _cryptoSymmetricCipher.decrypt(_key, encryptedData);

        Assert.assertTrue(Arrays.equals(decryptedData, _data));

        Assert.assertEquals(initVector.length, _symmetricCipherPolicy
            .getSymmetricCipherAlgorithmAndSpec().getInitVectorBitLength()
            / Byte.SIZE);

    }
}
