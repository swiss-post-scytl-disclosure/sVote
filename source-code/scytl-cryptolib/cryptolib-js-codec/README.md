# Codec Module

This module defines the codec utilities API for Scytl's JavaScript cryptographic library.

## How to view, build and test the source code of this module

Start by git cloning the project `comm/scytl-cryptolib` from `Stash`.

The source code of this module will be found in the directory `scytl-cryptolib/cryptolib-js-codec/lib`.

To build this module, change to the directory `scytl-cryptolib/cryptolib-js-codec` and do the following:

```java
npm install
--or--
mvn clean install -DskipTests
```
The unit tests of this module will be found in the directory `scytl-cryptolib/cryptolib-js-codec/spec`. To run the tests, change to the directory `scytl-cryptolib/cryptolib-js-codec` and do the following:

```javascript
karma test
--or--
mvn test
```
To only run a chosen test, add an `f` in front of the corresponding `it` statement. For example:

```javascript
fit('UTF-8 encode a string', function()
...
```
**Note:** To build and test the entire `scytl-cryptolib` project, change to the directory `scytl-cryptolib` and do the following:

```javascript
mvn clean install
```
## How to generate the JSDoc for this module

To generate the JSDoc for this module, change to the directory `scytl-cryptolib/cryptolib-js-codec`, build the module (if not already done) and do the following:

```javascript
node_modules/.bin/jsdoc lib
```
This will generate a file called `out`. Double click on the file `out/index.html` to view the JSDoc.

## How to `npm` install this module

To `npm` install this module in standalone mode, do the following:

```javascript
npm install --registry https://nexus.scytl.net/content/groups/public-npm/ scytl-codec
```
To `npm` install this module as a dependency of your own module, do the following:

1. Add the following line to the `dependencies` section of your module's `package.json` file.
    ```javascript
    "scytl-codec": "^2.1.0",
    ```
2. Make sure that the `.npmrc` file of your module contains the following line.
    ```javascript
    registry=https://nexus.scytl.net/content/groups/public-npm/
    ```
3. Install all dependencies.
    ```javascript
    npm install
    ```

## How to UTF-8 encode and decode a string

The following example shows how to UTF-8 encode and decode a string. The output of the UTF-8 encoding method will be a byte array, wrapped in a `Uint8Array` object.

```javascript
var codec = require('scytl-codec');

var str = 'myString';

var encoded = codec.utf8Encode(str);
var decoded = codec.utf8Decode(encoded);

expect(decoded).toBe(str);
```
## How to convert a BigInteger object to and from bytes

The following example shows how to convert a `BigInteger` object to and from bytes. The output of the `bigIntegerToBytes` method will be in the form of a byte array, wrapped in a `Uint8Array` object.

```javascript
var codec = require('scytl-codec');

var bigInt = new BigInteger('85');

var bytes = codec.bigIntegerToBytes(bigInt);
var bigIntFromBytes = codec.bytesToBigInteger(bytes);

expect(bigIntFromBytes).toEqual(bigInt);
```
## How to Base64 encode and decode some data

The following example shows how to Base64 encode and decode some bytes. The output of the Base64 encoding method will be a string.

```javascript
var codec = require('scytl-codec');

var bytes = new Uint8Array([105, 73, 99]);

var encoded = codec.base64Encode(bytes);
var decoded = codec.base64Decode(encoded);

expect(decoded).toEqual(bytes);
```
The following example shows how to Base64 encode and decode a string. The output of the Base64 encoding method will be a string. The output of the Base64 decoding method will be a byte array, wrapped in a `Uint8Array` object, and it must be UTF-8 decoded to retrieve the string.

```javascript
var codec = require('scytl-codec');

var str = 'myString';

var encoded = codec.base64Encode(str);
var decoded = codec.base64Decode(encoded);
var decodedStr = codec.utf8Decode(decoded);

expect(decodedStr).toBe(str);
```
The following example shows how to Base64 encode and decode a `BigInteger` object. The output of the Base64 encoding method will be a string. The output of the Base64 decoding method will be a byte array, wrapped in a `Uint8Array` object, and it must be converted to retrieve the `BigInteger` object.

```javascript
var codec = require('scytl-codec');

var bigInt = new BigInteger('120');

var encoded = codec.base64Encode(bigInt);
var decoded = codec.base64Decode(encoded);
var decodedBigInt = codec.bytesToBigInteger(decoded);

expect(decodedBigInt).toEqual(bigInt);
```
## How to hexadecimally encode and decode some data

The following example shows how to hexadecimally encode and decode some bytes. The output of the hexadecimal encoding method will be a string.

```javascript
var codec = require('scytl-codec');

var bytes = new Uint8Array([23, 33, 90]);

var encoded = codec.hexEncode(bytes);
var decoded = codec.hexDecode(encoded);

expect(decoded).toEqual(bytes);
```
The following example shows how to hexadecimally encode and decode a string. The output of the hexadecimal encoding method will be a string. The output of the hexadecimal decoding method will be a byte array, wrapped in a `Uint8Array` object, and it must be UTF-8 decoded to retrieve the string.

```javascript
var codec = require('scytl-codec');

var str = 'myString';

var encoded = codec.hexEncode(str);
var decoded = codec.hexDecode(encoded);
var decodedStr = codec.utf8Decode(decoded);

expect(decodedStr).toBe(str);
```
The following example shows how to hexadecimally encode and decode a `BigInteger` object. The output of the hexadecimal encoding method will be a string. The output of the hexadecimal decoding method will be a byte array, wrapped in a `Uint8Array` object, and it must be converted to retrieve the `BigInteger` object.

```javascript
var codec = require('scytl-codec');

var bigInt = new BigInteger('35');

var encoded = codec.hexEncode(bigInt);
var decoded = codec.hexDecode(encoded);
var decodedBigInt = codec.bytesToBigInteger(decoded);

expect(decodedBigInt).toEqual(bigInt);
```
## How to binary encode and decode some bytes

**WARNING:** The methods described here are **DEPRECATED**. They are primarily intended for internal use by the library, in order to handle the conversion between `Uint8Array` objects and the `binary encoded strings` defined by earlier versions of `forge` to represent byte arrays, before `Uint8Array` objects were in widespread use. The only reason you would need these methods would be if you were working with `forge` directly, rather than using this library.

The following example shows how to binary encode and decode some bytes. The output of the binary encoding method will be a string.
```javascript
var codec = require('scytl-codec');

var bytes = new Uint8Array([5, 16, 9]);

var encoded = codec.binaryEncode(bytes);
var decoded = codec.binaryDecode(encoded);

expect(decoded).toEqual(bytes);
```
