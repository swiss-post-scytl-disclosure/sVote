/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import static com.scytl.cryptolib.proofs.maurer.configuration.Constants.PLAINTEXT_EQUALITY_PROOF_AUXILIARY_DATA;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.mathematical.groups.activity.GroupElementsDivider;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionPlaintextEquality;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Verifier of the zero knowledge proof for two ciphertexts that were each
 * generated with a different key pair and random exponent from the same
 * plaintext.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public class PlaintextEqualityProofVerifier<T, E extends GroupElement<T>> {

    private final E _primaryCiphertextGamma;

    private final List<E> _primaryCiphertextPhis;

    private final ElGamalPublicKey _primaryPublicKey;

    private final E _secondaryCiphertextGamma;

    private final List<E> _secondaryCiphertextPhis;

    private final ElGamalPublicKey _secondaryPublicKey;

    private final Proof _proof;

    private final MathematicalGroup<E> _group;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    private final GroupElementsDivider _divider;

    /**
     * Default constructor.
     * 
     * @param primaryCiphertext
     *            the primary ElGamal encrypted plaintext.
     * @param primaryPublicKey
     *            the primary ElGamal public key.
     * @param secondaryCiphertext
     *            the secondary ElGamal encrypted plaintext.
     * @param secondaryPublicKey
     *            the secondary ElGamal public key.
     * @param proof
     *            the proof to be verified.
     * @param group
     *            the Zp subgroup used for ElGamal encryption.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    PlaintextEqualityProofVerifier(
            final List<E> primaryCiphertext,
            final ElGamalPublicKey primaryPublicKey,
            final List<E> secondaryCiphertext,
            final ElGamalPublicKey secondaryPublicKey,
            final Proof proof,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset)
            throws GeneralCryptoLibException {

        _primaryCiphertextGamma = primaryCiphertext.get(0);
        _primaryCiphertextPhis =
            new ArrayList<>(primaryCiphertext.subList(1,
                primaryCiphertext.size()));
        _primaryPublicKey = primaryPublicKey;

        _secondaryCiphertextGamma = secondaryCiphertext.get(0);
        _secondaryCiphertextPhis =
            new ArrayList<>(secondaryCiphertext.subList(1,
                secondaryCiphertext.size()));
        _secondaryPublicKey = secondaryPublicKey;

        _proof = proof;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

        _divider = new GroupElementsDivider();
    }

    /**
     * Verifies the plaintext equality zero knowledge proof of knowledge.
     * 
     * @return true if the proof is verified as true, false otherwise.
     * @throws GeneralCryptoLibException
     *             if {@code PlaintextEqualityProofVerifier} was initialized by
     *             the invalid arguments.
     */
    public boolean verify() throws GeneralCryptoLibException {

        PhiFunctionPlaintextEquality<T> phiFunctionPlaintextEquality =
            new PhiFunctionPlaintextEquality<>(_group,
                buildListBaseElements());

        Verifier<T, E> verifier =
            new Verifier<>(_group, phiFunctionPlaintextEquality,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return verifier.verify(buildListPublicValues(), _proof,
            PLAINTEXT_EQUALITY_PROOF_AUXILIARY_DATA);
    }

    @SuppressWarnings("unchecked")
    private List<E> buildListBaseElements() {

        List<E> invertedSecondaryPublicKey =
            buildSecondaryInvertedPublicKey();

        List<E> baseElements = new ArrayList<>();
        baseElements.add(_group.getGenerator());
        baseElements.addAll((List<? extends E>) _primaryPublicKey
            .getKeys());
        baseElements.addAll(invertedSecondaryPublicKey);

        return baseElements;
    }

    @SuppressWarnings("unchecked")
    private List<E> buildSecondaryInvertedPublicKey() {

        List<E> invertedSecondaryPublicKey = new ArrayList<>();
        List<? extends E> secondaryPublicKeyElements =
            (List<? extends E>) _secondaryPublicKey.getKeys();
        for (E element : secondaryPublicKeyElements) {
            invertedSecondaryPublicKey.add((E) element.invert());
        }

        return invertedSecondaryPublicKey;
    }

    private List<E> buildListPublicValues()
            throws GeneralCryptoLibException {

        List<E> dividedSubCiphertext =
            _divider.divide(_primaryCiphertextPhis,
                _secondaryCiphertextPhis, _group);

        List<E> publicValues = new ArrayList<>();
        publicValues.add(_primaryCiphertextGamma);
        publicValues.add(_secondaryCiphertextGamma);
        publicValues.addAll(dividedSubCiphertext);

        return publicValues;
    }
}
