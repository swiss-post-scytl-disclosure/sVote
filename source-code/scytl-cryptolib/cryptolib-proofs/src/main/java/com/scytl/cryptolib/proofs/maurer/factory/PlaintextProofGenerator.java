/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import static com.scytl.cryptolib.proofs.maurer.configuration.Constants.PLAINTEXT_PROOF_AUXILIARY_DATA;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.mathematical.groups.activity.GroupElementsDivider;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.bean.ProofPreComputedValues;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionPlaintext;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Plaintext proof generator.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public final class PlaintextProofGenerator<T, E extends GroupElement<T>> {

    private final ElGamalPublicKey _publicKey;

    private final E _gamma;

    private final List<E> _phis;

    private final List<E> _plaintext;

    private final Exponent _r;

    private final MathematicalGroup<E> _group;

    private final GroupElementsDivider _divider;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    private final ProofPreComputedValues _preComputedValues;

    /**
     * Default constructor.
     * 
     * @param publicKey
     *            the public key used to ElGamal encrypt the plaintext.
     * @param ciphertext
     *            the ElGamal encrypted plaintext.
     * @param plaintext
     *            the plaintext.
     * @param r
     *            the ElGamal encryption exponent, which acts as the witness for
     *            this proof. @param group the Zp subgroup used for ElGamal
     *            encryption.
     * @param group
     *            the Zp subgroup used for ElGamal encryption.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @param preComputedValues
     *            the pre-computed values used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    PlaintextProofGenerator(
            final ElGamalPublicKey publicKey,
            final List<E> ciphertext,
            final List<E> plaintext,
            final Exponent r,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset,
            final ProofPreComputedValues preComputedValues)
            throws GeneralCryptoLibException {

        _gamma = ciphertext.get(0);
        _phis = new ArrayList<>(ciphertext.subList(1, ciphertext.size()));

        _publicKey = publicKey;
        _plaintext = new ArrayList<>(plaintext);
        _r = r;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

        _preComputedValues = preComputedValues;

        _divider = new GroupElementsDivider();
    }

    /**
     * Generates the plaintext proof.
     * 
     * @return the generated proof.
     * @throws GeneralCryptoLibException
     *             if any of the generation input parameters are null.
     */
    public Proof generate() throws GeneralCryptoLibException {

        PhiFunctionPlaintext<T> phiFunctionPlaintext =
            new PhiFunctionPlaintext<>(_group, buildListOfBaseElements());

        Prover<T, E> prover =
            new Prover<>(_group, phiFunctionPlaintext,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return prover.prove(buildListPublicValues(), buildListExponents(),
            PLAINTEXT_PROOF_AUXILIARY_DATA, _preComputedValues);
    }

    private List<Exponent> buildListExponents() {

        List<Exponent> exponentList = new ArrayList<>();
        exponentList.add(_r);

        return exponentList;
    }

    private List<E> buildListPublicValues()
            throws GeneralCryptoLibException {

        List<E> dividedCiphertext =
            _divider.divide(_phis, _plaintext, _group);

        List<E> publicValues = new ArrayList<>();
        publicValues.add(_gamma);
        publicValues.addAll(dividedCiphertext);

        return publicValues;
    }

    @SuppressWarnings("unchecked")
    private List<E> buildListOfBaseElements() {

        List<E> list = new ArrayList<>();
        list.add(_group.getGenerator());
        list.addAll((List<? extends E>) _publicKey.getKeys());
        return list;
    }
}
