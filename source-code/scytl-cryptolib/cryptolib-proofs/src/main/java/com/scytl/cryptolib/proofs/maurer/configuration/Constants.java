/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.configuration;

/**
 * This is a temporary class that is needed until the new commons module has
 * been fully refactored.
 */
public final class Constants {

    /**
     * This is a helper class. It cannot be instantiated.
     */
    private Constants() {
    }

    public static final String DECRYPTION_PROOF_AUXILIARY_DATA =
        "DecryptionProof";

    public static final String EXPONENTIATION_PROOF_AUXILIARY_DATA =
        "ExponentiationProof";

    public static final String PLAINTEXT_PROOF_AUXILIARY_DATA =
        "PlaintextProof";

    public static final String PLAINTEXT_EQUALITY_PROOF_AUXILIARY_DATA =
        "PlaintextEqualityProof";

    public static final String SIMPLE_PLAINTEXT_EQUALITY_PROOF_AUXILIARY_DATA =
        "SimplePlaintextEqualityProof";

    public static final String PLAINTEXT_EXPONENT_EQUALITY_PROOF_AUXILIARY_DATA =
        "PlaintextExponentEqualityProof";
}
