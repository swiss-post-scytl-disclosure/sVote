/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.configuration;

import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicy;
import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;

/**
 * Defines Maurer Unified Proof policy.
 */
public interface MaurerProofPolicy extends SecureRandomPolicy,
        MessageDigestPolicy {

    /**
     * Gets the character set to be used during the generation and
     *         verification of proofs.
     * @return charset.
     */
    ConfigProofHashCharset getCharset();
}
