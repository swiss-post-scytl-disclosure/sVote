/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.service;

import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.ServiceFactoryHelper;

/**
 * This class sets up default factories of {@link ProofsServiceAPI} objects.
 */
public final class ProofsServiceFactoryHelper {

    /**
     * This is a helper class. It cannot be instantiated.
     */
    private ProofsServiceFactoryHelper() {
        super();
    }

    /**
     * Retrieves a new factory of non thread-safe services.
     * <p>
     * Default factory is {@link BasicProofsServiceFactory}.
     * 
     * @param params
     *            List of parameters used in the creation of the default
     *            factory.
     * @return the new factory.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<ProofsServiceAPI> getInstance(
            final Object... params) {
        return ServiceFactoryHelper.get(BasicProofsServiceFactory.class,
            params);

    }

    /**
     * Retrieves a new factory of thread-safe services.
     * <p>
     * Default factory is {link {@link PollingProofsServiceFactory}
     * 
     * @param params
     *            a list of parameters used in the creation of the default
     *            factory.
     * @return the new factory of thread-safe services.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<ProofsServiceAPI> getFactoryOfThreadSafeServices(
            final Object... params) {
        return ServiceFactoryHelper.get(PollingProofsServiceFactory.class,
            params);
    }

}
