/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.service;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of services that are not thread-safe.
 */
public class BasicProofsServiceFactory extends
        PropertiesConfiguredFactory<ProofsServiceAPI> {

    /**
     * Default constructor. This factory will create a service configured with
     * default values.
     */
    public BasicProofsServiceFactory() {
        super(ProofsServiceAPI.class);
    }

    /**
     * Constructor that will setup the factory to use this path in order to
     * setup the Symmetric Services.
     * 
     * @param path
     *            a path to the file with required properties.
     */
    public BasicProofsServiceFactory(final String path) {
        super(ProofsServiceAPI.class, path);
    }

    @Override
    protected ProofsServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new ProofsService(_path);
    }

    @Override
    protected ProofsServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new ProofsService();
    }
}
