/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import static com.scytl.cryptolib.proofs.maurer.configuration.Constants.PLAINTEXT_EXPONENT_EQUALITY_PROOF_AUXILIARY_DATA;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.bean.ProofPreComputedValues;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionPlaintextExponentEquality;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Plaintext exponent equality proof generator.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public final class PlaintextExponentEqualityProofGenerator<T, E extends GroupElement<T>> {

    private final List<E> _ciphertext;

    private final List<E> _baseElements;

    private final Exponent _exponent1;

    private final Exponent _exponent2;

    private final MathematicalGroup<E> _group;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    private final ProofPreComputedValues _preComputedValues;

    /**
     * Default constructor.
     * 
     * @param ciphertext
     *            the ElGamal encrypted list of base elements.
     * @param baseElements
     *            the list of base elements.
     * @param exponent1
     *            the ephemeral key exponent, which acts as a witness for this
     *            proof.
     * @param exponent2
     *            the message exponent, which acts as a witness for this proof.
     * @param group
     *            the Zp subgroup used for ElGamal encryption.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @param preComputedValues
     *            the pre-computed values used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    PlaintextExponentEqualityProofGenerator(
            final List<E> ciphertext,
            final List<E> baseElements,
            final Exponent exponent1,
            final Exponent exponent2,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset,
            final ProofPreComputedValues preComputedValues)
            throws GeneralCryptoLibException {

        _ciphertext = new ArrayList<>(ciphertext);
        _baseElements = new ArrayList<>(baseElements);
        _exponent1 = exponent1;
        _exponent2 = exponent2;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

        _preComputedValues = preComputedValues;
    }

    /**
     * Generates a plaintext exponent equality proof.
     * 
     * @return the generated proof.
     * @throws GeneralCryptoLibException
     *             if {@code PlaintextExponentEqualityProofGenerator} was
     *             initialized by the invalid arguments.
     */
    public Proof generate() throws GeneralCryptoLibException {

        PhiFunctionPlaintextExponentEquality<T> phiFunctionPlaintextExponentEquality =
            new PhiFunctionPlaintextExponentEquality<>(_group,
                _baseElements);

        Prover<T, E> prover =
            new Prover<>(_group, phiFunctionPlaintextExponentEquality,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return prover.prove(_ciphertext, buildListExponents(),
            PLAINTEXT_EXPONENT_EQUALITY_PROOF_AUXILIARY_DATA,
            _preComputedValues);
    }

    private List<Exponent> buildListExponents() {

        List<Exponent> exponentList = new ArrayList<>();
        exponentList.add(_exponent1);
        exponentList.add(_exponent2);

        return exponentList;
    }
}
