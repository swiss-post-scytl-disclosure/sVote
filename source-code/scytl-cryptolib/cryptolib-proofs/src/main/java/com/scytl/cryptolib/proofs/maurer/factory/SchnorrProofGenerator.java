/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.bean.ProofPreComputedValues;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionSchnorr;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Schnorr proof generator.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public final class SchnorrProofGenerator<T, E extends GroupElement<T>> {

    private final String _voterID;

    private final String _electionEventID;

    private final E _exponentiatedElement;

    private final Exponent _exponent;

    private final MathematicalGroup<E> _group;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    private final ProofPreComputedValues _preComputedValues;

    /**
     * Default constructor.
     * 
     * @param voterID
     *            the voter identification number, which forms part of the proof
     *            generation business data.
     * @param electionEventID
     *            the election event identification number, which forms part of
     *            the proof generation business data.
     * @param exponent
     *            the ElGamal encryption exponent, which acts as the witness for
     *            this proof.
     * @param group
     *            the Zp subgroup used for exponentiation.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @param preComputedValues
     *            the pre-computed values used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    SchnorrProofGenerator(
            final String voterID,
            final String electionEventID,
            final E exponentiatedElement,
            final Exponent exponent,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset,
            final ProofPreComputedValues preComputedValues)
            throws GeneralCryptoLibException {

        _voterID = voterID;
        _electionEventID = electionEventID;
        _exponentiatedElement = exponentiatedElement;
        _exponent = exponent;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

        _preComputedValues = preComputedValues;
    }

    /**
     * Generates a schnorr proof.
     * 
     * @return the generated proof.
     * @throws GeneralCryptoLibException
     *             if {@code SchnorrProofGenerator} was initialized by the
     *             invalid arguments.
     */
    public Proof generate() throws GeneralCryptoLibException {

        PhiFunctionSchnorr<T> phiFunctionSchnorr =
            new PhiFunctionSchnorr<>(_group, buildListOfBaseElements());

        Prover<T, E> prover =
            new Prover<>(_group, phiFunctionSchnorr,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return prover.prove(buildListPublicValues(), buildListExponents(),
            buildDataString(), _preComputedValues);
    }

    private String buildDataString() {

        return "SchnorrProof:VoterID=" + _voterID + "ElectionEventID="
            + _electionEventID;
    }

    private List<Exponent> buildListExponents() {

        List<Exponent> exponentList = new ArrayList<>();
        exponentList.add(_exponent);

        return exponentList;
    }

    private List<E> buildListPublicValues() {

        List<E> publicList = new ArrayList<>();
        publicList.add(_exponentiatedElement);

        return publicList;
    }

    private List<E> buildListOfBaseElements() {

        List<E> baseElements = new ArrayList<>();
        baseElements.add(_group.getGenerator());

        return baseElements;
    }
}
