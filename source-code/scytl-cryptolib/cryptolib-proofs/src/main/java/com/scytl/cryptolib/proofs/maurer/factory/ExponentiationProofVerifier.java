/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import static com.scytl.cryptolib.proofs.maurer.configuration.Constants.EXPONENTIATION_PROOF_AUXILIARY_DATA;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionExponentiation;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Verifier of the zero knowledge proof of an exponent used to perform the
 * exponentiation operation on a collection of mathematical group elements.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public class ExponentiationProofVerifier<T, E extends GroupElement<T>> {

    private final List<E> _exponentiatedElements;

    private final List<E> _baseElements;

    private final Proof _proof;

    private final MathematicalGroup<E> _group;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    /**
     * Default constructor.
     * 
     * @param exponentiatedElements
     *            the exponentiated base elements.
     * @param baseElements
     *            the base elements.
     * @param proof
     *            the proof to be verified.
     * @param group
     *            the Zp subgroup used for the exponentiation.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    ExponentiationProofVerifier(
            final List<E> exponentiatedElements,
            final List<E> baseElements,
            final Proof proof,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset)
            throws GeneralCryptoLibException {

        _exponentiatedElements = new ArrayList<>(exponentiatedElements);
        _baseElements = new ArrayList<>(baseElements);
        _proof = proof;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

    }

    /**
     * Verifies the exponentiation zero knowledge proof.
     * 
     * @return true if the proof is verified as true, false otherwise.
     * @throws GeneralCryptoLibException
     *             if {@code ExponentiationProofVerifier} was instantiated by
     *             invalid arguments.
     */
    public boolean verify() throws GeneralCryptoLibException {

        PhiFunctionExponentiation<T> phiFunctionExponentiation =
            new PhiFunctionExponentiation<>(_group, _baseElements);

        Verifier<T, E> verifier =
            new Verifier<>(_group, phiFunctionExponentiation,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return verifier.verify(_exponentiatedElements, _proof,
            EXPONENTIATION_PROOF_AUXILIARY_DATA);
    }
}
