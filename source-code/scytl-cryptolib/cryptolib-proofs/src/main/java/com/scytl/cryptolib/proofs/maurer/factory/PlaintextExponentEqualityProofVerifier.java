/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import static com.scytl.cryptolib.proofs.maurer.configuration.Constants.PLAINTEXT_EXPONENT_EQUALITY_PROOF_AUXILIARY_DATA;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionPlaintextExponentEquality;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Plaintext exponent equality proof verifier.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public final class PlaintextExponentEqualityProofVerifier<T, E extends GroupElement<T>> {

    private final List<E> _ciphertext;

    private final List<E> _baseElements;

    private final Proof _proof;

    private final MathematicalGroup<E> _group;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    /**
     * Default constructor.
     * 
     * @param ciphertext
     *            the ElGamal encrypted list of base elements.
     * @param baseElements
     *            the list of base elements.
     * @param proof
     *            the proof to be verified.
     * @param group
     *            the Zp subgroup used for ElGamal encryption.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    PlaintextExponentEqualityProofVerifier(
            final List<E> ciphertext,
            final List<E> baseElements,
            final Proof proof,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset)
            throws GeneralCryptoLibException {

        _ciphertext = new ArrayList<>(ciphertext);
        _baseElements = new ArrayList<>(baseElements);
        _proof = proof;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;
    }

    /**
     * Verifies a plaintext exponent equality proof.
     * 
     * @return true if the proof is verified as true, false otherwise.
     * @throws GeneralCryptoLibException
     *             if {@code PlaintextExponentEqualityProofVerifier} was
     *             initialized by invalid values.
     */
    public boolean verify() throws GeneralCryptoLibException {

        PhiFunctionPlaintextExponentEquality<T> _phiFunctionPlaintextExponentEquality =
            new PhiFunctionPlaintextExponentEquality<>(_group,
                _baseElements);

        Verifier<T, E> verifier =
            new Verifier<>(_group, _phiFunctionPlaintextExponentEquality,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return verifier.verify(_ciphertext, _proof,
            PLAINTEXT_EXPONENT_EQUALITY_PROOF_AUXILIARY_DATA);
    }
}
