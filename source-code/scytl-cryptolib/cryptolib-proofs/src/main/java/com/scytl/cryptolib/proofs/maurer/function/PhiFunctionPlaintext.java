/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.function;

import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;

/**
 * A definition of the phi function used for generating the Zero Knowledge Proof
 * of Knowledge of a plain text.
 * <p>
 * 
 * @param <T>
 *            the {@link GroupElement} value type.
 */
public final class PhiFunctionPlaintext<T> extends PhiFunction<T> {

    /**
     * Creates an instance of {@code PhiFunctionPlaintext}.
     * <P>
     * Note: the number of inputs is set to one, while the number of outputs and
     * the computation rules are derived from the length (size) of baseElements.
     * 
     * @param group
     *            the mathematical group on which this PHI function operates.
     * @param baseElements
     *            a list of base elements which are members of the received
     *            mathematical group.
     * @throws GeneralCryptoLibException
     *             if arguments are invalid.
     */
    public <E extends GroupElement<T>> PhiFunctionPlaintext(
            final MathematicalGroup<E> group, final List<E> baseElements)
            throws GeneralCryptoLibException {

        super(group, 1, baseElements.size(), baseElements,
            buildComputationRules(baseElements.size()));
    }

    private static int[][][] buildComputationRules(final int numElements) {

        int[][][] rules = new int[numElements][][];

        for (int i = 0; i < numElements; i++) {

            rules[i] = new int[][] {{i + 1, 1 } };
        }

        return rules;
    }
}
