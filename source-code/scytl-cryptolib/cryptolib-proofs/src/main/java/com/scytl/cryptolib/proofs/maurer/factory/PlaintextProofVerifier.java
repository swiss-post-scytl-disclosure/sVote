/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import static com.scytl.cryptolib.proofs.maurer.configuration.Constants.PLAINTEXT_PROOF_AUXILIARY_DATA;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.mathematical.groups.activity.GroupElementsDivider;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionPlaintext;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Plaintext proof verifier.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public final class PlaintextProofVerifier<T, E extends GroupElement<T>> {

    private final ElGamalPublicKey _publicKey;

    private final E _gamma;

    private final List<E> _phis;

    private final List<E> _plaintext;

    private final Proof _proof;

    private final MathematicalGroup<E> _group;

    private final GroupElementsDivider _divider;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    /**
     * Default constructor.
     * 
     * @param publicKey
     *            the public key used to ElGamal encrypt the plaintext.
     * @param ciphertext
     *            the ElGamal encrypted plaintext.
     * @param plaintext
     *            the plaintext.
     * @param proof
     *            the proof to be verified.
     * @param group
     *            the Zp subgroup used for ElGamal encryption.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    PlaintextProofVerifier(
            final ElGamalPublicKey publicKey,
            final List<E> ciphertext,
            final List<E> plaintext,
            final Proof proof,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset)
            throws GeneralCryptoLibException {

        _gamma = ciphertext.get(0);
        _phis = new ArrayList<>(ciphertext.subList(1, ciphertext.size()));

        _publicKey = publicKey;
        _plaintext = new ArrayList<>(plaintext);
        _proof = proof;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

        _divider = new GroupElementsDivider();
    }

    /**
     * Verifies a plaintext proof.
     * 
     * @return true if the proof is verified as true, false otherwise.
     * @throws GeneralCryptoLibException
     *             if {@code PlaintextProofVerifier} was initialized by invalid
     *             arguments.
     */
    public boolean verify() throws GeneralCryptoLibException {

        PhiFunctionPlaintext<T> phiFunctionPlaintext =
            new PhiFunctionPlaintext<>(_group, buildBaseElementsList());

        Verifier<T, E> verifier =
            new Verifier<>(_group, phiFunctionPlaintext,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return verifier.verify(buildListPublicValues(), _proof,
            PLAINTEXT_PROOF_AUXILIARY_DATA);
    }

    private List<E> buildListPublicValues()
            throws GeneralCryptoLibException {

        List<E> dividedCiphertext =
            _divider.divide(_phis, _plaintext, _group);

        List<E> publicValues = new ArrayList<>();
        publicValues.add(_gamma);
        publicValues.addAll(dividedCiphertext);

        return publicValues;
    }

    @SuppressWarnings("unchecked")
    private List<E> buildBaseElementsList() {

        List<E> baseElements = new ArrayList<>();
        baseElements.add(_group.getGenerator());
        baseElements.addAll((List<? extends E>) _publicKey.getKeys());
        return baseElements;
    }
}
