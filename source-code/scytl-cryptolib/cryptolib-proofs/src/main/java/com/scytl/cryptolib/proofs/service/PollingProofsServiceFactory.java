/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.service;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.cryptolib.commons.concurrent.PooledProxiedServiceFactory;

/**
 * This Service factory creates thread-safe services. Thread-safe services
 * proxy all requests to a pool of non thread-safe service instances.
 */
public class PollingProofsServiceFactory extends
        PooledProxiedServiceFactory<ProofsServiceAPI> {
    /**
     * Constructor that uses default values.
     */
    public PollingProofsServiceFactory() {
        super(new BasicProofsServiceFactory(),
            new GenericObjectPoolConfig());
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param path
     *            the path to the properties file.
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingProofsServiceFactory(final String path,
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicProofsServiceFactory(path), poolConfig);
    }

    /**
     * Constructor that uses the default path to the cryptographic properties
     * and the provided configuration of the pool to setup the pool of services.
     * 
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingProofsServiceFactory(
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicProofsServiceFactory(), poolConfig);
    }

}
