/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.function;

import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;

/**
 * An implementation of Maurer's PHI function for providing a Schnorr proof.
 * <P>
 * Uses the method implementation in its superclass {@code PhiFunction} to
 * actually perform the calculations. When an instance of this class is created,
 * it sets the number of inputs to one, the number of outputs to one, and the
 * computation rules to the rules for the Schnorr proof.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            mathematical group that will be used during this exchange.
 */
public final class PhiFunctionSchnorr<T> extends PhiFunction<T> {

    /**
     * Creates an instance of {@code PhiFunctionSchnorr}.
     * <P>
     * Note: the number of inputs and the number of outputs are both set to one,
     * and the computation rules are set to the following:
     * <P>
     * {@code { { 1, 1 } } } }
     * 
     * @param group
     *            the mathematical group on which this PHI function operates.
     * @param baseElements
     *            a list of base elements, of type {@code <E>}, which are
     *            members of the received mathematical group.
     * @throws GeneralCryptoLibException
     *             if arguments are invalid.
     */
    public <E extends GroupElement<T>> PhiFunctionSchnorr(
            final MathematicalGroup<E> group, final List<E> baseElements)
            throws GeneralCryptoLibException {

        super(group, 1, 1, baseElements, new int[][][] {{{1, 1 } } });
    }
}
