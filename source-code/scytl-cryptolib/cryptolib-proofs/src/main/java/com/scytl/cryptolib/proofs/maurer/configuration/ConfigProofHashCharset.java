/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.configuration;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Defines character sets that are supported during the generation and
 * verification of proofs.
 */
public enum ConfigProofHashCharset {

    UTF8(StandardCharsets.UTF_8);

    private final Charset _charset;

    ConfigProofHashCharset(final Charset algorithm) {
        _charset = algorithm;
    }

    public Charset getCharset() {
        return _charset;
    }
}
