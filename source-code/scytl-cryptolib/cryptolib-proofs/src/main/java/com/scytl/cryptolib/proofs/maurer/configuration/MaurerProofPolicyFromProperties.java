/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * A Maurer Unified Proof policy obtained from a properties source.
 */
public final class MaurerProofPolicyFromProperties implements
        MaurerProofPolicy {

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    private final ConfigSecureRandomAlgorithmAndProvider _configSecureRandomAlgorithmAndProvider;

    /**
     * Creates an instance of the class from the specified path.
     * 
     * @param path
     *            a path to the file with the required properties.
     * @throws CryptoLibException
     *            if the required properties are invalid.
     */
    public MaurerProofPolicyFromProperties(final String path) {

        PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);
        
        _configMessageDigestAlgorithmAndProvider =
                ConfigMessageDigestAlgorithmAndProvider
                .valueOf(helper
                    .getNotBlankPropertyValue("proofs.hashbuilder.messagedigest"));
        
        _configProofHashCharset =
                ConfigProofHashCharset
                .valueOf(helper
                    .getNotBlankPropertyValue("proofs.hashbuilder.charset"));
        
        _configSecureRandomAlgorithmAndProvider =
                ConfigSecureRandomAlgorithmAndProvider
                .valueOf(helper
                    .getNotBlankOSDependentPropertyValue("proofs.securerandom"));
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
        return _configSecureRandomAlgorithmAndProvider;
    }

    @Override
    public ConfigMessageDigestAlgorithmAndProvider getMessageDigestAlgorithmAndProvider() {
        return _configMessageDigestAlgorithmAndProvider;
    }

    @Override
    public ConfigProofHashCharset getCharset() {
        return _configProofHashCharset;
    }

}
