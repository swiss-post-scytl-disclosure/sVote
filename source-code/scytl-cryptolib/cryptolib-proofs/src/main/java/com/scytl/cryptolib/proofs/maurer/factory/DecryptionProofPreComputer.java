/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.bean.ProofPreComputedValues;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionDecryption;
import com.scytl.cryptolib.securerandom.cryptoapi.CryptoAPIRandomInteger;

/**
 * Decryption proof pre-computer.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public class DecryptionProofPreComputer<T, E extends GroupElement<T>> {

    private final E _gamma;

    private final MathematicalGroup<E> _group;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    private final PhiFunctionDecryption<T> _phiFunctionDecryption;

    /**
     * Default constructor.
     * 
     * @param privateKeyLength
     *            the number of exponents in the ElGamal private key used for
     *            decryption.
     * @param gamma
     *            the pre-computed gamma of the ElGamal encrypted plaintext.
     * @param group
     *            the Zp subgroup used for ElGamal encryption.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    DecryptionProofPreComputer(
            final int privateKeyLength,
            final E gamma,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset)
            throws GeneralCryptoLibException {

        _gamma = gamma;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

        _phiFunctionDecryption =
            new PhiFunctionDecryption<>(_group, privateKeyLength,
                buildListOfBaseElements());
    }

    /**
     * Pre-computes the values needed for proof generation.
     * 
     * @param cryptoRandomInteger
     *            the random number generator used to create the proof secrets.
     * @return the pre-computed values.
     * @throws GeneralCryptoLibException
     *             if an error occurs during the pre-computation process.
     */
    public ProofPreComputedValues preCompute(
            final CryptoAPIRandomInteger cryptoRandomInteger)
            throws GeneralCryptoLibException {

        Prover<T, E> prover =
            new Prover<>(_group, _phiFunctionDecryption,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return prover.preCompute(cryptoRandomInteger);
    }

    private List<E> buildListOfBaseElements() {

        List<E> list = new ArrayList<>();
        list.add(_group.getGenerator());
        list.add(_gamma);
        return list;
    }
}
