/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import static com.scytl.cryptolib.proofs.maurer.configuration.Constants.SIMPLE_PLAINTEXT_EQUALITY_PROOF_AUXILIARY_DATA;

import java.util.ArrayList;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.MathematicalGroup;
import com.scytl.cryptolib.mathematical.groups.activity.GroupElementsDivider;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.bean.ProofPreComputedValues;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.function.PhiFunctionSimplePlaintextEquality;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Simple plaintext equality proof generator.
 * 
 * @param <T>
 *            the type which parameterizes the group elements of the
 *            {@link MathematicalGroup} that will be used during this exchange.
 * @param <E>
 *            the type of the group elements of the {@link MathematicalGroup}
 *            that will be used during this exchange. E must be a type which
 *            extends GroupElement{@code <T>}.
 */
public final class SimplePlaintextEqualityProofGenerator<T, E extends GroupElement<T>> {

    private final E _primaryCiphertextGamma;

    private final List<E> _primaryCiphertextPhis;

    private final ElGamalPublicKey _primaryPublicKey;

    private final Exponent _exponent;

    private final List<E> _secondaryCiphertextPhis;

    private final ElGamalPublicKey _secondaryPublicKey;

    private final MathematicalGroup<E> _group;

    private final GroupElementsDivider _divider;

    private final ConfigMessageDigestAlgorithmAndProvider _configMessageDigestAlgorithmAndProvider;

    private final ConfigProofHashCharset _configProofHashCharset;

    private final ProofPreComputedValues _preComputedValues;

    /**
     * Default constructor.
     * 
     * @param primaryCiphertext
     *            the primary ElGamal encrypted plaintext.
     * @param primaryPublicKey
     *            the primary ElGamal public key.
     * @param exponent
     *            the ElGamal encryption exponent, which acts as the witness for
     *            this proof.
     * @param secondaryCiphertext
     *            the secondary ElGamal encrypted plaintext.
     * @param secondaryPublicKey
     *            the secondary ElGamal public key.
     * @param group
     *            the Zp subgroup used for ElGamal encryption.
     * @param configMessageDigestAlgorithmAndProvider
     *            the algorithm and provider of the message digest used to
     *            generate the proof.
     * @param configProofHashCharset
     *            the character set of the hash used to generate the proof.
     * @param preComputedValues
     *            the pre-computed values used to generate the proof.
     * @throws GeneralCryptoLibException
     *             if the phi function cannot be created.
     */
    SimplePlaintextEqualityProofGenerator(
            final List<E> primaryCiphertext,
            final ElGamalPublicKey primaryPublicKey,
            final Exponent exponent,
            final List<E> secondaryCiphertext,
            final ElGamalPublicKey secondaryPublicKey,
            final MathematicalGroup<E> group,
            final ConfigMessageDigestAlgorithmAndProvider configMessageDigestAlgorithmAndProvider,
            final ConfigProofHashCharset configProofHashCharset,
            final ProofPreComputedValues preComputedValues)
            throws GeneralCryptoLibException {

        _primaryCiphertextGamma = primaryCiphertext.get(0);
        _primaryCiphertextPhis =
            new ArrayList<>(primaryCiphertext.subList(1,
                primaryCiphertext.size()));

        _secondaryCiphertextPhis =
            new ArrayList<>(secondaryCiphertext.subList(1,
                secondaryCiphertext.size()));

        _primaryPublicKey = primaryPublicKey;
        _exponent = exponent;
        _secondaryPublicKey = secondaryPublicKey;
        _group = group;

        _configMessageDigestAlgorithmAndProvider =
            configMessageDigestAlgorithmAndProvider;
        _configProofHashCharset = configProofHashCharset;

        _preComputedValues = preComputedValues;

        _divider = new GroupElementsDivider();
    }

    /**
     * Generates a simple plaintext equality proof.
     * 
     * @return the generated proof.
     * @throws GeneralCryptoLibException
     *             if (@code SimplePlaintextEqualityProofGenerator) was
     *             initialized by the invalid arguments.
     */
    public Proof generate() throws GeneralCryptoLibException {

        PhiFunctionSimplePlaintextEquality<T> phiFunctionSimplePlaintextEquality =
            new PhiFunctionSimplePlaintextEquality<>(_group,
                buildListOfBaseElements());

        Prover<T, E> prover =
            new Prover<>(_group, phiFunctionSimplePlaintextEquality,
                _configMessageDigestAlgorithmAndProvider,
                _configProofHashCharset);

        return prover.prove(buildInputList(), buildListExponents(),
            SIMPLE_PLAINTEXT_EQUALITY_PROOF_AUXILIARY_DATA,
            _preComputedValues);
    }

    private List<Exponent> buildListExponents() {

        List<Exponent> exponentList = new ArrayList<>();
        exponentList.add(_exponent);

        return exponentList;
    }

    @SuppressWarnings("unchecked")
    private List<E> buildListOfBaseElements() {

        List<E> invertedSecondaryKeyValues =
            buildInvertedSecondaryPublicKey();

        List<E> baseElements = new ArrayList<>();
        baseElements.add(_group.getGenerator());
        baseElements.addAll((List<? extends E>) _primaryPublicKey
            .getKeys());
        baseElements.addAll(invertedSecondaryKeyValues);

        return baseElements;
    }

    private List<E> buildInputList() throws GeneralCryptoLibException {

        List<E> dividedSubCiphertext =
            _divider.divide(_primaryCiphertextPhis,
                _secondaryCiphertextPhis, _group);

        List<E> generatedList = new ArrayList<>();
        generatedList.add(_primaryCiphertextGamma);
        generatedList.addAll(dividedSubCiphertext);

        return generatedList;
    }

    @SuppressWarnings("unchecked")
    private List<E> buildInvertedSecondaryPublicKey() {

        List<E> listInvertedElements = new ArrayList<>();
        List<? extends E> originalList =
            (List<? extends E>) _secondaryPublicKey.getKeys();
        for (E e : originalList) {
            listInvertedElements.add((E) e.invert());
        }

        return listInvertedElements;
    }
}
