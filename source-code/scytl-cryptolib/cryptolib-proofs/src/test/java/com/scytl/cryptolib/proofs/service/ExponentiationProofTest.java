/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.service;

import static org.junit.Assert.assertFalse;

import java.security.Security;
import java.util.List;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.cryptoapi.Witness;
import com.scytl.cryptolib.elgamal.utils.ElGamalTestDataGenerator;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.cryptolib.mathematical.groups.utils.MathematicalTestDataGenerator;
import com.scytl.cryptolib.proofs.bean.ProofPreComputedValues;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Tests of the Proofs service API for exponentiation zero knowledge proofs of
 * knowledge.
 */
public class ExponentiationProofTest {

    private static final int NUM_BASE_ELEMENTS = 5;

    private static ProofsService _proofsServiceForDefaultPolicy;

    private static ZpSubgroup _zpSubgroup;

    private static Witness _witness;

    private static List<ZpGroupElement> _baseElements;

    private static List<ZpGroupElement> _exponentiatedElements;

    private static Proof _proof;

    private static ProofPreComputedValues _preComputedValues;

    private static ZpSubgroup _differentZpSubgroup;

    private static List<ZpGroupElement> _differentBaseElements;

    @BeforeClass
    public static void setUp() throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        _proofsServiceForDefaultPolicy = new ProofsService();

        _zpSubgroup = MathematicalTestDataGenerator.getZpSubgroup();

        _witness = ElGamalTestDataGenerator.getWitness(_zpSubgroup);
        _baseElements = MathematicalTestDataGenerator
            .getZpGroupElements(_zpSubgroup, NUM_BASE_ELEMENTS);
        _exponentiatedElements =
            MathematicalTestDataGenerator.exponentiateZpGroupElements(
                _baseElements, _witness.getExponent());

        _proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup).createExponentiationProof(
                _exponentiatedElements, _baseElements, _witness);

        _preComputedValues = _proofsServiceForDefaultPolicy
            .createProofPreComputerAPI(_zpSubgroup)
            .preComputeExponentiationProof(_baseElements);

        _differentZpSubgroup =
            MathematicalTestDataGenerator.getQrSubgroup();

        do {
            _differentBaseElements = MathematicalTestDataGenerator
                .getZpGroupElements(_zpSubgroup, NUM_BASE_ELEMENTS);
        } while (_differentBaseElements.equals(_baseElements));
    }

    @AfterClass
    public static void tearDown() {

        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public final void whenGenerateAndVerifyProofThenOk()
            throws GeneralCryptoLibException {

        Assert.assertTrue(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                _exponentiatedElements, _baseElements, _proof));
    }

    @Test
    public final void whenGenerateProofUsingPreComputedValuesAndVerifyThenOk()
            throws GeneralCryptoLibException {

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup)
            .createExponentiationProof(_exponentiatedElements,
                _baseElements, _witness, _preComputedValues);

        Assert.assertTrue(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                _exponentiatedElements, _baseElements, proof));
    }

    @Test
    public final void whenSerializeAndDeserializeProofThenOk()
            throws GeneralCryptoLibException {

        Proof deserializedProof = Proof.fromJson(_proof.toJson());

        Assert.assertTrue(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                _exponentiatedElements, _baseElements, deserializedProof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidPreComputedValuesThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        ProofPreComputedValues differentPreComputedValues;
        do {
            differentPreComputedValues = _proofsServiceForDefaultPolicy
                .createProofPreComputerAPI(_zpSubgroup)
                .preComputeExponentiationProof(_differentBaseElements);
        } while (differentPreComputedValues.getExponents()
            .equals(_preComputedValues.getExponents()));

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup)
            .createExponentiationProof(_exponentiatedElements,
                _baseElements, _witness, differentPreComputedValues);

        assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                _exponentiatedElements, _baseElements, proof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidfBaseElementsThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup).createExponentiationProof(
                _exponentiatedElements, _differentBaseElements, _witness);

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                _exponentiatedElements, _differentBaseElements, proof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidWitnessThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Witness differentWitness;
        do {
            differentWitness =
                ElGamalTestDataGenerator.getWitness(_zpSubgroup);
        } while (differentWitness.getExponent()
            .equals(_witness.getExponent()));

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup).createExponentiationProof(
                _exponentiatedElements, _baseElements, differentWitness);

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                _exponentiatedElements, _baseElements, proof));
    }

    @Test
    public final void whenVerifyProofUsingInvalidExponentiatedElementsThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        List<ZpGroupElement> differentExponentiatedElements =
            MathematicalTestDataGenerator.exponentiateZpGroupElements(
                _differentBaseElements, _witness.getExponent());

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                differentExponentiatedElements, _baseElements, _proof));
    }

    @Test
    public final void whenVerifyProofUsingInvalidBaseElementsThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyExponentiationProof(
                _exponentiatedElements, _differentBaseElements, _proof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidMathematicalGroupThenExceptionThrown()
            throws GeneralCryptoLibException {

        thrown.expect(GeneralCryptoLibException.class);
        thrown.expectMessage(
            "The exponent should be of the same Zp subgroup order as this Zp group element");

        _proofsServiceForDefaultPolicy
            .createProofProverAPI(_differentZpSubgroup)
            .createExponentiationProof(_exponentiatedElements,
                _baseElements, _witness);
    }

    @Test
    public final void whenVerifyProofUsingInvalidMathematicalGroupThenExceptionThrown()
            throws GeneralCryptoLibException {

        thrown.expect(GeneralCryptoLibException.class);
        thrown.expectMessage(
            "All elements of list of public values must be elements of mathematical group associated with proof.");

        _proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_differentZpSubgroup)
            .verifyExponentiationProof(_exponentiatedElements,
                _baseElements, _proof);
    }
}
