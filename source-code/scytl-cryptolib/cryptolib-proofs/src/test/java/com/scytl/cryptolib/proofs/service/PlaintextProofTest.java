/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.service;

import java.security.Security;
import java.util.List;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalEncrypterValues;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.elgamal.cryptoapi.Witness;
import com.scytl.cryptolib.elgamal.utils.ElGamalTestDataGenerator;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.cryptolib.mathematical.groups.utils.MathematicalTestDataGenerator;
import com.scytl.cryptolib.proofs.bean.ProofPreComputedValues;
import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Tests of the Proofs service API for plaintext zero knowledge proofs of
 * knowledge.
 */
public class PlaintextProofTest {

    private static final int NUM_PLAINTEXT_ELEMENTS = 6;

    private static ProofsService _proofsServiceForDefaultPolicy;

    private static ZpSubgroup _zpSubgroup;

    private static List<ZpGroupElement> _plaintext;

    private static ElGamalPublicKey _publicKey;

    private static Ciphertext _ciphertext;

    private static Witness _witness;

    private static Proof _proof;

    private static ProofPreComputedValues _preComputedValues;

    private static ZpSubgroup _differentZpSubgroup;

    private static List<ZpGroupElement> _differentPlaintext;

    private static ElGamalPublicKey _differentPublicKey;

    private static Ciphertext _differentCiphertext;

    private static Witness _differentWitness;

    @BeforeClass
    public static void setUp() throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        _proofsServiceForDefaultPolicy = new ProofsService();

        _zpSubgroup = MathematicalTestDataGenerator.getZpSubgroup();

        _publicKey = ElGamalTestDataGenerator
            .getKeyPair(_zpSubgroup, NUM_PLAINTEXT_ELEMENTS)
            .getPublicKeys();

        _plaintext = MathematicalTestDataGenerator
            .getZpGroupElements(_zpSubgroup, NUM_PLAINTEXT_ELEMENTS);

        ElGamalEncrypterValues encrypterValues =
            (ElGamalEncrypterValues) ElGamalTestDataGenerator
                .encryptGroupElements(_publicKey, _plaintext);
        _ciphertext = encrypterValues;
        _witness = encrypterValues;

        _proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup).createPlaintextProof(
                _publicKey, _ciphertext, _plaintext, _witness);

        _preComputedValues = _proofsServiceForDefaultPolicy
            .createProofPreComputerAPI(_zpSubgroup)
            .preComputePlaintextProof(_publicKey);

        _differentZpSubgroup =
            MathematicalTestDataGenerator.getQrSubgroup();

        do {
            _differentPublicKey = ElGamalTestDataGenerator
                .getKeyPair(_zpSubgroup, NUM_PLAINTEXT_ELEMENTS)
                .getPublicKeys();
        } while (_differentPublicKey.equals(_publicKey));

        do {
            _differentPlaintext = MathematicalTestDataGenerator
                .getZpGroupElements(_zpSubgroup, NUM_PLAINTEXT_ELEMENTS);
        } while (_differentPlaintext.equals(_plaintext));

        do {
            encrypterValues =
                (ElGamalEncrypterValues) ElGamalTestDataGenerator
                    .encryptGroupElements(_publicKey, _differentPlaintext);
            _differentCiphertext = encrypterValues;
            _differentWitness = encrypterValues;
        } while (_differentWitness.getExponent()
            .equals(_witness.getExponent()));
    }

    @AfterClass
    public static void tearDown() {

        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public final void whenGenerateAndVerifyProofThenOk()
            throws GeneralCryptoLibException {

        Assert.assertTrue(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _ciphertext, _plaintext, _proof));
    }

    @Test
    public final void whenGenerateProofUsingPreComputedValuesAndVerifyThenOk()
            throws GeneralCryptoLibException {

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup)
            .createPlaintextProof(_publicKey, _ciphertext, _plaintext,
                _witness, _preComputedValues);

        Assert.assertTrue(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _ciphertext, _plaintext, proof));
    }

    @Test
    public final void whenSerializeAndDeserializeProofThenOk()
            throws GeneralCryptoLibException {

        Proof deserializedProof = Proof.fromJson(_proof.toJson());

        Assert.assertTrue(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _ciphertext, _plaintext, deserializedProof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidPreComputedValuesThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        ProofPreComputedValues differentPreComputedValues;
        do {
            differentPreComputedValues = _proofsServiceForDefaultPolicy
                .createProofPreComputerAPI(_zpSubgroup)
                .preComputePlaintextProof(_differentPublicKey);
        } while (differentPreComputedValues.getExponents()
            .equals(_preComputedValues.getExponents()));

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup)
            .createPlaintextProof(_publicKey, _ciphertext, _plaintext,
                _witness, differentPreComputedValues);

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _ciphertext, _plaintext, proof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidPublicKeyThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup).createPlaintextProof(
                _differentPublicKey, _ciphertext, _plaintext, _witness);

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _differentPublicKey, _ciphertext, _plaintext, proof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidPlaintextThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup).createPlaintextProof(
                _publicKey, _ciphertext, _differentPlaintext, _witness);

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _ciphertext, _differentPlaintext, proof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidWitnessThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Proof proof = _proofsServiceForDefaultPolicy
            .createProofProverAPI(_zpSubgroup).createPlaintextProof(
                _publicKey, _ciphertext, _plaintext, _differentWitness);

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _ciphertext, _plaintext, proof));
    }

    @Test
    public final void whenVerifyProofUsingInvalidPublicKeyThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _differentPublicKey, _ciphertext, _plaintext, _proof));
    }

    @Test
    public final void whenVerifyProofUsingInvalidCiphertextThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _differentCiphertext, _plaintext, _proof));
    }

    @Test
    public final void whenVerifyProofUsingInvalidPlaintextThenVerificationIsFalse()
            throws GeneralCryptoLibException {

        Assert.assertFalse(_proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_zpSubgroup).verifyPlaintextProof(
                _publicKey, _ciphertext, _differentPlaintext, _proof));
    }

    @Test
    public final void whenGenerateProofUsingInvalidMathematicalGroupThenExceptionThrown()
            throws GeneralCryptoLibException {

        thrown.expect(GeneralCryptoLibException.class);
        thrown.expectMessage(
            "The exponent should be of the same Zp subgroup order as this Zp group element");

        _proofsServiceForDefaultPolicy
            .createProofProverAPI(_differentZpSubgroup)
            .createPlaintextProof(_publicKey, _ciphertext, _plaintext,
                _witness);
    }

    @Test
    public final void whenVerifyProofUsingInvalidMathematicalGroupThenExceptionThrown()
            throws GeneralCryptoLibException {

        thrown.expect(GeneralCryptoLibException.class);
        thrown.expectMessage(
            "All elements of list of public values must be elements of mathematical group associated with proof.");

        _proofsServiceForDefaultPolicy
            .createProofVerifierAPI(_differentZpSubgroup)
            .verifyPlaintextProof(_publicKey, _ciphertext, _plaintext,
                _proof);
    }
}
