/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.maurer.factory;

import com.scytl.cryptolib.elgamal.configuration.ConfigGroupType;
import com.scytl.cryptolib.elgamal.configuration.ElGamalPolicy;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * Implementation of {@link ElGamalPolicy}, used for testing.
 */
public class ElGamalPolicyImpl implements ElGamalPolicy {

    private final ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmProviderPair;
    private final ConfigGroupType _groupType;


    /**
     * Creates an instance of policy with provided
     * {@code secureRandomAlgorithmProviderPair}.
     * 
     * @param secureRandomAlgorithmProviderPair
     *            secure random number generation cryptographic policy.
     */
    public ElGamalPolicyImpl(
            final ConfigSecureRandomAlgorithmAndProvider secureRandomAlgorithmProviderPair,
            final ConfigGroupType groupType) {

        _secureRandomAlgorithmProviderPair = secureRandomAlgorithmProviderPair;
        _groupType = groupType;
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
        return _secureRandomAlgorithmProviderPair;
    }

    @Override
    public ConfigGroupType getGroupType() {
        return _groupType;
    }
}
