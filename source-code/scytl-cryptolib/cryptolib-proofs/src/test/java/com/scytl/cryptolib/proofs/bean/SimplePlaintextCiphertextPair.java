/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.proofs.bean;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.elgamal.cryptoapi.Witness;

/**
 * Container class for a pair of ElGamal ciphertexts suitable for use with
 * simple plaintext equality zero knowledge proofs of knowledge.
 */
public class SimplePlaintextCiphertextPair {

    private final Ciphertext _primaryCiphertext;

    private final Ciphertext _secondaryCiphertext;

    private final Witness _witness;

    /**
     * @param primaryCiphertext
     *            the primary ElGamal ciphertext for a simple plaintext zero
     *            knowledge proof of knowledge.
     * @param secondaryCiphertext
     *            the secondary ElGamal ciphertext for a simple plaintext zero
     *            knowledge proof of knowledge.
     * @param witness
     *            the witness wrapping the randomly generated exponent used to
     *            create the ciphertext pair.
     */
    public SimplePlaintextCiphertextPair(
            final Ciphertext primaryCiphertext,
            final Ciphertext secondaryCiphertext, final Witness witness) {

        _primaryCiphertext = primaryCiphertext;
        _secondaryCiphertext = secondaryCiphertext;
        _witness = witness;
    }

    public Ciphertext getPrimaryCiphertext() {

        return _primaryCiphertext;
    }

    public Ciphertext getSecondaryCiphertext() {

        return _secondaryCiphertext;
    }

    public Witness getWitness() {

        return _witness;
    }
}
