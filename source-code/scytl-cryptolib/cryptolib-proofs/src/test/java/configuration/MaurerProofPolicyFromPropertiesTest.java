/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package configuration;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.proofs.maurer.configuration.ConfigProofHashCharset;
import com.scytl.cryptolib.proofs.maurer.configuration.MaurerProofPolicyFromProperties;

/**
 * Tests of MaurerProofPolicyFromProperties.
 */
public class MaurerProofPolicyFromPropertiesTest {

    private static MaurerProofPolicyFromProperties _maurerProofPolicyFromProperties;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _maurerProofPolicyFromProperties =
            new MaurerProofPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);
    }

    @Test
    public void givenPolicyWhenGetAlgorithmThenExpected() {

        ConfigMessageDigestAlgorithmAndProvider expectedConfigProofHashAlgorithm =
            ConfigMessageDigestAlgorithmAndProvider.SHA256_SUN;

        assertEquals(expectedConfigProofHashAlgorithm,
            _maurerProofPolicyFromProperties
                .getMessageDigestAlgorithmAndProvider());
    }

    @Test
    public void givenPolicyWhenGetCharsetThenExpected() {

        ConfigProofHashCharset expectedConfigProofHashCharset =
            ConfigProofHashCharset.UTF8;

        assertEquals(expectedConfigProofHashCharset,
            _maurerProofPolicyFromProperties.getCharset());
    }
}
