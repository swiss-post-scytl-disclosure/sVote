/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.derivation.cryptoapi;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Interface which provides methods to derive a {@link CryptoAPIDerivedKey},
 * using a password based key derivation function.
 */
public interface CryptoAPIPBKDFDeriver {

    /**
     * Derives a {@link CryptoAPIDerivedKey} from a given password. For security
     * reasons, the password must contain a minimum of 16 characters.
     * 
     * @param password
     *            The user password.
     * @return The derived {@link CryptoAPIDerivedKey}.
     * @throws GeneralCryptoLibException
     *             if the password is invalid.
     */
    CryptoAPIDerivedKey deriveKey(char[] password)
            throws GeneralCryptoLibException;

    /**
     * Derives a {@link CryptoAPIDerivedKey} from a given password and a given
     * salt. For security reasons, the password must contain a minimum of 16
     * characters.
     * 
     * @param password
     *            the password.
     * @param salt
     *            the salt.
     * @return the derived {@link CryptoAPIDerivedKey}.
     * @throws GeneralCryptoLibException
     *             if the password or the salt are invalid.
     */
    CryptoAPIDerivedKey deriveKey(final char[] password, final byte[] salt)
            throws GeneralCryptoLibException;
}
