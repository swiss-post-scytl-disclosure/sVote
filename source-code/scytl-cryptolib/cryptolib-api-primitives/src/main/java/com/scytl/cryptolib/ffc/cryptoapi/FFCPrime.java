/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.ffc.cryptoapi;

import java.math.BigInteger;

/**
 * Prime value generated as it is specified in FIPS PUB 186-4, sections A, C
 * accompanied by data necessary for its validation..
 * <p>
 * Instances of this class are immutable.
 */
public final class FFCPrime {
    private final BigInteger value;

    private final BigInteger seed;

    private final int genCounter;

    /**
     * Constructor.
     * 
     * @param value
     * @param seed
     * @param genCounter
     */
    public FFCPrime(BigInteger value, BigInteger seed, int genCounter) {
        this.value = value;
        this.seed = seed;
        this.genCounter = genCounter;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FFCPrime other = (FFCPrime) obj;
        if (genCounter != other.genCounter) {
            return false;
        }
        if (seed == null) {
            if (other.seed != null) {
                return false;
            }
        } else if (!seed.equals(other.seed)) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    /**
     * Returns the generator counter.
     * 
     * @return the generator counter.
     */
    public int genCounter() {
        return genCounter;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + genCounter;
        result = prime * result + ((seed == null) ? 0 : seed.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /**
     * Returns the seed.
     * 
     * @return the seed.
     */
    public BigInteger seed() {
        return seed;
    }

    @Override
    public String toString() {
        return "FFCPrime [value=" + value + ", seed=" + seed
            + ", genCounter=" + genCounter + "]";
    }

    /**
     * Returns the value.
     * 
     * @return the value.
     */
    public BigInteger value() {
        return value;
    }
}
