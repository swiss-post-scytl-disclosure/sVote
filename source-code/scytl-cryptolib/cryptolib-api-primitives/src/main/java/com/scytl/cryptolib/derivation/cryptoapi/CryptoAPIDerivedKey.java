/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.derivation.cryptoapi;

/**
 * Interface for a class that encapsulates a derived key.
 */
public interface CryptoAPIDerivedKey {

    /**
     * Returns the byte representation of the key.
     * 
     * @return The byte[] representation of the key.
     */
    byte[] getEncoded();
}
