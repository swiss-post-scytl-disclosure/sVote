/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.securerandom.cryptoapi;

import java.util.Set;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Interface that provides the functionality to obtain random strings.
 */
public interface CryptoAPIRandomString {

    /**
     * Creates a random string according to the specified {@code lengthInChars}.
     * 
     * @param lengthInChars
     *            The lengthInChars that specifies the number of characters to
     *            be generated.
     * @return A random string.
     * @throws GeneralCryptoLibException
     *             if {@code lengthInChars} is out of the range for this
     *             generator.
     * @see java.security.SecureRandom#nextBytes(byte[] bytes)
     */
    String nextRandom(final int lengthInChars)
            throws GeneralCryptoLibException;

    /**
     * Creates a random string according to the specified
     * {@code lengthInChars}. Note: the {@code blackList} cannot contain more
     * elements than: {@code setOfConsideredCharactersLength}^(
     * {@code lengthInChars})-1. Moreover, the {@code blackList} should only
     * contain strings of length equal to {@code lengthInChars}.
     * 
     * @param lengthInChars
     *            The lengthInChars that specifies the number of characters to
     *            be generated.
     * @param blackList
     *            Set of strings in the black list.
     * @return A random string.
     * @throws GeneralCryptoLibException
     *             if {@code lengthInChars} is out of the range for this
     *             generator.
     * @throws GeneralCryptoLibException
     *             if the {@code blackList} contains too many elements.
     * @throws GeneralCryptoLibException
     *             if the {@code blackList} contains any element with a length
     *             different than the given {@code lengthInChars}.
     * @see java.security.SecureRandom#nextBytes(byte[] bytes)
     */
    String nextRandom(final int lengthInChars, Set<String> blackList)
            throws GeneralCryptoLibException;

}
