/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * An API for creating Random data, hashing and key deriving.
 * <p>
 *<strong>How to generate two passwords with a standard base32 alphabet</strong>
 * <pre>
 *
PrimitivesService _primitivesService;
StrictlyPositiveInteger TWO;
...

CryptoAPIRandomString randomString =
            _primitivesService.get32CharAlphabetCryptoRandomString();

String pass1 = randomString.nextRandom(TWO);
String pass2 = randomString.nextRandom(TWO);
</pre>
<p>
Best practice: randomString object should be reused instead of getting a new instance each time
 */
package com.scytl.cryptolib.api.primitives;

