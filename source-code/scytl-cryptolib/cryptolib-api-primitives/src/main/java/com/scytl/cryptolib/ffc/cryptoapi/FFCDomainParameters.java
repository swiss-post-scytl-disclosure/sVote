/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.ffc.cryptoapi;

import static java.util.Objects.requireNonNull;

import java.math.BigInteger;

/**
 * Finite Field Cryptography (FFC) domain parameters.
 * <p>
 * Instances of this class are immutable.
 */
public final class FFCDomainParameters {
    private final FFCPrimes primes;

    private final BigInteger g;

    /**
     * Constructor.
     * 
     * @param primes
     *            the primes
     * @param g
     *            the generator
     */
    public FFCDomainParameters(FFCPrimes primes, BigInteger g) {
        this.primes = requireNonNull(primes, "Primes are null.");
        this.g = requireNonNull(g, "G is null.");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FFCDomainParameters other = (FFCDomainParameters) obj;
        if (g == null) {
            if (other.g != null) {
                return false;
            }
        } else if (!g.equals(other.g)) {
            return false;
        }
        if (primes == null) {
            if (other.primes != null) {
                return false;
            }
        } else if (!primes.equals(other.primes)) {
            return false;
        }
        return true;
    }

    /**
     * Returns the generator.
     * 
     * @return the generator.
     */
    public BigInteger g() {
        return g;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((g == null) ? 0 : g.hashCode());
        result =
            prime * result + ((primes == null) ? 0 : primes.hashCode());
        return result;
    }

    /**
     * Returns the primes.
     * 
     * @return the primes.
     */
    public FFCPrimes primes() {
        return primes;
    }

    @Override
    public String toString() {
        return "FFCDomainParameters [primes=" + primes + ", g=" + g + "]";
    }
}
