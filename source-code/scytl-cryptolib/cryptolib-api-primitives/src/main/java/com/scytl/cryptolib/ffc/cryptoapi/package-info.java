/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Provides API for generation of Finite Field Cryptography (FFC) domain
 * parameters as specified in FIPS PUB 18644
 * {@link http://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.186-4.pdf}.
 */
package com.scytl.cryptolib.ffc.cryptoapi;
