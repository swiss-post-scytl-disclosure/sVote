/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.ffc.cryptoapi;

import java.math.BigInteger;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Finite Field Cryptography (FFC) engine.
 * <p>
 * Implementation must be thread-safe.
 */
public interface FFCEngine {

    /**
     * Generates domain parameters for given bit lengths of the prime numbers as
     * it is specified in FIPS PUB 186-4, section A.
     * <p>
     * This is a shortcut for <pre>
     * <code>
     * FFCPrimes primes = generatePrimes(n, l);
     * BigInteger g = generateG(primes, 1);
     * return new FFCDomainParameters(primes, g);
     * </code> </pre>
     * 
     * @param l
     *            the length of p in bits
     * @param n
     *            the length of q in bits
     * @return the domain parameters
     * @throws GeneralCryptoLibException
     *             the values of n and/or l are invalid
     */
    FFCDomainParameters generateDomainParameters(int l, int n)
            throws GeneralCryptoLibException;

    /**
     * Generates a generator for given pair of prime numbers and an index as it
     * is specified in FIPS PUB 186-4, section A 2.3.
     * 
     * @param primes
     *            the primes
     * @param index
     *            the index
     * @return the generator
     * @throws GeneralCryptoLibException
     *             the primes and/or index are invalid.
     */
    BigInteger generateG(FFCPrimes primes, int index)
            throws GeneralCryptoLibException;

    /**
     * Generates a pair of prime numbers for given bit lengths as it is
     * specified in FIPS PUB 186-4, section A 1.2.
     * 
     * @param l
     *            the length of p in bits
     * @param n
     *            the length of q in bits
     * @return the primes
     * @throws GeneralCryptoLibException
     *             the values of n and/or l are invalid
     */
    FFCPrimes generatePrimes(int l, int n)
            throws GeneralCryptoLibException;

    /**
     * Validates given domain parameters.
     * 
     * @param parameters
     *            the parameters
     * @throws GeneralCryptoLibException
     *             the parameters are invalid.
     */
    void validateDomainParameters(FFCDomainParameters parameters)
            throws GeneralCryptoLibException;

    /**
     * Validates a given generator which is supposed to be generated from the
     * specified primes and index.
     * 
     * @param g
     *            the generator
     * @param primes
     *            the primes
     * @param index
     *            the index
     * @throws GeneralCryptoLibException
     *             the generator is invalid.
     */
    void validateG(BigInteger g, FFCPrimes primes, int index)
            throws GeneralCryptoLibException;

    /**
     * Validates given primes.
     * 
     * @param primes
     *            the primes
     * @throws GeneralCryptoLibException
     *             the primes are invalid.
     */
    void validatePrimes(FFCPrimes primes) throws GeneralCryptoLibException;
}
