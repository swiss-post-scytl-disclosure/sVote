/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
module.exports = function(config) {
  'use strict';
  config.set({
    basePath: '.',
    singleRun: true,
    browserNoActivityTimeout: 60 * 60 * 1000,
    files: ['spec/**/*.js'],
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    preprocessors:
        {'lib/**/*.js': ['coverage', 'webpack'], 'spec/**/*.js': ['webpack']},
    junitReporter: {
      useBrowserName: false,
      outputDir: 'target/reports/junit',
      outputFile: 'TESTS-xunit.xml'
    },
    coverageReporter:
        {type: 'lcov', dir: 'target/reports', subdir: 'coverage'}
  });
};
