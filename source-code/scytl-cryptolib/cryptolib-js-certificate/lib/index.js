/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var CertificateService = require('./service');

module.exports = {
  /**
   * Creates a new CertificateService object, which encapsulates a certificate
   * service.
   *
   * @function newService
   * @global
   * @returns {CertificateService} The new CertificateService object.
   */
  newService: function() {
    return new CertificateService();
  }
};
