/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var certificate = require('../lib/index');

describe('The certificate module should be able to ...', function() {
  var validChain_ = {
    leaf: {
      pem:
          '-----BEGIN CERTIFICATE-----MIICCzCCAXSgAwIBAgIBATANBgkqhkiG9w0BAQUFADBHMRswGQYDVQQDDBJ3aGl0ZSBpbnRlcm1lZGlhdGUxDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwHhcNMTQwOTA1MTAxNDEzWhcNMjQwODEzMTAxNDEzWjA/MRMwEQYDVQQDDAp3aGl0ZSBsZWFmMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3yWZQRc5xk14TIL3/2UGVgNz94dHq7vg4ORduUvtio4nVMSriQNSPnEbAfV9FR8qA6UCzqXQroNX6eSpQfJ4MblyUWUj7VYtY/jC2UxGcE4GJxhngiwfO+It2ejTr6I7EPlj7pwPOVTDWByaDsYWtJ3JeaVhR9Tcf5L+H5k7HTQIDAQABow8wDTALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQEFBQADgYEAQNoV1G2zwceE1/4VppH3eJYi8u1CUWUoaQe1rH3owm9CH9/OfS+MicUIW/7yRi86rtATnhTcfN34E18AwbJsmESBnCkx+WaVZ/WC1jGJ/Dd5rgxvRfZpwd1RKivKzZsoYm19PVFRDg9K9qqA6dkuq29wTnwMV48rQPkfix9lWjg=-----END CERTIFICATE-----',
      keyType: 'Sign',
      subject: {
        commonName: 'white leaf',
        organizationalUnit: 'se',
        organization: 'scytl',
        country: 'ES'
      },
      time: '2014-12-25\'T\'00:00:00Z'
    },
    intermediates: {
      pems: [
        '-----BEGIN CERTIFICATE-----MIICGjCCAYOgAwIBAgIBATANBgkqhkiG9w0BAQUFADA9MREwDwYDVQQDDAh3aGl0ZSBjYTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzAeFw0xNDA5MDUwOTM1MThaFw0yNDA4MjMwOTM1MThaMEcxGzAZBgNVBAMMEndoaXRlIGludGVybWVkaWF0ZTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUmpmT/rNcxIP8Xulj0RcbdvaNGJNoEPMzEGztguB+mKsTPgJIlrKvdskY5NIPd1+hzS+HjvW11uwyT2FnmyfKBqND4EH5BqU8Ph4bAyrT950DPcOGE4vG0fLO0jTD3h0n4d4yugWmrND2sScFZQsGLbgOYX4QM8vV+owoKNRxMCAwEAAaMgMB4wDwYDVR0TBAgwBgEB/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEFBQADgYEAF0B5LQDo3uRs9dSkxp39ER64XIa6XYV0zdbq+fqHg8Qcoggpox7vwmc6YoCNQpfY2zLiIUMbb6VMP4pkp7jZMju9TBKPOjMFrQKom0cowT/gYdokgnsk8yUYOdo46GJR43PjFLTiQk5JDawWaECbhcgyLf59AalX9z9dbY0AwqI=-----END CERTIFICATE-----'
      ],
      subjects: [{
        commonName: 'white intermediate',
        organizationalUnit: 'se',
        organization: 'scytl',
        country: 'ES'
      }]
    },
    root: {
      pem:
          '-----BEGIN CERTIFICATE-----MIICSDCCAbGgAwIBAgIJAPJ2LYuwqa81MA0GCSqGSIb3DQEBBQUAMD0xETAPBgNVBAMMCHdoaXRlIGNhMQ4wDAYDVQQKDAVzY3l0bDELMAkGA1UECwwCc2UxCzAJBgNVBAYTAkVTMB4XDTE0MDkwNTA5MzQwNloXDTI0MDkwMjA5MzQwNlowPTERMA8GA1UEAwwId2hpdGUgY2ExDjAMBgNVBAoMBXNjeXRsMQswCQYDVQQLDAJzZTELMAkGA1UEBhMCRVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnpKp2eeY1hC4V8E1nUIZJpJVhAZk3oJeEbq0Xi2uSYrh6wmfuQIHFs5rYF4yHoGvf+PM66od8ccHa1GNGB/b6lFb4/+2vEvYWA51OWY9BBdgjhAsqoks9ANNDQ1oxX4dqLwjM/GDDg2v9xoT9tj0CaJnieQbhMkH8oQveBBR57AgMBAAGjUDBOMB0GA1UdDgQWBBRwKg/RntWH8EvCkEX3jtHuvSpbwTAfBgNVHSMEGDAWgBRwKg/RntWH8EvCkEX3jtHuvSpbwTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBADk/OOoc2tbfxja95RqH22EcAVJ/mDin2ev9cXhZ2A2xh4SrUad0fNsyFnY+m3EzfA0KPrI46sEPDeuWqwnGAYccmp7ylVIS6edYRvnI5mdLze2CnuDNJYQzZXjASQF2ih5wTNm3VMZkN0SqGQZiOCa9NQHPfmNj8mJe0Ooj2b3o-----END CERTIFICATE-----'
    }
  };

  var validChainFromJava_ = {
    leaf: {
      pem:
          '-----BEGIN CERTIFICATE-----MIIDeDCCAmCgAwIBAgIVAJ81fEEjKAKK8Yg5D654L3suMkbVMA0GCSqGSIb3DQEBCwUAMFExEjAQBgNVBAMMCVZvdGVycyBDQTETMBEGA1UECwwKc2cxMDAxLTAwMDEOMAwGA1UECgwFTlNXRUMxCTAHBgNVBAcMADELMAkGA1UEBhMCQVUwHhcNMTQwOTA4MDc0NDExWhcNMTUwOTA4MDc0MzU0WjB4MTkwNwYDVQQDDDBWb3RlclNpZ24gNDExNTQ5NzQxMjM5NzMxMjgwNDQ0MjI3NzQxNzUxODczMDY5OTcxEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqYeNTbS15+IpxgXEXivFK5Rt3I5du1NDgYdxRRebYqWi/0t2LsyAljZkAqWbgyQswJbXwpuhJmlSlz2RNr04PBxxm93BMoVGXlbVGEGAmk3ZkwVB0VjSFZkJHvARmJ9oudYC7uUyxtr8brPB16Q8FIv2j9CSPMWUXa0YbxLFgdZmJ6Ulu5B3RToBvpXL0aFNiIpIm4Vz5f0N4f+AxV1CV8vwzoQpefuGuss32WIi1vrhx0j1+K6hWGx8xgID/yKEIBPsKupjjcPhNxdzZepI0FOSpPnjjVMHHWgEaiRloRk66T7FiKOPkanazQBbo/zULRrKfvXkQ4NAnoV29T7GBwIDAQABoyAwHjAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIGwDANBgkqhkiG9w0BAQsFAAOCAQEADghhwPmZ2LTPhtVyKtBa1ljNcNODVRrLFX05h9ZIVojteI6BGE+xM/IagfsaOrK804T0psbdWEpWQJqXJtbI5TEiFAbliVdFOKv7Al5jnd2HNqauU3YWoZ2jy32wbtBnTpR8E8ILJLsMMyvy+ZSRUKyHaboOsphy1dtyncACNnuLhLtSC1IeB2LbFOLZWnU7dnKETAwMCBJ3ELlhFIPPwLLQjkLtX3Z4kYWRNUB/ePoKGfXPhBYXaY4KE135kA+5GNpIRwzZmh8VrDo3aTLEAJZD0xDv4ohir5FWcnMPdUvmwfya/mZZhns94y5aiql/kTJPl0f0G1t0AOkRYqbB/w==-----END CERTIFICATE-----',
      keyType: 'Sign',
      subject: {
        commonName: 'VoterSign 41154974123973128044422774175187306997',
        organizationalUnit: 'sg1001-000',
        organization: 'NSWEC',
        country: 'AU'
      },
      time: '2014-12-25\'T\'00:00:00Z'
    },
    intermediates: {
      pems: [
        '-----BEGIN CERTIFICATE-----MIIDWzCCAkOgAwIBAgIULFolrOnoUb8h6GroW8JoRZtvSrcwDQYJKoZIhvcNAQELBQAwWTEaMBgGA1UEAwwRRWxlY3Rpb24gRXZlbnQgQ0ExEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMB4XDTE0MDkwODA3NDM1N1oXDTE1MDkwODA3NDM1NFowUTESMBAGA1UEAwwJVm90ZXJzIENBMRMwEQYDVQQLDApzZzEwMDEtMDAwMQ4wDAYDVQQKDAVOU1dFQzEJMAcGA1UEBwwAMQswCQYDVQQGEwJBVTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALAjG+fLhtK6FS0iLdAPscY9jCGYjROueU0F+ans8a5e214RDNS2WdnYhJSuPRcbftztncze6vCD0/KSZO6MTwagqx7iudFhSITJA96tTDT0pEQ/fInkcJbOh8LYAHukEXYPxQSiHTD52UTLwPpAVd/DhBflf7LKL0VEjXuDifU/EvVDp96g6yoyHMiv6REsIrr8o2cSI5UbrxPIX7V/fAwfHKDyRp2ECrqaMmz/7dKWjhB0zoBQGI7r4WCIDmUbU9psn90a5y6xj5XhuF6HzozaZH1IibCTULdTQEUfs4dPaF+5oWlCZrF+dkBTXQh4Pm1h+iOboCk+8FcZtt4ijC0CAwEAAaMjMCEwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYwDQYJKoZIhvcNAQELBQADggEBAD96TL6FvCMtoRJqjowqVRSQTAhnCcPesmG6+yzz9YXWu46koEM/XkeZm+wtIF6YC2R59vAvbFNtrnkThvFG3sDXDUAeDS4vrVr8S0BApbHDa94ri7Km8aYrPhwy5sDOsq5QR//RZORPOJaMeZHiJA0MP31GJJjwcJJs4uOm3sQHEdNArSEp8+Dzim3X/EY6NWo8COeKEwcPSm0c/jMung1NYKFV6CGF/jOOEcrESNdnBWi7LYAWn/ba9XiyuKhD26bLX1L20IFEO+EvcnEvLrhulmRe/bZsLkyZMieZqh+w3KFUe2F452J6o+/i2TbHcn8/c2B9T490/dV5w8sB1bM=-----END CERTIFICATE-----'
      ],
      subjects: [{
        commonName: 'Voters CA',
        organizationalUnit: 'sg1001-000',
        organization: 'NSWEC',
        country: 'AU'
      }]
    },
    root: {
      pem:
          '-----BEGIN CERTIFICATE-----MIIDYzCCAkugAwIBAgIUax82wdIaGWRKCH3J1h1u4Ij4F7AwDQYJKoZIhvcNAQELBQAwWTEaMBgGA1UEAwwRRWxlY3Rpb24gRXZlbnQgQ0ExEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMB4XDTE0MDkwODA3NDM1NloXDTE1MDkwODA3NDM1NFowWTEaMBgGA1UEAwwRRWxlY3Rpb24gRXZlbnQgQ0ExEzARBgNVBAsMCnNnMTAwMS0wMDAxDjAMBgNVBAoMBU5TV0VDMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkFVMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgXYi9cTerqmun+SS56tByn9Z9OYwP2cS8NkvHO0Pv04lTEJ//1jo8uUpn3kza4m5lgUFEr6hl+e+G21yk/NrieD4Ke008Be0F9arSedsYyQvqcg1VcssY3+mtbz8osOH8f/jB6cMkASrzESvDOIM45MkaFlAYqkxN4q819qaOgWvlotkQKUKu+Dnb59Cpqf8MQ2nVv+mEwXVZiVCKquOnEN29vqppI8GigE1jqRkKx5mIwmUBYWrOalWVexMNJVSAqyxlJ1ozgKC6Z/o37l/pRFJMG1iIiAiMRucokWn7p9414O+3O4OuDP1lkBG7PxzmOHEl3WrJ7hseGLWIF/mcwIDAQABoyMwITAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBBjANBgkqhkiG9w0BAQsFAAOCAQEAO89PQSEeZkGxydWgFJ61DKq6eiaNjCRXKorH3yDCd7wwuOiKamJRojk29zrLZR+0BrYGVLV4JPj/0ZStNtbQfz1jeadEVdrpjlk9NfNgPcYIbb9tENNNFRqeQR8JB1K+qOELZt87jX9wg1jzgAo0kSHDAIIADDOhX//lzeE2amaXgYYHmLtdKpZI2lXKj1VmdeeR/wGtpkXGkDO65FkvcY65lj25wzksanpk/juy4ENLoY1AvC6f30Kw7CSduoctsB58MOPPtlR6rA7EmSWUAdfTmobB3kHOm6OZN+nikUhC73l1kS9mZXg/oabO7yoOmRQ7ttqqNmecaB7EnS5nHg==-----END CERTIFICATE-----'
    }
  };

  var timeCheckChain_ = {
    leaf: {
      pem:
          '-----BEGIN CERTIFICATE-----MIIDszCCApugAwIBAgIJALAhpG3UDbqKMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMB4XDTE0MTAyMzE1MTMxMVoXDTE1MTAyMzE1MTMxMVowVjELMAkGA1UEBhMCRVMxEzARBgNVBAgMClRlc3QtU3RhdGUxDjAMBgNVBAoMBVNjeXRsMQswCQYDVQQLDAJRQTEVMBMGA1UEAwwMU2VjRW5kRW50aXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1isXtrHkDvChE1jGCSnleAc547Hnswxhuf3XcTs6cG4y5oOgLgEZrbzHAe6Z47wBmt/35OC5oIa5tgmWBs7QVnAZ4PZWAdZTAq2J5TBQCcpilw2Kc5NHp231Ckw52pHT6/O4VieeZdOA/+PCbL7/4PrQ/Uq/23W1kHvUqToJTRwrOZFMg8QfTlo3RYN6xFmlW6hXCCHkolqihbK44jX6FELCLwbE1dw43eao5Pnbxw//2jyqReC2qrpcKQ4MdgJJYgK9rA+zCCj0m7H+LsNyiat4OTYvDVx9Lg7nmjgD2cFWewLDE9t4QVST41vYgWObJjisLyGsbEwLfOjXkf8jrQIDAQABo4GKMIGHMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgbAMC0GCWCGSAGG+EIBDQQgFh5TY3l0bCBRQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFLBtuBGwweIxC3h8ILFblDSg2x8SMB8GA1UdIwQYMBaAFL0z1LklZYtSH+ayTVfCjxPfiDf4MA0GCSqGSIb3DQEBCwUAA4IBAQBDTsidJItoNaPPp+idbBCLm8VrNxlVpWYQ4KmA6glCw5z7TcH1JW+GsfSxXfXo2bFGtxDFsjZCV76fBwoQVAvJ6ntCyn/vrslSQy6RvArM2e7Di6h6ND500so0a5mfycN7rauhj0tWcmZv3zExJcwgtar/P3BHYpfN9/eG46NpxRq4Sq9uwfYEH9lzAfZ3xgLWgIRAEoF6b2VRMU9ApVX/Ifai17+0JKhNkmBp63EUJCcXNSjTV4UtKTO2QFply4hOqluhMkFZDqFsbzB+mrSQcCget+6ymCVdTaN3GwVuW4CKIoR85Nr+3OV1VupDz3bnzqDjeiTNIJEV6aONP2zD-----END CERTIFICATE-----',
      keyType: 'Sign',
      subject: {
        commonName: 'SecEndEntity',
        organizationalUnit: 'QA',
        organization: 'Scytl',
        country: 'ES'
      },
      time: '2014-10-23\'T\'19:11:28Z'
    },
    intermediates: {
      pems: [
        '-----BEGIN CERTIFICATE-----MIIDsDCCApigAwIBAgIJALAhpG3UDbqJMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTEyMTZaFw0xNTEwMjMxNTEyMTZaME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyR4qNaHpEoj/48+iJtJUJuXv4i/KEKRKwrTYOVbDplyfTg0KMO+zeDd5SyE/aBnANetlZo8Ku0m1LPbUdge67Jzj3jinZ+iUQbkKr15YgMjehb3Y0V/02ImIr4+w8oXBGIN6eDpRt7tKrCPuyeRiWOZqIae7t5KY5enQ2NC6YbteTwjO/O1ldTnFo3xeDiuQ7P3zBmkAXaAxDxEE9dh61mB/QXVeJpphTgF8yoRWeZhl1S9YnNfTvMyXih8WzY/DlRTKs9JnqRmI7YhLDV95v3Aah1FhelG+l3FsWciE3brcZDa0Y+zumck2sy35PU3TF7RxifUumCEiNHEaawV7xQIDAQABo4GNMIGKMAwGA1UdEwQFMAMBAf8wLQYJYIZIAYb4QgENBCAWHlNjeXRsIFFBIEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUvTPUuSVli1If5rJNV8KPE9+IN/gwHwYDVR0jBBgwFoAUDnogh/ZLwbQuOwiVIBJ08GAyLaQwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQB/R7kXa2NblyepSBkdjIQNEvxmaryje9MQLJg+kRaWjpkWz0OW+a9vYoK+6hAt15RYFWNASTHF6StSe0En9F35fzaU8/XUxoKH5pmm7aypVIN7mWWV+XMv8iao1kP2tcpnk5tsvnuqBfqcU22r+oLLcnJ1hWy2kijtIBGEe+kxhZi2twlDG66RQpEZA2CmDjLHYbAXF7wnqiY9PSKwXcKPjcHXxs8LgPbbZjLRZ2FSvfgya/9drHfu7QYHRBFFHYljdgcpF/Z/nerocmvTgX8iuOu133JA3JmMOy3E7riVUfRkKh4Nnivh9S5mCKSff0yW4i+kJJ9TuAuDi30FN3tR-----END CERTIFICATE-----'
      ],
      subjects: [{
        commonName: 'SecQA',
        organizationalUnit: 'QA',
        organization: 'Scytl',
        country: 'ES'
      }]
    },
    root: {
      pem:
          '-----BEGIN CERTIFICATE-----MIIDgDCCAmigAwIBAgIJALAhpG3UDbqIMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTExMTJaFw0yNDEwMjAxNTExMTJaMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKanbx9BsuYLjq4RVAS269FD2Gtpp3YkcvKHwHO5XI89+pJELO9CVnyIvEmon3ndbvPTqM3D/HOvl0M4/kPFeHij8d24iM48GVbYfLVBrV3BtKe24pHeIagSn6hl9ouiknbvDBqF8qZcBTNhnH8yX7isoJf3TShBpTOdyJ6XkDOQ+R0GtS1M4NN9BS74oGt0REC17VlBrO6JyR9rNswk4abqqpF7ekw1yXxcnrqa480+oyEcY9ji5QNoP9kGyibeB08seeB+wlt0fCWcJXtecsnPRpBNJmhLr4ziNyXoIOCEeVMA/MFx7vrrl+pkyhd49rQybbctv/mQGUM4QG6IZ3cCAwEAAaNdMFswHQYDVR0OBBYEFA56IIf2S8G0LjsIlSASdPBgMi2kMB8GA1UdIwQYMBaAFA56IIf2S8G0LjsIlSASdPBgMi2kMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQCE15Vvk+I+iR2RutD1+Mm8Yx1J8dtObqUYeIVh/xJlCA2zTBB2MPdNz6IroIHqb4/ZYd2l9ZtNkIyCZ3PV5ulV1ShQD6H2gXmE1AnwQeojqrdKQYAehcDOYWxMAZNBK1piimEpQCCSiBlNKG4f14GTjqpI4WuqcZ7DGQtWqR3LVpNsKCdE9OHZo/Ep4Kp1AwQ+qhmLl0wA1SmHP7+0nxRI5i5w0Yl8NQx/v6W4j7ohWOwzpRRZ4rCz856oOVJqDDOMWqQfqYKQGY4WROpM447OsQ+svno3JVbqtvMDoRLBu6GEJtSl2DOvccPSqI2cxotsBzF+Ppz9EpeWtW+Wut6k-----END CERTIFICATE-----'
    }
  };

  var intermediateCertificatePem_ =
      '-----BEGIN CERTIFICATE-----MIICGjCCAYOgAwIBAgIBATANBgkqhkiG9w0BAQUFADA9MREwDwYDVQQDDAh3aGl0ZSBjYTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzAeFw0xNDA5MDUwOTM1MThaFw0yNDA4MjMwOTM1MThaMEcxGzAZBgNVBAMMEndoaXRlIGludGVybWVkaWF0ZTEOMAwGA1UECgwFc2N5dGwxCzAJBgNVBAsMAnNlMQswCQYDVQQGEwJFUzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUmpmT/rNcxIP8Xulj0RcbdvaNGJNoEPMzEGztguB+mKsTPgJIlrKvdskY5NIPd1+hzS+HjvW11uwyT2FnmyfKBqND4EH5BqU8Ph4bAyrT950DPcOGE4vG0fLO0jTD3h0n4d4yugWmrND2sScFZQsGLbgOYX4QM8vV+owoKNRxMCAwEAAaMgMB4wDwYDVR0TBAgwBgEB/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEFBQADgYEAF0B5LQDo3uRs9dSkxp39ER64XIa6XYV0zdbq+fqHg8Qcoggpox7vwmc6YoCNQpfY2zLiIUMbb6VMP4pkp7jZMju9TBKPOjMFrQKom0cowT/gYdokgnsk8yUYOdo46GJR43PjFLTiQk5JDawWaECbhcgyLf59AalX9z9dbY0AwqI=-----END CERTIFICATE-----';

  var tooEarlyStartLeafCertificatePem_ =
      '-----BEGIN CERTIFICATE-----MIIDszCCApugAwIBAgIJAJZdx2FxigSQMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMB4XDTE0MTAyMzE1MzYxOVoXDTE1MTAyMzE1MzYxOVowVjELMAkGA1UEBhMCRVMxEzARBgNVBAgMClRlc3QtU3RhdGUxDjAMBgNVBAoMBVNjeXRsMQswCQYDVQQLDAJRQTEVMBMGA1UEAwwMU2VjRW5kRW50aXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwFcomvOSzm2HkrOodZ93pyCxkpW7CVUMYC9zusJ7SdHEhcMib+44+rcfjnxPLmSO9xZTRYQo1DXwktEqkMutNih+YB51MPddKFylvMP9cUKkT1sIJzoIhRXC+xOtPAILfYx2XsNJCuRrEKM27JrC2xIVCBIiJuNz6UyFe7ZvtKmt+n40Nsi4Mcx0DhGRuZ3uhuA5OEotbG6VZZVhAovYFh09x+GyT1Dpug8NRD+mM+g1qJkhNZ+MST/rtw5DB++S+VwVLyVfxrTNycUafWWHAIMsjRlp6qlgfK32yY6N0haZXvP4RcXYSIWZK5ON+6r5lIY/FBR2m7eiGxlxdPkr7QIDAQABo4GKMIGHMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgbAMC0GCWCGSAGG+EIBDQQgFh5TY3l0bCBRQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFAOC8miWiUpLGw0TQzpjMUhAyYtvMB8GA1UdIwQYMBaAFDK0r97umjjsFvU6NAkgEvKeaIMVMA0GCSqGSIb3DQEBCwUAA4IBAQAQH64q51KivF3lUd78nOvHzuv04pUGihcVnYm3MTnXiODpwj4f6s5sZnAUWB+BOCJ1btaJvNAWUpnOH36nLr+qKV5mIxOyiKQWxY0tunySctsOpSscW/OfatvOUdkjshC7QuNq0fhjYh8SGO9Bk4ePO0uaIEWWywaH0wHVJPFaETklFXg3TZr61T5scrDEfs+lk+fQjkJ0YiS4e5VKfS7uVPLtDLgT9sx3l1QlVV2xZ8jF7hPpTy0JX8adp0NEQd+kfFFQuuSnY0QfLKtut9LHIjrl2q2sLfi0ibJ+uokCY85XZeyPzovIaSu3b2941E3dxL6UbDCEJyjlffybakyN-----END CERTIFICATE-----';
  var tooEarlyStartIntermediateCertificatePem_ =
      '-----BEGIN CERTIFICATE-----MIIDsDCCApigAwIBAgIJAJZdx2FxigSPMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTQyMDJaFw0xNTEwMjMxNTQyMDJaME8xCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDjAMBgNVBAMMBVNlY1FBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlvRn5OjWMaQfWCknqnsTzupcpx6zPQCLDe9DbizYRBghJcr4UlxI1uLsCHHzEMXCta5rJ+PSQ+TZ71PP7Ep3JyLAYObhVM8ltQyAKm8fgZa/FhCer46O3CohiD0Cdjg3r+qGOB+OKcGigXnNSoEsBlJ7HTzi5egiSg6D1Mx4uNw35F15ahTDxzIT/HN/Dnl3fEGzuJFebhSxj3u/+EEFJQrYvl62bUzzpXKlg/1C8BUYUXcPqhuUwZhyxE/DKyunA489XwpvN11y9NgygKb45qFAi9WmXil7q5JkW8iBqhXWbAoGiYf8nG5OanQBHERmAYAzJBI3QCK+6WiDXCRT/wIDAQABo4GNMIGKMAwGA1UdEwQFMAMBAf8wLQYJYIZIAYb4QgENBCAWHlNjeXRsIFFBIEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUMrSv3u6aOOwW9To0CSAS8p5ogxUwHwYDVR0jBBgwFoAUqHgXrevwre4z3tVx05iDe/KVIfMwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQA/Jxb3/gHb6mGyTEti+g3Zs5nrX64dtbFPWEgpyKu2jZ0ZoyllAUThIBjwvPw0hI3LqTOG06CCrgrPmndakasc5bJTEokshoI/IxTp/itME3ZBx/TUpu6v3baHYQ5C3HcKGhJxScikeO/Gh17A71Q4wA1PbLQ8jHCgfAFIODbXNRcYj8BVpv+7rXY/px4ai2UEf6ZAQZ3H6OyN9PGRA4EABJEiBsRNJvzcUcfq1JHF02FNlhFn8ZZXxOzpSc9bYX8trJGgB6GXDj2BPnFzsz1+ksNW6CaLU9AuuS2Bj/NseF2WVRwi3nCIvAPbpgAhivIHZOxY/eNRjJijo7+OKG40-----END CERTIFICATE-----';
  var tooLateStartRootCertificatePem_ =
      '-----BEGIN CERTIFICATE-----MIIDgDCCAmigAwIBAgIJAJZdx2FxigSOMA0GCSqGSIb3DQEBCwUAMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjAeFw0xNDEwMjMxNTQ0MzdaFw0yNDEwMjAxNTQ0MzdaMFAxCzAJBgNVBAYTAkVTMRMwEQYDVQQIDApUZXN0LVN0YXRlMQ4wDAYDVQQKDAVTY3l0bDELMAkGA1UECwwCUUExDzANBgNVBAMMBlNlY0RldjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL4xWFqJpuQtcbywy9aYeH1X8cvlueJNPG/ixZsZjXY5KUdq7bjBLDlns49UWw96uxbU+JEL5aKMibtA8sJP9ZhDKY15ps9+wtceFnkT0g0IZ1shupQPEOKH2UfIS5/2teL7vjCplX3mDX8YOgzLqlqFrVtifejH2ETRzpDFc7K/P3KtSQqGKYUEZw5olCdLcg9Fp+I9QVkNGGr6Ds16BGWnb6Pgm5NOegdoOJLF6vZkhvyTYz9/Tg2men/HJLCVCXhbYwfUcu0ezj8w5G6zCdxVrqSTvjHVzp0kskVGRZjlrEOFLvswOGTjAon/U4iuBtqR+pLZg4Law+ETujPJPs0CAwEAAaNdMFswHQYDVR0OBBYEFKh4F63r8K3uM97VcdOYg3vylSHzMB8GA1UdIwQYMBaAFKh4F63r8K3uM97VcdOYg3vylSHzMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQAq2bLqJ+cLtnivXrNP5j9YL031zAO6xQDehxqasKOb7e6pdB8NKAOwa7smXCRsJxphT2vTv+VsecD/b8D3irEV000pbemeVmyrnOyaJxTCjBRvhjIbOmaBZ5k0KfxDubT8xx9H8Im+jxX0tZU5IQuTBG+Izd+5Dm5kxAhh7JDzclUYfUMDYrv43/7zm5wPkRqqK5SY7J4CPMiW3Uzxpc+uPCwydMSTYbDTdNN5nf0eDGepm2h/iewfickgkCFoNe6b3nxlQiK8EL9FUSKeq+1AWVnT3riC8C/CXTZwKo3ULXkKe3Jl3mvVYjkz/O/3NzeTfFdktuNaSh28Wrzlb9h4-----END CERTIFICATE-----';

  var certificateValidator_;

  beforeAll(function() {
    certificateValidator_ = certificate.newService().newValidator();
  });

  describe('create a certificate service that should be able to ..', function() {
    describe('create a CertificateValidator object that should be able to', function() {
      it('successfully validate a valid certificate chain', function() {
        var validations = certificateValidator_.validateChain(validChain_);
        expect(validations).toEqual([[], []]);
      });

      it('successfully validate a valid certificate chain, generated with Java',
         function() {
           var validations =
               certificateValidator_.validateChain(validChainFromJava_);
           expect(validations).toEqual([[], []]);
         });

      it('flatten a two-dimensional array of certificate chain failed validations',
         function() {
           var validationFailed = [['SIGNATURE', 'NOT_AFTER'], ['NOT_BEFORE']];
           var validationFailedFlattened =
               ['signature_0', 'not_after_0', 'not_before_1'];

           expect(
               certificateValidator_.flattenFailedValidations(validationFailed))
               .toEqual(validationFailedFlattened);
         });

      it('usuccessfully validate a certificate chain for which the leaf certificate subject common name is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.leaf.subject.commonName = 'Invalid common name';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_0');
         });

      it('usuccessfully validate a certificate chain for which the leaf certificate subject organizational unit is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.leaf.subject.organizationalUnit =
               'Invalid organizational unit';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_0');
         });

      it('usuccessfully validate a certificate chain for which the leaf certificate subject organization is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.leaf.subject.organization = 'Invalid organization';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_0');
         });

      it('usuccessfully validate a certificate chain for which the leaf certificate country is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.leaf.subject.country = 'Invalid country';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_0');
         });

      it('usuccessfully validate a certificate chain for which an intermediate certificate subject common name is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.intermediates.subjects[0].commonName =
               'Invalid common name';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_1');
         });

      it('usuccessfully validate a certificate chain for which an intermediate certificate subject organizational unit is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.intermediates.subjects[0].organizationalUnit =
               'Invalid organizational unit';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_1');
         });

      it('usuccessfully validate a certificate chain for which an intermediate certificate subject organization is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.intermediates.subjects[0].organization =
               'Invalid organization';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_1');
         });

      it('usuccessfully validate a certificate chain for which an intermediate certificate subject country is invalid',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.intermediates.subjects[0].country = 'Invalid country';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('subject_1');
         });

      it('usuccessfully validate a certificate chain for which a leaf certificate with a Sign key type is validated as being an CA key type',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.leaf.keyType = 'CA';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('key_type_0');
         });

      it('usuccessfully validate a certificate chain for which a leaf certificate with a Sign key type is validated as being an Encryption key type',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.leaf.keyType = 'Encryption';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('key_type_0');
         });

      it('usuccessfully validate a certificate chain for which the leaf certificate signature cannot be verified by the next certificate in the chain',
         function() {
           var invalidChain = clone(validChain_);
           invalidChain.leaf.pem = intermediateCertificatePem_;

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('signature_0');
         });

      it('usuccessfully validate a certificate chain for which the leaf certificate ending time of validity is after that of an intermediate certificate',
         function() {
           var validations =
               certificateValidator_.validateChain(timeCheckChain_);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toEqual(['not_after_0']);
         });

      it('usuccessfully validate a certificate chain for which the leaf and intermediate certificate starting times of validity are earlier than that of the root certificate',
         function() {
           var invalidChain = clone(timeCheckChain_);
           invalidChain.leaf.pem = tooEarlyStartLeafCertificatePem_;
           invalidChain.intermediates.pems[0] =
               tooEarlyStartIntermediateCertificatePem_;
           invalidChain.root.pem = tooLateStartRootCertificatePem_;

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('not_before_0');
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('not_before_1');
         });

      it('successfully validate a certificate chain time reference if the time reference is set to the leaf certificate starting time of validity',
         function() {
           var chain = clone(timeCheckChain_);
           chain.leaf.time = '2014-10-23\'T\'15:13:11Z';

           var validations = certificateValidator_.validateChain(chain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .not.toContain('time_0');
         });

      it('successfully validate a certificate chain time reference if the time reference is set to the leaf certificate ending time of validity',
         function() {
           var chain = clone(timeCheckChain_);
           chain.leaf.time = '2015-10-23\'T\'15:13:11Z';

           var validations = certificateValidator_.validateChain(chain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .not.toContain('time_0');
         });

      it('usuccessfully validate a certificate chain time reference if the time reference is set to 1 second before the leaf certificate starting time of validity',
         function() {
           var invalidChain = clone(timeCheckChain_);
           invalidChain.leaf.time = '2014-10-23\'T\'15:13:10Z';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('time_0');
         });

      it('usuccessfully validate a certificate chain time reference if the time reference is set to 1 second after the leaf certificate ending time of validity',
         function() {
           var invalidChain = clone(timeCheckChain_);
           invalidChain.leaf.time = '2015-10-23\'T\'15:13:12Z';

           var validations = certificateValidator_.validateChain(invalidChain);
           expect(certificateValidator_.flattenFailedValidations(validations))
               .toContain('time_0');
         });
    });
  });

  function clone(object) {
    return JSON.parse(JSON.stringify(object));
  }
});
