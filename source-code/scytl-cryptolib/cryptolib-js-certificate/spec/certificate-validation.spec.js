/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* jshint node:true */
'use strict';

var certificate = require('../lib/index');

describe('The certificate module should be able to ...', function() {
  var rootCertificatePem_ =
      '-----BEGIN CERTIFICATE-----MIIEKjCCAxKgAwIBAgIQQtWFdPN4NAvUIWyyJyUlbTANBgkqhkiG9w0BAQsFADBUMRAwDgYDVQQDDAdSb290IENOMRYwFAYDVQQLDA1Sb290IE9yZyBVbml0MREwDwYDVQQKDAhSb290IE9yZzEVMBMGA1UEBhMMUm9vdCBDb3VudHJ5MB4XDTE0MDYxODEwMjMyOFoXDTE1MDYxODEwMjMyOFowVDEQMA4GA1UEAwwHUm9vdCBDTjEWMBQGA1UECwwNUm9vdCBPcmcgVW5pdDERMA8GA1UECgwIUm9vdCBPcmcxFTATBgNVBAYTDFJvb3QgQ291bnRyeTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJ4AidkWId1zr4IZgItjE9uv38eB8fIGJGEts2n/XNh/EJ7NYZyBzvKSJR83v7LVJjc4pup5crWyS0B5lQ7uuD3/XZ9QpQFtnGiqlKOKH45zsw3ekaHEAco07L2dZznBuQTGLTlhrmvkCFRa8b8WYC+k90oUPvSd/9S4kA1Jlo9JDKHLer0SbjZCcVRXoLSBKmGWE0xSNfmNuZNONDRbtHvSA8A10AOtxKii9w464MXYzmPil7uM1Og1HC+FXCmkzLNfqQ31Om0jra3nLmrCpBJjRPX7svVnoxajRqpazVQnmJjjpzV7yNLwnR9W8OPwqanXcbxmTkrXMxfLxiVXDFUCAwEAAaOB9zCB9DAPBgNVHRMBAf8EBTADAQH/MDUGCCsGAQUFBwEBAQH/BCYwJDAiBggrBgEFBQcwAYYWaHR0cDovL29jc3AudGhhd3RlLmNvbTA0BgNVHR8BAf8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZlcmlzaWduLmNvbS9wY2EzLmNybDArBgNVHREBAf8EITAfpB0wGzEZMBcGA1UEAwwQUHJpdmF0ZUxhYmVsMy0xNTAOBgNVHQ8BAf8EBAMCAQYwNwYDVR0lAQH/BC0wKwYIKwYBBQUHAwEGCCsGAQUFBwMCBgpghkgBhvhFAQgBBglghkgBhvhCBAEwDQYJKoZIhvcNAQELBQADggEBADmtmjApZAXIkGLaZCdkRnhel53BtEdQnG990Oo/tBBboqy2ipum9ByTj3hNWJB3zuPN77rkrek9rbookNcCgVWhHtTk1lUpUK6ZohDsZh8k0MqIhkz+X+HiWGRsEOptjsCaknyWcWb4aXAevMAQMPm/ktkpQ8AOxAq+gtieewWQZP3kGPhBBCfn8TGjdrn9+ymf8EIbAUFXQ8m+oWeNlrdWhqzRXwQbj4EDds1kZdTo0nCYUdH+XEBF9nMyhAxSQWzCKQQTRFWv1dr3dKapzfgrdH8wEgvptiBYCY62O5+3DxiNK/VWquHz6S5GqIwkmSPDPMUU/qK3SNG3xIL1U1k=-----END CERTIFICATE-----';

  var leafCertificatePem_ =
      '-----BEGIN CERTIFICATE-----MIIEMzCCAxugAwIBAgIQRbaPaToIM+VS/d6etgYZ4jANBgkqhkiG9w0BAQsFADBUMRAwDgYDVQQDDAdSb290IENOMRYwFAYDVQQLDA1Sb290IE9yZyBVbml0MREwDwYDVQQKDAhSb290IE9yZzEVMBMGA1UEBhMMUm9vdCBDb3VudHJ5MB4XDTE0MDYxODEwMjMyOFoXDTE1MDYxODEwMjMyOFowYDETMBEGA1UEAwwKU3ViamVjdCBDTjEZMBcGA1UECwwQU3ViamVjdCBPcmcgVW5pdDEUMBIGA1UECgwLU3ViamVjdCBPcmcxGDAWBgNVBAYTD1N1YmplY3QgQ291bnRyeTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAIJcqWVOOW539qKZ0SPmOdVaqLgabw0998SAfzrW8Cs8FaYvia4wRbX5N97RPbf3UkbO/QiveB0YQlnDoi2tqlj643mfUgYhMknK4SL0WVQReNcwYMEDbkbyQrCpgKpWWhTSQ2cRD+K3qZpQZ9Qn1RZn615jiqsD+Re5fWbbnL3kc7H5hdclTxZFvEzLgd2KjIsspsqh5UvLowrLuSPLaYD28LsXTDmPeHzYUl62JGPCLl9YNc3av2dY5bjmkf1KiuFKZ27iWh/xdFYAzYloenEw9AxRhJNG+9IucFOENy/0ul2UEb0rgA6Am4cASrhS+aVuZ/OuaC1W+Ut8LlXVmhsCAwEAAaOB9DCB8TAMBgNVHRMBAf8EAjAAMDUGCCsGAQUFBwEBAQH/BCYwJDAiBggrBgEFBQcwAYYWaHR0cDovL29jc3AudGhhd3RlLmNvbTA0BgNVHR8BAf8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZlcmlzaWduLmNvbS9wY2EzLmNybDArBgNVHREBAf8EITAfpB0wGzEZMBcGA1UEAwwQUHJpdmF0ZUxhYmVsMy0xNTAOBgNVHQ8BAf8EBAMCBsAwNwYDVR0lAQH/BC0wKwYIKwYBBQUHAwEGCCsGAQUFBwMCBgpghkgBhvhFAQgBBglghkgBhvhCBAEwDQYJKoZIhvcNAQELBQADggEBAAWZDJD6bg4ohHewszrAbL2tdUNxhrwCgNaHUhwNK43kiLGH0U9innhL1i0jP1VHNkL1G/+ZCo1qzh/Usji/jtlurfAWtrXku6VRF9NP+itKOY5jJ91Ijkc7t4dgoeJq6iMHn6JbDKIQ88r/Ikd0GdF04o5Qjqq1HlUVmqyIOHeHFla4i4tOxTyUBj34eE1No/xmaKYV1QtR1dqSHblR7OagEo7Dd3fXp7iSrKrXaN0Ef/6zeF3zjU5SMKcUcU9d3CbhS/CrGb+UGlqTXgzPXQWESH9AqBNl67+HF3mYktDQOZYPT5WRO5IKSko2cy9pP9UCsLk4oU3xyOxacWDpk1k=-----END CERTIFICATE-----';

  var issuerValidationData_ = {
    issuer: {
      commonName: 'Root CN',
      organizationalUnit: 'Root Org Unit',
      organization: 'Root Org',
      country: 'Root Country'
    }
  };

  var subjectValidationData_ = {
    subject: {
      commonName: 'Subject CN',
      organizationalUnit: 'Subject Org Unit',
      organization: 'Subject Org',
      country: 'Subject Country'
    }
  };

  var certificateValidator_;

  beforeAll(function() {
    certificateValidator_ = certificate.newService().newValidator();
  });

  describe('create a certificate service that should be able to ..', function() {
    describe('create a CertficateValidator object that should be able to', function() {
      it('successfully validate a certificate that contains the expected issuer DN.',
         function() {
           var validations = certificateValidator_.validate(
               leafCertificatePem_, issuerValidationData_);
           expect(validations.length).toBe(0);
         });

      it('usuccessfully validate a certificate that contains an unexpected issuer common name',
         function() {
           var validationData = clone(issuerValidationData_);
           validationData.issuer.commonName = 'Different Root CN';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['ISSUER']);
         });

      it('usuccessfully validate a certificate that contains an unexpected issuer organizational unit',
         function() {
           var validationData = clone(issuerValidationData_);
           validationData.issuer.organizationalUnit = 'Different Root Org Unit';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['ISSUER']);
         });

      it('usuccessfully validate a certificate that contains an unexpected issuer organization',
         function() {
           var validationData = clone(issuerValidationData_);
           validationData.issuer.organization = 'Different Root Org';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['ISSUER']);
         });

      it('usuccessfully validate a certificate that contains an unexpected issuer country',
         function() {
           var validationData = clone(issuerValidationData_);
           validationData.issuer.country = 'Different Root Country';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['ISSUER']);
         });

      it('successfully validate a certificate that contains the expected subject DN.',
         function() {
           var validations = certificateValidator_.validate(
               leafCertificatePem_, subjectValidationData_);
           expect(validations.length).toBe(0);
         });

      it('usuccessfully validate a certificate that contains an unexpected subject common name',
         function() {
           var validationData = clone(subjectValidationData_);
           validationData.subject.commonName = 'Different Subject CN';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['SUBJECT']);
         });

      it('usuccessfully validate a certificate that contains an unexpected subject organizational unit',
         function() {
           var validationData = clone(subjectValidationData_);
           validationData.subject.organizationalUnit =
               'Different Subject Org Unit';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['SUBJECT']);
         });

      it('usuccessfully validate a certificate that contains an unexpected subject organization',
         function() {
           var validationData = clone(subjectValidationData_);
           validationData.subject.organization = 'Different Subject Org';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['SUBJECT']);
         });

      it('usuccessfully validate a certificate that contains an unexpected subject country',
         function() {
           var validationData = clone(subjectValidationData_);
           validationData.subject.country = 'Different Subject Country';

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['SUBJECT']);
         });

      it('successfully validate a certificate when the validation time is within the certificate validity period',
         function() {
           var validationData = {time: '2014-12-25\'T\'00:00:00Z'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations.length).toBe(0);
         });

      it('successfully validate a certificate when the validation time is exactly the certificate starting time of validity',
         function() {
           var validationData = {time: '2014-06-18\'T\'10:23:28Z'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations.length).toBe(0);
         });

      it('successfully validate a certificate when the validation time is exactly the certificate ending time of validity',
         function() {
           var validationData = {time: '2015-06-18\'T\'10:23:28Z'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations.length).toBe(0);
         });

      it('usuccessfully validate a certificate when the validation time is before the certificate starting time of validity',
         function() {
           var validationData = {time: '2014-05-18\'T\'10:23:28Z'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['TIME']);
         });

      it('usuccessfully validate a certificate when the validation time is after the certificate ending time of validity',
         function() {
           var validationData = {time: '2015-07-18\'T\'10:23:28Z'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['TIME']);
         });

      it('usuccessfully validate a certificate when the validation time is specified as a single character',
         function() {
           var validationData = {time: 'a'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['TIME']);
         });

      it('usuccessfully validate a certificate when the validation time is specified as three characters',
         function() {
           var validationData = {time: 'abc'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['TIME']);
         });

      it('usuccessfully validate a certificate whose validation time is specified as twenty characters',
         function() {
           var validationData = {time: 'aaaaaaaaaaaaaaaaaaaa'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['TIME']);
         });

      it('usuccessfully validate a leaf certificate when the validation key type is specified as CA',
         function() {
           var validationData = {keyType: 'CA'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['KEY_TYPE']);
         });

      it('successfully validate a certificate of key type Sign when the validation key type is specified as Sign',
         function() {
           var validationData = {keyType: 'Sign'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations.length).toBe(0);
         });

      it('successfully validate a certificate of key type CA when the validation key type is specified as CA',
         function() {
           var validationData = {keyType: 'CA'};

           var validations = certificateValidator_.validate(
               rootCertificatePem_, validationData);
           expect(validations.length).toBe(0);
         });

      it('usuccessfully validate a certificate of key type Sign when the validation key type is specified as CA',
         function() {
           var validationData = {keyType: 'CA'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['KEY_TYPE']);
         });

      it('usuccessfully validate a certificate of key type Sign when the validation key type is specified as Encryption',
         function() {
           var validationData = {keyType: 'Encryption'};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['KEY_TYPE']);
         });

      it('usuccessfully validate a certificate of key type CA when the validation key type is specified as Sign',
         function() {
           var validationData = {keyType: 'Sign'};

           var validations = certificateValidator_.validate(
               rootCertificatePem_, validationData);
           expect(validations).toEqual(['KEY_TYPE']);
         });

      it('usuccessfully validate a certificate  of key type CA when the validation key type is specified as Encryption',
         function() {
           var validationData = {keyType: 'Encryption'};

           var validations = certificateValidator_.validate(
               rootCertificatePem_, validationData);
           expect(validations).toEqual(['KEY_TYPE']);
         });

      it('successfully validate a root certificate when the validation signing certificate is specified as the root certificate itself',
         function() {
           var validationData = {issuerCertificatePem: rootCertificatePem_};

           var validations = certificateValidator_.validate(
               rootCertificatePem_, validationData);
           expect(validations.length).toBe(0);
         });

      it('successfully validate a leaf certificate when the validation signing certificate is specified as its parent certificate',
         function() {
           var validationData = {issuerCertificatePem: rootCertificatePem_};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations.length).toBe(0);
         });

      it('usuccessfully validate a root certificate when the validation signing certificate is specified as another certificate',
         function() {
           var validationData = {issuerCertificatePem: leafCertificatePem_};

           var validations = certificateValidator_.validate(
               rootCertificatePem_, validationData);
           expect(validations).toEqual(['SIGNATURE']);
         });

      it('usuccessfully validate a leaf certificate when the validation signing certificate is specified as the leaf certificate itself',
         function() {
           var validationData = {issuerCertificatePem: leafCertificatePem_};

           var validations = certificateValidator_.validate(
               leafCertificatePem_, validationData);
           expect(validations).toEqual(['SIGNATURE']);
         });
    });
  });

  function clone(object) {
    return JSON.parse(JSON.stringify(object));
  }
});
