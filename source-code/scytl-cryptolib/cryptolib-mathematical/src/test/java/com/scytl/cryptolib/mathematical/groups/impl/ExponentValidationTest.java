/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.mathematical.groups.impl;

import static junitparams.JUnitParamsRunner.$;

import java.math.BigInteger;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.test.tools.configuration.ZpSubgroupLoader;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

/**
 * Tests of the Exponent input validation.
 */
@RunWith(JUnitParamsRunner.class)
public class ExponentValidationTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    @Parameters(method = "createExponent")
    public void testExponentCreationValidation(BigInteger q,
            BigInteger value, String errorMsg)
            throws GeneralCryptoLibException {

        thrown.expect(GeneralCryptoLibException.class);
        thrown.expectMessage(errorMsg);
        new Exponent(q, value);
    }

    public static Object[] createExponent()
            throws GeneralCryptoLibException {

        BigInteger q = new ZpSubgroupLoader().getQ();
        BigInteger exponentValue = BigInteger.TEN;

        return $(
            $(null, exponentValue, "Zp subgroup q parameter is null."),
            $(q, null, "Exponent value is null."),
            $(BigInteger.ZERO, exponentValue,
                "Zp subgroup q parameter must be greater than or equal to : 1; Found 0"));
    }

    @Test
    @Parameters(method = "deserializeExponent")
    public void testExponentDeserializationValidation(String jsonStr,
            String errorMsg) throws GeneralCryptoLibException {

        thrown.expect(GeneralCryptoLibException.class);
        thrown.expectMessage(errorMsg);
        Exponent.fromJson(jsonStr);
    }

    public static Object[] deserializeExponent() {

        String nullQParamJsonStr =
            "{\"exponent\":{\"q\":null,\"value\":\"Cg==\"}}";
        String nullExponentValueJsonStr =
            "{\"exponent\":{\"q\":\"Cw==\",\"value\":null}}";
        String zeroQParamJsonStr =
            "{\"exponent\":{\"q\":\"AA==\",\"value\":\"Cg==\"}}";

        return $($(null, "Exponent JSON string is null."),
            $("", "Exponent JSON string is blank."),
            $("   ", "Exponent JSON string is blank."),
            $(nullQParamJsonStr, "Zp subgroup q parameter is null."),
            $(nullExponentValueJsonStr, "Exponent value is null."),
            $(zeroQParamJsonStr,
                "Zp subgroup q parameter must be greater than or equal to : 1; Found 0"));
    }
}
