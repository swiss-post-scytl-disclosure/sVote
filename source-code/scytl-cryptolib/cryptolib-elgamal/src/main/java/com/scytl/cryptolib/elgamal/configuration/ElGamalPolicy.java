/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.SecureRandomPolicy;

/**
 * Defines the ElGamal policy. Extends the secure random policy.
 */
public interface ElGamalPolicy extends SecureRandomPolicy {

    /**
     * Returns the {@link ConfigGroupType} that should be used when
     * generating instances of
     * {@link com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters}.
     *
     * @return the configuration for creating parameters of Zp subgroup.
     */
    ConfigGroupType getGroupType();

}
