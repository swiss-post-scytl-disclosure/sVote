/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.configuration;

public enum Group {

    /**
     * Zp group.
     */
    ZP,

    /**
     * Quadratic Residue group.
     */
    QR;
}
