/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.service;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.ServiceFactoryHelper;

/**
 * This class sets up default factories of {@link ElGamalServiceAPI} objects.
 */
public final class ElGamalServiceFactoryHelper {
    /**
     * This is a helper class. It cannot be instantiated.
     */
    private ElGamalServiceFactoryHelper() {
        super();
    }

    /**
     * Retrieves a new factory of non thread-safe services.
     * <p>
     * Default factory is {@link BasicElGamalServiceFactory}.
     * 
     * @param params
     *            parameters used in the creation of the default factory.
     * @return the new factory.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<ElGamalServiceAPI> getInstance(
            final Object... params) {
        return ServiceFactoryHelper.get(BasicElGamalServiceFactory.class,
            params);

    }

    /**
     * Retrieves a new factory of thread-safe services.
     * <p>
     * Default factory is {link {@link PollingElGamalServiceFactory}
     * 
     * @param params
     *            parameters used in the creation of the default
     *            factory.
     * @return the new factory of thread-safe services.
     */
    @SuppressWarnings("unchecked")
    public static ServiceFactory<ElGamalServiceAPI> getFactoryOfThreadSafeServices(
            final Object... params) {
        return ServiceFactoryHelper.get(
            PollingElGamalServiceFactory.class, params);
    }
}
