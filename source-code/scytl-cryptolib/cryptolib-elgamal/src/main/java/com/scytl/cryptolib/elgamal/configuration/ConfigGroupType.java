/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.configuration;

public enum ConfigGroupType {

    ZP_2048_224(Group.ZP, 2048, 224),
    ZP_2048_256(Group.ZP, 2048, 256),
    QR_2048(Group.QR, 2048);

    private final Group _group;

    private final int _L;

    private final int _N;

    ConfigGroupType(final Group group,
            final int L, final int N) {
        _group = group;
        _L = L;
        _N = N;
    }

    ConfigGroupType(final Group group, final int L) {
        _group = group;
        _L = L;
        _N = L - 1;
    }

    public int getL() {
        return _L;
    }

    public int getN() {
        return _N;
    }

    public Group getGroup() {
        return _group;
    }
}

