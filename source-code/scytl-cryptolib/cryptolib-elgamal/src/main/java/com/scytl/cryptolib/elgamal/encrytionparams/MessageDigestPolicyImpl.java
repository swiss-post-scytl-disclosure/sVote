/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.encrytionparams;

import com.scytl.cryptolib.primitives.messagedigest.configuration.ConfigMessageDigestAlgorithmAndProvider;
import com.scytl.cryptolib.primitives.messagedigest.configuration.MessageDigestPolicy;

/**
 * An implementation of MessageDigestPolicy that can be passed to a
 * MessageDigestFactory, and permits the message digest algorithm and provider
 * configuration to be set.
 */
final class MessageDigestPolicyImpl implements MessageDigestPolicy {

    private final ConfigMessageDigestAlgorithmAndProvider
            _configMessageDigestAlgorithmAndProvider;

    /**
     * Default constructor.
     *
     * @param algorithmAndProvider
     *            the message digester and the associated cryptographic service
     *            provider.
     */
    public MessageDigestPolicyImpl(
            final ConfigMessageDigestAlgorithmAndProvider algorithmAndProvider) {
        _configMessageDigestAlgorithmAndProvider = algorithmAndProvider;
    }

    @Override
    public ConfigMessageDigestAlgorithmAndProvider getMessageDigestAlgorithmAndProvider() {
        return _configMessageDigestAlgorithmAndProvider;
    }
}
