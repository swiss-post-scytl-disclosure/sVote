/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.encrytionparams;

import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;

public interface SubgroupParamsGenerator {

    /**
     * Generates {@code ElGamalEncryptionParameters}.
     *
     * @return parameters.
     */
    ElGamalEncryptionParameters generate();
}
