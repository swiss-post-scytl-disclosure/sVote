/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.configuration;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.commons.configuration.PolicyFromPropertiesHelper;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * An implementation of {@link ElGamalPolicy} which reads the policy parameters
 * from a properties source.
 */
public final class ElGamalPolicyFromProperties implements ElGamalPolicy {

    private final ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmProviderPair;

    private final ConfigGroupType _groupType;

    /**
     * Initializes a class instance with the properties specified at the
     * received path.
     * 
     * @param path
     *            the path where the properties are read from.
     */
    public ElGamalPolicyFromProperties(final String path) {

        try {

            PolicyFromPropertiesHelper helper =
                new PolicyFromPropertiesHelper(path);

            _secureRandomAlgorithmProviderPair =
                ConfigSecureRandomAlgorithmAndProvider
                    .valueOf(helper.getNotBlankOSDependentPropertyValue(
                        "elgamal.securerandom"));

            _groupType = ConfigGroupType.valueOf(
                helper.getNotBlankPropertyValue("elgamal.grouptype"));

        } catch (IllegalArgumentException e) {
            throw new CryptoLibException("Illegal property value", e);
        }
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
        return _secureRandomAlgorithmProviderPair;
    }

    @Override
    public ConfigGroupType getGroupType() {
        return _groupType;
    }
}
