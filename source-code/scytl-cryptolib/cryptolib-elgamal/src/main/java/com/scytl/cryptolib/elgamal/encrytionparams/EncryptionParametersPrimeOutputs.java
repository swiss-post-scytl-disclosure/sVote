/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.encrytionparams;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Class that encapsulates the output of calculating the p, q and seed
 * parameters.
 */
public final class EncryptionParametersPrimeOutputs {

    private final BigInteger _p;

    private final BigInteger _q;

    private final byte[] _seed;

    /**
     * Creates an EncryptionParametersPrimeOutputs, setting all of the
     * parameters of the class.
     * 
     * @param p
     *            the p parameter (modulus).
     * @param q
     *            the q parameter (order).
     * @param seed
     *            the seed bytes.
     */
    EncryptionParametersPrimeOutputs(final BigInteger p,
            final BigInteger q, final byte[] seed) {
        _p = p;
        _q = q;
        _seed = Arrays.copyOf(seed, seed.length);
    }

    public BigInteger getP() {
        return _p;
    }

    public BigInteger getQ() {
        return _q;
    }

    /**
     * Returns a copy of the seed.
     * 
     * @return a copy of the seed.
     */
    public byte[] getSeed() {
        return _seed.clone();
    }
}
