/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.service;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.PropertiesConfiguredFactory;

/**
 * Factory of {@link ElGamalService} objects which are non thread-safe.
 */
public class BasicElGamalServiceFactory extends
        PropertiesConfiguredFactory<ElGamalServiceAPI> implements
        ServiceFactory<ElGamalServiceAPI> {
    /**
     * Creates an instance of factory that will create services configured with
     * default values.
     */
    public BasicElGamalServiceFactory() {
        super(ElGamalServiceAPI.class);
    }

    /**
     * This factory creates services configured with the given configuration
     * file.
     * 
     * @param path
     *            the path of the properties file to be used to configure the
     *            service.
     */
    public BasicElGamalServiceFactory(final String path) {
        super(ElGamalServiceAPI.class, path);
    }

    @Override
    protected ElGamalServiceAPI createObjectWithPath()
            throws GeneralCryptoLibException {
        return new ElGamalService(_path);
    }

    @Override
    protected ElGamalServiceAPI createObjectUsingDefaultPath()
            throws GeneralCryptoLibException {
        return new ElGamalService();
    }
}
