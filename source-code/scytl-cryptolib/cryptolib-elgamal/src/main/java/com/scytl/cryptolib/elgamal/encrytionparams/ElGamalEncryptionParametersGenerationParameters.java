/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.encrytionparams;

import com.scytl.cryptolib.primitives.messagedigest.factory.CryptoMessageDigest;
import com.scytl.cryptolib.primitives.securerandom.factory.CryptoRandomBytes;
import com.scytl.cryptolib.securerandom.cryptoapi.CryptoAPIRandomBytes;

/**
 * Class which encapsulates the inputs that are the passed to
 * {@link EncryptionParameterGenerator}.
 * <P>
 * This class is quite similar to the class
 * {@link org.bouncycastle.crypto.params.DSAParameterGenerationParameters},
 * however this class has a very differences, including the fact that this class
 * contains a {@link CryptoRandomBytes} instead of a SecureRandom, and a
 * {@link CryptoMessageDigest} instead of a Digest.
 */
public final class ElGamalEncryptionParametersGenerationParameters {

    private final int _L;

    private final int _N;

    private final int _CERTAINTY;

    private final CryptoMessageDigest _digester;

    private final CryptoAPIRandomBytes _cryptoRandomBytes;

    /**
     * Instantiates an object with specified parameters.
     * 
     * @param L
     *            the length of the p parameter (modulus).
     * @param N
     *            the length of the q parameter (order).
     * @param CERTAINTY
     *            the certainty that the p and q parameters are prime.
     * @param digester
     *            the message digest to use.
     * @param cryptoRandomBytes
     *            random bytes generator.
     */
    ElGamalEncryptionParametersGenerationParameters(final int L,
            final int N, final int CERTAINTY,
            final CryptoMessageDigest digester,
            final CryptoAPIRandomBytes cryptoRandomBytes) {

        _L = L;
        _N = N;
        _CERTAINTY = CERTAINTY;
        _digester = digester;
        _cryptoRandomBytes = cryptoRandomBytes;
    }

    public int getL() {
        return _L;
    }

    public int getN() {
        return _N;
    }

    public int getCertainty() {
        return _CERTAINTY;
    }

    public CryptoMessageDigest getDigester() {
        return _digester;
    }

    public CryptoAPIRandomBytes getCryptoRandomBytes() {
        return _cryptoRandomBytes;
    }
}
