/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.service;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.services.ServiceFactory;
import com.scytl.cryptolib.commons.concurrent.PooledProxiedServiceFactory;

/**
 * This Service factory creates thread-safe services. Thread-safe services proxy
 * all requests to a pool of non thread-safe service instances.
 */
public class PollingElGamalServiceFactory extends
        PooledProxiedServiceFactory<ElGamalServiceAPI> implements
        ServiceFactory<ElGamalServiceAPI> {

    /**
     * Constructor that uses default values.
     */
    public PollingElGamalServiceFactory() {
        super(new BasicElGamalServiceFactory(),
            new GenericObjectPoolConfig());
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param path
     *            the path where the properties file is located.
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingElGamalServiceFactory(final String path,
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicElGamalServiceFactory(path), poolConfig);
    }

    /**
     * Constructor that uses the given path to read cryptographic properties and
     * the pool config to setup the pool of services.
     * 
     * @param poolConfig
     *            the configuration of the pool.
     */
    public PollingElGamalServiceFactory(
            final GenericObjectPoolConfig poolConfig) {
        super(new BasicElGamalServiceFactory(), poolConfig);
    }
}
