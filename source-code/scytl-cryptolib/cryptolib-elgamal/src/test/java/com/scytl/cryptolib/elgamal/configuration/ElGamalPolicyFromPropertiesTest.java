/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.commons.constants.Constants;
import com.scytl.cryptolib.commons.utils.system.OperatingSystem;
import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

import mockit.Deencapsulation;

public class ElGamalPolicyFromPropertiesTest {

    private static ConfigSecureRandomAlgorithmAndProvider _secureRandomAlgorithmProviderPair;

    private static ElGamalPolicy _cryptoElGamalPolicyFromProperties;

    private static OperatingSystem _os;

    @BeforeClass
    public static void setUp() {

        _secureRandomAlgorithmProviderPair =
            getCryptoSecureRandomAlgorithmAndProvider();
        _os = OperatingSystem.current();

    }

    @After
    public void revertSystemHelper() {
        Deencapsulation.setField(OperatingSystem.class, "current",
            _os);
    }

    @Test
    public void givenPolicyFromPropertiesWhenGetFieldsThenExpectedValues()
            throws GeneralCryptoLibException {

        _cryptoElGamalPolicyFromProperties =
            new ElGamalPolicyFromProperties(
                Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH);

        assertPolicyHasExpectedValues(_secureRandomAlgorithmProviderPair,
            _cryptoElGamalPolicyFromProperties);
    }

    @Test(expected = CryptoLibException.class)
    public void givenBasPathWhenBuildPolicyFromPropertiesThenException() {

        String badPath = "badPath";

        _cryptoElGamalPolicyFromProperties =
            new ElGamalPolicyFromProperties(badPath);
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateElGamalPolicyPassingAFileWithInvalidLabels() {
        new ElGamalPolicyFromProperties(
            "properties/cryptolibPolicy_invalidLabels.properties");
    }

    @Test(expected = CryptoLibException.class)
    public void whenCreateElGamalPolicyPassingAFileWithBlankLabels()
            throws GeneralCryptoLibException {
        new ElGamalPolicyFromProperties(
            "properties/cryptolibPolicy_blankLabels.properties");
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private void assertPolicyHasExpectedValues(
            final ConfigSecureRandomAlgorithmAndProvider pair,
            final ElGamalPolicy policy) {

        assertSecureRandomPairIsExpectedValue(pair, policy);
    }

    private void assertSecureRandomPairIsExpectedValue(
            final ConfigSecureRandomAlgorithmAndProvider pair,
            final ElGamalPolicy elGamalPolicy) {

        String errorMsg =
            "The SecureRandom algorithm and provider pair are not the expected values";
        assertEquals(errorMsg, pair,
            elGamalPolicy.getSecureRandomAlgorithmAndProvider());
    }

    private static ConfigSecureRandomAlgorithmAndProvider getCryptoSecureRandomAlgorithmAndProvider() {

        switch (OperatingSystem.current()) {

        case WINDOWS:
            return ConfigSecureRandomAlgorithmAndProvider.PRNG_SUN_MSCAPI;
        case LINUX:
            return ConfigSecureRandomAlgorithmAndProvider.NATIVE_PRNG_SUN;
        default:
            throw new CryptoLibException("OS not supported");
        }
    }
}
