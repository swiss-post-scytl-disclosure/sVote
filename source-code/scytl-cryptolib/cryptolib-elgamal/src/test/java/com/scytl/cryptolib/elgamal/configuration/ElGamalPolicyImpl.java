/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.cryptolib.elgamal.configuration;

import com.scytl.cryptolib.primitives.securerandom.configuration.ConfigSecureRandomAlgorithmAndProvider;

/**
 * Implementation of {@link ElGamalPolicy}, used for testing.
 */
public class ElGamalPolicyImpl implements ElGamalPolicy {

    private final ConfigSecureRandomAlgorithmAndProvider
            _secureRandomAlgorithmProviderPair;

    private final ConfigGroupType _groupType;

    public ElGamalPolicyImpl(
            final ConfigSecureRandomAlgorithmAndProvider secureRandomAlgorithmProviderPair,
            final ConfigGroupType groupType) {

        _secureRandomAlgorithmProviderPair = secureRandomAlgorithmProviderPair;
        _groupType = groupType;
    }

    @Override
    public ConfigSecureRandomAlgorithmAndProvider getSecureRandomAlgorithmAndProvider() {
        return _secureRandomAlgorithmProviderPair;
    }

    @Override
    public ConfigGroupType getGroupType() {
        return _groupType;
    }
}
