# JavaScript build tools

This project provides utilities to help build the `crytolib-js` modules. In order to use them, this dependency must be copied to the target folder of the given `cryptolib-js` module, at an appropriate time, for the tools to be available when they are required.

## `prepare-dist.js`

The `prepare-dist.js` file is a pre-publish script that takes care of organizing the source code of the `cryptolib-js` modules, so that only that which is required is published to the `npm` registry. It also updates the published `npm` package version number to match that of the module's POM file. If the `npm` publishing type is `release`, the string `-SNAPSHOT` will be removed from the version number. If the `npm` publishing type is `snapshot`, the string `-SNAPSHOT` will be replaced by the string `-patch.<time stamp>` in the version number. For default builds the `npm` publishing type will be set to `unpublished` so that no publishing action will be taken.

A module that needs to use this tool must declare a `cryptolib-js-buildtools` dependency in its `POM` file, and use the `maven-resources` plug-in to copy the tools to its target folder. In the following example, the tools will become available during the `generate-resources` phase.

```xml
<dependency>
    <groupId>com.scytl.cryptolib</groupId>
    <artifactId>cryptolib-js-buildtools</artifactId>
    <version>${project.version}</version>
</dependency>
...
<build>
    <plugins>
        ...
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-resources-plugin</artifactId>
            <executions>
                <execution>
                    <id>copy-buildtools</id>
                    <phase>generate-resources</phase>
                    <goals>
                        <goal>copy-resources</goal>
                    </goals>
                    <configuration>
                        <outputDirectory>${basedir}/target/dependency</outputDirectory>
                        <resources>
                            <resource>
                                <directory>${basedir}/../cryptolib-js-buildtools/src/main/js</directory>
                            </resource>
                        </resources>
                    </configuration>
                </execution>
            </executions>
        </plugin>
        ...
    </plugins>
</build>
```
To make use of the tools after they are copied to the folder `target/dependency`, the `package.json` file of the module must include the following three lines in its `scripts` section.

```json
"scripts": {
  ...
  "snapshot": "node target/dependency/prepare-dist.js 'snapshot' && npm publish target/dist/",
  "release": "node target/dependency/prepare-dist.js 'release' && npm publish target/dist/",
  "unpublished": "node target/dependency/prepare-dist.js",
  ...
},
```
