# maven-generic-conf

Scytl Super POM is the parent project for all Scytl projects.

It is the central point where global configuration is defined.

In maven the dependency is defined as:

```xml
    <parent>
        <groupId>com.scytl</groupId>
        <artifactId>maven-generic-conf</artifactId>
        <version>1.5.4</version>
    </parent>
```