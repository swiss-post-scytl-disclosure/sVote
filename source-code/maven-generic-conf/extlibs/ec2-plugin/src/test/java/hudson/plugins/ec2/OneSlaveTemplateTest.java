package hudson.plugins.ec2;

import hudson.plugins.ec2.one.OneEC2Cloud;
import hudson.plugins.ec2.one.OneEC2Tag;
import hudson.plugins.ec2.one.OneInstanceType;
import hudson.plugins.ec2.one.OneSlaveTemplate;

import java.util.ArrayList;
import java.util.List;

import org.jvnet.hudson.test.HudsonTestCase;

/**
 * Basic test to validate SlaveTemplate.
 */
public class OneSlaveTemplateTest extends HudsonTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        OpenNebula.testMode = true;
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        OpenNebula.testMode = false;
    }

    public void testConfigRoundtrip() throws Exception {
        String ami = "ami-00000022";

        OneEC2Tag tag1 = new OneEC2Tag("name1", "value1");
        OneEC2Tag tag2 = new OneEC2Tag("name2", "value2");
        List<OneEC2Tag> tags = new ArrayList<OneEC2Tag>();
        tags.add(tag1);
        tags.add(tag2);

        OneSlaveTemplate orig =
            new OneSlaveTemplate(ami, EC2Slave.TEST_ZONE, "default",
                "foo", "22", OneInstanceType.M1Jenkins, "ttt", "foo ami",
                "bar", "aaa", "10", "rrr", "fff", "-Xmx1g", false,
                "subnet 456", tags, null, false, null);

        List<OneSlaveTemplate> templates =
            new ArrayList<OneSlaveTemplate>();
        templates.add(orig);

        OpenNebula ac =
            new OpenNebula("http://opennebula.scytl.net:4568", "pollbook",
                "e31f60dbc44b76aba3ca4071e5268eef1b9e9e67", null, "3",
                templates);
        hudson.clouds.add(ac);

        submit(createWebClient().goTo("configure").getFormByName("config"));
        OneSlaveTemplate received =
            ((OneEC2Cloud) hudson.clouds.iterator().next())
                .getTemplate(ami);
        assertEqualBeans(
            orig,
            received,
            "ami,zone,description,remoteFS,type,jvmopts,stopOnTerminate,securityGroups,subnetId,usePrivateDnsName");
    }

    public void testConfigRoundtripWithPrivateDns() throws Exception {
        String ami = "ami-00000022";

        OneEC2Tag tag1 = new OneEC2Tag("name1", "value1");
        OneEC2Tag tag2 = new OneEC2Tag("name2", "value2");
        List<OneEC2Tag> tags = new ArrayList<OneEC2Tag>();
        tags.add(tag1);
        tags.add(tag2);

        OneSlaveTemplate orig =
            new OneSlaveTemplate(ami, EC2Slave.TEST_ZONE, "default",
                "foo", "22", OneInstanceType.M1Jenkins, "ttt", "foo ami",
                "bar", "aaa", "10", "rrr", "fff", "-Xmx1g", false,
                "subnet 456", tags, null, true, null);

        List<OneSlaveTemplate> templates =
            new ArrayList<OneSlaveTemplate>();
        templates.add(orig);

        OpenNebula ac =
            new OpenNebula("http://opennebula.scytl.net:4568", "pollbook",
                "e31f60dbc44b76aba3ca4071e5268eef1b9e9e67", null, "3",
                templates);
        hudson.clouds.add(ac);

        submit(createWebClient().goTo("configure").getFormByName("config"));
        OneSlaveTemplate received =
            ((OneEC2Cloud) hudson.clouds.iterator().next())
                .getTemplate(ami);
        assertEqualBeans(
            orig,
            received,
            "ami,zone,description,remoteFS,type,jvmopts,stopOnTerminate,securityGroups,subnetId,tags,usePrivateDnsName");
    }

}
