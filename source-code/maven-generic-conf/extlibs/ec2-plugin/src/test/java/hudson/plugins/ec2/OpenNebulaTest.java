package hudson.plugins.ec2;

import hudson.plugins.ec2.one.OneSlaveTemplate;

import java.util.Collections;

import org.jvnet.hudson.test.HudsonTestCase;

/**
 * @author Kohsuke Kawaguchi
 */
public class OpenNebulaTest extends HudsonTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        OpenNebula.testMode = true;
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        OpenNebula.testMode = false;
    }

    public void testConfigRoundtrip() throws Exception {
        OpenNebula orig =
            new OpenNebula("http://opennebula.scytl.net:4568", "pollbook",
                "qa4ever", null, "3",
                Collections.<OneSlaveTemplate> emptyList());
        hudson.clouds.add(orig);
        submit(createWebClient().goTo("configure").getFormByName("config"));

        assertEqualBeans(orig, hudson.clouds.iterator().next(),
            "accessId,secretKey,privateKey,instanceCap");
    }
}
