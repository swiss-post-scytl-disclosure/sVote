/**
 * $Id$
 * @author dcampos
 * @date   Nov 6, 2012 2:54:25 PM
 *
 * Copyright (C) 2012 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 *
 */
package hudson.plugins.ec2.one;

/**
 *
 */
public enum OneInstanceType {

    M0("m0"), M1("m1"), M2("m2"), M3("m3"), M4("m4"), M1Jenkins(
            "m1.jenkins"), M2Jenkins("m2.jenkins");

    private String value;

    /**
     * 
     */
    private OneInstanceType(final String value) {
        this.value = value;
    }

    public static OneInstanceType fromValue(final String value) {
        if (null == value) {
            throw new IllegalArgumentException(
                "Value cannot be null or empty!");
        }

        for (OneInstanceType it : values()) {
            if (it.toString().equalsIgnoreCase(value)) {
                return it;
            }
        }

        throw new IllegalArgumentException("Cannot create enum from "
            + value);
    }

    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return value;
    }

}
