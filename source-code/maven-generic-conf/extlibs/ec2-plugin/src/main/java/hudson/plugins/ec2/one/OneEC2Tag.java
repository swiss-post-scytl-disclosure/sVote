package hudson.plugins.ec2.one;

import hudson.model.Descriptor;
import hudson.model.AbstractDescribableImpl;
import hudson.Extension;
import org.kohsuke.stapler.DataBoundConstructor;

import java.util.List;
import java.util.LinkedList;

import com.amazonaws.services.ec2.model.Tag;


public class OneEC2Tag extends AbstractDescribableImpl<OneEC2Tag>
{
   private String name;
   private String value;

   @DataBoundConstructor
   public OneEC2Tag(String name, String value) {
      this.name = name;
      this.value = value;
   }


   /* Constructor from Amazon Tag */
   public OneEC2Tag(Tag t) {
       name = t.getKey();
       value = t.getValue();
   }


   public String getName() {
      return name;
   }


   public String getValue() {
      return value;
   }


   @Override
   public String toString() {
      return "EC2Tag: " + name + "->" + value;
   }


   @Override
   public boolean equals(Object o) {
      if (o == null) return false;
      if (!(o instanceof OneEC2Tag)) return false;

      OneEC2Tag other = (OneEC2Tag) o;
      if ((name == null && other.name != null) || !name.equals( other.name)) return false;
      if ((value == null && other.value != null) || !value.equals( other.value)) return false;

      return true;
   }


   @Extension
   public static class DescriptorImpl extends Descriptor<OneEC2Tag> {
      @Override
	  public String getDisplayName() { return ""; }
   }


   /* Helper method to convert lists of Amazon tags into internal format */ 
   public static List<OneEC2Tag> fromAmazonTags(List<Tag> amazon_tags) {
       if (null == amazon_tags) {
           return null;
       }

       LinkedList<OneEC2Tag> result = new LinkedList<OneEC2Tag>();
       for (Tag t : amazon_tags) {
           result.add(new OneEC2Tag(t));
       }

       return result;
   }
}
