To deploy the file to the Maven Repository do the following:

  alias mvndeploy "mvn deploy:deploy-file -Durl=scpexe://pnyx@nightly01/opt/jakarta-tomcat-5.0.28/webapps/maven-repos"
  mvndeploy -DgroupId=com.scytl.extlibs -DartifactId=ireport -Dversion=1.3.1 -Dpackaging=jar -Dfile=ireport.jar