To install the file do the following:

  alias mvndeploy "mvn deploy:deploy-file -Durl=scpexe://pnyx@nightly01/opt/jakarta-tomcat-5.0.28/webapps/maven-repos"
  mvndeploy -DgroupId=com.scytl.extlibs -DartifactId=barcode4j -Dversion=2.0alpha2 -Dpackaging=jar -Dfile=barcode4j.jar