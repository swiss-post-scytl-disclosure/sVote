/*
Multi-exponentiation modulo the specified modulus.
*/

#include "gmp.h"

#ifndef _MPZ_MULTI_POWM_H_
#define _MPZ_MULTI_POWM_H_

/*
Performs multi-exponentiation modulo the specified modulus.

value - the value
bases - the bases
exponents - the exponents
count  - the number of bases and exponents
modulus - the modulus.
*/
void mpz_multi_powm(mpz_t value, mpz_t *bases, mpz_t *exponents, size_t count, mpz_t modulus);

#endif
