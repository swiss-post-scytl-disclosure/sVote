#include <stdlib.h>
#include "mpz_multi_powm.h"
#include "mpz_multi_powm_bg.h"
#include "mpz_multi_powm_ll.h"
#include "mpz_multi_powm_wu.h"

static size_t get_max_bit_count(mpz_t *exponents, size_t count);

void mpz_multi_powm(mpz_t value, mpz_t *bases, mpz_t *exponents, size_t count, mpz_t modulus)
{
    size_t bit_count = get_max_bit_count(exponents, count);
    mpz_multi_powm_wu(value, bases, exponents, count, modulus, bit_count);
}

/*
Returns the maximum number of bits in given numbers.

numbers - the numbers
count - the number of numbers.
*/
static size_t get_max_bit_count(mpz_t *numbers, size_t count)
{
    size_t bit_count = 0;
    int i;
    for (i = 0; i < count; i++)
    {
        size_t size = mpz_sizeinbase(numbers[i], 2);
        if (bit_count < size)
        {
           bit_count = size;
        }
    }
    return bit_count;
}
