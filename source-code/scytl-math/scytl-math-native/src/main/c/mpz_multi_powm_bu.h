/*
Binary algorithm for multi-exponentiation described in
http://dasan.sejong.ac.kr/~chlim/pub/multi_exp.ps
*/

#include "gmp.h"

#ifndef _MPZ_MULTI_POWM_BU_H_
#define _MPZ_MULTI_POWM_BU_H_

/*
Computes multi-exponention modulo some number for given bases and exponents.

value - the computation result
bases - the bases
exponents - the exponents
count - the number of bases and exponents
modulus - the modulus.
*/
void mpz_multi_powm_bu(mpz_t value, mpz_t *bases, mpz_t *exponents, size_t count, mpz_t modulus);

#endif
