/*
Functions for conversion from mpz_t to jbyteArray and back.
*/

#include "gmp.h"
#include "jni.h"

#ifndef _MPZ_JNI_H_
#define _MPZ_JNI_H_

/*
Initializes and set a given integer value with data from the specified Java array of bytes.

value - the value
env - the JNI environment
array - the array.
*/
void mpz_init_set_jbyteArray(mpz_t value, JNIEnv *env, jbyteArray array);

/*
Returns a given value as a Java array of bytes.

env - the JNI environment
value - the value.
*/
jbyteArray mpz_get_jbyteArray(JNIEnv *env, mpz_t value);

#endif
