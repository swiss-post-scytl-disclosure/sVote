#include "mpz_multi_powm_bu.h"

void mpz_multi_powm_bu(mpz_t value, mpz_t *bases, mpz_t *exponents, size_t count, mpz_t modulus)
{
    mpz_t power;
    mpz_init2(power, mpz_sizeinbase(modulus, 2));

    mpz_set_si(value, 1);
    int i;
    for (i = 0; i < count; i++)
    {
        mpz_powm(power, bases[i], exponents[i], modulus);
        mpz_mul(value, value, power);
        mpz_mod(value, value, modulus);
    }

    mpz_clear(power);
}
