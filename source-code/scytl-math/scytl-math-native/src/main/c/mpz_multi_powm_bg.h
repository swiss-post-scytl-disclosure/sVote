/*
Brickell et al's algorithm for multi-exponentiation described in
http://dasan.sejong.ac.kr/~chlim/pub/multi_exp.ps
*/

#include "gmp.h"

#ifndef _MPZ_MULTI_POWM_BG_H_
#define _MPZ_MULTI_POWM_BG_H_

/*
Computes multi-exponention modulo some number for given bases and exponents.

value - the computation result
bases - the bases
exponents - the exponents
count - the number of bases and exponents
modulus - the modulus
bit_count - the number of bits in the greatest exponent.
*/
void mpz_multi_powm_bg(mpz_t value, mpz_t *bases, mpz_t *exponents, size_t count, mpz_t modulus, size_t bit_count);

#endif
