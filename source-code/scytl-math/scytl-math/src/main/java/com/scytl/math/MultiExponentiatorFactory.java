/*
 * $Id$
 * @author aakimov
 * @date   Jun 30, 2016 7:33:17 PM
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
package com.scytl.math;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnegative;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Factory of {@link MultiExponentiator}.
 * 
 * @author aakimov
 */
@ThreadSafe
public interface MultiExponentiatorFactory {
    /**
     * Creates a new {@link MultiExponentiator} instance for given bases,
     * modulus and maximum bit length of exponent.
     * 
     * @param bases
     *            the bases
     * @param modulus
     *            the modulus, must be positive
     * @param maximumBitLengthOfExponent
     *            the maximum bit length of exponent, must be positive
     * @return the new instance
     * @throws IllegalArgumentException
     *             some bases are negative, the modulus is not positive or the
     *             maximum bit length is not positive.
     */
    MultiExponentiator newMultiExponentiator(
            @Nonnegative List<BigInteger> bases,
            @Nonnegative BigInteger modulus,
            @Nonnegative int maximumBitLengthOfExponent);
}
