/*
 * $Id$
 * 
 * @author aakimov
 * 
 * @date Jun 16, 2016 5:24:18 PM
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
/**
 * This package provides extensions and optimizations to basic mathematical
 * abstractions defined in {@code java.math} package.
 * <p>
 * The public API consists of
 * <ul>
 * <li>{@link com.scytl.math.BigIntegers}. Utility class with provides some
 * extension and optimization to standard {@link java.math.BigInteger}.</li>
 * <li>{@link com.scytl.math.MultiExponentiator}. Abstract interface which llows
 * to perform a series of multi-exponentiation operations for given bases and
 * modulus.</li>
 * <li>{@link com.scytl.math.MultiExponentiatorFactory}. Abstract factory for
 * creating {@link com.scytl.math.MultiExponentiator} instances.</li>
 * <li>{@link com.scytl.math.MultiExponentiatorFactoryImpl}. Default
 * implementation of {@link com.scytl.math.MultiExponentiatorFactory}.</li>
 * </ul>
 * 
 * @author aakimov
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.scytl.math;