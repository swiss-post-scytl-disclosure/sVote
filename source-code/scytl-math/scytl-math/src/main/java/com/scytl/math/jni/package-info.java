/*
 * $Id$
 * 
 * @author aakimov
 * 
 * @date Jun 16, 2016 7:00:27 PM
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
/**
 * This package provides JNI facilities.
 * <p>
 * The public API consists of
 * <ul>
 * <li>{@link com.scytl.math.jni.JNI}. Utility class which is responsible for
 * loading the optional native library.</li>
 * </ul>
 * 
 * @author aakimov
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.scytl.math.jni;