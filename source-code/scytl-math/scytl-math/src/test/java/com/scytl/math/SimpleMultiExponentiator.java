/*
 * $Id$
 * @author aakimov
 * @date   Jun 30, 2016 7:55:54 PM
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
package com.scytl.math;

import static com.scytl.math.BigIntegers.modMultiply;

import java.math.BigInteger;
import java.util.List;

/**
 * Simple implementation of {@link MultiExponentiator} which uses individual
 * multi-exponentiation.
 * 
 * @author aakimov
 */
class SimpleMultiExponentiator extends AbstractMultiExponentiator {

    /**
     * Constructor.
     * 
     * @param bases
     * @param modulus
     * @param maximumBitLengthOfExponent
     */
    public SimpleMultiExponentiator(List<BigInteger> bases, BigInteger modulus,
            int maximumBitLengthOfExponent) {
        super(bases, modulus, maximumBitLengthOfExponent);
    }

    @Override
    public BigInteger doExponentiate(BigInteger[] exponents) {
        BigInteger value = BigInteger.ONE;
        for (int i = 0; i < bases.length; i++) {
            BigInteger power = bases[i].modPow(exponents[i], modulus());
            value = modMultiply(value, power, modulus());
        }
        return value;
    }
}
