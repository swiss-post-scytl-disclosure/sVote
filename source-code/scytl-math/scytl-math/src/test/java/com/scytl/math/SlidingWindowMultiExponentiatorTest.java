/*
 * $Id$
 * @author aakimov
 * @date   Jul 1, 2016 12:42:52 PM
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
package com.scytl.math;

import java.math.BigInteger;
import java.util.List;

/**
 * Tests of {@link SlidingWindowMultiExponentiator}.
 * 
 * @author aakimov
 */
public class SlidingWindowMultiExponentiatorTest
        extends GenericMultiExponentiatorTest {
    @Override
    protected MultiExponentiator newMultiExponentiator(List<BigInteger> bases,
            BigInteger modulus, int maximumBitLengthOfExponent) {
        return new SlidingWindowMultiExponentiator(bases, modulus,
                maximumBitLengthOfExponent);
    }
}
