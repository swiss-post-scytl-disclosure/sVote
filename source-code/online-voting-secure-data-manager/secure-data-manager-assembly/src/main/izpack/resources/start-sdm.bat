@echo off

set sdmDir=%~dp0

if exist "%USERPROFILE%\sdm" goto startSDM
	xcopy "%sdmDir%sdm" "%USERPROFILE%\sdm" /D /E /C /R /I /K /Y 
:startSDM
start "" "%sdmDir%Secure Data Manager.exe"