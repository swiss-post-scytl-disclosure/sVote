#!/usr/bin/env bash
NEXUS_DIR=https://nexus.scytl.net/service/local/repositories

mkdir jars
cd jars
wget ${NEXUS_DIR}/applications/content/bsf/bsf/2.4.0/bsf-2.4.0.jar
wget ${NEXUS_DIR}/applications/content/org/codehaus/groovy/groovy-all/2.4.5/groovy-all-2.4.5.jar
wget ${NEXUS_DIR}/applications/content/commons-logging/commons-logging/1.2/commons-logging-1.2.jar
wget ${NEXUS_DIR}/applications/content/com/scytl/izpack/validator/1.0.0/validator-1.0.0.jar
wget ${NEXUS_DIR}/applications/content/tools/groovy-ant/2.1.1/groovy-ant-2.1.1.jar
wget ${NEXUS_DIR}/central/content/org/apache/ant/ant/1.9.5/ant-1.9.5.jar
wget ${NEXUS_DIR}/applications/content/tools/ant-launcher/1.9.3/ant-launcher-1.9.3.jar

cd ..
unzip mixnet-engine*distribution.zip -d install/sdm/mixing
mv *.jar install/sdm
unzip sdmConfig.zip -d install/sdm
unzip ov-secure-data-manager*.zip -d install

cp install/sdm/sdmConfig/admin_portal.properties admin_portal.properties.bak
cp install/sdm/sdmConfig/voting_portal.properties voting_portal.properties.bak

sed 's/192.168.10.151:8080/${Admin_Portal}/' admin_portal.properties.bak > install/sdm/sdmConfig/admin_portal.properties
sed 's/localhost:8080/${Voting_Portal}/' voting_portal.properties.bak > install/sdm/sdmConfig/voting_portal.properties
rm *.bak

cd install 
wget ${NEXUS_DIR}/thirdparty/content/org/apache/tomcat/apache-tomcat/8.5.32-windows/apache-tomcat-8.5.32-windows-x64.zip
wget ${NEXUS_DIR}/thirdparty/content/org/apache/tomcat/apache-tomcat/8.5.32-windows/apache-tomcat-8.5.32-windows-x86.zip
wget ${NEXUS_DIR}/thirdparty/content/org/apache/tomcat/apache-tomcat/8.5.32/apache-tomcat-8.5.32.zip
wget ${NEXUS_DIR}/jenkins-ci/content/org/bouncycastle/bcprov-jdk15on/1.59/bcprov-jdk15on-1.59.jar
wget ${NEXUS_DIR}/applications/content/com/oracle/UnlimitedJCEPolicy/8/UnlimitedJCEPolicy-8.zip
wget ${NEXUS_DIR}/applications/content/com/safesign/SafeSign-x64/1.0/SafeSign-x64-1.0.exe
wget ${NEXUS_DIR}/applications/content/com/safesign/Safesign/3.0.93/Safesign-3.0.93.exe
wget ${NEXUS_DIR}/applications/content/com/ifdokccid/ifdokccid_linux_i686/4.2.4/ifdokccid_linux_i686-4.2.4.zip
wget ${NEXUS_DIR}/applications/content/com/ifdokccid/ifdokccid_linux_x86_64/4.2.4/ifdokccid_linux_x86_64-4.2.4.zip

sudo docker rm -vf  izpack || true
sudo docker run -v $WORKSPACE:/install --name izpack 10.4.0.107:5000/izpack compile -b /install /install/install.xml
