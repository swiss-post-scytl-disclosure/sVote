SDM Installer
==============

Contains all the scripts that are necessary for the creation of Secure Data Manager Installer.


Description
-----

* Packaging to jar is done with Izpack 5. The documentation of Izpack can be found [here](https://izpack.atlassian.net/wiki/display/IZPACK/IzPack+5).

The packaging with izpack is done with an Izpack Docker image: 10.4.0.107:5000/izpack.
In order to pack it execute the following command:

	sudo docker run -v $WORKSPACE:/install --name izpack 10.4.0.107:5000/izpack compile -b /install /install/install.xml

$WORKSPACE is the location where install.xml is.
After the packaging is finished you will see install.jar in the workspace folder.


* The packaging from jar to exe is done with launch4j. The documentation is [here](http://launch4j.sourceforge.net/docs.html).

The packaging from jar to exe is done with a Launch4j Docker image: 10.4.0.107:5000/launch4j.

	sudo docker run -v $WORKSPACE:/install --name launch4j 10.4.0.107:5000/launch4j java -jar /launch4j/launch4j.jar launch4j-config-file.xml 

$WORKSPACE is the location where install.jar file is.

* Installation of SDM to Linux can be done with install.jar file.

In order to install it in Linux run:

	sudo java  -Duser.home=$HOME -jar install.jar

