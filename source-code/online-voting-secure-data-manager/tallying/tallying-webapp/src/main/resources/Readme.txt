The tallying service will get a decrypted ballot box file and generate several files.
For each decrypted ballot box it generates:
Several CSV files inside the folder of the corresponding decrypted ballot, one for each contest of the ballot box.
The name of the files are the contest IDs.

An example of the RESTful request is in Tallying.json
An example of reply is the following:

{
  "tallyingResults": {
    "27aa7af87de444758e58623b88431d6b": [
      "2;17;23;29;37",
      "2;29;37;53;59",
      "1;11;13;17;23"
    ],
    "d1d24859cbef408b816a1467b0b3afc7": [
      "3;2",
      "1;3",
      "1;5"
    ]
  },
  "errors": []
}

Where 27aa7af87de444758e58623b88431d6b and d1d24859cbef408b816a1467b0b3afc7 are contest IDs,
the first number of each line is the counter.
For example, 2;17;23;29;37 means that the vote 17;23;29;37 was found twice in contest with
ID 27aa7af87de444758e58623b88431d6b.
