/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.webapp.mvc.dto.output;

import com.scytl.products.ov.tallying.commons.beans.TallyingError;

import java.util.List;

public class TallyingWebappOutput {

	private List<TallyingError> _errors;

	public List<TallyingError> getErrors() {
		return _errors;
	}

	public void setErrors(List<TallyingError> errors) {
		_errors = errors;
	}

}
