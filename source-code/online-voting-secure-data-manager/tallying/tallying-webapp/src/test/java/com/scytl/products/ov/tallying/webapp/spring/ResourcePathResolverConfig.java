/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.webapp.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.products.ov.tallying.path.PathResolver;
import com.scytl.products.ov.tallying.path.ResourcePathResolver;

@Configuration
public class ResourcePathResolverConfig {

	@Bean
	public PathResolver getPrefixPathResolver() {
		return new ResourcePathResolver();
	}
}
