/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.webapp.mvc;

import java.util.UUID;

public class ContestInformation {

	private String contestName = UUID.randomUUID().toString();
	private String contestType = UUID.randomUUID().toString();

	public ContestInformation(String contestName) {
		super();
		this.contestName = contestName;
	}

	public String getContestName() {
		return contestName;
	}

	public String getContestType() {
		return contestType;
	}

}
