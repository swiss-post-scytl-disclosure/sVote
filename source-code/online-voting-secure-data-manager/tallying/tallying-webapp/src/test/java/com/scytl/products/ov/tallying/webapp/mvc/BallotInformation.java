/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.webapp.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BallotInformation {

	private String electionId = UUID.randomUUID().toString();
	private String bbId = UUID.randomUUID().toString();
	private List<ContestInformation> contests = new ArrayList<>();

	public BallotInformation(String electionId, String bbId, List<ContestInformation> contests) {
		super();
		this.electionId = electionId;
		this.bbId = bbId;
		if (contests != null) {
			this.contests = contests;
		}
	}

	public String getElectionId() {
		return electionId;
	}

	public String getBbId() {
		return bbId;
	}

	public List<ContestInformation> getContests() {
		return contests;
	}

}
