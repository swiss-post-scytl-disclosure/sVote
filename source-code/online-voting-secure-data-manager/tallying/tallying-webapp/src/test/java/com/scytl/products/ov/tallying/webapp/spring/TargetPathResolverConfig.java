/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.webapp.spring;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.products.ov.tallying.path.PathResolver;

@Configuration
public class TargetPathResolverConfig {
	
	private static final Path temDir;
	
	static {
		 try {
			temDir = Files.createTempDirectory(Paths.get("target"),"tallytest");
			temDir.toFile().deleteOnExit();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Bean
	public PathResolver getPrefixPathResolver() {
		return new PathResolver() {
			
			@Override
			public Path resolve(String path) {
				return temDir;
			}
		};
	}
}
