Feature: tally service
	Scenario: tally one ballot box contest type referendum
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 5	3
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 1;3	1;5

	Scenario: tally two ballot box
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 5	3
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: 11	3
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 1;3	1;5
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: 1;3	1;11

	Scenario: tally two ballot boxes with write-ins
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 5;5#Mrx	5;5#Mrx	5;5#MrX	3
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: 11	3
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 1;3	2;5;5#Mrx	1;5;5#MrX
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: 1;3	1;11

	Scenario: tally two ballot boxes with write-ins
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 5;5#Mrx	2;2#Mrx	5;5#MrX	3
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: 11	3
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 1;3	1;2;2#Mrx	1;5;5#Mrx	1;5;5#MrX
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: 1;3	1;11

   	Scenario: tally two ballot boxes with write-ins
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 5;5#Mrx	5;5#Mrx	5;5#MrX	3
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: 11	3
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 1;3	2;5;5#Mrx	1;5;5#MrX
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: 1;3	1;11
        
   	Scenario: tally two ballot boxes with write-ins
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 3;5;2;5#Mrx;2#Mrsx	2;2#Mrx	5;5#MrX	3	3;5;2;5#Mrx;2#Mrsx
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: 11	3
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 2;3;5;2;5#Mrx;2#Mrsx	1;3	1;2;2#Mrx	1;5;5#MrX
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: 1;3	1;11

   	Scenario: tally two ballot boxes with write-ins
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 5;5#Mrx##	5;5#Mrx	5;5#MrX	3
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: 11	3	5;5#MrX
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 1;3	1;5;5#Mrx##	1;5;5#Mrx	1;5;5#MrX
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: 1;3	1;11	1;5;5#MrX

	Scenario: tally empty ballot box
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: EMPTY
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: EMPTY

	Scenario: tally two empty ballot box
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: EMPTY
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: EMPTY
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: EMPTY
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: EMPTY

	Scenario: tally two ballot box one empty
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: 5	3
		And also a decrypted ballot box with contestId Contest1 with id 49 and with decryptedBallots: EMPTY
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: 1;3	1;5
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 49 is: EMPTY

	Scenario: tally one ballot box with 3 contest types
		Given a decrypted ballot box with contestId Contest1, Contest2, Contest3 with id 45 and with decryptedBallots: 2;67;73;79;13	5;37;53;59;13
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 45 is: 1;2	1;5
		Then the outcome upon tallying the ballot box with contestId Contest2 with id 45 is: 2;13
		Then the outcome upon tallying the ballot box with contestId Contest3 with id 45 is: 1;67;73;79	1;37;53;59

	Scenario: tally one ballot box with auditable votes
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: EMPTY
		Given a decrypted ballot box with contestId Contest1 with id 43 and with auditableVotes: 2016-09-27T09:17:44.309Z;RULE_VALIDATION,NON_FACTORIZABLE_REMAINDER,DUPLICATED_FACTOR;6370;2^1 5^1 7^2 13^1	2016-09-27T09:17:44.556Z;RULE_VALIDATION,NON_FACTORIZABLE_REMAINDER,DUPLICATED_FACTOR;6370;2^1 5^1 7^2 13^1	2016-09-27T09:17:44.557Z;RULE_VALIDATION,NON_FACTORIZABLE_REMAINDER,DUPLICATED_FACTOR;6370;2^1 5^1 7^2 13^1
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: EMPTY
		Then the invalid votes upon tallying the ballot box with contestId Contest1 with id 43 is: 0;cleansing_invalid_votes	3;decryption_invalid_votes	3;total_invalid_votes

	Scenario: tally one ballot box with failed votes
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: EMPTY
		Given a decrypted ballot box with contestId Contest1 with id 43 and with failedVotes: 9ead024a8be94dbcbe4a530ac7b1a782;2016-09-26T13:11:49.481Z;NOT_CONFIRMED;4iZv3uAFM+NM9EeWpbxvCWwu1KmKUIPSDXKbFQNY59c=
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: EMPTY
		Then the invalid votes upon tallying the ballot box with contestId Contest1 with id 43 is: 1;cleansing_invalid_votes	0;decryption_invalid_votes	1;total_invalid_votes

	Scenario: tally one ballot box with auditable votes and failed votes
		Given a decrypted ballot box with contestId Contest1 with id 43 and with decryptedBallots: EMPTY
		Given a decrypted ballot box with contestId Contest1 with id 43 and with failedVotes: 9ead024a8be94dbcbe4a530ac7b1a782;2016-09-26T13:11:49.481Z;NOT_CONFIRMED;4iZv3uAFM+NM9EeWpbxvCWwu1KmKUIPSDXKbFQNY59c=
		Given a decrypted ballot box with contestId Contest1 with id 43 and with auditableVotes: 2016-09-27T09:17:44.309Z;RULE_VALIDATION,NON_FACTORIZABLE_REMAINDER,DUPLICATED_FACTOR;6370;2^1 5^1 7^2 13^1	2016-09-27T09:17:44.556Z;RULE_VALIDATION,NON_FACTORIZABLE_REMAINDER,DUPLICATED_FACTOR;6370;2^1 5^1 7^2 13^1	2016-09-27T09:17:44.557Z;RULE_VALIDATION,NON_FACTORIZABLE_REMAINDER,DUPLICATED_FACTOR;6370;2^1 5^1 7^2 13^1
		When tallying
		Then the outcome upon tallying the ballot box with contestId Contest1 with id 43 is: EMPTY
		Then the invalid votes upon tallying the ballot box with contestId Contest1 with id 43 is: 1;cleansing_invalid_votes	3;decryption_invalid_votes	4;total_invalid_votes

