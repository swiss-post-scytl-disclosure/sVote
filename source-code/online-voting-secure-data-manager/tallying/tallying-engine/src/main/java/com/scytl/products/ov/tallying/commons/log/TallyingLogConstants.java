/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.log;

/**
 * Constants to secure log tallying.
 */
public class TallyingLogConstants {

	public static final String FILE_NAME = "#file_name";

    public static final String FILE_HASH = "#file_h";

    public static final String CERT_ID = "#cert_id";

    public static final String CERT_COMMON_NAME = "#cert_cn";

    public static final String CERT_SERIAL_NUMBER = "#cert_sn";

    public static final String ERR_DESC = "#err_desc";

    public static final String TALLY_SHEET_ID = "#tally_sheet_id";

    public static final String TALLY_SHEET_HASH = "#tally_sheet_h";

    /**
     * Non-public constructor
     */
	private TallyingLogConstants() {
    }
}