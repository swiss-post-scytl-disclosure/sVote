/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.io.reader;

import com.googlecode.jcsv.reader.CSVEntryParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * A utility class used by BallotReader to read line by line csv entries (decrypted values) and convert them to Strings.
 */
public final class DecryptedBallotEntryParser implements CSVEntryParser<List<String>> {

    /**
     * @see CSVEntryParser#parseEntry(String[])
     */
    @Override
    public List<String> parseEntry(final String... args) {

        List<String> elements = new ArrayList<>(args.length);

        Collections.addAll(elements, args);

        return elements;
    }
}
