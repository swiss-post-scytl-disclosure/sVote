/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.io.reader;

import com.googlecode.jcsv.reader.CSVEntryParser;
import com.scytl.products.ov.tallying.commons.beans.AuditableVote;

/*
 * A utility class used by {@link com.scytl.products.ov.tallying.commons.io.reader.AuditableVotesReader}
 * to read line by line csv entries (auditable vote values) and convert them to Strings.
 */
public final class AuditableVoteEntryParser implements CSVEntryParser<AuditableVote> {

    /**
     * @see CSVEntryParser#parseEntry(String[])
     */
    @Override
    public AuditableVote parseEntry(final String... args) {

        return new AuditableVote(args[1], args[2], args[3]);
    }
}
