/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.beans;

/**
 * Class for storing failed vote that was read from failed votes file. This file is created during the cleansing process.
 */
public class FailedVote {

    private final String votingCardId;

    private final String status;

    private final String receipt;

    public FailedVote(final String votingCardId, final String status, final String receipt) {
        this.votingCardId = votingCardId;
        this.status = status;
        this.receipt = receipt;
    }

    public String getReceipt() {
        return receipt;
    }

    public String getStatus() {
        return status;
    }

    public String getVotingCardId() {
        return votingCardId;
    }
}
