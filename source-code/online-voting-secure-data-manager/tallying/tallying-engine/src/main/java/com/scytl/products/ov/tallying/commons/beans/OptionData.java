/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.beans;

public class OptionData {

    private final String representation;
	private final String writeInValue;

    public OptionData(final String representation, String writeInValue) {
        this.representation = representation;
		this.writeInValue = writeInValue;
    }

    public String getRepresentation() {
        return representation;
    }

	public String getWriteInValue() {
		return writeInValue;
	}
    

}
