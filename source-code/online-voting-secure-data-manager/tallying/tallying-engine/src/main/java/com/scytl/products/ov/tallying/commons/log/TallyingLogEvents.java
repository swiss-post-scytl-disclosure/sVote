/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

public enum TallyingLogEvents implements LogEvent {

    DECRYPTION_VOTES_FILE_NOT_FOUND("TAL","TSREAD", "1301", "Decrypted votes file was not found"),
    AUDITABLE_VOTES_FILE_NOT_FOUND("TAL","TSREAD", "1302", "Auditable votes file was not found"),
    FAILED_VOTES_FILE_NOT_FOUND("TAL","TSREAD", "1303", "Failed votes file was not found"),
    DECRYPTED_VOTES_READING_FAILED("TAL","TSREAD", "1304", "Error occurred while reading decrypted votes file"),
    AUDITABLE_VOTES_READING_FAILED("TAL","TSREAD", "1305", "Error occurred while reading auditable votes file"),
    FAILED_VOTES_READING_FAILED("TAL","TSREAD", "1306", "Error occurred while reading failed votes file"),
    DECRYPTED_VOTES_FILE_SIGNATURE_INVALID("TAL","TSREAD", "1307", "Decrypted votes file signature is not valid"),
    AUDITABLE_VOTES_FILE_SIGNATURE_INVALID("TAL","TSREAD", "1308", "Auditable votes file signature is not valid"),
    FAILED_VOTES_FILE_SIGNATURE_INVALID("TAL","TSREAD", "1309", "Failed votes file signature is not valid"),
    DECRYPTED_VOTES_FILE_SIGNATURE_VALID("TAL","TSREAD", "000", "Decrypted votes file signature successfully verified"),
    AUDITABLE_VOTES_FILE_SIGNATURE_VALID("TAL","TSREAD", "000", "Auditable votes file signature successfully verified"),
    FAILED_VOTES_FILE_SIGNATURE_VALID("TAL","TSREAD", "000", "Failed votes file signature successfully verified"),
    TALLYING_PROCESS_FAILURE("TAL","TSGEN", "1310", "Error during the tallying process"),
    TALLYING_FILE_CREATED_SUCCESSFULLY("TAL","TSGEN", "000", "Tallying file was created successfully"),
    TALLYING_FILE_CREATION_FAILED("TAL","TSGEN", "1313", "Error during tallying file creating"),
    TALLYING_FILE_SIGNED_SUCCESSFULLY("TAL","TSSIGN", "000", "Tallying file was signed successfully"),
    TALLYING_FILE_SIGNING_FAILED("TAL","TSREAD", "1311", "Error during tallying file signing"),
    SIGNATURE_CERTIFICATE_RECOVERING_FAILED("TAL","TSREAD", "1312", "Error during signature certificate recovering"),
    SIGNATURE_CERTIFICATE_RECOVERED_SUCCESSFULLY("TAL","TSREAD", "000", "Signature certificate was recovered successfully");

    private final String layer;

    private final String action;

    private final String outcome;

    private final String info;

    TallyingLogEvents(String layer, String action, String outcome, String info) {

        this.layer = layer;
        this.action = action;
        this.outcome = outcome;
        this.info = info;
    }

    @Override
    public String getLayer() {
        return layer;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public String getOutcome() {
        return outcome;
    }

    @Override
    public String getInfo() {
        return info;
    }
}