/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.beans;


/**
 * Class for storing auditable vote that was read from auditable votes file. This file is created during the decrypting process.
 */
public class AuditableVote {

    private final String failures;

    private final String compressed;

    private final String factorization;

    public AuditableVote(final String failures, final String compressed, final String factorization) {
        this.failures = failures;
        this.compressed = compressed;
        this.factorization = factorization;
    }

    public String getFailures() {
        return failures;
    }

    public String getCompressed() {
        return compressed;
    }

    public String getFactorization() {
        return factorization;
    }
}
