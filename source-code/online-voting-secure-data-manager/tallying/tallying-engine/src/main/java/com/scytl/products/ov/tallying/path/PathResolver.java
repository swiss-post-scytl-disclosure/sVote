/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.path;

import java.nio.file.Path;

/**
 * Interface for resolving path.
 */
public interface PathResolver {

	Path resolve(final String path);
}
