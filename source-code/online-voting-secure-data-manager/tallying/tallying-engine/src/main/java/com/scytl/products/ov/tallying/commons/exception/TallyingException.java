/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.exception;

public class TallyingException extends Exception {

	private static final long serialVersionUID = 4468397514712183041L;

	public TallyingException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public TallyingException(final String message) {
		super(message);
	}

}
