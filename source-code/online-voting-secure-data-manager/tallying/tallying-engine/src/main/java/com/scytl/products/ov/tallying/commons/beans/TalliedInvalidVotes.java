/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.beans;

public class TalliedInvalidVotes {

    private final int numberOfFailedVotes;

    private final int numberOfAuditableVotes;

    public TalliedInvalidVotes(int numberOfAuditableVotes, int numberOfFailedVotes) {
        this.numberOfAuditableVotes = numberOfAuditableVotes;
        this.numberOfFailedVotes = numberOfFailedVotes;
    }

    public int getNumberOfFailedVotes() {
        return numberOfFailedVotes;
    }

    public int getNumberOfAuditableVotes() {
        return numberOfAuditableVotes;
    }

    public int getTotalNumberOfVotes() {
        return numberOfAuditableVotes + numberOfFailedVotes;
    }
}
