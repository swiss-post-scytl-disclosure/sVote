/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.beans.Ballot;
import com.scytl.products.ov.commons.beans.Contest;
import com.scytl.products.ov.commons.beans.ElectionOption;

/**
 * A class to parse a ballot and return all necessary information structured
 * properly to be used by the tallying process.
 */
public class MappingStrategy {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    public Map<String, Contest> mapBallot(final Ballot ballot) {

        final Map<String, Contest> optionContestMappings = new HashMap<>();

        for (final Contest contest : ballot.getContests()) {

            LOG.info("Parsing Ballot " + ballot.getId() + " Contest " + contest.getId() + "...");
            for (ElectionOption option : contest.getOptions()) {
                optionContestMappings.put(option.getRepresentation(), contest);
            }
        }

        return optionContestMappings;
    }
}
