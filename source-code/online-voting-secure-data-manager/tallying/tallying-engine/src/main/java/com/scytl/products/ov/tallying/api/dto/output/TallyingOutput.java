/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.api.dto.output;

import com.scytl.products.ov.tallying.commons.beans.ContestVote;
import com.scytl.products.ov.tallying.commons.beans.TalliedInvalidVotes;
import com.scytl.products.ov.tallying.commons.beans.TallyingError;

import java.util.ArrayList;
import java.util.List;

public class TallyingOutput {

    private final List<TallyingError> errors = new ArrayList<>();

    private List<ContestVote> contestVotes;

    private TalliedInvalidVotes talliedInvalidVotes;

    public List<TallyingError> getErrors() {
        return errors;
    }

    public List<ContestVote> getContestVotes() {
        return contestVotes;
    }

    public void setContestVotes(List<ContestVote> contestVotes) {
        this.contestVotes = contestVotes;
    }

    public void addError(TallyingError tallyingError) {
        errors.add(tallyingError);
    }

    public void setTalliedInvalidVotes(TalliedInvalidVotes talliedInvalidVotes) {
        this.talliedInvalidVotes = talliedInvalidVotes;
    }

    public TalliedInvalidVotes getTalliedInvalidVotes() {
        return talliedInvalidVotes;
    }
}
