/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.beans;

/**
 * A class to centralize all the decryption error codes.
 */
public class ErrorCodes {

	public static final String TALLY_EXCEPTION = "001";

    public static final String BALLOT_NOT_ENRICHED = "002";

    public static final String UNKNOWN_ERROR = "003";

    public static final String SIGNATURE_VALIDATION_EXCEPTION = "004";

    /**
	 * Non-public constructor
	 */
	private ErrorCodes() {
	}
}
