/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.command;

import java.nio.file.Path;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import com.scytl.products.ov.commons.beans.Contest;
import com.scytl.products.ov.tallying.commons.beans.AuditableVote;
import com.scytl.products.ov.tallying.commons.beans.FailedVote;

/**
 * A simple wrapper object to encapsulate all required data needed to perform
 * tallying.
 */
public class TallyingHolder {

    private List<List<String>> decryptedValues;

    private Map<String, Contest> optionContestMappings;

    private List<Contest> contests;

    private Path outputDir;

    private X509Certificate signatureCert;

    private Path decryptedBallotsPath;

    private String tenantId;

    private String ballotBoxId;

    private String writeInSeparator;

    private String electionEventId;

    private PrivateKey signatureKey;

    private Path auditableVotesPath;

    private Path failedVotesPath;

    private List<AuditableVote> auditableValues;

    private List<FailedVote> failedValues;

    private String invalidVotesFileName;

    public List<List<String>> getDecryptedValues() {
        return decryptedValues;
    }

    public void setDecryptedValues(List<List<String>> decryptedValues) {
        this.decryptedValues = decryptedValues;
    }

    public Map<String, Contest> getOptionContestMappings() {
        return optionContestMappings;
    }

    public void setOptionContestMappings(Map<String, Contest> optionContestMappings) {
        this.optionContestMappings = optionContestMappings;
    }

    public Path getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(Path outputDir) {
        this.outputDir = outputDir;
    }

    public X509Certificate getSignatureCert() {
        return signatureCert;
    }

    public void setSignatureCert(X509Certificate signatureCert) {
        this.signatureCert = signatureCert;
    }

    public Path getDecryptedBallotsPath() {
        return decryptedBallotsPath;
    }

    public void setDecryptedBallotsPath(Path decryptedBallotsPath) {
        this.decryptedBallotsPath = decryptedBallotsPath;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setBallotBoxId(String ballotBoxId) {
        this.ballotBoxId = ballotBoxId;
    }

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public void setElectionEventId(String electionId) {
        this.electionEventId = electionId;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public void setSignatureKey(PrivateKey signatureKey) {
        this.signatureKey = signatureKey;
    }

    public PrivateKey getSignatureKey() {
        return signatureKey;
    }

    public void setAuditableVotesPath(Path auditableVotesPath) {
        this.auditableVotesPath = auditableVotesPath;
    }

    public Path getAuditableVotesPath() {
        return auditableVotesPath;
    }

    public void setFailedVotesPath(Path failedVotesPath) {
        this.failedVotesPath = failedVotesPath;
    }

    public Path getFailedVotesPath() {
        return failedVotesPath;
    }

    public void setAuditableValues(List<AuditableVote> auditableValues) {
        this.auditableValues = auditableValues;
    }

    public List<AuditableVote> getAuditableValues() {
        return auditableValues;
    }

    public void setFailedValues(List<FailedVote> failedValues) {
        this.failedValues = failedValues;
    }

    public List<FailedVote> getFailedValues() {
        return failedValues;
    }

    public void setInvalidVotesFileName(String invalidVotesFileName) {
        this.invalidVotesFileName = invalidVotesFileName;
    }

    public String getInvalidVotesFileName() {
        return invalidVotesFileName;
    }

    public List<Contest> getContests() {
        return contests;
    }

    public void setContests(List<Contest> contests) {
        this.contests = contests;
    }

    public String getWriteInSeparator() {
        return writeInSeparator;
    }

    public void setWriteInSeparator(String writeInSeparator) {
        this.writeInSeparator = writeInSeparator;
    }

}
