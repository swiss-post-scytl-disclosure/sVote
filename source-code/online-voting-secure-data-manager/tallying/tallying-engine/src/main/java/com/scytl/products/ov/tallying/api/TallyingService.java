/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.api;

import com.scytl.products.ov.tallying.api.dto.output.TallyingOutput;
import com.scytl.products.ov.tallying.command.TallyingHolder;
import com.scytl.products.ov.tallying.commons.exception.TallyingException;

public interface TallyingService {

	void tallyValidVotes(TallyingHolder tallyingHolder, TallyingOutput output)
			throws TallyingException;

	void tallyInvalidVotes(TallyingHolder holder, TallyingOutput tallyingOutput)
			throws TallyingException;
}
