/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.io.writer;

import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.scytl.products.ov.tallying.commons.beans.TallyingError;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A class in charge of serialize a List<Error> object  to csv.
 */
public class TallyingErrorWriter {

    private static final Logger LOG = Logger.getLogger("std");

    public void write(final List<TallyingError> errors, final Path outputFile) throws IOException {

        LOG.log(Level.INFO, "Persisting tallying errors in {0}", outputFile);

        try (final Writer out = new FileWriter(outputFile.toFile());
                final CSVWriter<TallyingError> csvWriter = new CSVWriterBuilder<TallyingError>(out).entryConverter(
                        new TallyingErrorConverter()).build()) {

            csvWriter.writeAll(errors);
            csvWriter.flush();
        }
    }
}
