/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.spring;

import com.scytl.products.ov.tallying.commons.MappingStrategy;
import com.scytl.products.ov.tallying.commons.io.reader.AuditableVoteEntryParser;
import com.scytl.products.ov.tallying.commons.io.reader.AuditableVotesReader;
import com.scytl.products.ov.tallying.commons.io.reader.BallotsReader;
import com.scytl.products.ov.tallying.commons.io.reader.DecryptedBallotEntryParser;
import com.scytl.products.ov.tallying.commons.io.reader.FailedVoteEntryParser;
import com.scytl.products.ov.tallying.commons.io.reader.FailedVotesReader;
import com.scytl.products.ov.utils.ConfigObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class CommonSpringConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public ConfigObjectMapper configObjectMapper() {
        return new ConfigObjectMapper();
    }

    @Bean
    public DecryptedBallotEntryParser decryptedBallotEntryParser() {
        return new DecryptedBallotEntryParser();
    }

    @Bean
    public BallotsReader ballotsReader(DecryptedBallotEntryParser decryptedBallotEntryParser) {
        return new BallotsReader(decryptedBallotEntryParser);
    }

    @Bean
    public AuditableVoteEntryParser auditableVoteEntryParser() {
        return new AuditableVoteEntryParser();
    }

    @Bean
    public AuditableVotesReader auditableVotesReader(AuditableVoteEntryParser auditableVoteEntryParser) {
        return new AuditableVotesReader(auditableVoteEntryParser);
    }

    @Bean
    public FailedVoteEntryParser failedVoteEntryParser() {
        return new FailedVoteEntryParser();
    }

    @Bean
    public FailedVotesReader failedVotesReader(FailedVoteEntryParser failedVoteEntryParser) {
        return new FailedVotesReader(failedVoteEntryParser);
    }

    @Bean
    public MappingStrategy mappingStrategy() {
        return new MappingStrategy();
    }
}
