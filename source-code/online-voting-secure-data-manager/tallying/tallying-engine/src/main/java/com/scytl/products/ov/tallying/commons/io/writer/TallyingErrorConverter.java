/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.io.writer;

import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.scytl.products.ov.tallying.commons.beans.TallyingError;

import java.util.ArrayList;
import java.util.List;

/**
 * A utility class to write entries on the decrypt_errors.csv file.
 */
public class TallyingErrorConverter implements CSVEntryConverter<TallyingError> {

    @Override
    public String[] convertEntry(TallyingError tallyingError) {

        List<String> values = new ArrayList<>();
        values.add(tallyingError.getCode());
        values.add(tallyingError.getMessage());

        return values.toArray(new String[values.size()]);
    }
}
