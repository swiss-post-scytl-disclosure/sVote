/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.io.reader;

import com.googlecode.jcsv.reader.CSVEntryParser;
import com.scytl.products.ov.tallying.commons.beans.FailedVote;

/*
 * A utility class used by {@code com.scytl.products.ov.tallying.commons.io.reader.FailedVotesReader}
 * to read line by line csv entries (failed vote values) and convert them to Strings.
 */
public final class FailedVoteEntryParser implements CSVEntryParser<FailedVote> {

    /**
     * @see CSVEntryParser#parseEntry(String[])
     */
    @Override
    public FailedVote parseEntry(final String... args) {

        return new FailedVote(args[0], args[2], args[3]);
    }
}
