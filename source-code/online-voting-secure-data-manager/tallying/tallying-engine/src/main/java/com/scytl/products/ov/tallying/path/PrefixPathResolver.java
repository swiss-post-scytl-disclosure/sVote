/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.path;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * {@link PathResolver} implementation that prepends a suffix to the provided
 * path
 */
public class PrefixPathResolver implements PathResolver {

    private static final Path ROOT = Paths.get(File.separator);

    private final Path prefix;

    /**
     * @param prefix
     *            The prefix should be absolute, if it doesn't start with '/', a
     *            '/' will be added
     */
    public PrefixPathResolver(final String prefix) {
        super();
        Path path = Paths.get(prefix.trim());
        if (path.isAbsolute() || path.startsWith(File.separator)) {
            this.prefix = path;
        } else {
        	this.prefix = ROOT.resolve(path);

        }
    }

    @Override
    public Path resolve(final String path) {
        return Paths.get(prefix.toString(), path.trim());
    }
}
