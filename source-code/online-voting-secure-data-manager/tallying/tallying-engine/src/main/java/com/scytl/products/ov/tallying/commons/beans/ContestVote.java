/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.beans;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ContestVote {

    private final String contestId;

    private final List<OptionData> representations;

    private BigInteger multiplication;

    private final AtomicInteger count;

    public ContestVote(final String contestId, final OptionData optionData) {
        this.contestId = contestId;
        this.representations = new ArrayList<>();
        this.representations.add(optionData);
        if(optionData.getWriteInValue()!=null){
        	this.multiplication = new BigInteger(optionData.getRepresentation()).multiply(new BigInteger(1, optionData.getWriteInValue().getBytes(StandardCharsets.UTF_8)));
        } else {
        	this.multiplication = new BigInteger(optionData.getRepresentation());
        }
        this.count = new AtomicInteger(1);
    }

    public String getContestId() {
        return contestId;
    }

    public int getCount() {

        return count.get();
    }

    public List<OptionData> getRepresentations() {

        return representations;
    }

    public BigInteger getMultiplication() {
        return multiplication;
    }

    public void add(final OptionData optionData) {
        representations.add(optionData);
        if(optionData.getWriteInValue()!=null){
            multiplication = multiplication.multiply(new BigInteger(1, optionData.getWriteInValue().getBytes(StandardCharsets.UTF_8)));
        }
        multiplication = multiplication.multiply(new BigInteger(optionData.getRepresentation()));
    }

    public void increment() {
        count.incrementAndGet();
    }
}
