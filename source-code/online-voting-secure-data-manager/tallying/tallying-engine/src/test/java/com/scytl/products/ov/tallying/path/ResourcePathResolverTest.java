/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.path;

import org.junit.Test;

import java.nio.file.Path;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ResourcePathResolverTest {

    @Test
    public void resolve_path_correctly() {
        ResourcePathResolver resolver = new ResourcePathResolver();
        Path path = resolver.resolve("log4j.xml");
        System.out.println(path);
        assertTrue(path.toFile().exists());
        assertTrue(path.endsWith("log4j.xml"));
    }

    @Test
    public void fail_when_resolve_null_path() {
        ResourcePathResolver resolver = new ResourcePathResolver();
        Path path = resolver.resolve("DoesNotExist");
        assertNull(path);
    }
}