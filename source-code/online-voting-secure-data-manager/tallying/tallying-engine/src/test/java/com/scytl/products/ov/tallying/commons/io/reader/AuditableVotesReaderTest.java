/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.io.reader;

import com.scytl.products.ov.tallying.commons.beans.AuditableVote;
import com.scytl.products.ov.tallying.commons.exception.TallyingException;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class AuditableVotesReaderTest {

    private final static String AUDITABLE_VOTES_PATH = "src/test/resources/auditableVotes.csv";

    @Test
    public void read_correctly() throws TallyingException {
        AuditableVotesReader reader = new AuditableVotesReader(new AuditableVoteEntryParser());

        List<AuditableVote> auditableVotes = reader.read(new File(AUDITABLE_VOTES_PATH).toPath(), "electionEventId", "bbid");

        assertThat(auditableVotes, notNullValue());
        assertThat(auditableVotes.size(), is(3));
        assertThat(auditableVotes.get(2).getFailures(), is("RULE_VALIDATION,NON_FACTORIZABLE_REMAINDER,DUPLICATED_FACTOR"));
        assertThat(auditableVotes.get(2).getCompressed(), is("6370"));
        assertThat(auditableVotes.get(2).getFactorization(), is("2^1 5^1 7^2 13^1"));

    }
}
