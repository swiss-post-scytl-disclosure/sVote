/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.commons.io.reader;

import com.scytl.products.ov.tallying.commons.beans.FailedVote;
import com.scytl.products.ov.tallying.commons.exception.TallyingException;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class FailedVotesReaderTest {

    private final static String FAILED_VOTES_PATH = "src/test/resources/failedVotes.csv";

    @Test
    public void read_correctly() throws TallyingException {
        FailedVotesReader reader = new FailedVotesReader(new FailedVoteEntryParser());

        List<FailedVote> failedVotes = reader.read(new File(FAILED_VOTES_PATH).toPath(), FAILED_VOTES_PATH,
                "BallotBoxId");

        assertThat(failedVotes, notNullValue());
        assertThat(failedVotes.size(), is(1));
        assertThat(failedVotes.get(0).getVotingCardId(), is("9ead024a8be94dbcbe4a530ac7b1a782"));
        assertThat(failedVotes.get(0).getStatus(), is("NOT_CONFIRMED"));
        assertThat(failedVotes.get(0).getReceipt(), is("4iZv3uAFM+NM9EeWpbxvCWwu1KmKUIPSDXKbFQNY59c="));
    }
}
