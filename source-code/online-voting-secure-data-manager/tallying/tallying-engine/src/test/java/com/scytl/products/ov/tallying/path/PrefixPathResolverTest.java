/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.tallying.path;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.nio.file.Path;

import static org.junit.Assert.assertTrue;

public class PrefixPathResolverTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void resolve_path_correctly()  {

        PrefixPathResolver resolver = new PrefixPathResolver(temporaryFolder.getRoot().getParent());
        Path path = resolver.resolve(temporaryFolder.getRoot().getName());

        assertTrue(path.endsWith(temporaryFolder.getRoot().getName()));

    }
}