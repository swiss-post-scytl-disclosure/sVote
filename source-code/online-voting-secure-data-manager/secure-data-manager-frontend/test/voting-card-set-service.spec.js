/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
describe("Voting card set service", function () {
    var votingCardSetService, httpBackend;
  
    beforeEach(module("votingCardSet"));
  
    beforeEach(inject(function (_votingCardSetService_, $httpBackend) {
      votingCardSetService = _votingCardSetService_;
      httpBackend = $httpBackend;
    }));
  
    it("should change status", function () {
      // Define a minimal voting card set.
      var votingCardSet = {
        id: "votingCard",
        electionEvent: {
          id: "electionEvent"
        },
        status: 'LOCKED'
      };
      // Attempt to chage status.
      httpBackend.whenPUT("/votingcardset/electionevent/electionEvent/votingcardset/votingCardSet").respond({
          data: {
            status: 'PRECOMPUTED'
          }
      });
      votingCardSetService.changeStatus(votingCardSet, "PRECOMPUTED").then(function(data) {
        expect(data.status).toEqual('PRECOMPUTED');
      });
      httpBackend.flush();
    });
  
  });