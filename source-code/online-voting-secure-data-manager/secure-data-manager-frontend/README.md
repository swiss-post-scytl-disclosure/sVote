Secure Data Manager Front-End
=============================

nwjs (node-webkit) desktop app acting as a frontend for OV configuration.
It works with the Secure Data Manager REST service, which you can grab from:
https://scm.scytl.net/stash/projects/PROJ/repos/20120073-ov-secure-data-manager

Setting up the local server
----------------------------

The following resources from _ov-secure-data-manager_ need to be in
your user home directory:
    
* _sdm-ws-rest.jar_
* _sdm_ folder, containing _sdmConfig_

On startup the app will automatically launch the local REST service.

development
-----------

* `grunt app`

will build the platform executables. You will find them in the _desktop_app_ folder


mock server
-----------

A mock backend server is PARTIALLY implemented. To use it you need to
edit _src/app/services/endpoints.js_ and ensure that the relative
version of the _host_ config value is used, like so:

```
 host : '/sdm-ws-rest/',
 // host: 'http://localhost:8090/sdm-ws-rest/',
```

Then just run

```
$ grunt dev
```

and point your browser to _http://localhost:3000/_


running the FE as a webapp in a regular browser
-----------------------------------------------

* run `grunt app` to build the development version.
* set up any http server pointing to the 'build' directory, and point your browser to the appropiate url (root dir is fine)
* `java -jar <userhome>/sdm-ws-rest.jar` to manually start the server


release
-------
    
* `grunt release` (or just `grunt`)

will build all platform executables and bundle them in a zip file in
the 'dist' folder.

i18n management
==================

Note that as of 19/01/2016 english is no longer the base language,
although the literal text in the code is unchanged. English ('en') is
now another language just like any other, meaning it has it's own
translation file and you can now change _translated_ english text
without disrupting the other translations. Please don't modify any
source literals.

Secure Data Manager language config
----------------------------

The _i18n_ service (in src/service) configures available languages
like so. This is the single point of language configuration.

```javascript
angular.module('i18n', [])
.constant('i18n', {

    languages: [{
        code: 'fr',
        shortname: 'FR',
        name: 'Francais'
    }, {
        code: 'de',
        shortname: 'DE',
        name: 'Deutsch'
    }, {
        code: 'en',
        shortname: 'EN',
        name: 'English'
    }],
    default: 'en'

});
```

The default language will be automatically selected on app startup.

Adding a new supported language
-------------------------------

To add a new language just add an entry to _i18n_ and make sure the
default is ok.

Then of course you need to create a new catalog (_po_ file) from the
template (_pot_ file) and add translations to it. Next run `grunt
nggettext_compile'.


Adding/modifying translatable elements
--------------------------------------

After you added/changed text to be translated, run `grunt
nggettext_extract' to update the template ( _pot_ file), then use your
favorite _poeditor_ to add/edit translations to the _po_ files, and
finally run `grunt nggettext_compile' to convert the _po_ files into
the JSON files in 'assets/lang' used by the app.