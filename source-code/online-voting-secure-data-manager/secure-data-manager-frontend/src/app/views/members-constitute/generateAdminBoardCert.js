/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('generateAdminBoardCert', [
        // 'conf.globals'
    ])
    .factory('generateAdminBoardCert',function(Upload){

        'use strict';
        var upload =function(url, data) {

            Upload.upload({
                url: url,
                data: {
                    file: data.file,
                    keystorePassword: data.keystorePassword
                }
            }).then(data.callbackOK,data.callbackKO);
        };
        return{
          upload:upload
        };

    });



