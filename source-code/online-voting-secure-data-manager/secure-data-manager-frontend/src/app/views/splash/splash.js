/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
(function() {

    'use strict';

    angular.module('splash', [
        'ui.router', 'endpoints'
    ])
    .controller('splash', function($scope, $rootScope, endpoints, $http, $state, $interval, sessionService) {

        // admin boards initialization
        var listAdminBoards = function() {
            $http.get(endpoints.host + endpoints.administrationBoards).
            success(function(data) {
                try {
                    sessionService.setAdminBoards(data.result);
                } catch (e) {
                    console.log(e);
                }

            }).
            error(function() {
                $scope.errors.administrationBoardsFailed = true;
            });
        };

        var delay = 2000;
        var poll = $interval(function() {

            $http.get(endpoints.host + endpoints.status)
                .success(function(data) {
                    if (data.status === 'OPEN') {
                        $interval.cancel(poll);
                        listAdminBoards();
                        $rootScope.getConfig();
                    }
                });

        }, delay);
    });

})();
