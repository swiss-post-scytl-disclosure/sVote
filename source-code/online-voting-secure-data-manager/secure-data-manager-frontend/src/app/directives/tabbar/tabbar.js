/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('tabBar', [])

.directive('tabBar', function(sessionService) {

  'use strict';

  return {
    restrict: 'E',
    transclude: true,
    templateUrl: 'app/directives/tabbar/tabbar.html',
    scope: {
      filterTabs: '=',
      filterActive: '=',
      onTabSelected: '=',
      filterCounter: '=',
    },
    link: function(scope) {

      // Init
      scope.selectedTab = scope.filterActive;

      scope.filterBy = function(filter) {
        scope.onTabSelected(filter);
      };

    },

  };

});
