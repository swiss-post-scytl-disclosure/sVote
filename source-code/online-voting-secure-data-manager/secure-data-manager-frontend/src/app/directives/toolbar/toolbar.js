/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('ovToolbar', [])

    .directive('ovToolbar', function($mdSidenav, $state, ToolbarTitle, sessionService, gettext) {
        'use strict';
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'app/directives/toolbar/toolbar.html',

            link: function(scope) {

                var toolbarTitle = new ToolbarTitle(sessionService);
                scope.getMainTitle = function() {
                    switch($state.current.name) {
                    case 'election-event-list':
                        return gettext('Election Events');
                    case 'administration-board-list':
                        return gettext('Administration Boards');
                    	default:
                        return sessionService.getSelectedElectionEvent().defaultTitle;
                    }
                };
								
                scope.getElectionEventSection = function() {
                    switch($state.current.name) {
                    case 'ballots':
                        return gettext('Ballots');
                    case 'voting-cards':
                        return gettext('Voting Card Sets');
                    case 'electoral-authorities':
                        return gettext('Electoral Authorities');
                    case 'ballot-boxes':
                        return gettext('Ballot Boxes');
                    default:
                        return gettext('Ballots');
                    }
                };
								
                scope.enableElectionEventSection = function () {
                    return (sessionService.getSelectedElectionEvent() && $state.current.name !== 'administration-board-list' && $state.current.name !== 'election-event-list');
                };

                scope.showImport = function () {
                    return $state.current.name == 'election-event-list' && sessionService.isImportExportEnabled();
                };

                scope.toggleLeftMenu = function() {
                    var target = angular.element(document.getElementById('main-wrapper'));
                    target.toggleClass('sidebar-is-closed');
                };

            }

        };

    })
    .factory('ToolbarTitle', function() {
        'use strict';

        return function(sessionService) {

            this.title = function() {
                return sessionService.getSelectedElectionEvent().alias;
            };

            function _has(obj) {
                return angular.isDefined(obj) && obj !== null;
            }

            this.has = function() {
                return _has(sessionService.getSelectedElectionEvent());
            };
        };
    })

;
