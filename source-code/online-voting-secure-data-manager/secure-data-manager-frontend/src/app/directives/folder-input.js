/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('ovFolderInputDirective', [])

.directive('ovFolderInput', function($parse) {
	'use strict';
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
            element.bind('click', function() {
                angular.element(document.querySelector('#' + attrs.ovFolderInput))[0].click();
            });
		}
	};
})
.directive('ovFolderInputModel', function($parse) {
    'use strict';
    return {
        restrict: 'A',
        scope: {CtrlFn: '&callback'},
        link: function(scope, element, attrs) {       

            function extractPath(e) {
                var path = e.path[0].files[0].path || '';
                scope.CtrlFn({path: path});
                element.val('');
            }

            element.on('change', extractPath);
        }
    };
})