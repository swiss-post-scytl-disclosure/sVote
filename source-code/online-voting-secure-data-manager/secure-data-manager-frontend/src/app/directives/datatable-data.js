/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('datatableData', [])

.directive('datatableData', function() {
	'use strict';
	return {
		restrict: 'C',
		link: function(scope, element) {
            var target = element[0].getElementsByTagName('thead');
            element[0].onscroll = function() {
                target[0].style.transform = 'translateY(' + element[0].scrollTop +'px)';
                if (element[0].scrollTop > 0) {
                    angular.element(target[0]).addClass('has-shadow');
                } else {
                    angular.element(target[0]).removeClass('has-shadow');
                }
            }
		}
	};
})