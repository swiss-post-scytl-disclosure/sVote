/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('app.sessionService', [
    // 'conf.globals'
])

.factory('sessionService', function($timeout) {

    'use strict';

    // key expiration config
    var keyExpiration = {
        ab: {
            ms: 60 * 60 * 1000,
            timer: null
        },
        eb: {
            ms: 60 * 60 * 1000,
            timer: null
        }
    };


    var setKeyExpiration = function(type, value) {

        var minutes = Number.parseInt(value, 10);
        if ( !isNaN(minutes) ) {
            keyExpiration[type].ms = minutes * 60 * 1000;
            console.log('Setting (' + type + ') expiration to' + minutes + ' minutes');
        } else {
            console.log('** ERROR **  Wrong argument for expiration (' + type + '):' + value);
        }

    };

    var expireAfter = function(type, board) {

        // reset any previous expiration

        if (keyExpiration[type].timer) {
            // console.log('reset[' + type + ']', board);
            $timeout.cancel(keyExpiration[type].timer);
            keyExpiration[type].timer = null;
        }

        // enable expiration for this key

        if (board && board.privateKey) {

            keyExpiration[type].timer = $timeout(function() {

                // console.log('timeout[' + type + ']', board);
                if (board) {
                    // console.log('clearing[' + type + ']', board.privateKey);
                    board.privateKey = null;
                // } else {
                    // console.log('nothing to clear for [' + type + ']');
                }
                keyExpiration[type].timer = null;

            }, keyExpiration[type].ms);

        }

    };


    this.userJWTToken = null;
    this.authenticated = false;
    this.status = ''; // partial|voted
    this.membersPoll = {};
    this.tallyingEnabled = false;

    var setElectionEvents = function(electionEvents) {
        this.electionEvents = electionEvents;
    };

    var getElectionEvents = function() {
        return this.electionEvents;
    };

    var setAdminBoards = function(adminBoards) {
        this.adminBoards = adminBoards;
    };

    var getAdminBoards = function() {
        return this.adminBoards;
    };

    var setElectoralAuthorities = function(electoralAuthorities) {
        this.electoralAuthorities = electoralAuthorities;
    };

    var getElectoralAuthorities = function() {
        return this.electoralAuthorities;
    };

    var setSelectedElectionEvent = function(selectedElectionEvent) {
        this.selectedElectionEvent = selectedElectionEvent;
    };

    var getSelectedElectionEvent = function() {
        return this.selectedElectionEvent;
    };

    var clearSelectedElectionEvent = function() {
        this.selectedElectionEvent = undefined;
    };

    var setBallots = function(ballots) {
        this.ballots = ballots;
    };

    var getBallots = function() {
        return this.ballots;
    };
    var setVotingCardSets = function(votingCardSets) {
        this.votingCardSets = votingCardSets;
    };

    var getVotingCardSets = function() {
        return this.votingCardSets;
    };

    var setSelectedVotingCardSet = function(selectedVotingCardSet) {
        this.selectedVotingCardSet = selectedVotingCardSet;
    };

    var getSelectedVotingCardSet = function() {
        return this.selectedVotingCardSet;
    };

    var setSelectedElectoralAuthority = function(electoralAuthority) {
        this.selectedElectoralAuthority = electoralAuthority;
    };

    var getSelectedElectoralAuthority = function() {
        return this.selectedElectoralAuthority;
    };

    var setBallotBoxes = function(ballotBoxes) {
        this.ballotBoxes = ballotBoxes;
    };

    var getBallotBoxes = function() {
        return this.ballotBoxes;
    };

    var setSelectedAdminBoard = function(selectedAdminBoard) {
        this.selectedAdminBoard = selectedAdminBoard;
    };


    var getSelectedAdminBoard = function() {
        return this.selectedAdminBoard;
    };

    var setNumberOfSuccessfullyWrittenSmartCards = function(successfullyWrittenSmartCards) {
        this.successfullyWrittenSmartCards = successfullyWrittenSmartCards;
    };

    var getNumberOfSuccessfullyWrittenSmartCards = function() {
        return this.successfullyWrittenSmartCards;
    };

    var setMembersPoll = function(poll) {
        this.membersPoll = poll;
    };

    var getMembersPoll = function() {
        return this.membersPoll;
    };

    var syncronizing = 0;

    var startSync = function() {
        syncronizing++;
    };

    var stopSync = function() {
        syncronizing--;
    };

    var isSync = function() {
        return syncronizing > 0;
    };

    var isGeneratePreVotingOutputsEnabled = function() {
        return this.generatePreVotingOutputsEnabled;
    };
    var setGeneratePreVotingOutputsEnabled = function(bool) {
        this.generatePreVotingOutputsEnabled = bool;
    };

    var isGeneratePostVotingOutputsEnabled = function() {
        return this.generatePostVotingOutputsEnabled;
    };
    var setGeneratePostVotingOutputsEnabled = function(bool) {
        this.generatePostVotingOutputsEnabled = bool;
    };

    var isVcGenerationPreparationEnabled = function() {
        return this.vcGenerationPreparationEnabled;
    };
    var setVcGenerationPreparationEnabled = function(bool) {
        this.vcGenerationPreparationEnabled = bool;
    };

    var isVcPrecomputationEnabled = function() {
        return this.vcPrecomputationEnabled;
    };
    var setVcPrecomputationEnabled = function(bool) {
        this.vcPrecomputationEnabled = bool;
    };

    var isVcComputationEnabled = function() {
        return this.vcComputationEnabled;
    };
    var setVcComputationEnabled = function(bool) {
        this.vcComputationEnabled = bool;
    };

    var isVcDownloadEnabled = function() {
        return this.vcDownloadEnabled;
    };
    var setVcDownloadEnabled = function(bool) {
        this.vcDownloadEnabled = bool;
    };

    var isDownloadParticipationEnabled = function() {
        return this.downloadParticipationEnabled;
    };
     var setDownloadParticipationEnabled = function(bool) {
        this.downloadParticipationEnabled = bool;
    };

    var isImportExportEnabled = function() {
        return this.importExportEnabled;
    };
    var setImportExportEnabled = function(bool) {
        this.importExportEnabled = bool;
    };

    var isTallyingEnabled = function() {
        return this.tallyingEnabled;
    };
    var setTallyingEnabled = function(bool) {
        this.tallyingEnabled = bool;
    };

    var doesActivatedABBelongToSelectedEE = function () {
        var electionEvent = this.getSelectedElectionEvent();
        var adminBoard = this.getSelectedAdminBoard();

        if(electionEvent && adminBoard){
            var eEventAdminBoard = electionEvent.administrationAuthority.id;
            var activatedAdminBoard = adminBoard.id;
            if(eEventAdminBoard === activatedAdminBoard){
                return true;
            }else{
                return false;
            }
        }
    };

    return {
        getElectionEvents: getElectionEvents,
        setElectionEvents: setElectionEvents,
        setAdminBoards: setAdminBoards,
        getAdminBoards: getAdminBoards,
        setElectoralAuthorities: setElectoralAuthorities,
        getElectoralAuthorities: getElectoralAuthorities,
        setSelectedAdminBoard: setSelectedAdminBoard,
        getSelectedAdminBoard: getSelectedAdminBoard,
        setSelectedElectoralAuthority: setSelectedElectoralAuthority,
        getSelectedElectoralAuthority: getSelectedElectoralAuthority,
        getSelectedElectionEvent: getSelectedElectionEvent,
        setSelectedElectionEvent: setSelectedElectionEvent,
        clearSelectedElectionEvent: clearSelectedElectionEvent,
        getBallots: getBallots,
        setBallots: setBallots,
        getVotingCardSets: getVotingCardSets,
        setVotingCardSets: setVotingCardSets,
        setSelectedVotingCardSet: setSelectedVotingCardSet,
        getSelectedVotingCardSet: getSelectedVotingCardSet,
        setBallotBoxes: setBallotBoxes,
        getBallotBoxes: getBallotBoxes,
        setNumberOfSuccessfullyWrittenSmartCards: setNumberOfSuccessfullyWrittenSmartCards,
        getNumberOfSuccessfullyWrittenSmartCards: getNumberOfSuccessfullyWrittenSmartCards,
        setMembersPoll: setMembersPoll,
        getMembersPoll: getMembersPoll,
        startSync: startSync,
        stopSync: stopSync,
        isSync: isSync,
        expireAfter: expireAfter,
        setKeyExpiration: setKeyExpiration,
        isGeneratePreVotingOutputsEnabled: isGeneratePreVotingOutputsEnabled,
        setGeneratePreVotingOutputsEnabled: setGeneratePreVotingOutputsEnabled,
        isGeneratePostVotingOutputsEnabled: isGeneratePostVotingOutputsEnabled,
        setGeneratePostVotingOutputsEnabled: setGeneratePostVotingOutputsEnabled,
        isVcGenerationPreparationEnabled: isVcGenerationPreparationEnabled,
        setVcGenerationPreparationEnabled: setVcGenerationPreparationEnabled,
        isVcPrecomputationEnabled: isVcPrecomputationEnabled,
        setVcPrecomputationEnabled: setVcPrecomputationEnabled,
        isVcComputationEnabled: isVcComputationEnabled,
        setVcComputationEnabled: setVcComputationEnabled,
        isVcDownloadEnabled: isVcDownloadEnabled,
        setVcDownloadEnabled: setVcDownloadEnabled,
        isDownloadParticipationEnabled: isDownloadParticipationEnabled,
        setDownloadParticipationEnabled: setDownloadParticipationEnabled,
        isImportExportEnabled: isImportExportEnabled,
        setImportExportEnabled: setImportExportEnabled,
        isTallyingEnabled: isTallyingEnabled,
        setTallyingEnabled: setTallyingEnabled,
        doesActivatedABBelongToSelectedEE: doesActivatedABBelongToSelectedEE
    };
});
