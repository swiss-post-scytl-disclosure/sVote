/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
(function() {
    'use strict';

    angular.module('app.errors.dict', [])
        .factory('ErrorsDict', function(gettextCatalog) {
            var errors = {
                99: 'Unexpected error, please check the Integration tools logs',
                100: 'General error, please check the Integration tools logs',
                101: 'Invalid action, please check the Integration tools logs',
                102: 'Missing parameter, please check the Integration tools logs',
                103: 'Parameter not valid, please check the Integration tools logs',
                104: 'File not found, please check the Integration tools logs',
                105: 'Error parsing file, please check the Integration tools logs',
                106: 'Content not valid, please check the Integration tools logs',
                107: 'Usb not found',
                108: 'Error copying files, please check the Integration tools logs',
                109: 'Election Event already in the usb, please check the Integration tools logs',
                4000: 'Secure Data Manager error, please check the logs',
                4001: 'Integration issue between components',
                4002: 'Import/Export problems reading files or some files are missing',
                4003: 'Error parsing plugin.xml',
                4004: 'Integration issue between components',
                4005: 'No action has been performed. Please review the plugin.xml configuration.'
            };

            return function(number) {
                return errors[number];
            };
        });
})();