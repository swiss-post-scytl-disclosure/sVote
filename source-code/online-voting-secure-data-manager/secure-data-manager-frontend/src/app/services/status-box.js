/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('statusBox', [
    // 'conf.globals'
])

.factory('statusBox', function() {
    'use strict';

    var status = {
        // false == is NOT collapsed
        // true == is collapsed
        bb: false,
        vcs: false
    }; 

    function toggleStatusBox(key) {
        status[key] = !status[key];
        return status[key];
    }

    function getStatusBox(key) {
        return status[key];
    }

    return {
        toggleStatusBox: toggleStatusBox,
        getStatusBox: getStatusBox
    };
});
