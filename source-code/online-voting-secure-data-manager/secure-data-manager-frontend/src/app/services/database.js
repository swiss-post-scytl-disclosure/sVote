/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('databaseService', [
    // 'conf.globals'
])

.factory('databaseService', function($http, endpoints, $interval) {

    'use strict';

    var close = function(callback) {
        $http.post(endpoints.host + endpoints.close).success(function() {
            var delay = 2000;
            var poll = $interval(function() {
                $http.get(endpoints.host + endpoints.status).success(function(data) {
                        console.log('Status: ' + data.status);
                        if (data.status === 'CLOSED') {
                            $interval.cancel(poll);
                            callback();
                        }
                    });
                }, delay);
        });
    };

    return {
        close: close
    };
});
