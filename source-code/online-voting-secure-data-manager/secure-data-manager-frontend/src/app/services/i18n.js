/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('i18n', ['gettext'])
    .constant('i18n', {
    })
    .factory('statuses', function(gettext) {

        'use strict';

        var statuses = [
            gettext('New'),
            gettext('Locked'),
            gettext('Constituted'),
            gettext('Ready'),
            gettext('Approved'),
            gettext('Signed'),
            gettext('Generated'),
            gettext('Closed'),
            gettext('Downloaded'),
            gettext('Ready To Mix'),
            gettext('Mixed'),
            gettext('Decrypted'),
            gettext('Counted'),
            gettext('Synchronized'),
            gettext('Error synchronizing'),
            gettext('Pending to synchronize')
        ];

        return {
            statuses: statuses
        };
    });
