/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('toastCustom', [
    // 'conf.globals'
])

.factory('toastCustom', function($mdToast, gettextCatalog) {
    'use strict';

    // customization for showing the simple toast in the top center & adding an OK for closing it
    var topCenter = function(text, type) {
        var toastClass = 'toast-cutom md-top';
        switch(type) {
            case 'success':
                var closeToast = function(event) {
                    $mdToast.hide();
                    document.removeEventListener('click', closeToast, true);
                };
                document.addEventListener('click', closeToast, true);
                toastClass += ' toast-success';
                break;
            case 'error':
                toastClass += ' toast-error';
                break;
        }
        return $mdToast
            .simple()
            .content(text)
            .toastClass(toastClass)
            .action(gettextCatalog.getString('Close'))
            .highlightAction(true)
            .highlightClass('toast-btn-close')
            .hideDelay(false)
            .position('top start');
    };

    return {
        topCenter: topCenter
    };
});





