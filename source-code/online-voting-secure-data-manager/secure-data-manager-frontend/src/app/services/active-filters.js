/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
(function() {

    'use strict';

    angular.module('activeFilters', [])

    .factory('activeFilters', function(_) {

        // Init filters
        var activeFilters = {};

        var setActiveFilter = function(target, value) {
            activeFilters[target] = value;
        };

        var getActiveFilter = function(target) {
            return activeFilters[target];
        };

        return {
            getActiveFilter: getActiveFilter,
            setActiveFilter: setActiveFilter,
        };

    });

})();
