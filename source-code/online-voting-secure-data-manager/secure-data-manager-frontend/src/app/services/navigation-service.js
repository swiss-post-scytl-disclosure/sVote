/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('entitiesCounterService', [
  // 'conf.globals'
])

.factory('entitiesCounterService', function($http, endpoints, sessionService) {

  'use strict';

  var model = {};

  // init
  model.allowElectionEventNav = false;

  var setElectionEventNav = function(value) {
    model.allowElectionEventNav = value;
  }

  var getTheMainItemsCount = function() {

    $http({
      method: 'GET',
      url: endpoints.host + endpoints.electionEvents,
    }).then(function successCallback(response) {
      model.electionEvents = response.data.result.length;
    }, function errorCallback(response) {
      model.electionEvents = '?';
    });

    $http({
      method: 'GET',
      url: endpoints.host + endpoints.administrationBoards,
    }).then(function successCallback(response) {
      model.adminBoards = response.data.result.length;
    }, function errorCallback(data) {
      model.adminBoards = '?';
    });

  };

  var getEEItemsStuff = function(electionEvent) {

    model.selectedElectionEvent = electionEvent.defaultTitle;

    $http({
      method: 'GET',
      url: endpoints.host + endpoints.ballots.replace('{electionEventId}', electionEvent.id),
    }).then(function successCallback(response) {
      model.ballots = response.data.result.length;
    }, function errorCallback(response) {
      model.ballots = '?';
    });

    $http({
      method: 'GET',
      url: endpoints.host + endpoints.votingCardSets.replace('{electionEventId}', electionEvent.id),
    }).then(function successCallback(response) {
      model.votingCards = response.data.result.length;
    }, function errorCallback(response) {
      model.votingCards = '?';
    });

    $http({
      method: 'GET',
      url: endpoints.host + endpoints.electoralAuthorities.replace('{electionEventId}', electionEvent.id),
    }).then(function successCallback(response) {
      model.electoralAuthorities = response.data.result.length;
    }, function errorCallback(response) {
      model.electoralAuthorities = '?';
    });

    $http({
      method: 'GET',
      url: endpoints.host + endpoints.ballotboxes.replace('{electionEventId}', electionEvent.id),
    }).then(function successCallback(response) {
      model.ballotBoxes = response.data.result.length;
    }, function errorCallback(response) {
      model.ballotBoxes = '?';
    });

  };

  var resetEEItemsStuff = function() {
    model.selectedElectionEvent = undefined;
    model.ballots = '-';
    model.votingCards = '-';
    model.electoralAuthorities = '-';
    model.ballotBoxes = '-';
  };

  return {
    model,
    setElectionEventNav,
    getTheMainItemsCount,
    getEEItemsStuff,
    resetEEItemsStuff,
  };

});
