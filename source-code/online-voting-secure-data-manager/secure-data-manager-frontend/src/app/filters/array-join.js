/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
angular.module('filters', [])


// formats an array as a 'delimiter' separated string

.filter('join', function() {

    'use strict';

    return function(input, delimiter) {
        if (input && input.join) {
            return input.join(delimiter || ', ');
        } else {
            return '';
        }
    };

})

.filter('truncate', function () {

    'use strict';

    return function (text, length, end) {
        if (isNaN(length)) {
            length = 10;
        }
        if (end === undefined) {
            end = '...';
        }
        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length - end.length) + end;
        }

    };

 })


.filter('sparse', function () {

    'use strict';

    return function (text) {

        return text ? text.replace(/,/g, ', ') : text;

    };

 });

