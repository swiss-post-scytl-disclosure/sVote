/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch;

import org.springframework.batch.item.ExecutionContext;

import com.scytl.products.ov.config.commons.Constants;

public class VotingCardGenerationJobExecutionContext {

    private final ExecutionContext executionContext;

    public VotingCardGenerationJobExecutionContext(final ExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    public String getTenantId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_TENANT_ID);
    }

    public void setTenantId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_TENANT_ID, value);
    }

    public String getElectionEventId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_ELECTIONEVENT_ID);
    }

    public void setElectionEventId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_ELECTIONEVENT_ID, value);
    }

    public String getJobInstanceId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_JOB_INSTANCE_ID);
    }

    public void setJobInstanceId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_JOB_INSTANCE_ID, value);
    }

    public String getBallotBoxId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_BALLOTBOX_ID);
    }

    public void setBallotBoxId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_BALLOTBOX_ID, value);
    }

    public String getBallotId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_BALLOT_ID);
    }

    public void setBallotId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_BALLOT_ID, value);
    }

    public void setBasePath(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_BASE_PATH, value);
    }

    public String getVotingCardSetId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_VOTINGCARDSET_ID);
    }

    public void setVotingCardSetId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_VOTINGCARDSET_ID, value);
    }

    public String getElectoralAuthorityId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_ELECTORALAUTHORITY_ID);
    }

    public void setElectoralAuthorityId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_ELECTORALAUTHORITY_ID, value);
    }

    public void setValidityPeriodStart(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_VALIDITY_PERIOD_START, value);
    }

    public void setValidityPeriodEnd(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_VALIDITY_PERIOD_END, value);
    }

    public String getKeyForProtectingKeystorePassword() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_KEY_FOR_PROTECTING_KEYSTORE_PW);
    }

    public void setKeyForProtectingKeystorePassword(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_KEY_FOR_PROTECTING_KEYSTORE_PW, value);
    }

    public void setNumberOfVotingCards(final int value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_NUMBER_VOTING_CARDS, value);
    }

    public int getNumberOfVotingCards() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_NUMBER_VOTING_CARDS);
    }

    private <R extends Object> R getExecutionContextValue(String key) {
        return (R) executionContext.get(key);
    }

    private void setExecutionContextValue(final String key, final String value) {
        executionContext.putString(key, value);
    }

    private void setExecutionContextValue(final String key, final int value) {
        executionContext.putInt(key, value);
    }   

    public String getVerificationCardSetId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_VERIFICATIONCARDSET_ID);
    }

    public void setVerificationCardSetId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_VERIFICATIONCARDSET_ID, value);
    }

    public int getGeneratedCardCount() {
        return getExecutionContextValue(Constants.JOB_OUT_PARAM_GENERATED_VC_COUNT);
    }

    public void setGeneratedCardCount(int value) {
        setExecutionContextValue(Constants.JOB_OUT_PARAM_GENERATED_VC_COUNT, value);
    }

    public int getErrorCount() {
        return getExecutionContextValue(Constants.JOB_OUT_PARAM_ERROR_COUNT);
    }

    public void setErrorCount(final int value) {
        setExecutionContextValue(Constants.JOB_OUT_PARAM_ERROR_COUNT, value);
    }

    public void setSaltCredentialId(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_SALT_CREDENTIAL_ID, value);
    }

    public void setSaltPIN(final String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_SALT_PIN, value);
    }

    public String getSaltCredentialId() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_SALT_CREDENTIAL_ID);
    }

    public String getSaltPin() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_SALT_PIN);
    }

    public String getVotingCardSetName() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_VOTING_CARD_SET_NAME);
    }

    public void setVotingCardSetName(String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_VOTING_CARD_SET_NAME, value);
    }

    public String getChoiceCodesEncryptionKey() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_CHOICE_CODES_ENCRYPTION_KEY);
    }

    public void setChoiceCodesEncryptionKey(String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_CHOICE_CODES_ENCRYPTION_KEY, value);
    }
    
    public String getPrimeEncryptionPrivateKeyJson() {
        return getExecutionContextValue(Constants.JOB_IN_PARAM_PRIME_ENCRYPTION_PRIVATE_KEY_JSON);
    }

    public void setPrimeEncryptionPrivateKeyJson(String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_PRIME_ENCRYPTION_PRIVATE_KEY_JSON, value);
    }

    public void setPlatformRootCACertificate(String value) {
        setExecutionContextValue(Constants.JOB_IN_PARAM_PLATFORM_ROOT_CA_CERTIFICATE, value);
    }
    
}
