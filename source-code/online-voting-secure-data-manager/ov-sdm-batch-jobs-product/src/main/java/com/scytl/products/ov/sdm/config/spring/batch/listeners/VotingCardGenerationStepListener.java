/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import java.util.Queue;

import com.scytl.products.ov.sdm.config.spring.batch.GeneratedVotingCardOutput;

public class VotingCardGenerationStepListener implements StepExecutionListener {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private final Queue<GeneratedVotingCardOutput> outputQueue;

    public VotingCardGenerationStepListener(final Queue<GeneratedVotingCardOutput> outputQueue) {
        this.outputQueue = outputQueue;
    }

    @Override
    public void beforeStep(final StepExecution stepExecution) {
        //nothing to do.
    }

    @Override
    public ExitStatus afterStep(final StepExecution stepExecution) {
        LOG.debug("Voting card generation step has terminated. Inject 'poison pill' to signal end of work.");
        outputQueue.add(GeneratedVotingCardOutput.poisonPill());
        return ExitStatus.COMPLETED;
    }

}
