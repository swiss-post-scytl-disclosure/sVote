/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.config;

import com.scytl.products.ov.commons.verify.CryptolibPayloadSigningCertificateValidator;
import com.scytl.products.ov.commons.verify.PayloadSigningCertificateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.products.ov.commons.verify.CryptolibPayloadVerifier;
import com.scytl.products.ov.commons.verify.PayloadVerifier;

@Configuration
public class PayloadVerifierConfig {
   
    @Bean
    public PayloadSigningCertificateValidator certificateValidator() {
        return new CryptolibPayloadSigningCertificateValidator();
    }
    
    @Bean
    public PayloadVerifier payloadVerifier(AsymmetricServiceAPI asymmetricServiceAPI,
            PayloadSigningCertificateValidator certificateValidator) {
        return new CryptolibPayloadVerifier(asymmetricServiceAPI, certificateValidator);
    }

}


