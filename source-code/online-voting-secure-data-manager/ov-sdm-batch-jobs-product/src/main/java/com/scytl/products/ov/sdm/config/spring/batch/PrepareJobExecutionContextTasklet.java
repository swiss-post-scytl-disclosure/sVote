/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.scytl.products.ov.config.commons.Constants;

class PrepareJobExecutionContextTasklet implements Tasklet {

    private static final Logger LOGGER = LoggerFactory.getLogger("std");

    @Override
    public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext) throws Exception {

        try {
            final Map<String, Object> jobParameters = chunkContext.getStepContext().getJobParameters();
            final VotingCardGenerationJobExecutionContext jobExecutionContext =
                new VotingCardGenerationJobExecutionContext(
                    chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext());

            final String jobInstanceId = (String) jobParameters.get(Constants.JOB_IN_PARAM_JOB_INSTANCE_ID);
            jobExecutionContext.setJobInstanceId(jobInstanceId);
            final String tenantId = (String) jobParameters.get(Constants.JOB_IN_PARAM_TENANT_ID);
            jobExecutionContext.setTenantId(tenantId);
            final String electionEventId = (String) jobParameters.get(Constants.JOB_IN_PARAM_ELECTIONEVENT_ID);
            jobExecutionContext.setElectionEventId(electionEventId);
            final String ballotBoxId = (String) jobParameters.get(Constants.JOB_IN_PARAM_BALLOTBOX_ID);
            jobExecutionContext.setBallotBoxId(ballotBoxId);
            final String ballotId = (String) jobParameters.get(Constants.JOB_IN_PARAM_BALLOT_ID);
            jobExecutionContext.setBallotId(ballotId);
            final String basePath = (String) jobParameters.get(Constants.JOB_IN_PARAM_BASE_PATH);
            jobExecutionContext.setBasePath(basePath);
            final String votingCardSetId = (String) jobParameters.get(Constants.JOB_IN_PARAM_VOTINGCARDSET_ID);
            jobExecutionContext.setVotingCardSetId(votingCardSetId);
            final String electoralAuthorityId =
                (String) jobParameters.get(Constants.JOB_IN_PARAM_ELECTORALAUTHORITY_ID);
            jobExecutionContext.setElectoralAuthorityId(electoralAuthorityId);

            final int numberOfVotingCards =
                Integer.parseInt((String) jobParameters.get(Constants.JOB_IN_PARAM_NUMBER_VOTING_CARDS));
            jobExecutionContext.setNumberOfVotingCards(numberOfVotingCards);

            final String votingCardSetName = (String) jobParameters.get(Constants.JOB_IN_PARAM_VOTING_CARD_SET_NAME);
            jobExecutionContext.setVotingCardSetName(votingCardSetName);

            final String startValidityPeriod = (String) jobParameters.get(Constants.JOB_IN_PARAM_VALIDITY_PERIOD_START);
            final String endValidityPeriod = (String) jobParameters.get(Constants.JOB_IN_PARAM_VALIDITY_PERIOD_START);

            jobExecutionContext.setValidityPeriodStart(startValidityPeriod);
            jobExecutionContext.setValidityPeriodEnd(endValidityPeriod);

            final String keyForProtectingKeystorePassword =
                (String) jobParameters.get(Constants.JOB_IN_PARAM_KEY_FOR_PROTECTING_KEYSTORE_PW);
            jobExecutionContext.setKeyForProtectingKeystorePassword(keyForProtectingKeystorePassword);

            final String verificationCardSetId =
                (String) jobParameters.get(Constants.JOB_IN_PARAM_VERIFICATIONCARDSET_ID);
            jobExecutionContext.setVerificationCardSetId(verificationCardSetId);

            final String choiceCodesEncryptionKey = (String) jobParameters.get(Constants.JOB_IN_PARAM_CHOICE_CODES_ENCRYPTION_KEY);
            jobExecutionContext.setChoiceCodesEncryptionKey(choiceCodesEncryptionKey);
            
            final String primeEncryptionPrivateKeyJson = (String) jobParameters.get(Constants.JOB_IN_PARAM_PRIME_ENCRYPTION_PRIVATE_KEY_JSON);
            jobExecutionContext.setPrimeEncryptionPrivateKeyJson(primeEncryptionPrivateKeyJson);
            
            final String platformRootCACertificate = (String) jobParameters.get(Constants.JOB_IN_PARAM_PLATFORM_ROOT_CA_CERTIFICATE);
            jobExecutionContext.setPlatformRootCACertificate(platformRootCACertificate);

        } catch (Exception e) {
            LOGGER.error("Prepare job execution task failed.", e);
            throw e;
        }

        return RepeatStatus.FINISHED;
    }
}
