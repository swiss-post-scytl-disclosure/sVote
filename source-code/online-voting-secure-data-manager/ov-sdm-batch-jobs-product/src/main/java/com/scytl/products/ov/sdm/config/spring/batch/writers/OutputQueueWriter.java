/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch.writers;

import org.springframework.batch.item.ItemWriter;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import com.scytl.products.ov.sdm.config.spring.batch.GeneratedVotingCardOutput;

public class OutputQueueWriter implements ItemWriter<GeneratedVotingCardOutput> {

    private final BlockingQueue<GeneratedVotingCardOutput> queue;

    public OutputQueueWriter(final BlockingQueue<GeneratedVotingCardOutput> queue) {
        this.queue = queue;
    }

    @Override
    public void write(final List<? extends GeneratedVotingCardOutput> items) throws Exception {
        for(GeneratedVotingCardOutput item : items) {
            queue.put(item);
        }
    }
}
