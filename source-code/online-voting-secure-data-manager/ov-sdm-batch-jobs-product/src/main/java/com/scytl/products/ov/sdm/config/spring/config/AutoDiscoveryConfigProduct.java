/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.config;

import org.springframework.batch.core.configuration.support.ApplicationContextFactory;
import org.springframework.batch.core.configuration.support.GenericApplicationContextFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.products.ov.sdm.config.spring.batch.ConfigJobConfigProduct;

@Configuration
public class AutoDiscoveryConfigProduct {

    @Bean
    public ApplicationContextFactory configContextProduct() {
        return new GenericApplicationContextFactory(ConfigJobConfigProduct.class);
    }
}
