/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch.readers;

import java.util.Queue;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.scytl.products.ov.datapacks.beans.VerificationCardIdComputedValues;

public class ComputedValuesReader implements ItemReader<VerificationCardIdComputedValues> {
    
    private Queue<VerificationCardIdComputedValues> computedValuesQueue;

    public ComputedValuesReader(Queue<VerificationCardIdComputedValues> computedValuesQueue) {
        this.computedValuesQueue = computedValuesQueue;
    }

    @Override
    public VerificationCardIdComputedValues read()
            throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        return computedValuesQueue.poll();
    }

}