/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch.writers;


import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.springframework.batch.item.ExecutionContext;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.scytl.products.ov.sdm.config.spring.batch.GeneratedVotingCardOutput;

public class VoterInformationWriterTest {


    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    //this is to document what the exact format we are expecting the writer writes
    //votingCardId, ballotId, ballotBoxId, credentialId,electionEventId, votingCardSetId, verificationCardId, verificationCardSetId

    private String votingCardId = "votingCardId";
    private String ballotId = "ballotId";
    private String ballotBoxId = "ballotBoxId";
    private String credentialId = "credentialId";
    private String electionEventId = "electionEventId";
    private String votingCardSetId = "votingCardSetId";
    private String verificationCardId = "verificationCardId";
    private String verificationCardSetId = "verificationCardSetId";
    private int expectedFormatLength = "%s,%s,%s,%s,%s,%s,%s,%s".split(",").length;

    @Test
    public void generateOutputFileWithCorrectDataFormat() throws Exception {

        Path tempPath = tempFolder.newFile().toPath();

        VoterInformationWriter sut = new VoterInformationWriter(tempPath);
        sut.open(new ExecutionContext(Collections.emptyMap()));

        List<GeneratedVotingCardOutput> items = new ArrayList<>();
        items.add(createOutput());
        items.add(createOutput());
        items.add(createOutput());

        sut.write(items);

        final List<String> strings = Files.readAllLines(tempPath);
        Assert.assertEquals(items.size(), strings.size());

        strings.forEach((String l) -> {
            final String[] columns = l.split(",");
            Assert.assertEquals(expectedFormatLength, columns.length);
            Assert.assertEquals(votingCardId, columns[0]);
            Assert.assertEquals(ballotId, columns[1]);
            Assert.assertEquals(ballotBoxId, columns[2]);
            Assert.assertEquals(credentialId, columns[3]);
            Assert.assertEquals(electionEventId, columns[4]);
            Assert.assertEquals(votingCardSetId, columns[5]);
            Assert.assertEquals(verificationCardId, columns[6]);
            Assert.assertEquals(verificationCardSetId, columns[7]);
        });
    }

    private GeneratedVotingCardOutput createOutput() {
        return GeneratedVotingCardOutput.success(votingCardId,votingCardSetId,ballotId,ballotBoxId,credentialId,
            electionEventId,verificationCardId,verificationCardSetId, null, null, null,null,null);
    }

}
