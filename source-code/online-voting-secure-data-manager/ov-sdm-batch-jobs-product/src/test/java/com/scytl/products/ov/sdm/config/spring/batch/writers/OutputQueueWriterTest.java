/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch.writers;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.scytl.products.ov.sdm.config.spring.batch.GeneratedVotingCardOutput;

public class OutputQueueWriterTest {

    private OutputQueueWriter sut;

    @Test
    public void writeIntoQueueAllItems() throws Exception {

        //given
        BlockingQueue<GeneratedVotingCardOutput> queue = new LinkedBlockingQueue<>();
        List<GeneratedVotingCardOutput> items = new ArrayList<>();
        items.add(createOutput());
        items.add(createOutput());
        items.add(createOutput());
        sut = new OutputQueueWriter(queue);

        //when
        sut.write(items);

        //then
        Assert.assertEquals(items.size(), queue.size() );
    }

    private GeneratedVotingCardOutput createOutput() {
        return GeneratedVotingCardOutput.success(null,null,null,null,null,null,null,null,null, null, null,null,null);
    }
}
