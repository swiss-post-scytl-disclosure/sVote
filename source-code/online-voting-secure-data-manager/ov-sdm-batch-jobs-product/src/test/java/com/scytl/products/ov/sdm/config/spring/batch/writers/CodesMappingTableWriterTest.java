/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch.writers;


import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.springframework.batch.item.ExecutionContext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.scytl.products.ov.config.commands.voters.datapacks.beans.VerificationCardCodesDataPack;
import com.scytl.products.ov.sdm.config.spring.batch.GeneratedVotingCardOutput;
import com.scytl.products.ov.utils.ConfigObjectMapper;
import static org.junit.Assert.fail;

//@SuppressWarnings({"unchecked"})
public class CodesMappingTableWriterTest {

    private ConfigObjectMapper objectMapper = new ConfigObjectMapper();

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    //this is to document what the exact format we are expecting the writer writes
    private int expectedFormatLength = "%s,%s".split(",").length;

    @Test
    public void generateOutputFileWithCorrectDataFormat() throws Exception {

        //given
        Path tempPath = tempFolder.newFile().toPath();

        CodesMappingTableWriter sut = new CodesMappingTableWriter(tempPath);
        sut.open(new ExecutionContext(Collections.emptyMap()));

        final String verificationCardId = "verificationCardId";
        List<GeneratedVotingCardOutput> items = new ArrayList<>();
        items.add(createOutput(verificationCardId));
        items.add(createOutput(verificationCardId));
        items.add(createOutput(verificationCardId));

        //when
        sut.write(items);

        //then
        final List<String> strings = Files.readAllLines(tempPath);
        Assert.assertEquals(items.size(), strings.size());

        strings.forEach((String l) -> {
            final String[] columns = l.split(",");

            Assert.assertEquals(expectedFormatLength, columns.length);
            Assert.assertEquals(verificationCardId, columns[0]);
            String json = new String(Base64.getDecoder().decode(columns[1]));
            try {
                //map = VerificationCardCodesDataPack.getCodesMappingTable()
                Map<String,String> map = objectMapper.fromJSONToJava(json, Map.class);
                Assert.assertTrue(map.isEmpty());
            } catch (IOException e) {
                fail("unexpected format");
            }
        });
    }

    private GeneratedVotingCardOutput createOutput(String verificationCardId) {
        String ballotCastingKey = "ignored";
        String voteCastingCode = "ignored";

        VerificationCardCodesDataPack verificationCardCodesPack =
            new VerificationCardCodesDataPack(Collections.emptyMap(), ballotCastingKey, voteCastingCode,
                Collections.emptyMap());

        return GeneratedVotingCardOutput.success(null,null,null,null,null,null,verificationCardId,null,null, null,
            null,verificationCardCodesPack,null);
    }

}
