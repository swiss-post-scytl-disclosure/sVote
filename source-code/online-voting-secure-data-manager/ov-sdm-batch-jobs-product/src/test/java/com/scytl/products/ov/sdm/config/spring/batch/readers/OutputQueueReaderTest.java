/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch.readers;


import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.scytl.products.ov.sdm.config.spring.batch.GeneratedVotingCardOutput;

public class OutputQueueReaderTest {

    private OutputQueueReader sut;

    @Test
    public void returnNullWhenReceivedPoisonPill() throws Exception {

        //given
        BlockingQueue<GeneratedVotingCardOutput> queue = new LinkedBlockingQueue<>();
        queue.add(createOutput());
        queue.add(createOutput());
        queue.add(createPoisonPillOutput());
        sut = new OutputQueueReader(queue);

        GeneratedVotingCardOutput output1 = sut.read();
        GeneratedVotingCardOutput output2 = sut.read();
        GeneratedVotingCardOutput output3 = sut.read();

        Assert.assertNotNull(output1);
        Assert.assertNotNull(output2);
        Assert.assertNull(output3);

    }

    private GeneratedVotingCardOutput createOutput() {
        return GeneratedVotingCardOutput.success(null,null,null,null,null,null,null,null,null, null, null,null,null);
    }

    private GeneratedVotingCardOutput createPoisonPillOutput() {
        return GeneratedVotingCardOutput.poisonPill();
    }
}
