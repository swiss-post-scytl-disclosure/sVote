/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.config.spring.batch;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.products.oscore.logging.api.factory.LoggingFactory;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.ov.config.commands.uuid.UUIDGenerator;
import com.scytl.products.ov.config.commands.voters.JobExecutionObjectContext;
import com.scytl.products.ov.config.commands.voters.VotersHolderInitializer;
import com.scytl.products.ov.config.commands.voters.VotersParametersHolder;
import com.scytl.products.ov.config.commands.voters.VotersSerializationDestProvider;
import com.scytl.products.ov.config.commands.voters.datapacks.generators.VerificationCardSetCredentialDataPackGenerator;
import com.scytl.products.ov.config.commands.voters.datapacks.generators.VotingCardSetCredentialDataPackGenerator;
import com.scytl.products.ov.config.logs.LoggableInjector;
import com.scytl.products.ov.utils.FingerprintGenerator;

@Configuration
public class TestConfigServices {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Bean
    JobExecutionObjectContext executionObjectContext() {
        return mock(JobExecutionObjectContext.class);
    }

    @Bean
    VotingCardSetCredentialDataPackGenerator votingCardSetCredentialDataPackGenerator() {
        return mock(VotingCardSetCredentialDataPackGenerator.class);
    }

    @Bean
    VerificationCardSetCredentialDataPackGenerator verificationCardSetCredentialDataPackGenerator() {
        return mock(VerificationCardSetCredentialDataPackGenerator.class);
    }

    @Bean
    VotersHolderInitializer votersHolderInitializer(VotersParametersHolder holder) {
        final VotersHolderInitializer initializer = mock(VotersHolderInitializer.class);
        when(initializer.init(any(), any(InputStream.class))).thenReturn(holder);
        return initializer;
    }

    @Bean
    public PrimitivesServiceAPI primitivesServiceAPI() throws GeneralCryptoLibException {
        return new PrimitivesService();
    }

    @Bean
    VotersSerializationDestProvider serializationDestProvider() {
        return mock(VotersSerializationDestProvider.class);
    }

    @Bean // cannot be mocked (final)
    FingerprintGenerator fingerprintGenerator(PrimitivesServiceAPI serviceAPI) {
        return new FingerprintGenerator(serviceAPI);
    }

    @Bean
    UUIDGenerator uuidGenerator() {
        return new UUIDGenerator();
    }

    // these 3 below are to make @Loggable injection points work..
    @Bean
    public LoggableInjector loggableInjector() {
        return new LoggableInjector();
    }

    @Bean
    LoggingWriter loggingWriter() {
        return (level, logContent) -> System.out.println("test log message");
    }

    @Bean
    LoggingFactory loggingFactory(LoggingWriter writer) {
        LoggingFactory factory = mock(LoggingFactory.class);
        when(factory.getLogger(anyString())).thenReturn(writer);
        return factory;
    }
}
