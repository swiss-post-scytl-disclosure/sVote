/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.webapp.mvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.scytl.products.ov.commons.verify.PayloadSigningCertificateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.domain.model.messaging.PayloadVerificationException;
import com.scytl.products.ov.commons.verify.PayloadVerifier;
import com.scytl.products.ov.mixnet.webapp.spring.MixDecValSpringConfig;

@Configuration
@EnableWebMvc
@Import(MixDecValSpringConfig.class)
public class TestConfig {

    @Bean
    MixDecValController mixnetController() {
        return new MixDecValController();
    }

    @Bean
    SecureLoggingWriter secureLoggingWriter() {
        SecureLoggingWriter slw = mock(SecureLoggingWriter.class);
        doNothing().when(slw).log(any(), any(), any());
        doNothing().when(slw).log(any(), any());

        return slw;
    }

    @Bean
    PayloadVerifier payloadVerifier(AsymmetricServiceAPI asymmetricService,
            PayloadSigningCertificateValidator certificateValidator) throws PayloadVerificationException {
        PayloadVerifier payloadVerifier = mock(PayloadVerifier.class);
        when(payloadVerifier.isValid(any(), any())).thenReturn(true);
        return payloadVerifier;
    }
}
