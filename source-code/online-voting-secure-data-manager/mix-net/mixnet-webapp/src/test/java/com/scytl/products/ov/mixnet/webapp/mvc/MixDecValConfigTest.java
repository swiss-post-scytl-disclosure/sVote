/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.webapp.mvc;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.Test;

public class MixDecValConfigTest {

  /**
   * Ensure the object can be correctly deserialised from an actual JSON sample. It also checks that
   * resources for the tests that will use the JSON file are available.
   */
  @Test
  public void testDeserialization() throws IOException {
    String filename = "src/test/resources/mixdecval/request_body.json";

    ObjectMapper objectMapper = new ObjectMapper();
    MixDecValConfig config = objectMapper.readValue(new File(filename), MixDecValConfig.class);

    assertNotNull(config);
    assertNotNull(config.getValidator().getBallot().getId());
    assertNotNull(config.getMixer().getOutputDirectory());

    assertTrue(Files.exists(Paths.get(config.getMixer().getOutputDirectory())));
    assertTrue(Files.exists(Paths.get(config.getMixer().getInput().get("publicKey") + ".json")));
    assertTrue(Files.exists(Paths.get(config.getMixer().getInput().get("encryptedBallots") + ".csv")));
    assertTrue(Files.exists(Paths.get(config.getMixer().getInput().get("encryptionParameters") + ".json")));
    assertTrue(Files.exists(Paths.get(config.getVerifier().getInputDirectory())));
    assertTrue(Files.exists(Paths.get(config.getVerifier().getOutputDirectory())));
  }

}
