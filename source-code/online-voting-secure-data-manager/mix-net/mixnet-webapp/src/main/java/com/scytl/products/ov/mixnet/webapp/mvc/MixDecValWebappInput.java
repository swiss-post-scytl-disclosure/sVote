/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.mixnet.webapp.mvc;

import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.products.ov.commons.beans.Ballot;
import com.scytl.products.ov.mixnet.commons.configuration.MixerConfig;
import com.scytl.products.ov.mixnet.commons.configuration.VerifierConfig;

/**
 * Body of the request to start a mixing/decryption/validation operation in the SDM cryptoservices.
 */
public class MixDecValWebappInput {

  private Integer concurrencyLevel;
  private Integer numberOfParallelBatches;
  private Integer numOptions;
  private Integer minimumBatchSize;
  private MixerConfig mixer;
  private VerifierConfig verifier;
  private Ballot ballot;
  private String tenantId;
  private String ballotBoxId;
  private String certId;
  private ElGamalEncryptionParameters elGamalEncryptionParameters;
  private String privateKeyPEM;
  private String writeInAlphabet;

  public Integer getConcurrencyLevel() {
    return concurrencyLevel;
  }

  public void setConcurrencyLevel(final Integer concurrencyLevel) {
    this.concurrencyLevel = concurrencyLevel;
  }

  public Integer getNumOptions() {
    return numOptions;
  }

  public void setNumOptions(final Integer numOptions) {
    this.numOptions = numOptions;
  }

  public MixerConfig getMixer() {
    return mixer;
  }

  public void setMixer(final MixerConfig mixer) {
    this.mixer = mixer;
  }

  public VerifierConfig getVerifier() {
    return verifier;
  }

  public void setVerifier(final VerifierConfig verifier) {
    this.verifier = verifier;
  }

  public boolean hasMixerConfig() {
    return this.mixer != null;
  }

  public boolean hasVerifierConfig() {
    return this.verifier != null;
  }

  public Integer getMinimumBatchSize() {
    return minimumBatchSize;
  }

  public void setMinimumBatchSize(final Integer minimumBatchSize) {
    this.minimumBatchSize = minimumBatchSize;
  }

  public Integer getNumberOfParallelBatches() {
    return numberOfParallelBatches;
  }

  public void setNumberOfParallelBatches(final Integer numberOfParallelBatches) {
    this.numberOfParallelBatches = numberOfParallelBatches;
  }


  public ElGamalEncryptionParameters getElGamalEncryptionParameters() {
    return elGamalEncryptionParameters;
  }

  public void setElGamalEncryptionParameters(
      ElGamalEncryptionParameters elGamalEncryptionParameters) {
    this.elGamalEncryptionParameters = elGamalEncryptionParameters;
  }

  public Ballot getBallot() {
    return ballot;
  }

  public void setBallot(Ballot ballot) {
    this.ballot = ballot;
  }

  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  public String getBallotBoxId() {
    return ballotBoxId;
  }

  public void setBallotBoxId(String ballotBoxId) {
    this.ballotBoxId = ballotBoxId;
  }

  public String getCertId() {
    return certId;
  }

  public void setCertId(String certId) {
    this.certId = certId;
  }

  public String getPrivateKeyPEM() {
    return privateKeyPEM;
  }

  public void setPrivateKeyPEM(String privateKeyPEM) {
    this.privateKeyPEM = privateKeyPEM;
  }

  public String getWriteInAlphabet() {
    return writeInAlphabet;
  }

  public void setWriteInAlphabet(String alphabet) {
    this.writeInAlphabet = alphabet;
  }
}
