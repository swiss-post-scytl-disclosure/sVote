/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Decryption logging events
 */
public enum ValidationLogEvents implements LogEvent {

  VOTES_WITH_PROOF_READING_FAILURE("VAL", "DECVAL", "000",
      "Error reading list of votes with proof from file"),
  RECEBPK_SUCCESS_BATCH_OF_VOTES_DECRYPTED("DEC", "DECVAL", "000",
      "Batch of votes successfully decrypted"),
  RECEBPK_ERROR_VALIDATING_FACTORIZED_VOTE("VAL", "DECVAL", "1204",
      "Error while validating factorized vote");

  private final String layer;

  private final String action;

  private final String outcome;

  private final String info;

  ValidationLogEvents(final String layer, final String action, final String outcome, final String info) {
    this.layer = layer;
    this.action = action;
    this.outcome = outcome;
    this.info = info;
  }

  @Override
  public String getAction() {
    return action;
  }

  @Override
  public String getOutcome() {
    return outcome;
  }

  @Override
  public String getInfo() {
    return info;
  }

  @Override
  public String getLayer() {
    return layer;
  }

}
