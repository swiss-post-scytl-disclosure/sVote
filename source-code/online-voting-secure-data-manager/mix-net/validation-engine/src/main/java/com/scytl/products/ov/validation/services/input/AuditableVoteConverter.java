/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.services.input;

import com.googlecode.jcsv.writer.CSVEntryConverter;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.scytl.products.ov.validation.commons.beans.AuditableVote;
import com.scytl.products.ov.validation.commons.beans.BigIntegerFactor;
import com.scytl.products.ov.validation.commons.beans.ValidationFailure;

/**
 * A utility class used to write line by line the following values as a csv entry:
 * decrypted vote,
 * error code,
 * factorized values,
 * the rest value if it is bigger than 1.
 */
public class AuditableVoteConverter implements CSVEntryConverter<AuditableVote> {

    private static final String EXPONENT_DELIM = "^";

    @Override
    public String[] convertEntry(final AuditableVote vote) {

        List<String> values = new ArrayList<>();
        values.add(String.valueOf(ZonedDateTime.now(ZoneOffset.UTC)));
        values.add(vote.getFailures().stream().map(ValidationFailure::getCode).collect(Collectors.joining(",")));
        values.add(vote.getCompressed().toString());
        List<BigIntegerFactor> factorization = vote.getOptions();
        values.add(factorization.stream().map(factor ->
                factor.getBase().toString() + EXPONENT_DELIM + factor.getExponent()).collect(Collectors.joining(" ")));

        return values.toArray(new String[values.size()]);
    }
}
