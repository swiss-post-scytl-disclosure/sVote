/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.spring.batch;

import org.springframework.core.task.TaskDecorator;

import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;

/**
 * Implementation of {@link TaskDecorator} which allows to run the supplied job
 * task in appropriate context.
 */
class JobTaskDecorator implements TaskDecorator {
    private final TransactionInfoProvider provider;

    private final String tenantId;

    /**
     * Constructor.
     *
     * @param provider
     * @param tenantId
     */
    public JobTaskDecorator(final TransactionInfoProvider provider,
            final String tenantId) {
        this.provider = provider;
        this.tenantId = tenantId;
    }

    @Override
    public Runnable decorate(final Runnable task) {
        return () -> runTask(task);
    }

    private void runTask(final Runnable task) {
        provider.generate(tenantId, "", "");
        task.run();
    }
}
