/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation;

/**
 * An error during the validation process in the SDM.
 */
public class ValidationEngineException extends RuntimeException {

	private static final long serialVersionUID = 1809097422782053572L;

	public ValidationEngineException(Throwable cause) {
        super(cause);
    }
	
	public ValidationEngineException(String message) {
        super(message);
    }
	
	public ValidationEngineException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
