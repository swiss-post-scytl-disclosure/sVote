/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.services;


import com.scytl.products.ov.validation.ValidationException;
import com.scytl.products.ov.validation.ValidatorConfig;
import java.nio.file.Path;
import java.util.List;

/**
 * Service that validates the decrypted votes coming out of a mixing process.
 */
public interface ValidationService {

  /**
   * Validate decrypted votes in all the mixing paths.
   * @param config settings for the validation
   * @param mixingPaths paths where the results of the mixing were left
   */
  void validate(ValidatorConfig config, List<Path> mixingPaths)
      throws ValidationException;
}
