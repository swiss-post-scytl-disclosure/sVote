/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.services;


import com.scytl.products.ov.validation.commons.beans.AuditableVote;
import com.scytl.products.ov.validation.commons.beans.DecompressedVote;
import com.scytl.products.ov.mixnet.commons.beans.VoteWithProof;

public class DecryptedVoteOutput {

    private AuditableVote auditableVote;
    private VoteWithProof voteWithProof;
    private DecompressedVote decompressedVote;

    private boolean isPoisonPill;

    private DecryptedVoteOutput(final AuditableVote auditableVote, final VoteWithProof voteWithProof,
                                final DecompressedVote decompressedVote) {
        this.auditableVote = auditableVote;
        this.voteWithProof = voteWithProof;
        this.decompressedVote = decompressedVote;
    }

    public DecryptedVoteOutput(final boolean isPoisonPill) {
        this.isPoisonPill = isPoisonPill;
    }

    public static DecryptedVoteOutput create(final AuditableVote auditableVote, final VoteWithProof voteWithProof,
                                             final DecompressedVote decompressedVote) {
        return new DecryptedVoteOutput(auditableVote, voteWithProof, decompressedVote);
    }

    public static DecryptedVoteOutput poisonPill() {
        return new DecryptedVoteOutput(true);

    }

    public AuditableVote getAuditableVote() {
        return auditableVote;
    }

    public VoteWithProof getVoteWithProof() {
        return voteWithProof;
    }

    public DecompressedVote getDecompressedVote() {
        return decompressedVote;
    }

    public boolean isPoisonPill() {
        return isPoisonPill;
    }
}
