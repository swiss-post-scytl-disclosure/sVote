/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.utils;

import org.springframework.beans.factory.annotation.Value;

public class PropertiesBasedJobSelectionStrategy implements JobSelectionStrategy {

    @Value("${spring.batch.jobs.qualifier:product}")
    private String qualifier;

    private String prefix;

    public PropertiesBasedJobSelectionStrategy(final String prefix) {
        this.prefix = prefix;
    }


    @Override
    public String select() {
        return String.format("%s-%s", prefix, qualifier);
    }
}
