/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.services.input;

import java.util.List;
import java.util.stream.Collectors;

import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.scytl.products.ov.validation.commons.beans.DecompressedVote;

/**
 * Converts decompressed votes into a format suitable to be written as a record
 * in a CSV file.
 */
public class DecompressedVoteConverter implements CSVEntryConverter<DecompressedVote> {

    @Override
    public String[] convertEntry(final DecompressedVote vote) {

        // retrieve the bases of the options.
        List<String> values =
            vote.getOptions().stream().map(option -> option.getBase().toString()).collect(Collectors.toList());

        // retrieve the write-in textss.
        values.addAll(vote.getWriteIns());

        return values.toArray(new String[values.size()]);
    }
}
