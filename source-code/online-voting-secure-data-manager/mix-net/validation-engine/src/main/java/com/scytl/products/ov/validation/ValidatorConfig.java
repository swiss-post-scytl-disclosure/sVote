/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scytl.products.ov.commons.beans.Ballot;

/**
 * Input data for the validation controller.
 */
public class ValidatorConfig {

    private final Ballot ballot;

    private final String tenantId;

    private final String ballotBoxId;

    private final String elGamalEncryptionParametersP;

    private final String privateKeyPEM;

    private final String writeInAlphabet;

    private final Path outputPath;

    @JsonCreator
    public ValidatorConfig(@JsonProperty(required = true, value = "ballot") Ballot ballot,
            @JsonProperty(required = true, value = "tenantId") String tenantId,
            @JsonProperty(required = true, value = "ballotBoxId") String ballotBoxId,
            @JsonProperty(required = true, value = "elGamalEncryptionParametersP") String elGamalEncryptionParametersP,
            @JsonProperty(required = true, value = "signingKey") String signingKey,
            @JsonProperty(required = true, value = "writeInAlphabet") String writeInAlphabet,
            @JsonProperty(required = true, value = "outputPath") String outputPath) {
        this.ballot = ballot;
        this.tenantId = tenantId;
        this.ballotBoxId = ballotBoxId;
        this.elGamalEncryptionParametersP = elGamalEncryptionParametersP;
        this.privateKeyPEM = signingKey;
        this.writeInAlphabet = writeInAlphabet;
        this.outputPath = Paths.get(outputPath);
    }

    public String getElGamalEncryptionParametersP() {
        return elGamalEncryptionParametersP;
    }

    public Ballot getBallot() {
        return ballot;
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public String getElectionEventId() {
        return ballot.getElectionEvent().getId();
    }

    public String getPrivateKeyPEM() {
        return privateKeyPEM;
    }

    public String getWriteInAlphabet() {
        return writeInAlphabet;
    }

    public Path getOutputPath() {
        return outputPath;
    }
}
