/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.log;

/**
 * Constants to log decryption.
 */
public class ValidationLogConstants {
	
	/**
	 * The votes with proof file - additional info
	 */
	public static final String FILE_NAME = "#file_name";

	/**
	 * An error description - additional information.
	 */
	public static final String INFO_ERR_DESC = "#err_desc";

	/**
	 * A hash of the vote - additional information.
	 */
	public static final String INFO_HASH_VOTE = "#hashVote";
	
	/**
	 * Non-public constructor
	 */
	private ValidationLogConstants() {
	}
}
