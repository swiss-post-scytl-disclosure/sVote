/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.services.input;

import com.googlecode.jcsv.reader.CSVEntryParser;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.products.ov.commons.util.ConfigObjectMapper;
import com.scytl.products.ov.mixnet.commons.beans.ElGamalEncryptedBallot;
import com.scytl.products.ov.mixnet.commons.beans.VoteWithProof;
import com.scytl.products.ov.validation.ValidationEngineException;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * A utility class used by BallotReader to read line by line CSV entries and convert them to a
 * VoteWithProof objects
 */
public final class VoteWithProofEntryParser implements CSVEntryParser<VoteWithProof> {


  private final ConfigObjectMapper objectMapper;

  public VoteWithProofEntryParser(ConfigObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public VoteWithProof parseEntry(final String... data) {
    try {
      return new VoteWithProof(
          new ElGamalEncryptedBallot(Arrays.asList(
              objectMapper.fromJSONToJava(data[0], ZpGroupElement[].class)
          )),
          Arrays.asList(objectMapper.fromJSONToJava(data[1], BigInteger[].class)),
          data[2]
      );
    } catch (IOException e) {
      throw new ValidationEngineException("Failed while parsing vote with proof from CSV", e);
    }
  }
}
