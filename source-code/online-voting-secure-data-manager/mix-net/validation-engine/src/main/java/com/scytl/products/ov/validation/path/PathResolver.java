/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.path;

import java.nio.file.Path;

/**
 * Interface for resolving path.
 */
public interface PathResolver {

    /**
     * This method returns the Path from the given string.
     * 
     * @param path
     * @return
     */
	Path resolve(final String path);
}
