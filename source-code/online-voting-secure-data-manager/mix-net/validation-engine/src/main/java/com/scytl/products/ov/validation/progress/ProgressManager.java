/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.progress;


import java.util.Optional;
import java.util.UUID;

import com.scytl.products.ov.validation.commons.progress.JobProgressDetails;

public interface ProgressManager {

    void registerJob(UUID jobId, JobProgressDetails job);
    void registerJob(String jobId, JobProgressDetails job);
    void unregisterJob(UUID jobId);
    void unregisterJob(String jobId);

    Optional<JobProgressDetails> getJobProgress(UUID jobId);
    Optional<JobProgressDetails> getJobProgress(String jobId);

    void updateJobProgress(UUID jobId, long deltaValue);
}
