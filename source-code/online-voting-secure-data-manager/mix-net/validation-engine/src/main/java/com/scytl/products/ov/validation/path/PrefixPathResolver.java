/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.path;

import static java.text.MessageFormat.format;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * {@link PathResolver} implementation that prepends a suffix to the provided
 * path
 */
public class PrefixPathResolver implements PathResolver {

    private final Path prefix;

    /**
     * Constructor.
     *
     * @param prefix
     *            the prefix, should be absolute path
     * @throws IllegalArgumentException
     *             the prefix is not absolute.
     */
    public PrefixPathResolver(final String prefix) {
		Path path = Paths.get(prefix);
		if (!path.isAbsolute()) {
			throw new IllegalArgumentException(format("Prefix ''{0}'' is not absolute.", prefix));
		}
		this.prefix = path;
    }

    @Override
    public Path resolve(final String path) {
        return prefix.resolve(path.trim());
    }
}
