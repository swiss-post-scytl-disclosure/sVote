/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation;

/**
 * An error during the validation process in the SDM.
 */
public class ValidationException extends Exception {

  public ValidationException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public ValidationException(final String message) {
    super(message);
  }
}
