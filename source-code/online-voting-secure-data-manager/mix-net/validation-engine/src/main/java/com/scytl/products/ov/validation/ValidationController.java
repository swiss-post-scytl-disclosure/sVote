/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation;

import com.scytl.products.ov.validation.services.ValidationService;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validates the outcome of a mixing+decryption process.
 *
 * This class takes the form of a controller to keep consistency with its package neighbours.
 */
// SV-6484 Remove this controller in favour of a direct call to the service
public class ValidationController {

  private static final Logger logger = LoggerFactory.getLogger(ValidationController.class);

  private final ValidationService validationService;

  public ValidationController(ValidationService validationService) {
    this.validationService = validationService;
  }

  /**
   * @param config the configuration of the validation job
   * @return a response resulting from trying to start the validation job
   * @throws com.scytl.products.ov.validation.ValidationException
   */
  public boolean validate(ValidatorConfig config, List<Path> mixingPaths) throws ValidationException {
    Instant start = Instant.now();

    validationService.validate(config, mixingPaths);

    Instant end = Instant.now();

    logger.info("Validation duration was {}", Duration.between(start, end));

    return true;
  }
}
