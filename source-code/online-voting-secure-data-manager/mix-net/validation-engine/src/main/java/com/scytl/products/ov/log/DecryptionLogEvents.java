/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Decryption logging events
 */
public enum DecryptionLogEvents implements LogEvent {

    RECEBPK_SUCCESS_ELECTORAL_BOARD_PRIVATE_KEY_RECONSTRUCTED("DEC", "RECEBPK", "000",
            "Electoral board private key sucessfully reconstructed"),

    RECEBPK_ERROR_RECONSTRUCTING_ELECTORAL_BOARD_PRIVATE_KEY("DEC", "RECEBPK", "1201",
            "Error while trying to reconstruct the electoral board private"),

    RECEBPK_SUCCESS_MIXED_BALLOT_BOX_SIGNATURE_VALIDATED("DEC", "DECVAL", "000",
            "Mixed ballot box signature successfully validated"),

    RECEBPK_ERROR_VALIDATING_MIXED_BALLOT_BOX_SIGNATURE("DEC", "DECVAL", "1202",
            "Error while validating signature of mixed ballot"),

    RECEBPK_ERROR_DECRYPTING_VOTE("DEC", "DECVAL", "1203", "Error while decrypting a vote"),

    RECEBPK_ERROR_VALIDATING_FACTORIZED_VOTE("DEC", "DECVAL", "1204",
            "Error while validating factorized vote"),

    RECEBPK_ERROR_GENERATING_DECRYPTION_PROOF("DEC", "DECVAL", "1205", "Error generating decryption proof"),

    RECEBPK_ERROR_GENERATING_LIST_VOTES_WITH_FACTORIZATION_ERRORS("DEC", "DECVAL", "1206",
            "Error generating a list of a votes with factorization errors"),

    RECEBPK_SUCCESS_BATCH_OF_VOTES_DECRYPTED("DEC", "DECVAL", "000", "Batch of votes successfully decrypted"),

    RECEBPK_SUCCESS_LIST_OF_DECRYPTED_VOTES_CREATED("DEC", "DECVAL", "000", "List of all decrypted votes created"),

    RECEBPK_SUCCESS_LIST_OF_DECRYPTED_VOTES_WRITTEN_TO_FILE("DEC", "DECVAL", "000",
            "List of all decrypted votes written to file"),

    RECEBPK_ERROR_WRITING_LIST_OF_DECRYPTED_VOTES_TO_FILE("DEC", "DECVAL", "1207",
            "Error writing list of all decrypted votes to file"),

    RECEBPK_SUCCESS_LIST_OF_AUDITABLE_VOTES_WRITTEN_TO_FILE("DEC", "DECVAL", "000",
            "List of all auditable votes written to file"),

    RECEBPK_ERROR_WRITING_LIST_OF_AUDITABLE_VOTES_TO_FILE("DEC", "DECVAL", "1208",
            "Error while writing list of auditable votes to file"),

    RECEBPK_SUCCESS_LIST_OF_DECRYPTION_PROOFS_WRITTEN_TO_FILE("DEC", "DECVAL", "000",
            "List of all decryption proofs written to file"),

    RECEBPK_ERROR_WRITING_LIST_OF_DECRYPTION_PROOFS_TO_FILE("DEC", "DECVAL", "1209",
            "Error writing list of decryption proofs to file");

    private final String layer;

    private final String action;

    private final String outcome;

    private final String info;

    DecryptionLogEvents(final String layer, final String action, final String outcome, final String info) {
        this.layer = layer;
        this.action = action;
        this.outcome = outcome;
        this.info = info;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public String getOutcome() {
        return outcome;
    }

    @Override
    public String getInfo() {
        return info;
    }

    @Override
    public String getLayer() {
        return layer;
    }

}
