/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.services.input;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.scytl.products.ov.validation.services.input.AuditableVoteConverter;
import java.math.BigInteger;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Test;

import com.scytl.products.ov.validation.commons.beans.AuditableVote;
import com.scytl.products.ov.validation.commons.beans.BigIntegerFactor;
import com.scytl.products.ov.validation.commons.beans.Factorization;
import com.scytl.products.ov.validation.commons.beans.ValidationFailure;

/**
 * Tests of {@link AuditableVoteConverter}.
 */
public class AuditableVoteConverterTest {

    @Test
    public void testConvertEntry() {
        AuditableVoteConverter converter = new AuditableVoteConverter();
        List<BigIntegerFactor> factors =
            asList(new BigIntegerFactor("2"), new BigIntegerFactor("5"));
        Factorization factorization =
            new Factorization(BigInteger.TEN, factors, BigInteger.ONE);
        List<ValidationFailure> failures =
            asList(new ValidationFailure("message1", "code1"),
                new ValidationFailure("message2", "code2"));
        AuditableVote vote = new AuditableVote(factorization, failures);

        ZonedDateTime before = ZonedDateTime.now(ZoneOffset.UTC);
        String[] entry = converter.convertEntry(vote);
        ZonedDateTime after = ZonedDateTime.now(ZoneOffset.UTC);
        assertEquals(4, entry.length);
        ZonedDateTime time = ZonedDateTime.parse(entry[0]);
        assertTrue(!before.isAfter(time) && !after.isBefore(time));
        assertEquals("code1,code2", entry[1]);
        assertEquals(BigInteger.TEN.toString(), entry[2]);
        assertEquals("2^1 5^1", entry[3]);
    }
}
