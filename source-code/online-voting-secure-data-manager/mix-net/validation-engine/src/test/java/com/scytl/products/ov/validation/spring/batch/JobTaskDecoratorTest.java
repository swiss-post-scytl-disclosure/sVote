/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.spring.batch;

import static org.junit.Assert.assertEquals;

import com.scytl.products.ov.validation.spring.batch.JobTaskDecorator;
import org.junit.Before;
import org.junit.Test;

import com.scytl.products.oscore.logging.api.domain.TransactionInfo;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;

/**
 * Tests of {@link JobTaskDecorator}.
 */
public class JobTaskDecoratorTest {
    private static final String TENANT_ID = "tenantId";

    private TransactionInfoProvider provider;

    private JobTaskDecorator decorator;

    private TransactionInfo info;

    @Before
    public void setUp() throws Exception {
        provider = new TransactionInfoProvider();
        decorator = new JobTaskDecorator(provider, TENANT_ID);
    }

    @Test
    public void testDecorate() {
        Runnable task = () -> {
            info = provider.get();
        };
        Thread thread = new Thread(decorator.decorate(task));
        thread.setDaemon(true);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        assertEquals(TENANT_ID, info.getTenant());
    }
}
