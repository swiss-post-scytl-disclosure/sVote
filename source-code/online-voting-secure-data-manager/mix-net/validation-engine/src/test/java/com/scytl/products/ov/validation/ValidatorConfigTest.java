/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.scytl.products.ov.commons.util.ConfigObjectMapper;
import com.scytl.products.ov.validation.ValidatorConfig;
import java.io.File;
import java.io.IOException;
import org.junit.Test;

public class ValidatorConfigTest {

  @Test
  public void deserializeFromJson() throws IOException {
    ValidatorConfig sut = new ConfigObjectMapper()
        .fromJSONFileToJava(new File("src/test/resources/validator-config.json"),
            ValidatorConfig.class);

    // Ensure the basic properties are present.
    assertThat(sut.getBallotBoxId(), is("6af19d867c7a43e8b62e3bc4814183df"));
    // Ensure the ballot object was correctly deserialised.
    assertThat(sut.getBallot().getElectionEvent().getId(), is("51683a22a7c849679b787342c691d7ab"));
  }
}
