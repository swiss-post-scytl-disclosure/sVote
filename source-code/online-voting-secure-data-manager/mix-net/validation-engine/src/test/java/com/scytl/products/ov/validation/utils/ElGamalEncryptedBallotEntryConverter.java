/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.utils;

import com.googlecode.jcsv.writer.CSVEntryConverter;

import java.util.List;

import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.products.ov.mixnet.commons.beans.ElGamalEncryptedBallot;

/*
 * A utility class used by InputBallotWriter to write line by line a ElGamalEncryptedBallot as a csv entry
 */
public final class ElGamalEncryptedBallotEntryConverter implements CSVEntryConverter<ElGamalEncryptedBallot> {

    /**
     * @see CSVEntryConverter#convertEntry(Object)
     */
    @Override
    public String[] convertEntry(final ElGamalEncryptedBallot encryptedBallot) {

        final List<ZpGroupElement> phis = encryptedBallot.getPhis();
        final int columnsSize = encryptedBallot.getPhis().size() + 1;

        String[] columns = new String[columnsSize];

        columns[0] = encryptedBallot.getGamma().getValue().toString();

        for (int i = 1; i < columnsSize; i++) {
            columns[i] = phis.get(i - 1).getValue().toString();

        }

        return columns;
    }

}
