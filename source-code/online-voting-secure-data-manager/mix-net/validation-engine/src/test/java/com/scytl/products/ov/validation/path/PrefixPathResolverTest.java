/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.path;

import static org.junit.Assert.assertEquals;

import com.scytl.products.ov.validation.path.PathResolver;
import com.scytl.products.ov.validation.path.PrefixPathResolver;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

/**
 * Tests of {@link PrefixPathResolver}.
 */
public class PrefixPathResolverTest {
    @Test(expected = IllegalArgumentException.class)
    public void testPrefixPathResolverNonAbsolute() {
        Path prefix = Paths.get("foo", "bar");
        new PrefixPathResolver(prefix.toString());
    }

    @Test
    public void testResolve() {
        Path prefix = Paths.get("foo", "bar").toAbsolutePath();
        PathResolver resolver = new PrefixPathResolver(prefix.toString());
        assertEquals(prefix.resolve("test"), resolver.resolve("test"));
    }
}
