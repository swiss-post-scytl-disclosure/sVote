/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.progress;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;

import com.scytl.products.ov.validation.progress.ProgressManagerImpl;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.scytl.products.ov.validation.commons.progress.JobProgressDetails;

/**
 * Tests of {@link ProgressManagerImpl}.
 */
public class ProgressManagerImplTest {
    private static final UUID ID = UUID.randomUUID();

    private static final JobProgressDetails DETAILS =
        new JobProgressDetails(ID, 100);

    private ProgressManagerImpl manager;

    @Before
    public void setUp() {
        manager = new ProgressManagerImpl();
    }

    @Test
    public void testGetJobProgressString() {
        manager.registerJob(ID, DETAILS);
        assertEquals(DETAILS, manager.getJobProgress(ID.toString()).get());
    }

    @Test
    public void testRegisterJobStringJobProgressDetails() {
        manager.registerJob(ID.toString(), DETAILS);
        assertEquals(DETAILS, manager.getJobProgress(ID).get());
    }

    @Test
    public void testRegisterJobStringJobProgressDetailsReplace() {
        manager.registerJob(ID, DETAILS);
        manager.registerJob(ID.toString(),
            new JobProgressDetails(ID, 200));
        assertNotSame(DETAILS, manager.getJobProgress(ID).get());
    }

    @Test
    public void testRegisterJobUUIDJobProgressDetails() {
        manager.registerJob(ID, DETAILS);
        assertEquals(DETAILS, manager.getJobProgress(ID).get());
    }

    @Test
    public void testRegisterJobUUIDJobProgressDetailsReplace() {
        manager.registerJob(ID, DETAILS);
        manager.registerJob(ID, new JobProgressDetails(ID, 200));
        assertNotSame(DETAILS, manager.getJobProgress(ID).get());
    }

    @Test
    public void testUnregisterJobString() {
        manager.registerJob(ID, DETAILS);
        manager.unregisterJob(ID.toString());
        assertFalse(manager.getJobProgress(ID).isPresent());
    }

    @Test
    public void testUnregisterJobUUID() {
        manager.registerJob(ID, DETAILS);
        manager.unregisterJob(ID);
        assertFalse(manager.getJobProgress(ID).isPresent());
    }

    @Test
    public void testUpdateJobProgress() {
        JobProgressDetails details = new JobProgressDetails(ID, 100);
        manager.registerJob(ID, details);
        manager.updateJobProgress(ID, 1);
        assertEquals(99, details.getRemainingWork());
    }

    @Test
    public void testUpdateJobProgressNotFound() {
        manager.updateJobProgress(ID, 1);
    }
}
