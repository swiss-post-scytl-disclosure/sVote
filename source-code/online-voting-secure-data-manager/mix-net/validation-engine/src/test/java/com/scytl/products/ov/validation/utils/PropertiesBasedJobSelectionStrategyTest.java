/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.utils;

import static org.junit.Assert.assertTrue;

import com.scytl.products.ov.validation.utils.PropertiesBasedJobSelectionStrategy;
import java.lang.reflect.Field;

import org.junit.Test;

/**
 * Tests of {@link PropertiesBasedJobSelectionStrategy}.
 */
public class PropertiesBasedJobSelectionStrategyTest {
    @Test
    public void testSelect()
            throws NoSuchFieldException, SecurityException,
            IllegalArgumentException, IllegalAccessException {
        PropertiesBasedJobSelectionStrategy strategy =
            new PropertiesBasedJobSelectionStrategy("prefix");
        Field field = PropertiesBasedJobSelectionStrategy.class
            .getDeclaredField("qualifier");
        field.setAccessible(true);
        field.set(strategy, "qualifier");

        String value = strategy.select();
        assertTrue(value.startsWith("prefix"));
        assertTrue(value.endsWith("qualifier"));
    }
}
