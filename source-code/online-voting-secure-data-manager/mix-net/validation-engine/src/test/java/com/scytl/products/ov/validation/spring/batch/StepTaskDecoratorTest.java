/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.spring.batch;

import static org.junit.Assert.assertEquals;

import com.scytl.products.ov.validation.spring.batch.StepTaskDecorator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.scope.context.JobContext;
import org.springframework.batch.core.scope.context.JobSynchronizationManager;

import com.scytl.products.oscore.logging.api.domain.TransactionInfo;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;

/**
 * Tests of {@link StepTaskDecorator}.
 */
public class StepTaskDecoratorTest {
    private static final String TENANT_ID = "tenantId";

    private TransactionInfoProvider provider;

    private StepTaskDecorator decorator;

    private JobContext context;

    private TransactionInfo info;

    @Before
    public void setUp() throws Exception {
        JobSynchronizationManager.register(new JobExecution(1L));
        provider = new TransactionInfoProvider();
        decorator = new StepTaskDecorator(provider, TENANT_ID);
    }

    @After
    public void tearDown() {
        JobSynchronizationManager.close();
    }

    @Test
    public void testDecorate() {
        Runnable task = () -> {
            context = JobSynchronizationManager.getContext();
            info = provider.get();
        };
        Thread thread = new Thread(decorator.decorate(task));
        thread.setDaemon(true);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        assertEquals(1L, context.getJobExecution().getId().longValue());
        assertEquals(TENANT_ID, info.getTenant());
    }
}
