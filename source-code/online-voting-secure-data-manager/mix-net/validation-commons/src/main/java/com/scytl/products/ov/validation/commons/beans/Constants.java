/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

public class Constants {

	public static final String DECRYPTION_SERVICE_NAME = "decryption";

    public static final String JOB_TENANT_ID = "tenantId";

    public static final String JOB_ELECTION_EVENT_ID = "electionEventId";

    public static final String JOB_INSTANCE_ID = "jobInstanceId";

    public static final String JOB_BALLOTBOX_ID = "ballotBoxId";

    public static final String JOB_BALLOT_ID = "ballotId";

    public static final String JOB_STATUS = "status";

    public static final String JOB_ID = "jobId";

    public static final String JOB_INPUT_QUEUE = "inputQueue";

    public static final String JOB_OUTPUT_QUEUE = "outputQueue";

    public static final String JOB_BALLOT = "ballot";

    public static final String JOB_CERT_ID = "certId";

    public static final String JOB_ENCRYPTION_PARAMS = "encryptionParams";

    public static final String JOB_PRIVATE_KEY_PEM = "privateKeyPEM";

    public static final String JOB_SERIALIZED_PRIVATE_KEY = "serializedPrivateKey";

    public static final String JOB_ENCRYPTION_PARAMS_P = "encryptionParams_P";

    public static final String JOB_ENCRYPTION_PARAMS_Q = "encryptionParams_Q";

    public static final String JOB_ENCRYPTION_PARAMS_G = "encryptionParams_G";

    public static final String JOB_BALLOT_JSON_FILE_NAME = "ballot.json";

    public static final String JOB_DECRYPTED_VOTES_COUNT = "decryptedVotesCount";

    public static final String JOB_AUDITABLE_VOTES_COUNT = "auditableVotesCount";

    public static final String JOB_DECRYPTION_ERRORS_COUNT = "decryptionErrorsCount";

    public static final String JOB_APPROXIMATE_VOTE_COUNT = "approximateVoteCount";

    public static final String JOB_ENCRYPTED_BALLOTS_FILE = "encryptedBallotsFile";

    public static final String JOB_SIGNATURE_CERTIFICATE_PEM = "signatureCertificatePem";

    public static final String JOB_DECODED_SERIALIZED_PRIVATE_KEY = "decodedSerializedPrivateKey";

    public static final String JOB_SIGNATURE_PRIVATE_KEY_PEM = "signaturePrivateKeyPem";

    public static final String JOB_DECRYPTED_VOTES_OUTPUT_FILE_PATH = "decryptedVotesOutputFilePath";

    public static final String JOB_VOTES_WITH_PROOF_OUTPUT_FILE_PATH = "votesWithProofOutputFilePath";

    public static final String JOB_AUDITABLE_VOTES_OUTPUT_FILE_PATH = "auditableVotesOutputFilePath";

    public static final String JOB_NAME_PREFIX = "decryption";

    public static final String JOB_WRITEIN_ALPHABET = "writeInAlphabet";
    
    /**
	 * Non-public constructor
	 */
	private Constants() {
	}
}
