/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

/**
 * A class to centralize all the factorized values validation failure codes.
 */
public class ValidationFailureCodes {

	public static final String RULE_VALIDATION = "RULE_VALIDATION";

    public static final String DUPLICATED_FACTOR = "DUPLICATED_FACTOR";

    public static final String NON_FACTORIZABLE_REMAINDER = "NON_FACTORIZABLE_REMAINDER";

    public static final String WRITE_IN_CONTENT_VIOLATION = "WRITE_IN_CONTENT_VIOLATION";
    
    /**
	 * Non-public constructor
	 */
	private ValidationFailureCodes() {
	}
}
