/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans.spring.batch;

import org.springframework.batch.core.JobParametersBuilder;

/**
 * This class overrides extends JobParametersBuilder to allow the addition of "sensitive value" job parameters
 */
public class SensitiveAwareJobParametersBuilder extends JobParametersBuilder {

    public JobParametersBuilder addSensitiveString(final String key, final String parameter) {
        SensitiveJobParameter sensitiveJobParameter = new SensitiveJobParameter(parameter, true);
        super.addParameter(key, sensitiveJobParameter);
        return this;
    }

    public JobParametersBuilder addSensitiveString(final String key, final String parameter, final boolean identifying) {
        SensitiveJobParameter sensitiveJobParameter = new SensitiveJobParameter(parameter, identifying);
        super.addParameter(key, sensitiveJobParameter);
        return this;
    }
}
