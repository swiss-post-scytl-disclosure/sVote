/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * A bean to encapsulate the partially correct vote that has to be audited manually.
 */
public class AuditableVote {

    private final BigInteger _compressed;
    
    private final List<BigIntegerFactor> _options;

    private final List<ValidationFailure> _failures;

    public AuditableVote(final Factorization factorization, final List<ValidationFailure> failures) {
        _compressed = factorization.getCompressed();
        _options = new ArrayList<>(factorization.getFactors());
        if (!BigInteger.ONE.equals(factorization.getRemainder())) {
            _options.add(new BigIntegerFactor(factorization.getRemainder(), 1));
        }
        _failures = failures;
    }

    public BigInteger getCompressed() {
        return _compressed;
    }
    
    public List<BigIntegerFactor> getOptions() {
        return _options;
    }

    public List<ValidationFailure> getFailures() {
        return _failures;
    }
}
