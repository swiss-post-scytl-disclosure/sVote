/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

import java.time.Instant;
import java.time.ZoneId;

public class StartValidationJobResponse {

    private final String jobId;
    private final String jobStatus;
    private final Instant createdAt;

    public StartValidationJobResponse(final String jobId, final String jobStatus, final Instant createdAt) {
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.createdAt = createdAt;
    }

    public String getJobId() {
        return jobId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public String getCreatedAt() {
        return createdAt.atZone(ZoneId.systemDefault()).toString();
    }
}
