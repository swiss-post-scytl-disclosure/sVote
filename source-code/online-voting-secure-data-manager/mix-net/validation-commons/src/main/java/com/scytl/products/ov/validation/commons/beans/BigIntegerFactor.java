/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

import java.math.BigInteger;

/**
 * Class represents the number {@link #_base} (usually prime)
 * and the number of times it appeared as factors in the decrypted vote {@link #_exponent}.
 */
public class BigIntegerFactor {

    private final BigInteger _base;

    private final int _exponent;

    public BigIntegerFactor(final String base) {
        this(base, 1);
    }

    public BigIntegerFactor(final String base, final int exponent) {
        this(new BigInteger(base), exponent);
    }

    public BigIntegerFactor(final BigInteger base, final int exponent) {
        this._base = base;
        this._exponent = exponent;
    }

    public BigInteger getBase() {
        return _base;
    }

    public int getExponent() {
        return _exponent;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BigIntegerFactor factor = (BigIntegerFactor) o;

        return _exponent == factor._exponent && (_base != null ? _base.equals(factor._base) : factor._base == null);

    }

    @Override
    public int hashCode() {
        int result = _base != null ? _base.hashCode() : 0;
        result = 31 * result + _exponent;
        return result;
    }
}
