/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

import java.math.BigInteger;
import java.util.List;

/**
 * A bean to encapsulate the decompression of decrypted vote result.
 */
public class Factorization {

    private final BigInteger _compressed;
    /**
     * Decompressed voting options.
     */
    private final List<BigIntegerFactor> factors;

    /**
     * The rest of decompression. If decompression was performed correct it should be equal 1.
     */
    private final BigInteger remainder;

    public Factorization(final BigInteger compressed, final List<BigIntegerFactor> factors, final BigInteger remainder) {
        this._compressed = compressed;
        this.factors = factors;
        this.remainder = remainder;
    }

    public BigInteger getCompressed() {
        return _compressed;
    }

    public List<BigIntegerFactor> getFactors() {
        return factors;
    }

    public BigInteger getRemainder() {
        return remainder;
    }
}
