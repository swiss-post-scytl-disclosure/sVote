/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;


import java.time.Instant;
import java.time.ZoneId;
import java.util.UUID;

import com.scytl.products.ov.validation.commons.progress.JobProgressDetails;

public class ValidationJobStatus {

    public static final ValidationJobStatus UNKNOWN =
        new ValidationJobStatus(new UUID(0, 0), Instant.MIN, "UNKNOWN", "UNKNOWN", JobProgressDetails.EMPTY,
            0, 0, 0);

    private final UUID jobId;
    private final String status;
    private final Instant startTime;
    private final String statusDetails;
    private final JobProgressDetails progressDetails;
    private final int auditableVotesCount;
    private final int decryptedVotesCount;
    private final int errorCount;

    public ValidationJobStatus(final UUID jobId, final Instant startTime, final String status,
                               final String statusDetails, final JobProgressDetails progressDetails,
                               final int auditableVotesCount, final int decryptedVotesCount, final int errorCount) {
        this.jobId = jobId;
        this.status = status;
        this.startTime = startTime;
        this.statusDetails = statusDetails;
        this.progressDetails = progressDetails;
        this.auditableVotesCount = auditableVotesCount;
        this.decryptedVotesCount = decryptedVotesCount;
        this.errorCount = errorCount;
    }

    public String getJobId() {
        return jobId.toString();
    }

    public String getStatus() {
        return status;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public String getStartedAt() {
        return startTime.atZone(ZoneId.systemDefault()).toString();
    }

    public JobProgressDetails getProgressDetails() {
        return progressDetails;
    }

    public int getAuditableVotesCount() {
        return auditableVotesCount;
    }

    public int getDecryptedVotesCount() {
        return decryptedVotesCount;
    }

    public int getErrorCount() {
        return errorCount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ValidationJobStatus{");
        sb.append("jobId='").append(jobId).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", startTime=").append(startTime);
        sb.append(", statusDetails='").append(statusDetails).append('\'');
        sb.append(", progressDetails=").append(progressDetails);
        sb.append(", auditableVotesCount=").append(auditableVotesCount);
        sb.append(", decryptedVotesCount=").append(decryptedVotesCount);
        sb.append(", errorCount=").append(errorCount);
        sb.append(", startedAt='").append(getStartedAt()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
