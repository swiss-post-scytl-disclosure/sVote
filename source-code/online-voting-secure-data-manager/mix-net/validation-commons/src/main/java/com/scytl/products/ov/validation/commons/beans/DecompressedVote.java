/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * A bean to encapsulate the correct decompressed vote.
 */
public final class DecompressedVote {

    private final List<BigIntegerFactor> options;
    private final List<String> writeIns;
    
    public DecompressedVote(List<BigIntegerFactor> options,List<String> writeIns) {
        this.options = options;
        this.writeIns = writeIns;
    }

	public DecompressedVote(List<BigIntegerFactor> options) {
		this.options = options;
		this.writeIns = new ArrayList<>();
    }
	
    public List<BigIntegerFactor> getOptions() {
        return options;
    }

    public List<String> getWriteIns() {
		return writeIns;
	}	
}
