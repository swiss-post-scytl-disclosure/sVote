/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons;

public class ValidationCommonsException extends RuntimeException {
  
	private static final long serialVersionUID = -3789397353121790105L;

	public ValidationCommonsException(Throwable cause) {
        super(cause);
    }
	
	public ValidationCommonsException(String message) {
        super(message);
    }
	
	public ValidationCommonsException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
