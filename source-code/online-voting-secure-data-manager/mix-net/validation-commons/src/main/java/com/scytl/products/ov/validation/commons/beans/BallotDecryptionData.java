/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class BallotDecryptionData {
	
	private final Factorization factorization;
	
	private final List<BigInteger> writeIns;

	public BallotDecryptionData(Factorization factorization, List<BigInteger> writeIns) {
		super();
		this.factorization = factorization;
		this.writeIns = writeIns;
	}

	public BallotDecryptionData(Factorization factorization) {
		super();
		this.factorization = factorization;
		writeIns = new ArrayList<>();
	}

	public Factorization getFactorization() {
		return factorization;
	}

	public List<BigInteger> getWriteIns() {
		return writeIns;
	}

}
