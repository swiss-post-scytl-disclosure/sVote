/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

/**
 * A class to centralize all the decryption error codes.
 */
public class ErrorCodes {
	
    public static final String DECRYPT_EXCEPTION = "001";

    public static final String BALLOT_NOT_ENRICHED = "002";

    public static final String UNKOWN_ERROR = "003";

    public static final String CONCURRENT_EXCEPTION = "004";

    public static final String SINATURE_VALIDATION_EXCEPTION = "005";

    /**
	 * Non-public constructor
	 */
	private ErrorCodes() {
	}
}
