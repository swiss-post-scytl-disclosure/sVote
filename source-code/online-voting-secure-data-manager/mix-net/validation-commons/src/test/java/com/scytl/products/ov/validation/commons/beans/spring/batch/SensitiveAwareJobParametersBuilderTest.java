/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans.spring.batch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;

/**
 * Tests of {@link SensitiveAwareJobParametersBuilder}.
 */
public class SensitiveAwareJobParametersBuilderTest {

    @Test
    public void testAddSensitiveString() {
        JobParameters parameters = new SensitiveAwareJobParametersBuilder()
            .addSensitiveString("sensitive", "value").toJobParameters();
        JobParameter parameter =
            parameters.getParameters().get("sensitive");
        assertEquals("value", parameter.getValue());
        assertFalse(parameter.toString().contains("value"));
    }
}
