/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.List;

import org.junit.Test;

/**
 * Tests of {@link AuditableVote}.
 */
public class AuditableVoteTest {
    private static final BigInteger COMPRESSED = BigInteger.TEN;

    private static final List<BigIntegerFactor> FACTORS =
        asList(new BigIntegerFactor("2", 1), new BigIntegerFactor("5", 1));

    private static final List<ValidationFailure> FAILURES =
        singletonList(new ValidationFailure("test", "test"));

    @Test
    public void testAuditableVoteRemainder1() {
        Factorization factorization =
            new Factorization(COMPRESSED, FACTORS, BigInteger.ONE);
        AuditableVote vote = new AuditableVote(factorization, FAILURES);
        assertEquals(COMPRESSED, vote.getCompressed());
        assertEquals(FACTORS, vote.getOptions());
        assertEquals(FAILURES, vote.getFailures());
    }

    @Test
    public void testAuditableVoteReminder3() {
        Factorization factorization =
            new Factorization(COMPRESSED, FACTORS, BigInteger.valueOf(3));
        AuditableVote vote = new AuditableVote(factorization, FAILURES);
        assertEquals(COMPRESSED, vote.getCompressed());
        assertEquals(FAILURES, vote.getFailures());
        assertEquals(asList(new BigIntegerFactor("2", 1),
            new BigIntegerFactor("5", 1), new BigIntegerFactor("3", 1)),
            vote.getOptions());
    }
}
