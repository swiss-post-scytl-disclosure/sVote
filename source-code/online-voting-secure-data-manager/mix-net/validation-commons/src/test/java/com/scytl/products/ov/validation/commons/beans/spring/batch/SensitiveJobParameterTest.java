/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.validation.commons.beans.spring.batch;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

/**
 * Tests of {@link SensitiveJobParameter}.
 */
public class SensitiveJobParameterTest {
    @Test
    public void testToString() {
        SensitiveJobParameter parameter =
            new SensitiveJobParameter("value", true);
        assertFalse(parameter.toString().contains("value"));
    }
}
