/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.impl;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import java.nio.file.Path;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

import com.scytl.products.ov.commons.path.PathResolver;
import com.scytl.products.ov.sdm.domain.model.ballotbox.BallotBoxRepository;
import com.scytl.products.ov.sdm.domain.model.electionevent.ElectionEventRepository;
import com.scytl.products.ov.sdm.domain.model.generator.DataGeneratorResponse;
import com.scytl.products.ov.sdm.domain.model.votingcardset.VotingCardSetRepository;
import com.scytl.products.ov.sdm.infrastructure.JsonConstants;

/**
 * JUnit for {@link VotingCardSetDataGeneratorServiceImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("test")
public class VotingCardSetDataGeneratorServiceImplTest {

    @Spy
    @InjectMocks
    private VotingCardSetDataGeneratorServiceImpl votingCardSetDataGeneratorService =
        new VotingCardSetDataGeneratorServiceImpl();

    private String id;

    private String electionEventId;

    @Mock
    private VotingCardSetRepository votingCardSetRepositoryMock;

    @Mock
    private ElectionEventRepository electionEventRepository;

    @Mock
    private BallotBoxRepository ballotBoxRepositoryMock;

    @Mock
    private PathResolver pathResolverMock;

    @Mock
    private Path configPathMock;

    @Mock
    private Path configPropertiesPathMock;

    @Mock
    private Path configElectionEventPathMock;

    @Mock
    private Path destinationBallotFilePathMock;

    @Mock
    private WebTarget webTarget;

    @Mock
    private Invocation.Builder builderMock;

    @Mock
    private Response response;

    @Test
    public void generateWithNull() {
        DataGeneratorResponse result = votingCardSetDataGeneratorService.generate(id, electionEventId);

        assertFalse(result.isSuccessful());
    }

    @Test
    public void generateWithEmpty() {
        id = "";

        DataGeneratorResponse result = votingCardSetDataGeneratorService.generate(id, electionEventId);
        assertFalse(result.isSuccessful());
    }

    @Test
    public void generateVCNotFound() {
        id = "12345";
        when(votingCardSetRepositoryMock.find(id)).thenReturn(JsonConstants.JSON_EMPTY_OBJECT);

        DataGeneratorResponse result = votingCardSetDataGeneratorService.generate(id, electionEventId);
        assertFalse(result.isSuccessful());
    }

}
