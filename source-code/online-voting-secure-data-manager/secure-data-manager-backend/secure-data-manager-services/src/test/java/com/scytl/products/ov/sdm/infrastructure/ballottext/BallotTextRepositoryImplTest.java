/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.ballottext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.JsonObject;

import com.scytl.products.ov.sdm.domain.model.status.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.orientechnologies.common.exception.OException;
import com.scytl.products.ov.commons.util.JsonUtils;
import com.scytl.products.ov.sdm.application.exception.DatabaseException;
import com.scytl.products.ov.sdm.infrastructure.DatabaseFixture;
import com.scytl.products.ov.sdm.infrastructure.JsonConstants;

/**
 * Tests of {@link BallotTextRepositoryImpl}.
 */
public class BallotTextRepositoryImplTest {
    private DatabaseFixture fixture;

    private BallotTextRepositoryImpl repository;

    @Before
    public void setUp() throws OException, IOException {
        fixture = new DatabaseFixture(getClass());
        fixture.setUp();
        repository =
            new BallotTextRepositoryImpl(fixture.databaseManager());
        repository.initialize();
        URL resource =
            getClass().getResource(getClass().getSimpleName() + ".json");
        fixture.createDocuments(repository.entityName(), resource);
    }

    @After
    public void tearDown() {
        fixture.tearDown();
    }

    @Test
    public void testUpdateSignedBallotText() {
        repository.updateSignedBallotText(
            "1dde9338ca074c6389407caba5c28d26en-GB", "signedBallotText");
        JsonObject object = JsonUtils.getJsonObject(
            repository.find("1dde9338ca074c6389407caba5c28d26en-GB"));
        assertEquals("signedBallotText", object
            .getString(JsonConstants.JSON_ATTRIBUTE_NAME_SIGNED_OBJECT));
        assertEquals(Status.SIGNED.name(), object.getString(JsonConstants.JSON_ATTRIBUTE_NAME_STATUS));
    }

    @Test(expected = DatabaseException.class)
    public void testUpdateSignedBallotTextNotFound() {
        repository.updateSignedBallotText("unknownBallotText",
            "signedBallotText");
    }

    @Test
    public void testListSignatures() {
        Map<String, Object> criteria = new HashMap<>();
        criteria.put("ballot.id", "1dde9338ca074c6389407caba5c28d26");
        criteria.put(JsonConstants.JSON_ATTRIBUTE_NAME_LOCALE, "fr-CH");
        List<String> signatures = repository.listSignatures(criteria);
        assertEquals(1, signatures.size());
        assertEquals(
            "eyJhbGciOiJQUzI1NiJ9.eyJvYmplY3RUb1NpZ24iOiJ7XCJiYWxsb3RcIjp7XCJpZ"
                + "FwiOlwiMWRkZTkzMzhjYTA3NGM2Mzg5NDA3Y2FiYTVjMjhkMjZcIn0sXCJsb2Nhb"
                + "GVcIjpcImZyLUNIXCIsXCJzdGF0dXNcIjpcIkxPQ0tFRFwiLFwidGV4dHNcIjp7X"
                + "CIxZGRlOTMzOGNhMDc0YzYzODk0MDdjYWJhNWMyOGQyNlwiOntcInRpdGxlXCI6X"
                + "CJUZXN0IGUyZSAyMDE2MTAxMyBDQyAzNDZcIixcImRlc2NyaXB0aW9uXCI6XCIwN"
                + "FwifX0sXCJpZFwiOlwiMWRkZTkzMzhjYTA3NGM2Mzg5NDA3Y2FiYTVjMjhkMjZmc"
                + "i1DSFwiLFwiZGV0YWlsc1wiOlwiMDkvMDEvMjAxNyAxNTo0NToxMVwiLFwic3luY"
                + "2hyb25pemVkXCI6XCJ0cnVlXCJ9In0.wbK1ht8AumfltCx_BVTcs9oZTfoLjUw0G"
                + "1N3zmG8rloXFFKXEsClI1z6YwZJ1g-uRdRyfZtSfP7mO3OKxl_B8zMAf1n-HRchT"
                + "1_PVgP6siWIO5hJ3KFEA5Ljby9IbZwVFwHmcht-4XurKVtmtdy0DBMQV3Zh5PCwq"
                + "NwIOxix0v_Pu6iHXg0-7N3UO0qM3GOjul82skxRFXDU3szpEEEy14yB7JSSrBwht"
                + "GTZR-1rTM84r0v3QzmY8PLS1QIfj1h7IjUX1hbcffsNrADh3rQQZwKOO3tcgyBcy"
                + "9MeOtrr5FIy61_2h_dJxnh4WCQkoWXd7J3setyAJzuvi1vmazo4NA",
            signatures.get(0));
    }

    @Test
    public void testListSignaturesNotFound() {
        Map<String, Object> criteria = new HashMap<>();
        criteria.put("ballot.id", "1dde9338ca074c6389407caba5c28d26");
        criteria.put(JsonConstants.JSON_ATTRIBUTE_NAME_LOCALE, "ru-RU");
        assertTrue(repository.listSignatures(criteria).isEmpty());
    }
}
