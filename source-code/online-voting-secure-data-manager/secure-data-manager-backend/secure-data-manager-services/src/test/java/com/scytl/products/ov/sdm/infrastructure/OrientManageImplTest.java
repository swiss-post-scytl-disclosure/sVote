/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.orientechnologies.orient.core.Orient;

/**
 * Tests of {@link OrientManager}.
 */
public class OrientManageImplTest {
    private Orient orient;

    private OrientManageImpl manager;

    @Before
    public void setUp() {
        orient = new LocalOrient();
        manager = new OrientManageImpl(orient);
    }

    @Test
    public void testIsActive() {
        assertFalse(manager.isActive());
        orient.startup();
        try {
            assertTrue(manager.isActive());
        } finally {
            orient.shutdown();
        }
        assertFalse(manager.isActive());
    }

    @Test
    public void testShutdown() {
        manager.shutdown();
        orient.startup();
        try {
            manager.shutdown();
            assertFalse(orient.isActive());
        } finally {
            orient.shutdown();
        }
    }

    private static class LocalOrient extends Orient {
    }
}
