/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.json.Json;
import javax.json.JsonReader;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.sdm.domain.model.status.Status;
import com.scytl.products.ov.sdm.domain.model.votingcardset.VotingCardSetRepository;

public class BaseVotingCardSetServiceTest {

    public static final String BALLOT_ID = "dd5bd34dcf6e4de4b771a92fa38abc11";

    public static final String BALLOT_BOX_ID = "4b88e5ec5fc14e14934c2ef491fe2de7";

    public static final String VOTING_CARD_SET_ID = "355";

    /**
     * Return a voting card set's JSON in a particular state
     *
     * @param status
     *            the status of the voting card set
     * @return the voting card set's JSON
     */
    protected String listVotingCardSets(String status) {
        return "{\"result\": [\n" + getVotingCardSetWithStatus(status) + "]}";
    }

    /**
     * @param status
     * @return
     */
    protected String getVotingCardSetWithStatus(String status) {
        return "{\n" + "\"id\":\"" + VOTING_CARD_SET_ID + "\",\n"
            + "\"defaultTitle\":\"Default Voting Card Set Title\",\n"
            + "\"defaultDescription\":\"Default Voting Card Set Description\",\n"
            + "\"verificationCardSetId\":\"9a0\",\n" + "\"alias\":\"Default VCS Alias\",\n"
            + "\"numberOfVotingCardsToGenerate\":10,\n" + "\"electionEvent\":{\n"
            + "\"id\":\"8324e5f17dc9475d891af3051a22e759\"\n" + "},\n" + "\"ballotBox\":{\n" + "\"id\":\""
            + BALLOT_BOX_ID + "\"\n" + "},\n" + "\t \"status\": \"" + status + "\"\n" + "}";
    }

    protected static Path getPathOfFileInResources(final Path path) throws URISyntaxException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL resource = classLoader.getResource(path.toString());
        return Paths.get(resource.toURI());
    }

    /**
     * For the voting card set that is returned by the mock repository, specify
     * its status.
     *
     * @param status
     *            the status
     * @throws ResourceNotFoundException 
     */
    protected void setStatusForVotingCardSetFromRepository(String status,
            VotingCardSetRepository votingCardSetRepositoryMock) throws ResourceNotFoundException {
        setUpVCSJson(status, votingCardSetRepositoryMock);
        when(votingCardSetRepositoryMock.list(any())).thenReturn(listVotingCardSets(status));
    }
    
    /**
     * Sets up the repository with a mock of a voting card set's JSON.
     *
     * @param status
     *            the status
     * @throws ResourceNotFoundException 
     */
    protected void setUpVCSJson(String status,
            VotingCardSetRepository votingCardSetRepositoryMock) throws ResourceNotFoundException {
        try (JsonReader jsonReader = Json.createReader(new StringReader(getVotingCardSetWithStatus(status)))) {
            when(votingCardSetRepositoryMock.getVotingCardSetJson(any(), any()))
                .thenReturn(jsonReader.readObject());
        }
    }

    /**
     * @param status
     * @return
     */
    protected String getBallotBoxWithStatus(Status status) {
        return "{ \"id\": \"" + BALLOT_BOX_ID
            + "\", \"defaultTitle\": \"Ballot Box Title\", \"defaultDescription\": \"Ballot Box Description\", \"alias\": \"Ballot Box Alias\", "
            + "\"dateFrom\": \"12/12/2012\", \"dateTo\": \"14/12/2012\","
            + "\"electionEvent\": { \"id\": \"314bd34dcf6e4de4b771a92fa3849d3d\"}," + "\"ballot\": { \"id\": \""
            + BALLOT_ID + "\"}," + "\"confirmationRequired\": true,"
            + "\"electoralAuthority\": { \"id\": \"hhhbd34dcf6e4de4b771a92fa38abhhh\"}," + "\"status\": \""
            + status.name() + "\"}";
    }
}
