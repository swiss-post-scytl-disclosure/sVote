/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.service.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.sdm.domain.model.EntityRepository;
import com.scytl.products.ov.sdm.infrastructure.JsonConstants;
import com.scytl.products.ov.sdm.infrastructure.service.ConfigurationEntityStatusService;

/**
 * JUnit for the class {@link ConfigurationEntityStatusServiceImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfigurationEntityStatusServiceImplTest {

	private String newStatus = "";

	private String id = "";

	private String updateResult = JsonConstants.JSON_EMPTY_OBJECT;

	@Mock
	private EntityRepository baseRepository;

	@InjectMocks
	private ConfigurationEntityStatusService configurationEntityStatusService = new ConfigurationEntityStatusServiceImpl();

	@Test
	public void updateEmptyObjectReturned() {
		when(baseRepository.update(anyString())).thenReturn(updateResult);

		assertTrue(JsonConstants.JSON_EMPTY_OBJECT.equals(configurationEntityStatusService
			.update(newStatus, id, baseRepository)));
	}

	@Test
	public void update() {
		updateResult = com.scytl.products.ov.sdm.infrastructure.JsonConstants.JSON_RESULT_EMPTY;
		when(baseRepository.update(anyString())).thenReturn(updateResult);

		assertTrue(com.scytl.products.ov.sdm.infrastructure.JsonConstants.JSON_RESULT_EMPTY
			.equals(configurationEntityStatusService.update(newStatus, id, baseRepository)));
	}
}
