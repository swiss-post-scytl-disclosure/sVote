/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.electionevent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.orientechnologies.common.exception.OException;
import com.scytl.products.ov.sdm.infrastructure.DatabaseFixture;

/**
 * Tests of {@link ElectionEventRepositoryImpl}.
 */
public class ElectionEventRepositoryImplTest {
    private DatabaseFixture fixture;

    private ElectionEventRepositoryImpl repository;

    @Before
    public void setUp() throws OException, IOException {
        fixture = new DatabaseFixture(getClass());
        fixture.setUp();
        repository =
            new ElectionEventRepositoryImpl(fixture.databaseManager());
        repository.initialize();
        URL resource =
            getClass().getResource(getClass().getSimpleName() + ".json");
        fixture.createDocuments(repository.entityName(), resource);
    }

    @After
    public void tearDown() {
        fixture.tearDown();
    }

    @Test
    public void testGetElectionEventAlias() {
        assertEquals("legislative2017T2", repository
            .getElectionEventAlias("101549c5a4a04c7b88a0cb9be8ab3df6"));
    }

    @Test
    public void testGetElectionEventAliasNotFound() {
        assertTrue(repository.getElectionEventAlias("unknownElectionEvent")
            .isEmpty());
    }

    @Test
    public void testListIds() {
        List<String> ids = repository.listIds();
        assertEquals(2, ids.size());
        assertTrue(ids.contains("101549c5a4a04c7b88a0cb9be8ab3df6"));
        assertTrue(ids.contains("101549c5a4a04c7b88a0cb9be8ab3df7"));
    }
}
