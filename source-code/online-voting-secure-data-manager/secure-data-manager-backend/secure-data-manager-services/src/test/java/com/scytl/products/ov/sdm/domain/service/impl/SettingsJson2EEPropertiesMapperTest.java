/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * JUnit for the class {@link SettingsJson2EEPropertiesMapper}
 */
@RunWith(MockitoJUnitRunner.class)
public class SettingsJson2EEPropertiesMapperTest {

	private SettingsJson2EEPropertiesMapper settingsJson2EEPropertiesMapper = new SettingsJson2EEPropertiesMapper();

	private String electionEventAsJson = null;

	@Test
	public void createConfigPropertiesFromJson4Null() {
		Properties properties = settingsJson2EEPropertiesMapper.createConfigPropertiesFromJson(electionEventAsJson);

		assertTrue(properties.isEmpty());
	}

	@Test
	public void createConfigPropertiesFromJson4Empty() {
		electionEventAsJson = "";
		Properties properties = settingsJson2EEPropertiesMapper.createConfigPropertiesFromJson(electionEventAsJson);

		assertTrue(properties.isEmpty());
	}

	@Test
	public void createConfigPropertiesFromJson4Blank() {
		electionEventAsJson = " ";
		Properties properties = settingsJson2EEPropertiesMapper.createConfigPropertiesFromJson(electionEventAsJson);

		assertTrue(properties.isEmpty());
	}

	@Test
	public void createConfigPropertiesFromJson() {
		final int AUTH_TOKEN_EXP_TIME = 1600;
		final int CHALLENGE_LENGTH = 16;
		final int MAX_NR_ATTEMPTS = 5;
		electionEventAsJson = "{\"id\":\"db033b3f729c45719db8aba15d24043c\"," + "\"defaultTitle\":\"Election Event Title\","
			+ "\"defaultDescription\":\"Election Event Description\"," + "\"alias\":\"Election Event Alias\","
			+ "\"dateFrom\":\"12/12/2012 10:30\"," + "\"dateTo\":\"14/12/2012 10:30\"," + "\"settings\":{"
			+ "\"certificatesValidityPeriod\":1," + "\"challengeLength\":" + CHALLENGE_LENGTH + ","
			+ "\"challengeResponseExpirationTime\":2000," + "\"authTokenExpirationTime\":" + AUTH_TOKEN_EXP_TIME + ","
			+ "\"numberVotesPerVotingCard\":1," + "\"numberVotesPerAuthToken\":1," + "\"maximumNumberOfAttempts\":"
			+ MAX_NR_ATTEMPTS + "," + "\"encryptionParameters\":{" + "\"p\":\"123\"," + "\"q\":\"456\"," + "\"g\":\"3\""
			+ "}" + "}}}";
		Properties properties = settingsJson2EEPropertiesMapper.createConfigPropertiesFromJson(electionEventAsJson);

		assertFalse(properties.isEmpty());
		assertTrue(String.valueOf(AUTH_TOKEN_EXP_TIME)
			.equals(properties.getProperty(SettingsJson2EEPropertiesMapper.CONFIG_PROPERTY_NAME_AUTH_TOKEN_EXP_TIME)));
		assertTrue(String.valueOf(CHALLENGE_LENGTH)
			.equals(properties.getProperty(SettingsJson2EEPropertiesMapper.CONFIG_PROPERTY_NAME_CHALLENGE_LENGTH)));
		assertTrue(String.valueOf(MAX_NR_ATTEMPTS)
			.equals(properties.getProperty(SettingsJson2EEPropertiesMapper.CONFIG_PROPERTY_NAME_MAX_NUMBER_OF_ATTEMPTS)));
		assertFalse(String.valueOf(CHALLENGE_LENGTH)
			.equals(properties.getProperty(SettingsJson2EEPropertiesMapper.CONFIG_PROPERTY_NAME_AUTH_TOKEN_EXP_TIME)));
		assertFalse(String.valueOf(MAX_NR_ATTEMPTS)
			.equals(properties.getProperty(SettingsJson2EEPropertiesMapper.CONFIG_PROPERTY_NAME_CHALLENGE_LENGTH)));
		assertFalse(String.valueOf(AUTH_TOKEN_EXP_TIME)
			.equals(properties.getProperty(SettingsJson2EEPropertiesMapper.CONFIG_PROPERTY_NAME_MAX_NUMBER_OF_ATTEMPTS)));
	}
}
