/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.impl;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import mockit.Deencapsulation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.commons.path.PathResolver;
import com.scytl.products.ov.sdm.domain.model.electionevent.ElectionEventRepository;
import com.scytl.products.ov.sdm.infrastructure.JsonConstants;

/**
 * Junit for the class {@link ElectionEventDataGeneratorServiceImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class ElectionEventDataGeneratorServiceImplTest {

	@Mock
	private PathResolver pathResolverMock;

	@Mock
	private Path configPathMock;

	@Mock
	private Path configPropertiesPathMock;

	@Mock
	private ElectionEventRepository electionEventRepositoryMock;

	@Mock
	private WebTarget webTarget;

	@Mock
	private Invocation.Builder builderMock;

	@Mock
	private Response responseMock;

	private String electionEventId;

	@InjectMocks
	@Spy
	private ElectionEventDataGeneratorServiceImpl electionEventDataGeneratorService =
		new ElectionEventDataGeneratorServiceImpl();

	@Before
	public void setUp() {
		Deencapsulation.setField(electionEventDataGeneratorService, "secureLogger", Mockito.mock(SecureLoggingWriter.class));
	}

	@Mock
	private OutputStream outputStreamMock;

	@Test
	public void generateWithIdNull() throws IOException {
		electionEventId = null;

		assertFalse(electionEventDataGeneratorService.generate(electionEventId).isSuccessful());
	}

	@Test
	public void generateWithIdEmpty() throws IOException {
		electionEventId = "";

		assertFalse(electionEventDataGeneratorService.generate(electionEventId).isSuccessful());
	}

	@Test
	public void generateMakePathThrowIOException() throws IOException {
		electionEventId = "123";

		when(pathResolverMock.resolve(anyString())).thenReturn(configPathMock);
		doThrow(new IOException()).when(electionEventDataGeneratorService).makePath(configPathMock);

		assertFalse(electionEventDataGeneratorService.generate(electionEventId).isSuccessful());
	}

	@Test
	public void generateElectionEventNotFound() throws IOException {
		electionEventId = "123";

		when(pathResolverMock.resolve(anyString())).thenReturn(configPathMock);
		doReturn(configPathMock).when(electionEventDataGeneratorService).makePath(configPathMock);
		when(configPathMock.resolve(anyString())).thenReturn(configPropertiesPathMock);
		when(electionEventRepositoryMock.find(electionEventId)).thenReturn(JsonConstants.JSON_EMPTY_OBJECT);

		assertFalse(electionEventDataGeneratorService.generate(electionEventId).isSuccessful());
	}

}
