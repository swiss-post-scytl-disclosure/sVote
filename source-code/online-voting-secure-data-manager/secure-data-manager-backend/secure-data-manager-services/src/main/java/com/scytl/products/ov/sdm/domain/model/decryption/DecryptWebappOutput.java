/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.decryption;

import java.util.ArrayList;
import java.util.List;

public class DecryptWebappOutput {

    private List<DecryptError> errors = new ArrayList<>();

    private String outputProofsFilePath;

    private String outputFilePath;

    private String outputAuditableVotesFilePath;

	/**
	 * Returns the current value of the field errors.
	 *
	 * @return Returns the errors.
	 */
	public List<DecryptError> getErrors() {
		return errors;
	}

	/**
	 * Sets the value of the field errors.
	 *
	 * @param errors The errors to set.
	 */
	public void setErrors(List<DecryptError> errors) {
		this.errors = errors;
	}

	/**
	 * Returns the current value of the field outputProofsFilePath.
	 *
	 * @return Returns the outputProofsFilePath.
	 */
	public String getOutputProofsFilePath() {
		return outputProofsFilePath;
	}

	/**
	 * Sets the value of the field outputProofsFilePath.
	 *
	 * @param outputProofsFilePath The outputProofsFilePath to set.
	 */
	public void setOutputProofsFilePath(String outputProofsFilePath) {
		this.outputProofsFilePath = outputProofsFilePath;
	}

	/**
	 * Returns the current value of the field outputFilePath.
	 *
	 * @return Returns the outputFilePath.
	 */
	public String getOutputFilePath() {
		return outputFilePath;
	}

	/**
	 * Sets the value of the field outputFilePath.
	 *
	 * @param outputFilePath The outputFilePath to set.
	 */
	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

	/**
	 * Returns the current value of the field outputAuditableVotesFilePath.
	 *
	 * @return Returns the outputAuditableVotesFilePath.
	 */
	public String getOutputAuditableVotesFilePath() {
		return outputAuditableVotesFilePath;
	}

	/**
	 * Sets the value of the field outputAuditableVotesFilePath.
	 *
	 * @param outputAuditableVotesFilePath The outputAuditableVotesFilePath to set.
	 */
	public void setOutputAuditableVotesFilePath(String outputAuditableVotesFilePath) {
		this.outputAuditableVotesFilePath = outputAuditableVotesFilePath;
	}


}