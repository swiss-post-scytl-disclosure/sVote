/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.constant;

public class Constants {

	public static final String FOLDER_NAME_SDM = "sdm";

    public static final String FOLDER_NAME_CUSTOMER_KEYS = "customerKeys";

    public static final String NULL_ELECTION_EVENT_ID = "";

    public static final String LOGGING_USER = "SDM";
    
    /**
	 * Non-public constructor
	 */
	private Constants() {
	}
}
