/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.electoralauthority;

import java.util.List;

import com.scytl.products.ov.sdm.application.exception.DatabaseException;
import com.scytl.products.ov.sdm.domain.model.EntityRepository;

/**
 * Interface for operations on the repository of electoral authority.
 */
public interface ElectoralAuthorityRepository extends EntityRepository {

    /**
     * Updates the related ballot box(es).
     *
     * @param list
     *            The list of identifiers of the electoral authorities where to
     *            update the identifiers of the related ballot boxes.
     */
    void updateRelatedBallotBox(List<String> list);

    /**
     * Lists authorities matching which belong to the specified election event.
     *
     * @param electionEventId
     *            the election event identifier
     * @return the authorities in JSON format
     * @throws DatabaseException
     *             failed to list the authorities
     */
    String listByElectionEvent(String electionEventId)
            throws DatabaseException;
}
