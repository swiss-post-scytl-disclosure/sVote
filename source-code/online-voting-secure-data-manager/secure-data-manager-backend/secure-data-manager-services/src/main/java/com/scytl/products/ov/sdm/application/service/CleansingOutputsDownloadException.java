/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

public class CleansingOutputsDownloadException extends Exception {

    public CleansingOutputsDownloadException(String message) {
        super(message);
    }

    public CleansingOutputsDownloadException(String message, Throwable cause) {
        super(message, cause);
    }
}
