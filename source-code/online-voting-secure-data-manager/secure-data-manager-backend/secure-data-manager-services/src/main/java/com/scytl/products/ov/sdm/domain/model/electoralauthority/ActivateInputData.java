/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.electoralauthority;

public class ActivateInputData {

	private String ballotBoxID;

	public String getBallotBoxID() {
		return ballotBoxID;
	}

	public void setBallotBoxID(String ballotBoxID) {
		this.ballotBoxID = ballotBoxID;
	}
}
