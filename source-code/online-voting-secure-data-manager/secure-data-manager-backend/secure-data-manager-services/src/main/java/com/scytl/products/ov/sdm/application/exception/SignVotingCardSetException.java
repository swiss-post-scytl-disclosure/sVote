/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class SignVotingCardSetException extends Exception {

    private static final long serialVersionUID = 4468397518642183041L;

    public SignVotingCardSetException(final String message) {
        super(message);
    }

    public SignVotingCardSetException(final String message, final Throwable e) {
        super(message, e);
    }

}
