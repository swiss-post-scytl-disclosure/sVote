/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class ChoiceCodesComputationServiceException extends RuntimeException {

	private static final long serialVersionUID = 4050170916637297678L;

	public ChoiceCodesComputationServiceException(Throwable cause) {
		super(cause);
	}
	
	public ChoiceCodesComputationServiceException(String message) {
		super(message);
	}
	
	public ChoiceCodesComputationServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
