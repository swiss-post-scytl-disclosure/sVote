/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.clients;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

import javax.validation.constraints.NotNull;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public interface ExtendedAuthenticationClient {

    String TENANT_ID_PARAM = "tenantId";
    String ELECTION_EVENT_ID_PARAM = "electionEventId";
    String VOTING_CARD_SET_ID_PARAM = "votingCardSetId";
    String ADMIN_BOARD_ID_PARAM = "adminBoardId";

    @POST("extendedauthentication/tenant/{tenantId}/electionevent/{electionEventId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveExtendedAuthenticationData(@Path(TENANT_ID_PARAM) String tenantId,
                                      @Path(ELECTION_EVENT_ID_PARAM) String electionEventId,
                                      @Path(ADMIN_BOARD_ID_PARAM) String adminBoardId,
                                      @NotNull @Body RequestBody body);
}
