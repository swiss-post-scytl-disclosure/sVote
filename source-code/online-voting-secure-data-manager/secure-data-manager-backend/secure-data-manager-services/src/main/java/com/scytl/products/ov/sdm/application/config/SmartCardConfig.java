/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 9/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.sdm.application.config;

/**
 * Enum representing the different status allowed for handling the smart cards
 */
public enum SmartCardConfig {

    FILE(false),SMART_CARD(true) ;


    private boolean smartCardEnabled;

    SmartCardConfig(boolean smartCardEnabled){
        this.smartCardEnabled = smartCardEnabled;
    }

    public boolean isSmartCardEnabled(){
        return smartCardEnabled;
    }
}
