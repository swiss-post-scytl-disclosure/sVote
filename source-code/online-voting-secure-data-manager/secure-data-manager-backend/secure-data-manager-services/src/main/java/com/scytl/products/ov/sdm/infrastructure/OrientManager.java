/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import com.orientechnologies.orient.core.Orient;

/**
 * OrientManager is used to manage the life-cycle of {@link Orient}.
 * <p>
 * Implementation must be thread-safe.
 */
public interface OrientManager {
    /**
     * Returns if the underlying {@link Orient} instance is active.
     *
     * @return the underlying {@link Orient} instance is active.
     */
    boolean isActive();

    /**
     * Shutdowns the underlying {@link Orient} instance.
     */
    void shutdown();
}
