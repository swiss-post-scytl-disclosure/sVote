/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

public class AdminBoardServiceException extends RuntimeException {
  
	private static final long serialVersionUID = -5601384812692612498L;

	public AdminBoardServiceException(Throwable cause) {
        super(cause);
    }
	
	public AdminBoardServiceException(String message) {
        super(message);
    }
	
	public AdminBoardServiceException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
