/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import com.orientechnologies.orient.core.metadata.security.OUser;

/**
 * Factory of {@link DatabaseManager}.
 * <p>
 * Implementation must be thread-safe.
 */
public interface DatabaseManagerFactory {
    /**
     * The default user name.
     */
    String DEFAULT_USERNAME = OUser.ADMIN;

    /**
     * The default password.
     */
    String DEFAULT_PASSWORD = OUser.ADMIN;

    /**
     * Creates a new database manager for given URL and the default user. This
     * is a shortcut for
     * {@code newDatabaseManager(url, DEFAULT_USERNAME, DEFAULT_PASSWORD)}.
     *
     * @param url
     *            the URL
     * @return the database manager
     */
    DatabaseManager newDatabaseManager(String url);

    /**
     * Creates a new database manager for given URL and user.
     *
     * @param url
     *            the URL
     * @param username
     *            the user name
     * @param password
     *            the password
     * @return the database manager
     */
    DatabaseManager newDatabaseManager(String url, String username,
            String password);
}
