/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

public enum SdmSecureLogEvent implements LogEvent {

    ADMIN_BOARD_ACTIVATING("", "SDM", "0000", "Administration Board activating..."),
    ADMIN_BOARD_ACTIVATING_FAILED("", "SDM", "1403", "Administration Board activation failed"),
    ADMIN_BOARD_ACTIVATED_SUCCESSFULLY("", "SDM", "0000", "Administration Board activation has been completed successfully"),
    ADMIN_BOARD_CONSTITUTING("","SDM","0000","Administration Board constituting ..."),
    ADMIN_BOARD_CONSTITUTION_COMPLETED_SUCCESSFULLY("","SDM","0000","Administration Board constitution has been completed successfully"),
    ADMIN_BOARD_CONSTITUTION_FAILED("","SDM","1401","Administration Board constitution failed"),
    ADMIN_BOARD_CERTIFICATE_UPLOADED_SUCCESSFULLY("","SDM","0000","The administration board certificate was successfully uploaded"),
    ADMIN_BOARD_CERTIFICATE_UPLOADING_FAILED("","SDM","1428","The administration board certificate uploading failed"),
    AP_CONFIG_DATA_DOWNLOADED_SUCCESSFULLY("","SDM","0000","Configuration data from the administration portal was successfully downloaded and saved"),
    AP_CONFIG_DATA_DOWNLOADING_FAILED("","SDM","1419","Error downloading data from administrator portal"),
    AUTHENTICATION_CONTEXT_DATA_UPLOADED_SUCCESSFULLY("","SDM","0000", "Authentication context data was uploaded successfully" ),
    AUTHENTICATION_CONTEXT_DATA_UPLOADING_FAILED("","SDM","1421","Authentication context data was not uploaded"),
    BALLOT_BOX_DECRYPTING_COMPLETED_SUCCESSFULLY("","SDM","0000","Ballot Box Decrypting has been completed successfully"),
    BALLOT_BOX_DECRYPTING_FAILED("","SDM","1416","Ballot Box Decrypting failed"),
    BALLOT_BOX_DOWNLOADED_SUCCESSFULLY("","SDM","0000","Ballot Box was downloaded successfully"),
    BALLOT_BOX_DOWNLOADING_FAILED("","SDM","1425","Ballot Box downloading failed"),
    BALLOT_BOX_KEY_TRANSLATION_UPLOADED_SUCCESSFULLY("","SDM","0000","Ballot Box key translation uploaded successfully"),
    BALLOT_BOX_KEY_TRANSLATION_UPLOADING_FAILED("","SDM","1442","Error occurred while uploading Ballot Box key translation"),
    BALLOT_BOX_SIGNED_SUCCESSFULLY("","SDM","0000","Ballot Box signing has been completed successfully"),
    BALLOT_BOX_SIGNING_FAILED("","SDM","1411","Ballot Box signing failed"),
    BALLOT_BOX_MIXED_SUCCESSFULLY("","SDM","0000","Ballot Box Mixing has been completed successfully"),
    BALLOT_BOX_MIXING_FAILED("","SDM","1415","Ballot Box Mixing failed"),
    BALLOT_BOX_NOT_DOWNLOADED("","SDM","1413","Ballot box was not downloaded"),
    BALLOT_SIGNED_SUCCESSFULLY("","SDM","0000","Signing ballot has been completed successfully"),
    BALLOT_BOX_CHECK_MIXING_STATUS("","SDM","0000","Checking if ballot box is ready"),
    CHECKING_COMPUTED_VALUES("","SDM","0000", "Checking if computed values are ready"),
    CLEANSING_BALLOT_BOX("","SDM","0000","Cleansing Ballot Box..."),
    CLEANSING_BALLOT_BOX_COMPLETED_SUCCESSFULLY("","SDM","0000","Cleansing Ballot Box has been completed successfully"),
    CLEANSING_BALLOT_BOX_FAILED("","SDM","1414","Cleansing Ballot Box failed"),
    CLOSING_SDM("","SDM","0000","Closing SDM..."),
    CODES_MAPPING_UPLOADED_SUCCESSFULLY("","SDM","0000","Codes mapping file and its signature were successfully uploaded"),
    CODES_MAPPING_UPLOADING_FAILED("","SDM","1438","An error occurred while uploading codes mapping file and its signature"),
    CONSTITUTING_ELECTORAL_AUTHORITY("","SDM","0000","Constituting Electoral Authority..."),
    CREDENTIAL_DATA_UPLOADED_SUCCESSFULLY("","SDM","0000","Credential data file and its signature were successfully uploaded"),
    CREDENTIAL_DATA_UPLOADING_FAILED("","SDM","1404","An error occurred while uploading credential data file and its signature"),
    DATABASE_EXPORTED_TO_DUMP_FILE_SUCCESSFULLY("","SDM","0000","Database export to dump file has been completed successfully"),
    DATABASE_EXPORTING_TO_DUMP_FILE_FAILED("","SDM","1426","Database export to dump file failed"),
    DECRYPTING_BALLOT_BOX("","SDM","0000","Decrypting Ballot Box..."),
    DOWNLOADING_BALLOT_BOX("","SDM","0000","Downloading ballot box..."),
    DUMP_FILE_NOT_EXISTS("","SDM","1441","Dump file to import does not exists"),
    DUMP_FILE_IMPORTED_TO_DB_SUCCESSFULLY("","SDM","0000","Dump file import to database has been completed successfully"),
    DUMP_FILE_IMPORTING_TO_DB_FAILED("","SDM","1436","Dump file import to database failed"),
    ELECTION_EVENT_KEY_TRANSLATION_UPLOADED_SUCCESSFULLY("","SDM","0000","Election Event key translation uploaded successfully"),
    ELECTION_EVENT_KEY_TRANSLATION_UPLOADING_FAILED("","SDM","1431","Error occurred while uploading Election Event key translation"),
    ELECTION_INFORMATION_CONTEXT_DATA_UPLOADED_SUCCESSFULLY("","SDM","0000","Election Information context data was uploaded successfully"),
    ELECTION_INFORMATION_CONTEXT_DATA_UPLOADING_FAILED("","SDM","1422","Election Information context data was not uploaded"),
    ELECTORAL_AUTHORITY_CONSTITUTION_COMPLETED_SUCCESSFULLY("","SDM","0000","Electoral Authority constitution has been completed successfully"),
    ELECTORAL_AUTHORITY_CONSTITUTION_FAILED("","SDM","1409","Electoral Authority constitution failed"),
    ELECTORAL_AUTHORITY_SIGNING_COMPLETED_SUCCESSFULLY("","SDM","0000","Electoral Authority signing has been completed successfully"),
    ELECTORAL_AUTHORITY_SIGNING_FAILED("","SDM","1410","Electoral Authority signing failed"),
    EXTENDED_AUTHENTICATION_UPLOADED_SUCCESSFULLY("","SDM","0000","Extended authentication file and its signature were successfully uploaded"),
    EXTENDED_AUTHENTICATION_UPLOADING_FAILED("","SDM","1440","An error occurred while uploading extended authentication file and its signature"),
    FAILED_VOTES_DOWNLOADED_SUCCESSFULLY("","SDM","0000","Failed votes were downloaded successfully"),
    FAILED_VOTES_DOWNLOADING_FAILED("","SDM","1448","Failed votes downloading failed"),
    FILE_COPIED_SUCCESSFULLY("","SDM","0000","File has been copied"),
    FILE_COPYING_FAILED("","SDM","1427","Error occurred during copying a file"),
    GENERATING_PRE_VOTING_OUTPUT("","SDM","0000","Generating Pre-Voting Output..."),
    GENERATING_VCS("","SDM","0000","Generating Voting Card Set..."),
    MIXING_BALLOT_BOX("","SDM","0000","Mixing Ballot Box..."),
    PRE_VOTING_OUTPUT_GENERATED_SUCCESSFULLY("","SDM","0000","Pre-Voting Output generating has been completed successfully"),
    PRE_VOTING_OUTPUT_GENERATING_FAILED("","SDM","1408","Pre-Voting Output generating failed"),
    SDM_CLOSED_SUCCESSFULLY("","SDM","0000","SDM was closed successfully"),
    SDM_SYNCHRONIZING_WITH_AP("","SDM","0000","Secure Data Manager synchronizing with Admin Portal..."),
    SDM_SYNCHRONIZED_WITH_AP_SUCCESSFULLY("","SDM","0000","Secure Data Manager synchronization with Admin Portal has been completed successfully"),
    SDM_SYNCHRONIZATION_WITH_AP_FAILED("","SDM","1400","Secure Data Manager synchronization with Admin Portal failed"),
    SECURING_EE("","SDM","0000","Securing Election Event..."),
    SECURING_EE_COMPLETED_SUCCESSFULLY("","SDM","0000","Securing Election Event has been completed successfully"),
    SECURING_EE_FAILED("","SDM","1402","Securing Election Event failed"),
    SIGNED_ELECTORAL_AUTHORITY_UPLOADED_TO_VV_SUCCESSFULLY("","SDM","0000","Signed electoral authority has been uploaded successfully to Vote Verification context"),
    SIGNED_ELECTORAL_AUTHORITY_UPLOADING_TO_VV_FAILED("","SDM","1434","Error occurred while uploading a signed electoral authority to Vote Verification context"),
    SIGNED_ELECTORAL_AUTHORITY_UPLOADED_TO_EI_SUCCESSFULLY("","SDM","0000","Signed electoral authority has been uploaded successfully to Election Information context"),
    SIGNED_ELECTORAL_AUTHORITY_UPLOADING_TO_EI_FAILED("","SDM","1435","Error occurred while uploading a signed electoral authority to Election Information context"),
    SIGNED_BALLOT_AND_TEXT_UPLOADED_SUCCESSFULLY("","SDM","0000","The signed ballot and signed ballot text was uploaded successfully"),
    SIGNED_BALLOT_AND_TEXT_UPLOADING_FAILED("","SDM","1429","Error occurred while uploading the signed ballot and signed ballot text"),
    SIGNED_BALLOT_BOX_CONFIG_UPLOADED_SUCCESSFULLY("","SDM","0000","Signed ballot box configuration was uploaded successfully"),
    SIGNED_BALLOT_BOX_UPLOADING_FAILED("","SDM","1430","Error occurred while uploading the signed ballot box"),
    SIGNING_BALLOT("","SDM","0000","Signing ballot..."),
    SIGNING_BALLOT_BOX("","SDM","0000","Signing Ballot Box..."),
    SIGNING_ELECTORAL_AUTHORITY("","SDM","0000","Signing Electoral Authority..."),
    SIGNING_VCS("","SDM","0000","Signing Voting Card Set..."),
    SIGNING_BALLOT_FAILED("","SDM","1405","Signing ballot failed"),
    SUCCESSFUL_VOTES_DOWNLOADED_SUCCESSFULLY("","SDM","0000","Successful votes were downloaded successfully"),
    SUCCESSFUL_VOTES_DOWNLOADING_FAILED("","SDM","1447","Successful votes downloading failed"),
    SYNCHRONIZING_WITH_VP("","SDM","0000","Synchronizing with Voting Portal..."),
    SYNCHRONIZING_WITH_VP_COMPLETED_SUCCESSFULLY("","SDM","0000","Synchronizing with Voting Portal has been completed successfully"),
    SYNCHRONIZING_WITH_VP_FAILED("","SDM","1412","Synchronizing with Voting Portal failed"),
    TALLYING_RESULTS("","SDM","0000","Tallying results..."),
    TALLYING_RESULTS_COMPLETED_SUCCESSFULLY("","SDM","0000","Tallying results has been completed successfully"),
    TALLYING_RESULTS_FAILED("","SDM","1417","Tallying results failed"),
    PRECOMPUTING_VCS("","SDM","0000","Precomputing Voting Card Set..."),
    VCS_PRECOMPUTED_SUCCESSFULLY("","SDM","0000","Voting Card Set pre-computation has been completed successfully"),
    VCS_PRECOMPUTATION_FAILED("","SDM","1443","Voting Card Set pre-computation failed"),
    SENDING_VCS_TO_COMPUTE("","SDM","0000","Sending Voting Card Set to compute service..."),
    COMPUTING_VCS("","SDM","0000","Computing Voting Card Set..."),
    VCS_SENT_TO_COMPUTATION_SUCCESSFULLY("","SDM","0000","Voting Card Set sent to computation successfully"),
    VCS_SENDING_TO_COMPUTATION_FAILED("","SDM","1444","Voting Card Set sent to computation failed"),
    VCS_COMPUTED_SUCCESSFULLY("","SDM","0000","Voting Card Set computation has been completed successfully"),
    VCS_COMPUTATION_FAILED("","SDM","1445","Voting Card Set computation failed"),
    DOWNLOADING_VCS("","SDM","0000","Downloading Voting Card Set..."),
    VCS_DOWNLOADED_SUCCESSFULLY("","SDM","0000","Voting Card Set has been successfully downloaded"),
    VCS_DOWNLOAD_FAILED("","SDM","1446","Voting Card Set download failed"),
    VCS_GENERATED_SUCCESSFULLY("","SDM","0000","Voting Card Set generating has been completed successfully"),
    VCS_GENERATION_FAILED("","SDM","1406","Voting Card Set generating failed"),
    VCS_SIGNED_SUCCESSFULLY("","SDM","0000","Voting Card Set signing has been completed successfully"),
    VCS_SIGNING_FAILED("","SDM","1407","Voting Card Set signing failed"),
    VERIFICATION_CARD_DATA_UPLOADED_SUCCESSFULLY("","SDM","0000","Verification card data file and its signature were successfully uploaded"),
    VERIFICATION_CARD_DATA_UPLOADING_FAILED("","SDM","1439","An error occurred while uploading verification card data file and its signature"),
    VOTER_INFORMATION_UPLOADED_SUCCESSFULLY("","SDM","0000","Voter information file and its signature were successfully uploaded"),
    VOTER_INFORMATION_FILE_UPLOADING_FAILED("","SDM","1420","An error occurred while uploading voter information file and its signature");

    private final String layer;

    private final String action;

    private final String outcome;

    private final String info;

    SdmSecureLogEvent(final String layer, final String action, final String outcome, final String info) {
        this.layer = layer;
        this.action = action;
        this.outcome = outcome;
        this.info = info;
    }

    @Override
    public String getLayer() {
        return layer;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public String getOutcome() {
        return outcome;
    }

    @Override
    public String getInfo() {
        return info;
    }
}
