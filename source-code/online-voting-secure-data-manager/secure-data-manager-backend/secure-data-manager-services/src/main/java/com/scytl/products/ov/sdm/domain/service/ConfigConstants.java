/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service;

/**
 * Contains the constants used at the service level.
 */
public final class ConfigConstants {

    /**
     * Name of the SDM directory.
     */
    public static final String SDM_DIR_NAME = "sdm";

    /**
     * Name of the sdmConfig directory.
     */
    public static final String SDM_CONFIG_DIR_NAME = "sdmConfig";

    /**
     * The name of the config directory
     */
    public static final String CONFIG_DIR_NAME = "config";

    /**
     * The name of the base directory where all the configuration files are.
     */
    public static final String CONFIG_FILES_BASE_DIR = SDM_DIR_NAME + "/config";

    /**
     * The name of the base directory where all the configuration files are.
     */
    public static final String SYSTEM_TENANT_KEYS = SDM_DIR_NAME + "/systemKeys";

    /**
     * The name of the configuration properties file.
     */
    public static final String CONFIG_FILE_NAME_PROPERTIES = "config.properties";

    /**
     * The name of the data base dump file
     */
    public static final String DBDUMP_FILE_NAME = "db_dump.json";

    /**
     * The name of the file containing the encryption parameters.
     */
    public static final String CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_JSON = "encryptionParameters.json";

    /**
     * The name of the file containing the ballot in json format.
     */
    public static final String CONFIG_FILE_NAME_BALLOT_JSON = "ballot.json";

    /**
     * The name of the file containing the ballot in json format.
     */
    public static final String CONFIG_FILE_NAME_BALLOT_BOX_JSON = "ballotBox.json";

    /**
     * The name of the file containing the electoral authority in json format.
     */
    public static final String CONFIG_FILE_NAME_ELECTORAL_AUTHORITY_JSON = "electoralAuthority.json";

    /**
     * The name of the file containing the electoral authority signature.
     */
    public static final String CONFIG_FILE_NAME_SIGNED_ELECTORAL_AUTHORITY_JSON = "electoralAuthority.json.sign";

    /**
     * The name of the file containing the decryption key in json format.
     */
    public static final String CONFIG_FILE_NAME_DECRYPTION_KEY_JSON = "decryptionKey.json";

    /**
     * The name of the file containing the decryption key signature.
     */
    public static final String CONFIG_FILE_NAME_SIGNED_DECRYPTION_KEY_JSON = "decryptionKey.json.sign";

    /**
     * The name of the file containing the electoral board properties.
     */
    public static final String CONFIG_FILE_NAME_EB_PROPS = "eb.properties";

    /**
     * The name of the file certificate authority
     */
    public static final String CONFIG_FILE_NAME_AUTHORITIESCA_PEM = "authoritiesca.pem";

    /**
     * The name of the directory online.
     */
    public static final String CONFIG_DIR_NAME_ONLINE = "ONLINE";

    /**
     * The name of the directory offline.
     */
    public static final String CONFIG_DIR_NAME_OFFLINE = "OFFLINE";

    /**
     * The name of the directory electionInformation.
     */
    public static final String CONFIG_DIR_NAME_ELECTIONINFORMATION = "electionInformation";

    /**
     * The name of the directory authentication context.
     */
    public static final String CONFIG_DIR_NAME_AUTHENTICATION = "authentication";

    /**
     * The name of the directory voterMaterial.
     */
    public static final String CONFIG_DIR_NAME_VOTERMATERIAL = "voterMaterial";

    /**
     * The name of the directory CUSTOMER
     */
    public static final String CONFIG_DIR_NAME_CUSTOMER = "CUSTOMER";

    /**
     * The name of the directory voteVerification.
     */
    public static final String CONFIG_DIR_NAME_VOTERVERIFICATION = "voteVerification";

    /**
     * The name of the directory electoral authority
     */
    public static final String CONFIG_DIR_NAME_ELECTORAL_AUTHORITY = "electoralAuthorities";

    /**
     * The name of the directory printing
     */
    public static final String CONFIG_DIR_NAME_PRINTING = "printing";

    /**
     * The name of the directory ballots.
     */
    public static final String CONFIG_DIR_NAME_BALLOTS = "ballots";

    /**
     * The name of the directory ballotboxes.
     */
    public static final String CONFIG_DIR_NAME_CLEANSING = "cleansing";

    /**
     * The name of the directory ballotboxes.
     */
    public static final String CONFIG_DIR_NAME_BALLOTBOXES = "ballotBoxes";

    /**
     * The name of the elections config file.
     */
    public static final String CONFIG_DIR_NAME_ELECTIONS_CONFIG_JSON = "elections_config.json";

    /**
     * The name of the ballotbox file.
     */
    public static final String CONFIG_DIR_NAME_BALLOTBOX_JSON = "ballotBox.json";

    /**
     * The name of the signed ballotbox file.
     */
    public static final String CONFIG_DIR_NAME_SIGNED_BALLOTBOX_JSON = "ballotBox.json.sign";

    /**
     * The name of the ballotbox context.data file.
     */
    public static final String CONFIG_DIR_NAME_BALLOTBOX_CONTEXT_DATA_JSON = "ballotBoxContextData.json";

    /**
     * The name of the signed ballotbox context.data file.
     */
    public static final String CONFIG_DIR_NAME_SIGNED_BALLOTBOX_CONTEXT_DATA_JSON = "ballotBoxContextData.json.sign";

    /**
     * Constant string VOTER_INFORMATION.
     */
    public static final String CONFIG_FILE_NAME_VOTER_INFORMATION = "voterInformation";

    /**
     * Constant string COMPUTED_VALUES
     */
    public static final String CONFIG_FILE_NAME_COMPUTED_VALUES = "computedValues";
    
    /**
     * Constant string DERIVED_KEYS
     */
    public static final String CONFIG_FILE_NAME_DERIVED_KEYS = "derivedKeys";

    /**
     * Constant string NODE_CONTRIBUTIONS
     */
    public static final String CONFIG_FILE_NAME_NODE_CONTRIBUTIONS = "nodeContributions";

    /**
     * Constant string CREDENTIAL_DATA.
     */
    public static final String CONFIG_FILE_NAME_CREDENTIAL_DATA = "credentialData";

    /**
     * Constant string VOTER_INFORMATION.
     */
    public static final String CONFIG_FILE_NAME_CODES_MAPPING = "codesMappingTablesContextData";

    /**
     * Constant string VERIFICATION_CARD_SET_DATA_FILE.
     */
    public static final String CONFIG_FILE_NAME_VERIFICATIONSET_DATA = "verificationCardSetData.json";

    /**
     * Constant string SIGNED_VERIFICATION_CARD_SET_DATA_FILE.
     */
    public static final String CONFIG_FILE_NAME_SIGNED_VERIFICATIONSET_DATA = "verificationCardSetData.json.sign";

    /**
     * Constant string VERIFICATION_CARD_IDS_FILE.
     */
    public static final String CONFIG_FILE_NAME_VERIFICATIONCARDIDS = "verificationCardIds.csv";

    /**
     * File name prefix for choice code generation request payload's JSON.
     */
    public static final String CONFIG_FILE_NAME_PREFIX_CHOICE_CODE_GENERATION_REQUEST_PAYLOAD = "choiceCodeGenerationRequestPayload.";
    
    /**
     * File name suffix for choice code generation request payload's JSON.
     */
    public static final String CONFIG_FILE_NAME_SUFFIX_CHOICE_CODE_GENERATION_REQUEST_PAYLOAD = ".json";


    /**
     * Constant string ELECTION_OPTION_REPRESENTATIONS_FILE.
     */
    public static final String CONFIG_FILE_NAME_ELECTION_OPTION_REPRESENTATIONS = "electionOptionRepresentations.csv";

    /**
     * Constant string BALLOT_CASTING_KEYSE.
     */
    public static final String CONFIG_FILE_NAME_BALLOT_CASTING_KEYS = "ballotCastingKeys.csv";

    /**
     * Constant string VERIFICATION_CARD_SET_DATA_FILE.
     */
    public static final String CONFIG_FILE_NAME_VERIFICATION_CONTEXT_DATA = "voteVerificationContextData.json";

    /**
     * Constant string VERIFICATION_CARD_SET_DATA_FILE.
     */
	public static final String CONFIG_FILE_NAME_SIGNED_VERIFICATION_CONTEXT_DATA = "voteVerificationContextData.json.sign";

    /**
     * Constant string Authentication Context Data.
     */
    public static final String CONFIG_FILE_NAME_AUTH_CONTEXT_DATA = "authenticationContextData.json";

    /**
     * Constant string Signed Authentication Context Data.
     */
    public static final String CONFIG_FILE_NAME_SIGNED_AUTH_CONTEXT_DATA = "authenticationContextData.json.sign";

    /**
     * Constant string Authentication Voter Data.
     */
    public static final String CONFIG_FILE_NAME_AUTH_VOTER_DATA = "authenticationVoterData.json";

    /**
     * Constant string Signed Authentication Voter Data.
     */
    public static final String CONFIG_FILE_NAME_SIGNED_AUTH_VOTER_DATA = "authenticationVoterData.json.sign";

    /**
     * Constant string Election Information Contents.
     **/
    public static final String CONFIG_FILE_NAME_ELECTION_INFORMATION_CONTESTS = "electionInformationContents.json";

    /**
     * Constant string Signed Election Information Contents.
     **/
	public static final String CONFIG_FILE_NAME_SIGNED_ELECTION_INFORMATION_CONTENTS = "electionInformationContents.json.sign";

    /**
     * Constant string Encrypted Ballot Box
     */
    public static final String CONFIG_FILE_NAME_ELECTION_INFORMATION_ENCRYPTED_BALLOT_BOX = "encryptedBallotBox.csv";

    /**
     * Constant string Encrypted Ballot Box
     */
    public static final String CONFIG_FILE_NAME_ELECTION_INFORMATION_DOWNLOADED_BALLOT_BOX = "downloadedBallotBox.csv";

    public static final String CONFIG_FILE_NAME_AUDITABLE_VOTES = "auditableVotes.csv";

    public static final String CONFIG_FILE_NAME_SUCCESSFUL_VOTES = "successfulVotes.csv";

    public static final String CONFIG_FILE_NAME_FAILED_VOTES = "failedVotes.csv";

    public static final String CONFIG_FILE_NAME_ENCRYPTED_BALLOTS = "encryptedBallots.csv";

    /**
     * COMMITMENT PARAMETERS_FILE_NAME
     */
    public static final String CONFIG_FILE_NAME_COMMITMENT_PARAMS = "commitmentParameters";

    /**
     * ENCRYPTED BALLOT BOX_FILE_NAME
     */
    public static final String CONFIG_FILE_NAME_ENCRYPTED_BALLOT_BOX = "downloadedBallotBox";

    /**
     * PROOFS FILE_NAME
     */
    public static final String CONFIG_FILE_NAME_PROOFS = "proofs";

    /**
     * REENCRYPTED BALLOT BOX_FILE_NAME
     */
    public static final String CONFIG_FILE_NAME_REENCRYPTED_BALLOT_BOX = "reencryptedBallots";

    /**
     * ENCRYPTED PARAMS BOX_FILE_NAME
     */
    public static final String CONFIG_FILE_NAME_ENCRYPTED_PARAMS = "encryptedParams";

    /**
     * ENCRYPTED PARAMS BOX_FILE_NAME
     */
    public static final String CONFIG_FILE_NAME_PUBLIC_KEY = "publicKey";

    /**
     * Constant string VERIFICATION_DATA.
     */
    public static final String CONFIG_FILE_VERIFICATION_CARD_DATA = "verificationCardData";

    /**
     * EXTENDED AUTHENTICATION file name
     */
    public static final String CONFIG_FILE_EXTENDED_AUTHENTICATION_DATA = "extendedAuthentication";
    
    /**
     * VERIFICATION_CARDS_KEY_PAIR file name
     */
    public static final String CONFIG_VERIFICATION_CARDS_KEY_PAIR_DIRECTORY = "verificationCardsKeyPairs";

    /**
     * Votes with proofs files name
     */
    public static final String CONFIG_FILE_NAME_VOTES_WITH_PROOF = "votesWithProof";
    
    /**
     * Votes files name
     */
    public static final String CONFIG_FILE_NAME_VOTES = "votes";

    /**
     * Constant string for the csv extension
     */
    public static final String CSV = ".csv";

    /**
     * Constant string for the sign extension
     */
    public static final String SIGN = ".sign";

    /**
     * Constant for the json extension
     */
    public static final String JSON = ".json";

    /**
     * Constant for the pem extension
     */
    public static final String PEM = ".pem";
    
    /**
     * Constant for the key extension
     */
    public static final String KEY = ".key";

    /**
     *
     */
    public static final String CONFIG_ENCRYPTED_BALLOT_BOX = "output.csv";

    public static final String CSR_FOLDER = "csr";

    public static final String MINIMUM_THRESHOLD = "minimumThreshold";

    public static final String ADMINISTRATION_BOARD_LABEL = "administrationBoard";

    public static final String ELECTORAL_BOARD_LABEL = "electoralBoard";

    public static final String MIX_DEC_KEY_LABEL = "mixDecKey";

    public static final String SUBJECT_COMMON_NAME = "subject.common.name";

    public static final String SUBJECT_ORGANIZATIONAL_UNIT = "subject.organizational.unit";

    public static final String SUBJECT_ORGANIZATION = "subject.organization";

    public static final String SUBJECT_COUNTRY = "subject.country";

    public static final String CSV_GLOB = "*" + CSV;

    /**
     * Language files folder name
     */
    public static final String SDM_LANGS_DIR_NAME = "langs";

    /**
     * PlatformRoot CA certificate file name.
     */
    public static final String CONFIG_FILE_NAME_PLATFORM_ROOT_CA = "platformRootCA.pem";

    /**
     * Tenant CA certificate file name.
     */
    public static final String CONFIG_FILE_NAME_TENANT_CA_PATTERN = "tenant-%s-CA.pem";
    
    public static final int NUM_KEYS_VERIFICATION_CARD_KEYPAIR = 1;

    // Avoid instantiation.
    private ConfigConstants() {
    }
}
