/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.cc;

/**
 * An error condition while storing or retrieving a payload.
 */
public class PayloadStorageException extends Exception {

    public PayloadStorageException(Throwable cause) {
        super(cause);
    }
}
