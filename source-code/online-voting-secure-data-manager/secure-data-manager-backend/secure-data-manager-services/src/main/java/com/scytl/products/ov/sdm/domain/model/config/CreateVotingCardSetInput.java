/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.config;

import java.util.List;

import com.scytl.products.ov.datapacks.beans.CreateVotingCardSetCertificatePropertiesContainer;

public class CreateVotingCardSetInput {

    private String start;

    private String end;

    private Integer validityPeriod;

    private int numberVotingCards;

    private String ballotID;

    private String ballotBoxID;

    private String electoralAuthorityID;

    private String votingCardSetID;

    private String verificationCardSetID;

    private String votingCardSetAlias;

    private String basePath;

    private int numCredentialsPerFile;

    private int numProcessors;

    private String eeID;

    private String ballotPath;

    private String keyForProtectingKeystorePassword;

    private List<String> choiceCodesEncryptionKey;

    private String primeEncryptionPrivateKeyJson;
    
    private String platformRootCACertificate;

    private CreateVotingCardSetCertificatePropertiesContainer createVotingCardSetCertificateProperties;

    /**
     * Sets new _eeID.
     *
     * @param eeID
     *            New value of _eeID.
     */
    public void setEeID(final String eeID) {
    	this.eeID = eeID;
    }

    /**
     * Gets _votingCardSetID.
     *
     * @return Value of _votingCardSetID.
     */
    public String getVotingCardSetID() {
        return votingCardSetID;
    }

    /**
     * Gets _numProcessors.
     *
     * @return Value of _numProcessors.
     */
    public int getNumProcessors() {
        return numProcessors;
    }

    /**
     * Gets _numberVotingCards.
     *
     * @return Value of _numberVotingCards.
     */
    public int getNumberVotingCards() {
        return numberVotingCards;
    }

    /**
     * Gets _ballotID.
     *
     * @return Value of _ballotID.
     */
    public String getBallotID() {
        return ballotID;
    }

    /**
     * Sets new _end.
     *
     * @param end
     *            New value of _end.
     */
    public void setEnd(final String end) {
    	this.end = end;
    }

    /**
     * Sets new _votingCardSetID.
     *
     * @param votingCardSetID
     *            New value of _votingCardSetID.
     */
    public void setVotingCardSetID(final String votingCardSetID) {
    	this.votingCardSetID = votingCardSetID;
    }

    public void setVerificationCardSetID(String verificationCardSetID) {
        this.verificationCardSetID = verificationCardSetID;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getVerificationCardSetID() {
        return verificationCardSetID;
    }

    public String getBasePath() {
        return basePath;
    }

    /**
     * Gets _validityPeriod.
     *
     * @return Value of _validityPeriod.
     */
    public Integer getValidityPeriod() {
        return validityPeriod;
    }

    /**
     * Sets new _ballotPath.
     *
     * @param ballotPath
     *            New value of _ballotPath.
     */
    public void setBallotPath(final String ballotPath) {
    	this.ballotPath = ballotPath;
    }

    /**
     * Gets _numCredentialsPerFile.
     *
     * @return Value of _numCredentialsPerFile.
     */
    public int getNumCredentialsPerFile() {
        return numCredentialsPerFile;
    }

    /**
     * Gets _ballotBoxID.
     *
     * @return Value of _ballotBoxID.
     */
    public String getBallotBoxID() {
        return ballotBoxID;
    }

    /**
     * Sets new _ballotID.
     *
     * @param ballotID
     *            New value of _ballotID.
     */
    public void setBallotID(final String ballotID) {
    	this.ballotID = ballotID;
    }

    /**
     * Sets new _numCredentialsPerFile.
     *
     * @param numCredentialsPerFile
     *            New value of _numCredentialsPerFile.
     */
    public void setNumCredentialsPerFile(final int numCredentialsPerFile) {
    	this.numCredentialsPerFile = numCredentialsPerFile;
    }

    /**
     * Sets new _numberVotingCards.
     *
     * @param numberVotingCards
     *            New value of _numberVotingCards.
     */
    public void setNumberVotingCards(final int numberVotingCards) {
    	this.numberVotingCards = numberVotingCards;
    }

    /**
     * Sets new _validityPeriod.
     *
     * @param validityPeriod
     *            New value of _validityPeriod.
     */
    public void setValidityPeriod(final Integer validityPeriod) {
    	this.validityPeriod = validityPeriod;
    }

    /**
     * Sets new _ballotBoxID.
     *
     * @param ballotBoxID
     *            New value of _ballotBoxID.
     */
    public void setBallotBoxID(final String ballotBoxID) {
    	this.ballotBoxID = ballotBoxID;
    }

    /**
     * Returns the current value of the field electoralAuthorityID.
     *
     * @return Returns the electoralAuthorityID.
     */
    public String getElectoralAuthorityID() {
        return electoralAuthorityID;
    }

    /**
     * Sets the value of the field electoralAuthorityID.
     *
     * @param electoralAuthorityID
     *            The electoralAuthorityID to set.
     */
    public void setElectoralAuthorityID(final String electoralAuthorityID) {
    	this.electoralAuthorityID = electoralAuthorityID;
    }

    /**
     * Gets _eeID.
     *
     * @return Value of _eeID.
     */
    public String getEeID() {
        return eeID;
    }

    /**
     * Sets new _start.
     *
     * @param start
     *            New value of _start.
     */
    public void setStart(final String start) {
    	this.start = start;
    }

    /**
     * Sets new _numProcessors.
     *
     * @param numProcessors
     *            New value of _numProcessors.
     */
    public void setNumProcessors(final int numProcessors) {
    	this.numProcessors = numProcessors;
    }

    /**
     * Gets _end.
     *
     * @return Value of _end.
     */
    public String getEnd() {
        return end;
    }

    /**
     * Gets _start.
     *
     * @return Value of _start.
     */
    public String getStart() {
        return start;
    }

    /**
     * Gets _ballotPath.
     *
     * @return Value of _ballotPath.
     */
    public String getBallotPath() {
        return ballotPath;
    }

    public String getKeyForProtectingKeystorePassword() {
        return keyForProtectingKeystorePassword;
    }

    public void setKeyForProtectingKeystorePassword(final String keyForProtectingKeystorePassword) {
    	this.keyForProtectingKeystorePassword = keyForProtectingKeystorePassword;
    }

    public String getVotingCardSetAlias() {
        return votingCardSetAlias;
    }

    public void setVotingCardSetAlias(String votingCardSetAlias) {
    	this.votingCardSetAlias = votingCardSetAlias;
    }

    public List<String> getChoiceCodesEncryptionKey() {
        return choiceCodesEncryptionKey;
    }

    public void setChoiceCodesEncryptionKey(List<String> choiceCodesEncryptionKey) {
    	this.choiceCodesEncryptionKey = choiceCodesEncryptionKey;
    }

    public String getPrimeEncryptionPrivateKeyJson() {
        return primeEncryptionPrivateKeyJson;
    }
    
    public void setPrimeEncryptionPrivateKeyJson(
            String primeEncryptionPrivateKeyJson) {
    	this.primeEncryptionPrivateKeyJson = primeEncryptionPrivateKeyJson;
    }

    public String getPlatformRootCACertificate() {
        return platformRootCACertificate;
    }

    public void setPlatformRootCACertificate(String platformRootCACertificate) {
        this.platformRootCACertificate = platformRootCACertificate;
    }

    public CreateVotingCardSetCertificatePropertiesContainer getCreateVotingCardSetCertificateProperties() {
        return createVotingCardSetCertificateProperties;
    }

    public void setCreateVotingCardSetCertificateProperties(
            CreateVotingCardSetCertificatePropertiesContainer createVotingCardSetCertificateProperties) {
        this.createVotingCardSetCertificateProperties = createVotingCardSetCertificateProperties;
    }
}
