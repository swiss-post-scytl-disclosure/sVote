/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

/**
 * Exception for encapsulate signature exceptions.
 */
public class VotingCardSetChoiceCodesServiceException extends RuntimeException {

	private static final long serialVersionUID = 8069770207692371130L;

	public VotingCardSetChoiceCodesServiceException(Throwable cause) {
		super(cause);
	}
	
	public VotingCardSetChoiceCodesServiceException(String message) {
		super(message);
	}
	
	public VotingCardSetChoiceCodesServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
