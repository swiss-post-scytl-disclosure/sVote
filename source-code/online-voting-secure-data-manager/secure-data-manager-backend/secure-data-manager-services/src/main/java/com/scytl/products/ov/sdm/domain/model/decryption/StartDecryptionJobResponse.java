/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.decryption;

import com.scytl.products.ov.sdm.domain.common.JobStatus;

public class StartDecryptionJobResponse {

    private String jobId;
    private JobStatus jobStatus;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(final String jobId) {
        this.jobId = jobId;
    }

    public JobStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(final JobStatus status) {
        this.jobStatus = status;
    }

}
