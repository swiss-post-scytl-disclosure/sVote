/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.config;

public class RestConnectionConfig {

    private final String readTimeOut;

    private final String writeTimeOut;

    private final String connectionTimeOut;

    public RestConnectionConfig(String readTimeOut, String writeTimeOut, String connectionTimeOut) {
        this.readTimeOut = readTimeOut;
        this.writeTimeOut = writeTimeOut;
        this.connectionTimeOut = connectionTimeOut;

        System.setProperty("read.time.out", readTimeOut);
        System.setProperty("write.time.out", writeTimeOut);
        System.setProperty("connection.time.out", connectionTimeOut);

    }

    /**
     * Gets readTimeOut.
     *
     * @return Value of readTimeOut.
     */
    public String getReadTimeOut() {
        return readTimeOut;
    }

    /**
     * Gets connectionTimeOut.
     *
     * @return Value of connectionTimeOut.
     */
    public String getConnectionTimeOut() {
        return connectionTimeOut;
    }

    /**
     * Gets writeTimeOut.
     *
     * @return Value of writeTimeOut.
     */
    public String getWriteTimeOut() {
        return writeTimeOut;
    }
}
