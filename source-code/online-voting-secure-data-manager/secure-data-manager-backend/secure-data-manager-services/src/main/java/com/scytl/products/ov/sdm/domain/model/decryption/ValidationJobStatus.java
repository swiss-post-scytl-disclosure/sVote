/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.decryption;


import com.scytl.products.ov.sdm.domain.common.GenericJobStatus;
import com.scytl.products.ov.sdm.domain.common.JobProgressDetails;
import com.scytl.products.ov.sdm.domain.common.JobStatus;

public class ValidationJobStatus extends GenericJobStatus {

    private int auditableVotesCount;
    private int decryptedVotesCount;
    private int errorCount;

    protected ValidationJobStatus(){}

    public ValidationJobStatus(final String jobId) {
        super(jobId, JobStatus.UNKNOWN, "", JobProgressDetails.UNKNOWN);
    }

    public int getAuditableVotesCount() {
        return auditableVotesCount;
    }

    protected void setAuditableVotesCount(final int auditableVotesCount) {
        this.auditableVotesCount = auditableVotesCount;
    }

    public int getDecryptedVotesCount() {
        return decryptedVotesCount;
    }

    protected void setDecryptedVotesCount(final int decryptedVotesCount) {
        this.decryptedVotesCount = decryptedVotesCount;
    }

    public int getErrorCount() {
        return errorCount;
    }

    protected void setErrorCount(final int errorCount) {
        this.errorCount = errorCount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("status=").append(getStatus());
        sb.append(", statusDetails=").append(getStatusDetails());
        sb.append(", auditableVotesCount=").append(auditableVotesCount);
        sb.append(", decryptedVotesCount=").append(decryptedVotesCount);
        sb.append(", errorCount=").append(errorCount);
        sb.append('}');
        return sb.toString();
    }
}
