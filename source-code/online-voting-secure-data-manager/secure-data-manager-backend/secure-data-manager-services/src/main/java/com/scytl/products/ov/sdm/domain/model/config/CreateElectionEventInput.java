/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.config;

import com.scytl.products.ov.datapacks.beans.CreateElectionEventCertificatePropertiesContainer;

/**
 * Input for the create election event service
 */
public class CreateElectionEventInput {

    private String eeid;

    private String start;

    private String end;

    private Integer validityPeriod;

    private String challengeResExpTime;

    private String authTokenExpTime;

    private String challengeLength;

    private String numVotesPerVotingCard;

    private String numVotesPerAuthToken;

    private String maxNumberOfAttempts;

    private String outputPath;

    private String keyForProtectingKeystorePassword;

    private CreateElectionEventCertificatePropertiesContainer createElectionEventCertificateProperties;

    /**
     * Sets new _outputPath.
     *
     * @param outputPath
     *            New value of _outputPath.
     */
    public void setOutputPath(final String outputPath) {
        this.outputPath = outputPath;
    }

    /**
     * Sets new _end.
     *
     * @param end
     *            New value of _end.
     */
    public void setEnd(final String end) {
    	this.end = end;
    }

    /**
     * GetsStart.
     *
     * @return Value of start.
     */
    public String getStart() {
        return start;
    }

    /**
     * Sets new _challengeLength.
     *
     * @param challengeLength
     *            New value of _challengeLength.
     */
    public void setChallengeLength(final String challengeLength) {
    	this.challengeLength = challengeLength;
    }

    /**
     * Sets new _validityPeriod.
     *
     * @param validityPeriod
     *            New value of _validityPeriod.
     */
    public void setValidityPeriod(final Integer validityPeriod) {
    	this.validityPeriod = validityPeriod;
    }

    /**
     * Gets _numVotesPerAuthToken.
     *
     * @return Value of _numVotesPerAuthToken.
     */
    public String getNumVotesPerAuthToken() {
        return numVotesPerAuthToken;
    }

    /**
     * Sets new _numVotesPerAuthToken.
     *
     * @param numVotesPerAuthToken
     *            New value of _numVotesPerAuthToken.
     */
    public void setNumVotesPerAuthToken(final String numVotesPerAuthToken) {
    	this.numVotesPerAuthToken = numVotesPerAuthToken;
    }

    /**
     * Sets new _challengeResExpTime.
     *
     * @param challengeResExpTime
     *            New value of _challengeResExpTime.
     */
    public void setChallengeResExpTime(final String challengeResExpTime) {
    	this.challengeResExpTime = challengeResExpTime;
    }

    /**
     * Gets _numVotesPerVotingCard.
     *
     * @return Value of _numVotesPerVotingCard.
     */
    public String getNumVotesPerVotingCard() {
        return numVotesPerVotingCard;
    }

    /**
     * Gets _challengeLength.
     *
     * @return Value of _challengeLength.
     */
    public String getChallengeLength() {
        return challengeLength;
    }

    /**
     * Gets _eeid.
     *
     * @return Value of _eeid.
     */
    public String getEeid() {
        return eeid;
    }

    /**
     * Sets new _numVotesPerVotingCard.
     *
     * @param numVotesPerVotingCard
     *            New value of _numVotesPerVotingCard.
     */
    public void setNumVotesPerVotingCard(final String numVotesPerVotingCard) {
    	this.numVotesPerVotingCard = numVotesPerVotingCard;
    }

    /**
     * Gets _challengeResExpTime.
     *
     * @return Value of _challengeResExpTime.
     */
    public String getChallengeResExpTime() {
        return challengeResExpTime;
    }

    /**
     * Gets _authTokenExpTime.
     *
     * @return Value of _authTokenExpTime.
     */
    public String getAuthTokenExpTime() {
        return authTokenExpTime;
    }

    /**
     * Gets _outputPath.
     *
     * @return Value of _outputPath.
     */
    public String getOutputPath() {
        return outputPath;
    }

    /**
     * Gets _validityPeriod.
     *
     * @return Value of _validityPeriod.
     */
    public Integer getValidityPeriod() {
        return validityPeriod;
    }

    /**
     * Sets new _eeid.
     *
     * @param eeid
     *            New value of _eeid.
     */
    public void setEeid(final String eeid) {
    	this.eeid = eeid;
    }

    /**
     * Gets _end.
     *
     * @return Value of _end.
     */
    public String getEnd() {
        return end;
    }

    /**
     * Sets new _authTokenExpTime.
     *
     * @param authTokenExpTime
     *            New value of _authTokenExpTime.
     */
    public void setAuthTokenExpTime(final String authTokenExpTime) {
    	this.authTokenExpTime = authTokenExpTime;
    }

    /**
     * Sets new _maxNumberOfAttempts.
     *
     * @param maxNumberOfAttempts
     *            New value of _maxNumberOfAttempts.
     */
    public void setMaxNumberOfAttempts(final String maxNumberOfAttempts) {
    	this.maxNumberOfAttempts = maxNumberOfAttempts;
    }

    /**
     * Sets new _start.
     *
     * @param start
     *            New value of _start.
     */
    public void setStart(final String start) {
    	this.start = start;
    }

    /**
     * Gets _maxNumberOfAttempts.
     *
     * @return Value of _maxNumberOfAttempts.
     */
    public String getMaxNumberOfAttempts() {
        return maxNumberOfAttempts;
    }

    public String getKeyForProtectingKeystorePassword() {
        return keyForProtectingKeystorePassword;
    }

    public void setKeyForProtectingKeystorePassword(final String keyForProtectingKeystorePassword) {
    	this.keyForProtectingKeystorePassword = keyForProtectingKeystorePassword;
    }

    public CreateElectionEventCertificatePropertiesContainer getCertificatePropertiesInput() {
        return createElectionEventCertificateProperties;
    }

    public void setCertificatePropertiesInput(
            final CreateElectionEventCertificatePropertiesContainer createElectionEventCertificateProperties) {
        this.createElectionEventCertificateProperties = createElectionEventCertificateProperties;
    }
}
