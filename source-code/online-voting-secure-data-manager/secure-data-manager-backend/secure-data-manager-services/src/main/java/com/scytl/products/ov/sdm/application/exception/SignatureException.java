/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

/**
 * Exception for encapsulate signature exceptions.
 */
public class SignatureException extends RuntimeException {

	/**
	 * Constructor.
	 *
	 * @param message the string message.
	 * @param e the thrown exception.
	 */
	public SignatureException(String message, Throwable e) {
		super(message, e);
	}

}
