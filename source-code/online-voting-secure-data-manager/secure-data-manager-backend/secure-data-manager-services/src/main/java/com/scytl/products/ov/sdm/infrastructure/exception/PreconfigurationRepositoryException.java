/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.exception;

public class PreconfigurationRepositoryException extends RuntimeException {
  
	private static final long serialVersionUID = -5271446720919937859L;

	public PreconfigurationRepositoryException(Throwable cause) {
        super(cause);
    }
	
	public PreconfigurationRepositoryException(String message) {
        super(message);
    }
	
	public PreconfigurationRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
