/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.keytranslation;

import java.util.List;

public class ElectionEventTranslationList {

    private List<ElectionEventTranslation> electionEventTranslations;


    /**
     * Sets new translations.
     *
     * @param electionEventTranslations New value of translations.
     */
    public void setElectionEventTranslations(List<ElectionEventTranslation> electionEventTranslations) {
        this.electionEventTranslations = electionEventTranslations;
    }

    /**
     * Gets translations.
     *
     * @return Value of translations.
     */
    public List<ElectionEventTranslation> getElectionEventTranslations() {
        return electionEventTranslations;
    }
}
