/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service;

import com.scytl.products.ov.sdm.domain.model.generator.DataGeneratorResponse;

/**
 * This interface defines the API for a service which accesses a generator of encryption parameters data.
 */
public interface EncryptionParametersDataGeneratorService {

	/**
	 * This method generates all the data for encryption parameters.
	 * 
	 * @param electionEventId The identifier of the election event to whom the encryption parameters belong.
	 * @return a bean containing information about the result of the generation.
	 */
	DataGeneratorResponse generate(String electionEventId);
}
