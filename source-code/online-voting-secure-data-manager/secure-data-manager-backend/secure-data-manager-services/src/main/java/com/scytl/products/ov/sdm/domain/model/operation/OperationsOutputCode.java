/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.operation;

public enum OperationsOutputCode {
	GENERAL_ERROR(4000, "General Error"),
	MISSING_PARAMETER(4001, "Missing parameter"),
	ERROR_IO_OPERATIONS(4002, "I/O operations error"),
	ERROR_PARSING_FILE(4003, "Error parsing file"),
    ERROR_STATUS_NAME(4004, "Illegal argument status name"), 
    MISSING_COMMANDS_FOR_PHASE(4005, "Missing commands for phase");
	
	private final int value;
	private final String reasonPhrase;
	
	OperationsOutputCode(int value, String reasonPhrase) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
	}
	
	public int value() {
		return value;
	}
	
	public String getReasonPhrase() {
		return reasonPhrase;
	}
}
