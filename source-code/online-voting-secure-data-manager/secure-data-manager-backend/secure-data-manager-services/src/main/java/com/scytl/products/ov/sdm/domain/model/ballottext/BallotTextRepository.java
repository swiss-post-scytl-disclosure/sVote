/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.ballottext;

import java.util.List;
import java.util.Map;

import com.scytl.products.ov.sdm.application.exception.DatabaseException;
import com.scytl.products.ov.sdm.domain.model.EntityRepository;

/**
 * Interface providing operations with ballot text.
 */
public interface BallotTextRepository extends EntityRepository {

    void updateSignedBallotText(String ballotTextId,
            String signedBallotText);

    /**
     * Lists the signatures of the entities matching the criteria.
     *
     * @param criteria
     *            the criteria
     * @return the signatures
     * @throws DatabaseException
     *             failed to list signatures.
     */
    List<String> listSignatures(Map<String, Object> criteria)
            throws DatabaseException;
}
