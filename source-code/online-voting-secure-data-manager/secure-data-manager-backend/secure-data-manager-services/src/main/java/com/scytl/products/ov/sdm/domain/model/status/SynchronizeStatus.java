/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.status;

/**
 * Identifies the possible status in a synchronization operation
 */
public enum SynchronizeStatus {

    SYNCHRONIZED("Synchronized",true),
    FAILED("Error synchronizing",false),
    PENDING("Pending to synchronize",false);

    private String status;
    private Boolean isSynchronized;

    SynchronizeStatus(String status,Boolean sync){
        this.status = status;
        isSynchronized = sync;
    }

    public String getStatus(){
        return status;
    }


    /**
     * Gets isSynchronized.
     *
     * @return Value of isSynchronized.
     */
    public Boolean getIsSynchronized() {
        return isSynchronized;
    }
}
