/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class ElectionAuthorityServiceException extends RuntimeException {
	
	private static final long serialVersionUID = 7263023751982893102L;

	public ElectionAuthorityServiceException(Throwable cause) {
		super(cause);
	}
	
	public ElectionAuthorityServiceException(String message) {
		super(message);
	}
	
	public ElectionAuthorityServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
