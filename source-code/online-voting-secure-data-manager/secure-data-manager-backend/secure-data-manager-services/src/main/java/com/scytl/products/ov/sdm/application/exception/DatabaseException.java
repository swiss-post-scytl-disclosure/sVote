/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

/**
 * Exception for encapsulate database exceptions.
 */
public class DatabaseException extends RuntimeException {

    private static final long serialVersionUID = -9137284752915548117L;

    /**
     * Constructor.
     *
     * @param message
     *            the string message.
     * @param e
     *            the thrown exception.
     */
    public DatabaseException(final String message, final Throwable e) {
        super(message, e);
    }

    /**
     * Constructor.
     *
     * @param message
     */
    public DatabaseException(final String message) {
        super(message);
    }
}
