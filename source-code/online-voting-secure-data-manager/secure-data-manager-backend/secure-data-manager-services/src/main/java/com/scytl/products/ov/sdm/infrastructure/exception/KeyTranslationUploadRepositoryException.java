/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.exception;

public class KeyTranslationUploadRepositoryException extends RuntimeException {
  
	private static final long serialVersionUID = 3471676193283989367L;

	public KeyTranslationUploadRepositoryException(Throwable cause) {
        super(cause);
    }
	
	public KeyTranslationUploadRepositoryException(String message) {
        super(message);
    }
	
	public KeyTranslationUploadRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
