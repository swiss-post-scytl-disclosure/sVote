/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.service;

import com.scytl.products.ov.sdm.domain.model.EntityRepository;
import com.scytl.products.ov.sdm.domain.model.status.SynchronizeStatus;

/**
 * Interface for managing status of an entity from the configuration.
 *
 */
public interface ConfigurationEntityStatusService {

	/**
	 * Updates the status of the given entity using the given repository.
	 * 
	 * @param newStatus the new state of the entity.
	 * @param id the id of the entity to which to update the status.
	 * @param repository the specific entity repository to be used for updating the status.
	 * @return the fields that were updated.
	 */
	String update(String newStatus, String id, EntityRepository repository);

	/**
	 * Updates the status of the given entity using the given repository.
	 *
	 * @param newStatus the new state of the entity.
	 * @param id the id of the entity to which to update the status.
	 * @param repository the specific entity repository to be used for updating the status.
	 * @param syncDetails - details of the status of synchronization.
	 * @return
	 */
	String updateWithSynchronizedStatus(String newStatus, String id, EntityRepository repository, SynchronizeStatus syncDetails);
}
