/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.clients;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpHeaders;

import com.scytl.products.ov.sdm.domain.model.keytranslation.BallotBoxTranslationList;
import com.scytl.products.ov.sdm.domain.model.keytranslation.ElectionEventTranslationList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface KeyTranslationClient {

    String TENANT_ID_PARAM = "tenantId";

    String ELECTION_EVENT_ID_PARAM = "electionEventId";

    @POST("electionevent/tenant/{tenantId}")
    Call<ResponseBody> createUpdateElectionEventKeyTranslation(@Path(TENANT_ID_PARAM) String tenantId,
            @NotNull @Body ElectionEventTranslationList translations);

    @POST("ballotbox/tenant/{tenantId}")
    @Headers(HttpHeaders.CONTENT_TYPE + ": " + MediaType.APPLICATION_JSON)
    Call<ResponseBody> createUpdateBallotBoxKeyTranslation(@Path(TENANT_ID_PARAM) String tenantId,
            @NotNull @Body BallotBoxTranslationList translations);
}
