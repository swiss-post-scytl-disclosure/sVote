/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.administrationauthority;

/**
 * Bean holding the serialized share.
 */
public class ReadShareOutputData {

	private String serializedShare;

	public String getSerializedShare() {
		return serializedShare;
	}

	public void setSerializedShare(String serializedShare) {
		this.serializedShare = serializedShare;
	}
}
