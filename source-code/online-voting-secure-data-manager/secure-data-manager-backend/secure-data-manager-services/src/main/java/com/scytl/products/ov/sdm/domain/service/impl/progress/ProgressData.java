/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.impl.progress;

import java.net.URI;
import java.util.concurrent.CompletableFuture;

public class ProgressData<T> {
    private final URI uri;
    private CompletableFuture<T> future;
    private T progressStatus;

    ProgressData(URI uri, CompletableFuture<T> future) {
        this.uri = uri;
        this.future = future;
    }

    ProgressData(URI uri, CompletableFuture<T> future, T progressStatus) {
        this.uri = uri;
        this.future = future;
        this.progressStatus = progressStatus;
    }

    public URI getUri() {
        return uri;
    }

    public CompletableFuture<T> getFuture() {
        return future;
    }

    public void setProgressStatus(T status) {
        this.progressStatus = status;
    }

    public T getProgressStatus() {
        return progressStatus;
    }
}
