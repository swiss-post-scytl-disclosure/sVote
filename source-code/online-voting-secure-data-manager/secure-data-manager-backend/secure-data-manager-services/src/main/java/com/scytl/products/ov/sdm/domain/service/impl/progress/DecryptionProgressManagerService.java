/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.impl.progress;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import com.scytl.products.ov.sdm.domain.model.decryption.ValidationJobStatus;

/**
 * This implementation contains a call to the configuration engine for progressManager 
 */
@Service
public class DecryptionProgressManagerService extends GenericProgressManagerService<ValidationJobStatus> {

    @Value("${DECRYPTION_URL}")
    private String configServiceBaseUrl;

    @PostConstruct
    void setup() {
        setServiceUrl(configServiceBaseUrl);
    }

    @Override
    protected ValidationJobStatus defaultData(final String jobId) {
        return new ValidationJobStatus(jobId);
    }

    @Override
    protected List<ValidationJobStatus> doCall(final URI uri) {
        final ValidationJobStatus[] response = restClient.getForObject(uri, ValidationJobStatus[].class);
        return Arrays.asList(response);
    }
}

