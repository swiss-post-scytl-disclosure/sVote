/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * WE should remove this and move to Spring RestTemplate
 */
@Provider
public class JacksonJerseyJsonProvider implements ContextResolver<ObjectMapper> {

    private static ObjectMapper mapper;
    static {
        mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        mapper.findAndRegisterModules();
    }

    @Override
    public ObjectMapper getContext(final Class<?> aClass) {
        return mapper;
    }
}
