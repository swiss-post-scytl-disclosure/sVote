/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.administrationauthority;

/**
 * Bean holding the information about an administration authority.
 */

import com.scytl.products.ov.sdm.domain.model.EntityRepository;

public interface AdministrationAuthorityRepository
        extends EntityRepository {
}
