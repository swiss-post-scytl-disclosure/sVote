/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.status;

/**
 * Defines a set of status for entities.
 */
public enum Status {
    NEW(0),
    LOCKED(1),
    READY(2),
    APPROVED(3),
    PRECOMPUTING(4),
    PRECOMPUTED(5),
    COMPUTING(6),
    COMPUTED(7),
    VCS_DOWNLOADED(8),
    GENERATING(9),
    GENERATED(10),
    CONSTITUTED(11),
    SIGNED(12),
    MIXING(13),
    MIXED(14),
    BB_DOWNLOADED(15),
    DECRYPTING(16),
    DECRYPTED(17),
    COUNTED(18),
    TALLYING(19),
    TALLIED(20);
    
    private final int index;
    
    private Status(int index) {
        this.index = index;
    }

    /**
     * Returns whether this status is before a given one.
     * 
     * @param other the other status
     * @return is before.
     */
    public boolean isBefore(Status other) {
        return index < other.index;
    }
}
