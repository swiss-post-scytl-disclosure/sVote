/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;

/**
 * Manager for database.
 */
public interface OldDatabaseManager {

	/**
	 * Returns the database instance.
	 * 
	 * @return the database.
	 */
			ODatabaseDocumentTx getDatabase();

	/**
	 * Returns the database status.
	 * 
	 * @return the database status.
	 */
			String getStatus();

	/**
	 * Set database for current thread.
	 * 
	 * @param db the database instance.
	 */
			void setDatabaseForCurrentThread(ODatabaseDocumentTx db);

	/**
	 * Closes the database.
	 */
			void closeDatabase();
}
