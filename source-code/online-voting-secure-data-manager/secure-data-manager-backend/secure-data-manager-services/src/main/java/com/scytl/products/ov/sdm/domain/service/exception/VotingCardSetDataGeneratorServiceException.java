/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.exception;

public class VotingCardSetDataGeneratorServiceException extends RuntimeException {
  
	private static final long serialVersionUID = 315280617446598532L;

	public VotingCardSetDataGeneratorServiceException(Throwable cause) {
        super(cause);
    }
	
	public VotingCardSetDataGeneratorServiceException(String message) {
        super(message);
    }
	
	public VotingCardSetDataGeneratorServiceException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
