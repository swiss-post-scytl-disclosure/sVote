/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The class with the database path as field.
 */
@Component
public class DatabasePath {

    @Value("${database.path}")
    private String dbPath;

    /**
     * Sets the value of the field DB_PATH.
     *
     * @param dbPath
     *            The DB_PATH to set.
     */
    public void setDatabasePath(String dbPath) {
        this.dbPath = dbPath;
    }

    /**
     * Returns the current value of the field DB_PATH.
     *
     * @return Returns the DB_PATH.
     */
    public String getDatabasePath() {
        return dbPath;
    }
}
