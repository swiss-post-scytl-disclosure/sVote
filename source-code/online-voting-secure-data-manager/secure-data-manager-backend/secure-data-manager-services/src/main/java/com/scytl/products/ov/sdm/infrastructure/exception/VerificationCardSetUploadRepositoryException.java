/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.exception;

public class VerificationCardSetUploadRepositoryException extends RuntimeException {
  
	private static final long serialVersionUID = 1907863331648478803L;

	public VerificationCardSetUploadRepositoryException(Throwable cause) {
        super(cause);
    }
	
	public VerificationCardSetUploadRepositoryException(String message) {
        super(message);
    }
	
	public VerificationCardSetUploadRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
