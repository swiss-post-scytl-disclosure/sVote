/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.config;

import com.scytl.products.oscore.logging.api.factory.SecureLoggingFactory;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.factory.SecureLoggingFactoryLog4j;
import com.scytl.products.oscore.logging.core.formatter.SplunkFormatter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecureLoggingConfig {

    @Bean
    public TransactionInfoProvider transactionInfoProvider() {
        return new TransactionInfoProvider();
    }

    @Bean
    public MessageFormatter messageFormatter() {
        return new SplunkFormatter("OV", "SDM", transactionInfoProvider());
    }

    @Bean
    public SecureLoggingFactory secureLoggingFactory() {
        return new SecureLoggingFactoryLog4j(messageFormatter());
    }

    @Bean
    public SecureLoggingWriter loggingWriter() {
        return secureLoggingFactory().getLogger("SecureLogger");
    }
}
