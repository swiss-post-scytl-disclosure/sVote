/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.decryption;

public class DecryptionInputData {

	private String serializedPrivateKey;
	
	private String privateKeyPEM;

	public String getSerializedPrivateKey() {
		return serializedPrivateKey;
	}

	public void setSerializedPrivateKey(String serializedPrivateKey) {
		this.serializedPrivateKey = serializedPrivateKey;
	}

	public String getPrivateKeyPEM() {
		return privateKeyPEM;
	}

	public void setPrivateKeyPEM(String privateKeyPEM) {
		this.privateKeyPEM = privateKeyPEM;
	}
	
}
