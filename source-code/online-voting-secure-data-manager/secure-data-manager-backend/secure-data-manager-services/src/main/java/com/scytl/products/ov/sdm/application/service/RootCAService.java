/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

import java.security.cert.X509Certificate;

public interface RootCAService {

    /**
     * Stores a given certificate.
     * 
     * @param certificate
     *            the certificate
     */
    void save(X509Certificate certificate) throws CertificateManagementException;

    /**
     * Retrieves a certificate.
     * 
     * @return the certificate
     */
    X509Certificate load() throws CertificateManagementException;

}
