/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

class RootCertificateManagementException extends Exception {

    private static final long serialVersionUID = 1L;

    public RootCertificateManagementException(Throwable cause) {
        super(cause);
    }
}
