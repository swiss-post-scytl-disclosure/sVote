/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

/**
 * This class contains constants used for managing json formats.
 */
public final class JsonConstants {

    /**
     * The name of the json attribute election events.
     */
    public static final String JSON_ATTRIBUTE_NAME_ELECTION_EVENTS = "electionEvents";

    /**
     * The name of the json attribute voting card sets.
     */
    public static final String JSON_ATTRIBUTE_NAME_VOTING_CARD_SETS = "votingCardSets";

    /**
     * The name of the json attribute electoralAuthorities.
     */
    public static final String JSON_ATTRIBUTE_NAME_ELECTORAL_AUTHORITIES = "electoralAuthorities";

    /**
     * The name of the json attribute administrationAuthorities
     */
    public static final String JSON_ATTRIBUTE_NAME_ADMINISTRATION_AUTHORITIES = "administrationAuthorities";

    /**
     * The name of the json attribute administrationAuthoity
     */
    public static final String JSON_ATTRIBUTE_NAME_ADMINISTRATION_AUTHORITY = "administrationAuthority";

    /**
     * The name of the json attribute electoralAuthorities.
     */
    public static final String JSON_ATTRIBUTE_NAME_ELECTORAL_AUTHORITY = "electoralAuthority";

    /**
     * The name of the json attribute publicKey.
     */
    public static final String JSON_ATTRIBUTE_NAME_PUBLIC_KEY = "publicKey";

    /**
     * The name of the json attribute encryptionParameters p.
     */
    public static final String JSON_ATTRIBUTE_NAME_P = "p";

    /**
     * The name of the json attribute encryptionParameters q.
     */
    public static final String JSON_ATTRIBUTE_NAME_Q = "q";

    /**
     * The name of the json attribute encryptionParameters g.
     */
    public static final String JSON_ATTRIBUTE_NAME_G = "g";

    /**
     * The name of the json attribute encryptionParameters.
     */
    public static final String JSON_ATTRIBUTE_NAME_ENCRYPTION_PARAMS = "encryptionParameters";

    /**
     * The name of the json attribute ballot boxes.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_BOXES = "ballotBoxes";

    /**
     * The name of the json attribute ballot boxes.
     */
    public static final String JSON_ATTRIBUTE_NAME_SETTINGS = "settings";

    /**
     * The name of the json attribute ballot boxes.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOTS = "ballots";

    /**
     * The name of the json attribute ballot.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT = "ballot";

    /**
     * The name of the json attribute id.
     */
    public static final String JSON_ATTRIBUTE_NAME_ID = "id";

    /**
     * The name of the json attribute status.
     */
    public static final String JSON_ATTRIBUTE_NAME_STATUS = "status";

    /**
     * The name of the json attribute test.
     */
    public static final String JSON_ATTRIBUTE_NAME_TEST = "test";

    /**
     * The name of the json attribute election event.
     */
    public static final String JSON_ATTRIBUTE_NAME_ELECTION_EVENT = "electionEvent";

    /**
     * The name of the json attribute ballot texts.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_TEXTS = "texts";

    /**
     * The name of the json attribute ballot texts.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_TRANSLATIONS = "translations";

    /**
     * The name of the json attribute ballot texts.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_TEXTS_UPLOAD = "ballottext";

    /**
     * The name of the json attribute locale.
     */
    public static final String JSON_ATTRIBUTE_NAME_LOCALE = "locale";

    /**
     * The name of the json attribute result.
     */
    public static final String JSON_ATTRIBUTE_NAME_RESULT = "result";

    /**
     * The name of the json attribute details.
     */
    public static final String JSON_ATTRIBUTE_NAME_DETAILS = "details";

    /**
     * The name of the json attribute ballot box.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_BOX = "ballotBox";

    /**
     * The name of the json attribute verificationCardSetId.
     */
    public static final String JSON_ATTRIBUTE_NAME_VERIFICATIONCARDSETID = "verificationCardSetId";

    /**
     * The name of the json attribute electoralAuthorityId.
     */
    public static final String JSON_ATTRIBUTE_NAME_ELECTORALAUTHORITYID = "electoralAuthorityId";

    /**
     * The name of the json attribute ballotBox.
     */
    public static final String JSON_ATTRIBUTE_NAME_ALIAS = "alias";

    /**
     * The name of the json attribute ballotBoxAlias.
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_BOX_ALIAS = "ballotBoxAlias";

    /**
     * The name of the json attribute for the ballotBox closing date
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_BOX_CLOSING_DATE = "dateTo";

    /**
     * The name of the json attribute for the ballotBox grace period
     */
    public static final String JSON_ATTRIBUTE_NAME_BALLOT_BOX_GRACE_PERIOD = "gracePeriod";

    /**
     * The name of the json structure for id attribute of electionEvent object.
     */
    public static final String JSON_STRUCTURE_NAME_ELECTION_EVENT_ID = "electionEvent.id";

    /**
     * The name of the json structure for id attribute of ballotBox object.
     */
    public static final String JSON_STRUCTURE_NAME_BALLOT_BOX_ID = "ballotBox.id";

    /**
     * The name of the json structure for id attribute of ballotBox object.
     */
    public static final String JSON_STRUCTURE_NAME_BALLOT_ID = "ballot.id";

    /**
     * The name of the json structure for id attribute of electoralAuthority id object.
     */
    public static final String JSON_STRUCTURE_NAME_ELECTORAL_AUTHORITY_ID = "electoralAuthority.id";

    /**
     * The name of the json field synchronized
     */
    public static final String JSON_ATTRIBUTE_NAME_SYNCHRONIZED = "synchronized";

    /**
     * The name of the json field computing
     */
    public static final String JSON_ATTRIBUTE_NAME_STATUS_COMPUTING = "computing";

    /**
     * The format of an empty json.
     */
    public static final String JSON_EMPTY_OBJECT = "{}";

    /**
     * The json format for an empty result.
     */
    public static final String JSON_RESULT_EMPTY = "{\"result\":[]}";

    /**
     * The name of the json attribute dateTo.
     */
    public static final String JSON_ATTRIBUTE_NAME_DATE_TO = "dateTo";

    /**
     * The name of the json attribute dateFrom.
     */
    public static final String JSON_ATTRIBUTE_NAME_DATE_FROM = "dateFrom";

    /**
     * The validity period in years for a given certificate
     */
    public static final String JSON_ATTRIBUTE_CERTIFICATES_VALIDITY = "certificatesValidityPeriod";

    public static final String JSON_ATTRIBUTE_NAME_SIGNED_OBJECT = "signedObject";

    /**
     * The name of the json attribute contests.
     */
    public static final String JSON_ATTRIBUTE_NAME_CONTESTS = "contests";

    /**
     * The name of the json attribute optionsAttributes.
     */
    public static final String JSON_ATTRIBUTE_NAME_OPTION_ATTRIBUTES = "optionsAttributes";

    /**
     * The name of the json attribute ballotAlias.
     */
    public static final java.lang.String JSON_ATTRIBUTE_NAME_BALLOT_ALIAS = "ballotAlias";

    /**
     * The name of the json attribute castRequired.
     */
    public static final String JSON_ATTRIBUTE_NAME_CONFIRMATION_REQUIRED = "confirmationRequired";

    /**
     * The name of the json attribute writeInAlphabet.
     */
    public static final String JSON_PARAM_NAME_WRITE_IN_ALPHABET = "writeInAlphabet";

    /**
     * The name of the json attribute numberOfVotingCardsToGenerate
     */
    public static final String JSON_ATTRIBUTE_NUMBER_OF_VC_TO_GENERATE = "numberOfVotingCardsToGenerate";

    /**
     * The name of the json attribute choiceCodesEncryptionKey
     */
    public static final String JSON_ATTRIBUTE_CHOICE_CODES_ENCRYPTION_KEY = "choiceCodesEncryptionKey";

    // Avoid instantiation.
    private JsonConstants() {
    }
}
