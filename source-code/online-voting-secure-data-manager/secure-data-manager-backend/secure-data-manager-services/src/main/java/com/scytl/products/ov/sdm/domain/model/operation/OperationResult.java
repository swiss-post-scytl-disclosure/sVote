/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.operation;

public class OperationResult {
	
	private int error;
	private String message;
	private String exception;
	
	public int getError() {
		return error;
	}
	public void setError(int errorCode) {
		this.error = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exceptionName) {
		this.exception = exceptionName;
	}
	
}
