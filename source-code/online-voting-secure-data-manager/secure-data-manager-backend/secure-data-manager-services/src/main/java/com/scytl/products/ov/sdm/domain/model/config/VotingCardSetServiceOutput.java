/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.config;

public class VotingCardSetServiceOutput {

    private String verificationCardSetID;


    /**
     * Sets new verificationCardSetID.
     *
     * @param verificationCardSetID New value of verificationCardSetID.
     */
    public void setVerificationCardSetID(String verificationCardSetID) {
        this.verificationCardSetID = verificationCardSetID;
    }

    /**
     * Gets verificationCardSetID.
     *
     * @return Value of verificationCardSetID.
     */
    public String getVerificationCardSetID() {
        return verificationCardSetID;
    }
}