/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.clients;

import javax.validation.constraints.NotNull;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public interface OrchestratorClient {

    String TENANT_ID_PARAM = "tenantId";

    String ELECTION_EVENT_ID_PARAM = "electionEventId";

    String VERIFICATION_CARD_SET_ID_PARAM = "verificationCardSetId";

    String BALLOT_BOX_ID_PARAM = "ballotBoxId";

    String BALLOT_BOX_PATH = "mixdec/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}";

    String CHUNK_ID_PARAM = "chunkId";

    String CHUNK_COUNT_PARAM = "chunkCount";

    @POST("choicecodes/computeGenerationContributions")
    Call<ResponseBody> compute(@NotNull @Body RequestBody body);

    @GET("choicecodes/tenant/{tenantId}/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/chunkId/{chunkId}/computeGenerationContributions")
    @Streaming
    Call<ResponseBody> download(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID_PARAM) String verificationCardSetId,
            @Path(CHUNK_ID_PARAM) int chunkId);

    @GET("choicecodes/tenant/{tenantId}/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/generationContributions/status")
    Call<ResponseBody> getChoiceCodesComputationStatus(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID_PARAM) String verificationCardSetId,
            @Query(CHUNK_COUNT_PARAM) int chunkCount);

    @GET(BALLOT_BOX_PATH + "/status")
    Call<ResponseBody> getBallotBoxMixingStatus(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId, @Path(BALLOT_BOX_ID_PARAM) String ballotBoxId);

    @GET(BALLOT_BOX_PATH + "/nodeoutputs")
    @Headers("Accept:application/octet-stream")
    @Streaming
    Call<ResponseBody> getMixDecNodeOutputs(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId, @Path(BALLOT_BOX_ID_PARAM) String ballotBoxId);
}
