/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class CleansingOutputsDownloadServiceException extends RuntimeException {

	private static final long serialVersionUID = 1739724214530186991L;

	public CleansingOutputsDownloadServiceException(Throwable cause) {
		super(cause);
	}
	
	public CleansingOutputsDownloadServiceException(String message) {
		super(message);
	}
	
	public CleansingOutputsDownloadServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
