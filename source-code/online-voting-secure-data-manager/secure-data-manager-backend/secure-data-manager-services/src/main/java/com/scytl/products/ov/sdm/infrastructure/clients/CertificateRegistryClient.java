/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.clients;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpHeaders;

import com.scytl.products.ov.sdm.domain.model.certificateRegistry.Certificate;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CertificateRegistryClient {

    String QUERY_PARAMETER_TENANT_ID = "tenantId";

    String QUERY_PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    @POST("certificates/tenant/{tenantId}/electionevent/{electionEventId}")
    Call<ResponseBody> saveCertificate(@Path(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @Path(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @NotNull @Body final Certificate certificate);

    @Headers(HttpHeaders.CONTENT_TYPE + ": " + MediaType.APPLICATION_JSON)
    @POST("certificates/tenant/{tenantId}")
    Call<ResponseBody> saveCertificate(@Path(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @NotNull @Body final Certificate certificate);

    @POST("certificates")
    Call<ResponseBody> saveCertificate(@NotNull @Body final Certificate certificate);

}
