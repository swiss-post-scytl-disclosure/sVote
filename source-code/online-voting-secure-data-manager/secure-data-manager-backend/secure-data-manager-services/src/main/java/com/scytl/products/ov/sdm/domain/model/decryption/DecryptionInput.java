/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.decryption;

import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.products.ov.commons.beans.Ballot;

/**
 *
 */
public class DecryptionInput {

    private Ballot ballot;

    private String tenantId;

    private String ballotBoxId;

    private String serializedPrivateKey;

    private String privateKeyPEM;

    private String certId;

    private ElGamalEncryptionParameters elGamalEncryptionParameters;

    private String writeInAlphabet;

    public ElGamalEncryptionParameters getElGamalEncryptionParameters() {
        return elGamalEncryptionParameters;
    }

    public void setElGamalEncryptionParameters(ElGamalEncryptionParameters elGamalEncryptionParameters) {
        this.elGamalEncryptionParameters = elGamalEncryptionParameters;
    }

    public Ballot getBallot() {
        return ballot;
    }

    public void setBallot(Ballot ballot) {
        this.ballot = ballot;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public void setBallotBoxId(String ballotBoxId) {
        this.ballotBoxId = ballotBoxId;
    }

    public String getSerializedPrivateKey() {
        return serializedPrivateKey;
    }

    public void setSerializedPrivateKey(String serializedPrivateKey) {
        this.serializedPrivateKey = serializedPrivateKey;
    }

    public String getCertId() {
        return certId;
    }

    public void setCertId(String certId) {
        this.certId = certId;
    }

    public String getPrivateKeyPEM() {
        return privateKeyPEM;
    }

    public void setPrivateKeyPEM(String privateKeyPEM) {
        this.privateKeyPEM = privateKeyPEM;
    }

    public String getWriteInAlphabet() {
        return writeInAlphabet;
    }

    public void setWriteInAlphabet(String writeInAlphabet) {
        this.writeInAlphabet = writeInAlphabet;
    }
}
