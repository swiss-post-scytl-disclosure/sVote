/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import org.springframework.stereotype.Component;

/**
 * Implementation of {@link DatabaseManagerFactory}.
 */
@Component
public final class DatabaseManagerFactoryImpl
        implements DatabaseManagerFactory {

    @Override
    public DatabaseManager newDatabaseManager(final String url) {
        return newDatabaseManager(url, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    @Override
    public DatabaseManager newDatabaseManager(final String url,
            final String username, final String password) {
        return new DatabaseManagerImpl(url, username, password);
    }
}
