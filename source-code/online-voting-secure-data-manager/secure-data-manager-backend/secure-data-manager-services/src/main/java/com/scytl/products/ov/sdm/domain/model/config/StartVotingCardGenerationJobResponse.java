/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.config;

import com.scytl.products.ov.sdm.domain.common.JobStatus;

public class StartVotingCardGenerationJobResponse {

    private String jobId;
    private JobStatus jobStatus;
    private String createdAt;
    private String verificationCardSetId;

    protected StartVotingCardGenerationJobResponse() {}

    public StartVotingCardGenerationJobResponse(final String jobId, final JobStatus jobStatus, final String createdAt,
                                                final String verificationCardSetId) {
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.createdAt = createdAt;
        this.verificationCardSetId = verificationCardSetId;
    }

    public String getJobId() {
        return jobId;
    }

    public JobStatus getJobStatus() {
        return jobStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getVerificationCardSetId() {
        return verificationCardSetId;
    }
}
