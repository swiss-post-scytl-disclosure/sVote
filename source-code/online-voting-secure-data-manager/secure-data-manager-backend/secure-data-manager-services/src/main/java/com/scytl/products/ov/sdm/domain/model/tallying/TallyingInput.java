/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.tallying;

import com.scytl.products.ov.commons.beans.Ballot;

/**
 * Class which represents the input for the Counting Service
 */
public class TallyingInput {

    private Ballot ballot;

    private String tenantID;

    private String ballotBoxID;

    private String writeInSeparator;

    private String certId;

    private String privateKeyPEM;

    public Ballot getBallot() {
        return ballot;
    }

    public void setBallot(Ballot ballot) {
        this.ballot = ballot;
    }

    public String getTenantID() {
        return tenantID;
    }

    public void setTenantID(String tenantID) {
        this.tenantID = tenantID;
    }

    public String getBallotBoxID() {
        return ballotBoxID;
    }

    public void setBallotBoxID(String ballotBoxID) {
        this.ballotBoxID = ballotBoxID;
    }

    public void setCertId(String certId) {
        this.certId = certId;
    }

    public String getCertId() {
        return certId;
    }

    public void setPrivateKeyPEM(String privateKeyPEM) {
    	this.privateKeyPEM = privateKeyPEM;
    }

    public String getPrivateKeyPEM() {
        return privateKeyPEM;
    }

    public String getWriteInSeparator() {
        return writeInSeparator;
    }

    public void setWriteInSeparator(String writeInSeparator) {
        this.writeInSeparator = writeInSeparator;
    }

}
