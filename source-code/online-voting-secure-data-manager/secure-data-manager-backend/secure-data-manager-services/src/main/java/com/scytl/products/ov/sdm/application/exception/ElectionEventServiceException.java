/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class ElectionEventServiceException extends RuntimeException {

	private static final long serialVersionUID = -1380844286483578202L;

	public ElectionEventServiceException(Throwable cause) {
		super(cause);
	}
	
	public ElectionEventServiceException(String message) {
		super(message);
	}
	
	public ElectionEventServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
