/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.mixing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

/**
 * The contents of the request body of the SDM back-end's mixing endpoint.
 */
public class MixingData {

  private String privateKeyPEM;
  private String electoralAuthorityPrivateKey;

  @JsonCreator
  public MixingData(
      @JsonProperty("privateKeyPEM") String privateKeyPEM,
      @JsonProperty("serializedPrivateKey") String electoralAuthorityPrivateKey
      ) {
    this.privateKeyPEM = privateKeyPEM;
    this.electoralAuthorityPrivateKey = electoralAuthorityPrivateKey;
  }


  public String getElectoralAuthorityPrivateKey() {
    return electoralAuthorityPrivateKey;
  }

  /**
   * Returns the current value of the field privateKeyPEM.
   *
   * @return Returns the privateKeyPEM.
   */
  public String getPrivateKeyPEM() {
    return privateKeyPEM;
  }
}
