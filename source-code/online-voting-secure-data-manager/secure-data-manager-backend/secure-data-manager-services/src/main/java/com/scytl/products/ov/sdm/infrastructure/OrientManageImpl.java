/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import org.springframework.stereotype.Component;

import com.orientechnologies.orient.core.Orient;

/**
 * Implementation of {@link OrientManager}.
 */
@Component
public final class OrientManageImpl implements OrientManager {
    private final Orient orient;

    /**
     * Constructor. For internal use only.
     *
     * @param orient
     */
    OrientManageImpl(final Orient orient) {
        this.orient = orient;
    }

    /**
     * Constructor.
     */
    public OrientManageImpl() {
        this(Orient.instance());
    }

    @Override
    public boolean isActive() {
        return orient.isActive();
    }

    @Override
    public void shutdown() {
        orient.shutdown();
    }
}
