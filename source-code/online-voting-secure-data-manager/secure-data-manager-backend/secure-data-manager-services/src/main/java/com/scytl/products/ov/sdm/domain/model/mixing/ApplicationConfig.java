/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.mixing;

public class ApplicationConfig {

    private Integer concurrencyLevel;

    private Integer numberOfParallelBatches;

    private Integer numOptions;

    private Integer minimumBatchSize;

    private MixerConfig mixer;

    private VerifierConfig verifier;

    public Integer getConcurrencyLevel() {
        return concurrencyLevel;
    }

    public void setConcurrencyLevel(final Integer concurrencyLevel) {
        this.concurrencyLevel = concurrencyLevel;
    }

    public Integer getNumOptions() {
        return numOptions;
    }

    public void setNumOptions(final Integer numOptions) {
        this.numOptions = numOptions;
    }

    public MixerConfig getMixer() {
        return mixer;
    }

    public void setMixer(final MixerConfig mixer) {
        this.mixer = mixer;
    }

    public VerifierConfig getVerifier() {
        return verifier;
    }

    public void setVerifier(final VerifierConfig verifier) {
        this.verifier = verifier;
    }

    public boolean hasMixerConfig() {
        return this.mixer != null;
    }

    public boolean hasVerifierConfig() {
        return this.verifier != null;
    }

    public Integer getMinimumBatchSize() {
        return minimumBatchSize;
    }

    public void setMinimumBatchSize(final Integer minimumBatchSize) {
        this.minimumBatchSize = minimumBatchSize;
    }

    public Integer getNumberOfParallelBatches() {
        return numberOfParallelBatches;
    }

    public void setNumberOfParallelBatches(final Integer numberOfParallelBatches) {
        this.numberOfParallelBatches = numberOfParallelBatches;
    }
}
