/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class BallotBoxServiceException extends RuntimeException {

	private static final long serialVersionUID = -188943882949381942L;

	public BallotBoxServiceException(Throwable cause) {
		super(cause);
	}
	
	public BallotBoxServiceException(String message) {
		super(message);
	}
	
	public BallotBoxServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
