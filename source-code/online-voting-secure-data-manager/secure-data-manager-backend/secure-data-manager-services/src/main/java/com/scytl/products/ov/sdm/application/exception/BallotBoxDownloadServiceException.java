/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class BallotBoxDownloadServiceException extends RuntimeException {

	private static final long serialVersionUID = -5397322233066334805L;

	public BallotBoxDownloadServiceException(Throwable cause) {
		super(cause);
	}
	
	public BallotBoxDownloadServiceException(String message) {
		super(message);
	}
	
	public BallotBoxDownloadServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
