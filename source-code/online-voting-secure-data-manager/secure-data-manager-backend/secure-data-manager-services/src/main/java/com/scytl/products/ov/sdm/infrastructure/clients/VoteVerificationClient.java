/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.clients;

import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.Call;

import javax.validation.constraints.NotNull;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public interface VoteVerificationClient {

    String TENANT_ID_PARAM = "tenantId";

    String ELECTION_EVENT_ID_PARAM = "electionEventId";

    String VERIFICATION_CARD_SET_ID_PARAM = "verificationCardSetId";

    String ADMIN_BOARD_ID_PARAM = "adminBoardId";

    String VERIFICATION_CARD_ID_PARAM = "verificationCardId";

    @POST("codesmappingdata/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveCodesMappingData(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID_PARAM) String verificationCardSetId,
            @Path(ADMIN_BOARD_ID_PARAM) String adminBoardId, @NotNull @Body RequestBody body);

    @POST("verificationcarddata/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveVerificationCardData(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID_PARAM) String votingCardSetId,
            @Path(ADMIN_BOARD_ID_PARAM) String adminBoardId, @NotNull @Body RequestBody body);

    @POST("verificationcardsetdata/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveVerificationCardSetData(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID_PARAM) String votingCardSetId,
            @Path(ADMIN_BOARD_ID_PARAM) String adminBoardId, @NotNull @Body RequestBody body);

    @POST("derivedkeys/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveVerificationCardDerivedKeys(@Path(TENANT_ID_PARAM) String tenantId,
            @Path(ELECTION_EVENT_ID_PARAM) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID_PARAM) String verificationCardSetId,
            @Path(ADMIN_BOARD_ID_PARAM) String adminBoardId, @NotNull @Body RequestBody body);
}
