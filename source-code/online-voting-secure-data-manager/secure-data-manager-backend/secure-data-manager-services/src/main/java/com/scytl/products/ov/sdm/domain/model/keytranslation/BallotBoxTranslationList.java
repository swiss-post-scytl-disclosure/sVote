/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.keytranslation;

import java.util.ArrayList;
import java.util.List;

public class BallotBoxTranslationList {

    private List<BallotBoxTranslation> ballotBoxTranslations = new ArrayList<>();

    public void addKeyTranslation(BallotBoxTranslation translation) {
        ballotBoxTranslations.add(translation);
    }

    /**
     * Gets ballotBoxTranslations.
     *
     * @return Value of ballotBoxTranslations.
     */
    public List<BallotBoxTranslation> getBallotBoxTranslations() {
        return ballotBoxTranslations;
    }

    /**
     * Sets new ballotBoxTranslations.
     *
     * @param ballotBoxTranslations
     *            New value of ballotBoxTranslations.
     */
    public void setBallotBoxTranslations(List<BallotBoxTranslation> ballotBoxTranslations) {
        this.ballotBoxTranslations = ballotBoxTranslations;
    }
}
