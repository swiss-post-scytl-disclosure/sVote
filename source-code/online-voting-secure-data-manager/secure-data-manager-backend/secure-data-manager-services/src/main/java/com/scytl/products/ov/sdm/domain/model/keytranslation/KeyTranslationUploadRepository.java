/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.keytranslation;//package com.scytl.products.ov.sdm.domain.model.keytranslation;

import java.io.IOException;


/**
 * @author lsantamaria
 * @date 11/10/16
 * <p>
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 * <p>
 * All rights reserved.
 */
public interface KeyTranslationUploadRepository {

    /**
     * Check if AG endpoint is available
     * @return endpoint availability
     */

    boolean checkHealthAG();

    /**
     * Upload election event id-alias mapping to keytranslation repository
     * @param translations
     * @param electionEventId
     * @return response result
     * @throws IOException 
     */
    boolean createUpdateElectionEventKeyTranslations(ElectionEventTranslationList translations);


    /**
     * Upload a set of ballot box alias-id translations belonging to a particular election  event
     *
     * @param electionEventId electionEventId
     * @param translations
     * @return response result
     * @throws IOException
     */
    boolean createUpdateBallotBoxKeyTranslations(String electionEventId, BallotBoxTranslationList translations);


    /**
     * Check if for a given election event exists an alias as key translation
     * @param electionEventId
     * @return
     */
    boolean isAlreadyUploaded(String electionEventId);
}
