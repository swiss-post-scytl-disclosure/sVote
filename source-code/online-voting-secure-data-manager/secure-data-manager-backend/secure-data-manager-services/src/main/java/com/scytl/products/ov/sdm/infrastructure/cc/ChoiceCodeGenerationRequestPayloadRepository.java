/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.cc;

import com.scytl.products.ov.commons.dto.ChoiceCodeGenerationReqPayload;

/**
 * Manages the storage and retrieval of choice code generation request payloads.
 */
public interface ChoiceCodeGenerationRequestPayloadRepository {
    /**
     * Stores a choice code generation request payload.
     * 
     * @param payload
     *            the payload to store.
     * @throws PayloadStorageException
     *             if the storage did not succeed
     */
    void store(ChoiceCodeGenerationReqPayload payload) throws PayloadStorageException;

    /**
     * Retrieves a choice code generation request payload.
     * 
     * @param electionEventId
     *            the identifier of the election event the verification card set
     *            belongs to
     * @param verificationCardSetId
     *            the identifier of the verification card set the payload is for
     * @param chunkId
     *            the chunk identifier           
     * @return the requested choice code generation request payload
     * @throws PayloadStorageException
     *             if retrieving the payload did not succeed
     */
    ChoiceCodeGenerationReqPayload retrieve(String electionEventId, String verificationCardSetId, int chunkId)
            throws PayloadStorageException;
    
    /**
     * Removes all the payloads for given election event and verification card
     * set.
     * 
     * @param electionEventId the election event identifier
     * @param verificationCardSetId the verification card set identifier
     * @throws PayloadStorageException failed to remove payloads
     */
    void remove(String electionEventId, String verificationCardSetId)
            throws PayloadStorageException;
    
    /**
     * Returns the number of payloads for given election event and verification card
     * set.
     * @param electionEventId the election event identifier
     * @param verificationCardSetId the verification card set identifier
     * @return the number of payloads
     * @throws PayloadStorageException failed to get the number of payloads
     */
    int getCount(String electionEventId, String verificationCardSetId)
            throws PayloadStorageException;
}
