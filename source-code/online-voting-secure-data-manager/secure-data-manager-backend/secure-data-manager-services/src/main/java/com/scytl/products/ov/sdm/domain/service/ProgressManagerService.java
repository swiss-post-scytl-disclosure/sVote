/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service;

import java.net.URI;
import java.util.List;
import java.util.concurrent.Future;

import com.scytl.products.ov.sdm.domain.common.GenericJobStatus;

/**
 * This interface defines the API for a service which accesses to ProgressManager
 */
public interface ProgressManagerService<T extends GenericJobStatus> {

    
    /**
     * @param jobId the job identifier
     * @return the job status
     */
     T getForJob(final String jobId);

    Future<T> registerJob(final String jobId, final URI statusUri);

    List<T> getAllByStatus(final String status);

    List<T> getAll();
}
