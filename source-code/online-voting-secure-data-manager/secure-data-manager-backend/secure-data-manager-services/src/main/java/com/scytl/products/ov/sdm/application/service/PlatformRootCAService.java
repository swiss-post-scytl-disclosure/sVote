/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

import org.springframework.stereotype.Service;

import com.scytl.products.ov.commons.path.PathResolver;
import com.scytl.products.ov.sdm.domain.service.ConfigConstants;

@Service
public class PlatformRootCAService extends FileRootCAService {
    public PlatformRootCAService(PathResolver pathResolver) {
        super(pathResolver, ConfigConstants.CONFIG_FILE_NAME_PLATFORM_ROOT_CA);
    }
}
