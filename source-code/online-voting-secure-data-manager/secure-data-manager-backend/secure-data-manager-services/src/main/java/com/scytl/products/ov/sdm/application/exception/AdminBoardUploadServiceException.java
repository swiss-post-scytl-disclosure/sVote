/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class AdminBoardUploadServiceException extends RuntimeException {

	private static final long serialVersionUID = 8026276349431237116L;

	public AdminBoardUploadServiceException(Throwable cause) {
		super(cause);
	}
	
	public AdminBoardUploadServiceException(String message) {
		super(message);
	}
	
	public AdminBoardUploadServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
