/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.config;


import com.scytl.products.ov.sdm.domain.common.GenericJobStatus;
import com.scytl.products.ov.sdm.domain.common.JobProgressDetails;
import com.scytl.products.ov.sdm.domain.common.JobStatus;

public class VotingCardGenerationJobStatus extends GenericJobStatus {

    private String verificationCardSetId;
    private int generatedCount;
    private int errorCount;

    protected VotingCardGenerationJobStatus(){}

    public VotingCardGenerationJobStatus(final String jobId) {
        super(jobId, JobStatus.UNKNOWN, "", JobProgressDetails.UNKNOWN);
    }

    public String getVerificationCardSetId() {
        return verificationCardSetId;
    }

    public int getGeneratedCount() {
        return generatedCount;
    }

    public int getErrorCount() {
        return errorCount;
    }


    protected void setVerificationCardSetId(final String verificationCardSetId) {
        this.verificationCardSetId = verificationCardSetId;
    }

    protected void setGeneratedCount(final int generatedCount) {
        this.generatedCount = generatedCount;
    }

    protected void setErrorCount(final int errorCount) {
        this.errorCount = errorCount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("status=").append(getStatus());
        sb.append(", statusDetails=").append(getStatusDetails());
        sb.append(", verificationCardSetId=").append(verificationCardSetId);
        sb.append(", generatedCount=").append(generatedCount);
        sb.append(", errorCount=").append(errorCount);
        sb.append('}');
        return sb.toString();
    }
}
