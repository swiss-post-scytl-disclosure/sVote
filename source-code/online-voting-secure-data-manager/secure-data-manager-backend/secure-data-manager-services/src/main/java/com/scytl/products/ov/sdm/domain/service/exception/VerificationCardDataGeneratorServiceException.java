/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.exception;

public class VerificationCardDataGeneratorServiceException extends RuntimeException {
  
	private static final long serialVersionUID = 262169831436423910L;

	public VerificationCardDataGeneratorServiceException(Throwable cause) {
        super(cause);
    }
	
	public VerificationCardDataGeneratorServiceException(String message) {
        super(message);
    }
	
	public VerificationCardDataGeneratorServiceException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
