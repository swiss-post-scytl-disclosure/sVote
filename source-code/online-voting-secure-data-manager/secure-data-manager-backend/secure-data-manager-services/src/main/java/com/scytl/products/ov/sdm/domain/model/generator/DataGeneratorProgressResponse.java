/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.generator;

import com.scytl.products.ov.sdm.domain.common.GenericJobStatus;

/**
 * This bean holds all the information returned by a data generator progress.
 */
public class DataGeneratorProgressResponse extends GenericJobStatus {

    private int percentage;

    private Long remainingTime;

    /**
     * @return Returns the remainingTime.
     */
    public Long getRemainingTime() {
        return remainingTime;
    }

    /**
     * @param remainingTime
     *            The remainingTime to set.
     */
    public void setRemainingTime(final Long remainingTime) {
        this.remainingTime = remainingTime;
    }

    /**
     * @return Returns the percentage.
     */
    public int getPercentage() {
        return percentage;
    }

    /**
     * @param percentage
     *            The percentage to set.
     */
    public void setPercentage(final int percentage) {
    	this.percentage = percentage;
    }

}
