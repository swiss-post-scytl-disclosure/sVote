/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.exception;

public class VotingCardSetUploadRepositoryException extends RuntimeException {
  
	private static final long serialVersionUID = 7085337598214927323L;

	public VotingCardSetUploadRepositoryException(Throwable cause) {
        super(cause);
    }
	
	public VotingCardSetUploadRepositoryException(String message) {
        super(message);
    }
	
	public VotingCardSetUploadRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
