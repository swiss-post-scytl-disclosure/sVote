/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.config;

import java.util.List;
import java.util.Map;

public class CreateElectoralBoardKeyPairInput {

    private String outputFolder;

    private Map<String, List<String>> ballotMappings;

    public String getOutputFolder() {
        return outputFolder;
    }

    public void setOutputFolder(final String outputFolder) {
        this.outputFolder = outputFolder;
    }

    public Map<String, List<String>> getBallotMappings() {
        return ballotMappings;
    }

    public void setBallotMappings(final Map<String, List<String>> ballotMappings) {
        this.ballotMappings = ballotMappings;
    }
}
