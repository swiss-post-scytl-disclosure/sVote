/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.sdmconfig;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class I18nConfiguration {

    private Language defaultLanguage;

    private List<Language> supportedLanguages = new ArrayList<>();

    public void addLanguage(final Language language, boolean isDefault) {
        if (!languageExists(language)) {
            supportedLanguages.add(language);
        }
        
        if (isDefault) {
            defaultLanguage = language;
        }
    }

    private boolean languageExists(Language language) {
        return supportedLanguages.stream().anyMatch(l -> l.getCode().equals(language.getCode()));
    }

    @JsonProperty("default")
    public String getDefaultLanguage() {
        return defaultLanguage.getCode();
    }

    @JsonProperty("languages")
    public List<Language> getSupportedLanguages() {
        return supportedLanguages;
    }
}
