/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class KeyStoreServiceException extends RuntimeException {

	private static final long serialVersionUID = 3880324231542129840L;

	public KeyStoreServiceException(Throwable cause) {
		super(cause);
	}
	
	public KeyStoreServiceException(String message) {
		super(message);
	}
	
	public KeyStoreServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
