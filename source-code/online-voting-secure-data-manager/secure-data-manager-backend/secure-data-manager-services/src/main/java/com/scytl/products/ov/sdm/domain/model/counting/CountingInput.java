/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.counting;

import com.scytl.products.ov.commons.beans.Ballot;

/**
 * Class which represents the input for the Counting Service
 */
public class CountingInput {

    private Ballot ballot;

    private String tenantID;

    private String ballotBoxID;

    public Ballot getBallot() {
        return ballot;
    }

    public void setBallot(Ballot ballot) {
        this.ballot = ballot;
    }

    public String getTenantID() {
        return tenantID;
    }

    public void setTenantID(String tenantID) {
        this.tenantID = tenantID;
    }

    public String getBallotBoxID() {
        return ballotBoxID;
    }

    public void setBallotBoxID(String ballotBoxID) {
        this.ballotBoxID = ballotBoxID;
    }
}
