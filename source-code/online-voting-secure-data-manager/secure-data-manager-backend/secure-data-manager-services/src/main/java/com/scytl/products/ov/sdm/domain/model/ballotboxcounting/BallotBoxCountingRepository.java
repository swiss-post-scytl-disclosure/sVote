/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.ballotboxcounting;

import com.scytl.products.ov.sdm.domain.model.EntityRepository;

/**
 * Interface providing operations with ballot box Counting.
 */
public interface BallotBoxCountingRepository extends EntityRepository {

}
