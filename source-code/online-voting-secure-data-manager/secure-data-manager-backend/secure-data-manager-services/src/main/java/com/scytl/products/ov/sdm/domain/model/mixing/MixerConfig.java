/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.mixing;

import java.util.Map;

public class MixerConfig {

    private boolean mix;

    private Integer warmUpSize;

    private Map<String, String> input;

    private String outputDirectory;

    public boolean isMix() {
        return mix;
    }

    public void setMix(final boolean mix) {
        this.mix = mix;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public void setOutputDirectory(final String outputDirectory) {
    	this.outputDirectory = outputDirectory;
    }

    public Map<String, String> getInput() {
        return input;
    }

    public void setInput(final Map<String, String> input) {
    	this.input = input;
    }

    public Integer getWarmUpSize() {
        return warmUpSize;
    }

    public void setWarmUpSize(Integer warmUpSize) {
    	this.warmUpSize = warmUpSize;
    }
}
