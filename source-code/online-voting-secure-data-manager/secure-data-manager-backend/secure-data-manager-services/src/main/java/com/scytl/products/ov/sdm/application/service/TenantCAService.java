/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.scytl.products.ov.commons.path.PathResolver;
import com.scytl.products.ov.sdm.domain.service.ConfigConstants;

@Service
public class TenantCAService extends FileRootCAService {

    public TenantCAService(PathResolver pathResolver, @Value("${tenantID}") String tenantId) {
        super(pathResolver, String.format(ConfigConstants.CONFIG_FILE_NAME_TENANT_CA_PATTERN, tenantId));
    }
}
