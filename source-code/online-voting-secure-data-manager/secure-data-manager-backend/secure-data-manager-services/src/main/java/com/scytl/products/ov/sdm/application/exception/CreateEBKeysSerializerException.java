/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.exception;

public class CreateEBKeysSerializerException extends RuntimeException {

	private static final long serialVersionUID = 4330090041872284503L;

	public CreateEBKeysSerializerException(Throwable cause) {
		super(cause);
	}
	
	public CreateEBKeysSerializerException(String message) {
		super(message);
	}
	
	public CreateEBKeysSerializerException(String message, Throwable cause) {
		super(message, cause);
	}
}
