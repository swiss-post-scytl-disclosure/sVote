/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service.impl.progress;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.scytl.products.ov.sdm.domain.model.generator.DataGeneratorProgressResponse;
import com.scytl.products.ov.sdm.domain.service.ProgressManagerService;

/**
 * This implementation is meant to support old-style progress update still used for election securitization
 */
@Service
public class DefaultProgressManagerService implements ProgressManagerService<DataGeneratorProgressResponse> {

    @Autowired
    private RestTemplate restClient;

    @Value("${PROGRESS_URL}")
    private String configServiceBaseUrl;

    @Override
    public DataGeneratorProgressResponse getForJob(final String jobId) {

		final URI uri = UriComponentsBuilder.fromUriString(configServiceBaseUrl + "/{id}").buildAndExpand(jobId)
				.toUri();
		return restClient.getForObject(uri, DataGeneratorProgressResponse.class);
    }

    /**
     * not to be used
     * 
     * @param jobId
     *            ignored
     * @param statusUri
     *            ignored
     * @return null
     */
    @Override
    public Future<DataGeneratorProgressResponse> registerJob(final String jobId, final URI statusUri) {
        return null;
    }

    /**
     * not to be used
     * 
     * @param status
     *            ignored
     * @return empty list
     */
    @Override
    public List<DataGeneratorProgressResponse> getAllByStatus(final String status) {
        return Collections.emptyList();
    }

    /**
     * not to be used
     * 
     * @return empty list
     */
    @Override
    public List<DataGeneratorProgressResponse> getAll() {
        return Collections.emptyList();
    }

}
