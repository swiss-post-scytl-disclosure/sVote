/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service;

import java.io.BufferedReader;
import java.io.IOException;

public interface VerificationCardDataGeneratorService {

    /**
     * Requests the generation of verification card IDs.
     *
     * @param electionEventId
     *            the election event the verification cards belong to
     * @param verificationCardSetId
     *            the verification card set identifier
     * @param numberOfVotingCards
     *            the number of verification card IDs to generate
     * @return a buffered reader with a verification card ID per line
     */
    BufferedReader precompute(String electionEventId, String verificationCardSetId, int numberOfVotingCards)
            throws IOException;
}
