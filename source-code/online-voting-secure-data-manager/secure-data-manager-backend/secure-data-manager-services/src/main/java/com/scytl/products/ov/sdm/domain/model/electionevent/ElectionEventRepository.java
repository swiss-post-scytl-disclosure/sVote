/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.electionevent;

import java.util.List;

import com.scytl.products.ov.sdm.application.exception.DatabaseException;
import com.scytl.products.ov.sdm.domain.model.EntityRepository;

/**
 * Interface providing operations with election event.
 */
public interface ElectionEventRepository extends EntityRepository {

    /**
     * Returns the election event alias from the election event identified by
     * the given id.
     *
     * @param electionEventId
     *            identifies the election event where to search.
     * @return the election event alias.
     */
    String getElectionEventAlias(String electionEventId);

    /**
     * Lists all the identifiers.
     *
     * @return the identifiers
     * @throws DatabaseException
     *             failed to list the identifier
     */
    List<String> listIds() throws DatabaseException;
}
