/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

/**
 * Database manager is used to manage a single OrientDB database.
 * <p>
 * Implementation encapsulates database URL and a user and allows to create,
 * open and drop the corresponding database.
 * <p>
 * Implementation must be thread-safe.
 */
public interface DatabaseManager {
    /**
     * Creates the database if not exists. The encapsulated database user is
     * added to administrative role.
     *
     * @throws OException
     *             failed to create the database.
     */
    void createDatabase() throws OException;

    /**
     * Drops the database if exists.
     *
     * @throws OException
     *             failed to drop the database.
     */
    void dropDatabase() throws OException;

    /**
     * Opens the database, returning a {@link ODatabaseDocument} instance which
     * is already activated on the current thread. Client is responsible for
     * closing the returned object after use.
     *
     * @return the database interface
     * @throws OException
     *             failed to open the database.
     */
    ODatabaseDocument openDatabase() throws OException;
}
