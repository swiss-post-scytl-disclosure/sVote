/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.administrationauthority;

public class ActivateOutputData {

	private String issuerPublicKeyPEM;

	private String serializedSubjectPublicKey;

	public String getIssuerPublicKeyPEM() {
		return issuerPublicKeyPEM;
	}

	public void setIssuerPublicKeyPEM(String issuerPublicKeyPEM) {
		this.issuerPublicKeyPEM = issuerPublicKeyPEM;
	}

	public String getSerializedSubjectPublicKey() {
		return serializedSubjectPublicKey;
	}

	public void setSerializedSubjectPublicKey(String serializedSubjectPublicKey) {
		this.serializedSubjectPublicKey = serializedSubjectPublicKey;
	}
}
