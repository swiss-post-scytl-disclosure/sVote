/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.electionevent;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.scytl.products.ov.commons.beans.ElectionEvent;
import com.scytl.products.ov.sdm.application.exception.DatabaseException;
import com.scytl.products.ov.sdm.domain.model.electionevent.ElectionEventRepository;
import com.scytl.products.ov.sdm.infrastructure.AbstractEntityRepository;
import com.scytl.products.ov.sdm.infrastructure.DatabaseManager;
import com.scytl.products.ov.sdm.infrastructure.JsonConstants;

/**
 * Implementation of operations with election event.
 */
@Repository
public class ElectionEventRepositoryImpl extends AbstractEntityRepository
        implements ElectionEventRepository {

    /**
     * Constructor
     *
     * @param databaseManager
     *            the injected database manager
     */
    @Autowired
    public ElectionEventRepositoryImpl(
            final DatabaseManager databaseManager) {
        super(databaseManager);
    }

    @PostConstruct
    @Override
    public void initialize() throws DatabaseException {
        super.initialize();
    }

    @Override
    public String getElectionEventAlias(final String electionEventId) {
        String sql =
            "select alias from " + entityName() + " where id = :id";
        Map<String, Object> parameters = singletonMap(
            JsonConstants.JSON_ATTRIBUTE_NAME_ID, electionEventId);
        List<ODocument> documents;
        try {
            documents = selectDocuments(sql, parameters, 1);
        } catch (OException e) {
            throw new DatabaseException(
                "Failed to get election event alias.", e);
        }
        return documents.isEmpty() ? ""
                : documents.get(0).field("alias", String.class);
    }

    @Override
    public List<String> listIds() throws DatabaseException {
        String sql = "select id from " + entityName();
        Map<String, Object> parameters = emptyMap();
        List<ODocument> documents;
        try {
            documents = selectDocuments(sql, parameters, -1);
        } catch (OException e) {
            throw new DatabaseException("Failed to list identifiers.", e);
        }
        List<String> ids = new ArrayList<>(documents.size());
        for (ODocument document : documents) {
            ids.add(document.field(JsonConstants.JSON_ATTRIBUTE_NAME_ID,
                String.class));
        }
        return ids;
    }

    @Override
    protected String entityName() {
        return ElectionEvent.class.getSimpleName();
    }
}
