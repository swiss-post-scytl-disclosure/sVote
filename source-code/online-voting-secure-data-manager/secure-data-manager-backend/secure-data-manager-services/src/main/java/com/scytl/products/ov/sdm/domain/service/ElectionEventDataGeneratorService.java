/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service;

import com.scytl.products.ov.sdm.domain.model.generator.DataGeneratorResponse;

import java.io.IOException;

/**
 * This interface defines the API for a service which accesses a generator of election event data.
 */
public interface ElectionEventDataGeneratorService {

	/**
	 * This method generates all the data for an election event.
	 * 
	 * @param electionEventId The identifier of the election event for whom to generate the data.
	 * @return a bean containing information about the result of the generation.
	 */
	DataGeneratorResponse generate(String electionEventId) throws IOException;
}
