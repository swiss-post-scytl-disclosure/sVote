/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.clients;

import javax.ws.rs.core.MediaType;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface ElectionInformationClient {

    String QUERY_PARAMETER_TENANT_ID = "tenantId";

    String QUERY_PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    String QUERY_PARAMETER_BALLOT_BOX_ID = "ballotBoxId";

    @GET("electioneventdata/tenant/{tenantId}/electionevent/{electionEventId}/cast")
    @Streaming
    Call<ResponseBody> getCastedVotingCards(@Path(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @Path(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId);

    @GET("ballotboxes/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}")
    @Headers("Accept:" + MediaType.APPLICATION_OCTET_STREAM)
    @Streaming
    Call<ResponseBody> getRawBallotBox(@Path(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @Path(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @Path(QUERY_PARAMETER_BALLOT_BOX_ID) final String ballotBoxId);

    @GET("cleansingoutputs/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/successfulvotes")
    @Headers("Accept:" + MediaType.APPLICATION_OCTET_STREAM)
    @Streaming
    Call<ResponseBody> downloadSuccessfulVotes(@Path(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @Path(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @Path(QUERY_PARAMETER_BALLOT_BOX_ID) final String ballotBoxId);

    @GET("cleansingoutputs/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/failedvotes")
    @Headers("Accept:" + MediaType.APPLICATION_OCTET_STREAM)
    @Streaming
    Call<ResponseBody> downloadFailedVotes(@Path(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @Path(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @Path(QUERY_PARAMETER_BALLOT_BOX_ID) final String ballotBoxId);
}
