/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.votingcardset;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scytl.products.ov.sdm.domain.model.status.Status;

/**
 * Request body for updating a voting card set.
 */
public class VotingCardSetUpdateInputData {

    private final Status status;

    private final String privateKeyPEM;

    private final String adminBoardId;

    @JsonCreator
    public VotingCardSetUpdateInputData(@JsonProperty("status") Status status,
            @JsonProperty("privateKeyPEM") String privateKeyPEM,
            @JsonProperty("adminBoardId") String adminBoardId) {
        this.status = status;
        this.privateKeyPEM = privateKeyPEM;
        this.adminBoardId = adminBoardId;
    }

    public Status getStatus() {
        return status;
    }

    public String getPrivateKeyPEM() {
        return privateKeyPEM;
    }

    public String getAdminBoardId() {
        return adminBoardId;
    }
}
