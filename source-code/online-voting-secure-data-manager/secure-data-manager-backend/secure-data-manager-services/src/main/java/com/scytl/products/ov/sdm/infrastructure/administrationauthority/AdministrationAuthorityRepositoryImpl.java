/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.administrationauthority;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scytl.products.ov.sdm.application.exception.DatabaseException;
import com.scytl.products.ov.sdm.domain.model.administrationauthority.AdministrationAuthority;
import com.scytl.products.ov.sdm.domain.model.administrationauthority.AdministrationAuthorityRepository;
import com.scytl.products.ov.sdm.infrastructure.AbstractEntityRepository;
import com.scytl.products.ov.sdm.infrastructure.DatabaseManager;

/**
 * Implementation of operations with election event.
 */
@Repository
public class AdministrationAuthorityRepositoryImpl
        extends AbstractEntityRepository
        implements AdministrationAuthorityRepository {

    /**
     * Constructor.
     *
     * @param databaseManager
     */
    @Autowired
    public AdministrationAuthorityRepositoryImpl(
            final DatabaseManager databaseManager) {
        super(databaseManager);
    }

    @PostConstruct
    @Override
    public void initialize() throws DatabaseException {
        super.initialize();
    }

    @Override
    protected String entityName() {
        return AdministrationAuthority.class.getSimpleName();
    }
}
