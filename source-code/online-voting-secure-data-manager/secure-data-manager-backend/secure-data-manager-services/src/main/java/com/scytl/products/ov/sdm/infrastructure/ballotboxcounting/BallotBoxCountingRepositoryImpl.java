/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.infrastructure.ballotboxcounting;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scytl.products.ov.sdm.application.exception.DatabaseException;
import com.scytl.products.ov.sdm.domain.model.ballotboxcounting.BallotBoxCounting;
import com.scytl.products.ov.sdm.domain.model.ballotboxcounting.BallotBoxCountingRepository;
import com.scytl.products.ov.sdm.infrastructure.AbstractEntityRepository;
import com.scytl.products.ov.sdm.infrastructure.DatabaseManager;

/**
 * Implementation of operations with ballot box.
 */
@Repository
public class BallotBoxCountingRepositoryImpl extends
        AbstractEntityRepository implements BallotBoxCountingRepository {

    /**
     * Constructor
     *
     * @param databaseManager
     *            the injected database manager
     */
    @Autowired
    public BallotBoxCountingRepositoryImpl(
            final DatabaseManager databaseManager) {
        super(databaseManager);
    }

    @PostConstruct
    @Override
    public void initialize() throws DatabaseException {
        super.initialize();
    }

    @Override
    protected String entityName() {
        return BallotBoxCounting.class.getSimpleName();
    }
}
