/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.model.keytranslation;

public class BallotBoxTranslation {

    private String ballotBoxId;

    private String tenantId;

    private String electionEventId;

    private String alias;


    /**
     * Sets new ballotBoxId.
     *
     * @param ballotBoxId New value of ballotBoxId.
     */
    public void setBallotBoxId(String ballotBoxId) {
        this.ballotBoxId = ballotBoxId;
    }

    /**
     * Sets new electionEventId.
     *
     * @param electionEventId New value of electionEventId.
     */
    public void setElectionEventId(String electionEventId) {
        this.electionEventId = electionEventId;
    }

    /**
     * Gets ballotBoxId.
     *
     * @return Value of ballotBoxId.
     */
    public String getBallotBoxId() {
        return ballotBoxId;
    }

    /**
     * Gets alias.
     *
     * @return Value of alias.
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets new tenantId.
     *
     * @param tenantId New value of tenantId.
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * Sets new alias.
     *
     * @param alias New value of alias.
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * Gets electionEventId.
     *
     * @return Value of electionEventId.
     */
    public String getElectionEventId() {
        return electionEventId;
    }

    /**
     * Gets tenantId.
     *
     * @return Value of tenantId.
     */
    public String getTenantId() {
        return tenantId;
    }
}
