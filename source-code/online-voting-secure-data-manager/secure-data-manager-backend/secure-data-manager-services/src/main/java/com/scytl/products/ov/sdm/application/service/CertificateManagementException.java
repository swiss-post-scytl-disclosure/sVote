/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.application.service;

/**
 * An exception while retrieving or storing a certificate.
 */
public class CertificateManagementException extends Exception {

    private static final long serialVersionUID = 1588022932761467427L;

    public CertificateManagementException(Throwable cause) {
        super(cause);
    }
}
