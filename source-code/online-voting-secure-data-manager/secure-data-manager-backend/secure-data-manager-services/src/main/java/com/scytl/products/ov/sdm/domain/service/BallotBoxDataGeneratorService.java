/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.domain.service;

import com.scytl.products.ov.sdm.domain.model.generator.DataGeneratorResponse;

import java.io.IOException;

/**
 * This interface defines the API for a service which accesses a generator of ballot box data.
 */
public interface BallotBoxDataGeneratorService {

	/**
	 * This method generates all the data for a ballot box.
	 * 
	 * @param id The identifier of the ballot box for which to generate the data.
	 * @param electionEventId The identifier of the election event to whom this ballot box belongs.
	 * @return a bean containing information about the result of the generation.
	 */
	DataGeneratorResponse generate(String id, String electionEventId) throws IOException;
}
