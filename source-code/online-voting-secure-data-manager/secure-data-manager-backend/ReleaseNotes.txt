Version 0.1.0
Date 18.10.2015

DESCRIPTION:
This is the first version of the Secure Data Manager Backend. This application handles:

- Synchronization with the entities created in the Admin Portal
- Generation of the necessary information for an election Event, in order to allow a Voting Process
- Synchronization with the Voter portal, uploading the generated information
- Integration with the mixing , decryption, and counting services for Ballot Boxes


WHAT'S NEW
Package infrastructure:


sdm-ws-rest module:

It defines the different endpoint which will be called by the frontend of the application

secure-data-manager-services module:

This module handles all the business logic of the application. Through the defined services and repositories
the information will be integrated using a noSqlDatabase (OrientDb)



