/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.plugin;

/**
 * Created by lvazquez on 24/11/2016.
 */

public enum ResultCode {

    UNEXPECTED_ERROR(99),
    GENERAL_ERROR(100),
    INVALID_ACTION(101),
    MISSING_PARAMETER(102),
    PARAMETER_NOT_VALID(103),
    FILE_NOT_FOUND(104),
    ERROR_PARSING_FILE(105),
    CONTENT_NOT_VALID(106),
    USB_NOT_FOUND(107),
    ERROR_COPYING_FILES(108),
    ELECTIONEVENT_ALREADY_IN_USB(109),

    SUCCESS(200);
    

    private final int value;

    private ResultCode(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

}