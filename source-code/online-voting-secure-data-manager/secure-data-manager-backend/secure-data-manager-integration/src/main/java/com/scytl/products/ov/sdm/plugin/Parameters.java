/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.plugin;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lvazquez on 23/11/2016.
 */
public class Parameters {

    private Map<String, String> parametersMap;


    public Parameters() {

        parametersMap = new HashMap<>();
    }

    public Parameters(Map<String, String> map) {

        parametersMap = map;
    }

    /**
     * Add param.
     *
     * @param param
     *            the param
     * @param value
     *            the value
     */
    public void addParam(final String param, final String value) {

        parametersMap.put(param, value);

    }

    /**
     * Gets param.
     *
     * @param param
     *            the param
     * @return the param
     */
    public String getParam(final String param) {

        return parametersMap.get(param);

    }

}
