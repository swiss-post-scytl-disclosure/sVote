/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.plugin;

/**
 * Created by lvazquez on 23/11/2016.
 */
public class ExecutionListenerImpl implements ExecutionListener {
	
	private int error;
	private String progress;
	private String message;

	public void onError(int error) {
		this.error = error;
	}

	public int getError() {
		return error;
	}

	@Override
	public void onProgress(String progress) {
		this.progress = progress;
		
	}

	public String getProgress() {
		return progress;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public void onMessage(String message) {
		this.message = message;
	}
	
}
