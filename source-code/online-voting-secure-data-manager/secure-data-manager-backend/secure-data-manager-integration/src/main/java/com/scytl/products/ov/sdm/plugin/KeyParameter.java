/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.plugin;

/**
 * Created by lvazquez on 23/11/2016.
 */
public enum KeyParameter {
    USB_LETTER,
    SDM_PATH,
    EE_ALIAS,
    EE_ID,
    PRIVATE_KEY;
}
