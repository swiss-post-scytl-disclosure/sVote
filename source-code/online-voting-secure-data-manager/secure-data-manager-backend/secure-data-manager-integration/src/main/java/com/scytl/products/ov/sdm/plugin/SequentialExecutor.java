/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.plugin;

import java.util.List;

public interface SequentialExecutor {

    void execute(List<String> commands, Parameters parameters, ExecutionListener listener);

}
