/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.xml.sax.SAXException;

import com.scytl.products.ov.sdm.plugin.XmlObjectsLoader;

public class XmlObjectsLoaderTest {

    @Test
    public void testXmlDeserialization()
            throws JAXBException, URISyntaxException, IOException, SAXException {

        String resourcePath = this.getClass()
            .getResource("/validPlugin.xml").toURI().getPath();
        XmlObjectsLoader.unmarshal(new File(resourcePath).toPath());
    }

    @Test
    public void validatePath_test(){

    }

    @Test
    public void loadFile_test(){

    }

}
