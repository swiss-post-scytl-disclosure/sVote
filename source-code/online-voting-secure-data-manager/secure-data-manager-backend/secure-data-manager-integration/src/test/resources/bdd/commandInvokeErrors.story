US: #10849 - Create command series executor
Scenario: command terminated unexpectedly by general exception error 100
Given a path of xml file with the commands to invoke /plugins_test_err.xml
When loading the file
When xml ummarshal is correct
When command is invoke ok
Then the ouput of test: 0 should be: 100

Scenario: command terminated unexpectedly by invalid action error 101
Given a path of xml file with the commands to invoke /plugins_test_err.xml
When loading the file
When xml ummarshal is correct
When command is invoke ok
Then the ouput of test: 1 should be: 101

Scenario: command terminated unexpectedly by missing params 102
Given a path of xml file with the commands to invoke /plugins_test_err.xml
When loading the file
When xml ummarshal is correct
When command is invoke ok
Then the ouput of test: 2 should be: 102

Scenario: command terminated unexpectedly by invalid params 103
Given a path of xml file with the commands to invoke /plugins_test_err.xml
When loading the file
When xml ummarshal is correct
When command is invoke ok
Then the ouput of test: 3 should be: 103

Scenario: command terminated unexpectedly by file not found params 104
Given a path of xml file with the commands to invoke /plugins_test_err.xml
When loading the file
When xml ummarshal is correct
When command is invoke ok
Then the ouput of test: 4 should be: 104

Scenario: command terminated unexpectedly by parsing params files 105
Given a path of xml file with the commands to invoke /plugins_test_err.xml
When loading the file
When xml ummarshal is correct
When command is invoke ok
Then the ouput of test: 5 should be: 105

Scenario: command terminated unexpectedly by content not valid 106
Given a path of xml file with the commands to invoke /plugins_test_err.xml
When loading the file
When xml ummarshal is correct
When command is invoke ok
Then the ouput of test: 6 should be: 106