US: #10847 - Export/Import functionality

Scenario: Test positive case of loading xml
Given a valid xml file /validPlugin.xml
When loading the file
When filtering for 'export'
Then the xml should have the following content:
|command				|
|fileConverter.bat 1	|
|fileConverter.bat 2	|
|fileConverter.bat 3	|
|fileConverter.bat 4	|