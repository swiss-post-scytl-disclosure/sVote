/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.db.utils;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;

import java.nio.file.Paths;
import java.util.Properties;

public class Application {


    public static void main(String[] args) {



        if (args.length == 0) {
            initializeContext(false);
        } else {
            initializeContext(true, args);
        }

    }

    private static void initializeContext(Boolean hasArgs, String... args) {


        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {

            if (hasArgs) {
                final String path = Paths.get(args[0]).toString();
                Properties props = new Properties();
                props.put("database.args.path", "plocal:".concat(path));
                context.getEnvironment().getPropertySources().addFirst(new PropertiesPropertySource("props", props));


            }
            context.register(SpringConfig.class);
            System.out.println("created database folders");
            context.refresh();
        }
    }
}
