/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.ui.ws.rs.application;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Beans used in other configuration classes.
 */
@Configuration
public class BaseTestConfig {

    @Bean
    SecureLoggingWriter secureLogger() {
        SecureLoggingWriter secureLogger = mock(SecureLoggingWriter.class);
        doNothing().when(secureLogger).log(any(), any(), any());
        doNothing().when(secureLogger).log(any(), any());

        return secureLogger;
    }

    @Bean
    TransactionInfoProvider transactionInfoProvider() {
        return new TransactionInfoProvider();
    }
}
