/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.ui.ws.rs.application;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.sdm.application.service.IdleStatusService;
import com.scytl.products.ov.sdm.application.service.MixDecValService;
import com.scytl.products.ov.sdm.domain.model.decryption.ValidationJobStatus;
import com.scytl.products.ov.sdm.domain.model.mixing.MixingData;
import com.scytl.products.ov.sdm.domain.service.ProgressManagerService;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class MixDecValResourceTest {

  @Mock
  MixDecValService mixingService;

  @Mock
  IdleStatusService idleStatusService;

  @Mock
  ProgressManagerService<ValidationJobStatus> progressManagerService;

  @Mock
  TransactionInfoProvider transactionInfoProvider;

  @Mock
  SecureLoggingWriter secureLogger;

  @InjectMocks
  MixDecValResource sut = new MixDecValResource();

  @Before
  public void setUp()
      throws ApplicationException, ResourceNotFoundException, GeneralCryptoLibException, IOException {
    when(idleStatusService.isIdReady(any())).thenReturn(true);
    when(mixingService.mixDecryptValidate(any(), any(), any(), any()))
        .thenReturn(true);
  }

  @Test
  public void testGetBallotBox() {
    String electionEventId = "electionEvent";
    String ballotBoxId = "ballotBox";
    MixingData mixingData = new MixingData("","");

    ResponseEntity response = sut.getBallotBox(
        electionEventId,
        ballotBoxId,
        mixingData
    );

    assertTrue(response.getStatusCode().equals(HttpStatus.OK));
  }
}
