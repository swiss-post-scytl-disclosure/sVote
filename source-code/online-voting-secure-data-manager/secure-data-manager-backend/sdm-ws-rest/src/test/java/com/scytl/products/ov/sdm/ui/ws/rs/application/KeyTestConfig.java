/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.ui.ws.rs.application;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalKeyPair;
import com.scytl.products.ov.sdm.CryptoTestData;

/**
 * RSA and ElGamal keys required for the mix-dec-val integration test.
 */
@Configuration
public class KeyTestConfig {

    @Bean
    ElGamalKeyPair electoralAuthorityKeyPair() throws GeneralCryptoLibException {
        return CryptoTestData.generateElectoralAuthorityKeyPair(1);
    }

    @Bean
    KeyPair signingKeyPair(AsymmetricServiceAPI asymmetricService) throws NoSuchAlgorithmException {
        return asymmetricService.getKeyPairForSigning();
    }

}
