/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.ui.ws.rs.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * $Id$
 *
 * @author dcheda
 * @date Jul 17, 2015 1:01:27 PM
 * <p>
 * Copyright (C) 2015 Scytl Secure Electronic Voting SA
 * <p>
 * All rights reserved.
 */

/**
 * Contains a main method to start spring application.
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.scytl.products.ov.sdm" })

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    /**
     * Runs the spring application.
     *
     * @param args
     *            the arguments
     * @throws Exception
     *             the exception
     */

    public static void main(String[] args) throws Exception {
        // these props allows us to keep using 'sdm.properties' file as the main/default config file instead of
        // 'application.properties'. These have to be defined as either OS env prop, system props or cmdline args.
        // This way we can have an internal default configuration file, and if needed, an external one with the same
        // name that we already use and not have to change anything else. Both props can be overridden by cmdline args
        System.setProperty("spring.config.location", "${user.home}/sdm/sdmConfig/");
        System.setProperty("spring.config.name", "sdm");

        LOGGER.info("-------------------- Starting Secure Data Manager... --------------------");

        ConfigurableApplicationContext applicationContext =
            SpringApplication.run(new Object[] {Application.class }, args);

        applicationContext.registerShutdownHook();

        LOGGER.info("-------------------- Secure Data Manager successfully started. -------------------- ");
    }
   
}
