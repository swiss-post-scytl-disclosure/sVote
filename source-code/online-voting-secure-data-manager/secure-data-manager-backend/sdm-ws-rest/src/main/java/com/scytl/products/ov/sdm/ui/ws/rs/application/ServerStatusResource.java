/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.ui.ws.rs.application;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.sdm.application.log.SdmSecureLogEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.scytl.products.ov.sdm.infrastructure.JsonConstants;
import com.scytl.products.ov.sdm.infrastructure.OrientManager;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * The server status end-point.
 */
@RestController
@RequestMapping("/sdm-ws-rest")
@Api(value = "Server status REST API", description = "")
public class ServerStatusResource {
    private static final String OPEN_STATUS = "OPEN";

    private static final String CLOSED_STATUS = "CLOSED";

    @Autowired
    private OrientManager manager;

    @Autowired
    private TransactionInfoProvider transactionInfoProvider;

    @Value("${tenantID}")
    private String tenantId;

    @Autowired
    private SecureLoggingWriter secureLogger;

    /**
     * Returns the server status. Possible states are: OPEN, CLOSED.
     */
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Health Check Service", response = String.class, notes = "Service to validate application is up & running.")
    public String getStatus() {
        String status = manager.isActive() ? OPEN_STATUS : CLOSED_STATUS;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add(JsonConstants.JSON_ATTRIBUTE_NAME_STATUS, status);
        return builder.build().toString();
    }

    /**
     * Closes the database.
     */
    @RequestMapping(value = "/close", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Close Database", response = String.class, notes = "Service to close the database.")
    public String closeDatabase() {
        transactionInfoProvider.generate(tenantId, "", "");

        secureLogger.log(Level.INFO, new LogContent.LogContentBuilder()
                .logEvent(SdmSecureLogEvent.CLOSING_SDM)
                .createLogInfo());

        manager.shutdown();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add(JsonConstants.JSON_ATTRIBUTE_NAME_STATUS,
            CLOSED_STATUS);

        secureLogger.log(Level.INFO, new LogContent.LogContentBuilder()
                .logEvent(SdmSecureLogEvent.SDM_CLOSED_SUCCESSFULLY)
                .createLogInfo());

        return builder.build().toString();
    }
}
