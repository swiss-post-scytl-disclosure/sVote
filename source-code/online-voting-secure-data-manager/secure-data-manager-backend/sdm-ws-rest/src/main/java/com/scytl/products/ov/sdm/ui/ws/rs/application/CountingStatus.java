/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.ui.ws.rs.application;

/**
 * Counting status.
 */
public final class CountingStatus {
    private boolean enabled;

    /**
     * Returns the enabled.
     *
     * @return the enabled.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets the enabled.
     *
     * @param enabled the enabled.
     */
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
}
