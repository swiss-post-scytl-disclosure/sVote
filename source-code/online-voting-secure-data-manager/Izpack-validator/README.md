Izpack Validator
==============

It is used for validating that Secure Data Manager cannot be installed in a folder that is not empty.

Steps to include it into installer:

(1) Download izpack-dist-5.0.6.jar

(2) After the changes are done or a new functionality is added execute:

	javac -cp _{path-to-izpack-jar}/izpack-dist-5.0.6.jar src/com/scytl/izpack/InstallPathValidator.

(3) Execute the following command in order to create a jar:

	jar cvf {name-of-jar}.jar com/scytl/izpack/InstallPathValidator.class

(4) Once it is created upload it to nexus.

(5) Add the path to the jar in Nexus in config.sh (it should be downloaded in jar folder)

(6) Add the jar in install.xml 

	<jar src="jars/{name-of-jar}.jar" stage="install" />

