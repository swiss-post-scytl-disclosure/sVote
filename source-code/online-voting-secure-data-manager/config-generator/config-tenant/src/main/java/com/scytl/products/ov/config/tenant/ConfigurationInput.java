/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant;

import com.scytl.products.ov.datapacks.beans.CredentialProperties;

public class ConfigurationInput {

	private CredentialProperties tenantRootCA;


	/**
	 * Gets tenantRootCA.
	 *
	 * @return Value of tenantRootCA.
	 */
	public CredentialProperties getTenantRootCA() {
		return tenantRootCA;
	}

	/**
	 * Sets new tenantRootCA.
	 *
	 * @param tenantRootCA New value of tenantRootCA.
	 */
	public void setTenantRootCA(CredentialProperties tenantRootCA) {
		this.tenantRootCA = tenantRootCA;
	}
}
