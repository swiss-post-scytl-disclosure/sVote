/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.commands;

/**
 * Parameter holder for generating the tenant CSR certificate
 */
public class GenerateTenantParametersHolder {

    private String name;

    private String output;

    public GenerateTenantParametersHolder(String name, String output) {
        this.name = name;
        this.output = output;
    }

    /**
     * Gets name.
     *
     * @return Value of name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets new name.
     *
     * @param name New value of name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets new output.
     *
     * @param output New value of output.
     */
    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * Gets output.
     *
     * @return Value of output.
     */
    public String getOutput() {
        return output;
    }
}