/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.commands;

/**
 * Represents an execution command line.
 * <p>
 * A Command contains the following parts:
 * <ul>
 * <li>A command identifier.</li>
 * <li>A parameters map</li>
 * </ul>
 */
public class Command {

    private final MutuallyExclusiveCommand identifier;

    private final Parameters parameters;

    /**
     * Instantiates a new Command.
     *
     * @param command
     *            the command
     * @param parameters
     *            the parameters
     */
    public Command(final MutuallyExclusiveCommand command, final Parameters parameters) {
        this.identifier = command;
        this.parameters = parameters;
    }

    /**
     * Gets identifier.
     *
     * @return the identifier
     */
    public MutuallyExclusiveCommand getIdentifier() {
        return identifier;
    }

    /**
     * Gets parameters.
     *
     * @return the parameters
     */
    public Parameters getParameters() {
        return parameters;
    }

}
