/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.commands;

/**
 * Class containing the necessary parameter for updating the configuration of a tenant *
 */
public class UpdateTenantParametersHolder {

	private String tenantCertificatePath;

	private String rootCACertificatePath;

	private String keyStorePath;

	public UpdateTenantParametersHolder(String tenantCertificatePath, String rootCACertificatePath, String keyStorePath) {

		this.tenantCertificatePath = tenantCertificatePath;
		this.rootCACertificatePath = rootCACertificatePath;
		this.keyStorePath = keyStorePath;

	}



	/**
	 * Gets tenantCertificatePath.
	 *
	 * @return Value of tenantCertificatePath.
	 */
	public String getTenantCertificatePath() {
		return tenantCertificatePath;
	}

	/**
	 * Sets new tenantCertificatePath.
	 *
	 * @param tenantCertificatePath New value of tenantCertificatePath.
	 */
	public void setTenantCertificatePath(String tenantCertificatePath) {
		this.tenantCertificatePath = tenantCertificatePath;
	}



	/**
	 * Gets rootCACertificatePath.
	 *
	 * @return Value of rootCACertificatePath.
	 */
	public String getRootCACertificatePath() {
		return rootCACertificatePath;
	}

	/**
	 * Sets new rootCACertificatePath.
	 *
	 * @param rootCACertificatePath New value of rootCACertificatePath.
	 */
	public void setRootCACertificatePath(String rootCACertificatePath) {
		this.rootCACertificatePath = rootCACertificatePath;
	}

	/**
	 * Sets new keyStorePath.
	 *
	 * @param keyStorePath New value of keyStorePath.
	 */
	public void setKeyStorePath(String keyStorePath) {
		this.keyStorePath = keyStorePath;
	}

	/**
	 * Gets keyStorePath.
	 *
	 * @return Value of keyStorePath.
	 */
	public String getKeyStorePath() {
		return keyStorePath;
	}
}
