/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.scytl.products.ov.config.tenant.commands.ConfigurationCommandLine;

public class Application {

    public static void main(final String... args) {

        final Logger LOG = LoggerFactory.getLogger("std");

        try (AbstractApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class)) {

            final ConfigurationCommandLine engine =
                (ConfigurationCommandLine) context.getBean("configurationCommandLine");
            context.registerShutdownHook();

            engine.run(args);
        } catch (Exception e) {
        	 LOG.error("An error occurred during the execution of the command.", e);
            // Let the OS know the application exited abruptly.
            System.exit(-1);
        }
    }
}
