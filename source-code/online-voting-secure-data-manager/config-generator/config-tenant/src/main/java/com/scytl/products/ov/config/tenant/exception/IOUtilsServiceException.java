/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.exception;

public class IOUtilsServiceException extends ConfigTenantException {

	private static final long serialVersionUID = -195648582489872312L;

	public IOUtilsServiceException(final String message) {
		super(message);
	}

	public IOUtilsServiceException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public IOUtilsServiceException(final Throwable cause) {
		super(cause);
	}

}