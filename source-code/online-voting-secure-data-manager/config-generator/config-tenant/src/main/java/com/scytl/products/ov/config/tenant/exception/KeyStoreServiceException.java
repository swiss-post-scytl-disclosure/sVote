/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.exception;

public class KeyStoreServiceException extends ConfigTenantException {

	private static final long serialVersionUID = -3589200081340878956L;

	public KeyStoreServiceException(final String message) {
		super(message);
	}

	public KeyStoreServiceException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public KeyStoreServiceException(final Throwable cause) {
		super(cause);
	}

}