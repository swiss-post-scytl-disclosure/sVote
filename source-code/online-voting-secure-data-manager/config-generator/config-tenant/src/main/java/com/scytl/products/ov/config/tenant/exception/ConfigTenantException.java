/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.exception;

public class ConfigTenantException extends RuntimeException {

	private static final long serialVersionUID = -7972511828583990234L;

	public ConfigTenantException(final String message) {
		super(message);
	}

	public ConfigTenantException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ConfigTenantException(final Throwable cause) {
		super(cause);
	}

}