/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.commands;

import org.apache.commons.cli.Option;

/**
 * Represents a parameter of a command. A CommandParameter contains the following parts:
 * <ul>
 * <li>A parameter name.</li>
 * <li>A required indicator.</li>
 * <li>A number of values.</li>
 * </ul>
 * Each CommandParameter is capable to generate a {@link org.apache.commons.cli.Option} to be used by the command parser.
 */
public enum CommandParameter {
	TENANT_NAME("name", true, 1) {
		@Override
		public Option generateOption() {
			return Option.builder(getParameterName()).longOpt("tenantName").desc("name which identifies a given tenant")
				.numberOfArgs(getNumberOfArgs()).build();
		}
	},
	OUTPUT("out", true, 1) {
		@Override
		public Option generateOption() {
			return Option.builder(getParameterName()).longOpt("output")
				.desc("the output folder in which the files will be generated").numberOfArgs(getNumberOfArgs()).build();
		}
	},
	TENANT_CERTIFICATE("t", true, 1) {
		@Override
		public Option generateOption() {
			return Option.builder(getParameterName()).longOpt("tenantCertificate")
				.desc("the path to the tenant certificate in PEM format").numberOfArgs(getNumberOfArgs()).build();
		}
	},
	PLATFORM_ROOT_CA("root", true, 1) {
		@Override
		public Option generateOption() {
			return Option.builder(getParameterName()).longOpt("plattformRootCA")
				.desc("the path to the platform root CA certificate in PEM format").numberOfArgs(getNumberOfArgs()).build();
		}

	},
	KEYSTORE_PATH("ks", true, 1) {
		@Override
		public Option generateOption() {
			return Option.builder(getParameterName()).longOpt("keystore").desc("the path to the keystore to be updated")
				.numberOfArgs(getNumberOfArgs()).build();
		}

	};

	private final String parameterName;

	private final boolean required;

	private final int numberOfArgs;

	CommandParameter(final String parameterName, final boolean required, final int numberOfArgs) {
		this.parameterName = parameterName;
		this.required = required;
		this.numberOfArgs = numberOfArgs;
	}

	/**
	 * Gets parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return parameterName;
	}

	/**
	 * Is required.
	 *
	 * @return true if the parameter is required, false otherwise
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * Gets number of args.
	 *
	 * @return the number of args
	 */
	public int getNumberOfArgs() {
		return numberOfArgs;
	}

	/**
	 * Generate {@link org.apache.commons.cli.Option} to be used by the command parser.
	 *
	 * @return the option to be used by the command parser
	 */
	public abstract Option generateOption();

}
