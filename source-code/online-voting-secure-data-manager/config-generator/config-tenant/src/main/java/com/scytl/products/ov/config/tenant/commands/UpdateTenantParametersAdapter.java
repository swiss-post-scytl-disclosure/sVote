/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.commands;

public class UpdateTenantParametersAdapter {

	/**
	 * @param receivedParameters
	 * @return
	 */
	public UpdateTenantParametersHolder adapt(final Parameters receivedParameters) {



		String platformCACertificatePath =
			String.valueOf(receivedParameters.getParam(CommandParameter.PLATFORM_ROOT_CA.getParameterName()));

		String tenantCertificatePath =
				String.valueOf(receivedParameters.getParam(CommandParameter.TENANT_CERTIFICATE.getParameterName()));

		String keyStorePath =
			String.valueOf(receivedParameters.getParam(CommandParameter.KEYSTORE_PATH.getParameterName()));



		validateReceivedParameters(tenantCertificatePath,platformCACertificatePath, keyStorePath);

		return new UpdateTenantParametersHolder(tenantCertificatePath,platformCACertificatePath, keyStorePath);
	}

	private void
			validateReceivedParameters(String tenantCAPath, String rootCAPath,String keyStorePath) {

		if ((tenantCAPath == null) || tenantCAPath.isEmpty()) {
			throw new IllegalArgumentException("The tenant certificate path must be initialized.");
		}


		if ((rootCAPath == null) || rootCAPath.isEmpty()) {
			throw new IllegalArgumentException("The Plattform Certificate must be provided.");
		}
		if ((keyStorePath == null) || keyStorePath.isEmpty()) {
			throw new IllegalArgumentException("The keystorepath must be provided.");
		}


	}
}
