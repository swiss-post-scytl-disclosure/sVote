/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.commands;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Parameters.
 */
public class Parameters {

    private final Map<String, Object> param2value;

    /**
     * Instantiates a new Parameters.
     */
    public Parameters() {

        param2value = new HashMap<>();
    }

    /**
     * Add param.
     *
     * @param param
     *            the param
     * @param value
     *            the value
     */
    public void addParam(final String param, final Object value) {

        param2value.put(param, value);

    }

    /**
     * Gets param.
     *
     * @param param
     *            the param
     * @return the param
     */
    public Object getParam(final String param) {

        if (!param2value.containsKey(param)) {

            throw new IllegalArgumentException("The parameter \"" + param
                + "\" has been requested and could not be found.");
        }

        return param2value.get(param);

    }
}
