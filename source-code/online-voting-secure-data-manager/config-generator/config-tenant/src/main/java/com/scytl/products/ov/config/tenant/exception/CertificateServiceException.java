/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.tenant.exception;

public class CertificateServiceException extends ConfigTenantException {

	private static final long serialVersionUID = 4184732808649095700L;

	public CertificateServiceException(final String message) {
		super(message);
	}

	public CertificateServiceException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public CertificateServiceException(final Throwable cause) {
		super(cause);
	}

}