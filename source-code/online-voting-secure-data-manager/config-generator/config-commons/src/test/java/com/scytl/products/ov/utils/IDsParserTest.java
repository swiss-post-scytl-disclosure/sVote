/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 *
 */
public class IDsParserTest {

    private final IDsParser target = new IDsParser();

    @Test(expected = IllegalArgumentException.class)
    public void parseIlegalArgument() {

        target.parse("3232,3232");
    }

    @Test
    public void parsePositiveCase() {

        String testCase =
                "[08b82ffc12e84dd6973ffd7b9feadeee,451a6ffc3e214ca8ae5c451d82e7fbe4,17ccbe962cf341bc93208c26e911090c,4b35ae490b2a495a98e709fb004e22a1]";
        List<String> idsArray = target.parse(testCase);

        Assert.assertEquals(4, idsArray.size());
        Assert.assertEquals("08b82ffc12e84dd6973ffd7b9feadeee", idsArray.get(0));
        Assert.assertEquals("451a6ffc3e214ca8ae5c451d82e7fbe4", idsArray.get(1));
        Assert.assertEquals("17ccbe962cf341bc93208c26e911090c", idsArray.get(2));
        Assert.assertEquals("4b35ae490b2a495a98e709fb004e22a1", idsArray.get(3));
    }
}
