/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.encryption.params.generation;

import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class ElGamalEncryptionParametersTest {

    private static ElGamalEncryptionParameters _target;

    private static BigInteger _p;

    private static BigInteger _q;

    private static BigInteger _g;

    @BeforeClass
    public static void setUp() {

        _p = new BigInteger("23");

        _q = new BigInteger("11");

        _g = new BigInteger("2");

        _target = new ElGamalEncryptionParameters(_p, _q, _g);
    }

    @Test
    public void return_expected_parameters_when_they_are_retrieved() {

        String errorMsg =
                "The returned p parameter was not the expected value";
        assertEquals(errorMsg, _p, _target.getP());

        errorMsg = "The returned q parameter was not the expected value";
        assertEquals(errorMsg, _q, _target.getQ());

        errorMsg = "The returned g parameter was not the expected value";
        assertEquals(errorMsg, _g, _target.getG());
    }
}
