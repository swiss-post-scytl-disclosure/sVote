/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.choices.codes;

import static org.junit.Assert.assertEquals;

import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.products.ov.constants.Constants;
import com.scytl.products.ov.utils.ChecksumUtils;

public class RandomBallotCastingKeyGeneratorTest {

    private static RandomWithChecksumBallotCastingKeyGenerator _randomWithChecksumBallotCastingKeyGeneratorStrategy;

    private static PrimitivesServiceAPI _primitivesService;

    private static ChecksumUtils _checksumUtils;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        Security.addProvider(new BouncyCastleProvider());

        _primitivesService = new PrimitivesService();

        _checksumUtils = new ChecksumUtils();

        _randomWithChecksumBallotCastingKeyGeneratorStrategy =
            new RandomWithChecksumBallotCastingKeyGenerator(_primitivesService, _checksumUtils);
    }

    @Test
    public void generate_valid_ballot_casting_key() {

        int NUM_TESTS = 1000;

        String ballotCastingKey;
        for (int i = 0; i < NUM_TESTS; i++) {

            ballotCastingKey = _randomWithChecksumBallotCastingKeyGeneratorStrategy.generateBallotCastingKey();

            Assert.assertTrue(_checksumUtils.isValid(ballotCastingKey));
            String errorMsg = "The generated ballot casting key did not have "
                + Constants.NUM_DIGITS_BALLOT_CASTING_KEY_FINAL + " digits, it was: " + ballotCastingKey;
            assertEquals(errorMsg, Constants.NUM_DIGITS_BALLOT_CASTING_KEY_FINAL, ballotCastingKey.length());
        }
    }
}
