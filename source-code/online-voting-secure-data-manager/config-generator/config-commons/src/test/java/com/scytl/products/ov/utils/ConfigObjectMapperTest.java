/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.scytl.products.ov.commons.beans.AuthenticationVoterData;

/**
 * mvn *
 */
public class ConfigObjectMapperTest {

    private final ConfigObjectMapper target = new ConfigObjectMapper();

    @Test
    public void fromJavaToJSON_and_fromJSONToJava() throws IOException {

        AuthenticationVoterData originalAuthenticationVoterDataContents = createDummyAuthenticationVoterDataContents();

        String json = target.fromJavaToJSON(originalAuthenticationVoterDataContents);

        AuthenticationVoterData returnedAuthenticationVoterDataContents =
            target.fromJSONToJava(json, AuthenticationVoterData.class);

        compareTwoAuthenticationContents(originalAuthenticationVoterDataContents,
            returnedAuthenticationVoterDataContents);
    }

    @Test
    public void fromJavaToFile_and_fromFileToJava() throws IOException {

        AuthenticationVoterData originalAuthenticationVoterDataContents = createDummyAuthenticationVoterDataContents();

        File dest = new File("./target", "fromJavaToFile_and_fromFileToJava.txt");

        Assert.assertTrue(!dest.exists());

        target.fromJavaToJSONFile(originalAuthenticationVoterDataContents, dest);

        Assert.assertTrue(dest.exists());

        AuthenticationVoterData returnedAuthenticationVoterDataContents =
            target.fromJSONFileToJava(dest, AuthenticationVoterData.class);

        compareTwoAuthenticationContents(originalAuthenticationVoterDataContents,
            returnedAuthenticationVoterDataContents);

        // Cleanup
        FileUtils.deleteQuietly(dest);
    }

    /**
     * @return AuthenticationContents dummy object
     */
    private AuthenticationVoterData createDummyAuthenticationVoterDataContents() {

        String electionRootCA = "test";
        String authoritiesCA = "test";
        String servicesCA = "test";
        String credentialsCA = "test";
        String authTokenSignerCert = "test";

        AuthenticationVoterData originalAuthenticationContents = new AuthenticationVoterData();
        originalAuthenticationContents.setElectionRootCA(electionRootCA);
        originalAuthenticationContents.setAuthoritiesCA(authoritiesCA);
        originalAuthenticationContents.setServicesCA(servicesCA);
        originalAuthenticationContents.setCredentialsCA(credentialsCA);
        originalAuthenticationContents.setAuthenticationTokenSignerCert(authTokenSignerCert);

        return originalAuthenticationContents;
    }

    private void compareTwoAuthenticationContents(final AuthenticationVoterData originalAuthenticationContents,
            final AuthenticationVoterData returnedAuthenticationContents) {

        Assert.assertEquals(originalAuthenticationContents.getElectionRootCA(),
            returnedAuthenticationContents.getElectionRootCA());
        Assert.assertEquals(originalAuthenticationContents.getAuthoritiesCA(),
            returnedAuthenticationContents.getAuthoritiesCA());
        Assert.assertEquals(originalAuthenticationContents.getServicesCA(),
            returnedAuthenticationContents.getServicesCA());
        Assert.assertEquals(originalAuthenticationContents.getCredentialsCA(),
                returnedAuthenticationContents.getCredentialsCA());
        Assert.assertEquals(originalAuthenticationContents.getAuthenticationTokenSignerCert(),
            returnedAuthenticationContents.getAuthenticationTokenSignerCert());
    }
}
