/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scytl.products.ov.config.exceptions.ConfigurationEngineException;
import com.scytl.products.ov.config.model.authentication.AuthSecretSpec;

public class AuthSecretSpecParserTest {

    private static AuthSecretsSpecParser parser = new AuthSecretsSpecParser();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void parseOneSecret() {

        String authSecretsSpecAsString = "[[id,12]]";

        List<AuthSecretSpec> authSecretSpec = parser.parse(authSecretsSpecAsString);

        assertTrue(authSecretSpec.size() == 1);
        assertTrue(authSecretSpec.get(0).getName().equals("id"));
        assertTrue(authSecretSpec.get(0).getLength() == 12);
    }

    @Test
    public void parseTwoSecrets() {

        String authSecretsSpecAsString = "[[id,12];[password,12]]";

        List<AuthSecretSpec> authSecretSpec = parser.parse(authSecretsSpecAsString);

        assertTrue(authSecretSpec.size() == 2);
        assertTrue(authSecretSpec.get(0).getName().equals("id"));
        assertTrue(authSecretSpec.get(0).getLength() == 12);
        assertTrue(authSecretSpec.get(1).getName().equals("password"));
        assertTrue(authSecretSpec.get(1).getLength() == 12);
    }

    @Test
    public void parseThreeSecrets() {

        String authSecretsSpecAsString = "[[id,12];[password,12];[ssssss,15]]";

        List<AuthSecretSpec> authSecretSpec = parser.parse(authSecretsSpecAsString);

        assertTrue(authSecretSpec.size() == 3);
        assertTrue(authSecretSpec.get(0).getName().equals("id"));
        assertTrue(authSecretSpec.get(0).getLength() == 12);
        assertTrue(authSecretSpec.get(1).getName().equals("password"));
        assertTrue(authSecretSpec.get(1).getLength() == 12);
        assertTrue(authSecretSpec.get(2).getName().equals("ssssss"));
        assertTrue(authSecretSpec.get(2).getLength() == 15);
    }

    @Test
    public void throwExpectedExceptionWhenNull() {

        String authSecretsSpecAsString = null;

        exception.expect(ConfigurationEngineException.class);
        exception.expectMessage("The received authentication secrets spec was not initialized.");

        parser.parse(authSecretsSpecAsString);
    }

    @Test
    public void throwExpectedExceptionWhenEmpty() {

        String authSecretsSpecAsString = "";

        exception.expect(ConfigurationEngineException.class);
        exception.expectMessage("The received authentication secrets spec was not initialized.");

        parser.parse(authSecretsSpecAsString);
    }

    @Test
    public void throwExpectedExceptionWhenTooShort() {

        String authSecretsSpecAsString = "[i,1]";

        exception.expect(ConfigurationEngineException.class);
        exception.expectMessage(
            "The received authentication does not follow the expected format. It is too short to be value.");

        parser.parse(authSecretsSpecAsString);
    }

    @Test
    public void throwExpectedExceptionWhenBadFormat() {

        String authSecretsSpecAsString = "[[XXXXXXXXXX]]";

        exception.expect(ConfigurationEngineException.class);
        exception.expectMessage("Failed to parse a name and length from the received string.");

        parser.parse(authSecretsSpecAsString);
    }

    @Test
    public void throwExpectedExceptionWhenNonNumericLength() {

        String authSecretsSpecAsString = "[[id,id]]";

        exception.expect(ConfigurationEngineException.class);
        exception.expectMessage("Could not parse interger from length.");

        List<AuthSecretSpec> authSecretSpec = parser.parse(authSecretsSpecAsString);
    }
}
