/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.readers;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.scytl.products.ov.datapacks.beans.ConfigurationInput;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigurationInputReaderTest {

    @Rule
    public ExpectedException exceptions = ExpectedException.none();

    private ConfigurationInputReader _sut;

    @Test
    public void read_a_given_file_correctly() throws IOException {
        _sut = new ConfigurationInputReader();

        final File file = Paths.get("src/test/resources/keys_config.json").toFile();
        final ConfigurationInput configurationInput = _sut.fromFileToJava(file);

        assertThat(configurationInput.getBallotBox().getAlias().get("privateKey"), is("privatekey"));

    }

    @Test
    public void throw_an_exception_if_the_given_file_is_not_consistent() throws IOException {
        _sut = new ConfigurationInputReader();

        final File file = Paths.get("src/test/resources/FAKE_HAHAHA.json").toFile();

        exceptions.expect(UnrecognizedPropertyException.class);
        final ConfigurationInput configurationInput = _sut.fromFileToJava(file);
    }

}
