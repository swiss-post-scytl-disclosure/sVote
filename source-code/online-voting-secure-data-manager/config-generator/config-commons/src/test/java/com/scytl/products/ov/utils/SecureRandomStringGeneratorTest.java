/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

public class SecureRandomStringGeneratorTest {

    private static SecureRandomStringGenerator secureRandomStringGenerator;

    private static final String alphabet = "qwertyuiop123";

    @BeforeClass
    public static void setUp() {
        secureRandomStringGenerator = new SecureRandomStringGenerator(alphabet);
    }

    @Test
    public void test_generated_length() {
        int stringLength = 12;
        String result = secureRandomStringGenerator.generate(stringLength);
        assertEquals(stringLength, result.length());
    }

    @Test
    public void test_generated_string_belongs_to_alphabet() {
        for (int i = 0; i < 10; i++) {
            int stringLength = 12;
            String result = secureRandomStringGenerator.generate(stringLength);

            for (int j = 0; j < result.length(); j++) {
                assertTrue(alphabet.contains(result.substring(j, j + 1)));
            }
        }
    }

}
