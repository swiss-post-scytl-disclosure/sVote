/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TimestampGeneratorTest {

    private TimestampGenerator _sut;

    @Test
    public void return_a_date_and_time_in_string_format_that_can_be_reconstructed_correctly() {

        _sut = new TimestampGenerator();

        final String formattedNow = _sut.getNewTimeStamp();

        final DateTimeFormatter formatter = _sut.getDateFormat();

        final LocalDateTime reformatted = LocalDateTime.parse(formattedNow, formatter);

        assertThat(formattedNow, is(reformatted.format(formatter)));

    }
}
