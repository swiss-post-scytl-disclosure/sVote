/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

public class PasswordCleanerTest {

    private static PasswordCleaner _target = new PasswordCleaner();

    @Test
    public void whenCleanThenPasswordCleaned() throws IOException, GeneralCryptoLibException {

        String password = "Q5H4B5WUIQWCR6J3UQS2KEVVZU";

        char[] passwordAsCharArray = password.toCharArray();

        _target.clean(passwordAsCharArray);

        for (int i = 0; i < passwordAsCharArray.length; i++) {
            assertEquals(passwordAsCharArray[i], '\u0000');
        }
    }
}
