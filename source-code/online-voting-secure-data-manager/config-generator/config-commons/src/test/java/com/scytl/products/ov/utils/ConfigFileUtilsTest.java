/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;

/**
 *
 */
public class ConfigFileUtilsTest {

    private final ConfigFileUtils target = new ConfigFileUtils();

    private final TimestampGenerator generator = new TimestampGenerator();

    @Test
    public void saveTimestampedFile_using_an_inputStreamTest() throws IOException {

        final InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test.zip");

        final File file =
            target.saveTimestampedFileFromInputStream(inputStream, "./target/saveTimestampedFileFromInputStreamTest",
                "test.zip");

        for (final File timestampsFolders : new File("./target/saveTimestampedFileFromInputStreamTest").listFiles()) {

            Assert.assertEquals(1, timestampsFolders.listFiles().length);
            for (final File fileSaved : timestampsFolders.listFiles()) {
                Assert.assertEquals(file, fileSaved);
            }

        }
    }

    @Test
    public void saveTimestampedFile_using_a_file() throws IOException {

        final File sourceFile = new File("./src/test/resources/test.zip");

        target.saveTimestampedFile(sourceFile, "./target/saveTimestampedFileTest");

        for (final File timestampsFolders : new File("./target/saveTimestampedFileTest").listFiles()) {

            Assert.assertEquals(1, timestampsFolders.listFiles().length);
            for (final File filesSaved : timestampsFolders.listFiles()) {
                Assert.assertEquals("test.zip", filesSaved.getName());
            }

        }
    }

    @Test
    public void saveFile() throws IOException {

        final InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test.zip");
        final String path = "./target/saveFileTest";

        final File destinationFile = target.saveFile(path, inputStream);

        Assert.assertTrue(Files.exists(destinationFile.toPath()));
    }

    @Test
    public void createTimestampedPath() throws ParseException {

        final String timeStamp = generator.getNewTimeStamp();

        final String folderName = "createPathTest";
        final String destinationFolder = "./target" + File.separator + folderName;
        final String path = destinationFolder + File.separator + timeStamp;

        final String createdPath = target.createTimestampedPath(path, new BigInteger(timeStamp), destinationFolder);

        Assert.assertTrue(Files.exists(Paths.get(createdPath)));

        final String newPath = target.createTimestampedPath(path, new BigInteger(timeStamp), destinationFolder);

        Assert.assertNotEquals(newPath, (destinationFolder) + new BigInteger(timeStamp).add(BigInteger.ONE));

        final File lastFolderTimestamp = target.getLastFolderTimestamp(new File(destinationFolder));

        Assert.assertNotEquals(newPath, lastFolderTimestamp.getAbsolutePath());

    }

    @AfterClass
    public static void cleanUp() {
        FileUtils.deleteQuietly(new File("./target/saveTimestampedFileFromInputStreamTest"));
        FileUtils.deleteQuietly(new File("./target/saveTimestampedFileTest"));
        FileUtils.deleteQuietly(new File("./target/createPathTest"));
        FileUtils.deleteQuietly(new File("./target/saveFileTest"));

    }

}
