/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.Security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * $Id$
 *
 * @author afries
 * @date Jun 11, 2015 9:26:08 AM Copyright (C) 2015 Scytl Secure Electronic Voting SA All rights reserved.
 */
public class ChecksumUtilsTest {

    private static ChecksumUtils _target;

    @BeforeClass
    public static void setUp() {

        _target = new ChecksumUtils();

        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void generate_and_validate_checksum() {

        int NUM_TESTS = 100;

        String numberWithChecksumAppended;
        for (int i = 12345678; i < NUM_TESTS; i++) {

            numberWithChecksumAppended = _target.generate("" + i);

            String errorMsg =
                "The generated number with checksum appended does not have 9 digits, it was: "
                    + numberWithChecksumAppended;
            assertEquals(errorMsg, 9, numberWithChecksumAppended.length());

            errorMsg = "The generated number with checksum appended failed to validate";
            assertTrue(errorMsg, _target.isValid(numberWithChecksumAppended));
        }
    }
}
