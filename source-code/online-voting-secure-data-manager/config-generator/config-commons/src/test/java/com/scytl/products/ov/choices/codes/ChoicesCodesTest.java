/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.choices.codes;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.cryptolib.mathematical.groups.utils.GroupUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class ChoicesCodesTest {

    private static PartialCodeGenerator<ZpGroupElement> _partialCodeGenerator;

    private static BigInteger _p;

    private static BigInteger _g;

    private static ZpSubgroup _group;

    private static Exponent _exponent;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        _partialCodeGenerator = new PartialCodeGenerator<ZpGroupElement>();

        _p = new BigInteger("23");

        _g = new BigInteger("2");

        _group =
            new GroupUtils().buildQuadraticResidueGroupFromPAndG(_p, _g);

        _exponent = new Exponent(_group.getQ(), new BigInteger("4"));
    }

    @Test
	public void generate_expected_partial_code() throws GeneralCryptoLibException {

        ZpGroupElement expectedResult =
            new ZpGroupElement(new BigInteger("16"), _group);

        ZpGroupElement result =
				_partialCodeGenerator.generatePartialCode(_group.getGenerator(), _exponent);

		String errorMsg = "The generated partial code was not the expected value";
		assertEquals(errorMsg, expectedResult, result);
    }

}
