/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ConfigZipUtilsTest {

    @Test
    public void zip_using_a_dir() throws IOException {

        File fromDir = new File("./src/test/resources/test_without_subfolders");
        File zipFile = new File("./target/zipTest.zip");
        ConfigZipUtils.zip(fromDir, zipFile);

        Assert.assertTrue(zipFile.exists());
    }

    @Test
    public void zip_using_a_list_of_files() throws IOException {

        List<File> fromFiles = new ArrayList<>();
        File file1 = new File("./src/test/resources/test/test1.txt");
        File file2 = new File("./src/test/resources/test/test2.txt");
        fromFiles.add(file1);
        fromFiles.add(file2);
        File zipFile = new File("./target/zipTest2.zip");
        ConfigZipUtils.zip(fromFiles, zipFile);

        Assert.assertTrue(zipFile.exists());
    }

    @Test
    public void unzipFile_using_a_file() throws IOException {

        File file = new File("./src/test/resources/test.zip");
        String outDir = "./target/";
        ConfigZipUtils.unzipFile(file, outDir);

        Assert.assertTrue(new File("./target/test.txt").exists());
    }

    @Test
    public void unzipFile_using_an_inputStream() throws IOException {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test.zip");
        String outDir = "./target/";
        ConfigZipUtils.unzipFile(inputStream, outDir);

        Assert.assertTrue(new File("./target/test.txt").exists());
    }

    @Test
    public void zipDir() throws IOException {

        File directory = new File("./src/test/resources/test");
        File zipFile = new File("./target/zipDir.zip");

        ConfigZipUtils.zipDir(directory, zipFile);

        Assert.assertTrue(zipFile.exists());
    }

    @AfterClass
    public static void cleanUp() {
        FileUtils.deleteQuietly(new File("./target/zipTest.zip"));
        FileUtils.deleteQuietly(new File("./target/zipTest2.zip"));
        FileUtils.deleteQuietly(new File("./target/test.txt"));
    }
}
