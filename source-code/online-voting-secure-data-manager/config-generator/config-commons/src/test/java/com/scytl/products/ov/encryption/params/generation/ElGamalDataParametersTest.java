/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.encryption.params.generation;

import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.cryptolib.mathematical.groups.utils.GroupUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;
import java.security.Security;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ElGamalDataParametersTest {

    private static final BigInteger P = new BigInteger(
        "722467980024196662750270851447");

    private static final BigInteger G = new BigInteger("2");

    private static final List<BigInteger> QUADRATIC_RESIDUES = Arrays
        .asList(BigInteger.valueOf(2), BigInteger.valueOf(3),
            BigInteger.valueOf(11), BigInteger.valueOf(13),
            BigInteger.valueOf(17));

    private static final int NUM_VOTE_OPTS = 5;

    private static GroupUtils _groupUtils;

    @BeforeClass
    public static void setUp() {

        Security.addProvider(new BouncyCastleProvider());

        _groupUtils = new GroupUtils();
    }

    @AfterClass
    public static void removeBouncyCastleProvider() {

        Security.removeProvider("BC");
    }

    @Test
    public void whenConstructFromBadPrimeListThenException()
            throws Exception {

        ZpSubgroup group =
            _groupUtils.buildQuadraticResidueGroupFromPAndG(P, G);

        ElGamalEncryptionParameters params =
            new ElGamalEncryptionParameters(group.getP(), group.getQ(),
                group.getGenerator().getValue());

        PrimeGroupMembersProvider dataParamsGenerator =
            new PrimeGroupMembersProvider();

        List<BigInteger> primes =
            dataParamsGenerator.generateDataParameters(NUM_VOTE_OPTS,
                params);

        assertEquals(P, params.getP());
        assertEquals(G, params.getG());
        assertEquals(NUM_VOTE_OPTS, primes.size());
        assertEquals(QUADRATIC_RESIDUES, primes);
    }
}
