/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.datapacks.beans;

import java.util.Map;
import java.util.Properties;

/**
 * Contains the properties needed to create the certificates that are created when creating an election event.
 * <P>
 * The framework that actually creates the certificates (inside the config generator) expects most of properties to be
 * included in a map. However, the Authentication Token Signer certificate (which is also created during the creation of
 * the election event) is handled separately, and therefore its properties are not included in the map. Although, the
 * properties needed for the creation of the Authentication Token Signer certificate are exactly the same type as those
 * needed for the other certificates.
 * 
 * @author afries
 * @date Sep 4, 2018
 */
public class CreateElectionEventCertificatePropertiesContainer {

    /**
     * Contains properties needed for the following certificates:
     * <ul>
     * <li>Election Event CA</li>
     * <li>Services CA</li>
     * <li>Credentails CA</li>
     * <li>Authorities CA</li>
     * </ul>
     */
    private Map<String, Properties> nameToCertificateProperties;

    /**
     * Properties needed for the creation of the Authentication Token Signer Certificate.
     */
    private Properties authTokenSignerCertificateProperties;

    /**
     * Set the map of certificate properties.
     * 
     * @param mapNameToCertificateParameters
     *            the map of certificate properties to set.
     */
    public void setNameToCertificateProperties(Map<String, Properties> mapNameToCertificateParameters) {
        this.nameToCertificateProperties = mapNameToCertificateParameters;
    }

    /**
     * Get the map of certificate properties.
     * 
     * @return the map of certificate properties.
     */
    public Map<String, Properties> getNameToCertificateProperties() {
        return this.nameToCertificateProperties;
    }

    /**
     * Set the Authentication Token Signer certificate properties.
     * 
     * @param authTokenSignerX509CertificateProperties
     *            the certificate properties to set.
     */
    public void setAuthTokenSignerCertificateProperties(Properties authTokenSignerX509CertificateProperties) {
        this.authTokenSignerCertificateProperties = authTokenSignerX509CertificateProperties;
    }

    /**
     * Get the Authentication Token Signer certificate properties.
     * 
     * @return the Authentication Token Signer certificate properties
     */
    public Properties getAuthTokenSignerCertificateProperties() {
        return authTokenSignerCertificateProperties;
    }
}
