/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import java.io.IOException;
import java.nio.file.Path;

import com.scytl.products.ov.commons.beans.EncryptionParameters;

/**
 * An adapter of configOjectMapper class to load a set of Encryption parameters
 * from a JSON file.
 */
public class EncryptionParametersLoader {

    private final ConfigObjectMapper _mapper;

    public EncryptionParametersLoader(final ConfigObjectMapper mapperToUse) {

        validateReceivedMapper(mapperToUse);

        _mapper = mapperToUse;
    }

    public EncryptionParameters load(final Path fullPathAndFilename) throws IOException {

        return _mapper.fromJSONFileToJava(fullPathAndFilename.toFile(), EncryptionParameters.class);
    }

    private void validateReceivedMapper(final ConfigObjectMapper mapperToUse) {

        if (mapperToUse == null) {
            throw new IllegalArgumentException("The received mapper was null");
        }
    }
}
