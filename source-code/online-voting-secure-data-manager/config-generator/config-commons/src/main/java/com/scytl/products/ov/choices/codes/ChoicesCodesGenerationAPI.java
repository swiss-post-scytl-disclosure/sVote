/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.choices.codes;

import java.util.List;

import javax.crypto.SecretKey;

import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;

public interface ChoicesCodesGenerationAPI {

    /**
     * Generate a vote casting code. This code consists of 8 digits.
     *
     * @return the generated vote casting code.
     */
    String generateVoteCastingCode();

    /**
     * Generate a short choice code. This code consists of 4 digits.
     *
     * @return the generated short choice code.
     */
    String generateShortChoiceCode();

    /**
     * Generate a long choice code.
     *
     * @return the generated long choice code.
     */
    byte[] generateLongChoiceCode(String eeid, String verificationCardId, ZpGroupElement partialCode,
            List<String> attributesWithCorrectness);

    /**
     * Generate a choices code mapping.
     *
     * @return the generated choices code mapping.
     */
    ChoicesCodesMappingEntry generateMapping(final byte[] shortCodeBytes, final byte[] longCode,
            final SecretKey codesKey);
}
