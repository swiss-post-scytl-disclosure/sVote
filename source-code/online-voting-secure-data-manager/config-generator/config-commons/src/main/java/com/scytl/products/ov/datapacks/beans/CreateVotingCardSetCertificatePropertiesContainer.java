/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.datapacks.beans;

import java.util.Properties;

/**
 * Contains the properties needed to create the certificates that are created when creating voting card sets and
 * verification card sets.
 * 
 * @author afries
 * @date Sep 4, 2018
 */
public class CreateVotingCardSetCertificatePropertiesContainer {

    /**
     * Properties needed for the creation of the Voting Card Set certificate.
     */
    private Properties votingCardSetCertificateProperties;

    /**
     * Properties needed for the creation of the Verification Card Set certificate.
     */
    private Properties verificationCardSetCertificateProperties;

    /**
     * Properties needed for the creation of the Credential Sign certificate.
     */
    private Properties credentialSignCertificateProperties;

    /**
     * Properties needed for the creation of the Credential Authentication certificate.
     */
    private Properties credentialAuthCertificateProperties;

    /**
     * Get the Voting Card Set certificate properties.
     * 
     * @return the Voting Card Set certificate properties.
     */
    public Properties getVotingCardSetCertificateProperties() {
        return votingCardSetCertificateProperties;
    }

    /**
     * Set the Voting Card Set certificate properties.
     * 
     * @param votingCardSetCertificateProperties
     *            the certificate properties to set.
     */
    public void setVotingCardSetCertificateProperties(Properties votingCardSetCertificateProperties) {
        this.votingCardSetCertificateProperties = votingCardSetCertificateProperties;
    }

    /**
     * Get the Verification Card Set certificate properties.
     * 
     * @return the Verification Card Set certificate properties.
     */
    public Properties getVerificationCardSetCertificateProperties() {
        return verificationCardSetCertificateProperties;
    }

    /**
     * Set the Verification Card Set certificate properties.
     * 
     * @param verificationCardSetCertificateProperties
     *            the certificate properties to set.
     */
    public void setVerificationCardSetCertificateProperties(Properties verificationCardSetCertificateProperties) {
        this.verificationCardSetCertificateProperties = verificationCardSetCertificateProperties;
    }

    /**
     * Get the Credential Sign certificate properties.
     * 
     * @return the Credential Sign certificate properties.
     */
    public Properties getCredentialSignCertificateProperties() {
        return credentialSignCertificateProperties;
    }

    /**
     * Set the Credential Sign certificate properties.
     * 
     * @param credentialSignCertificateProperties
     *            the certificate properties to set.
     */
    public void setCredentialSignCertificateProperties(Properties credentialSignCertificateProperties) {
        this.credentialSignCertificateProperties = credentialSignCertificateProperties;
    }

    /**
     * Get the Credential Authentication certificate properties.
     * 
     * @return the Credential Authentication certificate properties.
     */
    public Properties getCredentialAuthCertificateProperties() {
        return credentialAuthCertificateProperties;
    }

    /**
     * Set the Credential Authentication certificate properties.
     * 
     * @param credentialAuthCertificateProperties
     *            the certificate properties to set.
     */
    public void setCredentialAuthCertificateProperties(Properties credentialAuthCertificateProperties) {
        this.credentialAuthCertificateProperties = credentialAuthCertificateProperties;
    }
}
