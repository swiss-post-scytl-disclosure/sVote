/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.ConfigurationEngineException;

public class FingerprintGeneratorException extends ConfigurationEngineException {

	private static final long serialVersionUID = 8202109908404244520L;

	public FingerprintGeneratorException(final String message) {
        super(message);
    }

    public FingerprintGeneratorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FingerprintGeneratorException(final Throwable cause) {
        super(cause);
    }

}
