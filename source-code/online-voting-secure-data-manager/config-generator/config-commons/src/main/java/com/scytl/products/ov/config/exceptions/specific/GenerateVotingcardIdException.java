/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateVotingCardSetException;

public class GenerateVotingcardIdException extends CreateVotingCardSetException {

	private static final long serialVersionUID = -5270520282485321378L;

	public GenerateVotingcardIdException(final String message) {
        super(message);
    }

    public GenerateVotingcardIdException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateVotingcardIdException(final Throwable cause) {
        super(cause);
    }
}
