/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 7/09/15.
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateVotingCardSetException;

public class GenerateCredentialDataException extends CreateVotingCardSetException {

	private static final long serialVersionUID = 2706905178312822603L;

	public GenerateCredentialDataException(final String message) {
        super(message);
    }

    public GenerateCredentialDataException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateCredentialDataException(final Throwable cause) {
        super(cause);
    }
}
