/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

public class SequentialProvidedChallengeSourceException extends ConfigurationEngineException {

	private static final long serialVersionUID = -1115154567997726182L;

	public SequentialProvidedChallengeSourceException(final String message) {
        super(message);
    }

    public SequentialProvidedChallengeSourceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SequentialProvidedChallengeSourceException(final Throwable cause) {
        super(cause);
    }

}
