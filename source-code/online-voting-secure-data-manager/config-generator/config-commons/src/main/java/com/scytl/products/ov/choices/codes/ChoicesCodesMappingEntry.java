/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.choices.codes;

public final class ChoicesCodesMappingEntry {

    private final byte[] key;

    private final byte[] data;

    public ChoicesCodesMappingEntry(final byte[] key, final byte[] data) {

        this.key = key;
        this.data = data;
    }

    public byte[] getKey() {
        return key;
    }

    public byte[] getData() {
        return data;
    }
}
