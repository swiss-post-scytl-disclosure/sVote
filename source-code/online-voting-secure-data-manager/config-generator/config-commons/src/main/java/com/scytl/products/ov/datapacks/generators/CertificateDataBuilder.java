/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.datapacks.generators;

import java.security.PublicKey;
import java.util.Date;

import org.apache.commons.configuration.ConfigurationException;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.certificates.bean.CertificateData;
import com.scytl.cryptolib.certificates.bean.ValidityDates;
import com.scytl.cryptolib.certificates.bean.X509DistinguishedName;
import com.scytl.products.ov.datapacks.beans.CertificateParameters;

/**
 * Builds the object {@link CertificateData}
 */
public class CertificateDataBuilder {

	/**
	 * Builds the object {@link CertificateData}.
	 * 
	 * @param publicKey
	 * @param certificateParameters
	 * @return
	 * @throws GeneralCryptoLibException
	 * @throws ConfigurationException
	 */
	public CertificateData build(final PublicKey publicKey, final CertificateParameters certificateParameters)
			throws GeneralCryptoLibException, ConfigurationException {

		CertificateData certificateData = new CertificateData();

		certificateData.setSubjectPublicKey(publicKey);
		certificateData.setIssuerDn(createUserIssuerDistinguishedName(certificateParameters));
		certificateData.setSubjectDn(createUserSubjectDistinguishedName(certificateParameters));
		certificateData.setValidityDates(createUserValidityDates(certificateParameters));

		return certificateData;
	}

	/**
	 * @param certificateParameters
	 * @throws GeneralCryptoLibException
	 */
	private X509DistinguishedName createUserSubjectDistinguishedName(final CertificateParameters certificateParameters)
			throws GeneralCryptoLibException {
		return new X509DistinguishedName.Builder(certificateParameters.getUserSubjectCn(),
				certificateParameters.getUserSubjectCountry())
						.addOrganizationalUnit(certificateParameters.getUserSubjectOrgUnit())
						.addOrganization(certificateParameters.getUserSubjectOrg()).build();

	}

	/**
	 * @param certificateParameters
	 * @throws GeneralCryptoLibException
	 */
	private X509DistinguishedName createUserIssuerDistinguishedName(final CertificateParameters certificateParameters)
			throws GeneralCryptoLibException {
		if (certificateParameters.getType() == CertificateParameters.Type.ROOT) {
			return createUserSubjectDistinguishedName(certificateParameters);
		} else {
			return new X509DistinguishedName.Builder(certificateParameters.getUserIssuerCn(),
					certificateParameters.getUserIssuerCountry())
							.addOrganizationalUnit(certificateParameters.getUserIssuerOrgUnit())
							.addOrganization(certificateParameters.getUserIssuerOrg()).build();
		}

	}

	/**
	 * @throws GeneralCryptoLibException
	 */
	private ValidityDates createUserValidityDates(final CertificateParameters certificateParameters)
			throws GeneralCryptoLibException {

		Date userNotBefore = Date.from(certificateParameters.getUserNotBefore().toInstant());
		Date userNotAfter = Date.from(certificateParameters.getUserNotAfter().toInstant());

		return new ValidityDates(userNotBefore, userNotAfter);
	}
}
