/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 7/09/15.
 */
package com.scytl.products.ov.config.exceptions.specific;


public class VerificationCardIdsGenerationException extends Exception {

	private static final long serialVersionUID = 4191302688176860484L;

	public VerificationCardIdsGenerationException(final String message) {
        super(message);
    }

    public VerificationCardIdsGenerationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public VerificationCardIdsGenerationException(final Throwable cause) {
        super(cause);
    }
}