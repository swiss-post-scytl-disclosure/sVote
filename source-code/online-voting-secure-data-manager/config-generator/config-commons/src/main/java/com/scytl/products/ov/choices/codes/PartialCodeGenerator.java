/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.choices.codes;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.mathematical.groups.GroupElement;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.config.exceptions.specific.GenerateVerificationCardCodesException;

public final class PartialCodeGenerator<T extends GroupElement<?>> {

    /**
     * Generates a partial code.
     *
     * @param base
     *            The value to be exponentiated. This value should be an element of the specified group.
     * @param exponentValue
     *            The value to which the base should be exponentiated.
     * @return The generated partial code.
     */
    @SuppressWarnings("unchecked")
	public T generatePartialCode(final T base, final Exponent exponentValue) {

		try {
			return (T) base.exponentiate(exponentValue);
		} catch (GeneralCryptoLibException e) {
			throw new GenerateVerificationCardCodesException(
					"An error occurred while trying to generate a partial code. Base: " + base + ", exponent: "
							+ exponentValue,
					e);
		}
	}
}
