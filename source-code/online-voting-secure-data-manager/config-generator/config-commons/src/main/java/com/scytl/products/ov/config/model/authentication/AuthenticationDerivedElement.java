/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import com.scytl.cryptolib.derivation.cryptoapi.CryptoAPIDerivedKey;

/**
 * Entity that represents a cryptographic derived key
 */
public class AuthenticationDerivedElement {


    private final CryptoAPIDerivedKey derivedKey;

    private final String derivedKeyInEx;



    private AuthenticationDerivedElement(final CryptoAPIDerivedKey derivedKey, final String derivedKeyInEx){
        this.derivedKey = derivedKey;
        this.derivedKeyInEx = derivedKeyInEx;
    }

    public static AuthenticationDerivedElement of(final CryptoAPIDerivedKey derivedKey, final String derivedKeyInEx){
        return new AuthenticationDerivedElement(derivedKey, derivedKeyInEx);
    }


    public CryptoAPIDerivedKey getDerivedKey(){
        return derivedKey;
    }

    public String getDerivedKeyInEx(){
        return derivedKeyInEx;
    }
}
