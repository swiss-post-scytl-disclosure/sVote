/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

public class SingleSecretAuthenticationKeyGeneratorException extends ConfigurationEngineException {

	private static final long serialVersionUID = 6847341172518146890L;

	public SingleSecretAuthenticationKeyGeneratorException(final String message) {
        super(message);
    }

    public SingleSecretAuthenticationKeyGeneratorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SingleSecretAuthenticationKeyGeneratorException(final Throwable cause) {
        super(cause);
    }

}
