/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication.service;

import com.scytl.products.ov.config.model.authentication.AuthSecretSpec;

import java.util.List;

/**
 * Supports the generation of random secrets (as strings).
 * <P>
 * The characteristics of the generated secrets (number of secrets and the length of each one) will match a particular
 * configuration.
 */
public interface AuthSecretsGenerator {

    /**
     * @return a list of secrets.
     */
    List<String> generate(List<AuthSecretSpec> authSecretSpecs);



}
