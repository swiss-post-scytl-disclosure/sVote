/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by riglesias on 8/07/16.
 */
package com.scytl.products.ov.config.model.authentication;

/**
 * Interface for defining creation strategies of the authentication key
 */
public interface ChallengeGenerator {

    /**
     * @return
     */
    ExtraParams generateExtraParams();
}
