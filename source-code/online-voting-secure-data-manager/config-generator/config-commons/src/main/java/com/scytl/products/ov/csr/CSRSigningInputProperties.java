/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.csr;

import java.time.ZonedDateTime;

import com.scytl.products.ov.datapacks.beans.CertificateParameters;

public final class CSRSigningInputProperties {

    private final ZonedDateTime notBefore;

    private final ZonedDateTime notAfter;

    private final CertificateParameters.Type type;

    public CSRSigningInputProperties(final ZonedDateTime notBefore, final ZonedDateTime notAfter,
            final CertificateParameters.Type type) {
        this.notBefore = notBefore;
        this.notAfter = notAfter;
        this.type = type;
    }

    public ZonedDateTime getNotBefore() {
        return notBefore;
    }

    public ZonedDateTime getNotAfter() {
        return notAfter;
    }

    public CertificateParameters.Type getType() {
        return type;
    }
}
