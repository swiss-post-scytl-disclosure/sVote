/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 7/09/15.
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateVotingCardSetException;

public class GenerateVerificationCardCodesException extends CreateVotingCardSetException {

	private static final long serialVersionUID = -6464334697497064225L;

	public GenerateVerificationCardCodesException(final String message) {
        super(message);
    }

    public GenerateVerificationCardCodesException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateVerificationCardCodesException(final Throwable cause) {
        super(cause);
    }
}
