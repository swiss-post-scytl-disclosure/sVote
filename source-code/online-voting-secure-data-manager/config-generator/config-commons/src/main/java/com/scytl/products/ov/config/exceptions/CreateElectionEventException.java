/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

/**
 * Created by jruiz on 7/09/15.
 */
public class CreateElectionEventException extends ConfigurationEngineException {

	private static final long serialVersionUID = -1270573848048325071L;

	public CreateElectionEventException(final String message) {
        super(message);
    }

    public CreateElectionEventException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CreateElectionEventException(final Throwable cause) {
        super(cause);
    }

}
