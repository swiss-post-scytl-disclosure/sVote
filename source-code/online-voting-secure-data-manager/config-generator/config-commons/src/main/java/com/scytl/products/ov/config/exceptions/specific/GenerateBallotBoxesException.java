/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateBallotBoxesException;

public class GenerateBallotBoxesException extends CreateBallotBoxesException {

	private static final long serialVersionUID = 770571259291263625L;

	public GenerateBallotBoxesException(final String message) {
        super(message);
    }

    public GenerateBallotBoxesException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateBallotBoxesException(final Throwable cause) {
        super(cause);
    }

}
