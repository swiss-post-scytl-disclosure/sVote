/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

/**
 * Created by jruiz on 7/09/15.
 */
public class CreateBallotBoxesException extends ConfigurationEngineException {

	private static final long serialVersionUID = 770571259291263625L;

	public CreateBallotBoxesException(final String message) {
        super(message);
    }

    public CreateBallotBoxesException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CreateBallotBoxesException(final Throwable cause) {
        super(cause);
    }

}
