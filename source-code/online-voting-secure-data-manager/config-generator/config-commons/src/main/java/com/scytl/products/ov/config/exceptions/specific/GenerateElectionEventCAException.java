/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 7/09/15.
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateElectionEventException;

public class GenerateElectionEventCAException extends CreateElectionEventException {

	private static final long serialVersionUID = 7632515886602651521L;

	public GenerateElectionEventCAException(final String message) {
        super(message);
    }

    public GenerateElectionEventCAException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateElectionEventCAException(final Throwable cause) {
        super(cause);
    }
}
