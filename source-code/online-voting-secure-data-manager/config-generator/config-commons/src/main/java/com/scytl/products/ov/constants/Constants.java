/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.constants;

public class Constants {

    // ////////////////////////////////////
    //
    // characters
    //
    // ////////////////////////////////////

    public static final int HEX_RADIX = 16;

    public static final String COMMA = ",";

    public static final String EQUAL = "=";

    public static final String SEMICOLON = ";";

    public static final String EMPTY = "";

    public static final String UNDERSCORE = "_";

    public static final String HEX_ALPHABET = "[0-9a-f]+";

    public static final String ELEMENTS_DELIMITER = SEMICOLON;

    // ////////////////////////////////////
    //
    // file extensions
    //
    // ////////////////////////////////////

    public static final String SKS = ".sks";

    public static final String PEM = ".pem";

    public static final String OUT = ".out";

    public static final String CSV = ".csv";

    public static final String JSON = ".json";
    
    public static final String KEY = ".key";

    // ////////////////////////////////////
    //
    // filenames
    //
    // ////////////////////////////////////

    public static final String PRIMES_LIST_FILENAME = "listPrimes.txt";

    public static final String ENCRYPTION_PARAMS_FILENAME = "encryptionParameters.json";

    public static final String PW_TXT = "passwords.txt";

    public static final String SERVICES_SIGNER_SKS_FILENAME = "servicesca.sks";

    public static final String CREDENTIAL_SIGNER_SKS_FILENAME = "credentialsca.sks";

    public static final String CONFIGURATION_SERVICES_PROPERTIES_FILENAME = "servicesProperties.json";

    public static final String KEYS_CONFIG_FILENAME = "keys_config.json";

    public static final String EB_PRIVATE_KEY_FILENAME = "EB_private.txt";

    public static final String AUTHENTICATION_VOTERDATA_FILENAME = "authenticationVoterData.json";

    public static final String AUTHENTICATION_CONTEXTDATA_FILENAME = "authenticationContextData.json";

    public static final String ELECTION_INFORMATION_CONTENTS_FILENAME = "electionInformationContents.json";

    public static final String VOTING_WORKFLOW_CONTEXT_DATA_FILENAME = "votingWorkflowContextData.json";

    public static final String ELECTORAL_AUTHORITY_FILE_NAME = "electoralAuthority.json";

    public static final String DECRYPTION_KEY_FILE_NAME = "decryptionKey.json";

    public static final String BALLOTBOX_CONTEXTDATA_FILENAME = "ballotBoxContextData.json";

    public static final String ENRICHED_BALLOT_FILENAME = "ballot.json";

    public static final String BALLOT_TEXTS_FILENAME = "ballot_i18n.json";

    public static final String BALLOT_BOX_FILENAME = "ballotBox.json";

    public static final String CLEANSING_DATA_FILENAME = "cleansingData.json";

    public static final String CLEANSING_VERIFICATION_DATA_FILENAME = "verificationData";
    
    public static final String VERIFICATION_CARDS_KEY_PAIR_DIRECTORY = "verificationCardsKeyPairs";

    // ////////////////////////////////////
    //
    // directories
    //
    // ////////////////////////////////////

    public static final String ONLINE_DIRECTORY = "ONLINE";

    public static final String OFFLINE_DIRECTORY = "OFFLINE";

    public static final String BALLOTS_DIRECTORY = "ballots";

    public static final String BALLOT_BOXES_DIRECTORY = "ballotBoxes";

    public static final String AUTHENTICATION_DIRECTORY = "authentication";

    public static final String ELECTION_INFORMATION_DIRECTORY = "electionInformation";

    public static final String VOTING_WORKFLOW_DIRECTORY = "votingWorkflow";

    public static final String VOTER_MATERIAL_DIRECTORY = "voterMaterial";

    public static final String VOTE_VERIFICATION_DIRECTORY = "voteVerification";

    public static final String ELECTORAL_AUTHORITIES_DIRECTORY = "electoralAuthorities";

    public static final String PRINTING_DIRECTORY = "printing";

    public static final String CLEANSING_DIRECTORY = "cleansing";

    public static final String CUSTOM_DIRECTORY = "CUSTOMER";

    public static final String EXTENDED_AUTHENTICATION_DIRECTORY = "extendedAuthentication";

    // ////////////////////////////////////
    //
    // lengths
    //
    // ////////////////////////////////////

    public static final int NUM_DIGITS_SHORT_CHOICE_CODE = 4;

    public static final int NUM_DIGITS_BALLOT_CASTING_KEY_WITHOUT_CHECKSUM = 8;

    public static final int NUM_DIGITS_BALLOT_CASTING_KEY_FINAL = 9;

    public static final int NUM_DIGITS_VOTE_CASTING_CODE = 8;

    public static final int NUM_KEYS_CHOICES_CODE_KEYPAIR = 150;

    public static final int SVK_LENGTH = 20;

    public static final int SMART_CARD_LABEL_MAX_LENGTH = 32;

    public static final String SVK_ALPHABET = "23456789abcdefghijkmnpqrstuvwxyz";

    // ////////////////////////////////////
    //
    // tags and miscellaneous constants
    //
    // ////////////////////////////////////

    public static final String CONFIGURATION_SERVICES_CA_JSON_TAG = "servicesca";

    public static final String CONFIGURATION_ELECTION_CA_JSON_TAG = "electioneventca";

    public static final String CONFIGURATION_CREDENTIALS_CA_JSON_TAG = "credentialsca";

    public static final String CONFIGURATION_AUTHORITIES_CA_JSON_TAG = "authoritiesca";

    public static final String CONFIGURATION_ADMINBOARD_CA_JSON_TAG = "adminboard";

    public static final String CONFIGURATION_SERVICES_CA_PRIVATE_KEY_JSON_TAG = "privateKey";

    public static final String CONFIGURATION_CREDENTIALS_CA_PRIVATE_KEY_JSON_TAG = "privateKey";

    public static final String CONFIGURATION_VERIFICATION_CARD_PRIVATE_KEY_JSON_TAG = "privateKey";

    public static final String CONFIGURATION_AUTH_TOKEN_SIGNER_JSON_TAG = "authTokenSigner";

    public static final String CONFIGURATION_BALLOTBOX_JSON_TAG = "ballotBox";

    public static final String CONFIGURATION_SERVICES_SIGNER_JSON_TAG = "servicesSignerCA";

    public static final String CONFIGURATION_SERVICES_ENCRYPTION_JSON_TAG = "servicesSignerCA";

    public static final String CREDENTIAL_ID = "credentialid";

    public static final String AUTH_ID = "authid";

    public static final String AUTH_PW = "authpassword";

    public static final String START_VOTING_KEY = "startvotingkey";

    public static final String IDENTIFIER = "identifier";

    public static final String PW = "password";

    public static final String KEYSTORE_PIN = "keystorepin";

    public static final String VOTER_INFORMATION_FILENAME = "voterInformation";

    public static final String CREDENTIAL_DATA_FILENAME = "credentialData";

    public static final String VCID_TO_PIN_FILENAME = "vcid_to_pin";

    public static final String VCID_TO_SVK_FILENAME = "vcid_to_svk";

    public static final String CREDID_TO_VCID_FILENAME = "credid_to_vcid";

    public static final String CODES_MAPPING_TABLES_CONTEXT_DATA = "codesMappingTablesContextData";

    public static final String COMPUTED_VALUES_FILENAME = "computedValues";
    
    public static final String NODE_CONTRIBUTIONS_FILENAME = "nodeContributions";
    
    public static final String DERIVED_KEYS_FILENAME = "derivedKeys";

    public static final String VERIFICATION_CARD_ID_TO_VOTING_CARD_ID = "verificationCardID_to_vcid";

    public static final String VERIFICATION_CARD_IDS = "verificationCardIds";

    public static final String MESSAGE_DIGEST_ALGORITHM = "SHA-256";

    public static final String SECRET_KEY_ALGORITHM = "AES";

    public static final String VERIFICATION_CARD_SET_DATA = "verificationCardSetData";

    public static final String VOTE_VERIFICATION_CONTEXT_DATA = "voteVerificationContextData";

    public static final String VERIFICATION_CARD_DATA = "verificationCardData";

    public static final String VERIFICATION_CARD_CODES = "verificationCardCodes";

    public static final String PRINTING_DATA = "printingData";

    public static final String EXTENDED_AUTHENTICATION = "extendedAuthentication";

    public static final String PROVIDED_CHALLENGE = "aliases";

    public static final String SECRETS_DATA = "secrets";

    public static final String NUMIC_OTL_MAPPING_FILENAME = "notification_service";

    public static final String ID_MAPPING_FILENAME = "id_mapping";

    public static final String CRED_RECOVERY_FILENAME = "recovery";

    public static final String CRED_UPDATE_FILENAME = "update";

    public static final String SMS_PROVIDER_FILENAME = "sms";

    // ////////////////////////////////////
    //
    // CSR constants
    //
    // ////////////////////////////////////
    public static final String CSR_BEGIN_STRING = "-----BEGIN CERTIFICATE REQUEST-----";

    public static final String CSR_END_STRING = "-----END CERTIFICATE REQUEST-----";

    public static final String CSR_SIGNING_ALGORITHM = "SHA256withRSA";

    //////////////
    ///
    /// OTHER CONSTANTS
    ///
    ///////////////////

    public static final int RANDOM_SALT_LENGTH = 64;

    public static final int PBKDF2_MIN_EXTRA_PARAM_LENGTH = 16;
    
    public static final int KEYSTORE_PW_LENGTH = 26;
    
    public static final String LOGGING_CLIENT_ADDRESS = "127.0.0.1";	//NOSONAR 'Make this IP "127.0.0.1" address configurable'
    
    public static final String LOGGING_SERVER_ADDRESS = "127.0.0.1";	//NOSONAR 'Make this IP "127.0.0.1" address configurable'

    /**
	 * Non-public constructor
	 */
	private Constants() {
	}
}
