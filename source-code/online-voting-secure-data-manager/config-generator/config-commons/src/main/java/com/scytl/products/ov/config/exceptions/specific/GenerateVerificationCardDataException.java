/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 7/09/15.
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateVotingCardSetException;

public class GenerateVerificationCardDataException extends CreateVotingCardSetException {

	private static final long serialVersionUID = -2311425072164493511L;

	public GenerateVerificationCardDataException(final String message) {
        super(message);
    }

    public GenerateVerificationCardDataException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateVerificationCardDataException(final Throwable cause) {
        super(cause);
    }
}
