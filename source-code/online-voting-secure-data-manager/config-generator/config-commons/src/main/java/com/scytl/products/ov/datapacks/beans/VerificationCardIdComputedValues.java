/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.datapacks.beans;

import java.util.List;

/**
 * An abstract class that defines the basic information that arrives to a
 * generator.
 */
public class VerificationCardIdComputedValues {

    private String id;

    private String ballotVotingOption2prePartialChoiceCodes;

    private String encryptedBallotCastingKey;
    
    private String computedBallotCastingKey;
    
    private List<String> choiceCodesKeyCommitments;

    private List<String> ballotCastingKeyExponentCommitments;
    
    public VerificationCardIdComputedValues(String id,
            String ballotVotingOption2prePartialChoiceCodes,
            String encryptedBallotCastingKey,
            String computedBallotCastingKey) {
        super();
        this.id = id;
        this.ballotVotingOption2prePartialChoiceCodes = ballotVotingOption2prePartialChoiceCodes;
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
        this.computedBallotCastingKey = computedBallotCastingKey;
    }

    public VerificationCardIdComputedValues(String id,
            String ballotVotingOption2prePartialChoiceCodes,
            String encryptedBallotCastingKey,
            String computedBallotCastingKey,
            List<String> choiceCodesKeyCommitments,
            List<String> ballotCastingKeyExponentCommitments) {
        super();
        this.id = id;
        this.ballotVotingOption2prePartialChoiceCodes = ballotVotingOption2prePartialChoiceCodes;
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
        this.computedBallotCastingKey = computedBallotCastingKey;
        this.choiceCodesKeyCommitments = choiceCodesKeyCommitments;
        this.ballotCastingKeyExponentCommitments = ballotCastingKeyExponentCommitments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBallotVotingOption2prePartialChoiceCodes() {
        return ballotVotingOption2prePartialChoiceCodes;
    }

    public void setBallotVotingOption2prePartialChoiceCodes(
            String ballotVotingOption2prePartialChoiceCodes) {
        this.ballotVotingOption2prePartialChoiceCodes = ballotVotingOption2prePartialChoiceCodes;
    }
    
    public String getEncryptedBallotCastingKey() {
        return encryptedBallotCastingKey;
    }

    public String getComputedBallotCastingKey() {
        return computedBallotCastingKey;
    }
    
    public void setEncryptedBallotCastingKey(
            String encryptedBallotCastingKey) {
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
    }
    
    public void setComputedBallotCastingKey(
            String computedBallotCastingKey) {
        this.computedBallotCastingKey = computedBallotCastingKey;
    }

    public List<String> getChoiceCodesKeyCommitments() {
        return choiceCodesKeyCommitments;
    }

    public void setChoiceCodesKeyCommitments(List<String> choiceCodesKeyCommitments) {
        this.choiceCodesKeyCommitments = choiceCodesKeyCommitments;
    }

    public List<String> getBallotCastingKeyExponentCommitments() {
        return ballotCastingKeyExponentCommitments;
    }

    public void setBallotCastingKeyExponentCommitments(List<String> ballotCastingKeyExponentCommitments) {
        this.ballotCastingKeyExponentCommitments = ballotCastingKeyExponentCommitments;
    }
}
