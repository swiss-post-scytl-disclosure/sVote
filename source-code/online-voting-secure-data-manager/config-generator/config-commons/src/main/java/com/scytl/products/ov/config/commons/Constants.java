/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons;

public class Constants {

    public static final String JOB_IN_PARAM_VOTINGCARDSET_ID = "votingCardSetId";

    public static final String JOB_IN_PARAM_ELECTIONEVENT_ID = "electionEventId";

    public static final String JOB_IN_PARAM_TENANT_ID = "tenantId";

    public static final String JOB_IN_PARAM_BALLOTBOX_ID = "ballotBoxId";

    public static final String JOB_IN_PARAM_BALLOT_ID = "ballotId";

    public static final String JOB_IN_PARAM_BALLOT = "ballot";

    public static final String JOB_IN_PARAM_BASE_PATH = "basePath";

    public static final String JOB_IN_PARAM_ELECTORALAUTHORITY_ID = "electoralAuthorityId";

    public static final String JOB_IN_PARAM_ELECTION_START = "electionStart";

    public static final String JOB_IN_PARAM_ELECTION_END = "electionEnd";

    public static final String JOB_IN_PARAM_VALIDITY_PERIOD = "validityPeriod";

    public static final String JOB_IN_PARAM_KEY_FOR_PROTECTING_KEYSTORE_PW = "keyForProtectingKeystorePassword";

    public static final String JOB_IN_PARAM_VALIDITY_PERIOD_START = "validityPeriodStart";

    public static final String JOB_IN_PARAM_VALIDITY_PERIOD_END = "validityPeriodEnd";

    public static final String JOB_IN_PARAM_JOB_INSTANCE_ID = "jobInstanceId";

    public static final String JOB_IN_PARAM_NUMBER_VOTING_CARDS = "numberVotingCards";

    public static final String JOB_IN_PARAM_VOTING_CARD_SET_NAME = "votingCardSetName";

    public static final String JOB_IN_PARAM_VERIFICATIONCARDSET_ID = "verificationCardSetId";

    public static final String JOB_OUT_PARAM_GENERATED_VC_COUNT = "generatedVotingCardCount";

    public static final String JOB_OUT_PARAM_ERROR_COUNT = "errorCount";

    public static final String JOB_IN_PARAM_STATUS = "status";

    public static final String JOB_IN_PARAM_SALT_CREDENTIAL_ID = "saltCredentialId";

    public static final String JOB_IN_PARAM_SALT_PIN = "saltPIN";

    public static final String JOB_IN_PARAM_CHOICE_CODES_ENCRYPTION_KEY = "choiceCodesEncryptionKey";

    public static final String JOB_IN_PARAM_PRIME_ENCRYPTION_PRIVATE_KEY_JSON = "primeEncryptionPrivateKeyJson";

    public static final String JOB_IN_PARAM_PLATFORM_ROOT_CA_CERTIFICATE = "platformRootCACertificate";

    public static final String JOB_IN_PARAM_VOTING_CARD_SET_CERTIFICATE_PROPERTIES =
        "votingCardSetCertificateProperties";

    public static final String JOB_IN_PARAM_VERIFICATION_CARD_SET_CERTIFICATE_PROPERTIES =
        "verificationCardSetCertificateProperties";

    public static final String JOB_IN_PARAM_CREDENTIAL_SIGN_CERTIFICATE_PROPERTIES =
        "credentialSignCertificateProperties";

    public static final String JOB_IN_PARAM_CREDENTIAL_AUTH_CERTIFICATE_PROPERTIES =
        "credentialAuthCertificateProperties";

    public static final String JOB_NAME_PREFIX = "votingcardset-generation";

    public static final String CHOICE_CODES_KEY_DELIMITER = ";";

    /**
     * Non-public constructor
     */
    private Constants() {
    }
}
