/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.readers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.scytl.products.ov.datapacks.beans.ConfigurationInput;
import com.scytl.products.ov.utils.ConfigObjectMapper;

/**
 * A ConfigObjectMapper wrapper to get from a file ConfigurationInput objects.
 */
public class ConfigurationInputReader {

    private final ConfigObjectMapper configObjectMapper = new ConfigObjectMapper();

    public ConfigurationInput fromFileToJava(final File src)
            throws IOException {
        return configObjectMapper.fromJSONFileToJava(src, ConfigurationInput.class);
    }

    public ConfigurationInput fromStreamToJava(final InputStream src)
        throws IOException {
        return configObjectMapper.fromJSONStreamToJava(src, ConfigurationInput.class);
    }


}
