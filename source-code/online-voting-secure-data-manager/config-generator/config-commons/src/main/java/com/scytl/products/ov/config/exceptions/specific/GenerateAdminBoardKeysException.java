/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by jruiz on 7/09/15.
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateElectionEventException;

public class GenerateAdminBoardKeysException extends CreateElectionEventException {

	private static final long serialVersionUID = -6892894405026386682L;

	public GenerateAdminBoardKeysException(final String message) {
        super(message);
    }

    public GenerateAdminBoardKeysException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateAdminBoardKeysException(final Throwable cause) {
        super(cause);
    }
}
