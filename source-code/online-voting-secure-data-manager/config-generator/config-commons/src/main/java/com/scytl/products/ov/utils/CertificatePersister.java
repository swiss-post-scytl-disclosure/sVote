/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.products.ov.config.exceptions.ConfigurationEngineException;

public class CertificatePersister {

	/**
     * Non-public constructor
     */
	private CertificatePersister() {
    }
	
	public static void saveCertificate(final CryptoAPIX509Certificate certificate, final Path path)
			throws ConfigurationEngineException {

		try (final PrintWriter writer = new PrintWriter(path.toFile())) {
			writer.write(new String(certificate.getPemEncoded(), StandardCharsets.UTF_8));
			writer.close();
		} catch (FileNotFoundException e) {
			throw new ConfigurationEngineException(
					"An error occurred while persisting the certificate " + path.toString(), e);
		}

	}
}
