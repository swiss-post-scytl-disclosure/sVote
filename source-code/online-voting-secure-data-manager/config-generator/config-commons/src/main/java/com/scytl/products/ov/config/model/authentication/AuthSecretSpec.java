/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

/**
 * Defines a particular authentication secret specification.
 */
public final class AuthSecretSpec {

    private final String name;

    private final int length;

    public AuthSecretSpec(final String name, final int length) {
        this.name = name;
        this.length = length;
    }

    /**
     * @return the name of the secret.
     */
    public String getName() {
        return name;
    }

    /**
     * @return the length of the secret.
     */
    public int getLength() {
        return length;
    }
}
