/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by riglesias on 8/07/16.
 */
package com.scytl.products.ov.config.model.authentication;

import com.scytl.products.ov.constants.Constants;

/**
 * Interface for defining creation strategies of the authentication key
 */
public interface AuthenticationKeyGenerator {

    /**
     * @return generate a new AuthenticationKey using the starVotingKey as
     */
    AuthenticationKey generateAuthKey(StartVotingKey startVotingKey);


    default int getSecretsLength() {
        return Constants.SVK_LENGTH;
    }

    default String getAlphabet() {
        return Constants.SVK_ALPHABET;
    }

}
