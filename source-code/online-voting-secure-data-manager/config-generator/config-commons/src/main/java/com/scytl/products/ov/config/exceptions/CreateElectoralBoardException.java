/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

/**
 * Created by jruiz on 7/09/15.
 */
public class CreateElectoralBoardException extends ConfigurationEngineException {

	private static final long serialVersionUID = 1428478496064165833L;

	public CreateElectoralBoardException(final String message) {
        super(message);
    }

    public CreateElectoralBoardException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CreateElectoralBoardException(final Throwable cause) {
        super(cause);
    }

}
