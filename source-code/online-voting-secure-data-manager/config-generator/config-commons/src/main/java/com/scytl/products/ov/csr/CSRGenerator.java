/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.csr;

import java.security.PrivateKey;
import java.security.PublicKey;

import javax.security.auth.x500.X500Principal;

import com.scytl.products.ov.constants.Constants;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

public final class CSRGenerator {

	public PKCS10CertificationRequest generate(final PublicKey publickey, final PrivateKey privatekey,
			final X500Principal subject) throws OperatorCreationException {

		final ContentSigner signGen = new JcaContentSignerBuilder(Constants.CSR_SIGNING_ALGORITHM).build(privatekey);

		final PKCS10CertificationRequestBuilder builder = new JcaPKCS10CertificationRequestBuilder(subject, publickey);
		return builder.build(signGen);
	}
}
