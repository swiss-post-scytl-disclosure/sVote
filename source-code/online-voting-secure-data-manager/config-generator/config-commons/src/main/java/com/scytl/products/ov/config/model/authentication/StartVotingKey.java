/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

/**
 * Class Representing the SVK entity
 */
public class StartVotingKey {

    private final String value;

    private StartVotingKey(final String value){
        this.value = value;
    }
    public static StartVotingKey ofValue(String value){
        return new StartVotingKey(value);
    }

    public String getValue() {
        return value;
    }
}
