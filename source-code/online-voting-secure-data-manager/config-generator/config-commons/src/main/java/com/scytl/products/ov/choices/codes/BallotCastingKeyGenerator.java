/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.choices.codes;

public interface BallotCastingKeyGenerator {

    /**
     * Generate a ballot casting key.
     *
     * @return the generated ballot casting key.
     */
    String generateBallotCastingKey();
}
