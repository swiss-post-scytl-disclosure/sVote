/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

/**
 * Created by jruiz on 7/09/15.
 */
public class ConfigurationEngineException extends RuntimeException {

	private static final long serialVersionUID = 4444578007782610068L;

	public ConfigurationEngineException(final String message) {
        super(message);
    }

    public ConfigurationEngineException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ConfigurationEngineException(final Throwable cause) {
        super(cause);
    }
}
