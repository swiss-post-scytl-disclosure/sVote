/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.encryption.params.generation;

import java.math.BigInteger;

/**
 * Encapsulates ElGamal encryption parameters.
 */
public final class ElGamalEncryptionParameters {


    private BigInteger p;

    private BigInteger q;

    private BigInteger g;

    public ElGamalEncryptionParameters() {
    }

    public ElGamalEncryptionParameters(final BigInteger p, final BigInteger q, final BigInteger g) {

        this.p = p;
        this.q = q;
        this.g = g;
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getQ() {
        return q;
    }

    public BigInteger getG() {
        return g;
    }

    /**
     * @param p
     *            The p to set.
     */
    public void setP(final BigInteger p) {
    	this.p = p;
    }

    /**
     * @param q
     *            The q to set.
     */
    public void setQ(final BigInteger q) {
    	this.q = q;
    }

    /**
     * @param g
     *            The g to set.
     */
    public void setG(final BigInteger g) {
    	this.g = g;
    }

}
