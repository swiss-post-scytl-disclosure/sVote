/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecutionTimeLogger {

    private static final Logger LOG = LoggerFactory.getLogger("std");

	private final String operationName;

	private long lastActionInvocationTimestamp = System.currentTimeMillis();

	private final long firstActionInvocationTimestamp = lastActionInvocationTimestamp;

	public ExecutionTimeLogger(final String operationName) {
		this.operationName = operationName;
	}

	public void log(final String actionName) {
		lastActionInvocationTimestamp = logAction(actionName, lastActionInvocationTimestamp);
	}

	private long logAction(final String actionName, long invocationTimestamp) {
		long currentActionInvocationTimestamp = System.currentTimeMillis();
		if (LOG.isTraceEnabled()) {
			LOG.trace(operationName + "." + actionName + ": "
					+ Long.toString(currentActionInvocationTimestamp - invocationTimestamp));
		}
		return currentActionInvocationTimestamp;
	}

	public void resetTimer() {
		lastActionInvocationTimestamp = System.currentTimeMillis();
	}

	public void logTotalElapsedTime() {
		logAction("totalTime", firstActionInvocationTimestamp);
	}

}