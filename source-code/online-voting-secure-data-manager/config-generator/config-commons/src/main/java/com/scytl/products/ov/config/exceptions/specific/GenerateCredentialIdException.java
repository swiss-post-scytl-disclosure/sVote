/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateVotingCardSetException;

public class GenerateCredentialIdException extends CreateVotingCardSetException {

	private static final long serialVersionUID = 5449910856687285210L;

	public GenerateCredentialIdException(final String message) {
        super(message);
    }

    public GenerateCredentialIdException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateCredentialIdException(final Throwable cause) {
        super(cause);
    }
}
