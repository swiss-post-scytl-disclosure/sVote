/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.datapacks.beans;

import static java.util.Arrays.copyOf;
import static java.util.Arrays.fill;

import java.security.PrivateKey;

import com.scytl.cryptolib.scytl.keystore.cryptoapi.CryptoAPIScytlKeyStore;
import com.scytl.products.ov.utils.KeyStoreReader;

/**
 * A base class to represent the basic information of a datapack: a keystore, a
 * password and a parentPrivateKey representing the certificate parent on the
 * chain cert.
 */
public class SerializedCredentialDataPack {

    private String serializedKeyStore;

    private char[] password;

    private String encryptedPassword;

    private PrivateKey parentPrivateKey;

    /**
     * @return Returns the parentPrivateKey.
     */
    public PrivateKey getParentPrivateKey() {
        return parentPrivateKey;
    }

    /**
     * @param parentPrivateKey
     *            The parentPrivateKey to set.
     */
    public void setParentPrivateKey(final PrivateKey parentPrivateKey) {
        this.parentPrivateKey = parentPrivateKey;
    }

    public String getSerializedKeyStore() {
        return serializedKeyStore;
    }

    public void setKeystoreToBeSerialized(final CryptoAPIScytlKeyStore keyStore, char[] password) {
    	this.serializedKeyStore = KeyStoreReader.toString(keyStore, password);
        setPassword(password);
    }

    public void clearPassword() {
        fill(password, ' ');
    }

    public char[] getPassword() {
        return copyOf(password, password.length);
    }

    private void setPassword(final char[] password) {
    	this.password = copyOf(password, password.length);
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(final String encryptedPassword) {
    	this.encryptedPassword = encryptedPassword;
    }
}
