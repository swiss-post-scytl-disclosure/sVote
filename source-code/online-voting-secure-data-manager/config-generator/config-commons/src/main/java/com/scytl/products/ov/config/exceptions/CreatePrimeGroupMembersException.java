/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

public class CreatePrimeGroupMembersException extends ConfigurationEngineException {

	private static final long serialVersionUID = 3465465753572143460L;

	public CreatePrimeGroupMembersException(final String message) {
        super(message);
    }

    public CreatePrimeGroupMembersException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CreatePrimeGroupMembersException(final Throwable cause) {
        super(cause);
    }
}
