/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.ConfigurationEngineException;

public class CSRLoaderException extends ConfigurationEngineException {

	private static final long serialVersionUID = 3115164062771006609L;

	public CSRLoaderException(final String message) {
        super(message);
    }

    public CSRLoaderException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CSRLoaderException(final Throwable cause) {
        super(cause);
    }

}
