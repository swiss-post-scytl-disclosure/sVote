/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.beans.spring.batch;

import org.springframework.batch.core.JobParameter;

/**
 * This class overrides toString of JobParameter so that when Spring Batch prints this parameter, it will not reveal
 * its value
 */
public class SensitiveJobParameter extends JobParameter {

    public SensitiveJobParameter(String parameter, boolean identifying) {
        super(parameter, identifying);
    }

    @Override
    public String toString() {
        return "******";
    }
}
