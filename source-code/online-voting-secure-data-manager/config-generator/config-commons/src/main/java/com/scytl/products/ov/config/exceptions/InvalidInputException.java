/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions;

/**
 * Created by jruiz on 7/09/15.
 */
public class InvalidInputException extends ConfigurationEngineException {

	private static final long serialVersionUID = 3706414159718245813L;

	public InvalidInputException(final String message) {
        super(message);
    }

    public InvalidInputException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidInputException(final Throwable cause) {
        super(cause);
    }

}
