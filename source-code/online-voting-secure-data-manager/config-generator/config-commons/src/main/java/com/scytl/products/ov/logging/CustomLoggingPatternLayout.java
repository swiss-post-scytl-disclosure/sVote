/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.logging;

import org.apache.log4j.PatternLayout;

public class CustomLoggingPatternLayout extends PatternLayout {

    @Override
    public boolean ignoresThrowable() {
        return false;
    }

}