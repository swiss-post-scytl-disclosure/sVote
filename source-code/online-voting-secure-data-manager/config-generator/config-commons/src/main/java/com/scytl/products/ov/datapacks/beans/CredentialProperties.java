/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.datapacks.beans;

import java.util.Map;

/**
 * A class representing all properties needed to generate a credential: name, alias, properties file, parent name (for
 * the cert chain) and credential type. Used as a part of ConfigurationInput bean.
 */
public class CredentialProperties {

    private String name;

    private Map<String, String> alias;

    private String propertiesFile;

    private String parentName;

    private CertificateParameters.Type credentialType;

    /**
     * @return Returns the credentialType.
     */
    public CertificateParameters.Type getCredentialType() {
        return credentialType;
    }

    /**
     * @param credentialType
     *            The credentialType to set.
     */
    public void setCredentialType(final CertificateParameters.Type credentialType) {
        this.credentialType = credentialType;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
    	this.name = name;
    }

    /**
     * @return Returns the alias.
     */
    public Map<String, String> getAlias() {
        return alias;
    }

    /**
     * @param alias
     *            The alias to set.
     */
    public void setAlias(final Map<String, String> alias) {
    	this.alias = alias;
    }

    /**
     * @return Returns the propertiesFile.
     */
    public String getPropertiesFile() {
        return propertiesFile;
    }

    /**
     * @param propertiesFile
     *            The propertiesFile to set.
     */
    public void setPropertiesFile(final String propertiesFile) {
    	this.propertiesFile = propertiesFile;
    }

    /**
     * @return Returns the parentName.
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * @param parentName
     *            The parentName to set.
     */
    public void setParentName(final String parentName) {
    	this.parentName = parentName;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CredentialProperties [_name=" + name + ", _alias=" + alias + ", _propertiesFile=" + propertiesFile
            + ", _parentName=" + parentName + ", _credentialType=" + credentialType + "]";
    }

    public String obtainPrivateKeyAlias() {
        return alias.get("privateKey");
    }

    public String obtainSecretKeyAlias() {
        return alias.get("secretKey");
    }
}
