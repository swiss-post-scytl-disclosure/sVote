/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
  * Created by riglesias on 8/07/16.
*/
package com.scytl.products.ov.config.model.authentication;

import java.util.Optional;

/**
 * Entity representing an authentication key of a voter
 */
public class ExtraParams {

    private final Optional<String> value;

    private final Optional<String> alias;

    private ExtraParams(final Optional<String> value, final Optional<String> alias) {
        this.value = value;
        this.alias = alias;
    }

    public static ExtraParams ofChallenges(final Optional<String> value, final Optional<String> alias) {
        return new ExtraParams(value, alias);
    }

    public Optional<String> getValue() {
        return value;
    }

    public Optional<String> getAlias() {
        return alias;
    }
}
