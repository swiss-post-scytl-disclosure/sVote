/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.CreateVotingCardSetException;

public class GenerateAuthenticationValuesException extends CreateVotingCardSetException {

	private static final long serialVersionUID = 3880165186665546553L;

	public GenerateAuthenticationValuesException(final String message) {
        super(message);
    }

    public GenerateAuthenticationValuesException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GenerateAuthenticationValuesException(final Throwable cause) {
        super(cause);
    }

}
