/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.exceptions.specific;

import com.scytl.products.ov.config.exceptions.ConfigurationEngineException;

public class PasswordReaderUtilsException extends ConfigurationEngineException {

	private static final long serialVersionUID = -1941655914103369906L;

	public PasswordReaderUtilsException(final String message) {
        super(message);
    }

    public PasswordReaderUtilsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PasswordReaderUtilsException(final Throwable cause) {
        super(cause);
    }

}
