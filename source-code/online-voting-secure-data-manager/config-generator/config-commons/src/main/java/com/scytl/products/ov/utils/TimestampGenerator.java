/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * A utility class to share a DateTimeFormatter with yyyyMMddHHmmssnnnnnnnnn format.
 */
public class TimestampGenerator {

    private static final DateTimeFormatter FORMATTING_PATTERN = DateTimeFormatter.ofPattern("yyyyMMddHHmmssnnnnnnnnn");

    public String getNewTimeStamp() {
        final LocalDateTime dateTime = LocalDateTime.now();
        return dateTime.format(FORMATTING_PATTERN);
    }

    /**
     * @return Returns the dateFormat.
     */
    public DateTimeFormatter getDateFormat() {
        return FORMATTING_PATTERN;
    }

}
