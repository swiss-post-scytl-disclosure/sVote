/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import java.util.List;

public class ProvidedChallenges {

    private final String alias;

    private final List<String> challenges;

    public ProvidedChallenges(final String alias, final List<String> challenges) {
        this.alias = alias;
        this.challenges = challenges;
    }

    public String getAlias() {
        return alias;
    }

    public List<String> getChallenges() {
        return challenges;
    }
}
