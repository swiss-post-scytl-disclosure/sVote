/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

import java.io.IOException;
import java.security.Security;
import java.util.Map;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.beans.domain.model.platform.PlatformInstallationData;
import com.scytl.products.ov.config.install.PlatformDataUploader;

import mockit.Expectations;
import mockit.Mocked;

public class InstallPlatformCommandTest {

    private static String PLATFORM_ROOT_CA_PATH = "src/test/resources/testMaterial/platformRoot/swisspostRootCA.pem";

    private static String PLATFORM_ROOT_ISSUER_CA_PATH =
        "src/test/resources/testMaterial/platformRoot/swisspostRootCA.pem";

    private static String LOGGING_KEYS_FOLDER = "src/test/resources/testMaterial/loggingKeys";

    private static String SERVICES_PATH = "src/test/resources/serviceCertificateParameters.json";

    private static String PASSWORD = "password";

    private static Map<String, String> params2valuesForPlatformRoot;

    private static Map<String, String> params2valuesForPlatformRootWithExternalCa;

    private static InstallPlatformCommand _sut;

    @BeforeClass
    public static void setUp() {

        Security.addProvider(new BouncyCastleProvider());

        String[] inputParamsForOnlyPlatformRoot = {"-rootca", PLATFORM_ROOT_CA_PATH, "-loggingkeys",
                LOGGING_KEYS_FOLDER, "-ser", SERVICES_PATH, "-password", PASSWORD };
        params2valuesForPlatformRoot = Action.INSTALL_PLATFORM.validateAndArrange(inputParamsForOnlyPlatformRoot);

        String[] inputParams = {"-rootca", PLATFORM_ROOT_CA_PATH, "-rootcaissuer", PLATFORM_ROOT_ISSUER_CA_PATH,
                "-loggingkeys", LOGGING_KEYS_FOLDER, "-ser", SERVICES_PATH, "-password", PASSWORD };
        params2valuesForPlatformRootWithExternalCa = Action.INSTALL_PLATFORM.validateAndArrange(inputParams);
    }

    @Test
    public void installPlatformToContexts(@Mocked final PlatformDataUploader platformDataUploader,
            @Mocked final PlatformInstallationData platformInstallationData)
            throws GeneralCryptoLibException, IOException {

        _sut = new InstallPlatformCommand();

        new Expectations() {
            {
                platformDataUploader.upload("AU", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("EI", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("VM", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("VV", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("VW", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("CR", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;
            }
        };

        _sut.execute(params2valuesForPlatformRoot);
    }

    @Test
    public void installPlatformToContextsWithExternalRoot(@Mocked final PlatformDataUploader platformDataUploader,
            @Mocked final PlatformInstallationData platformInstallationData)
            throws GeneralCryptoLibException, IOException {

        _sut = new InstallPlatformCommand();

        new Expectations() {
            {
                platformDataUploader.upload("AU", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("EI", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("VM", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("VV", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("VW", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;

                platformDataUploader.upload("CR", (PlatformInstallationData) any, PASSWORD);
                result = true;
                times = 1;
            }
        };

        _sut.execute(params2valuesForPlatformRootWithExternalCa);
    }
}
