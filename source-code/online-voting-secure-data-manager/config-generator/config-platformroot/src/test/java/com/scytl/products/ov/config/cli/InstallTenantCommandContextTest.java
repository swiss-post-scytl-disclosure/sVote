/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.Security;
import java.util.Map;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.service.AsymmetricService;
import com.scytl.products.ov.config.commons.ConfigPlatformRootKeyLoader;
import com.scytl.products.ov.config.commons.exception.ConfigPlatformRootException;

import mockit.Mock;
import mockit.MockUp;

@RunWith(MockitoJUnitRunner.class)
public class InstallTenantCommandContextTest {

    private static String TENANT_ID = "100";

    private static String TENANT_CERT = "src/test/resources/testMaterial/tenantCAandKS/tenant_manolo.pem";

    private static String SYSTEM_KEYS = "src/test/resources/testMaterial/systemKeys";

    private static String PASSWORD = "33HQ7IHDTCHIV35SHBAOG7HKXAS4CI2Z";

    private static Map<String, String> params2values;

    private static InstallTenantCommand _sut;

    @BeforeClass
    public static void setUp() throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        String[] inputParams =
            {"-tenantid", TENANT_ID, "-tenantcert", TENANT_CERT, "-systemkeys", SYSTEM_KEYS, "-password", PASSWORD };
        params2values = Action.INSTALL_TENANT.validateAndArrange(inputParams);
    }

    @Test
    public void installPlatformToContexts() throws Exception {
        new MockUp<ConfigPlatformRootKeyLoader>() {
            @Mock
            PrivateKey getPrivateKeyFromKeystore(final String password)
                    throws ConfigPlatformRootException, GeneralCryptoLibException {
                AsymmetricService asymmetricService = new AsymmetricService();
                KeyPair keyPair = asymmetricService.getKeyPairForSigning();
                return keyPair.getPrivate();
            }
        };
        _sut = new InstallTenantCommand();
        _sut.execute(params2values);
    }
}
