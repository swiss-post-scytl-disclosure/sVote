/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.install;

import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.service.AsymmetricService;
import com.scytl.products.ov.commons.infrastructure.exception.OvCommonsInfrastructureException;
import com.scytl.products.ov.commons.infrastructure.remote.client.NodeIdentifier;
import com.scytl.products.ov.commons.infrastructure.remote.client.RestClientConnectionManager;
import com.scytl.products.ov.config.commons.ConfigPlatformRootKeyLoader;
import com.scytl.products.ov.keystore.KeystorePasswords;
import com.scytl.products.ov.keystore.KeystorePasswordsReader;
import com.scytl.products.ov.keystore.TestKeystorePasswordsReader;

public class PlatformDataUploaderTest {

    @Test
    public void test() throws IOException, OvCommonsInfrastructureException, GeneralCryptoLibException {

        Security.addProvider(new BouncyCastleProvider());

        AsymmetricService asymmetricService = new AsymmetricService();
        KeyPair keyPair = asymmetricService.getKeyPairForSigning();
        PrivateKey privateKey = keyPair.getPrivate();

        Assert.assertNotNull(privateKey);

        RestClientConnectionManager.getInstance().getRestClientWithInterceptor("https://localhost:8080", privateKey,
            NodeIdentifier.CONFIG_PLATFORM_ROOT);

    }

    @SuppressWarnings("unused")
    private PrivateKey getPrivateKeyFromKeystore() throws IOException {

        NodeIdentifier thisNode = NodeIdentifier.CONFIG_PLATFORM_ROOT;
        KeystorePasswordsReader keystorePasswordsReader = TestKeystorePasswordsReader.getInstance(thisNode);

        KeystorePasswords thisNodePasswords = keystorePasswordsReader.read();

        ConfigPlatformRootKeyLoader configPlatformRootKeyLoader = ConfigPlatformRootKeyLoader.getInstance();

        try {
            PrivateKey privateKey = configPlatformRootKeyLoader.getPrivateKeyFromKeystore(thisNodePasswords);
            return privateKey;
        } catch (Exception e) {
            String errorMsg = "Error while trying to extract private key from keystore: " + e.getMessage();
            // LOG.error(errorMsg);
            throw new RuntimeException(errorMsg, e);
        }
    }

}
