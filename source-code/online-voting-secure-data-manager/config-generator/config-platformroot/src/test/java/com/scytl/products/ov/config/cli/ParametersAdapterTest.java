/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ParametersAdapterTest {

    private static final String OUTPUT_PATH = "target/foo";

    private static String[] lackingParams = {"-out", OUTPUT_PATH };

    private static String[] invalidParamsNull = {"-out", OUTPUT_PATH, "-name", null };

    private static String[] invalidParamsEmpty = {"-out", OUTPUT_PATH, "-name", "" };

    private static final String DUMMY_PASSWORD = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    private static String[] installPlatformParams = {"-rootca", "SOME_VALID_PATH/platformRootCA.pem", "-loggingkeys",
            "SOME_VALID_PATH/loggingKeys", "-ser", "SOME_VALID_PATH/serviceCertificateParameters.json", "-password", DUMMY_PASSWORD };

    private static String[] installTenantParams = {"-tenantid", "100", "-tenantcert", "SOME_VALID_PATH/tenantCert.pem",
            "-systemkeys", "SOME_VALID_PATH/systemKeys", "-password", DUMMY_PASSWORD };

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void throwAnExceptionWhenLackingParams() {

        exception.expect(IllegalArgumentException.class);
        Action.GEN_ROOT.validateAndArrange(lackingParams);
    }

    @Test
    public void throwAnExceptionWhenInvalidParamsWithNull() {

        exception.expect(IllegalArgumentException.class);
        Action.GEN_ROOT.validateAndArrange(invalidParamsNull);
    }

    @Test
    public void throwAnExceptionWhenInvalidEmptyParams() {

        exception.expect(IllegalArgumentException.class);
        Action.GEN_ROOT.validateAndArrange(invalidParamsEmpty);
    }

    @Test
    public void whenCorrectParametersForPlatformInstallationThenOk() {

        Action.INSTALL_PLATFORM.validateAndArrange(installPlatformParams);
    }

    @Test
    public void whenCorrectParametersForTenantInstallationThenOk() {

        Action.INSTALL_TENANT.validateAndArrange(installTenantParams);
    }
}
