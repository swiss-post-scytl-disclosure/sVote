/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.utils;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.Assert;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.service.AsymmetricService;

public class KeyPairValidator {

	public void validateKeyPair(final PublicKey publicKey, final PrivateKey privateKey) throws GeneralCryptoLibException {

		final AsymmetricServiceAPI asymService = new AsymmetricService();

		final String testString = "word to be tested by foo hahaha bar";
		final byte[] encryptedTestString = asymService.encrypt(publicKey, testString.getBytes(StandardCharsets.UTF_8));
		final byte[] decryptedTestString = asymService.decrypt(privateKey, encryptedTestString);
		final String decrypted = new String(decryptedTestString, StandardCharsets.UTF_8);

		Assert.assertEquals(testString, decrypted);
	}
}
