/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.io;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.scytl.products.ov.datapacks.beans.CertificateParameters.Type;
import com.scytl.products.ov.utils.ConfigObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class ServicesInputTest {

    private static final String JSON = "{\"AU\":[\"ENCRYPTION\",\"SIGN\"],\"EA\":[\"ENCRYPTION\"]}";

    @Test
    public void testDeserialization() throws IOException {
        ConfigObjectMapper com = new ConfigObjectMapper();
        ServicesInput sut = com.fromJSONToJava(JSON, ServicesInput.class);

        assertTrue(sut.getServiceNames().contains("AU"));
        assertTrue(sut.getServiceNames().contains("EA"));

        assertTrue(sut.getServiceCertificateParameters().get("AU").contains(Type.ENCRYPTION));
        assertTrue(sut.getServiceCertificateParameters().get("AU").contains(Type.SIGN));
        assertTrue(sut.getServiceCertificateParameters().get("EA").contains(Type.ENCRYPTION));
        assertFalse(sut.getServiceCertificateParameters().get("EA").contains(Type.SIGN));
    }
}
