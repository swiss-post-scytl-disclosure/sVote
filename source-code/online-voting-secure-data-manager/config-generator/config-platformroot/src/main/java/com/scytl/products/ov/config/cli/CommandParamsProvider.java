/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

import java.util.List;

public class CommandParamsProvider {

	/**
	 *  Non-public constructor
	 */
	private CommandParamsProvider() {
	}
	 
	 public static List<ActionParams> provide(final Action action) {
        return action.getActionParams();
    }
}
