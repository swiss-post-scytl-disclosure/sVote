/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

/**
 * The class {@code ConfigPlatformRootException} and its subclasses are
 * <em>checked</em> exceptions. They indicate that some kind of expected
 * condition had happened and the caller application might want to catch.
 */
public class ConfigPlatformRootException extends Exception{

    private static final long serialVersionUID = -2520664061262060862L;

    public ConfigPlatformRootException(final String message) {
        super(message);
    }
    
    public ConfigPlatformRootException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public ConfigPlatformRootException(final Throwable cause) {
   	 super(cause);
   }
}
