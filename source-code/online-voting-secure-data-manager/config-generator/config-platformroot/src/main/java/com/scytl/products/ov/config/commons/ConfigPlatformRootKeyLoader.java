/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons;

import java.security.PrivateKey;

import com.scytl.products.ov.keystore.KeystorePasswords;
import com.scytl.products.ov.keystore.KeystoreReaderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.infrastructure.remote.client.NodeIdentifier;
import com.scytl.products.ov.config.commons.exception.ConfigPlatformRootException;
import com.scytl.products.ov.keystore.KeystoreReader;

/**
 * @author : Rafael Márquez [rmarquez]
 * @date : 28/11/16 - 12:52
 * Copyright (C) 2016
 * All rights reserved.
 */
public class ConfigPlatformRootKeyLoader {

	private static final Logger LOG = LoggerFactory.getLogger("std");

	private static final NodeIdentifier THIS_NODE = NodeIdentifier.CONFIG_PLATFORM_ROOT;

	private KeystoreReader keystoreReader;

	private ConfigPlatformRootKeyLoader() {
		keystoreReader = new KeystoreReaderFactory().getInstance();
	}

	public static ConfigPlatformRootKeyLoader getInstance() {
		return new ConfigPlatformRootKeyLoader();
	}

	public PrivateKey getPrivateKeyFromKeystore(final String password) throws ConfigPlatformRootException {
		LOG.info("Trying to get Private key from proper keystore to sign requests...");
		return this.keystoreReader.readSigningPrivateKey( THIS_NODE, password );
	}
	
	public PrivateKey getPrivateKeyFromKeystore(final KeystorePasswords passwords) throws ConfigPlatformRootException {
		LOG.info("Trying to get Private key from proper keystore to sign requests...");
		return this.keystoreReader.readSigningPrivateKey( THIS_NODE, passwords );
	}

}
