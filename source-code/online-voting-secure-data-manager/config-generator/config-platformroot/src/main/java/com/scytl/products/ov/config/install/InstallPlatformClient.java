/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.install;

import com.scytl.products.ov.commons.beans.domain.model.platform.PlatformInstallationData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

import okhttp3.ResponseBody;

public interface InstallPlatformClient {

    String HEADER_X_TRACKING_ID = "X-Request-ID";

    @POST(".")
    Call<ResponseBody> postToContext(@Header(HEADER_X_TRACKING_ID) String trackingId, @Body final PlatformInstallationData platformInstallationData);
}
