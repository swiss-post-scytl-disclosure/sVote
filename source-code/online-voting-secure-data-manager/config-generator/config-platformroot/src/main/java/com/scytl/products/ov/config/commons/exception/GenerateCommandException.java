/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class GenerateCommandException extends ConfigPlatformException {

	private static final long serialVersionUID = -7845530821348379663L;

	public GenerateCommandException(final String message) {
        super(message);
    }
    
    public GenerateCommandException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public GenerateCommandException(final Throwable cause) {
   	 super(cause);
   }
}
