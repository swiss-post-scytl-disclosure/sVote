/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.io;

import java.util.List;

/**
 * Encapsulates the list of nodes for which keys and certificates should be generated.
 */
public final class ControlComponentNodes {

    private List<String> nodes;

    /**
     * @return the list of nodes.
     */
    public List<String> getNodes() {
        return nodes;
    }

    /**
     * @param nodes
     *            the list of nodes.
     */
    public void setNodes(final List<String> nodes) {
        this.nodes = nodes;
    }
}
