/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class CommandFactoryException extends ConfigPlatformException {

	private static final long serialVersionUID = 952070105485302476L;

	public CommandFactoryException(final String message) {
        super(message);
    }
    
    public CommandFactoryException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public CommandFactoryException(final Throwable cause) {
   	 super(cause);
   }
}
