/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.infrastructure.config.InfrastructureConfig;
import com.scytl.products.ov.config.commons.exception.ConfigPlatformRootException;

public class PlatformInstallationProperties {

    public static final String propertiesFilename = "platformInstallation.properties";

    private static final Logger LOG = LoggerFactory.getLogger(PlatformInstallationProperties.class);

    private static PlatformInstallationProperties instance = null;

    private static String contextUrl;

    private static String interceptorCertificatesPath;

    private static String interceptorCertificatesPathFromHome;

    private static String interceptorCertificatesPrefix;

    private static String interceptorPrivateKeyAlias;

    public static void PlatformInstalationProperties() {
    }

    public static PlatformInstallationProperties getInstance() throws ConfigPlatformRootException {
        if (instance == null) {
            instance = new PlatformInstallationProperties();
            readProperties();
        }
        return instance;
    }

    private static void readProperties() throws ConfigPlatformRootException {

        Properties properties = new Properties();

        try (InputStream inputStream = InfrastructureConfig.class.getClassLoader().getResourceAsStream(propertiesFilename)) {

            if (inputStream == null) {
                System.out.println("Sorry, unable to find " + propertiesFilename);
                return;
            }

            properties.load(inputStream);

            contextUrl = properties.getProperty("CONTEXT_URL");

            interceptorCertificatesPath = properties.getProperty("interceptor.certificates.path");

            interceptorCertificatesPathFromHome = properties.getProperty("interceptor.certificates.path.from.home");

            interceptorCertificatesPrefix = properties.getProperty("interceptor.certificates.prefix");

            interceptorPrivateKeyAlias = properties.getProperty("interceptor.private.key.alias");

        } catch (IOException ioE) {
            LOG.error(ioE.getMessage());
            throw new ConfigPlatformRootException("Error trying to read properties.", ioE);
        }
    }

    public String getContext_url() {
        return contextUrl;
    }

    public String getInterceptor_certificates_path() {
        return interceptorCertificatesPath;
    }

    public String getInterceptor_certificates_path_from_home() {
        return interceptorCertificatesPathFromHome;
    }

    public String getInterceptor_certificates_prefix() {
        return interceptorCertificatesPrefix;
    }

    public String getInterceptor_private_key_alias() {
        return interceptorPrivateKeyAlias;
    }

}
