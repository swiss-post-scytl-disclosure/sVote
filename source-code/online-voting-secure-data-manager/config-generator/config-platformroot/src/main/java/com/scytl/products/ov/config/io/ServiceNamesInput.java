/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.io;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.List;

public class ServiceNamesInput {
    private final List<String> names;

    @JsonCreator
    public ServiceNamesInput(List<String> names) {
        this.names = names;
    }

    public List<String> getServiceNames() {
        return names;
    }
}
