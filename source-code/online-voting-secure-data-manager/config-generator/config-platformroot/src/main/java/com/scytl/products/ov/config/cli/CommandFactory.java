/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.cryptolib.scytl.keystore.service.ScytlKeyStoreService;
import com.scytl.products.ov.config.commons.exception.CommandFactoryException;

/**
 * Creates commands that can be executed by the Platform Root user.
 */
public class CommandFactory {

    /**
     * Creates a {@code Command} based on the given {@code Action}.
     */
    public Command create(final Action action) throws GeneralCryptoLibException {

        switch (action) {
        case GEN_ROOT:
            return new GenerateCommand();
        case CERT_TENANT:
            return new CertificateCommand();
        case GEN_LOGGING_KEYS:
            return new GenerateLoggingKeysCommand(new PrimitivesService());
        case GEN_SYSTEM_KEYS:
            return new GenerateSystemKeysCommand(new PrimitivesService(),
                    new ScytlKeyStoreService());
        case GEN_CCN_KEYS:
            return new GenerateControlComponentNodeCertsAndKeys(new PrimitivesService());
        case INSTALL_PLATFORM:
            return new InstallPlatformCommand();
        case INSTALL_TENANT:
            return new InstallTenantCommand();
        case INSTALL_TENANT_BY_SERVICE:
            return new InstallTenantPerServiceCommand();
        default:
            throw new CommandFactoryException("The command introduced is not valid.");
        }
    }
}
