/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class GenerateLoggingKeysCommandException extends ConfigPlatformException {

	private static final long serialVersionUID = -7064265099056878914L;

	public GenerateLoggingKeysCommandException(final String message) {
        super(message);
    }
    
    public GenerateLoggingKeysCommandException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public GenerateLoggingKeysCommandException(final Throwable cause) {
   	 super(cause);
   }
}
