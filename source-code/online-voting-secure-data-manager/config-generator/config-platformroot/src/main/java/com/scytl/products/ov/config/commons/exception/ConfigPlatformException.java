/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class ConfigPlatformException extends RuntimeException{

	private static final long serialVersionUID = 1391495464878416778L;

	public ConfigPlatformException(final String message) {
        super(message);
    }
    
    public ConfigPlatformException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public ConfigPlatformException(final Throwable cause) {
   	 super(cause);
   }
}
