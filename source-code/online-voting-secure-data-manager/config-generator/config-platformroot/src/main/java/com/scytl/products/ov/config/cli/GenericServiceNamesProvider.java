/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class GenericServiceNamesProvider implements IServiceNamesProvider<Void> {

    public enum ServiceNameE {

        AUTHENTICATION("authentication"), ELECTION_INFORMATION("electionInformation");

        private final String serviceName;

        ServiceNameE(final String serviceName) {
            this.serviceName = serviceName;
        }

        public String getServiceName() {
            return serviceName;
        }
    }

    @Override
    public Set<String> getServiceNames(final Void params) {
        return Arrays.asList(ServiceNameE.values()).stream().map(ServiceNameE::getServiceName)
                .collect(Collectors.toSet());
    }
}
