/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.io;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.scytl.products.ov.datapacks.beans.CertificateParameters;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ServicesInput {

    private Map<String, Set<CertificateParameters.Type>> serviceCertificateParameters;

    @JsonCreator
    public ServicesInput(Map<String, Set<CertificateParameters.Type>> serviceCertificateParameters) {
        this.serviceCertificateParameters = serviceCertificateParameters;
    }

    public Map<String, Set<CertificateParameters.Type>> getServiceCertificateParameters() {
        return serviceCertificateParameters;
    }

    /**
     * Gets serviceNames.
     *
     * @return Value of serviceNames.
     */
    public Set<String> getServiceNames() {
        return serviceCertificateParameters.keySet();
    }
}
