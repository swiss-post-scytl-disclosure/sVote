/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class CertificateCommandException extends ConfigPlatformException {

	private static final long serialVersionUID = 730152086340252559L;

	public CertificateCommandException(final String message) {
        super(message);
    }
    
    public CertificateCommandException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public CertificateCommandException(final Throwable cause) {
   	 super(cause);
   }
}
