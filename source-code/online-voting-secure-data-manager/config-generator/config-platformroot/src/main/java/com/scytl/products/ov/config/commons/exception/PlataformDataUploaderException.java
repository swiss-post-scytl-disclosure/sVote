/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class PlataformDataUploaderException extends ConfigPlatformException {

	private static final long serialVersionUID = -7890379004551324084L;

	public PlataformDataUploaderException(final String message) {
        super(message);
    }
    
    public PlataformDataUploaderException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public PlataformDataUploaderException(final Throwable cause) {
   	 super(cause);
   }
}
