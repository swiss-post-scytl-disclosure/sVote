/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class InstallPlatformCommandException extends ConfigPlatformException {

	private static final long serialVersionUID = -5724349179957091099L;

	public InstallPlatformCommandException(final String message) {
        super(message);
    }
    
    public InstallPlatformCommandException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public InstallPlatformCommandException(final Throwable cause) {
   	 super(cause);
   }
}
