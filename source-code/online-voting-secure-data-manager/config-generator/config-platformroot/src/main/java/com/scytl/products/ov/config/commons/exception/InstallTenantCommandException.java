/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class InstallTenantCommandException extends ConfigPlatformException {

	private static final long serialVersionUID = -7386041529966383152L;

	public InstallTenantCommandException(final String message) {
        super(message);
    }
    
    public InstallTenantCommandException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public InstallTenantCommandException(final Throwable cause) {
   	 super(cause);
   }
}
