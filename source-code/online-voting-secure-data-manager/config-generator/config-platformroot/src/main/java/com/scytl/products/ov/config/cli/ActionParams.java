/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

public enum ActionParams {

    PLATFORM_NAME("-name", false),

    OUTPUT_FOLDER("-out", false),

    KEYSTORE_PATH("-ks", false),

    CSR_PATH("-csr", false),

    SERVICES_NAMES_PATH("-ser", false),

    TENANT_NAME("-tenant", false),

    PLATFORM_ROOT_CA_CERT_PATH("-rootca", false),

    PLATFORM_ROOT_CA_ISSUER_CERT_PATH("-rootcaissuer", true),

    LOGGING_KEYSTORES_FOLDER("-loggingkeys", false),

    TENANT_CERT_PATH("-tenantcert", false),

    SYSTEM_KEYSTORES_FOLDER("-systemkeys", false),

    TENANT_ID("-tenantid", false),

    PASSWORD("-password", false),

    CONTROL_COMPONENTS_NODES_PATH("-nodes", false);

    private final String name;

    private final boolean optional;

    ActionParams(final String name, final boolean optional) {
        this.name = name;
        this.optional = optional;
    }
    
    public String getName() {
        return name;
    }

    public boolean isOptional() {
        return optional;
    }

}
