/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.install;

import com.scytl.products.ov.commons.beans.domain.model.tenant.TenantInstallationData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;


import okhttp3.ResponseBody;

public interface InstallTenantClient {

    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    String HEADER_X_TRACKING_ID = "X-Request-ID";

    @POST("tenantdata/tenant/{tenantId}")
    Call<ResponseBody> postToContext(@Header(HEADER_X_TRACKING_ID) String trackingId,  @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
                         @Body final TenantInstallationData tenantInstallationData);

    @POST("tenantdata/secured/tenant/{tenantId}")
    Call<ResponseBody> postToCrContext(@Header(HEADER_X_TRACKING_ID) String trackingId,  @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
                         @Body final TenantInstallationData tenantInstallationData);

}
