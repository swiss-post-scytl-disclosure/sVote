/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class GenerateControlComponentNodeCertsAndKeysException extends ConfigPlatformException {

	private static final long serialVersionUID = 8437899453138116436L;

	public GenerateControlComponentNodeCertsAndKeysException(final String message) {
        super(message);
    }
    
    public GenerateControlComponentNodeCertsAndKeysException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public GenerateControlComponentNodeCertsAndKeysException(final Throwable cause) {
   	 super(cause);
   }
}
