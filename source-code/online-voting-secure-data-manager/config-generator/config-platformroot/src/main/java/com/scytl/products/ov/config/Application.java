/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.config.cli.Action;
import com.scytl.products.ov.config.cli.Command;
import com.scytl.products.ov.config.cli.CommandFactory;

public class Application {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    public static void main(final String... args) {

    	if (args == null || args.length == 0) {
    		printHelp();
    		throw new IllegalArgumentException("It is mandatory set an action.");
    	}
    	
    	String inputAction = args[0];
    	
    	try {        
        	Action action = getAction(inputAction);
            Map<String, String> params2Value = action.validateAndArrange(Arrays.copyOfRange(args, 1, args.length));
            Command command = getCommand(action);
            command.execute(params2Value);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            
            // Let the OS know the program finished abruptly.
            System.exit(-1);
        }
    }

    private static Command getCommand(final Action action) throws GeneralCryptoLibException {
        CommandFactory commandFactory = new CommandFactory();
        return commandFactory.create(action);
    }

    private static Action getAction(final String inputAction) {
		
		if (inputAction.equals(Action.GEN_ROOT.getActionName())) {
			return Action.GEN_ROOT;
		}

		if (inputAction.equals(Action.CERT_TENANT.getActionName())) {
			return Action.CERT_TENANT;
		}

		if (inputAction.equals(Action.GEN_LOGGING_KEYS.getActionName())) {
			return Action.GEN_LOGGING_KEYS;
		}

		if (inputAction.equals(Action.GEN_SYSTEM_KEYS.getActionName())) {
			return Action.GEN_SYSTEM_KEYS;
		}

		if (inputAction.equals(Action.GEN_CCN_KEYS.getActionName())) {
			return Action.GEN_CCN_KEYS;
		}

		if (inputAction.equals(Action.INSTALL_PLATFORM.getActionName())) {
			return Action.INSTALL_PLATFORM;
		}

		if (inputAction.equals(Action.INSTALL_TENANT.getActionName())) {
			return Action.INSTALL_TENANT;
		}

		if (inputAction.equals(Action.INSTALL_TENANT_BY_SERVICE.getActionName())) {
			return Action.INSTALL_TENANT_BY_SERVICE;
		}

		LOG.info(inputAction);
		LOG.info(Action.INSTALL_TENANT_BY_SERVICE.getActionName());
		LOG.info(Boolean.toString(Action.INSTALL_TENANT_BY_SERVICE.getActionName().equals(inputAction)));
		throw new IllegalArgumentException("The action \"" + inputAction + "\" is not correct");
    }

    private static void printHelp() {

        try {
            final Enumeration<URL> systemResources = ClassLoader.getSystemResources("config-platformroot.help.txt");
            while (systemResources.hasMoreElements()) {
                System.out.println("URL = " + systemResources.nextElement());
            }
        } catch (IOException e) {
        	LOG.warn("Error trying to get a System Resources.", e);
        }
        try (InputStream is =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("config-platformroot.help.txt");
                PrintWriter pw = new PrintWriter(System.out)) {
            IOUtils.copy(is, pw);
        } catch (final IOException e) {
            LOG.warn("A error occurred when trying to print the help.", e);
        }
    }
}
