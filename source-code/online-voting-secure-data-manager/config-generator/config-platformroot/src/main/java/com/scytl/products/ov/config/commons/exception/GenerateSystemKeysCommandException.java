/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commons.exception;

public class GenerateSystemKeysCommandException extends ConfigPlatformException {

	private static final long serialVersionUID = -2172134869385078201L;

	public GenerateSystemKeysCommandException(final String message) {
        super(message);
    }
    
    public GenerateSystemKeysCommandException(final String message, final Throwable cause) {
    	 super(message, cause);
    }
    
    public GenerateSystemKeysCommandException(final Throwable cause) {
   	 super(cause);
   }
}
