/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.cli;

import com.scytl.products.ov.config.io.ServicesInput;
import java.util.Set;

public class JsonServiceNamesProvider implements IServiceNamesProvider<ServicesInput> {

    @Override
    public Set<String> getServiceNames(final ServicesInput input) {
        return input.getServiceNames();
    }
}
