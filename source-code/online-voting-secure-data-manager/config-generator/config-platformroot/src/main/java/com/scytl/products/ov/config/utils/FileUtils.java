/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.codec.binary.Base64;

/**
 * Provides utilities to deal with file-related operations.
 */
public class FileUtils {
	
	/**
     * Non-public constructor
     */
	private FileUtils() {
    }
	
    /**
     * Creates an output directory.
     */
    public static void createOutputDirectory(final Path outputPath) {
        try {
            Files.createDirectories(outputPath);
        } catch (IOException e) {
            throw new IllegalArgumentException("The given output path is not valid", e);
        }
    }

    /**
     * Get the bytes of a file, encoded in Base64.
     *
     * @param pathOfFile
     *            the full path of the file (including filename).
     * @return the bytes of the file, encoded in Base64.
     * @throws IOException
     */
    public static String getFileBytesAsBase64(final Path pathOfFile) throws IOException {

        byte[] keyStoreBytes = Files.readAllBytes(pathOfFile);

        return new String(Base64.encodeBase64(keyStoreBytes), StandardCharsets.UTF_8);
    }
}
