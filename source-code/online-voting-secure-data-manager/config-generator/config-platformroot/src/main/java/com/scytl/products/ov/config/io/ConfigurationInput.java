/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.io;

import com.scytl.products.ov.datapacks.beans.CredentialProperties;

public class ConfigurationInput {

    private CredentialProperties platformRootCA;

    private CredentialProperties tenantCA;

    private CredentialProperties loggingServicesSignerCert;

    private CredentialProperties loggingServicesEncryptionCert;

    private CredentialProperties systemServicesEncryptionCert;

    private CredentialProperties systemServicesSigningCert;

    private CredentialProperties controlComponentNodeCA;

    private CredentialProperties controlComponentNodeSystemEncryption;

    /**
     * Gets platformRootCA.
     *
     * @return Value of platformRootCA.
     */
    public CredentialProperties getPlatformRootCA() {
        return platformRootCA;
    }

    /**
     * Sets new platformRootCA.
     *
     * @param platformRootCA
     *            New value of platformRootCA.
     */
    public void setPlatformRootCA(CredentialProperties platformRootCA) {
        this.platformRootCA = platformRootCA;
    }

    /**
     * Gets tenantCA.
     *
     * @return Value of tenantCA.
     */
    public CredentialProperties getTenantCA() {
        return tenantCA;
    }

    /**
     * Sets new tenantCA.
     *
     * @param tenantCA
     *            New value of tenantCA.
     */
    public void setTenantCA(CredentialProperties tenantCA) {
        this.tenantCA = tenantCA;
    }

    /**
     * Gets systemServicesEncryptionCA.
     *
     * @return Value of systemServicesEncryptionCA.
     */
    public CredentialProperties getSystemServicesEncryptionCert() {
        return systemServicesEncryptionCert;
    }

    /**
     * Sets new systemServicesEncryptionCA.
     *
     * @param systemServicesEncryptionCert
     *            New value of systemServicesEncryptionCA.
     */
    public void setSystemServicesEncryptionCert(CredentialProperties systemServicesEncryptionCert) {
        this.systemServicesEncryptionCert = systemServicesEncryptionCert;
    }

    /**
     * Gets the credential properties for service certificates for signing.
     *
     * @return the credential properties for service certificates for signing.
     */
    public CredentialProperties getSystemServicesSigningCert() {
        return systemServicesSigningCert;
    }

    /**
     * Sets the credential properties for service certificates for signing.
     *
     * @param systemServicesSigningCert
     *            the credential properties for service certificates for signing.
     */
    public void setSystemServicesSigningCert(CredentialProperties systemServicesSigningCert) {
        this.systemServicesSigningCert = systemServicesSigningCert;
    }

    /**
     * Gets loggingServicesEncryptionCert.
     *
     * @return Value of loggingServicesEncryptionCert.
     */
    public CredentialProperties getLoggingServicesEncryptionCert() {
        return loggingServicesEncryptionCert;
    }

    /**
     * Sets new loggingServicesSignerCert.
     *
     * @param loggingServicesSignerCert
     *            New value of loggingServicesSignerCert.
     */
    public void setLoggingServicesSignerCert(CredentialProperties loggingServicesSignerCert) {
        this.loggingServicesSignerCert = loggingServicesSignerCert;
    }

    /**
     * Gets loggingServicesSignerCert.
     *
     * @return Value of loggingServicesSignerCert.
     */
    public CredentialProperties getLoggingServicesSignerCert() {
        return loggingServicesSignerCert;
    }

    /**
     * Sets new loggingServicesEncryptionCert.
     *
     * @param loggingServicesEncryptionCert
     *            New value of loggingServicesEncryptionCert.
     */
    public void setLoggingServicesEncryptionCert(CredentialProperties loggingServicesEncryptionCert) {
        this.loggingServicesEncryptionCert = loggingServicesEncryptionCert;
    }

    /**
     * Gets controlComponentNodeCA.
     * 
     * @return controlComponentNodeCA.
     */
    public CredentialProperties getControlComponentNodeCA() {
        return controlComponentNodeCA;
    }

    /**
     * Sets new controlComponentNodeCA.
     * 
     * @param controlComponentNodeCA
     */
    public void setControlComponentNodeCA(CredentialProperties controlComponentNodeCA) {
        this.controlComponentNodeCA = controlComponentNodeCA;
    }

    /**
     * Gets controlComponentNodeSystemEncryption.
     * 
     * @return controlComponentNodeSystemEncryption
     */
    public CredentialProperties getControlComponentNodeSystemEncryption() {
        return controlComponentNodeSystemEncryption;
    }

    /**
     * Sets new controlComponentNodeSystemEncryption.
     * 
     * @param controlComponentNodeSystemEncryption
     */
    public void setControlComponentNodeSystemEncryption(CredentialProperties controlComponentNodeSystemEncryption) {
        this.controlComponentNodeSystemEncryption = controlComponentNodeSystemEncryption;
    }
}
