/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;
import java.util.UUID;

import com.scytl.products.ov.config.commands.progress.ProgressManager;
import com.scytl.products.ov.config.commands.progress.ProgressManagerImpl;
import com.scytl.products.ov.config.commons.progress.JobProgressDetails;

@RunWith(MockitoJUnitRunner.class)
public class ProgressManagerTest {

    @InjectMocks
    ProgressManager sut = new ProgressManagerImpl();

    @Test
    public void returnEmptyIfJobNotRegistered() {

        final Optional<JobProgressDetails> progress = sut.getJobProgress(UUID.randomUUID());
        Assert.assertFalse(progress.isPresent());
    }

    @Test
    public void returnProgressIfJobRegistered() {

        UUID jobId = UUID.randomUUID();
        sut.registerJob(jobId, JobProgressDetails.EMPTY);

        final Optional<JobProgressDetails> progress = sut.getJobProgress(jobId);

        Assert.assertTrue(progress.isPresent());
    }

    @Test
    public void removeEmptyAfterUnregisterJob() {

        UUID jobId = UUID.randomUUID();
        sut.registerJob(jobId, JobProgressDetails.EMPTY);
        final Optional<JobProgressDetails> progress = sut.getJobProgress(jobId);
        Assert.assertTrue(progress.isPresent());

        sut.unregisterJob(jobId);

        final Optional<JobProgressDetails> progress2 = sut.getJobProgress(jobId);
        Assert.assertFalse(progress2.isPresent());


    }

    @Test
    public void returnUpdatedWorkCompletedAfterUpdateJob() {

        UUID jobId = UUID.randomUUID();
        long totalWorkAmount = 1;
        JobProgressDetails progressDetails = new JobProgressDetails(jobId, totalWorkAmount);
        sut.registerJob(jobId, progressDetails);
        final Optional<JobProgressDetails> progress = sut.getJobProgress(jobId);
        Assert.assertTrue(progress.isPresent());
        Assert.assertEquals(totalWorkAmount, (long)progress.get().getTotalWorkAmount());

        long workCompleted = 1;
        sut.updateProgress(jobId, workCompleted);

        final Optional<JobProgressDetails> updatedProgress = sut.getJobProgress(jobId);
        Assert.assertTrue(updatedProgress.isPresent());
        Assert.assertEquals(0, (long)progress.get().getRemainingWork());


    }
}
