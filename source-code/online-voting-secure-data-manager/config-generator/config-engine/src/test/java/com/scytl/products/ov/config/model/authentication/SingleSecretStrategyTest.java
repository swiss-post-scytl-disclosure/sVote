/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.scytl.products.ov.config.model.authentication.service.SingleSecretAuthenticationKeyGenerator;
import com.scytl.products.ov.constants.Constants;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SingleSecretStrategyTest {

    StartVotingKey ignored = null;
    Set<String> randomStrings = new HashSet<>();

    private AuthenticationKeyGenerator singleSecretStrategy = new SingleSecretAuthenticationKeyGenerator();

    @Test
    public void generateNotNull() {

        AuthenticationKey authenticationKey = singleSecretStrategy.generateAuthKey(ignored);
        assertNotNull(authenticationKey);

    }

    @Test
    public void generatesRandomString() {
        //why 1k? not reason
        for (int i = 0; i < 1000; i++) {
            AuthenticationKey authenticationKey = singleSecretStrategy.generateAuthKey(ignored);
            String authKeyValue = authenticationKey.getValue();
            boolean added = randomStrings.add(authKeyValue);
            //make sure each time is a different string
            Assert.assertTrue(added);
        }
    }

    @Test
    public void havePresentSecret() {

        AuthenticationKey authenticationKey = singleSecretStrategy.generateAuthKey(ignored);
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        assertTrue(optionalSecrets.isPresent());
    }

    @Test
    public void haveSingleSecret() {

        AuthenticationKey authenticationKey = singleSecretStrategy.generateAuthKey(ignored);
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        List<String> secrets = optionalSecrets.get();
        assertEquals(1, secrets.size());
    }

    @Test
    public void generatesRandomStringWithCorrectSize() {
        AuthenticationKey authenticationKey = singleSecretStrategy.generateAuthKey(ignored);
        String authKeyValue = authenticationKey.getValue();
        Assert.assertEquals(Constants.SVK_LENGTH, authKeyValue.length());

    }
}
