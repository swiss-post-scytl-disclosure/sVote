/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.scytl.products.ov.config.model.authentication.service.StartVotingKeyService;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.config.SpringConfigTest;
import com.scytl.products.ov.config.model.authentication.service.AuthenticationKeyCryptoService;
import com.scytl.products.ov.constants.Constants;

import java.security.Security;

/**
 * Test class for the StartVotingKeyService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class StartVotingKeyServiceLongSecretsTest {

    public static final String ELECTION_EVENT_ID = "cfdee3c861e944a4bf505bd86e9c4b73";
    public static final int TOO_LONG_RANDOM = 200;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    @Qualifier("LONGSECRETS")
    StartVotingKeyService startVotingKeyService;

    @Autowired
    @Qualifier("BEYONDLIMIT")
    StartVotingKeyService startVotingKeyServiceBeyondLimit;

    @Autowired
    AuthenticationKeyCryptoService authenticationKeyCryptoService;


    @BeforeClass
    public static void setUp(){

        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void tryToDeriveBeyondLimit() throws GeneralCryptoLibException {


        expectedException.expect(GeneralCryptoLibException.class);
        startVotingKeyServiceBeyondLimit.generateStartVotingKey();

    }

    @Test
    public void tryToDeriveUpToLimit() throws GeneralCryptoLibException {
        AuthenticationKeyGenerator mock = Mockito.mock(AuthenticationKeyGenerator.class);


        String svk = startVotingKeyService.generateStartVotingKey();

        AuthenticationDerivedElement credentialID =
                authenticationKeyCryptoService.deriveElement(Constants.CREDENTIAL_ID, ELECTION_EVENT_ID, svk);
        AuthenticationDerivedElement pin =
                authenticationKeyCryptoService.deriveElement(Constants.KEYSTORE_PIN, ELECTION_EVENT_ID, svk);

        assertThat(credentialID.getDerivedKeyInEx(), notNullValue());
        assertThat(pin.getDerivedKeyInEx(), notNullValue());
    }

}
