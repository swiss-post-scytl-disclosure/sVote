/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.Collections;

import com.scytl.products.ov.config.model.authentication.service.ProvidedChallengeGenerator;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ProvidedChallengeGeneratorTest {

    private ChallengeGenerator challengeGenerator =
        new ProvidedChallengeGenerator(
            () -> new ProvidedChallenges(RandomStringUtils.random(10), Collections.emptyList()));

    @Test
    public void generateNonNullParams() {
        final ExtraParams extraParams = challengeGenerator.generateExtraParams();
        assertNotNull(extraParams);
    }

    @Test
    public void generatesNonNullValue() {
        final ExtraParams extraParams = challengeGenerator.generateExtraParams();
        assertTrue(extraParams.getValue().isPresent());
    }


    @Test
    public void generatesNonNullAlias() {
        final ExtraParams extraParams = challengeGenerator.generateExtraParams();
        assertTrue(extraParams.getAlias().isPresent());
    }


}
