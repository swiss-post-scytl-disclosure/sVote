/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


/**
 * Auth generator for testing purposes. Uses the maximum length greater than the allowed by
 * the secure random String class
 */
public class LongSecretsAuthKeyGenerator implements AuthenticationKeyGenerator {


    public static final int SECRETS_LENGTH = 100;

    @Override
    public AuthenticationKey generateAuthKey(StartVotingKey startVotingKey) {
        final Optional<List<String>> secrets = Optional.of(Arrays.asList(startVotingKey.getValue()));

        return AuthenticationKey.ofSecrets(startVotingKey.getValue(), secrets);
    }

    @Override
    public int getSecretsLength() {
        return SECRETS_LENGTH;
    }
}
