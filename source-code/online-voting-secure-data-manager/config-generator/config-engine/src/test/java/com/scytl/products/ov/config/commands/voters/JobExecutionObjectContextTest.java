/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters;

import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class JobExecutionObjectContextTest {

    JobExecutionObjectContext sut = new JobExecutionObjectContext();

    @Test
    public void returnSameObjectAsStored() {
        String id = UUID.randomUUID().toString();
        String expectedValue = "someValue";

        sut.put(id, expectedValue, String.class);

        final String storedValue = sut.get(id, String.class);

        Assert.assertEquals(expectedValue.getClass(), storedValue.getClass());
        Assert.assertEquals(expectedValue, storedValue);
    }

    @Test
    public void returnNullRemoveAllOfId() {
        String id = UUID.randomUUID().toString();
        String expectedValue = "someValue";
        sut.put(id, expectedValue, String.class);

        sut.removeAll(id);

        final String shouldBeNull = sut.get(id, String.class);
        Assert.assertNull(shouldBeNull);
    }

    @Test
    public void keepValueForIdWhenRemoveAllOfOtherId() {
        String id = UUID.randomUUID().toString();
        String expectedValue = "someValue";
        sut.put(id, expectedValue, String.class);

        String id2 = UUID.randomUUID().toString();
        String expectedValue2 = "someValue2";
        sut.put(id2, expectedValue2, String.class);

        sut.removeAll(id);

        final String shouldBeNull = sut.get(id, String.class);
        final String shouldNotBeNull = sut.get(id2, String.class);
        Assert.assertNull(shouldBeNull);
        Assert.assertNotNull(shouldNotBeNull);
    }
}
