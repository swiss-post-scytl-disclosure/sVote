/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Test;

import com.scytl.products.ov.config.model.authentication.service.SimpleAuthenticationKeyGenerator;

public class SimpleStrategyTest {

    private SimpleAuthenticationKeyGenerator simpleStrategy = new SimpleAuthenticationKeyGenerator();

    private StartVotingKey startVotingKey = StartVotingKey.ofValue("68n7vr7znmrdmq2hkpj7");

    @Test
    public void generateNotNull() {

        AuthenticationKey authenticationKey = simpleStrategy.generateAuthKey(startVotingKey);
        assertNotNull(authenticationKey);

    }

    @Test
    public void haveValueEqualToSVK() {

        AuthenticationKey authenticationKey = simpleStrategy.generateAuthKey(startVotingKey);
        String authKeyValue = authenticationKey.getValue();
        assertEquals(startVotingKey.getValue(), authKeyValue);

    }

    @Test
    public void havePresentSecret() {

        AuthenticationKey authenticationKey = simpleStrategy.generateAuthKey(startVotingKey);
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        assertTrue(optionalSecrets.isPresent());
    }

    @Test
    public void haveSingleSecret() {

        AuthenticationKey authenticationKey = simpleStrategy.generateAuthKey(startVotingKey);
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        List<String> secrets = optionalSecrets.get();
        assertEquals(1, secrets.size());

    }

    @Test
    public void haveSecretEqualToSVK() {

        AuthenticationKey authenticationKey = simpleStrategy.generateAuthKey(startVotingKey);
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        List<String> secrets = optionalSecrets.get();
        assertEquals(startVotingKey.getValue(), secrets.get(0));

    }

}
