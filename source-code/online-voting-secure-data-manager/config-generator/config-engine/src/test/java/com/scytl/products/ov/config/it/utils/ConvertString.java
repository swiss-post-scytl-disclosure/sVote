/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.it.utils;

import org.apache.commons.codec.binary.Base64;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
class ConvertString {

	public static byte[] base64StringToByte(final String incoming) {
		return Base64.decodeBase64(incoming);
	}

	public static void convertB64StringToRawBytes(final String[] args)
			throws IOException {

		List<String> argsList = Arrays.asList(args);
		if (argsList.indexOf("-filename") != -1) {
			int indexPwd = argsList.indexOf("-filename");
			try (BufferedReader br = new BufferedReader(new FileReader(
					argsList.get(indexPwd + 1)))) {
				String line;
				while ((line = br.readLine()) != null) {
					// process the line.
					String[] lineSplit;
					lineSplit = line.split(";");
					for (String lineChunk : lineSplit) {
						for (byte b : Base64.decodeBase64(lineChunk)) {
							System.out.write(b);
						}
					}
				}
			}
		}
	}

	public static void convertHexStringNewLineToRawBytes(final String[] args)
			throws IOException {

		List<String> argsList = Arrays.asList(args);
		if (argsList.indexOf("-filename") != -1) {
			int indexPwd = argsList.indexOf("-filename");
			try (BufferedReader br = new BufferedReader(new FileReader(
					argsList.get(indexPwd + 1)))) {
				String line;
				while ((line = br.readLine()) != null) {
					// process the line.
					for (byte b : hexStringToByteArray(line)) {
						System.out.write(b);
					}
				}
			}
		}
	}

	public static String readFile(final String path, final Charset encoding)
			throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static void displayB64StringLength(final String[] args) {
		List<String> argsList = Arrays.asList(args);
		String str = null;
		if (argsList.indexOf("-B64string") != -1) {
			int indexB64string = argsList.indexOf("-B64string");
			str = argsList.get(indexB64string + 1);
		}
		System.out.println("String length in bytes: "
				+ String.valueOf(ConvertString.base64StringToByte(str).length));
	}

	public static byte[] plainStringToBase64EncodedBytes(final String incoming) {
		byte[] bytesEncoded = Base64.encodeBase64(incoming.getBytes());
		return bytesEncoded;
	}

	public static void stringToFile(final String fileName,
			final String textToWrite) throws IOException {
		FileWriter fw = new FileWriter(fileName);
		// create a new writer
		StringWriter sw = new StringWriter();
		// write strings
		sw.write(textToWrite);
		fw.write(sw.toString());
		fw.close();
	}

	public static String decodeBase64String(final String b64) {
		return Base64.decodeBase64(b64).toString();

	}

	public static byte[] hexStringToByteArray(final String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public static String convertRelativePathToAbsolute(final String relativePath) {
		File file = new File(relativePath);
		String absolutePath = file.getAbsoluteFile().toString();
		return absolutePath;

	}

}
