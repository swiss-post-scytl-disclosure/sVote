/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent.datapacks.generators;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;
import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.cryptolib.elgamal.bean.ElGamalKeyPair;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.elgamal.service.ElGamalService;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.cryptolib.scytl.keystore.cryptoapi.CryptoAPIScytlKeyStore;
import com.scytl.cryptolib.scytl.keystore.service.ScytlKeyStoreService;
import com.scytl.products.ov.config.commands.electionevent.datapacks.beans.ElGamalCredentialDataPack;
import com.scytl.products.ov.constants.Constants;
import com.scytl.products.ov.datapacks.beans.CredentialProperties;

/**
 * Tests of {@link ElGamalCredentialDataPackGenerator}.
 */
public class ElGamalCredentialDataPackGeneratorTest {
    private ElGamalCredentialDataPackGenerator generator;

    @BeforeClass
    public static void beforeClass() {
        if (Security
            .getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Before
    public void setUp() throws GeneralCryptoLibException {
        ElGamalServiceAPI elGamalService = new ElGamalService();
        ScytlKeyStoreServiceAPI storeService = new ScytlKeyStoreService();
        PrimitivesServiceAPI primitivesService = new PrimitivesService();
        generator = new ElGamalCredentialDataPackGenerator(elGamalService,
            storeService, primitivesService);
    }

    @Test
    public void testGenerate() throws GeneralCryptoLibException {
        CredentialProperties properties = new CredentialProperties();
        properties.setName("test");
        properties.setAlias(singletonMap("privateKey", "alias"));
        ElGamalEncryptionParameters parameters =
            new ElGamalEncryptionParameters(BigInteger.valueOf(23),
                BigInteger.valueOf(11), BigInteger.valueOf(2));
        int length = 3;

        ElGamalCredentialDataPack pack =
            generator.generate(properties, parameters, length);

        ElGamalKeyPair pair = pack.getKeyPair();
        ElGamalPrivateKey privateKey = pair.getPrivateKeys();
        assertEquals(length, privateKey.getKeys().size());
        assertEquals(parameters.getGroup(), privateKey.getGroup());

        ElGamalPublicKey publicKey = pair.getPublicKeys();
        assertEquals(length, publicKey.getKeys().size());
        assertEquals(parameters.getGroup(), publicKey.getGroup());

        CryptoAPIScytlKeyStore store = pack.getKeyStore();
        assertEquals(1, store.getElGamalPrivateKeyAliases().size());
        assertEquals(privateKey,
            store.getElGamalPrivateKeyEntry("alias", pack.getPassword()));

        assertEquals(Constants.KEYSTORE_PW_LENGTH,
            pack.getPassword().length);
    }
}
