/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.api;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.config.commands.uuid.UUIDGenerator;
import com.scytl.products.ov.config.exceptions.specific.VerificationCardIdsGenerationException;
import com.scytl.products.ov.config.spring.batch.CommonBatchInfrastructure;

public class ConfigurationServiceTest {

    private static final String VERIFICATION_CARD_SET_ID = "0d4c64d0187341c28aa9065d86fd0069";

    private static final String ELECTION_EVENT_ID = "34523tdfvasdfasfdadfasfdasdfa";

    @Mock
    TransactionInfoProvider transactionInfoProvider;

    @Mock
    private CommonBatchInfrastructure commonBatchInfrastructureMock;

    @Mock
    private LoggingWriter loggingWriterMock;

    @Mock
    private UUIDGenerator uuidGeneratorMock;

    @InjectMocks
    private final ConfigurationService configurationService = new ConfigurationService();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateZeroVerificationCardIds() throws Exception {
        createStreamAndTest(0);
    }

    @Test
    public void testCreateOneVerificationCardId() throws Exception {
        createStreamAndTest(1);
    }

    @Test
    public void testCreateManyVerificationCardIds() throws Exception {
        createStreamAndTest(10_000);
    }

    private void createStreamAndTest(int numItems) throws VerificationCardIdsGenerationException {
        Stream<String> verificationCardIds =
            configurationService.createVerificationCardIdStream(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, numItems);

        final AtomicInteger count = new AtomicInteger();
        verificationCardIds.forEach(vcid -> count.incrementAndGet());
        assertEquals(numItems, count.get());
    }
}
