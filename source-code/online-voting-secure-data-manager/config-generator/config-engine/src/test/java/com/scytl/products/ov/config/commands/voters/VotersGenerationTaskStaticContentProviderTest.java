/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.scytl.products.ov.commons.beans.Ballot;
import com.scytl.products.ov.commons.beans.Contest;
import com.scytl.products.ov.commons.beans.ElectionAttributes;
import com.scytl.products.ov.commons.beans.ElectionOption;
import com.scytl.products.ov.commons.beans.EncryptionParameters;
import com.scytl.products.ov.commons.beans.Question;

public class VotersGenerationTaskStaticContentProviderTest {

    @Test
    public void correctnessAttributesTest() throws IOException {
        EncryptionParameters encryptionParameters = new EncryptionParameters();
        encryptionParameters.setG("2");
        encryptionParameters.setP("23");
        encryptionParameters.setQ("11");
        List<Contest> contests = new ArrayList<>();
        List<ElectionOption> options = new ArrayList<>();
        options.add(new ElectionOption("", "15", "non-corr1"));
        options.add(new ElectionOption("", "17", "non-corr2"));
        options.add(new ElectionOption("", "19", "non-corr3"));
        List<ElectionAttributes> attributes = new ArrayList<>();
        List<String> related = new ArrayList<>();
        related.add("corr");
        List<String> related2 = new ArrayList<>();
        related2.add("corr2");
        List<String> related3 = new ArrayList<>();
        related3.add("corr");
        related3.add("corr2");
        attributes.add(new ElectionAttributes("non-corr1", "al", related, false));
        attributes.add(new ElectionAttributes("non-corr2", "al", related2, false));
        attributes.add(new ElectionAttributes("non-corr3", "al", related3, false));
        attributes.add(new ElectionAttributes("corr", "al", new ArrayList<>(), true));
        attributes.add(new ElectionAttributes("corr2", "al", new ArrayList<>(), true));
        List<Question> questions = new ArrayList<>();
        contests.add(new Contest("", "", "", "", "", "", options, attributes, questions, "", ""));
        Ballot ballot = new Ballot("", null, contests);
        List<String> choiceCodesKey = Arrays.asList("choiceCodesKey");
        VotersParametersHolder holder =
            new VotersParametersHolder(1, "", ballot, "", "", "", "", Files.createTempFile("", "test"), 1, 1, "",
                ZonedDateTime.now(), ZonedDateTime.now(), "", "", choiceCodesKey, "", "", null);

        VotersGenerationTaskStaticContentProvider provider =
            new VotersGenerationTaskStaticContentProvider(encryptionParameters, holder);
        Map<BigInteger, List<String>> representationsWithCorrectness = provider.getRepresentationsWithCorrectness();

        Assert.assertEquals(1, representationsWithCorrectness.get(BigInteger.valueOf(15)).size());
        Assert.assertEquals("corr", representationsWithCorrectness.get(BigInteger.valueOf(15)).get(0));
        Assert.assertEquals(1, representationsWithCorrectness.get(BigInteger.valueOf(17)).size());
        Assert.assertEquals("corr2", representationsWithCorrectness.get(BigInteger.valueOf(17)).get(0));
        Assert.assertEquals(2, representationsWithCorrectness.get(BigInteger.valueOf(19)).size());
        Assert.assertTrue(representationsWithCorrectness.get(BigInteger.valueOf(19)).contains("corr2"));
        Assert.assertTrue(representationsWithCorrectness.get(BigInteger.valueOf(19)).contains("corr"));
    }

}
