/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent.readers;

import com.scytl.products.ov.constants.Constants;
import com.scytl.products.ov.datapacks.beans.ConfigurationInput;
import com.scytl.products.ov.readers.ConfigurationInputReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 *
 */
public class ConfigurationInputReaderTest {

    private final ConfigurationInputReader target = new ConfigurationInputReader();

    @Test
    public void read_config_from_file() throws IOException {
        File src = new File("./src/test/resources", Constants.KEYS_CONFIG_FILENAME);
        ConfigurationInput configurationInput = target.fromFileToJava(src);
        Assert.assertEquals(4, configurationInput.getConfigProperties().values().size());
    }
}
