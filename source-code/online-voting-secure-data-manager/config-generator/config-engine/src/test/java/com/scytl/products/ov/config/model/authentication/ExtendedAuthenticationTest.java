/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import static org.junit.Assert.assertNotNull;

import java.security.Security;
import java.util.Optional;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.scytl.products.ov.config.SpringConfigTest;
import com.scytl.products.ov.config.actions.ExtendedAuthenticationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class ExtendedAuthenticationTest {

    public static final int EXPECTED_SECRETS = 2;

    @BeforeClass
    public static void init() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Autowired
    private ExtendedAuthenticationService extendedAuthenticationService;

    private final String eeID = "d710a4df654a4d7480df52f0ae9de610";

    private static final StartVotingKey startVotingKey = StartVotingKey.ofValue("zpfrcmn28mcct4pf682w");

    @Test
    public void generateExtendedAuthentication() {

        ExtendedAuthInformation extendedAuthentication = extendedAuthenticationService.create(startVotingKey, eeID);
        assertNotNull(extendedAuthentication.getAuthenticationId());
        AuthenticationKey authenticationKey = extendedAuthentication.getAuthenticationKey();
        assertNotNull(authenticationKey);

        assertNotNull(authenticationKey);

        assertNotNull(extendedAuthentication.getAuthenticationPin());

        Optional<ExtendedAuthChallenge> extendedAuthChallengeOptional =
            extendedAuthentication.getExtendedAuthChallenge();

        assertNotNull(extendedAuthChallengeOptional);

        assertNotNull(extendedAuthentication.getEncryptedSVK());
    }

}
