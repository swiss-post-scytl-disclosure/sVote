/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.InputStream;

import com.scytl.products.ov.readers.ConfigurationInputReader;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;


@RunWith(MockitoJUnitRunner.class)
public class CreateElectionEventHolderInitializerTest {

    @Mock
    ConfigurationInputReader reader;

    @InjectMocks
    CreateElectionEventHolderInitializer sut;

    @Test
    public void setKeysConfigurationFromFile() throws Exception {

        //given
        CreateElectionEventParametersHolder holder = mock(CreateElectionEventParametersHolder.class);
        File configFile = mock(File.class);

        //when
        sut.init(holder, configFile);

        //then
        Mockito.verify(holder, times(1)).setConfigurationInput(any());
    }


    @Test
    public void setKeysConfigurationFromStream() throws Exception {

        //given
        CreateElectionEventParametersHolder holder = mock(CreateElectionEventParametersHolder.class);
        InputStream configInputStream = mock(InputStream.class);

        //when
        sut.init(holder, configInputStream);

        //then
        Mockito.verify(holder, times(1)).setConfigurationInput(any());
    }
}
