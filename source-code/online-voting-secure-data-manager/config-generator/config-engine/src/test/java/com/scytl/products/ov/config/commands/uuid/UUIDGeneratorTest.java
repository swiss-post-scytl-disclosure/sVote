/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.uuid;

import com.scytl.products.ov.constants.Constants;
import mockit.Deencapsulation;
import mockit.Expectations;
import mockit.Mocked;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class UUIDGeneratorTest {

    private static final int STANDARD_LENGTH_UUID = 32;

    private static final String EMPTY_STRING = "";

    private final UUIDParametersHolder uuidHolder = new UUIDParametersHolder(2);

    @Test
    public void generates_a_single_not_empty_UUID() {

        final UUIDGenerator uuidGen = new UUIDGenerator();
        final String generatedUUID = uuidGen.generate();

        assertThat(generatedUUID, is(not(EMPTY_STRING)));
    }

    @Test
    public void generates_a_single_UUID_which_contains_chars_of_correct_alphabet() {

        final UUIDGenerator uuidGen = new UUIDGenerator();
        final String generatedUUID = uuidGen.generate();

        assertThat(generatedUUID.matches(Constants.HEX_ALPHABET), is(true));
    }

    @Test
    public void generates_a_single_UUID_which_contains_the_correct_length() {

        final UUIDGenerator uuidGen = new UUIDGenerator();
        final String generatedUUID = uuidGen.generate();

        assertThat(generatedUUID.length(), is(STANDARD_LENGTH_UUID));
    }

    @Test
    public void generates_a_given_number_of_UUIDs() {

        final UUIDGenerator uuidGen = new UUIDGenerator();

        final UUIDOutput generatedUUIDs = uuidGen.generate(uuidHolder);

        assertThat(generatedUUIDs.getUuids().size(), is(2));
    }

    @Mocked({"randomUUID" })
    UUID uuid;

    @Test
    public void call_generate_when_N_UUIDs_are_requested() {

        final UUIDGenerator uuidGen = new UUIDGenerator();

        new Expectations() {
            {
                Deencapsulation.invoke(UUID.class, "randomUUID");
                times = 2;
            }
        };

        uuidGen.generate(uuidHolder);
    }

    @Test
    public void generates_a_empty_list_when_zero_UUIDs_are_requested() {

        final UUIDParametersHolder zeroUuidHolder = new UUIDParametersHolder(0);

        final UUIDGenerator uuidGen = new UUIDGenerator();
        final UUIDOutput generatedUUIDs = uuidGen.generate(zeroUuidHolder);

        assertThat(generatedUUIDs.getUuids().isEmpty(), is(true));

    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void throw_exception_when_invalid_number_is_passed() {

        final UUIDParametersHolder invalidZeroUuidHolder = new UUIDParametersHolder(-1);

        final UUIDGenerator uuidGen = new UUIDGenerator();

        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("The requested number of UUIDs should be greater or equal than 0.");

        uuidGen.generate(invalidZeroUuidHolder);

    }

}
