/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.it.verifiers;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.List;

import org.junit.Assert;

public class GeneratePrimeGroupMembersVerifier {

    public void preconfigValidations(final Path path, final int numPrimes) throws ParseException, IOException {

        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            Assert.assertTrue(new BigInteger(line).isProbablePrime(90));
        }
        Assert.assertEquals(numPrimes, lines.size());
    }
}
