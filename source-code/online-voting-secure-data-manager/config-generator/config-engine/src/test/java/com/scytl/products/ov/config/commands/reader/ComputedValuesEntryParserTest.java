/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.datapacks.beans.VerificationCardIdComputedValues;

public class ComputedValuesEntryParserTest {
    private final ComputedValuesEntryParser target = new ComputedValuesEntryParser();

    @Test
    public void parseValidLine() throws GeneralCryptoLibException, JsonProcessingException {
        String verificationCardId = "verificationCardId";

        String encryptedBallotCastingKey = "encryptedBallotCastingKey";

        String computedBallotCastingKey = "computedBallotCastingKey";

        String computedValuesMap = "encryptedComputedValues";

        String[] validLine = {verificationCardId, computedValuesMap, encryptedBallotCastingKey, 
                computedBallotCastingKey};

        VerificationCardIdComputedValues computedValues = target.parseEntry(validLine);

        assertNotNull(computedValues);
        assertEquals(verificationCardId, computedValues.getId());
        assertEquals(computedValuesMap, computedValues.getBallotVotingOption2prePartialChoiceCodes());
        assertEquals(encryptedBallotCastingKey, computedValues.getEncryptedBallotCastingKey());
        assertEquals(computedBallotCastingKey, computedValues.getComputedBallotCastingKey());
    }
}
