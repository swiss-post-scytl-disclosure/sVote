/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Test;

import com.scytl.products.ov.config.model.authentication.service.MultipleSecretAuthenticationKeyGenerator;

public class MultipleSecrectStrategyTest {

    private MultipleSecretAuthenticationKeyGenerator multipleStrategy =
        new MultipleSecretAuthenticationKeyGenerator();

    private StartVotingKey startVotingKey = StartVotingKey.ofValue("68n7vr7znmrdmq2hkpj7");

    @Test
    public void generateNotNull() {

        AuthenticationKey authenticationKey = multipleStrategy.generateAuthKey(startVotingKey);
        assertNotNull(authenticationKey);

    }

    @Test
    public void havePresentSecret() {

        AuthenticationKey authenticationKey = multipleStrategy.generateAuthKey(startVotingKey);
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        assertTrue(optionalSecrets.isPresent());
    }

    @Test
    public void haveSingleSecret() {

        AuthenticationKey authenticationKey = multipleStrategy.generateAuthKey(startVotingKey);
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        List<String> secrets = optionalSecrets.get();
        assertEquals(2, secrets.size());

    }

    @Test
    public void haveValueEqualToSecretsConcat() {

        AuthenticationKey authenticationKey = multipleStrategy.generateAuthKey(startVotingKey);
        String authKeyValue = authenticationKey.getValue();
        Optional<List<String>> optionalSecrets = authenticationKey.getSecrets();
        List<String> secrets = optionalSecrets.get();
        String secretsConcat = secrets.stream().collect(Collectors.joining());
        assertEquals(authKeyValue, secretsConcat);

    }
}
