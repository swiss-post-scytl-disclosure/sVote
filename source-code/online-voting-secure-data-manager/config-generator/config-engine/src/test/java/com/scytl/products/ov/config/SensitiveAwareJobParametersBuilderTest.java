/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.batch.core.JobParameters;

import com.scytl.products.ov.config.commons.beans.spring.batch.SensitiveAwareJobParametersBuilder;

@RunWith(MockitoJUnitRunner.class)
public class SensitiveAwareJobParametersBuilderTest {

    @InjectMocks
    SensitiveAwareJobParametersBuilder sut;

    @Test
    public void returnHiddenValueForSensitiveParameter() {

        String sensitiveParameterKey = "sensitiveParameterKey";
        String sensitiveParameterValue = "sensitiveParameterValue";
        sut.addSensitiveString(sensitiveParameterKey, sensitiveParameterValue);

        final JobParameters jobParameters = sut.toJobParameters();

        //note: "jobParameters.getString" calls toString on the parameter. In production code we have to be aware of this
        //otherwise we may get the "hidden" value instead of the real parameter value. For sensitiveParameters we MUST
        //use the non-typed "get" and call getValue (or if using the
        //we may need to add a method to SensitiveParameter to get the underlying value
        Assert.assertNotEquals(sensitiveParameterValue, jobParameters.getString(sensitiveParameterKey));
        //this gets the underlying value without calling toString
        Assert.assertEquals(sensitiveParameterValue, jobParameters.getParameters().get(sensitiveParameterKey).getValue());
    }

    @Test
    public void returnCorrectValueForIdentifyingParameter() {

        String identifyingParamKey = "sensitiveParameterKey1";
        String identifyingParamValue = "sensitiveParameterValue1";
        String nonIdentifyingParamKey = "sensitiveParameterKey2";
        String nonIdentifyingParamValue = "sensitiveParameterValue2";
        sut.addSensitiveString(identifyingParamKey, identifyingParamValue);
        sut.addSensitiveString(nonIdentifyingParamKey, nonIdentifyingParamValue, false);

        final JobParameters jobParameters = sut.toJobParameters();

        Assert.assertEquals(true, jobParameters.getParameters().get(identifyingParamKey).isIdentifying());
        Assert.assertEquals(false, jobParameters.getParameters().get(nonIdentifyingParamKey).isIdentifying());

    }
}
