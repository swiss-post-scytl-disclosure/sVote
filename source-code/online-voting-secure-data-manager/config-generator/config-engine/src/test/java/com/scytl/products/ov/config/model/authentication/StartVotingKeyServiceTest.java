/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication;


import static org.junit.Assert.assertNotNull;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import static org.hamcrest.MatcherAssert.assertThat;


import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.scytl.products.ov.config.SpringConfigTest;
import com.scytl.products.ov.config.model.authentication.service.StartVotingKeyService;
import com.scytl.products.ov.constants.Constants;

/**
 * Test class for the StartVotingKeyService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class StartVotingKeyServiceTest {



    @Autowired
    @Qualifier("MULTISECRET")
    AuthenticationKeyGenerator authenticationKeyGenerator;

    @Autowired
    StartVotingKeyService startVotingKeyService;
    
    @BeforeClass
    public static void init() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void alphabetLengthIsPresent() {

        assertNotNull(authenticationKeyGenerator.getAlphabet());

    }

    @Test
    public void getSVKlengthWithSecretsSpec() {

        int startVotingKeyLength = startVotingKeyService.getStartVotingKeyLength();
        assertThat(startVotingKeyLength, Matchers.greaterThan(Constants.SVK_LENGTH));

    }

}
