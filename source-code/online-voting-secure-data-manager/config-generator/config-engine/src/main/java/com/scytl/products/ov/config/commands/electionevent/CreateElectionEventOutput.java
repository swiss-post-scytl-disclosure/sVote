/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent;

import java.util.LinkedHashMap;
import java.util.Map;

import com.scytl.products.ov.config.commands.electionevent.datapacks.beans.ElGamalCredentialDataPack;
import com.scytl.products.ov.config.commands.electionevent.datapacks.beans.ElectionCredentialDataPack;

/**
 * The class that contains all the info to be saved by the serializer
 */
public class CreateElectionEventOutput {

	private final Map<String, ElectionCredentialDataPack> dataPackMap = new LinkedHashMap<>();

    private ElectionCredentialDataPack authTokenSigner;
    
    private ElGamalCredentialDataPack primeEncryption;

    public Map<String, ElectionCredentialDataPack> getDataPackMap() {
        return dataPackMap;
    }

    /**
     * @return Returns the authTokenSigner.
     */
    public ElectionCredentialDataPack getAuthTokenSigner() {
        return authTokenSigner;
    }

    /**
     * @param authTokenSigner
     *            The authTokenSigner to set.
     */
	public void setAuthTokenSigner(final ElectionCredentialDataPack authTokenSigner) {
		this.authTokenSigner = authTokenSigner;
	}

    /**
     * Clears passwords in contained {@link ElectionCredentialDataPack}
     * instances.
     */
    public void clearPasswords() {
        for (ElectionCredentialDataPack pack : dataPackMap.values()) {
            pack.clearPassword();
        }
        authTokenSigner.clearPassword();
    }

    public ElGamalCredentialDataPack getPrimeEncryption() {
        return primeEncryption;
    }

    public void setPrimeEncryption(ElGamalCredentialDataPack primeEncryption) {
    	this.primeEncryption = primeEncryption;
    }
}