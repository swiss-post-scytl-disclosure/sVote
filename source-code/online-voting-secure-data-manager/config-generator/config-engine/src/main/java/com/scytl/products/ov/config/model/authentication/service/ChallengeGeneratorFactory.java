/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.scytl.products.ov.config.model.authentication.ChallengeGenerator;
import com.scytl.products.ov.config.model.authentication.ChallengeGeneratorStrategyType;

/**
 * Factory class to get the instance of the specific strategy to generate authentication challenge data of the voter
 */
public class ChallengeGeneratorFactory {

    @Autowired
    private ProvidedChallengeSource providedChallengeSource;

    public ChallengeGenerator createStrategy(final ChallengeGeneratorStrategyType challengeGeneratorStrategy) {

        switch (challengeGeneratorStrategy) {
        case NONE:
            return new NoneChallengeGenerator();
        case PROVIDED:
            return new ProvidedChallengeGenerator(providedChallengeSource);
        }
        throw new UnsupportedOperationException();
    }
}
