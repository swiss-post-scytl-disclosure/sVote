/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication.service;

import com.scytl.products.ov.config.model.authentication.AuthenticationKeyGenerator;
import com.scytl.products.ov.config.model.authentication.AuthenticationKeyGeneratorStrategyType;

/**
 * Factory class to get the instance of the specific strategy to generate
 * authentication data of the voter
 */
public class AuthenticationGeneratorFactory {

    public AuthenticationKeyGenerator createStrategy(
            final AuthenticationKeyGeneratorStrategyType authGeneratorStrategy) {

        switch (authGeneratorStrategy) {
        case SIMPLE:
            return new SimpleAuthenticationKeyGenerator();
        case SINGLESECRET:
            return new SingleSecretAuthenticationKeyGenerator();
        case MULTISECRET:
            return new MultipleSecretAuthenticationKeyGenerator();
        }
        throw new UnsupportedOperationException();
    }
}
