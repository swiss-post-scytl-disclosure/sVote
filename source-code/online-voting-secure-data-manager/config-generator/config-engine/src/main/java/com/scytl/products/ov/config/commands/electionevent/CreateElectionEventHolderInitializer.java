/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import com.scytl.products.ov.datapacks.beans.ConfigurationInput;
import com.scytl.products.ov.readers.ConfigurationInputReader;

public class CreateElectionEventHolderInitializer {

    private final ConfigurationInputReader configurationInputReader;

    /**
     * @param configurationInputReader
     */
    public CreateElectionEventHolderInitializer(final ConfigurationInputReader configurationInputReader) {
        this.configurationInputReader = configurationInputReader;
    }

    public void init(final CreateElectionEventParametersHolder holder, final File configurationInputFile)
            throws URISyntaxException, IOException {

        ConfigurationInput configurationInput = configurationInputReader.fromFileToJava(configurationInputFile);
        holder.setConfigurationInput(configurationInput);
    }

    public void init(final CreateElectionEventParametersHolder holder, final InputStream configurationInputStream)
        throws URISyntaxException, IOException {

        ConfigurationInput configurationInput = configurationInputReader.fromStreamToJava(configurationInputStream);
        holder.setConfigurationInput(configurationInput);
    }
}
