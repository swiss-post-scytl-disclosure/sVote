/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.ballotbox;

import java.nio.file.Path;
import java.security.PrivateKey;
import java.util.Properties;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.products.ov.commons.beans.Ballot;
import com.scytl.products.ov.commons.beans.EncryptionParameters;
import com.scytl.products.ov.config.commands.electionevent.datapacks.beans.ElectionInputDataPack;
import com.scytl.products.ov.datapacks.beans.CredentialProperties;

/**
 * Encapsulates the parameters required by this command:
 * <ul>
 * <li>a {@link Ballot} ID.
 * <li>a list of ballot box IDs.
 * <li>the absolute output path as a {@code Path}.
 * <li>the signer private key.
 * </ul>
 */
public class BallotBoxParametersHolder {

    private final String ballotBoxID;

    private final String alias;

    private final Path outputPath;

    private final String ballotID;

    private final String electoralAuthorityID;

    private final String eeID;

    private final ElectionInputDataPack electionInputDataPack;

    private PrivateKey signerPrivateKey;

    private CredentialProperties ballotBoxCredentialProperties;

    private EncryptionParameters encParams;

    private CryptoAPIX509Certificate servicesCACert;

    private CryptoAPIX509Certificate electionCACert;

    private String keyForProtectingKeystorePassword;

    private String test;

    private String gracePeriod;

    private String confirmationRequired;

    private String writeInAlphabet;

    private Properties certificateProperties;

    public BallotBoxParametersHolder(final String ballotID, final String electoralAuthorityID, final String ballotBoxID,
            final String alias, final Path outputPath, final String eeID,
            final ElectionInputDataPack electionInputDataPack, final String test, final String gracePeriod,
            final String confirmationRequired, final String writeInAlphabet, Properties certificateProperties) {
        this.ballotID = ballotID;
        this.electoralAuthorityID = electoralAuthorityID;
        this.ballotBoxID = ballotBoxID;
        this.alias = alias;
        this.outputPath = outputPath;
        this.eeID = eeID;
        this.electionInputDataPack = electionInputDataPack;
        this.test = test;
        this.gracePeriod = gracePeriod;
        this.confirmationRequired = confirmationRequired;
        this.writeInAlphabet = writeInAlphabet;
        this.certificateProperties = certificateProperties;
    }

    public String getBallotBoxID() {
        return ballotBoxID;
    }

    public Path getOutputPath() {
        return outputPath;
    }

    public String getBallotID() {
        return ballotID;
    }

    public String getElectoralAuthorityID() {
        return electoralAuthorityID;
    }

    public PrivateKey getSignerPrivateKey() {
        return signerPrivateKey;
    }

    public CredentialProperties getBallotBoxCredentialProperties() {
        return ballotBoxCredentialProperties;
    }

    public void setBallotBoxCredentialProperties(final CredentialProperties ballotBoxCredentialProperties) {
        this.ballotBoxCredentialProperties = ballotBoxCredentialProperties;
    }

    public String getEeID() {
        return eeID;
    }

    public ElectionInputDataPack getInputDataPack() {
        return electionInputDataPack;
    }

    public EncryptionParameters getEncryptionParameters() {
        return encParams;
    }

    public void setEncryptionParameters(final EncryptionParameters encryptionParameters) {
        encParams = encryptionParameters;
    }

    public CryptoAPIX509Certificate getServicesCACert() {
        return servicesCACert;
    }

    public void setServicesCACert(final CryptoAPIX509Certificate servicesCACert) {
        this.servicesCACert = servicesCACert;
    }

    public CryptoAPIX509Certificate getElectionCACert() {
        return electionCACert;
    }

    public void setElectionCACert(final CryptoAPIX509Certificate electionCACert) {
        this.electionCACert = electionCACert;
    }

    public String getKeyForProtectingKeystorePassword() {
        return keyForProtectingKeystorePassword;
    }

    public void setKeyForProtectingKeystorePassword(final String keyForProtectingKeystorePassword) {
        this.keyForProtectingKeystorePassword = keyForProtectingKeystorePassword;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public void setServicesCAPrivateKey(final PrivateKey servicesCAPrivateKey) {
        signerPrivateKey = servicesCAPrivateKey;
    }

    /**
     * Gets _gracePeriod.
     *
     * @return Value of gracePeriod.
     */
    public String getGracePeriod() {
        return gracePeriod;
    }

    /**
     * Sets new _gracePeriod.
     *
     * @param gracePeriod
     *            New value of _gracePeriod.
     */
    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    /**
     * Gets _confirmationRequired.
     *
     * @return Value of confirmationRequired.
     */
    public String getConfirmationRequired() {
        return confirmationRequired;
    }

    /**
     * Sets new _confirmationRequired.
     *
     * @param confirmationRequired
     *            New value of _confirmationRequired.
     */

    public void setConfirmationRequired(String confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    /**
     * Gets _alias.
     *
     * @return Value of _alias.
     */
    public String getAlias() {
        return alias;
    }

    public String getWriteInAlphabet() {
        return writeInAlphabet;
    }

    /**
     * Get the certificate properties
     * 
     * @return the certificate properties.
     */
    public Properties getCertificateProperties() {
        return certificateProperties;
    }

}
