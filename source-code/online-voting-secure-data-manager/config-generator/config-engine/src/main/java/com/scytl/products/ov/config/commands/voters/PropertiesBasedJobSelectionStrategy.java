/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters;

public class PropertiesBasedJobSelectionStrategy implements JobSelectionStrategy {

    private String prefix;
    private String qualifier;

    public PropertiesBasedJobSelectionStrategy(final String prefix, final String qualifier) {
        this.prefix = prefix;
        this.qualifier = qualifier;
    }


    @Override
    public String select() {
        return String.format("%s-%s", prefix, qualifier);
    }
}
