/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters.datapacks.beans;

import com.scytl.cryptolib.elgamal.bean.ElGamalKeyPair;
import com.scytl.products.ov.datapacks.beans.SerializedCredentialDataPack;

public class VerificationCardCredentialDataPack extends SerializedCredentialDataPack {

    private ElGamalKeyPair verificationCardKeyPair;

    private byte[] signatureVCardPubKeyEEIDVCID;

    public ElGamalKeyPair getVerificationCardKeyPair() {
        return verificationCardKeyPair;
    }

    public void setVerificationCardKeyPair(final ElGamalKeyPair verificationCardKeyPair) {
        this.verificationCardKeyPair = verificationCardKeyPair;
    }

    public byte[] getSignatureVCardPubKeyEEIDVCID() {
        return signatureVCardPubKeyEEIDVCID;
    }

    public void setSignatureVCardPubKeyEEIDVCID(final byte[] signatureVCardPubKeyEEIDVCID) {
        this.signatureVCardPubKeyEEIDVCID = new byte[signatureVCardPubKeyEEIDVCID.length];
        System.arraycopy(signatureVCardPubKeyEEIDVCID, 0, this.signatureVCardPubKeyEEIDVCID, 0,
            signatureVCardPubKeyEEIDVCID.length);
    }

}
