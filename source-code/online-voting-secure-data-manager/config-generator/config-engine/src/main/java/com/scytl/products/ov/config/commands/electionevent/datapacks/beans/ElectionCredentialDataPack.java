/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent.datapacks.beans;

import java.security.KeyPair;
import java.time.ZonedDateTime;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.products.ov.datapacks.beans.CredentialDataPack;

/**
 * A class that represents a credentialDataPack with a keyPair and a certificate
 */
public class ElectionCredentialDataPack extends CredentialDataPack {

	private KeyPair keyPair;

	private CryptoAPIX509Certificate certificate;

	private ZonedDateTime startDate;

	private ZonedDateTime endDate;

	public KeyPair getKeyPair() {
		return keyPair;
	}

	public void setKeyPair(final KeyPair keyPair) {
		this.keyPair = keyPair;
	}

	public CryptoAPIX509Certificate getCertificate() {
		return certificate;
	}

	public void setCertificate(final CryptoAPIX509Certificate certificate) {
		this.certificate = certificate;
	}

	/**
	 * @return Returns the startDate.
	 */
	public ZonedDateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(final ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return Returns the endDate.
	 */
	public ZonedDateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            The endDate to set.
	 */
	public void setEndDate(final ZonedDateTime endDate) {
		this.endDate = endDate;
	}

}
