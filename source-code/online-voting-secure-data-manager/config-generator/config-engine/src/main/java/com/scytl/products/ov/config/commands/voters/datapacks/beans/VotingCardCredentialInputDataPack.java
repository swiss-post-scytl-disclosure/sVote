/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters.datapacks.beans;

import com.scytl.products.ov.datapacks.beans.CredentialProperties;
import com.scytl.products.ov.datapacks.beans.InputDataPack;

public class VotingCardCredentialInputDataPack extends InputDataPack {

    private final CredentialProperties credentialSignProperties;

    private final CredentialProperties credentialAuthProperties;

    public VotingCardCredentialInputDataPack(final CredentialProperties credentialSignProperties,
            final CredentialProperties credentialAuthProperties) {
        super();
        this.credentialSignProperties = credentialSignProperties;
        this.credentialAuthProperties = credentialAuthProperties;
    }

    public CredentialProperties getCredentialSignProperties() {
        return credentialSignProperties;
    }

    public CredentialProperties getCredentialAuthProperties() {
        return credentialAuthProperties;
    }
}
