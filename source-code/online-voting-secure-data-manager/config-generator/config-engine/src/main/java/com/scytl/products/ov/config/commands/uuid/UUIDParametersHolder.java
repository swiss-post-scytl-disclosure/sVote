/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.uuid;

/**
 * Holds the adapted parameters for the generation of UUIDs.
 */
public class UUIDParametersHolder {

    private final Integer numberOfUUIDs;

    public UUIDParametersHolder(final Integer numberOfUUIDs) {
        this.numberOfUUIDs = numberOfUUIDs;
    }

    /**
     * Returns a the number of UUIDs.
     *
     */
    public int getNumberOfUUIDs() {

        return numberOfUUIDs;
    }

}
