/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters.datapacks.beans;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.products.ov.datapacks.beans.CredentialDataPack;

import javax.crypto.SecretKey;
import java.security.KeyPair;

public class VotingCardSetCredentialDataPack extends CredentialDataPack {

    private SecretKey codesSecretKey;

    private KeyPair voteCastCodeSignerKeyPair;

    private CryptoAPIX509Certificate voteCastCodeSignerCertificate;

    public KeyPair getVoteCastCodeSignerKeyPair() {
        return voteCastCodeSignerKeyPair;
    }

    public void setVoteCastCodeSignerKeyPair(final KeyPair keyPair) {
        voteCastCodeSignerKeyPair = keyPair;
    }

    public CryptoAPIX509Certificate getVoteCastCodeSignerCertificate() {
        return voteCastCodeSignerCertificate;
    }

    public void setVoteCastCodeSignerCertificate(final CryptoAPIX509Certificate certificate) {
        voteCastCodeSignerCertificate = certificate;
    }

    public SecretKey getCodesSecretKey() {
        return codesSecretKey;
    }

    public void setCodesSecretKey(final SecretKey codesSecretKey) {
        this.codesSecretKey = codesSecretKey;
    }
}
