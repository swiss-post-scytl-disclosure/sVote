/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.api.output;

public class ElectionEventServiceOutput {

    private String offlineFolder;

    private String onlineAuthenticationFolder;

    private String onlineElectionInformationFolder;

    public String getOfflineFolder() {
        return offlineFolder;
    }

    public void setOfflineFolder(final String offlineFolder) {
        this.offlineFolder = offlineFolder;
    }

    public String getOnlineAuthenticationFolder() {
        return onlineAuthenticationFolder;
    }

    public void setOnlineAuthenticationFolder(final String onlineAuthenticationFolder) {
        this.onlineAuthenticationFolder = onlineAuthenticationFolder;
    }

    public String getOnlineElectionInformationFolder() {
        return onlineElectionInformationFolder;
    }

    public void setOnlineElectionInformationFolder(final String onlineElectionInformationFolder) {
        this.onlineElectionInformationFolder = onlineElectionInformationFolder;
    }
}
