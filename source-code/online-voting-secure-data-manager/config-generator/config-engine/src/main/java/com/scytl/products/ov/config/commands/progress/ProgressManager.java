/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.progress;

import java.util.Optional;
import java.util.UUID;

import com.scytl.products.ov.config.commons.progress.JobProgressDetails;

public interface ProgressManager {

    void registerJob(UUID jobId, JobProgressDetails job);
    void unregisterJob(UUID jobId);

    Optional<JobProgressDetails> getJobProgress(UUID jobId);

    void updateProgress(UUID jobId, double deltaValue);
}
