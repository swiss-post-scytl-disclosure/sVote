/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication.service;

import java.util.Optional;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.config.model.authentication.ExtendedAuthChallenge;

public interface ChallengeServiceAPI {

    /**
     * Generates the necessary info to construct the extended authentication
     * information.
     *
     * @return
     */
    Optional<ExtendedAuthChallenge> createExtendedAuthChallenge() throws GeneralCryptoLibException;

}
