/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.model.authentication.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.scytl.products.ov.config.model.authentication.ChallengeGenerator;
import com.scytl.products.ov.config.model.authentication.ExtraParams;
import com.scytl.products.ov.config.model.authentication.ProvidedChallenges;

public class ProvidedChallengeGenerator implements ChallengeGenerator {

    private final ProvidedChallengeSource providedChallengeSource;

    public ProvidedChallengeGenerator(final ProvidedChallengeSource providedChallengeSource) {
        this.providedChallengeSource = providedChallengeSource;
    }

    @Override
    public ExtraParams generateExtraParams() {

        ProvidedChallenges providedChallenges = providedChallengeSource.next();

        String alias = providedChallenges.getAlias();
        List<String> challenges = providedChallenges.getChallenges();

        String value = challenges.stream().collect(Collectors.joining());

        return ExtraParams.ofChallenges(Optional.of(value), Optional.of(alias));
    }
}
