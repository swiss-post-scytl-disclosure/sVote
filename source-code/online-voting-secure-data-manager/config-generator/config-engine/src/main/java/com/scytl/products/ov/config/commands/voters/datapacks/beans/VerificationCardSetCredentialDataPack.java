/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters.datapacks.beans;

import java.security.KeyPair;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.products.ov.datapacks.beans.SerializedCredentialDataPack;

public class VerificationCardSetCredentialDataPack extends SerializedCredentialDataPack {

    private KeyPair verificationCardIssuerKeyPair;

    private CryptoAPIX509Certificate verificationCardIssuerCert;

    private ElGamalPublicKey choiceCodesEncryptionPublicKey;

    private ElGamalPublicKey[] nonCombinedChoiceCodesEncryptionPublicKeys;

    public ElGamalPublicKey[] getNonCombinedChoiceCodesEncryptionPublicKeys() {
        return nonCombinedChoiceCodesEncryptionPublicKeys;
    }

    public void setNonCombinedChoiceCodesEncryptionPublicKeys(
            ElGamalPublicKey[] nonCombinedChoiceCodesEncryptionPublicKeys) {
        this.nonCombinedChoiceCodesEncryptionPublicKeys = nonCombinedChoiceCodesEncryptionPublicKeys;
    }

    public KeyPair getVerificationCardIssuerKeyPair() {
        return verificationCardIssuerKeyPair;
    }

    public CryptoAPIX509Certificate getVerificationCardIssuerCert() {
        return verificationCardIssuerCert;
    }

    public ElGamalPublicKey getChoiceCodesEncryptionPublicKey() {
        return choiceCodesEncryptionPublicKey;
    }

    public void setVerificationCardIssuerKeyPair(final KeyPair verificationCardIssuerKeyPair) {
    	this.verificationCardIssuerKeyPair = verificationCardIssuerKeyPair;
    }

    public void setVerificationCardIssuerCert(final CryptoAPIX509Certificate verificationCardIssuerCert) {
    	this.verificationCardIssuerCert = verificationCardIssuerCert;
    }

    public void setChoiceCodesEncryptionPublicKey(final ElGamalPublicKey choicesCodePublicKey) {
    	this.choiceCodesEncryptionPublicKey = choicesCodePublicKey;
    }

}
