/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.products.oscore.logging.api.factory.LoggingFactory;
import com.scytl.products.oscore.logging.api.factory.SecureLoggingFactory;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.factory.LoggingFactoryLog4j;
import com.scytl.products.oscore.logging.core.factory.SecureLoggingFactoryLog4j;
import com.scytl.products.oscore.logging.core.formatter.SplunkFormatter;

@Configuration
public class SecureLoggingConfig {

	/**
     * We need this to make the app work. either activate it here or enable component-scan in the 'root'
     * config class
     * @return
     */

    @Autowired
    private SpringConfigServices springConfigServices;
    
    @Bean
    public SecureLoggingFactory secureLoggingFactory(final MessageFormatter messageFormatter) {
        return new SecureLoggingFactoryLog4j(messageFormatter);
    }

    @Bean
    public SecureLoggingWriter secureLoggingWriter() {
        return secureLoggingFactory(messageFormatter()).getLogger("SecureLogger");
    }

    @Bean
    public MessageFormatter messageFormatter() {
        return new SplunkFormatter("OV", "CS", springConfigServices.transactionInfoProvider());
    }

    @Bean
    public LoggingFactory loggingFactory() {
        return new LoggingFactoryLog4j(messageFormatter());
    }

    @Bean
    public LoggingWriter defaultLoggingWriter() {
        return loggingFactory().getLogger("splunkable");
    }

}
