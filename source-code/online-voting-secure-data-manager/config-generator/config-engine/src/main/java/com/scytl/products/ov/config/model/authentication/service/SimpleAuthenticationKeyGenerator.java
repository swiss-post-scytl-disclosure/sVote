/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by riglesias on 8/07/16.
 */
package com.scytl.products.ov.config.model.authentication.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.scytl.products.ov.config.model.authentication.AuthenticationKey;
import com.scytl.products.ov.config.model.authentication.AuthenticationKeyGenerator;
import com.scytl.products.ov.config.model.authentication.StartVotingKey;
import com.scytl.products.ov.constants.Constants;

/**
 * Pretty Basic strategy in which only the SVK will be used as the only secret.
 */
public class SimpleAuthenticationKeyGenerator implements AuthenticationKeyGenerator {

    @Override
    public AuthenticationKey generateAuthKey(StartVotingKey startVotingKey) {

        final Optional<List<String>> secrets = Optional.of(Arrays.asList(startVotingKey.getValue()));

        return AuthenticationKey.ofSecrets(startVotingKey.getValue(), secrets);
    }

    @Override
    public int getSecretsLength() {
        return Constants.SVK_LENGTH;
    }

}
