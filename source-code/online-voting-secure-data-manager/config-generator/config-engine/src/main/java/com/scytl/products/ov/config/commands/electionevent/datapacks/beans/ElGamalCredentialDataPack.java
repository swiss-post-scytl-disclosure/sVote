/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent.datapacks.beans;

import com.scytl.cryptolib.elgamal.bean.ElGamalKeyPair;
import com.scytl.products.ov.datapacks.beans.CredentialDataPack;

/**
 * Extension of {@link CredentialDataPack} for ElGamal keys.
 */
public class ElGamalCredentialDataPack extends CredentialDataPack {
    private ElGamalKeyPair keyPair;

    public ElGamalKeyPair getKeyPair() {
        return keyPair;
    }

    public void setKeyPair(ElGamalKeyPair keyPair) {
        this.keyPair = keyPair;
    }
}
