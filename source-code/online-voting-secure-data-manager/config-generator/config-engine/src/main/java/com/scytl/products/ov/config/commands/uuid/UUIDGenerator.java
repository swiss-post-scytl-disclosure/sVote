/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.uuid;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class which generates UUIDs.
 */
public class UUIDGenerator {

    /**
     * Generates a random {@link UUID}, from which all dashes "-" are removed.
     *
     * @return the UUID as a String.
     */
    public String generate() {

        final String pk = UUID.randomUUID().toString();

        return pk.replaceAll("-", "");
    }

    /**
     * Generates {@code n} UUIDs by calling {@link UUIDGenerator#generate()}. When requesting 0, an empty array is
     * returned.
     *
     * @param uuidHolder
     * @return
     */
    public UUIDOutput generate(final UUIDParametersHolder uuidHolder) {

        final int numberOfUUIDs = uuidHolder.getNumberOfUUIDs();

        validateNumber(numberOfUUIDs);

        final List<String> uuids = new ArrayList<>();

        for (int i = 0; i < numberOfUUIDs; i++) {
            uuids.add(generate());
        }

        return new UUIDOutput(uuids);
    }

    private void validateNumber(final int n) {
        if (n < 0) {
            throw new IllegalArgumentException("The requested number of UUIDs should be greater or equal than 0.");
        }
    }

}
