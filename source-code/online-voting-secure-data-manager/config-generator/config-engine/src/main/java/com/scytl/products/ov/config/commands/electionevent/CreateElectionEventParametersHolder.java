/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent;

import java.nio.file.Path;

import com.scytl.products.ov.commons.beans.AuthenticationParams;
import com.scytl.products.ov.commons.beans.ElectionInformationParams;
import com.scytl.products.ov.commons.beans.VotingWorkflowContextData;
import com.scytl.products.ov.config.commands.electionevent.datapacks.beans.ElectionInputDataPack;
import com.scytl.products.ov.datapacks.beans.ConfigurationInput;
import com.scytl.products.ov.datapacks.beans.CreateElectionEventCertificatePropertiesContainer;

/**
 * The container with all the info needed by the CreateElectionEventGenerator.
 */
public class CreateElectionEventParametersHolder {

    private final ElectionInputDataPack electionInputDataPack;

    private final Path electionFolder;

    private final Path outputFolder;

    private final Path offlineFolder;

    private final Path onlineAuthenticationFolder;

    private final Path onlineElectionInformationFolder;

    private final Path onlineVotingWorkflowFolder;

    private final AuthenticationParams authenticationParams;

    private final ElectionInformationParams electionInformationParams;

    private final VotingWorkflowContextData votingWorkflowContextData;

    private ConfigurationInput configurationInput;

    private final String keyForProtectingKeystorePassword;

    private CreateElectionEventCertificatePropertiesContainer certificatePropertiesInput;

    public CreateElectionEventParametersHolder(final ElectionInputDataPack electionInputDataPack,
            final Path outputFolder, final Path electionFolder, final Path offlineFolder,
            final Path onlineAuthenticationFolder, final Path onlineElectionInformationFolder,
            final Path onlineVotingWorkflowFolder, final AuthenticationParams authenticationParams,
            final ElectionInformationParams electionInformationParams,
            final VotingWorkflowContextData votingWorkflowContextData, final String keyForProtectingKeystorePassword,
            final CreateElectionEventCertificatePropertiesContainer certificatePropertiesInput) {

        this.electionInputDataPack = electionInputDataPack;
        this.outputFolder = outputFolder;
        this.electionFolder = electionFolder;
        this.offlineFolder = offlineFolder;
        this.onlineAuthenticationFolder = onlineAuthenticationFolder;
        this.onlineElectionInformationFolder = onlineElectionInformationFolder;
        this.onlineVotingWorkflowFolder = onlineVotingWorkflowFolder;
        this.authenticationParams = authenticationParams;
        this.electionInformationParams = electionInformationParams;
        this.votingWorkflowContextData = votingWorkflowContextData;
        this.keyForProtectingKeystorePassword = keyForProtectingKeystorePassword;
        this.certificatePropertiesInput = certificatePropertiesInput;
    }

    /**
     * @return Returns the onlineAuthenticationFolder.
     */
    public Path getOnlineAuthenticationFolder() {
        return onlineAuthenticationFolder;
    }

    /**
     * @return Returns the onlineElectionInformationFolder.
     */
    public Path getOnlineElectionInformationFolder() {
        return onlineElectionInformationFolder;
    }

    /**
     * @return Returns the onlinevotingWorkflowFolder.
     */
    public Path getOnlineVotingWorkflowFolder() {
        return onlineVotingWorkflowFolder;
    }

    /**
     * @return Returns the outputFolder.
     */
    public Path getOutputFolder() {
        return outputFolder;
    }

    /**
     * @return Returns the offlineFolder.
     */
    public Path getOfflineFolder() {
        return offlineFolder;
    }

    /**
     * @return Returns the authenticationParams.
     */
    public AuthenticationParams getAuthenticationParams() {
        return authenticationParams;
    }

    /**
     * @return Returns the electionInformationParams.
     */
    public ElectionInformationParams getElectionInformationParams() {
        return electionInformationParams;
    }

    /**
     * @return Returns the inputDataPack.
     */
    public ElectionInputDataPack getInputDataPack() {
        return electionInputDataPack;
    }

    /**
     * @return Returns the votingWorkflowContextData.
     */
    public VotingWorkflowContextData getVotingWorkflowContextData() {
        return votingWorkflowContextData;
    }

    /**
     * @return Returns the configurationInput.
     */
    public ConfigurationInput getConfigurationInput() {
        return configurationInput;
    }

    /**
     * @param configurationInput
     *            The configurationInput to set.
     */
    public void setConfigurationInput(final ConfigurationInput configurationInput) {
        this.configurationInput = configurationInput;
    }

    public String getKeyForProtectingKeystorePassword() {
        return keyForProtectingKeystorePassword;
    }

    /**
     * @return Returns the electionFolder.
     */
    public Path getElectionFolder() {
        return electionFolder;
    }

    /**
     * Get all the certificate properties.
     * 
     * @return all the certificate properties.
     */
    public CreateElectionEventCertificatePropertiesContainer getCertificatePropertiesInput() {
        return certificatePropertiesInput;
    }
}
