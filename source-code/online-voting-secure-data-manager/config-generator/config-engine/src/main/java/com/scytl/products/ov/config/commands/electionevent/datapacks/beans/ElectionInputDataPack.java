/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.electionevent.datapacks.beans;

import java.time.ZonedDateTime;

import com.scytl.products.ov.datapacks.beans.CredentialProperties;
import com.scytl.products.ov.datapacks.beans.InputDataPack;

/**
 * A class representing a inputDatapack with a credentialProperties object
 */
public class ElectionInputDataPack extends InputDataPack {

    private CredentialProperties credentialProperties;

    private ZonedDateTime electionStartDate;

    private ZonedDateTime electionEndDate;

    
    public void setCredentialProperties(CredentialProperties credentialProperties) {
        this.credentialProperties = credentialProperties;
    }

    public CredentialProperties getCredentialProperties() {
        return credentialProperties;
    }
    
    /**
     * Sets new electionStartDate.
     *
     * @param electionStartDate New value of _electionStartDate.
     */
    public void setElectionStartDate(ZonedDateTime electionStartDate) {
        this.electionStartDate = electionStartDate;
    }

    /**
     * Sets new electionEndDate.
     *
     * @param electionEndDate New value of _electionEndDate.
     */
    public void setElectionEndDate(ZonedDateTime electionEndDate) {
        this.electionEndDate = electionEndDate;
    }

    /**
     * Gets _electionStartDate.
     *
     * @return Value of _electionStartDate.
     */
    public ZonedDateTime getElectionStartDate() {
        return electionStartDate;
    }

    /**
     * Gets _electionEndDate.
     *
     * @return Value of _electionEndDate.
     */
    public ZonedDateTime getElectionEndDate() {
        return electionEndDate;
    }
}
