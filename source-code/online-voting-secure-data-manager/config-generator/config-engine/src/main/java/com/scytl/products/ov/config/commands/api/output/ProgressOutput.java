/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.api.output;

/**
 * @deprecated
 */
@Deprecated
public class ProgressOutput {

    private Integer percentage;

    private Long remainingTime;

    /**
     * @return Returns the remainingTime.
     */
    public Long getRemainingTime() {
        return remainingTime;
    }

    /**
     * @param remainingTime
     *            The remainingTime to set.
     */
    public void setRemainingTime(final Long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(final Integer percentage) {
        this.percentage = percentage;
    }
}
