/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters.datapacks.beans;

import com.scytl.products.ov.datapacks.beans.CredentialProperties;
import com.scytl.products.ov.datapacks.beans.InputDataPack;

public class VotingCardSetCredentialInputDataPack extends InputDataPack {

    private final CredentialProperties credentialProperties;

    public VotingCardSetCredentialInputDataPack(final CredentialProperties credentialProperties) {
        super();
        this.credentialProperties = credentialProperties;
    }

    public CredentialProperties getCredentialProperties() {
        return credentialProperties;
    }
}
