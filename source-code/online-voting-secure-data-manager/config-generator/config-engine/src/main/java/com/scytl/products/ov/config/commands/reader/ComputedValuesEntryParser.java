/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.reader;

import com.googlecode.jcsv.reader.CSVEntryParser;
import com.scytl.products.ov.datapacks.beans.VerificationCardIdComputedValues;

/**
 * A utility class used to parse the lines contained on the computed values CSV
 * and convert them to a VerificationCardIdComputedValues objects
 */
public final class ComputedValuesEntryParser implements CSVEntryParser<VerificationCardIdComputedValues> {

    @Override
    public VerificationCardIdComputedValues parseEntry(final String... data) {
        return new VerificationCardIdComputedValues(data[0], data[1], data[2], data[3]);
    }
}
