/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.uuid;

import java.util.List;

/**
 * Encapsulates the output produces by {@link UUIDGenerator}.
 */
public class UUIDOutput {

    private final List<String> uuids;

    public UUIDOutput(final List<String> uuids) {
        this.uuids = uuids;
    }

    public List<String> getUuids() {
        return uuids;
    }

}
