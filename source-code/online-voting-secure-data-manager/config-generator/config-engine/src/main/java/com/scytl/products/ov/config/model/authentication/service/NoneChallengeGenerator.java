/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by riglesias on 8/07/16.
 */
package com.scytl.products.ov.config.model.authentication.service;

import java.util.Optional;

import com.scytl.products.ov.config.model.authentication.ChallengeGenerator;
import com.scytl.products.ov.config.model.authentication.ExtraParams;

/**
 * Strategy in which there is no challenge.
 */
public class NoneChallengeGenerator implements ChallengeGenerator {

    /**
     * @return generate a new AuthenticationKey using the starVotingKey as
     */
    @Override
    public ExtraParams generateExtraParams() {
        return ExtraParams.ofChallenges(Optional.empty(), Optional.empty());
    }
}
