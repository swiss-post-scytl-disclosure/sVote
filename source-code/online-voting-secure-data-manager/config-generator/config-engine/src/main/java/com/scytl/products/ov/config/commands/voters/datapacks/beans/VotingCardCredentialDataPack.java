/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.commands.voters.datapacks.beans;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.products.ov.datapacks.beans.SerializedCredentialDataPack;

import java.security.KeyPair;

public class VotingCardCredentialDataPack extends SerializedCredentialDataPack {

    private KeyPair keyPairSign;

    private KeyPair keyPairAuth;

    private CryptoAPIX509Certificate certificateSign;

    private CryptoAPIX509Certificate certificateAuth;

    public KeyPair getKeyPairSign() {
        return keyPairSign;
    }

    public void setKeyPairSign(final KeyPair keyPairSign) {
        this.keyPairSign = keyPairSign;
    }

    public KeyPair getKeyPairAuth() {
        return keyPairAuth;
    }

    public void setKeyPairAuth(final KeyPair keyPairAuth) {
    	this.keyPairAuth = keyPairAuth;
    }

    public CryptoAPIX509Certificate getCertificateSign() {
        return certificateSign;
    }

    public void setCertificateSign(final CryptoAPIX509Certificate certificateSign) {
    	this.certificateSign = certificateSign;
    }

    public CryptoAPIX509Certificate getCertificateAuth() {
        return certificateAuth;
    }

    public void setCertificateAuth(final CryptoAPIX509Certificate certificateAuth) {
    	this.certificateAuth = certificateAuth;
    }
}
