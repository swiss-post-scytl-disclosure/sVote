The properties files in this folder are read when generating certain certificates during the generation
of election configurations.

If the values must be changed, then care should be taken because there are dependencies between the
generated certificates, and it could be possible to introduce some inconsistencies. To avoid introducing
any inconsistencies, make sure that all dependencies are also updated. For example, if certificate "A"
is the parent of certificate "B", and if the value for the property "subject.common.name" of certificate
"A" is changed, then the value of the property "issuer.common.name" of certificate "B" must also be
updated with the same value. If these values are left in an inconsistent state, then certificate validations
errors can occur during later phases.