/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.constants;

public final class Constants {

	public static final int MAX_EXPONENT_SIZE = 257;

    public static final String CSR_SIGNING_ALGORITHM = "SHA256withRSA";

    public static final String CSR_BEGIN_STRING = "-----BEGIN CERTIFICATE REQUEST-----";

    public static final String CSR_END_STRING = "-----END CERTIFICATE REQUEST-----";

    public static final char[] SHARE_SIGNATURE_LABEL = "share_signature".toCharArray();

    public static final char[] ENCRYPTED_SHARE_LABEL = "share_public_section".toCharArray();

    /**
	 * Non-public constructor
	 */
	private Constants() {
	}
}
