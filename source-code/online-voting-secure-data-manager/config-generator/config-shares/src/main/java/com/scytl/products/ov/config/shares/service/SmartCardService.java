/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 11/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.config.shares.service;

import java.security.PrivateKey;
import java.security.PublicKey;

import com.scytl.products.ov.config.multishare.MultipleSharesContainer;
import com.scytl.products.ov.config.shares.exception.SmartcardException;
import com.scytl.shares.Share;

public interface SmartCardService {

    /**
     * Write a share into the smartCard
     * 
     * @param share
     * @param name
     * @param oldPinPuk
     * @param newPinPuk
     * @param signingPrivateKey
     * @throws SmartcardException
     */
    void write(final Share share, final String name, final String oldPinPuk, final String newPinPuk,
            final PrivateKey signingPrivateKey) throws SmartcardException;

    /**
     * Read a share from the smart card
     * 
     * @param pin
     * @param signatureVerificationPublicKey
     * @return
     * @throws SmartcardException
     */
    Share read(final String pin, final PublicKey signatureVerificationPublicKey) throws SmartcardException;

    /**
     * Checks the status of the inserted smartcard.
     *
     * @return true if the smartcard status is satisfactory, false otherwise
     */
    boolean isSmartcardOk();

    /**
     * Read the smartcard label
     *
     * @return the label written to the smartcard
     * @throws SmartcardException
     */
    String readSmartcardLabel() throws SmartcardException;

    /**
     * @param pin
     * @param signatureVerificationPublicKey
     * @return
     * @throws SmartcardException
     */
    MultipleSharesContainer readElGamal(String pin, PublicKey signatureVerificationPublicKey) throws SmartcardException;
}
