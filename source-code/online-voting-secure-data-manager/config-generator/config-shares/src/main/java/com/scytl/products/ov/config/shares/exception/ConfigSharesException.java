/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.exception;

public final class ConfigSharesException extends RuntimeException {

	private static final long serialVersionUID = 6718357803841022614L;

	public ConfigSharesException(final String message, final Throwable cause) {
        super(message, cause);
    }

	public ConfigSharesException(final String cause) {
        super(cause);
    }
}
