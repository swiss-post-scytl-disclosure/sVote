/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.keys;

import java.security.KeyException;
import java.security.PrivateKey;
import java.security.PublicKey;

public interface PrivateKeySerializer {

    byte[] serialize(PrivateKey privateKey);

    PrivateKey reconstruct(byte[] recovered, PublicKey publicKey) throws KeyException;
}
