/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.exception;

public final class SharesException extends Exception {

    private static final long serialVersionUID = 3166454515838344983L;

    /**
     * Instantiates a new shares exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public SharesException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new shares exception.
     *
     * @param cause
     *            the cause
     */
	public SharesException(final String cause) {
        super(cause);
    }
}
