/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.domain;

/**
 * Defines the context in which a create shares operation is to be performed.
 */
public final class CreateSharesOperationContext {

    private final SharesType sharesType;

    /**
     * @param sharesType
     *            the type of key that the shares comprise.
     */
    public CreateSharesOperationContext(final SharesType sharesType) {
        this.sharesType = sharesType;
    }

    /**
     * @return the sharesType.
     */
    public SharesType getSharesType() {
        return sharesType;
    }
}
