/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.keys.elgamal;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.products.ov.config.shares.exception.ConfigSharesException;

public final class ElGamalPrivateKeyAdapter implements PrivateKey {

    private static final long serialVersionUID = 1392776796610868176L;

    private final ElGamalPrivateKey privateKey;

    /**
     * @param privateKey
     *            the ElGamal private key to set in this adapter.
     */
    public ElGamalPrivateKeyAdapter(final ElGamalPrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    /**
     * @return the private key.
     */
    public ElGamalPrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * @see java.security.Key#getAlgorithm()
     */
    @Override
    public String getAlgorithm() {
        return "SCYTL_EL_GAMAL";
    }

    /**
     * @see java.security.Key#getFormat()
     */
    @Override
    public String getFormat() {
        return "SCYTL";
    }

    /**
     * @see java.security.Key#getEncoded()
     */
    @Override
    public byte[] getEncoded() {
        try {
            return privateKey.toJson().getBytes(StandardCharsets.UTF_8);
        } catch (GeneralCryptoLibException e) {
            throw new ConfigSharesException("Error while trying to get the encoding of the ElGamal private key", e);
        }
    }
}
