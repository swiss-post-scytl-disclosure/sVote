/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.exception;

/**
 * Exception thrown when trying to read a smartcard with the wrong pin
 */
public final class WrongPinException extends SmartcardException {

    private static final long serialVersionUID = 7647658209631196258L;

    public WrongPinException() {
        super("The pin is not correct", null);
    }
}
