/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.keys.elgamal;

import java.util.List;

import com.scytl.crypto.destroy.BigIntegerDestroyer;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.products.ov.config.shares.exception.SharesException;

public final class ElGamalPrivateKeyDestroyer {

    private final BigIntegerDestroyer bigIntegerDestroyer;

    public ElGamalPrivateKeyDestroyer() {
        bigIntegerDestroyer = new BigIntegerDestroyer();
    }

    public void destroy(final ElGamalPrivateKey keyAsElGamalPrivateKey) throws SharesException {

        List<Exponent> keys = keyAsElGamalPrivateKey.getKeys();
        for (Exponent exponent : keys) {
            bigIntegerDestroyer.destroyInstances(exponent.getValue());
        }
    }
}
