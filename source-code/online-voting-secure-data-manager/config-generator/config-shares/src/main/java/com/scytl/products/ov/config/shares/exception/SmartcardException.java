/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.exception;

/**
 * Base exception for wrapping smartcard-related exceptions
 */
public class SmartcardException extends Exception {

    private static final long serialVersionUID = 1261427122008988894L;

    public SmartcardException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
