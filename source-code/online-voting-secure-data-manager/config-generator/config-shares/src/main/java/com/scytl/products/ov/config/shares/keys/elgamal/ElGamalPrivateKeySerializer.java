/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.keys.elgamal;

import java.security.KeyException;
import java.security.PrivateKey;
import java.security.PublicKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.products.ov.config.shares.keys.PrivateKeySerializer;

public final class ElGamalPrivateKeySerializer implements PrivateKeySerializer {

    private final ElGamalUtils elGamalUtils;

    public ElGamalPrivateKeySerializer() {
        this.elGamalUtils = new ElGamalUtils();
    }

    /**
     * Serializes an ElGamal private key, using the utils class from the projectslib
     *
     * @see com.scytl.products.ov.config.shares.keys.nsw.shares.applet.crypto.PrivateKeySerializer#serialize(java.security.PrivateKey)
     * @throws IllegalArgumentException
     *             If the argument passed is not an ElGamal key
     */
    @Override
    public byte[] serialize(final PrivateKey privateKeyAdapter) {
        if (!(privateKeyAdapter instanceof ElGamalPrivateKeyAdapter)) {
            throw new IllegalArgumentException("The private key must be an El Gamal private key");
        }

        return elGamalUtils.serialize(((ElGamalPrivateKeyAdapter) privateKeyAdapter).getPrivateKey());
    }

    /**
     * Reconstructs an ElGamal private key, using the utils class from the projectslib
     *
     * @see com.scytl.products.ov.config.shares.keys.nsw.shares.applet.crypto.PrivateKeySerializer#reconstruct(byte[], java.security.PublicKey)
     */
    @Override
    public PrivateKey reconstruct(final byte[] recovered, final PublicKey publicKeyAdapter) throws KeyException {
        if (!(publicKeyAdapter instanceof ElGamalPublicKeyAdapter)) {
            throw new IllegalArgumentException("The public key must be an El Gamal public key");
        }

        try {
            ElGamalPrivateKey elGamalPrivateKey =
                    elGamalUtils.reconstruct(((ElGamalPublicKeyAdapter) publicKeyAdapter).getPublicKey(), recovered);
            return new ElGamalPrivateKeyAdapter(elGamalPrivateKey);
        } catch (GeneralCryptoLibException e) {
            throw new KeyException("Reconstruction of El Gamal private key failed",e );
        }
    }
}
