/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.keys.rsa;

import java.security.KeyPair;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.products.ov.config.shares.keys.KeyPairGenerator;

public final class RSAKeyPairGenerator implements KeyPairGenerator {

    private final AsymmetricServiceAPI service;

    public RSAKeyPairGenerator(final AsymmetricServiceAPI asymmetricService) {
        this.service = asymmetricService;
    }

    @Override
    public KeyPair generate() {

        return service.getKeyPairForSigning();
    }
}
