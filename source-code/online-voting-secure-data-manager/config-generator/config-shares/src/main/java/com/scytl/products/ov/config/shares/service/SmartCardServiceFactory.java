/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 11/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.config.shares.service;

/**
 * Factory that instantiates the proper SmartCardService, whether the smart
 * cards will be used or not . Last scenario will be provided for automated e2e
 */
public final class SmartCardServiceFactory {

    private SmartCardServiceFactory() {
    }

    /**
     * Returns an implementation of {@link SmartCardService} which uses physical
     * smart cards depending on the specified flag.
     *
     * @param useSmartCards
     *            the flag
     * @return the instance.
     */
    public static SmartCardService getSmartCardService(
            final boolean useSmartCards) {
        return useSmartCards ? new DefaultSmartCardService()
                : new FileSystemSmartCardService();
    }
}
