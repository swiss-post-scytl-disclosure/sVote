/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.keys.elgamal;

import java.security.PublicKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.products.ov.config.shares.exception.ConfigSharesException;

public final class ElGamalPublicKeyAdapter implements PublicKey {

    private static final long serialVersionUID = 2142776796684568176L;

    private final ElGamalPublicKey publicKey;

    /**
     * @param publicKey
     *            the ElGamal public key to set in this adapter.
     */
    public ElGamalPublicKeyAdapter(final ElGamalPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * @return the public key.
     */
    public ElGamalPublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * @see java.security.Key#getAlgorithm()
     */
    @Override
    public String getAlgorithm() {
        return "SCYTL_EL_GAMAL";
    }

    /**
     * @see java.security.Key#getFormat()
     */
    @Override
    public String getFormat() {
        return "SCYTL";
    }

    /**
     * @see java.security.Key#getEncoded()
     */
    @Override
    public byte[] getEncoded() {
        try {
            return publicKey.toJson().getBytes();
        } catch (GeneralCryptoLibException e) {
            throw new ConfigSharesException("Error while trying to get the encoding of the ElGamal public key", e);
        }
    }
}
