/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.exception;

/**
 * Exception thrown when trying to read from/write to a smartcart and no smartcard is found in the reader
 */
public final class NoSmartcardFoundException extends SmartcardException {

    private static final long serialVersionUID = 3208202612677363214L;

    public NoSmartcardFoundException() {
        super("There is no smartcard in the reader", null);
    }
}
