/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.domain;

public enum SharesType {

    ADMIN_BOARD("admin"), ELECTORAL_BOARD("electoral");

    private String type;

    private SharesType(final String type) {
        this.type = type;
    }
    
    String getType() {
        return type;
    }
    
    public static SharesType fromString(final String type) {
        if (type == null || "".equals(type)) {
            throw new IllegalArgumentException("Type can't be empty");
        }
        for (SharesType t : SharesType.values()) {
            if (type.equals(t.getType())) {
                return t;
            }
        }

        throw new IllegalArgumentException("Wrong value for parameter type");
    }
}
