/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.shares.keys.elgamal;

import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.security.KeyException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.cryptolib.elgamal.service.ElGamalService;

public class ElGamalKeyPairGeneratorTest {

    private static ElGamalEncryptionParameters _elGamalEncryptionParameters;

    private static ElGamalServiceAPI _elGamalService;

    @BeforeClass
    public static void setUp() throws GeneralCryptoLibException {

        BigInteger p = new BigInteger("23");
        BigInteger q = new BigInteger("11");
        BigInteger g = new BigInteger("2");

        _elGamalEncryptionParameters = new ElGamalEncryptionParameters(p, q, g);

        _elGamalService = new ElGamalService();
    }

    @Test
    public void whenGenerateKeysThenExpectedAlgorithm() throws KeyException {
        ElGamalKeyPairGenerator elGamalKeyPairGenerator =
            new ElGamalKeyPairGenerator(_elGamalEncryptionParameters, 1, _elGamalService);

        KeyPair generatedKeyPair = elGamalKeyPairGenerator.generate();

        PrivateKey privateKey = generatedKeyPair.getPrivate();
        PublicKey publicKey = generatedKeyPair.getPublic();

        assertTrue(privateKey.getAlgorithm().equals("SCYTL_EL_GAMAL"));
        assertTrue(publicKey.getAlgorithm().equals("SCYTL_EL_GAMAL"));
    }

    @Test
    public void whenGenerateMultipleSubkeysThenExpectedAlgorithm() throws KeyException {
        ElGamalKeyPairGenerator elGamalKeyPairGenerator =
            new ElGamalKeyPairGenerator(_elGamalEncryptionParameters, 3, _elGamalService);

        KeyPair generatedKeyPair = elGamalKeyPairGenerator.generate();
        PrivateKey privateKey = generatedKeyPair.getPrivate();
        PublicKey publicKey = generatedKeyPair.getPublic();

        assertTrue(privateKey.getAlgorithm().equals("SCYTL_EL_GAMAL"));
        assertTrue(publicKey.getAlgorithm().equals("SCYTL_EL_GAMAL"));
    }
}
