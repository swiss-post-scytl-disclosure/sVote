/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.webapp.mvc;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MvcResult;

import com.scytl.products.ov.config.commands.api.ConfigurationService;

/**
 * Test the controller with the real verification card ID generator.
 * 
 * @see ConfigurationEngineControllerTest
 */
public class ConfigurationEngineControllerITest extends ConfigurationEngineControllerTestBase {
    @Test
    public void precomputeAThousandItems() throws Exception {
        final int itemCount = 1000;

        MvcResult result = runPrecomputeTest(itemCount);

        // Ensure that the right amount of items is generated.
        assertThat(result.getResponse().getContentAsString().split("\n").length, equalTo(itemCount));
    }

    @Configuration
    @Import(ConfigurationEngineControllerTestBase.TestConfig.class)
    static class TestConfig {
        @Bean
        ConfigurationService configurationService() {
            return new ConfigurationService();
        }
    }
}
