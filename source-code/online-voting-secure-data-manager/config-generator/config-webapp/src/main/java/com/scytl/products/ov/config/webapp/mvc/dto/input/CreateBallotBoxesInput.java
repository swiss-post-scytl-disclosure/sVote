/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.webapp.mvc.dto.input;

import java.util.Properties;

public class CreateBallotBoxesInput {

    private String ballotID;

    private String electoralAuthorityID;

    private String ballotBoxID;

    private String alias;

    private String outputFolder;

    private String start;

    private String end;

    private Integer validityPeriod;

    private String keyForProtectingKeystorePassword;

    private String test;

    private String gracePeriod;

    private String confirmationRequired;

    private String writeInAlphabet;

    private Properties ballotBoxCertificateProperties;

    public String getBallotID() {
        return ballotID;
    }

    public void setBallotID(final String ballotID) {
        this.ballotID = ballotID;
    }

    public String getElectoralAuthorityID() {
        return electoralAuthorityID;
    }

    public void setElectoralAuthorityID(final String electoralAuthorityID) {
        this.electoralAuthorityID = electoralAuthorityID;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public void setOutputFolder(final String outputFolder) {
        this.outputFolder = outputFolder;
    }

    public String getBallotBoxID() {
        return ballotBoxID;
    }

    public void setBallotBoxID(final String ballotBoxID) {
        this.ballotBoxID = ballotBoxID;
    }

    public String getStart() {
        return start;
    }

    public void setStart(final String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(final String end) {
        this.end = end;
    }

    public Integer getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(final Integer validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public String getKeyForProtectingKeystorePassword() {
        return keyForProtectingKeystorePassword;
    }

    public void setKeyForProtectingKeystorePassword(final String keyForProtectingKeystorePassword) {
        this.keyForProtectingKeystorePassword = keyForProtectingKeystorePassword;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    /**
     * Gets _gracePeriod.
     *
     * @return Value of gracePeriod.
     */
    public String getGracePeriod() {
        return gracePeriod;
    }

    /**
     * Sets new _gracePeriod.
     *
     * @param gracePeriod
     *            New value of _gracePeriod.
     */
    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    /**
     * Gets _confirmationRequired.
     *
     * @return Value of confirmationRequired.
     */
    public String getConfirmationRequired() {
        return confirmationRequired;
    }

    /**
     * Sets new _confirmationRequired.
     *
     * @return Value of confirmationRequired.
     */
    public void setConfirmationRequired(String confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    /**
     * Gets _alias.
     *
     * @return Value of _alias.
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets new _alias.
     *
     * @return Value of alias.
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getWriteInAlphabet() {
        return writeInAlphabet;
    }

    public void setWriteInAlphabet(String writeInAlphabet) {
        this.writeInAlphabet = writeInAlphabet;
    }

    public Properties getBallotBoxCertificateProperties() {
        return ballotBoxCertificateProperties;
    }

    public void setBallotBoxCertificateProperties(Properties ballotBoxCertificateProperties) {
        this.ballotBoxCertificateProperties = ballotBoxCertificateProperties;
    }
}
