/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.config.webapp.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.scytl.products.ov.config.spring.SecureLoggingConfig;
import com.scytl.products.ov.config.spring.SpringConfigServices;
import com.scytl.products.ov.config.spring.batch.CommonBatchInfrastructure;
import com.scytl.products.ov.config.webapp.mvc.commands.ballotbox.BallotBoxWebappAdapter;
import com.scytl.products.ov.config.webapp.mvc.commands.electionevent.ElectionEventWebappAdapter;
import com.scytl.products.ov.config.webapp.mvc.commands.voters.VotersWebappAdapter;
import com.scytl.products.ov.utils.ConfigObjectMapper;

@Configuration
@EnableWebMvc
@ComponentScan({"com.scytl.products.ov.config.webapp.mvc"})
@Import({SecureLoggingConfig.class, SpringConfigServices.class, CommonBatchInfrastructure.class})
public class WebAppConfig {

    @Bean
    public ElectionEventWebappAdapter electionEventWebappAdapter() {
        return new ElectionEventWebappAdapter();
    }

    @Bean
    public VotersWebappAdapter votersWebappAdapter(final ConfigObjectMapper mapper) {
        return new VotersWebappAdapter(mapper);
    }

    @Bean
    public BallotBoxWebappAdapter ballotBoxWebappAdapter() {
        return new BallotBoxWebappAdapter();
    }

}
