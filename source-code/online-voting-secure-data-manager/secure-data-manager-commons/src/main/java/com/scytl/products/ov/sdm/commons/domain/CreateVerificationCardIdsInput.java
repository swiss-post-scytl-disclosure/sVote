/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.sdm.commons.domain;

public class CreateVerificationCardIdsInput {

    private String verificationCardSetId;

    private int numberOfVerificationCardIds;

    private String electionEventId;

    public String getVerificationCardSetId() {
        return verificationCardSetId;
    }

    public void setVerificationCardSetId(String verificationCardSetId) {
        this.verificationCardSetId = verificationCardSetId;
    }

    public int getNumberOfVerificationCardIds() {
        return numberOfVerificationCardIds;
    }

    public void setNumberOfVerificationCardIds(int numberOfVerificationCardIds) {
        this.numberOfVerificationCardIds = numberOfVerificationCardIds;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public void setElectionEventId(String electionEventId) {
        this.electionEventId = electionEventId;
    }
}
