/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.crypto;

import java.security.KeyStore;

/**
 * A secure logger keystore.
 */
public interface SecureLoggerKeyStore {

  /**
   * Returns the JCE key store.
   *
   * @return the JCE key store.
   */
  KeyStore getKeyStore();

  /**
   * Returns whether the key store is for cipher only.
   *
   * @return the key store is for cipher only.
   */
  boolean isForCipherOnly();

  /**
   * Returns whether the key store is for signature only.
   *
   * @return the key store is for signature only.
   */
  boolean isForSignatureOnly();
}
