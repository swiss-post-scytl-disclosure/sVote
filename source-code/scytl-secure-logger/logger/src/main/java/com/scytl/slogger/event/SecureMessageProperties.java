/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.event;

/**
 * Constants to be used as keys in the wrapped message properties.
 */
public final class SecureMessageProperties {
  private SecureMessageProperties() {}

  /**
   * HMAC property.
   */
  public static final String HMAC = "HMAC";

  /**
   * HMAC of the previous entry.
   */
  public static final String PHMAC = "PHMAC";

  /**
   * Encrypted Session key property.
   */
  public static final String ENCRYPTED_SESSION_KEY = "ESK";

  /**
   * Liberated Session key property.
   */
  public static final String LIBERATED_SESSION_KEY = "LSK";

  /**
   * Signature property.
   */
  public static final String SIGNATURE = "SG";

  /**
   * Checkpoint property.
   */
  public static final String CHECKPOINT = "CP";

  /**
   * Number of the log lines.
   */
  public static final String NUMBER_LOG_LINES = "LS";

  /**
   * Log timer period in milliseconds.
   */
  public static final String TIMER_LOG_MILLISECONDS = "TL";

  /**
   * Timestamp as UTC time in milliseconds.
   */
  public static final String TIMESTAMP = "TS";
}
