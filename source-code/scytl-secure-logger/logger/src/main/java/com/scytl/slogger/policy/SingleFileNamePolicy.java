/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.policy;

import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;

/**
 * Single file name policy.
 */
public class SingleFileNamePolicy extends LogFileNamePolicy {

  /**
   * Constructor.
   *
   * @param baseFileName the base file name.
   */
  public SingleFileNamePolicy(final File baseFileName) {
    super(baseFileName);
  }

  @Override
  public File currentLogFile() throws LogFileNamePolicyException {

    return lastLogFile();
  }

  @Override
  public File lastLogFile() {
    if (getBaseFileName().exists()) {
      return getBaseFileName();
    }

    return null;
  }

  @Override
  public Iterator<File> logFiles() {
    File file = getBaseFileName();
    Collection<File> files = file.exists() ? singleton(file) : emptySet();
    return files.iterator();
  }

  @Override
  public File nextLogFile() {
    return getBaseFileName();
  }
}
