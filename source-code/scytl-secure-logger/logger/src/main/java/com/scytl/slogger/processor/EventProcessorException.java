/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor;

import com.scytl.slogger.SecureLoggerException;

/**
 * Exception thrown during event processor.
 */
public class EventProcessorException extends SecureLoggerException {

  private static final long serialVersionUID = 8018823613976540805L;

  /**
   * Constructor.
   */
  public EventProcessorException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message description message of the exception
   */
  public EventProcessorException(final String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param message description message of the exception
   * @param cause encapsulated source exception
   */
  public EventProcessorException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructor.
   *
   * @param cause encapsulated source exception
   */
  public EventProcessorException(final Throwable cause) {
    super(cause);
  }

}
