/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.event;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Decorator for the PatternLayout. Adds a new format method that allows to include or not include
 * the message properties.
 */
public class SecurePatternLayout extends PatternLayout {

  /**
   * Constructor.
   *
   * @param pattern the pattern
   */
  public SecurePatternLayout(final String pattern) {
    super(pattern);

  }

  @Override
  public synchronized String format(final LoggingEvent arg0) {
    // carriage return character removed to keep compatibility through windows and
    // linux
    return super.format(arg0).replaceAll("\r", "");
  }

  /**
   * Format a logging event with or without its message properties.
   *
   * @param loggingEvent the event
   * @param includeProperties the properties to include
   * @return the formatted event.
   */
  public synchronized String format(final SecureLoggingEvent loggingEvent,
      final boolean includeProperties) {
    return format(includeProperties ? loggingEvent : loggingEvent.removeProperties()); 
  }

  @Override
  public boolean ignoresThrowable() {
    return true;
  }
}
