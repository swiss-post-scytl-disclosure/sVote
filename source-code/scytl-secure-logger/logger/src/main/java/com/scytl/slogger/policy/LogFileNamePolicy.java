/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.policy;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Log file name policy.
 */
public abstract class LogFileNamePolicy {

  private final File baseFileName;

  public LogFileNamePolicy(final File baseFileName) {
    this.baseFileName = baseFileName.getAbsoluteFile();
  }

  /**
   * Tells the log file policy the current file won't be used anymore.
   *
   * @throws LogFileNamePolicyException failed to close the current file/
   */
  public void closeCurrentFile() throws LogFileNamePolicyException {}

  /**
   * Returns the current log file.
   *
   * @return the current log file
   * @throws LogFileNamePolicyException failed to return the current log file.
   */
  public abstract File currentLogFile() throws LogFileNamePolicyException;

  /**
   * Returns the base file name.
   *
   * @return the base file name.
   */
  public File getBaseFileName() {
    return baseFileName;
  }

  /**
   * Returns the existing last log file.
   *
   * @return the existing last log file.
   */
  public abstract File lastLogFile() throws LogFileNamePolicyException;

  /**
   * Returns a list with all the existing log files.
   *
   * @return all the existing log files
   */
  public abstract Iterator<File> logFiles() throws LogFileNamePolicyException;

  /**
   * Returns the name of the next log file
   *
   * @return the name of the next log file
   * @throws IOException I/O error occurred.
   */
  public abstract File nextLogFile() throws LogFileNamePolicyException;

}
