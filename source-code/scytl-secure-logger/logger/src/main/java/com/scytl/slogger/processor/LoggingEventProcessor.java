/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor;

import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecurePatternLayout;

import java.util.List;

/**
 * Interface to be implemented by all the processors used with the SecureFileAppender.
 */
public interface LoggingEventProcessor {

  /**
   * Method executed each time the appender is killed.
   *
   * @throws EventProcessorException failed to kill the processor.
   */
  void killProcessor() throws EventProcessorException;

  /**
   * Method executed each time the appender receives a logging event.
   *
   * @param loggingEvent logging event to be processed
   * @param layout layout used by the appender
   * @return list of the resulting logging events that will be serialized
   * @throws EventProcessorException failed to process the event.
   */
  List<SecureLoggingEvent> processEvent(SecureLoggingEvent loggingEvent, SecurePatternLayout layout)
      throws EventProcessorException;

  /**
   * Method executed each time the appender is started.
   *
   * @param layout layout used by the appender
   * @return list of the resulting logging events that will be serialized
   * @throws EventProcessorException failed to start the processor.
   */
  List<SecureLoggingEvent> startProcessor(SecurePatternLayout layout)
      throws EventProcessorException;

  /**
   * Method executed each time the appender is closed.
   *
   * @param layout layout used by the appender
   * @return list of the resulting logging events that will be serialized
   * @throws EventProcessorException failed to stop the processor.
   */
  List<SecureLoggingEvent> stopProcessor(SecurePatternLayout layout) throws EventProcessorException;

}
