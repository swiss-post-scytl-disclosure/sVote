/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.policy;

import com.scytl.slogger.SecureLoggerException;

@SuppressWarnings("serial")
public class LogFileNamePolicyException extends SecureLoggerException {

  public LogFileNamePolicyException() {
    super();
  }

  public LogFileNamePolicyException(final String message) {
    super(message);
  }

  public LogFileNamePolicyException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public LogFileNamePolicyException(final Throwable cause) {
    super(cause);
  }
}
