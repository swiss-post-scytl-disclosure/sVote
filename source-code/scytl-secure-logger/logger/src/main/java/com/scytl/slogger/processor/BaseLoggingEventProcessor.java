/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor;

import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecurePatternLayout;

import java.util.LinkedList;
import java.util.List;

public abstract class BaseLoggingEventProcessor implements LoggingEventProcessor {

  @Override
  public void killProcessor() throws EventProcessorException {

  }

  @Override
  public List<SecureLoggingEvent> processEvent(final SecureLoggingEvent loggingEvent,
      final SecurePatternLayout layout) throws EventProcessorException {

    List<SecureLoggingEvent> list = new LinkedList<>();
    list.add(loggingEvent);

    return list;
  }

  @Override
  public List<SecureLoggingEvent> startProcessor(final SecurePatternLayout layout)
      throws EventProcessorException {
    return new LinkedList<>();
  }

  @Override
  public List<SecureLoggingEvent> stopProcessor(final SecurePatternLayout layout)
      throws EventProcessorException {
    return new LinkedList<>();
  }

}
