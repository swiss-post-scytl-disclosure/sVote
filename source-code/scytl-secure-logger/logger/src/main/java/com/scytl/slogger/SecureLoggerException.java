/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger;

/**
 * Base exception for all the secure logger module exceptions.
 */
public class SecureLoggerException extends Exception {

  private static final long serialVersionUID = -365488835551826720L;

  /**
   * Constructor.
   */
  public SecureLoggerException() {}

  /**
   * Constructor.
   *
   * @param message description message of the exception
   */
  public SecureLoggerException(final String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param message description message of the exception
   * @param cause encapsulated source exception
   */
  public SecureLoggerException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructor.
   *
   * @param cause encapsulated source exception
   */
  public SecureLoggerException(final Throwable cause) {
    super(cause);
  }

}
