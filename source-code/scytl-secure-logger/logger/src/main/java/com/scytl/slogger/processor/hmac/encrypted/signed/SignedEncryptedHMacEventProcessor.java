/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor.hmac.encrypted.signed;

import com.scytl.slogger.crypto.SecureLoggerCryptographyProvider;
import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecurePatternLayout;
import com.scytl.slogger.processor.EventProcessorException;
import com.scytl.slogger.processor.hmac.encrypted.EncryptedHMacEventProcessor;
import com.scytl.slogger.processor.signature.SignatureEventProcessor;

import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.List;

import javax.crypto.SecretKey;

/**
 * EncryptedHMacEventProcessor that sign the secret key first log.
 */
public class SignedEncryptedHMacEventProcessor extends EncryptedHMacEventProcessor {

  private final SignatureEventProcessor signatureProcessor;

  /**
   * Constructor.
   *
   * @param currentHMac the current HMAC
   * @param secretKey the secret key
   * @param cipherPublicKey the public key
   * @param signatureProcessor the signature processor
   * @param cryptographyProvider the cryptography provider
   * @param numberLogLines the number of log lines
   * @param timerLogMilliseconds the timer log in milliseconds
   * @throws GeneralSecurityException failed to create a new instance
   */
  public SignedEncryptedHMacEventProcessor(byte[] currentHMac, SecretKey secretKey,
      PublicKey cipherPublicKey, SignatureEventProcessor signatureProcessor,
      SecureLoggerCryptographyProvider cryptographyProvider, int numberLogLines,
      long timerLogMilliseconds) throws GeneralSecurityException {
    super(currentHMac, secretKey, cipherPublicKey, cryptographyProvider, numberLogLines,
        timerLogMilliseconds);
    this.signatureProcessor = signatureProcessor;
  }

  @Override
  public List<SecureLoggingEvent> secretKeyLog(final SecurePatternLayout layout)
      throws EventProcessorException {
    List<SecureLoggingEvent> logEventList = super.secretKeyLog(layout);
    return signatureProcessor.processEvent(logEventList.get(0), layout);
  }
}
