/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.policy;

/**
 * Enum to select the policies easier.
 */
public enum LogFileNamePolicies {

  /**
   * Default policy.
   */
  DEFAULT(RollingSequentialFileNamePolicy.class),

  /**
   * Rolling sequential file name policy.
   */
  ROLLING_SEQUENTIAL_FILENAME(RollingSequentialFileNamePolicy.class),

  /**
   * Sequential date file name policy.
   */
  SEQUENTIAL_DATE_FILENAME(SequentialDateFileNamePolicy.class),

  /**
   * Sequential file name policy.
   */
  SEQUENTIAL_FILENAME(SequentialFileNamePolicy.class),

  /**
   * Single file name policy.
   */
  SINGLE_FILENAME(SingleFileNamePolicy.class);

  private final Class<? extends LogFileNamePolicy> policy;

  private LogFileNamePolicies(final Class<? extends LogFileNamePolicy> policy) {
    this.policy = policy;
  }

  /**
   * @return Returns the policy.
   */
  public Class<? extends LogFileNamePolicy> getPolicy() {
    return policy;
  }
}
