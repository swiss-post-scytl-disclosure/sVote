/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger;

/**
 * Rolling timer.
 */
public class RollingTimer {

  private long currentTime;

  private final long triggeringTime;

  /**
   * Constructor.
   *
   * @param triggeringTime the triggering time.
   */
  public RollingTimer(final long triggeringTime) {
    this.triggeringTime = triggeringTime;
  }

  /**
   * Returns whether the event is triggered.
   *
   * @return the event is triggered.
   */
  public boolean isTriggeringEvent() {
    if (triggeringTime <= 0) {
      return false;
    }
    return System.currentTimeMillis() - currentTime > triggeringTime;
  }

  /**
   * Starts the timer.
   */
  public void startTimer() {
    currentTime = System.currentTimeMillis();
  }
}
