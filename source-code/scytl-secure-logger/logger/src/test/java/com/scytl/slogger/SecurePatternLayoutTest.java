/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger;

import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecurePatternLayout;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

/**
 */
public class SecurePatternLayoutTest extends TestCase {

  public void testWrapperPatternLayout() {
    final String message = "message";
    LoggingEvent loggingEvent = new LoggingEvent(this.getClass().getCanonicalName(),
        Logger.getRootLogger(), Level.INFO, message, null);

    Map<String, String> mapProperties = new HashMap<>();
    mapProperties.put("key1", "value1");
    mapProperties.put("key2", "value2");
    SecureLoggingEvent wrapperLogging = 
        SecureLoggingEvent.newClientInstance(loggingEvent).addProperties(mapProperties);

    SecurePatternLayout layout = new SecurePatternLayout("%m");
    assertEquals(layout.format(wrapperLogging, false), message);
  }

}
