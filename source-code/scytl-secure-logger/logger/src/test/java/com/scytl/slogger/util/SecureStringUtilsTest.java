/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SecureStringUtilsTest {

  @Test
  public void testEscape() throws Exception {

    String escapedMessage = SecureStringUtils.escape("Log\n\r\n\b\fbnrf|");
    assertEquals("Log(n)(r)(n)(b)(f)bnrf|", escapedMessage);

  }
}
