/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger;

import com.scytl.slogger.util.Securities;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import java.io.File;

public abstract class AbstractSecureAppenderTest {

  protected static final String NAME = "AUDIT";

  protected static final String VERIFY_PATTERN =
      "TIMESTAMP LEVEL - CLASS (FILE:LINE) [THREAD] - MESSAGE";

  protected static final String PATTERN = "%d %p - %C (%F:%L) [%t] - %m%n";

  protected static final String SIGNATURE_PKCS12_PASSWORD = "GXXCNJH48X5F649VRY52";

  protected static final String CIPHER_PKCS12_PASSWORD = "649VRY52GXXCNJH48X5F";

  protected static final String SIGNATURE_PKCS12_FILE = "signatureKeyStore.p12";

  protected static final String CIPHER_PKCS12_FILE = "cipherKeyStore.p12";

  protected static final int TRIGGERING_TIME = 4000;

  protected static final int NUMBER_LOG_LINES = 100;

  protected static final String TEST_LOG = "audit.log";

  private final TemporaryFolder folder = new TemporaryFolder();

  protected SecureAppender secureAppender;

  protected File testLogFile;


  @Rule
  public TemporaryFolder getFolder() {
    return folder;
  }

  @Before
  public void setUp() throws Exception {
    Securities.addBouncyCastleProviderIfNecessary();
    testLogFile = new File(getFolder().getRoot(), TEST_LOG);
  }

  @After
  public void tearDown() throws Exception {
    try {
      if (secureAppender != null) {
        Logger.getRootLogger().removeAppender(secureAppender);
        secureAppender.close();
      }
    } finally {
      Securities.removeBouncyCastleProviderIfNecessary();
    }
  }
}
