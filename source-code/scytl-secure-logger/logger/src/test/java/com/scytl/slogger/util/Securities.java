/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.util;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Provider;
import java.security.Security;

/**
 * Utility class for managing security providers before and after tests.
 * 
 * @author aakimov
 */
public final class Securities {
  private static final Provider BC_PROVIDER = new BouncyCastleProvider();

  private Securities() {}

  /**
   * Adds Bouncy Castle provider if necessary.
   */
  public static void addBouncyCastleProviderIfNecessary() {
    if (Security.getProvider(BC_PROVIDER.getName()) == null) {
      Security.addProvider(BC_PROVIDER);
    }
  }

  /**
   * Removes Bouncy Castle provider if necessary.
   */
  public static void removeBouncyCastleProviderIfNecessary() {
    if (Security.getProvider(BC_PROVIDER.getName()) == BC_PROVIDER) {
      Security.removeProvider(BC_PROVIDER.getName());
    }
  }
}
