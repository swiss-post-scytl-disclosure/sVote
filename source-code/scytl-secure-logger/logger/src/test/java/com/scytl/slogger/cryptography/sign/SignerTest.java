/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.cryptography.sign;

import com.scytl.slogger.crypto.SecureLoggerCryptographyFactory;
import com.scytl.slogger.crypto.SecureLoggerCryptographyProvider;
import com.scytl.slogger.crypto.SecureLoggerKeyStore;
import com.scytl.slogger.util.Securities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.security.GeneralSecurityException;

/**
 * Test for SHA256Signer and P12KeyStorer
 */
public class SignerTest {

  private final static char[] KEY_STORE_PASSWORD = "GXXCNJH48X5F649VRY52".toCharArray();

  private final byte[] _what = new byte[] {0, 1, 2, 3, 4};

  private SecureLoggerCryptographyProvider provider;

  @Before
  public void setUp() throws GeneralSecurityException {
    Securities.addBouncyCastleProviderIfNecessary();
    provider = new SecureLoggerCryptographyFactory().build();
  }

  @After
  public void tearDown() throws GeneralSecurityException {
    Securities.removeBouncyCastleProviderIfNecessary();
  }
  
  @Test
  public void testSignAndVerify() throws Exception {

    final SecureLoggerKeyStore keyStore = provider.readKeyStore(
        Thread.currentThread().getContextClassLoader().getResourceAsStream("signatureKeyStore.p12"),
        KEY_STORE_PASSWORD);

    byte[] signed =
        provider.sign(_what, provider.getUserSignaturePrivateKey(keyStore, KEY_STORE_PASSWORD));

    provider.verify(_what, signed, provider.getUserSignaturePublicKey(keyStore));
  }
}
