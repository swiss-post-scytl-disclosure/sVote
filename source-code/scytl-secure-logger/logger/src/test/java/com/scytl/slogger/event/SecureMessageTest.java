/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.event;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.util.Map;

/**
 * Tests of {@link SecureMessage}.
 * 
 * @author aakimov
 */
public class SecureMessageTest {

  @Test
  public void testAddProperties() {
    SecureMessage original = SecureMessage.getHMacInstance();
    SecureMessage modified = original.addProperties(singletonMap("key1", "value1"))
        .addProperties(singletonMap("key2", "value1"))
        .addProperties(singletonMap("key2", "value2"));
    assertEquals(original.text(), modified.text());
    Map<String, String> properties = modified.properties();
    assertEquals(2, properties.size());
    assertEquals("value1", properties.get("key1"));
    assertEquals("value2", properties.get("key2"));
  }

  @Test
  public void testGetCheckpointInstance() {
    SecureMessage message = SecureMessage.getCheckpointInstance();
    assertEquals("Checkpoint Entry", message.text());
    assertTrue(message.isCheckpoint());
  }

  @Test
  public void testGetHMacInstance() {
    SecureMessage message = SecureMessage.getHMacInstance();
    assertEquals("New Secret Key generated.", message.text());
    assertTrue(message.properties().isEmpty());
  }

  @Test
  public void testIsCheckpoint() {
    SecureMessage message = SecureMessage.getHMacInstance();
    assertFalse(message.isCheckpoint());
    assertTrue(message.addProperties(singletonMap(SecureMessageProperties.CHECKPOINT, null))
        .isCheckpoint());
  }

  @Test
  public void testNewClientInstance() {
    String source = "Some\n\rtext. {*key1::value1,key2::\n\r,key3::value3,key4::,key5::value5*}";
    SecureMessage message = SecureMessage.newClientInstance(source);
    assertEquals("Some(n)(r)text.", message.text());
    Map<String, String> properties = message.properties();
    assertEquals("value1", properties.get("key1"));
    assertEquals("(n)(r)", properties.get("key2"));
    assertEquals("value3", properties.get("key3"));
    assertTrue(properties.containsKey("key4"));
    assertEquals("value5", properties.get("key5"));
  }

  @Test
  public void testNewFileInstance() {
    File file = new File("test.log");
    SecureMessage message = SecureMessage.newFileInstance(file);
    assertEquals("Log File Name: " + file.getAbsolutePath(), message.text());
    assertTrue(message.properties().isEmpty());
  }

  @Test
  public void testNewRenderedInstance() {
    String source = "Some\n\rtext. {*key1::value1,key2::\n\r,key3::value3,key4::,key5::value5*}";
    SecureMessage message = SecureMessage.newRenderedInstance(source);
    assertEquals("Some\n\rtext.", message.text());
    Map<String, String> properties = message.properties();
    assertEquals("value1", properties.get("key1"));
    assertEquals("\n\r", properties.get("key2"));
    assertEquals("value3", properties.get("key3"));
    assertTrue(properties.containsKey("key4"));
    assertEquals("value5", properties.get("key5"));
  }

  @Test
  public void testRemoveProperties() {
    SecureMessage original =
        SecureMessage.getHMacInstance().addProperties(singletonMap("key1", "value1"))
            .addProperties(singletonMap("key2", "value2"));
    SecureMessage modified = original.removeProperties();
    assertEquals(original.text(), modified.text());
    assertTrue(modified.properties().isEmpty());
  }

  @Test
  public void testRemoveProperty() {
    SecureMessage original =
        SecureMessage.getHMacInstance().addProperties(singletonMap("key1", "value1"))
            .addProperties(singletonMap("key2", "value2"));
    SecureMessage modified = original.removeProperty("key1").removeProperty("key3");
    Map<String, String> properties = modified.properties();
    assertEquals(1, properties.size());
    assertEquals("value2", properties.get("key2"));
  }

  @Test
  public void testRender() {
    String source = "Some\n\rtext. {*key1::value1,key2::\n\r,key3::value3,key4::,key5::value5*}";
    SecureMessage message = SecureMessage.newRenderedInstance(source);
    assertEquals(source, message.render());
  }
}
