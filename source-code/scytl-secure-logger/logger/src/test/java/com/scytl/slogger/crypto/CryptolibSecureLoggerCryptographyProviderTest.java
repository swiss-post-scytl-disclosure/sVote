/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.crypto;

import com.scytl.slogger.util.Securities;

import org.junit.Assert;
import org.junit.Test;

import java.security.GeneralSecurityException;

import javax.crypto.SecretKey;

public class CryptolibSecureLoggerCryptographyProviderTest {

  @Test
  public void testGenerateSymmetricKey() throws GeneralSecurityException {
    Securities.addBouncyCastleProviderIfNecessary();
    try {
      SecureLoggerCryptographyProvider provider =
          new SecureLoggerCryptographyFactory().build();
      SecretKey key = provider.generateSymmetricKey();
      Assert.assertEquals("Key length should be 256 bits", 256, key.getEncoded().length * 8);
    } finally {
      Securities.removeBouncyCastleProviderIfNecessary();
    }
  }
}
