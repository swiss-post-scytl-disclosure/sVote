/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 *
 */
public class Base64P12FileTest extends AbstractSecureAppenderTest {

  @Test
  public void testInitLogger() throws Exception {

    secureAppender = new SecureAppender();

    secureAppender.setName(NAME);
    secureAppender.setFile(testLogFile.getAbsolutePath());

    secureAppender.setPattern(PATTERN);
    secureAppender.setVerifyPattern(VERIFY_PATTERN);

    secureAppender.setNumberLogLines(NUMBER_LOG_LINES);
    secureAppender.setTimerLogMilliseconds(0);
    secureAppender.setTriggeringTime(TRIGGERING_TIME);

    secureAppender.setSignaturePkcs12FileName("signatureKeyStoreBase64.p12");
    secureAppender.setCipherPkcs12FileName("cipherKeyStoreBase64.p12");

    secureAppender.setSignaturePkcs12Password(SIGNATURE_PKCS12_PASSWORD);
    secureAppender.setCipherPkcs12Password(CIPHER_PKCS12_PASSWORD);

    secureAppender.activateOptions();

    Logger logger = Logger.getRootLogger();
    logger.setLevel(Level.INFO);
    logger.addAppender(secureAppender);
    logger.info("The secure logger is activated now");
    assertEquals("Directory should contain a file", 1, getFolder().getRoot().list().length);
  }
}
