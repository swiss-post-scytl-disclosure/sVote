/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.crypto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.scytl.slogger.AbstractSecureAppenderTest;

import org.junit.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class DefaultSecureLoggerKeyStoreTest extends AbstractSecureAppenderTest {

  @Test
  public void testIsForCipher() throws Exception {
    SecureLoggerKeyStore cipherKeyStore = loadKeyStore(CIPHER_PKCS12_FILE, CIPHER_PKCS12_PASSWORD);
    assertTrue(cipherKeyStore.isForCipherOnly());
  }

  @Test
  public void testIsForCipherWithSignatureKey() throws Exception {
    SecureLoggerKeyStore signatureKeyStore =
        loadKeyStore(SIGNATURE_PKCS12_FILE, SIGNATURE_PKCS12_PASSWORD);
    assertFalse(signatureKeyStore.isForCipherOnly());
  }

  @Test
  public void testIsForSignature() throws Exception {
    SecureLoggerKeyStore signatureKeyStore =
        loadKeyStore(SIGNATURE_PKCS12_FILE, SIGNATURE_PKCS12_PASSWORD);
    assertTrue(signatureKeyStore.isForSignatureOnly());
  }

  @Test
  public void testIsForSignatureWithCipherKey() throws Exception {
    SecureLoggerKeyStore cipherKeyStore = loadKeyStore(CIPHER_PKCS12_FILE, CIPHER_PKCS12_PASSWORD);
    assertFalse(cipherKeyStore.isForSignatureOnly());
  }

  private SecureLoggerKeyStore loadKeyStore(String pkcs12File, String pkcs12Password)
      throws IOException, GeneralSecurityException {
    return new SecureLoggerCryptographyFactory().build().readKeyStore(
        Thread.currentThread().getContextClassLoader().getResourceAsStream(pkcs12File),
        pkcs12Password.toCharArray());
  }
}
