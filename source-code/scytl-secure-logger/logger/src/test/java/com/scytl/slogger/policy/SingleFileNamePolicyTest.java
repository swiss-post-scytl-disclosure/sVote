/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.policy;

import com.scytl.slogger.CleanDirProvider;

import java.io.File;
import java.util.Iterator;

import junit.framework.TestCase;

public class SingleFileNamePolicyTest extends TestCase {

  private static final String LOG_FILE = "single-test.log";

  private File _baseLogFile;

  public void testFilePolicy() throws Exception {
    SingleFileNamePolicy policy = new SingleFileNamePolicy(_baseLogFile);

    // No log files
    assertFalse(policy.logFiles().hasNext());
    assertEquals(policy.nextLogFile(), _baseLogFile);
    assertNull(policy.lastLogFile());

    // Log files
    assertTrue(policy.nextLogFile().createNewFile());

    Iterator<File> logFiles = policy.logFiles();
    assertTrue(logFiles.hasNext());
    assertEquals(logFiles.next(), _baseLogFile);
    assertEquals(policy.lastLogFile(), _baseLogFile);

    assertFalse(logFiles.hasNext());

  }

  @Override
  protected void setUp() throws Exception {
    _baseLogFile = new File(CleanDirProvider.getCleanedTmpDir().getAbsolutePath(), LOG_FILE);
  }

  @Override
  protected void tearDown() throws Exception {
    CleanDirProvider.deleteTmpDir();
  }

}
