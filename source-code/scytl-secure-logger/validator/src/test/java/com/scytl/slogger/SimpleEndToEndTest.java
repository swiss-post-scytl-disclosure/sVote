/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger;

import static java.nio.file.Files.createTempDirectory;

import com.scytl.slogger.policy.LogFileNamePolicies;
import com.scytl.slogger.policy.SequentialDateFileNamePolicy;
import com.scytl.slogger.util.LogValidator;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Simple E2E test.
 *
 * @author aakimov
 */
public class SimpleEndToEndTest {
  private static final String NAME = "AUDIT";

  private static final String VERIFY_PATTERN =
      "TIMESTAMP LEVEL - CLASS (FILE:LINE) [THREAD] - MESSAGE";

  private static final String PATTERN = "%d %p - %C (%F:%L) [%t] - %m%n";

  private static final String SIGNATURE_PKCS12_PASSWORD = "GXXCNJH48X5F649VRY52";

  private static final String CIPHER_PKCS12_PASSWORD = "649VRY52GXXCNJH48X5F";

  private static final String SIGNATURE_PKCS12_FILE = "signatureKeyStore.p12";

  private static final String CIPHER_PKCS12_FILE = "cipherKeyStore.p12";

  private static final String FILE_NAME = "test-audit.log";

  /**
   * The main method.
   *
   * @param args
   * @throws IOException
   * @throws SecureLoggerException
   */
  public static void main(String[] args) throws IOException, SecureLoggerException {
    Path folder = createTempDirectory("slogger");
    Path file = generateLogFile(folder);
    validateLogFile(file);
    System.err.println(folder);
  }

  private static Path generateLogFile(Path folder) throws SecureLoggerException {
    Path file = folder.resolve(FILE_NAME);

    SecureAppender appender = new SecureAppender();
    appender.setName(NAME);
    appender.setPattern(PATTERN);
    appender.setVerifyPattern(VERIFY_PATTERN);
    appender.setFile(file.toString());
    appender.setEncoding(StandardCharsets.UTF_8.name());
    appender.setNumberLogLines(100);
    appender.setTimerLogMilliseconds(4000);
    appender.setTriggeringTime(1000);
    appender.setVerifyPattern(VERIFY_PATTERN);
    appender.setLogFileNamePolicyName(LogFileNamePolicies.SEQUENTIAL_DATE_FILENAME.name());
    appender.setSignaturePkcs12FileName(SIGNATURE_PKCS12_FILE);
    appender.setCipherPkcs12FileName(CIPHER_PKCS12_FILE);
    appender.setSignaturePkcs12Password(SIGNATURE_PKCS12_PASSWORD);
    appender.setCipherPkcs12Password(CIPHER_PKCS12_PASSWORD);

    appender.activateOptions();

    Logger logger = Logger.getRootLogger();
    logger.setLevel(Level.INFO);
    logger.addAppender(appender);
    logger.info("Test message");

    appender.close();

    return file;
  }

  private static void validateLogFile(Path file) {
    String certificateFile = SimpleEndToEndTest.class.getResource("/test.crt").getFile();
    String[] args = {"-c", certificateFile, "-l", file.toString(), "-e",
        StandardCharsets.UTF_8.name(), "-p", PATTERN, "-v", VERIFY_PATTERN, "-o",
        file.getParent().toString(), "-n", SequentialDateFileNamePolicy.class.getName()};
    LogValidator.main(args);
  }
}
