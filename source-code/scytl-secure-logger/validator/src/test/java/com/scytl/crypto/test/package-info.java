/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

/**
 * Package that contains cryptography-related classes used in tests. At some
 * point these should be moved to their own module in the Cryptolib.
 */
package com.scytl.crypto.test;
