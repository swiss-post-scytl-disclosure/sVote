/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.spi.LocationInfo;
import org.junit.Test;

/**
 * Tests of {@link ValidatorLocationInfo}.
 * 
 * @author aakimov
 */
public class ValidatorLocationInfoTest {
  private static final String LINE_NUMBER = "lineNumber";
  private static final String FILE_NAME = "fileName";
  private static final String METHOD_NAME = "methodName";
  private static final String CLASS_NAME = "className";

  @Test
  public void testGetClassName() {
    ValidatorLocationInfo info =
        new ValidatorLocationInfo(CLASS_NAME, METHOD_NAME, FILE_NAME, LINE_NUMBER);
    assertEquals(CLASS_NAME, info.getClassName());

    info = new ValidatorLocationInfo(null, METHOD_NAME, FILE_NAME, LINE_NUMBER);
    assertEquals(LocationInfo.NA, info.getClassName());
  }

  @Test
  public void testGetFileName() {
    ValidatorLocationInfo info =
        new ValidatorLocationInfo(CLASS_NAME, METHOD_NAME, FILE_NAME, LINE_NUMBER);
    assertEquals(FILE_NAME, info.getFileName());

    info = new ValidatorLocationInfo(CLASS_NAME, METHOD_NAME, null, LINE_NUMBER);
    assertEquals(LocationInfo.NA, info.getFileName());
  }

  @Test
  public void testGetLineNumber() {
    ValidatorLocationInfo info =
        new ValidatorLocationInfo(CLASS_NAME, METHOD_NAME, FILE_NAME, LINE_NUMBER);
    assertEquals(LINE_NUMBER, info.getLineNumber());

    info = new ValidatorLocationInfo(CLASS_NAME, METHOD_NAME, FILE_NAME, null);
    assertEquals(LocationInfo.NA, info.getLineNumber());
  }

  @Test
  public void testGetMethodName() {
    ValidatorLocationInfo info =
        new ValidatorLocationInfo(CLASS_NAME, METHOD_NAME, FILE_NAME, LINE_NUMBER);
    assertEquals(METHOD_NAME, info.getMethodName());

    info = new ValidatorLocationInfo(CLASS_NAME, null, FILE_NAME, LINE_NUMBER);
    assertEquals(LocationInfo.NA, info.getMethodName());
  }
}
