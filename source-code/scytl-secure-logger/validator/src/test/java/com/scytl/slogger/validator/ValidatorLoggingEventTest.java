/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.spi.LocationInfo;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests of {@link ValidatorLoggingEvent}.
 * 
 * @author aakimov
 */
public class ValidatorLoggingEventTest {
  private static final LocationInfo LOCATION_INFORMATION =
      new ValidatorLocationInfo("className", "methodName", "fileName", "lineNumber");
  private static final String LOGGER_NAME = "loggerName";
  private static final String MESSAGE = "message";
  private static final long TIME_STAMP = 1;
  private static final String THREAD_NAME = "threadName";
  private static final String NDC = "NDC";
  private ValidatorLoggingEvent event;

  @Before
  public void setUp() {
    event = new ValidatorLoggingEvent.WellFormedInstanceBuilder().setLevel(Level.WARN)
        .setLocationInformation(LOCATION_INFORMATION).setLoggerName(LOGGER_NAME).setMessage(MESSAGE)
        .setNdc(NDC).setThreadName(THREAD_NAME).setTimeStamp(TIME_STAMP).build();
  }

  @Test
  public void testNewMalformedInstance() {
    event = ValidatorLoggingEvent.newMalformedInstance(MESSAGE);
    assertEquals(MESSAGE, event.getRenderedMessage());
    assertTrue(event.isMalformed());
  }

  @Test
  public void testGetLocationInformation() {
    assertEquals(LOCATION_INFORMATION, event.getLocationInformation());
  }

  @Test
  public void testGetNDC() {
    assertEquals(NDC, event.getNDC());
  }

  @Test
  public void testGetRenderedMessage() {
    assertEquals(MESSAGE, event.getRenderedMessage());
  }

  @Test
  public void testGetThreadName() {
    assertEquals(THREAD_NAME, event.getThreadName());
  }

  @Test
  public void testIsMalformed() {
    assertFalse(event.isMalformed());
  }
}
