/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.util;

/**
 *
 */
public class LogValidatorBinaryP12Test extends LogValidatorTest {

  /**
   * @see com.scytl.slogger.util.LogValidatorTest#getBaseFilename()
   */
  @Override
  protected String getBaseFilename() {
    return "testFile.log";
  }

  /**
   * @see com.scytl.slogger.util.LogValidatorTest#getCertificate()
   */
  @Override
  protected String getCertificate() {
    return "test.crt";
  }

  /**
   * @see com.scytl.slogger.util.LogValidatorTest#getLastLogFile()
   */
  @Override
  protected String getLastLogFile() {

    return "testFile-0.log";
  }
}
