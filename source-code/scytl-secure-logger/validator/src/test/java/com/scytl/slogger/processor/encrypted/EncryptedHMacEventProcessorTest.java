/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor.encrypted;

import com.scytl.slogger.processor.LoggingEventProcessor;
import com.scytl.slogger.processor.ProcessorTester;
import com.scytl.slogger.processor.hmac.encrypted.EncryptedHMacEventProcessor;
import com.scytl.slogger.validator.LoggingEventValidator;
import com.scytl.slogger.validator.hmac.encrypted.EncryptedHMacEventValidator;

public class EncryptedHMacEventProcessorTest extends ProcessorTester {

  @Override
  protected LoggingEventProcessor getLoggingEventProcessor() throws Exception {
    return new EncryptedHMacEventProcessor(new byte[0], null, getCipherPublicKey(),
        _cryptographyProvider, NUMBER_LOG_LINES, TIMER_LOG_MILLISECONDS);
  }

  @Override
  protected LoggingEventValidator getLoggingEventValidator() throws Exception {
    return new EncryptedHMacEventValidator(getCipherPrivateKey(), getCryptographyProvider());
  }

}
