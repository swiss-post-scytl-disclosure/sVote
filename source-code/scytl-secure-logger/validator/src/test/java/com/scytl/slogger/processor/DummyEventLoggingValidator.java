/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor;

import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecurePatternLayout;
import com.scytl.slogger.validator.EventValidatorException;
import com.scytl.slogger.validator.LoggingEventValidator;

/**
 *
 */
public class DummyEventLoggingValidator implements LoggingEventValidator {

  @Override
  public void stopValidation(SecurePatternLayout layout) throws EventValidatorException {

  }

  /**
   * @see com.scytl.slogger.validator.LoggingEventValidator#validateEvent(com.scytl.slogger.event.SecureLoggingEvent,
   *      com.scytl.slogger.event.SecurePatternLayout)
   */
  @Override
  public void validateEvent(final SecureLoggingEvent event, final SecurePatternLayout layout)
      throws EventValidatorException {

  }

}
