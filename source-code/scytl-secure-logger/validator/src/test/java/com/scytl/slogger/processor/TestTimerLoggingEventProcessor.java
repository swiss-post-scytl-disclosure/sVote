/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor;

import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecureMessage;
import com.scytl.slogger.event.SecurePatternLayout;
import com.scytl.slogger.validator.LoggingEventValidator;

import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class TestTimerLoggingEventProcessor extends ProcessorTester {

  private static final long TIMER_MS = 50;

  /**
   * @throws Exception
   */
  public void testProcessor() throws Exception {
    _logger.info("Log1");
    String logLine = _logFileReader.readLine();
    assertEquals(logLine, "Log1");

    _logger.info("Log2");
    logLine = _logFileReader.readLine();
    assertEquals(logLine, "Log2");

    logLine = _logFileReader.readLine();
    while (logLine == null) {
      logLine = _logFileReader.readLine();
    }
    assertEquals(SecureMessage.newRenderedInstance(logLine).text(),
        TimerLoggingEventProcessor.CHECKPOINT_ENTRY);

  }

  /**
   * @see com.scytl.slogger.processor.ProcessorTester#getLoggingEventProcessor()
   */
  @Override
  protected LoggingEventProcessor getLoggingEventProcessor() {
    return new LoggingEventProcessor() {

      TimerLoggingEventProcessor _processor =
          new TimerLoggingEventProcessor(_secureAppender, TIMER_MS);

      @Override
      public void killProcessor() throws EventProcessorException {}

      @Override
      public List<SecureLoggingEvent> processEvent(final SecureLoggingEvent loggingEvent,
          final SecurePatternLayout layout) throws EventProcessorException {
        List<SecureLoggingEvent> loggingEventList = new LinkedList<>();
        loggingEventList.add(loggingEvent);

        return loggingEventList;
      }

      @Override
      public List<SecureLoggingEvent> startProcessor(final SecurePatternLayout layout)
          throws EventProcessorException {
        return _processor.startProcessor(layout);
      }

      @Override
      public List<SecureLoggingEvent> stopProcessor(final SecurePatternLayout layout)
          throws EventProcessorException {
        return _processor.stopProcessor(layout);
      }

    };
  }

  @Override
  protected LoggingEventValidator getLoggingEventValidator() throws Exception {
    return new DummyEventLoggingValidator();
  }
}
