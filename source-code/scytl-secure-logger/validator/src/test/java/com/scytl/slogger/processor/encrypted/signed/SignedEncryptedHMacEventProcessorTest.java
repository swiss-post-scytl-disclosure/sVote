/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor.encrypted.signed;

import com.scytl.slogger.processor.LoggingEventProcessor;
import com.scytl.slogger.processor.ProcessorTester;
import com.scytl.slogger.processor.hmac.encrypted.signed.SignedEncryptedHMacEventProcessor;
import com.scytl.slogger.processor.signature.SignatureEventProcessor;
import com.scytl.slogger.validator.LoggingEventValidator;
import com.scytl.slogger.validator.hmac.encrypted.signed.SignedEncryptedHMacEventValidator;
import com.scytl.slogger.validator.signature.SignatureEventValidator;

public class SignedEncryptedHMacEventProcessorTest extends ProcessorTester {

  @Override
  protected LoggingEventProcessor getLoggingEventProcessor() throws Exception {
    return new SignedEncryptedHMacEventProcessor(new byte[0], null, getCipherPublicKey(),
        new SignatureEventProcessor(getSignaturePrivateKey(), _cryptographyProvider),
        _cryptographyProvider, NUMBER_LOG_LINES, TIMER_LOG_MILLISECONDS);
  }

  @Override
  protected LoggingEventValidator getLoggingEventValidator() throws Exception {
    return new SignedEncryptedHMacEventValidator(
        new SignatureEventValidator(getSignaturePublicKey(), getCryptographyProvider()),
        getCipherPrivateKey(), getCryptographyProvider());
  }

}
