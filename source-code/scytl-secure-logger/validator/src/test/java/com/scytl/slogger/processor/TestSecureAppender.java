/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor;

import com.scytl.slogger.SecureFileAppender;
import com.scytl.slogger.policy.LogFileNamePolicies;

import java.io.IOException;

/**
 *
 */
public class TestSecureAppender extends SecureFileAppender {

  public TestSecureAppender() {
    super();
  }

  /**
   * @param filename
   * @throws IOException
   */
  public TestSecureAppender(final String filename) throws IOException {
    super();
    setPattern("%m%n");
    setFile(filename);
    setLogFileNamePolicyName(LogFileNamePolicies.SEQUENTIAL_DATE_FILENAME.name());
    setVerifyPattern("MESSAGE");
  }

  @Override
  public void activateOptions() {
    super.activateOptions();
    try {
      appendHeader();
    } catch (EventProcessorException e) {
      throw new AssertionError(e);
    }
  }
}
