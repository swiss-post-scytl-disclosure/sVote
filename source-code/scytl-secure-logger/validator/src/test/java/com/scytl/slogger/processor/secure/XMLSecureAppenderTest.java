/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor.secure;

import com.scytl.slogger.SecureFileAppender;
import com.scytl.slogger.XMLAppenderTester;
import com.scytl.slogger.validator.EventValidatorException;
import com.scytl.slogger.validator.LogFilePatternReceiver;
import com.scytl.slogger.validator.ValidatorHelper;

import org.apache.log4j.LogManager;

import java.net.URL;

/**
 *
 */
public class XMLSecureAppenderTest extends XMLAppenderTester {

  /**
   * @see com.scytl.slogger.XMLAppenderTester#getXMLConfigURL()
   */
  @Override
  protected URL getXMLConfigURL() {
    return Thread.currentThread().getContextClassLoader().getResource("log4j-test.xml");
  }

  @Override
  protected void verifyLog(final SecureFileAppender secureAppender) throws EventValidatorException {
    LogManager.shutdown();
    ValidatorHelper.validateLog(secureAppender, LogFilePatternReceiver.DEFAULT_TIMESTAMP_FORMAT);
  }

}
