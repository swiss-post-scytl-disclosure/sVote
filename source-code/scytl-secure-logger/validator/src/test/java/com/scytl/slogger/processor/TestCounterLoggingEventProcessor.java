/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.processor;

import com.scytl.slogger.validator.LoggingEventValidator;

/**
 *
 */
public class TestCounterLoggingEventProcessor extends ProcessorTester {

  private static final int LOG_LINES = 5;

  /**
   * @throws Exception
   */
  public void testProcessor() throws Exception {
    for (int i = 0; i < LOG_LINES; i++) {
      _logger.info("Log" + i);
    }

    assertEquals(_logFileReader.readLine(), "Log" + (LOG_LINES - 1));

  }

  /**
   * @see com.scytl.slogger.processor.ProcessorTester#getLoggingEventProcessor()
   */
  @Override
  protected LoggingEventProcessor getLoggingEventProcessor() {
    return new CounterLoggingEventProcessor(LOG_LINES, new DummyEventLoggingProcessor());
  }

  @Override
  protected LoggingEventValidator getLoggingEventValidator() throws Exception {
    return new DummyEventLoggingValidator();
  }
}
