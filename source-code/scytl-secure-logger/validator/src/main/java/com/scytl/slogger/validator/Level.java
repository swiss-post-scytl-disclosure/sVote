/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

@SuppressWarnings("serial")
public class Level extends org.apache.log4j.Level {

  /**
   * The unknown level.
   */
  public static final Level UNKNOWN = new Level(5000, "**UNKNOWN**", 0);

  /**
   * Constructor.
   *
   * @param level the level
   * @param levelStr the level string
   * @param syslogEquivalent the syslog equivalent
   */
  protected Level(final int level, final String levelStr, final int syslogEquivalent) {
    super(level, levelStr, syslogEquivalent);
  }

  /**
   * Converts a given level to {@link org.apache.log4j.Level} instance.
   *
   * @param level the level
   * @return the instane.
   */
  public static org.apache.log4j.Level toLevel(final String level) {

    if (level == null) {
      return DEBUG;
    }

    org.apache.log4j.Level result;

    if (level.equals("ALL")) {
      result = ALL;
    } else if (level.equals("TRACE")) {
      result = TRACE;
    } else if (level.equals("DEBUG")) {
      result = DEBUG;
    } else if (level.equals("INFO")) {
      result = INFO;
    } else if (level.equals("WARN")) {
      result = WARN;
    } else if (level.equals("ERROR")) {
      result = ERROR;
    } else if (level.equals("FATAL")) {
      result = FATAL;
    } else if (level.equals("OFF")) {
      result = OFF;
    } else {
      result = UNKNOWN;
    }

    return result;
  }
}
