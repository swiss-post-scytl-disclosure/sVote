/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

import com.scytl.slogger.SecureFileAppender;
import com.scytl.slogger.event.SecurePatternLayout;
import com.scytl.slogger.policy.LogFileNamePolicy;
import com.scytl.slogger.policy.LogFileNamePolicyException;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Utility class to verify a given log.
 */
public final class ValidatorHelper {

  private ValidatorHelper() {}

  /**
   * Validates the lof
   *
   * @param logFileNamePolicy the log file name policy
   * @param pattern the pattern
   * @param verifierPattern the verifier pattern
   * @param validator the validator
   * @param encoding the encoding
   * @param timestampFormat the timestamp format
   * @throws EventValidatorException validation failed.
   */
  public static void validateLog(final LogFileNamePolicy logFileNamePolicy, final String pattern,
      final String verifierPattern, final LoggingEventValidator validator, final String encoding,
      final String timestampFormat) throws EventValidatorException {
    try {

      for (Iterator<File> iterator = logFileNamePolicy.logFiles(); iterator.hasNext();) {

        String fileName = iterator.next().toURI().toURL().toString();

        validateSingleLogFile(pattern, verifierPattern, validator, fileName, encoding,
            timestampFormat);

      }
      validator.stopValidation(new SecurePatternLayout(pattern));
    } catch (LogFileNamePolicyException | IOException e) {
      throw new EventValidatorException(e);
    }
  }

  /**
   * Validates the log
   *
   * @param logFileNamePolicy the log file name policy
   * @param pattern the pattern
   * @param verifierPattern the verifier pattern
   * @param encoding the encoding
   * @param timestampFormat the timestamp format
   * @throws EventValidatorException validation failed.
   */
  public static void validateLog(final LogFileNamePolicy logFileNamePolicy, final String pattern,
      final String verifierPattern, final String encoding, final String timestampFormat)
      throws EventValidatorException {
    LoggingEventValidator validator = new ValidatorFactory().createValidator();
    validateLog(logFileNamePolicy, pattern, verifierPattern, validator, encoding, timestampFormat);

  }

  /**
   * Validates the log.
   *
   * @param secureAppender the secure appender
   * @param loggingEventValidator the event validator
   * @param timestampFormat the timestamp format
   * @throws EventValidatorException validation failed.
   */
  public static void validateLog(final SecureFileAppender secureAppender,
      final LoggingEventValidator loggingEventValidator, final String timestampFormat)
      throws EventValidatorException {
    validateLog(secureAppender.getLogFileNamePolicy(), secureAppender.getPattern(),
        secureAppender.getVerifyPattern(), loggingEventValidator, secureAppender.getEncoding(),
        timestampFormat);
  }

  /**
   * Validates the log.
   *
   * @param secureAppender the secure appender
   * @param timestampFormat the timestamp format
   * @throws EventValidatorException validation failed.
   */
  public static void validateLog(final SecureFileAppender secureAppender,
      final String timestampFormat) throws EventValidatorException {
    validateLog(secureAppender.getLogFileNamePolicy(), secureAppender.getPattern(),
        secureAppender.getVerifyPattern(), secureAppender.getEncoding(), timestampFormat);
  }

  private static void validateSingleLogFile(final String pattern, final String verifierPattern,
      final LoggingEventValidator validator, final String fileName, final String encoding,
      final String timestampFormat) throws EventValidatorException {
    ValidatorLogFilePatternReceiver rec =
        new ValidatorLogFilePatternReceiver(validator, new SecurePatternLayout(pattern));
    rec.setTimestampFormat(timestampFormat);
    rec.setFileUrl(fileName);
    rec.setEncoding(encoding);
    rec.setLogFormat(verifierPattern);
    rec.syncActivateOptions();
    EventValidatorException exception = rec.getException();
    if (exception != null) {
      throw exception;
    }
  }
}
