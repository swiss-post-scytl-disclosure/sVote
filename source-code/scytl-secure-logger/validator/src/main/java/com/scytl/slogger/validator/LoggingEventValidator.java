/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecurePatternLayout;

/**
 * Interface to be implemented by all the validators used with the SecureFileAppender.
 */
public interface LoggingEventValidator {

  /**
   * Method executed when the validator process reaches the end of the log file
   *
   * @param layout the layout
   * @throws EventValidatorException failed to stop validation.
   */
  void stopValidation(SecurePatternLayout layout) throws EventValidatorException;

  /**
   * Method executed each time a logging event is read.
   *
   * @param event event to verify
   * @param layout layout used by the appender
   * @throws EventValidatorException validation failed.
   */
  void validateEvent(SecureLoggingEvent event, SecurePatternLayout layout)
      throws EventValidatorException;

}
