/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

import com.scytl.slogger.SecureLoggerException;

import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Exception thrown during event validator.
 */
public class EventValidatorException extends SecureLoggerException {

  private static final long serialVersionUID = 278552338087420970L;

  /**
   * Constructor.
   *
   * @param message description message of the exception
   */
  public EventValidatorException(final String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param message description message of the exception
   */
  public EventValidatorException(final String message, final LoggingEvent event,
      final Layout layout) {
    super(message + " --> " + layout.format(event));
  }
  
  /**
   * Constructor.
   *
   * @param message description message of the exception
   */
  public EventValidatorException(final String message, final LoggingEvent event,
      final Layout layout, Throwable cause) {
    super(message + " --> " + layout.format(event), cause);
  }

  /**
   * Constructor.
   *
   * @param message description message of the exception
   * @param cause encapsulated source exception
   */
  public EventValidatorException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructor.
   *
   * @param cause encapsulated source exception
   */
  public EventValidatorException(final Throwable cause) {
    super(cause);
  }

}
