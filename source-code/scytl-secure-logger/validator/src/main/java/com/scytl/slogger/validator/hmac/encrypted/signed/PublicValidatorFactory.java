/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator.hmac.encrypted.signed;

import com.scytl.slogger.crypto.SecureLoggerCryptographyProvider;
import com.scytl.slogger.validator.EventValidatorException;
import com.scytl.slogger.validator.LoggingEventValidator;
import com.scytl.slogger.validator.ValidatorFactory;
import com.scytl.slogger.validator.signature.SignatureEventValidator;

import java.io.BufferedWriter;
import java.security.PublicKey;

/**
 * Public validator factory.
 */
public class PublicValidatorFactory extends ValidatorFactory {

  private final PublicKey publicKey;

  private final BufferedWriter okWriter;

  private final BufferedWriter warnWriter;

  private final BufferedWriter errorWriter;

  private final SecureLoggerCryptographyProvider cryptographyProvider;

  /**
   * Constructor.
   *
   * @param publicKey the public key
   * @param okWriter the OK writer
   * @param warnWriter the warning writer
   * @param errorWriter the rror writer
   * @param cryptographyProvider the cryptography provider.
   */
  public PublicValidatorFactory(final PublicKey publicKey, final BufferedWriter okWriter,
      final BufferedWriter warnWriter, final BufferedWriter errorWriter,
      final SecureLoggerCryptographyProvider cryptographyProvider) {
    this.publicKey = publicKey;
    this.okWriter = okWriter;
    this.errorWriter = errorWriter;
    this.warnWriter = warnWriter;
    this.cryptographyProvider = cryptographyProvider;
  }

  @Override
  public LoggingEventValidator createValidator() throws EventValidatorException {
    SignatureEventValidator signatureEventValidator =
        new SignatureEventValidator(publicKey, cryptographyProvider);
    PublicSignedEncryptedHMacEventValidator macEventValidator =
        new PublicSignedEncryptedHMacEventValidator(signatureEventValidator, okWriter, warnWriter,
            errorWriter, cryptographyProvider);
    return macEventValidator;
  }
}
