/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

import org.apache.log4j.spi.LocationInfo;

/**
 * Extension of {@link LocationInfo} to be used in the secure log validation.
 * 
 * @author aakimov
 */
@SuppressWarnings("serial")
public class ValidatorLocationInfo extends LocationInfo {
  private final String className;
  private final String methodName;
  private final String fileName;
  private final String lineNumber;

  /**
   * Constructor.
   * 
   * @param className the class name
   * @param methodName the method name
   * @param fileName the file name
   * @param lineNumber the line number
   */
  public ValidatorLocationInfo(String className, String methodName, String fileName,
      String lineNumber) {
    super(null, "");
    this.lineNumber = lineNumber;
    this.fileName = fileName;
    this.className = className;
    this.methodName = methodName;
  }

  @Override
  public String getClassName() {
    return className != null ? className : NA;
  }

  @Override
  public String getFileName() {
    return fileName != null ? fileName : NA;
  }
  
  @Override
  public String getLineNumber() {
    return lineNumber != null ? lineNumber : NA;
  }
  
  @Override
  public String getMethodName() {
    return methodName != null ? methodName : NA;
  }
}
