/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.LoggingEvent;

public class ValidatorSecureAppender extends SecureAppender implements ErrorHandler {

  public ValidatorSecureAppender() {
    super();
  }

  @Override
  public void error(final String message) {
    getErrorHandler().error(message);
  }

  @Override
  public void error(final String message, final Exception exception, final int errorCode) {
    getErrorHandler().error(message, exception, errorCode);
  }

  @Override
  public void error(final String message, final Exception exception, final int errprCode,
      final LoggingEvent event) {
    getErrorHandler().error(message, exception, errprCode, event);
  }

  @Override
  public void setAppender(final Appender appender) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setBackupAppender(final Appender appender) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setLogger(final Logger logger) {
    throw new UnsupportedOperationException();
  }
}
