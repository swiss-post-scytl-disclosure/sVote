/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt'
 * which is part of this source code package
 */

package com.scytl.slogger.validator;

import com.scytl.slogger.event.SecureLoggingEvent;
import com.scytl.slogger.event.SecurePatternLayout;

/**
 * Validaor factory.
 */
public class ValidatorFactory {

  /**
   * Creates a validator.
   *
   * @return a validator.
   * @throws EventValidatorException failed to create a validator.
   */
  public LoggingEventValidator createValidator() throws EventValidatorException {
    return new LoggingEventValidator() {

      @Override
      public void stopValidation(SecurePatternLayout layout) throws EventValidatorException {}

      @Override
      public void validateEvent(final SecureLoggingEvent event, final SecurePatternLayout layout)
          throws EventValidatorException {}
    };
  }
}
