/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
const gulp = require('gulp');
const jsdoc = require('gulp-jsdoc3');

gulp.task('generate-docs', (done) => {

  const config = require('../jsdoc.conf.json');

  gulp.src(
    [
      './doc/api-guide.md',
      './src/lib/client/config.js',
      './src/lib/api.js',
    ],
    { read: false }
  )
    .pipe(jsdoc(config, done));

});
