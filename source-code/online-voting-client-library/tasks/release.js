/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
const gulp   = require('gulp');
const bump   = require('gulp-bump');
const filter = require('gulp-filter');
const semver = require('semver');

const {
  getPackageVersion,
} = require('./helpers');

let newVer = null;
const packageJson = filter(['package.json']); // locate where is the package.json

// BUILD AND RELEASE TASKS
// to set the build or release version during the CI build/release

// bump versions for a development
gulp.task('bump:dev', ['newVer_dev', 'writeJson']);

// bump versions for a release candidate
gulp.task('bump:rc', ['newVer_rc', 'writeJson']);

// crops versions for a release
gulp.task('bump:release', ['newVer_release', 'writeJson']);

gulp.task('newVer_dev', function () {

  newVer = semver.inc(getPackageVersion(), 'prerelease', Date.now().toString());

});

gulp.task('newVer_rc', function () {

  newVer = semver.inc(getPackageVersion(), 'prerelease', 'rc');

});

gulp.task('newVer_release', function () {

  newVer = semver.coerce(getPackageVersion());

});

// DEVELOPMENT TASKS
// to set the new development iteration version after creating the release branch

// bump versions to next development iteration
gulp.task('bump:next', ['newVer_minor', 'writeJson']);

gulp.task('newVer_minor', function () {

  newVer = semver.inc(getPackageVersion(), 'minor');

});

// write the package.json to the new version
gulp.task('writeJson', function () {

  return gulp.src(['./package.json'])
    .pipe(bump({
      version: newVer,
    }))
    .pipe(packageJson)
    .pipe(gulp.dest('./'));

});
