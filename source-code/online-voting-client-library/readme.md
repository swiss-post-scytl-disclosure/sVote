# ov-client / Online Voting Client API

This is the JavaScript client API for the online voting platform. It
produces a JavaScript bundle file containing all necessary
dependencies for a voting client.

## API documentation
    
For API guide:

https://scm.scytl.net/stash/projects/COMP/repos/online-voting-client-library/browse/ov-client-js/doc/api-guide.md

For API JSdocs see:

Build the project and find it in `dist/doc/api-reference`

## Sample voter portal implementation

_Note that this sample app is a bit outdated and not currently maintained_

A sample voting client implementation using the _OvMsgApi_ is included
in the _/sample/_ folder. For instructions see:
https://scm.scytl.net/stash/projects/COMP/repos/online-voting-client-library/browse/ov-client-js/sample/readme.md

## Automated testing tool

A suite of scripts to set up several lightweight clients for
stress/performance tests. Docs for these tools can be found here:
  
https://scm.scytl.net/stash/projects/COMP/repos/online-voting-client-library/browse/ov-client-js/robot/readme.md

    
## prerequisites:

[nodejs](https://nodejs.org/download/)


## installing:

1. clone repo && cd into
2. `$ npm install`

## building:

- `$ npm run build`         build, test and generate docs
- `$ make`                  (same thing)

## build output

```
/dist
    ov-api.min.js
    /doc
        /api-reference       (api docs in html)
        /src
            ov-api.js        (unminified & unobfuscated source)
```


## JavaScript benchmarking

### crono.js

Basic benchmark support is provided by a custom embedded timing
component (crono.js). Collection of performance data occurs always
with negligible overhead, and a summary is logged to the browser
console. This logged output is suitable to be posted to a performance
data repository but this is still TBD.

See 'src/lib/client/send-vote.js' for more details.

## Integration with _forge_ 

As of 17/2/17 ov-client uses release 0.6.49 of [forge](https://github.com/digitalbazaar/forge/tree/0.6.45)

## Integration with _crypto-lib_

The current build uses _crypto-lib - v1.22.0 - 2015-12-21_. Note that
this is a slightly patched version found in _'./crypto/'_. Upgrading
crypto-lib is possible if you apply the changes listed in the
_'./crypto/cryptolib.diff'_ file. There are two tasks for updating
cryptolib or policies:

### Upgrading cryptolib version
    
Get the new _crypto-lib.js_, patch it (see above) and place it in /crypto/

### Upgrading/changing cryptolib policies
    
Update the policies in the existing _crypto/policies.js_ file

## CI for Javascript projects (Jenkins, NPM Nexus)

## Branches inside Jenkins

The purpose of this guide is to introduce the future changes of Javascript projects.

Branches inside Jenkins and his jobs:

- DEVELOP → every time that we make one merge in develop then publishes a package X.X.X-DEV.X incremental version.
- RELEASE → actually the creation of this branch will be manually and then we need to make a petition to CI people for the release job creation.  Then the job by parameters supports: release candidate incremental version X.X.X-RC.X, patch version, minor or major version.


## How it works NPM NEXUS

NPM Nexus is private repository to store dependencies that we'll use inside different projects.
There are two repositories and a group:

1. [npmjs](https://nexus.scytl.net/content/repositories/npmjs/) This is the official repository of npm (https://registry.npmjs.org) that we make proxy.

2. [npm-scytl](https://nexus.scytl.net/content/repositories/npm-scytl/) This is the private repository where we can upload/publish our NPM artifacts.

3. [group](https://nexus.scytl.net/content/groups/public-npm/) This group is a repository that groups the hole previously.



## How to how in development

1. Local or off-line.

To try a dependency locally and avoid to publish incorrect versions into nexus, then we’ll use the npm link command. This command is useful if you need to try some changes of your library locally and want to make sure that the changes works fine and this doesn’t break anything.

First go to your library main directory and type “npm link” this command copies all your project folder in a npm shared folder of your machine making your library “on-line” like npm do it by in this case is locally.
Next step you’ll need is going your project main folder and write “npm link name-of-the-dependency” , so at this moment the npm takes all of your share folder and place it inside your node_modules actual project.

Then voilà! You can try your library easy with two commands.

2. Remote or online

There are two possibilities: 

- _NPM NEXUS VERSION_: To use the library inside a project, you need to specify the name and version inside the package.json of the project. Example:  “name”: “x.x.x” inside package.json and then do the "npm install". In this case you don’t need to run any commands to build something, cause the package is yet distributed and the scripts of the project will be use it correctly.

- _GIT+SSH_: Or if you want, can work with git+ssh URL (needs ssh configuration) with main branch or by commit and then do npm install. In this second case you need to make npm run release in other to create the distribution folders, and then the scripts do the rest.


## OV-CLIENT inside NPM NEXUS

_ov-client_ is uploaded into npm nexus, so it's available to download wherever you want for consuming. All you need is specify the version inside you package.json of the project.

This _ov-client_ is inside _npm-scytl_ then you can found it here: [npm-scytl](https://nexus.scytl.net/content/repositories/npm-scytl/)