/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
const gulp        = require('gulp');
const del         = require('del');
const runSequence = require('run-sequence');

const paths = require('./paths.json');

const {
  handleTaskSequenceError,
} = require('./tasks/helpers');

require('./tasks/compile-js');
require('./tasks/release');
require('./tasks/compile-docs');

gulp.task('clean', () => {

  return del([paths.global.dist]);

});

gulp.task('compile', (done) => {

  runSequence(
    'clean',
    'build-js',
    handleTaskSequenceError(done)
  );

});

gulp.task('build', (done) => {

  runSequence(
    'clean',
    'unit-tests',
    [
      'build-js',
      'generate-docs',
      'build-js:for-doc',
    ],
    handleTaskSequenceError(done)
  );

});

gulp.task('dev', (done) => {

  runSequence(
    'build',
    'watch:js',
    handleTaskSequenceError(done)
  );

});

// Run the gulp
gulp.task('default', function () {

  console.log('Executing gulp and getting info from package.json');

});
