/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global forge */
/* global CL */

module.exports = (function() {

    'use strict';

    // receives and array of data , which will be concatenated and
    // signed with the given private key

    var signData = function(data,privateKey) {

    	var dataToSign,signedData;
                
        dataToSign = forge.util.encode64(data.join(''));                        
        signedData = CL.asymmetric.service.sign(privateKey, [dataToSign]);


        return signedData;
    };    

    // receives and array of data , which will be concatenated and
    // used to verify the given signature with the given public key

    var verifySignature = function(data,publicKey,signature) {

    	var dataToSign;
                
        dataToSign = forge.util.encode64(data.join(''));                        
        return CL.asymmetric.service.verifySignature(signature,publicKey, [dataToSign]);

    };    

    return {
    	signData:signData,
    	verifySignature:verifySignature
    };

})();
