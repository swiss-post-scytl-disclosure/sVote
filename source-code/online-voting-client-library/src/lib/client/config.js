/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
module.exports = (function() {

    'use strict';

    /**
     * @typedef {Object} configuration_options
     * @property {string}  lib                      Relative path and file name for the API library 
     * @property {string}  tenantId                 The tenant
     * @property {string}  electionEventId          The election event
     * @property {string}  host                     Back-end host
     * @property {object}  endpoints                Back-end endpoints
     * @property {string}  endpoints.authentication Authentication (extended)
     * @property {string}  endpoints.informations   Authentication step 1 ('auth/request-token')
     * @property {string}  endpoints.tokens         Authentication step 2 ('auth/authenticate-token')
     * @property {string}  endpoints.votes          Send vote ('vote')
     * @property {string}  endpoints.confirmations  Cast vote ('confirm')
     * @property {string}  endpoints.choicecodes    Request choice codes (interrupted flow) ('choicecodes')
     * @property {string}  endpoints.votecastcodes  Request vote cast code (already voted) ('castcode')
     * @property {string}  endpoints.receipts       Request receipt
     */

    var cfg = {

        lib: 'crypto/ov-api.min.js',

        tenantId: '',
        electionEventId: '',

        host : '/ag-ws-rest/api/ov/voting/v1',

        'endpoints.authentication':
        '/tenant/{tenantId}/electionevent/{electionEventId}/extended_authenticate',
        'endpoints.informations':
        '/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}/auth/request-token',
        'endpoints.tokens':
        '/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}/auth/authenticate-token',
        'endpoints.votes':
        '/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}/vote',
        'endpoints.confirmations':
        '/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}/confirm',
        'endpoints.choicecodes':
        '/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}/choicecodes',
        'endpoints.votecastcodes':
        '/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}/castcode',
        'endpoints.receipts':
        '/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}/receipt'
    };


    // get/set values in cfg

    var config = function(name, value) {

        if (value) {

            // if called with name and value args, asign value to name property
            return (cfg[name] = value);

        } else {

            switch (typeof name) {

                case 'object':
                    // if called with and object, assign it as new full state
                    return (cfg = name);

                case 'undefined':
                    // if called with no args, return full state
                    return cfg;

                default:
                    // if called with a single string, return that property
                    return cfg[name];

            }
        }
    };

    return config;

}());
