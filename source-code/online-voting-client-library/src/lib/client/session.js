/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
// session values container

module.exports = (function() {

    'use strict';

    var sess = {

    };

    // get/set values in sess

    var session = function(name, value) {

        if (value) {

            // if called with name and value args, asign value to name property
            return (sess[name] = value);

        } else {

            switch (typeof name) {

                case 'object':
                    // if called with and object, assign it as new full state
                    return (sess = name);

                case 'undefined':
                    // if called with no args, return full state
                    return sess;

                default:
                    // if called with a single string, return that property
                    return sess[name];

            }
        }
    };

    return session;

}());
