/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
module.exports = (function() {

    'use strict';

    var crono = function(fn) {

        var currentTask = '';
        var currentStart = 0;
        var firstStart = 0;
        var log = [];

        var timingFn = fn || Date.now;

        var reset = function() {

            currentTask = '';
            currentStart = 0;
            firstStart = 0;
            log = [];

        };

        var start = function(task) {

            currentStart = timingFn();
            if (!firstStart) {
                firstStart = currentStart;
            }
            currentTask = task;

        };

        var stop = function() {

            var ms = timingFn() - currentStart;

            log.push({
                task: currentTask,
                ms: ms
            });

        };
        
        var getLog = function() {

            var ms = timingFn() - firstStart;

            log.push({
                task: 'total',
                ms: ms
            });
            
            return log;

        };

        return {
            reset: reset,
            start: start,
            stop: stop,
            getLog: getLog
        };
    };

    return crono;

})();
