/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global require */
/* global OV */
/* jshint maxlen: 666  */

var _ = require('lodash');



module.exports = (function() {

  var parseCorrectnessIds = function(correctnessIds, option, attributes) {

      var attrMap = _.reduce(attributes, function (acc, attr) {
        acc[attr.id] = attr;
        return acc;
      }, {});

      var prime = option.representation;
      var attr = attrMap[option.attribute];

      if (attr) {
          if (!correctnessIds[prime]) {
              correctnessIds[prime] = [];
          }

          var related = [attr.id].concat(attr.related);

          _.each(related, function(rel) {
              if (attrMap[rel] && attrMap[rel].correctness && (attrMap[rel].correctness==='true')) {
                  correctnessIds[prime].push(rel);
              }
          });

      }

  };

  return parseCorrectnessIds;

}());
