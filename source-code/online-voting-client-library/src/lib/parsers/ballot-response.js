/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global OV */
/* jshint maxlen: 666  */

module.exports = (function() {

    'use strict';
  
    // validate ballot response
    // returns: {OV.Ballot}, the ballot

    var validateBallotResponse = function(response) {

        var ballot = OV.BallotParser.parseBallot(response.ballot);
        OV.BallotParser.parseTexts(ballot, response.ballotTexts);

        return ballot;
    };

    return validateBallotResponse;

}());
