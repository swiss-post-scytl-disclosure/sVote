/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global forge */
/* global CL */

module.exports = (function() {

    'use strict';

    // Stores voting protocol parameters and keys
    /* jshint unused:false */
    var extractVerificationCard = function(verificationData, pass) {

        var ksObject, ks, voterSecretKey, aliases, egKey,
            passb64 = forge.util.encode64(pass);

        try {
            ksObject = JSON.parse(forge.util.decode64(verificationData));
        } catch (e) {
            throw new Error('Invalid verification data');
        }

        try {
            ks = new CL.stores.service.loadStore(ksObject);
            aliases = Object.keys(ksObject.egPrivKeys);
            egKey = ks.getElGamalPrivateKey(aliases[0], passb64);
            voterSecretKey = egKey.getExponentsArray()[0].getValue();
        } catch (e) {
            throw new Error('Could not access keystore');
        }

        return voterSecretKey;

    };

    return extractVerificationCard;

})();
