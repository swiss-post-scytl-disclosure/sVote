/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global OV */
/* global CL */
/* jshint maxlen: 666  */
module.exports = (function() {

    'use strict';

    // validate vote signature
    // returns: vote content
    var validateSignedVote = function(vote, authToken, votingCardId, electionEventId, credentialCert) {
        var data = [
          vote.encryptedOptions,
          vote.encryptedWriteIns,
          vote.correctnessIds,
          vote.verificationCardPKSignature,
          authToken.signature,
          vote.schnorrProof,
          votingCardId,
          electionEventId
        ];

        var key = CL.certificates.service.load(credentialCert).getPublicKey();

        var verified = OV.verifySignature(data, key, vote.signature);
        if (!verified) {
            throw new Error('BAD_SIGNATURE');
        }

        return verified;

    };

    return validateSignedVote;

})();
