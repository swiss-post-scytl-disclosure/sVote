/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global require */
/* global OV */
/* jshint maxlen: 666  */

var _     = require('lodash');
var Utils = require('./util.js');

var getText = Utils.getText;
var parseAttributeTranslations = Utils.parseAttributeTranslations;

module.exports = (function() {

    var parseQuestion = function (rawContest, rawQuestion) {

        var question = new OV.Question(rawQuestion);

        question.attrIndex = _.findIndex(rawContest.attributes, { id: question.attribute });

        var options = _.filter(rawContest.options, function(option) {
            var attr = _.find(rawContest.attributes, { id: option.attribute });
            return attr && _.includes(attr.related, rawQuestion.attribute);
        });

        _.each(options, function(option) {
            var isBlank = option.attribute === rawQuestion.blankAttribute;
            var opt = new OV.Option(option, isBlank);
            opt.attrIndex = _.findIndex(rawContest.attributes, { id: option.attribute });
            question.addOption(opt);
            question.optionsMinChoices = Number(rawQuestion.min);
            question.optionsMaxChoices = Number(rawQuestion.max);
        });

        question.options =  _.sortBy(question.options, ['attrIndex']);

        return question;

    };

	var parseContest = function(rawContest) {

        var contest = new OV.Options(rawContest);

        _.each(rawContest.questions, function(rawQuestion) {
            contest.addQuestion(parseQuestion(rawContest, rawQuestion));
        });

        contest.questions = _.sortBy(contest.questions, ['attrIndex']);

        return contest;
    };
	
    var setLocale = function(contest, txt) {
        contest.title       = getText(txt, contest.id, 'title', null, 'contest');
        contest.description = getText(txt, contest.id, 'description', null, 'contest');
        contest.howToVote   = getText(txt, contest.id, 'howToVote', null, 'contest');

        _.each(contest.questions, function (question) {
            question.details = parseAttributeTranslations(txt, question.attribute);
            question.text = question.details.questionType_text;

            if (question.blankOption) {
                question.blankOption.details = parseAttributeTranslations(txt, question.blankOption.attribute);
                question.blankOption.text = question.blankOption.details.text;
            }

            _.each(question.options, function(option) {
                option.details = parseAttributeTranslations(txt, option.attribute);
                option.text = option.details.answerType_text;
            });
        });
    };

    return {
        parse: parseContest,
        setLocale: setLocale
    };

}());
