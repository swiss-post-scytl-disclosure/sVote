/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global OV */
/* global CL */
/* jshint maxlen: 6666 */

module.exports = (function() {

    'use strict';
	
	var precomputeSchnorr = function(electionEventId, voterId, encParams) {

        return CL.proofs.service.preComputeSchnorrProof(electionEventId, voterId, encParams.group);
    };

	var precomputeExponentiationProof = function(encrypterValues, encParams) {

        if (!encrypterValues) {
            return null;
        } else {
            var generator = new CL.commons.mathematical.ZpGroupElement(
                encParams.g,
                encParams.p,
                encParams.q);
            return CL.proofs.service.preComputeExponentiationProof([generator, encrypterValues.options.getGamma()], encParams.group);
        }

    };
	
	var precomputePlainTextEqualityProof = function(encParams) {

        var primaryPublicKey = (function() {

            return {
                getGroup: function() {
                    return encParams.group;
                },
                getGroupElementsArray: function() {

                    var firstSubKey = [];
                    firstSubKey.push(encParams.optionsEncryptionKey.getGroupElementsArray()[0]);
                    return firstSubKey;
                }
            };
        })();

        var secondaryPublicKey = (function() {

            var compressed = CL.commons.mathematical.groupUtils.compressGroupElements(
                encParams.group,
                encParams.choiceCodesEncryptionKey.getGroupElementsArray());

            return {
                getGroup: function() {
                    return encParams.group;
                },
                getGroupElementsArray: function() {
                    return [compressed];
                }
            };
        })();

        return CL.proofs.service.preComputePlaintextEqualityProof(
            primaryPublicKey,
            secondaryPublicKey,
            encParams.group);
    };
	
    var precomputeProofs = function(electionEventId, voterId, serializedEncParams, serializedEncrypterValues) {

        var encParams = new OV.model.EncryptionParams({
            serializedP: serializedEncParams.serializedP,
            serializedG: serializedEncParams.serializedG,
            serializedOptionsEncryptionKey: serializedEncParams.serializedOptionsEncryptionKey,
            serializedChoiceCodesEncryptionKey: serializedEncParams.serializedChoiceCodesEncryptionKey
        });

        var encrypterValues = null;
        if (serializedEncrypterValues) {
            encrypterValues = OV.deserializeEncrypterValues(serializedEncrypterValues);
        }

        return {
            schnorr: precomputeSchnorr(electionEventId, voterId, encParams).stringify(),
            exponentiation: precomputeExponentiationProof(encrypterValues, encParams).stringify(),
            plaintextEquality: precomputePlainTextEqualityProof(encParams).stringify()
        };
    };

    return {
        precomputeProofs: precomputeProofs
    };

})();
