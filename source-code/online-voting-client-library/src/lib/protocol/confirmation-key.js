/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global forge */

module.exports = (function() {

    'use strict';

    // generate the confirmation key for confirmation message

    // @param ballotCastingKey {String} 9 numerical digits
    // @param encryptionParms {model.EncryptionParms} The encryption parameters and key
    // @param verificationCardSecretKey {BigInteger} verification card secret key
    
    // @return {String} base 64 encoded confirmation key

    var generateConfirmationKey = function(ballotCastingKey, encryptionParms, verificationCardSecretKey) {

        // compute encoded BCK

        var bck = new forge.jsbn.BigInteger(ballotCastingKey);
        var big2 = new forge.jsbn.BigInteger('2');
        var encodedBCK = bck.modPow(big2, encryptionParms.p);

        // compute confirmation message

        var confirmationKey = encodedBCK.modPow(verificationCardSecretKey, encryptionParms.p);

        return forge.util.encode64(confirmationKey.toString());

    };

    return generateConfirmationKey;

}());
