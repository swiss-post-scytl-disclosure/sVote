/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global forge */

module.exports = (function() {

    'use strict';

    // generate partial choice codes

    // @param options {<Array<ZpGroupElement>} The vote option prime numbers
    // @param encryptionParms {model.EncryptionParms} The encryption parameters and key
    // @param exponent {BigInteger} The exponent
    // @param precomputedPCC {String[]} PrecomputedPCC

    var generatePartialChoiceCodes = function(options, encryptionParms, exponent, precomputedPCC) {

        var pccPool = precomputedPCC || [];

        return options.map(function(o) {
            
            var pcc = pccPool[o.getElementValue().toString()];

            if ( pcc ) {
                return new forge.jsbn.BigInteger(pcc);
            } else {
                return o.getElementValue().modPow(exponent, encryptionParms.p);
            }
            

        });
    };

    return generatePartialChoiceCodes;

}());
