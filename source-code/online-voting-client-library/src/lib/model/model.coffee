###
Copyright 2018 Scytl Secure Electronic Voting SA

All rights reserved

See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
###
model = 
    EncryptionParams: require './encryption-params.coffee' 
    EncrypterValues: require './encrypter-values.coffee' 
    ballot: require './ballot.coffee' 

module.exports = model
