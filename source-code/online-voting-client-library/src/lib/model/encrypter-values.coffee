###
Copyright 2018 Scytl Secure Electronic Voting SA

All rights reserved

See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
###
class EncrypterValues
    
    constructor: ( options ) ->
        {
        @rve,
        @C0,
        @preC1
        } = options

module.exports = EncrypterValues 
