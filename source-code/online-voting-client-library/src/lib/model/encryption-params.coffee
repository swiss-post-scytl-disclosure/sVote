###
Copyright 2018 Scytl Secure Electronic Voting SA

All rights reserved

See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
###
class EncryptionParams
        
    constructor: ( options ) ->
        {
        @serializedP,
        @serializedG,
        @serializedOptionsEncryptionKey,
        @serializedChoiceCodesEncryptionKey
        } = options

        @p = new forge.jsbn.BigInteger(@serializedP);
        @g = new forge.jsbn.BigInteger(@serializedG);
        b1 = new forge.jsbn.BigInteger '1'
        b2 = new forge.jsbn.BigInteger '2'
        @q = @p.subtract(b1).divide(b2) #  q=(p-1)/2
        @group = new CL.commons.mathematical.ZpSubgroup(@g, @p, @q)

        kf = new CL.homomorphic.keypair.factory.KeyFactory();
        @optionsEncryptionKey = kf.createPublicKey(@serializedOptionsEncryptionKey);
        @choiceCodesEncryptionKey = kf.createPublicKey(@serializedChoiceCodesEncryptionKey);


module.exports =  EncryptionParams
