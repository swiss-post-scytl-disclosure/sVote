/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
const paths    = require('./paths.json');
const babelify = require('babelify');

module.exports = function (config) {

  config.set({

    basePath: '',

    frameworks: ['browserify', 'jasmine', 'es6-shim'],

    browsers: ['PhantomJS'],

    files: [
      paths.js.entryFile,
      './node_modules/node-forge/dist/forge.min.js',
      './node_modules/sjcl/sjcl.js',
      `${paths.unitTests.baseDir}/mocks/testdata.js`,
      `${paths.unitTests.baseDir}/**/!(precompute|proofs).spec.js`,
    ],

    preprocessors: {
      [paths.js.entryFile]                    : ['browserify'],
      [`${paths.unitTests.baseDir}/**/*.js`]  : ['browserify'],
    },

    browserify: {
      debug: true,
      transform: [
        babelify.configure({
          ignore     : /(node_modules)/,
          presets    : ['es2015'],
          sourceMaps : true,
        }),
      ],
      extensions: ['.js'],
    },

    reporters: ['spec'],

    singleRun: true,

    browserDisconnectTolerance: 1,
    browserDisconnectTimeout: 100000,
    browserNoActivityTimeout: 100000,
    captureTimeout: 100000,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN
    //                  || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

  });

};
