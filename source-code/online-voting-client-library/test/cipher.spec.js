/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global forge */
/* global CL */
/* global OV */
/* global TD */

describe('Cipher api', function() {

    'use strict';

    var TD = require('./mocks/testdata.json');

    it('should cipher an array of elements', function() {

        var encParms = OV.parseEncryptionParams(TD.authResponse);
        var serializedEncrypterValues = OV.precomputeEncrypterValues(encParms);
        var encrypterValues = OV.deserializeEncrypterValues(serializedEncrypterValues);

        var keyValue = forge.jsbn.BigInteger.ONE;
        var element = new CL.commons.mathematical.ZpGroupElement(keyValue, encParms.p, encParms.q);
        var messages = [element];
        var ciphered = OV.encryptOptions(messages, encParms, encrypterValues);

        expect(ciphered).toBeDefined();

    });


});
