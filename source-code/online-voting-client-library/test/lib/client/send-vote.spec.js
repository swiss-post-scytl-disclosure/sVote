/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global _ */
/* global OV */

describe('Send vote', function () {

    'use strict';

    var response = require('../parsers/mocks/ballot_with_writeins.json');

    it('send a vote', function () {

        expect(OV).toBeDefined();
        expect(response).toBeDefined();

        var ballot = OV.BallotParser.parseBallot(response.ballot);
    });

});
