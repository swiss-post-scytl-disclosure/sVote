/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global _ */
/* global OV */

describe('Calculate max writeins', function () {

    'use strict';

    var session = require('../../../src/lib/client/session.js');

    var response = require('./mocks/ballot_with_writeins.json');

    it('should obtain max possible number of writeins', function () {

        var maximumPossibleWriteins = OV.writeInsParser.calculateMaximumPossibleWriteins(response.ballot);

        expect(maximumPossibleWriteins).toBe(10);
    });
});
