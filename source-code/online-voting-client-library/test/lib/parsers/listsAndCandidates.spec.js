/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global OV */

describe('Lists And Candidates contest parser', function() {

    'use strict';

    var lacParser      = OV.ListsAndCandidatesParser;
    var contest        = require('./mocks/listsAndCandidates.json');
    var candidatesOnly = require('./mocks/candidatesOnly.json');
    var candidatesWithWriteIns = require('./mocks/candidatesWithWriteIns.json');

    it('should parse the contest configuration', function() {

      var parsed = lacParser.parse(contest);

      expect(parsed.template).toBe('listsAndCandidates');
      expect(parsed.allowFullBlank).toBe(true);

    });

    it('should parse the lists min and max restrictions', function() {

      var parsed = lacParser.parse(contest);

      // If fails, probably is due to not being able to find the question (alias changed?)
      expect(parsed.listQuestion.minChoices).toBe(0);
      expect(parsed.listQuestion.maxChoices).toBe(1);

    });

    it('should parse the candidates min and max restrictions', function() {

        var parsed = lacParser.parse(contest);

        expect(parsed.candidatesQuestion.minChoices).toBe(0);
        expect(parsed.candidatesQuestion.maxChoices).toBe(2);

    });

    it('should parse the lists', function() {

        var parsed = lacParser.parse(contest);

        expect(parsed.lists.length).toBe(3);

        expect(parsed.lists
          .filter(function(l) { return l.prime === '100109' }).length).toBe(1);
        expect(parsed.lists
          .filter(function(l) { return l.prime === '100169' }).length).toBe(1);
        expect(parsed.lists
          .filter(function(l) { return l.prime === '100153' }).length).toBe(1);

    });

    it('should parse the blank list', function() {

      var parsed = lacParser.parse(contest);

      var blankList = parsed.lists.filter(function(l) {
        return l.prime === '100109' })[0];

      expect(blankList.isBlank).toBe(true);
      expect(blankList.candidates.length).toBe(2);

      expect(blankList.candidates
        .filter(function(c) { return c.prime === '100183' }).length).toBe(1);
      expect(blankList.candidates
        .filter(function(c) { return c.prime === '100193' }).length).toBe(1);

    });

    it('should parse the candidates', function() {

      var parsed = lacParser.parse(contest);

      var list1 = parsed.lists.filter(function(l) {
        return l.prime === '100169' })[0];

      expect(list1.isBlank).toBe(false);
      expect(list1.candidates.length).toBe(2);

      expect(list1.candidates
        .filter(function(c) { return c.prime === '100129' }).length).toBe(1);
      expect(list1.candidates
        .filter(function(c) { return c.prime === '100237' }).length).toBe(1);

      var list2 = parsed.lists.filter(function(l) {
        return l.prime === '100153' })[0];

      expect(list2.isBlank).toBe(false);
      expect(list2.candidates.length).toBe(2);

      expect(list2.candidates
        .filter(function(c) { return c.prime === '100207' }).length).toBe(1);
      expect(list2.candidates
        .filter(function(c) { return c.prime === '100267' }).length).toBe(1);

    });


    it('should parse the blank candidate inside a non selectable blank list', function() {

      var parsed = lacParser.parse(candidatesOnly);

      var blankList = parsed.lists.filter(function(l) {
        return l.isBlank === true })[0];

      expect(blankList.isBlank).toBe(true);
      expect(blankList.candidates.length).toBe(1);

      expect(blankList.candidates
        .filter(function(c) { return c.prime === '100673' }).length).toBe(1);

    });

    it('should parse the candidates with non selectable lists', function() {

      var parsed = lacParser.parse(candidatesOnly);

      var list1 = parsed.lists.filter(function(l) {
        return l.id === '9053f1203ecf4281b220b9c385c3c18e' })[0];

      expect(list1.isBlank).toBe(false);
      expect(list1.candidates.length).toBe(2);

      expect(list1.candidates
        .filter(function(c) { return c.prime === '10069' }).length).toBe(1);
      expect(list1.candidates
        .filter(function(c) { return c.prime === '100613' }).length).toBe(1);

      var list2 = parsed.lists.filter(function(l) {
        return l.id === '980cd86438a048e78737405f34687c05' })[0];

      expect(list2.isBlank).toBe(false);
      expect(list2.candidates.length).toBe(2);

      expect(list2.candidates
        .filter(function(c) { return c.prime === '100559' }).length).toBe(1);
      expect(list2.candidates
        .filter(function(c) { return c.prime === '100549' }).length).toBe(1);

    });

    it('should parse the candidates with writeins', function() {

      var parsed = lacParser.parse(candidatesWithWriteIns);

      var blankList = parsed.lists.find(function(l) {
        return l.isBlank === true });

      expect(blankList.candidates.length).toBe(2);

      expect(blankList.candidates.find(function (o) { return o.isBlank; })).toBeDefined();
      expect(blankList.candidates.find(function (o) { return o.isWriteIn; })).toBeDefined();

    });

});
