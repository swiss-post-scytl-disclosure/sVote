/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global OV */

describe('List model', function() {

    'use strict';

    var list1 = {
      id: '001'
    }

    var list2 = {
      id: '002',
      attribute1: 'Text 1',
      attribute2: 'Text 2',
    }

    it('should init a list', function() {
        var l1 = new OV.List(list1);

        expect(l1.id).toBe('001');
    });

    it('should init a list with min, max, attr1 and attr2', function() {
        var l1 = new OV.List(list2);

        expect(l1.id).toBe('002');
    });

    it('should add candidates', function() {
        var l1 = new OV.List(list2);
        var c1 = new OV.Candidate('001');

        l1.addCandidate(c1);
        expect(l1.candidates.length).toBe(1);
    });

});
