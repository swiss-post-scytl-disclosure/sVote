/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/* global OV */
/* global TD */
/* jshint maxlen: 6666 */

describe('Validators api', function() {

    'use strict';

    var TD = require('./mocks/testdata.json');

    it('should parse a start voting key', function() {
        var testKey = TD.startVotingKey;
        var eeid = TD.eventId;
        var expected = {
            credentialId: TD.infoResponse.credentialData.id,
            pin: '4df144b2c04b0b45ec1035adbba89e22'
        };
        expect(OV.parseStartVotingKey(testKey, eeid)).toEqual(expected);
    });

    it('should parse a server serverChallengeMessage', function() {

        var pin = '414dbbfbe79a26e53ce63f32747fc5cf';

        // no data
        // expect(function() {
        //     var dataToVerify = {
        //         challengeValue: TD.infoResponse.serverChallengeMessage.serverChallenge,
        //         timestamp: TD.infoResponse.serverChallengeMessage.timestamp,
        //         electionEventId: TD.eventId,
        //         credentialId: TD.infoResponse.credentialData.id
        //     };
        //     var credentials = OV.parseCredentials(TD.infoResponse.credentialData, pin);
        //     OV.parseServerChallenge(TD.infoResponse, dataToVerify, credentials);
        // }).not.toThrow();

        expect(function() {
            OV.parseServerChallenge({
                serverChallengeMessage: 'adsf'
            });
        }).toThrow(new Error('Bad server challenge'));

        expect(function() {
            OV.parseServerChallenge({
                serverChallengeMessage: {
                    signature: 'asdf'
                }
            });
        }).toThrow(new Error('Bad server challenge'));

        expect(function() {
            OV.parseServerChallenge({
                serverChallengeMessage: {
                    serverChallenge: '2190381097405701',
                    timestamp: '1428486196320'
                }
            });
        }).toThrow(new Error('Bad server challenge'));

    });

    it('should parse a ballot box response', function() {

        expect(function() {
            OV.parseEncryptionParams(TD.authResponse);
        }).not.toThrow();

    });

});
