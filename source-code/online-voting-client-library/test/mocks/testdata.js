/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
// Test Data and OV api bootstrap

/* jshint ignore: start */

OV._init(function() {
    console.log('Using low entropy. For unit tests only!');
});

// jshint ignore: end
