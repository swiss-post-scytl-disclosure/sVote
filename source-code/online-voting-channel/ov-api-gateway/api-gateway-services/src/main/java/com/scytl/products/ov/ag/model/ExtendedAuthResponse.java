/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Class representing a ExtendedAuthResponse
 */
@JsonInclude(Include.NON_NULL)
public class ExtendedAuthResponse {

    private String responseCode;

    private Integer numberOfRemainingAttempts;

    private String encryptedSVK;

    public String getEncryptedSVK() {
        return encryptedSVK;
    }

    public void setEncryptedSVK(String encryptedSVK) {
        this.encryptedSVK = encryptedSVK;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public Integer getNumberOfRemainingAttempts() {
        return numberOfRemainingAttempts;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public void setNumberOfRemainingAttempts(Integer numberOfRemainingAttempts) {
        this.numberOfRemainingAttempts = numberOfRemainingAttempts;
    }



}
