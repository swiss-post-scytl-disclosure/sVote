/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.model;

/**
 * Class for storing encrypted start voting key.
 */
public class EncryptedSVK {

	private String encryptedStartVotingKey;

	/**
	 * Constructor.
	 * 
	 * @param encryptedSVK the encrypted start voting key.
	 */
	public EncryptedSVK(String encryptedStartVotingKey) {
		super();
		this.encryptedStartVotingKey = encryptedStartVotingKey;
	}

	/**
	 * Returns the current value of the field encryptedSVK.
	 *
	 * @return Returns the encryptedSVK.
	 */
	public String getEncryptedSVK() {
		return encryptedStartVotingKey;
	}

	/**
	 * Sets the value of the field encryptedSVK.
	 *
	 * @param encryptedSVK The encryptedSVK to set.
	 */
	public void setEncryptedSVK(String encryptedStartVotingKey) {
		this.encryptedStartVotingKey = encryptedStartVotingKey;
	}
}
