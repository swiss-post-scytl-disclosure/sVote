/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.admin;

import javax.validation.constraints.NotNull;

import com.scytl.products.ov.commons.beans.domain.model.keytranslation.ElectionEventTranslations;
import com.scytl.products.ov.commons.beans.domain.model.keytranslation.VotingCardTranslationList;
import com.scytl.products.ov.commons.infrastructure.remote.client.RestClientInterceptor;

import retrofit2.Call;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import okhttp3.RequestBody;

/**
 * Key translation client.
 */
public interface KeyTranslationAdminClient {

    String PARAMETER_VALUE_TENANT_ID = "tenantId";

    String PARAMETER_PATH_ELECTION_EVENT = "pathElectionEvent";

    String PARAMETER_PATH_VOTING_CARD = "pathVotingCard";

    String PARAMETER_PATH_ALIAS = "alias";

    String PARAMETER_PATH_ELECTION_EVENT_ID = "electionEventId";

    String PARAMETER_PATH_BALLOT_BOX_ID = "bbid";

    String PARAMETER_X_FORWARDED_FOR = "X-Forwarded-For";

    /** The Constant X_REQUEST_ID. */
    String PARAMETER_X_REQUEST_ID = "X-Request-ID";

    String PARAMETER_PATH_BALLOT_BOX = "pathBallotBox";
    /**
     * Uploads election event translation information using a Rest client.
     *
     * @param pathElectionEvent the path for election event resource.
     * @param tenantId the tenant id
     * @param listEET list of election event translation.
     * @response the resource containing the requested data in json format.
     */
    @POST("{pathElectionEvent}/secured/tenant/{tenantId}")
    Call<ResponseBody> uploadElectionEventTranslation(
            @Path(value = PARAMETER_PATH_ELECTION_EVENT, encoded = true) String pathElectionEvent,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Header(RestClientInterceptor.HEADER_ORIGINATOR) String originator,
            @NotNull @Header(RestClientInterceptor.HEADER_SIGNATURE) String signature,
            @Body ElectionEventTranslations listEET);

    /**
     * Gets election event translation information using a Rest client.
     *
     * @param pathElectionEvent the path for election event resource.
     * @param tenantId the tenant id
     * @param alias election event alias.
     * @response the resource containing the requested data in json format.
     */
    @GET("{pathElectionEvent}/tenant/{tenantId}/alias/{alias}")
    Call<ResponseBody> getElectionEventTranslation(
            @Path(value = PARAMETER_PATH_ELECTION_EVENT, encoded = true) String pathElectionEvent,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_PATH_ALIAS) String alias,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    /**
     * Gets election event alias translation information using a Rest client.
     *
     * @param pathElectionEvent the path for election event resource.
     * @param tenantId the tenant id
     * @param electionEventId election event identifier.
     * @response the resource containing the requested data in json format.s
     */
    @GET("{pathElectionEvent}/tenant/{tenantId}/electionEvent/{electionEventId}")
    Call<ResponseBody> getElectionEventAliasTranslation(@Path(value = PARAMETER_PATH_ELECTION_EVENT, encoded = true) String pathElectionEvent,
                                              @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                              @Path(PARAMETER_PATH_ELECTION_EVENT_ID) String electionEventId,
                                              @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
                                              @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    /**
     * Uploads voting card translation information using Rest client.
     *
     * @param pathVotingCard the path for voting card resource.
     * @param tenantId the tenant id
     * @param listVCS list of voting card translation.
     * @response the resource containing the requested data in json format.
     */
    @POST("{pathVotingCard}/secured/tenant/{tenantId}")
    Call<ResponseBody> uploadVotingCardTranslation(
            @Path(value = PARAMETER_PATH_VOTING_CARD, encoded = true) String pathVotingCard,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Header(RestClientInterceptor.HEADER_ORIGINATOR) String originator,
            @NotNull @Header(RestClientInterceptor.HEADER_SIGNATURE) String signature,
            @Body VotingCardTranslationList listVCS);

    /**
     * Gets voting card translation information using Rest client.
     *
     * @param pathVotingCard the path for voting card resource.
     * @param tenantId the tenant id
     * @param alias voting card translation alias.
     * @response the resource containing the requested data in json format.
     */
    @GET("{pathVotingCard}/tenant/{tenantId}/electionevent/{electionEventId}/alias/{alias}")
    Call<ResponseBody> getVotingCardTranslation(
            @Path(value = PARAMETER_PATH_VOTING_CARD, encoded = true) String pathVotingCard,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_PATH_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_PATH_ALIAS) String alias,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    /**
     * Uploads ballot box translation information using Rest client.
     *
     * @param pathBallotBox the path for ballot box resource.
     * @param tenantId the tenant id
     * @param translations list of ballot box translations.
     * @response the resource containing the requested data in json format.
     */
    @POST("{pathBallotBox}/secured/tenant/{tenantId}")
    Call<ResponseBody> uploadBallotBoxTranslation(
        @Path(value = PARAMETER_PATH_BALLOT_BOX, encoded = true) String pathBallotBox,
        @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
        @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
        @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
        @NotNull @Header(RestClientInterceptor.HEADER_ORIGINATOR) String originator,
        @NotNull @Header(RestClientInterceptor.HEADER_SIGNATURE) String signature,
        @NotNull @Body RequestBody translations);

    /**
     * Gets ballot box translation, by the ballot box alias, information using Rest client.
     *
     * @param pathBallotBox the path for voting card resource.
     * @param tenantId the tenant id
     * @param electionEventId the election event id
     * @param alias the ballot box alias.
     * @response the resource containing the requested data in json format.
     */
    @GET("{pathBallotBox}/tenant/{tenantId}/electionevent/{electionEventId}/alias/{alias}")
    Call<ResponseBody> getBallotBoxTranslationByAlias(
        @Path(value = PARAMETER_PATH_BALLOT_BOX, encoded = true) String pathBallotBox,
        @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
        @Path(PARAMETER_PATH_ELECTION_EVENT_ID) String electionEventId,
        @Path(PARAMETER_PATH_ALIAS) String alias,
        @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
        @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    /**
     * Gets ballot box translation, by the ballot box id, information using Rest client.
     *
     * @param pathBallotBox the path for voting card resource.
     * @param tenantId the tenant id
     * @param electionEventId the election event id
     * @param bbid the ballot box id.
     * @response the resource containing the requested data in json format.
     */
    @GET("{pathBallotBox}/tenant/{tenantId}/electionevent/{electionEventId}/id/{bbid}")
    Call<ResponseBody> getBallotBoxTranslationByBallotBoxId(
        @Path(value = PARAMETER_PATH_BALLOT_BOX, encoded = true) String pathBallotBox,
        @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
        @Path(PARAMETER_PATH_ELECTION_EVENT_ID) String electionEventId,
        @Path(PARAMETER_PATH_BALLOT_BOX_ID) String bbid,
        @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
        @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);
}
