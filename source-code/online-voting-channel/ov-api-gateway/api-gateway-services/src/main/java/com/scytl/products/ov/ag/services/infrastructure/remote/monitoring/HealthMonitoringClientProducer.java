/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.monitoring;

import javax.enterprise.inject.Produces;
import javax.inject.Named;

import com.scytl.products.ov.ag.services.infrastructure.remote.ClientUtil;

/**
 * "Producer" class that centralizes instantiation of all HealthCheck client interfaces
 */
public class HealthMonitoringClientProducer {

    private static final String URI_AUTHENTICATION = System.getProperty("AUTHENTICATION_CONTEXT_URL");
    private static final String URI_EXTENDED_AUTH_CONTEXT = System.getProperty("EXTENDED_AUTHENTICATION_CONTEXT_URL");
    private static final String URI_ELECTION_INFORMATION = System.getProperty("ELECTION_INFORMATION_CONTEXT_URL");
    private static final String URI_VOTER_MATERIAL = System.getProperty("VOTER_MATERIAL_CONTEXT_URL");
    private static final String URI_VOTE_VERIFICATION = System.getProperty("VERIFICATION_CONTEXT_URL");
    private static final String URI_VOTING_WORKFLOW = System.getProperty("VOTING_WORKFLOW_CONTEXT_URL");
    private static final String URI_CERTIFICATE_REGISTRY = System.getProperty("CERTIFICATES_CONTEXT_URL");
    private static final String URI_KEY_TRANSLATION_CONTEXT = System.getProperty("KEY_TRANSLATION_CONTEXT_URL");
    private static final String URI_ORCHESTRATOR_CONTEXT = System.getProperty("ORCHESTRATOR_CONTEXT_URL");

    @Produces
    @Named(HealthMonitoringClient.AU)
    HealthMonitoringClient authenticationMonitoringClient() {
        return ClientUtil.createRestClient(URI_AUTHENTICATION, HealthMonitoringClient.class);
    }
    @Produces
    @Named(HealthMonitoringClient.EA)
    HealthMonitoringClient extendedAuthenticationMonitoringClient() {
        return ClientUtil.createRestClient(URI_EXTENDED_AUTH_CONTEXT, HealthMonitoringClient.class);
    }
    @Produces
    @Named(HealthMonitoringClient.EI)
    HealthMonitoringClient electionInformationMontoringClient() {
        return ClientUtil.createRestClient(URI_ELECTION_INFORMATION, HealthMonitoringClient.class);
    }
    @Produces
    @Named(HealthMonitoringClient.VM)
    HealthMonitoringClient voterMaterialMonitoringClient() {
        return ClientUtil.createRestClient(URI_VOTER_MATERIAL, HealthMonitoringClient.class);
    }
    @Produces
    @Named(HealthMonitoringClient.VV)
    HealthMonitoringClient voteVerificationMonitoringClient() {
        return ClientUtil.createRestClient(URI_VOTE_VERIFICATION, HealthMonitoringClient.class);
    }
    @Produces
    @Named(HealthMonitoringClient.VW)
    HealthMonitoringClient votingWorkflowMontoringClient() {
        return ClientUtil.createRestClient(URI_VOTING_WORKFLOW, HealthMonitoringClient.class);
    }
    @Produces
    @Named(HealthMonitoringClient.CR)
    HealthMonitoringClient certificateRegistryMonitoringClient() {
        return ClientUtil.createRestClient(URI_CERTIFICATE_REGISTRY, HealthMonitoringClient.class);
    }
    @Produces
    @Named(HealthMonitoringClient.KT)
    HealthMonitoringClient keyTranslationMonitoringClient() {
        return ClientUtil.createRestClient(URI_KEY_TRANSLATION_CONTEXT, HealthMonitoringClient.class);
    }    
    @Produces
    @Named(HealthMonitoringClient.OR)
    HealthMonitoringClient orchestratorMonitoringClient() {
        return ClientUtil.createRestClient(URI_ORCHESTRATOR_CONTEXT, HealthMonitoringClient.class);
    }

}
