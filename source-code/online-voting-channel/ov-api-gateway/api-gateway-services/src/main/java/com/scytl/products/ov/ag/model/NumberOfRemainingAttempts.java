/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.model;

public class NumberOfRemainingAttempts {

	private Integer numberOfRemainingAttempts;

	public NumberOfRemainingAttempts(Integer numberOfRemainingAttempts) {
		super();
		this.numberOfRemainingAttempts = numberOfRemainingAttempts;
	}

    public NumberOfRemainingAttempts() {
        super();
    }
    
    public Integer getNumberOfRemainingAttempts() {
		return numberOfRemainingAttempts;
	}

	public void setNumberOfRemainingAttempts(Integer numberOfRemainingAttempts) {
		this.numberOfRemainingAttempts = numberOfRemainingAttempts;
	}
	
}
