/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.admin;

import com.google.gson.JsonObject;

import retrofit2.Call;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import okhttp3.RequestBody;

import javax.validation.constraints.NotNull;

/**
 * The Interface of authentication client for admin.
 */
public interface AuthenticationAdminClient {

    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID =
        "electionEventId";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    public static final String PARAMETER_VALUE_VOTING_CARD_ID =
        "votingCardId";

    /** The Constant PARAMETER_VALUE_BALLOT_BOX_ID. */
    public static final String PARAMETER_VALUE_BALLOT_BOX_ID =
        "ballotBoxId";

    /** The Constant PARAMETER_VALUE_BALLOT_ID. */
    public static final String PARAMETER_VALUE_BALLOT_ID = "ballotId";

    /** The Constant PARAMETER_PATH_BALLOTBOXES. */
    public static final String PARAMETER_PATH_BALLOTBOXES =
        "pathBallotboxes";

    /** The Constant PARAMETER_PATH_BALLOTBOXDATA. */
    public static final String PARAMETER_PATH_BALLOTBOXDATA =
        "pathBallotboxdata";

    /** The Constant PARAMETER_PATH_BALLOTDATA. */
    public static final String PARAMETER_PATH_BALLOTDATA =
        "pathBallotdata";

    /** The Constant PARAMETER_PATH_BALLOTDATA. */
    public static final String PARAMETER_PATH_ELECTORALDATA =
        "pathElectoraldata";

    /** The Constant PARAMETER_PATH_BALLOTDATA. */
    public static final String PARAMETER_PATH_ELECTIONEVENTDATA =
        "pathElectioneventdata";

    /** The Constant PARAMETER_VALUE_ADMIN_BOARD_ID. */
    public static final String PARAMETER_VALUE_ADMIN_BOARD_ID =
        "adminBoardId";

    /** The Constant PARAMETER_TENANT_DATA_PATH. */
    public static final String PARAMETER_PATH_TENANT_DATA =
        "pathTenantData";

    /** The Constant X_FORWARDED_FOR. */
    public static final String PARAMETER_X_FORWARDED_FOR =
        "X-Forwarded-For";

    /** The Constant X_REQUEST_ID. */
    public static final String PARAMETER_X_REQUEST_ID = "X-Request-ID";

    @POST("{pathElectioneventdata}/tenant/{tenantId}/electionevent/{electionEventId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveElectionEventData(
            @Path(PARAMETER_PATH_ELECTIONEVENTDATA) String pathElectioneventdata,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body RequestBody electioneventData);

    @GET("{pathElectioneventdata}/tenant/{tenantId}/electionevent/{electionEventId}/status")
    Call<JsonObject> checkIfElectionEventDataIsEmpty(
            @Path(PARAMETER_PATH_ELECTIONEVENTDATA) String pathBallotboxstatus,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    @GET("{pathTenantData}/activatetenant/tenant/{tenantId}")
    Call<JsonObject> checkTenantActivation(
            @Path(PARAMETER_PATH_TENANT_DATA) String pathTenantData,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);
}
