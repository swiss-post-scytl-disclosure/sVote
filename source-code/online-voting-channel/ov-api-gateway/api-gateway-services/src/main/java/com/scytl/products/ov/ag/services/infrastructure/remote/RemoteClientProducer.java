/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote;

import com.scytl.products.ov.ag.services.infrastructure.remote.controlcomponents.OrchestratorClient;
import javax.enterprise.inject.Produces;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.AuthenticationAdminClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.CertificateServiceClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.ElectionInformationAdminClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.KeyTranslationAdminClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.VoteVerificationAdminClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.VoterMaterialAdminClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.voting.ExtendedAuthenticationVotingClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.voting.VotingWorkflowVotingClient;

/**
 * "Producer" class that centralizes instantiation of all (retrofit) remote
 * service client interfaces
 */
public class RemoteClientProducer {

    private static final String URI_CERTIFICATES_SERVICE = System.getProperty("CERTIFICATES_CONTEXT_URL");

    private static final String URI_AUTHENTICATION_CONTEXT = System.getProperty("AUTHENTICATION_CONTEXT_URL");

    private static final String URI_ELECTION_INFORMATION = System.getProperty("ELECTION_INFORMATION_CONTEXT_URL");

    private static final String URI_VERIFICATION_CONTEXT = System.getProperty("VERIFICATION_CONTEXT_URL");

    private static final String VOTER_MATERIAL_CONTEXT_URL = System.getProperty("VOTER_MATERIAL_CONTEXT_URL");

    private static final String URI_EXT_AUTH_CONTEXT = System.getProperty("EXTENDED_AUTHENTICATION_CONTEXT_URL");

    private static final String URI_KEY_TRANSLATION_CONTEXT = System.getProperty("KEY_TRANSLATION_CONTEXT_URL");

    private static final String URI_VOTING_WORKFLOW_CONTEXT = System.getProperty("VOTING_WORKFLOW_CONTEXT_URL");

    private static final String URI_ORCHESTRATOR_CONTEXT = System.getProperty("ORCHESTRATOR_CONTEXT_URL");

    @Produces
    CertificateServiceClient certificateServiceClient() {
        return ClientUtil.createRestClient(URI_CERTIFICATES_SERVICE, CertificateServiceClient.class);
    }

    @Produces
    AuthenticationAdminClient authenticationAdminClient() {
        return ClientUtil.createRestClient(URI_AUTHENTICATION_CONTEXT, AuthenticationAdminClient.class);
    }

    @Produces
    ElectionInformationAdminClient electionInformationAdminClient() {
        return ClientUtil.createRestClient(URI_ELECTION_INFORMATION, ElectionInformationAdminClient.class);
    }

    @Produces
    VoteVerificationAdminClient voteVerificationAdminClient() {
        return ClientUtil.createRestClient(URI_VERIFICATION_CONTEXT, VoteVerificationAdminClient.class);
    }

    @Produces
    VoterMaterialAdminClient voterMaterialAdminClient() {
        return ClientUtil.createRestClient(VOTER_MATERIAL_CONTEXT_URL, VoterMaterialAdminClient.class);
    }

    @Produces
    ExtendedAuthenticationVotingClient extendedAuthenticationVotingClient() {
        return ClientUtil.createRestClient(URI_EXT_AUTH_CONTEXT, ExtendedAuthenticationVotingClient.class);
    }

    @Produces
    KeyTranslationAdminClient keyTranslationAdminClient() {
        return ClientUtil.createRestClient(URI_KEY_TRANSLATION_CONTEXT, KeyTranslationAdminClient.class);
    }

    @Produces
    VotingWorkflowVotingClient votingWorkflowVotingClient() {
        return ClientUtil.createRestClient(URI_VOTING_WORKFLOW_CONTEXT, VotingWorkflowVotingClient.class);
    }

    @Produces
    OrchestratorClient orchestratorClient(){
        return ClientUtil.createRestClient(URI_ORCHESTRATOR_CONTEXT, OrchestratorClient.class);
    }
}
