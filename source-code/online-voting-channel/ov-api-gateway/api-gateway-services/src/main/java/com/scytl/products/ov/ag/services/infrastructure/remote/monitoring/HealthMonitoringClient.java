/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.monitoring;

import retrofit2.Call;
import retrofit2.http.GET;

import com.scytl.products.ov.commons.infrastructure.health.HealthCheckStatus;

/**
 * The Interface of health client for monitoring.
 */
public interface HealthMonitoringClient {

	String EA = "EA";
	String AU = "AU";
	String EI = "EI";
	String VM = "VM";
	String VW = "VW";
	String KT = "KT";
	String CR = "CR";
	String VV = "VV";
	String OR = "OR";

	@GET("check")
	Call<HealthCheckStatus> checkHealth();
}
