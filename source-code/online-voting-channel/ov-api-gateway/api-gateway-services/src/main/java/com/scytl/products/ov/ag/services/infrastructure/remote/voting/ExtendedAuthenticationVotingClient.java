/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.voting;

import retrofit2.Call;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import okhttp3.RequestBody;

import javax.validation.constraints.NotNull;

import com.scytl.products.ov.ag.model.ExtendedAuthResponse;
import com.scytl.products.ov.ag.model.ExtendedAuthentication;
import com.scytl.products.ov.commons.beans.domain.model.authentication.ExtendedAuthenticationUpdateRequest;

/**
 * The Interface of extended authentication client for voting.
 */
public interface ExtendedAuthenticationVotingClient {

    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    public static final String PARAMETER_VALUE_ADMIN_BOARD_ID = "adminBoardId";

    public static final String PARAMETER_PATH_EXT_AUTH = "extAuthPath";

    public static final String PARAMETER_X_FORWARDED_FOR = "X-Forwarded-For";

    public static final String PARAMETER_X_REQUEST_ID = "X-Request-ID";

    public static final String AUTHENTICATION_TOKEN = "authenticationToken";

    @POST("{extAuthPath}/tenant/{tenantId}/electionevent/{electionEventId}")
    Call<ExtendedAuthResponse> getEncryptedStartVotingKey(
            @Path(value = PARAMETER_PATH_EXT_AUTH, encoded = true) String extAuthPath,
            @Path(PARAMETER_VALUE_TENANT_ID) final String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) final String electionEventId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body ExtendedAuthentication extendedAuthentication);

    @PUT("{extAuthPath}/tenant/{tenantId}/electionevent/{electionEventId}")
    Call<ExtendedAuthResponse> updateExtendedAuthData(
            @Path(value = PARAMETER_PATH_EXT_AUTH, encoded = true) String extAuthPath,
            @Path(PARAMETER_VALUE_TENANT_ID) final String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) final String electionEventId,
            @NotNull @Header(AUTHENTICATION_TOKEN) final String authenticationToken,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body ExtendedAuthenticationUpdateRequest extendedAuthenticationUpdate);

    @POST("{extAuthPath}/tenant/{tenantId}/electionevent/{electionEventId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveExtendedAuthentication(@Path(value = PARAMETER_PATH_EXT_AUTH, encoded = true) String extAuthPath,
            @Path(PARAMETER_VALUE_TENANT_ID) final String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) final String electionEventId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) final String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId, @NotNull @Body RequestBody extendedAuth);

    @GET("{extAuthPath}/tenant/{tenantId}/electionevent/{electionEventId}/blocked")
    @Streaming
    Call<ResponseBody> getBlockedExtendedAuthentications(@Path(value = PARAMETER_PATH_EXT_AUTH, encoded = true) String extAuthPath,
                                        @Path(PARAMETER_VALUE_TENANT_ID) final String tenantId,
                                        @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) final String electionEventId,
                                        @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
                                        @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);
}
