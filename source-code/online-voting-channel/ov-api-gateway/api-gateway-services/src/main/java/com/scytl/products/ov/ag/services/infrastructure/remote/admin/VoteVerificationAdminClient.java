/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.admin;

import com.google.gson.JsonObject;

import retrofit2.Call;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import okhttp3.RequestBody;

import javax.validation.constraints.NotNull;

/**
 * The Interface of vote verification client for admin.
 */
public interface VoteVerificationAdminClient {

    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    public static final String PARAMETER_VALUE_VOTING_CARD_ID = "votingCardId";

    /** The Constant PARAMETER_VALUE_BALLOT_BOX_ID. */
    public static final String PARAMETER_VALUE_BALLOT_BOX_ID = "ballotBoxId";

    /** The Constant PARAMETER_VALUE_VERIFICATION_CARD_SET_ID. */
    public static final String PARAMETER_VALUE_VERIFICATION_CARD_SET_ID = "verificationCardSetId";

    /** The Constant PARAMETER_VALUE_ELECTORAL_AUTHORITY_ID. */
    public static final String PARAMETER_VALUE_ELECTORAL_AUTHORITY_ID = "electoralAuthorityId";

    /** The Constant PARAMETER_PATH_CODESMAPPINGDATA. */
    public static final String PARAMETER_PATH_CODESMAPPINGDATA = "pathCodesmappingdata";

    /** The Constant PARAMETER_PATH_BALLOTBOXDATA. */
    public static final String PARAMETER_PATH_BALLOTBOXDATA = "pathBallotboxdata";

    /** The Constant PARAMETER_PATH_BALLOTDATA. */
    public static final String PARAMETER_PATH_BALLOTDATA = "pathBallotdata";

    /** The Constant PARAMETER_PATH_BALLOTDATA. */
    public static final String PARAMETER_PATH_ELECTORALDATA = "pathElectoraldata";

    /** The Constant PARAMETER_PATH_VERIFICATIONCARDDATA. */
    public static final String PARAMETER_PATH_VERIFICATIONCARDDATA = "pathVerificationcarddata";

    /** The Constant PARAMETER_PATH_VERIFICATIONCARDSETDATA. */
    public static final String PARAMETER_PATH_VERIFICATIONCARDSETDATA = "pathVerificationcardsetdata";
    
    /** The Constant PARAMETER_PATH_VERIFICATIONCARDDERIVEDKEYS. */
    public static final String PARAMETER_PATH_VERIFICATIONCARDDERIVEDKEYS = "pathVerificationcardderivedkeys";

    /** The Constant PARAMETER_TENANT_DATA_PATH. */
    public static final String PARAMETER_PATH_TENANT_DATA = "pathTenantData";

    /** The Constant PARAMETER_VALUE_ADMIN_BOARD_ID. */
    public static final String PARAMETER_VALUE_ADMIN_BOARD_ID = "adminBoardId";

    /** The Constant X_FORWARDED_FOR. */
    public static final String PARAMETER_X_FORWARDED_FOR = "X-Forwarded-For";

    /** The Constant X_REQUEST_ID. */
    public static final String PARAMETER_X_REQUEST_ID = "X-Request-ID";

    @POST("{pathCodesmappingdata}/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveCodesMappingData(@Path(PARAMETER_PATH_CODESMAPPINGDATA) String pathCodesmappingdata,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VERIFICATION_CARD_SET_ID) String verificationCardSetId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body RequestBody codesMappings);

    @POST("{pathVerificationcarddata}/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveVerificationCardData(@Path(PARAMETER_PATH_VERIFICATIONCARDDATA) String pathVerificationcarddata,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VERIFICATION_CARD_SET_ID) String verificationCardSetId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body RequestBody verificationCardData);

    @POST("{pathVerificationcardsetdata}/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveVerificationCardSetData(
            @Path(PARAMETER_PATH_VERIFICATIONCARDSETDATA) String pathVerificationcardsetdata,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VERIFICATION_CARD_SET_ID) String verificationCardSetId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body RequestBody verificationCardSetData);
                                             
    @POST("{pathVerificationcardderivedkeys}/tenant/{tenantId}/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveVerificationCardDerivedKeys(
            @Path(PARAMETER_PATH_VERIFICATIONCARDDERIVEDKEYS) String pathVerificationcardsetderivedkeys,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VERIFICATION_CARD_SET_ID) String verificationCardSetId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body RequestBody verificationCardDerivedKeys);

    @POST("{pathElectoraldata}/tenant/{tenantId}/electionevent/{electionEventId}/electoralauthority/{electoralAuthorityId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveElectoralData(@Path(PARAMETER_PATH_ELECTORALDATA) String pathElectoraldata,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_ELECTORAL_AUTHORITY_ID) String electoralAuthorityId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Body RequestBody electoralData);

    @GET("{pathTenantData}/activatetenant/tenant/{tenantId}")
    Call<JsonObject> checkTenantActivation(
            @Path(PARAMETER_PATH_TENANT_DATA) String pathTenantData,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);
}
