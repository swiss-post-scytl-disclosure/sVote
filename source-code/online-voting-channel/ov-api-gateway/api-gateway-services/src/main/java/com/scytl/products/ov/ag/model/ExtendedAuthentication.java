/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.model;

public class ExtendedAuthentication {

    private String authId;

    private String extraParam;

    public ExtendedAuthentication() {
    	//This constructor is intentionally left blank
    }

    public ExtendedAuthentication(String authId, String extraParam) {
        super();
        this.authId = authId;
        this.extraParam = extraParam;
    }

    public String getExtraParam() {
        return extraParam;
    }

    public void setExtraParam(String extraParam) {
        this.extraParam = extraParam;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

}
