/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.admin;

import javax.validation.constraints.NotNull;

import com.google.gson.JsonObject;
import com.scytl.products.ov.commons.infrastructure.remote.client.RestClientInterceptor;

import retrofit2.Call;
import okhttp3.ResponseBody;
import retrofit2.http.*;
import okhttp3.RequestBody;

/**
 * The Interface of voter material client for admin.
 */
public interface VoterMaterialAdminClient {
    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    public static final String PARAMETER_VALUE_VOTING_CARD_ID = "votingCardId";

    /** The Constant PARAMETER_VALUE_BALLOT_BOX_ID. */
    public static final String PARAMETER_VALUE_BALLOT_BOX_ID = "ballotBoxId";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_SET_ID. */
    public static final String PARAMETER_VALUE_VOTING_CARD_SET_ID = "votingCardSetId";

    /** The Constant PARAMETER_PATH_CREDENTIALDATA. */
    public static final String PARAMETER_PATH_CREDENTIALDATA = "pathCredentialdata";

    /** The Constant PARAMETER_PATH_VOTERINFORMATIONDATA. */
    public static final String PARAMETER_PATH_VOTERINFORMATIONDATA = "pathVoterinformationdata";

    /** The Constant PARAMETER_PATH_VOTERINFORMATION. */
    public static final String PARAMETER_PATH_VOTERINFORMATION = "pathVoterinformation";

    /** The Constant QUERY_PARAMETER_SEARCH_WITH_VOTING_CARD_ID. */
    public static final String QUERY_PARAMETER_SEARCH_WITH_VOTING_CARD_ID = "id";

    /** The Constant PARAMETER_VALUE_ADMIN_BOARD_ID. */
    public static final String PARAMETER_VALUE_ADMIN_BOARD_ID = "adminBoardId";

    /** The Constant QUERY_PARAMETER_OFFSET. */
    public static final String QUERY_PARAMETER_OFFSET = "offset";

    /** The Constant QUERY_PARAMETER_LIMIT. */
    public static final String QUERY_PARAMETER_SIZE = "size";

    /** The Constant X_FORWARDED_FOR. */
    public static final String PARAMETER_X_FORWARDED_FOR = "X-Forwarded-For";

    /** The Constant X_REQUEST_ID. */
    public static final String PARAMETER_X_REQUEST_ID = "X-Request-ID";

    @POST("{pathCredentialdata}/tenant/{tenantId}/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveCredentialData(@Path(PARAMETER_PATH_CREDENTIALDATA) String pathCredentialdata,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VOTING_CARD_SET_ID) String votingCardSetId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId, @NotNull @Body RequestBody credentials);

    @POST("{pathVoterinformationdata}/tenant/{tenantId}/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/adminboard/{adminBoardId}")
    Call<ResponseBody> saveVoterInformationData(@Path(PARAMETER_PATH_VOTERINFORMATIONDATA) String pathVoterinformationdata,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VOTING_CARD_SET_ID) String votingCardSetId,
            @Path(PARAMETER_VALUE_ADMIN_BOARD_ID) String adminBoardId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId, @NotNull @Body RequestBody voterInformations);

    @GET("{pathVoterinformation}/secured/tenant/{tenantId}/electionevent/{electionEventId}/votingcards/query")
    Call<JsonObject> getVotingCardsByQuery(@Path(PARAMETER_PATH_VOTERINFORMATION) String pathVoterinformation,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Query(QUERY_PARAMETER_SEARCH_WITH_VOTING_CARD_ID) String termVotingCardId,
            @Query(QUERY_PARAMETER_OFFSET) String offset, @Query(QUERY_PARAMETER_SIZE) String sizeLimit,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Header(RestClientInterceptor.HEADER_ORIGINATOR) String originator,
            @NotNull @Header(RestClientInterceptor.HEADER_SIGNATURE) String signature);
}
