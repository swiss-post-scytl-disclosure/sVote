/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote.admin;

import javax.validation.constraints.NotNull;

import com.google.gson.JsonObject;
import com.scytl.products.ov.commons.infrastructure.remote.client.RestClientInterceptor;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import okhttp3.RequestBody;

public interface CertificateServiceClient {

    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    /** The Constant PARAMETER_CERTIFICATE_NAME */
    public static final String PARAMETER_VALUE_CERTIFICATE_NAME = "certificateName";

    /**
     * The constant PARAMETER_PATH_CERTIFICATE_DATA
     */
    public static final String PARAMETER_PATH_CERTIFICATE_DATA = "pathCertificateData";

    /** The Constant X_FORWARDED_FOR. */
    public static final String PARAMETER_X_FORWARDED_FOR = "X-Forwarded-For";

    /** The Constant X_REQUEST_ID. */
    public static final String PARAMETER_X_REQUEST_ID = "X-Request-ID";

    @POST("{pathCertificateData}/secured/tenant/{tenantId}/electionevent/{electionEventId}")
    Call<ResponseBody> saveCertificate(
            @Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Header(RestClientInterceptor.HEADER_ORIGINATOR) String originator,
            @NotNull @Header(RestClientInterceptor.HEADER_SIGNATURE) String signature,
            @NotNull @Body RequestBody certificateData);

    @POST("{pathCertificateData}/secured/tenant/{tenantId}")
    Call<ResponseBody> saveCertificate(
            @Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Header(RestClientInterceptor.HEADER_ORIGINATOR) String originator,
            @NotNull @Header(RestClientInterceptor.HEADER_SIGNATURE) String signature,
            @NotNull @Body RequestBody certificateData);

    @POST("{pathCertificateData}/secured")
    Call<JsonObject> saveCertificate(
            @Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
            @NotNull @Header(RestClientInterceptor.HEADER_ORIGINATOR) String originator,
            @NotNull @Header(RestClientInterceptor.HEADER_SIGNATURE) String signature,
            @NotNull @Body RequestBody certificateData);

    @GET("{pathCertificateData}/tenant/{tenantId}/electionevent/{electionEventId}/name/{certificateName}")
    Call<JsonObject> getCertificate(@Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_CERTIFICATE_NAME) String certificateName,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    @GET("{pathCertificateData}/tenant/{tenantId}/name/{certificateName}")
    Call<JsonObject> getCertificate(@Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_CERTIFICATE_NAME) String certificateName,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    @GET("{pathCertificateData}/tenant/{tenantId}/name/{certificateName}/status")
    Call<JsonObject> checkIfCertificateExists(
            @Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_CERTIFICATE_NAME) String certificateName,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

    @GET("{pathCertificateData}/name/{certificateName}")
    Call<JsonObject> getCertificate(@Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @Path(PARAMETER_VALUE_CERTIFICATE_NAME) String certificateName,
            @NotNull @Header(PARAMETER_X_FORWARDED_FOR) String xForwardedFor,
            @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId);

}
