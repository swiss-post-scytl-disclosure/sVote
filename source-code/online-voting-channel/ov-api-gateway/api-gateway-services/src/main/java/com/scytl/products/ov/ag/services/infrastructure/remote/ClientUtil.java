/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.services.infrastructure.remote;

import retrofit2.Retrofit;

import com.scytl.products.ov.commons.infrastructure.exception.OvCommonsInfrastructureException;
import com.scytl.products.ov.commons.infrastructure.exception.RestClientException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RestClientConnectionManager;

public final class ClientUtil {
    private ClientUtil() {
        super();
    }
    
    public static <T> T createRestClient(String url, Class<T> clazz){
        Retrofit client;
        try {
            client = RestClientConnectionManager.getInstance().getRestClient(url);
        } catch (OvCommonsInfrastructureException e) {
            throw new RestClientException("The Rest client could not be created.", e);
        }
        return client.create(clazz);
    }
}
