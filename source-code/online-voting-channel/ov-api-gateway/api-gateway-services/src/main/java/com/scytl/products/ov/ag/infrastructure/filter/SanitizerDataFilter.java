/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.infrastructure.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter to sanitize the request data to avoid Cross-Site Scripting XSS attacks.
 */
public class SanitizerDataFilter implements Filter {

	private static final Logger LOG = LoggerFactory.getLogger(SanitizerDataFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//This method is intentionally left blank
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		LOG.trace("SanitizerDataFilter starting");
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		SanitizerDataHttpServletRequestWrapper sanitizedRequest = new SanitizerDataHttpServletRequestWrapper(httpRequest);
		filterChain.doFilter(sanitizedRequest, response);
		LOG.trace("SanitizerDataFilter ending");
	}

	@Override
	public void destroy() {
		//This method is intentionally left blank
	}
}
