/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.infrastructure.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter to prevent leakage of information of what the content request was, so to prevent the browser from unnecessarily caching resources.
 */
public class CacheControlFilter implements Filter {

	private static final Logger LOG = LoggerFactory.getLogger(CacheControlFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//This method is intentionally left blank
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		LOG.trace("CacheControlFilter starting");
		HttpServletResponse resp = (HttpServletResponse) response;
        resp.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		filterChain.doFilter(request, response);
		LOG.trace("CacheControlFilter ending");
	}

	@Override
	public void destroy() {
		//This method is intentionally left blank
	}
}
