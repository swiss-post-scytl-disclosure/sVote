/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.proxy;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * This factory creates {@code X-Forwarded-For} headers for the incoming requests which should be forwarded to the internal
 * services.
 * </p>
 * <p>
 * Implementation must be thread-safe.
 * </p>
 */
public interface XForwardedForFactory {
    /**
     * Creates a {@code X-Forwarded-For} header for a given incoming request.
     *
     * @param request the request
     * @return the header.
     */
    String newXForwardedFor(HttpServletRequest request);
}
