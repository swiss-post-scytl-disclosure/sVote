/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * This class define the application path for initializing the rest engine.
 */
@ApplicationPath("")
public class RestApplication extends Application {

    public static final String API_OV_VOTING_BASEURI_AUTH =
        "/api/ov/voting/{version}/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}";

    public static final String API_OV_VOTING_BASEURI =
        "/api/ov/voting/{version}/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}";

    public static final String API_OV_VOTING_BASEURI_EXTENDED_AUTH =
        "/api/ov/voting/{version}/tenant/{tenantId}/electionevent/{electionEventId}";

    public static final String API_OV_MONITORING_VERSION_BASEURI = "/api/ov/monitoring/{version}";

    public static final String API_OV_MONITORING_BASEURI =
        API_OV_MONITORING_VERSION_BASEURI + "/tenant/{tenantId}/electionevent/{electionEventId}";

    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    public static final String PARAMETER_VALUE_VOTING_CARD_ID = "votingCardId";

    public static final String PARAMETER_VALUE_VERIFICATION_CARD_SET_ID = "verificationCardSetId";

    public static final String PARAMETER_VALUE_CREDENTIAL_ID = "credentialId";

    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    public static final String PARAMETER_VALUE_VERSION = "version";

    public static final String PARAMETER_AUTHENTICATION_TOKEN = "authenticationToken";

    public static final String PARAMETER_VALUE_EXTRA_PARAM = "extauth";

    public static final String PARAMETER_EXTENDED_AUTH_PATH = "extended_authenticate";

    public static final String PARAMETER_VALUE_CHUNK_ID = "chunkId";

    public static final String PARAMETER_VALUE_CHUNK_COUNT = "chunkCount";

}
