/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.operation.admin;

import com.google.gson.JsonObject;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

import com.scytl.products.ov.ag.services.infrastructure.remote.admin.AuthenticationAdminClient;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactory;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactoryImpl;
import com.scytl.products.ov.commons.infrastructure.remote.client.InputStreamTypedOutput;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import com.scytl.products.ov.commons.tracking.TrackIdGenerator;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

/**
 * Web service for handling electoral data resource.
 */
@Stateless(name = "ag-au-ElectionEventDataResource")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path(ElectionEventDataAUResource.RESOURCE_PATH)
public class ElectionEventDataAUResource {
    
    private static final String PATH_TO_ELECTION_EVENT = "tenant/{tenantId}/electionevent/{electionEventId}";

    static final String SAVE_ELECTION_EVENT_DATA = PATH_TO_ELECTION_EVENT + "/adminboard/{adminBoardId}";

    static final String CHECK_IF_ELECTION_EVENT_DATA_IS_EMPTY = PATH_TO_ELECTION_EVENT + "/status";

    static final String RESOURCE_PATH = "/au/electioneventdata";

    // The name of the query parameter tenantId
    private static final String QUERY_PARAMETER_TENANT_ID = "tenantId";

    // The name of the query parameter electionEventId
    private static final String QUERY_PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    // The properties file reader.
    private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

    // The path to the resource electoral data.
    static final String ELECION_EVENT_DATA_PATH = PROPERTIES.getPropertyValue("ELECION_EVENT_DATA_PATH");

    private static final String QUERY_PARAMETER_ADMIN_BOARD_ID = "adminBoardId";

    private final XForwardedForFactory xForwardedForFactory = XForwardedForFactoryImpl.getInstance();

    private AuthenticationAdminClient authenticationAdminClient;

    private TrackIdGenerator trackIdGenerator;
    
    private static final Logger LOG = LoggerFactory.getLogger(ElectionEventDataAUResource.class);
    
    @Inject
    ElectionEventDataAUResource(AuthenticationAdminClient authenticationAdminClient,
                                TrackIdGenerator trackIdGenerator) {
        this.authenticationAdminClient = authenticationAdminClient;
        this.trackIdGenerator = trackIdGenerator;
    }

    @POST
    @Path(SAVE_ELECTION_EVENT_DATA)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveElectionEventData(@PathParam(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @PathParam(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @PathParam(QUERY_PARAMETER_ADMIN_BOARD_ID) final String adminBoardId,
            @NotNull final InputStream data,
            @Context final HttpServletRequest request) {
        String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
        String trackingId = trackIdGenerator.generate();

		RequestBody body = new InputStreamTypedOutput(MediaType.APPLICATION_JSON, data);

		try (ResponseBody responseBody = RetrofitConsumer
				.processResponse(authenticationAdminClient.saveElectionEventData(ELECION_EVENT_DATA_PATH, tenantId,
						electionEventId, adminBoardId, xForwardedFor, trackingId, body))) {
			return Response.ok().build();
		} catch (RetrofitException rfE) {
			LOG.error("Error trying to save election event data.", rfE);
			return Response.status(rfE.getHttpCode()).build();
		}  
    }

    /**
     * Returns the result of validate if the election event data for the Au context is empty.
     *
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @return Returns the result of the validation.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(CHECK_IF_ELECTION_EVENT_DATA_IS_EMPTY)
    public Response checkIfElectionEventDataIsEmpty(@PathParam(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @PathParam(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @Context final HttpServletRequest request) {
        String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
        String trackingId = trackIdGenerator.generate();

        try {
        	JsonObject processResponse = RetrofitConsumer.processResponse(
        			authenticationAdminClient.checkIfElectionEventDataIsEmpty(ELECION_EVENT_DATA_PATH, tenantId,
                            electionEventId, xForwardedFor, trackingId));
	        return Response.ok().entity(processResponse.toString()).build();
        } catch (RetrofitException rfE) {
			LOG.error("Error trying to check if election event data is empty.", rfE);
			return Response.status(rfE.getHttpCode()).build();
		}
    }
}
