/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * This package provides HTTP proxy support.
 */
package com.scytl.products.ov.ag.ui.ws.rs.proxy;
