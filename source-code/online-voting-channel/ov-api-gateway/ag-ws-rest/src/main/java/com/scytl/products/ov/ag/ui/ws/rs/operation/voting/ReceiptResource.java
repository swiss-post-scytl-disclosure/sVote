/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.operation.voting;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.ag.services.infrastructure.remote.voting.VotingWorkflowVotingClient;
import com.scytl.products.ov.ag.ui.ws.rs.RestApplication;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactory;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactoryImpl;
import com.scytl.products.ov.commons.beans.domain.model.receipt.Receipt;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import com.scytl.products.ov.commons.tracking.TrackIdGenerator;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

@Stateless(name = "ag-ReceiptsResource")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path(ReceiptResource.RESOURCE_PATH)
public class ReceiptResource {

    static final String RESOURCE_PATH = RestApplication.API_OV_VOTING_BASEURI + "/receipt";

    private final XForwardedForFactory xForwardedForFactory = XForwardedForFactoryImpl.getInstance();

    private VotingWorkflowVotingClient votingWorkflowVotingClient;

    // The properties file reader.
    private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

    static final String RECEIPT_PATH = PROPERTIES.getPropertyValue("RECEIPT_PATH");

    private TrackIdGenerator trackIdGenerator;

    private static final Logger LOG = LoggerFactory.getLogger(ReceiptResource.class);
    
	@Inject
	ReceiptResource(VotingWorkflowVotingClient votingWorkflowVotingClient, TrackIdGenerator trackIdGenerator) {
		this.votingWorkflowVotingClient = votingWorkflowVotingClient;
		this.trackIdGenerator = trackIdGenerator;
	}

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReceiptByVotingCardId(
            @HeaderParam(Constants.PARAMETER_VALUE_AUTHENTICATION_TOKEN) String authenticationTokenString,
            @PathParam(RestApplication.PARAMETER_VALUE_VERSION) final String version,
            @PathParam(Constants.PARAMETER_VALUE_TENANT_ID) String tenantId,
            @PathParam(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @PathParam(Constants.PARAMETER_VALUE_VOTING_CARD_ID) String votingCardId,
            @Context HttpServletRequest request) throws ApplicationException, ResourceNotFoundException {

        String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
        String trackingId = trackIdGenerator.generate();

		try {
			Receipt processResponse = RetrofitConsumer
					.processResponse(votingWorkflowVotingClient.getReceipt(RECEIPT_PATH, tenantId, electionEventId,
							votingCardId, authenticationTokenString, xForwardedFor, trackingId));
			return Response.ok().entity(processResponse).build();
		} catch (RetrofitException rfE) {
			LOG.error("Error trying to get receipt by voting card Id.", rfE);
			return Response.status(rfE.getHttpCode()).build();
		}
    }

}
