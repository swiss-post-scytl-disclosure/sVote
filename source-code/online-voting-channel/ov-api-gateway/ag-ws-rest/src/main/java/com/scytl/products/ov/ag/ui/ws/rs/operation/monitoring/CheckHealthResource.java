/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.operation.monitoring;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Service for connectivity validation purposes
 */
@Stateless(name = "agCheckHealth")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path(CheckHealthResource.RESOURCE_PATH)
public class CheckHealthResource {

    static final String RESOURCE_PATH = "/check";

    private static final String READY_PATH = "ready";

    /**
     * Checks for healthy of resource.
     * 
     * @return Returns a 200 response code, if the context is initialized properly.
     */

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(READY_PATH)
    public Response isApplicationRunning() {
        return Response.ok().build();
    }

    /**
     * Checks for healthy of resource. Maintained for backwards compatibility
     *
     * @return Returns a 200 response code, if the context is initialized properly.
     */

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response check() {
        return Response.ok().build();
    }
}
