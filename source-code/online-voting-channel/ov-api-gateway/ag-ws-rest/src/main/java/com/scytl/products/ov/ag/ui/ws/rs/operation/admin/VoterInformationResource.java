/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.operation.admin;

import com.google.gson.JsonObject;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.ag.services.infrastructure.remote.admin.VoterMaterialAdminClient;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactory;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactoryImpl;
import com.scytl.products.ov.commons.infrastructure.remote.client.RestClientInterceptor;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import com.scytl.products.ov.commons.tracking.TrackIdGenerator;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

/**
 * The Class VoterInformationResource.
 */
@Stateless(name = "ag-VoterInformationResource")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path(VoterInformationResource.RESOURCE_PATH)
public class VoterInformationResource {

    static final String GET_VOTING_CARDS_BY_QUERY =
        "tenant/{tenantId}/electionevent/{electionEventId}/votingcards/query";

    static final String RESOURCE_PATH = "/vm/informations";

    /** The Constant PATH_PARAMETER_TENANT_ID. */
    private static final String PATH_PARAMETER_TENANT_ID = "tenantId";

    /** The Constant PATH_PARAMETER_ELECTION_EVENT_ID. */
    private static final String PATH_PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    private static final String QUERY_PARAMETER_SEARCH_WITH_VOTING_CARD_ID = "id";

    /** The Constant QUERY_PARAMETER_OFFSET. */
    private static final String QUERY_PARAMETER_OFFSET = "offset";

    /** The Constant QUERY_PARAMETER_LIMIT. */
    private static final String QUERY_PARAMETER_SIZE = "size";

    /** The Constant PROPERTIES. */
    private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

    /** The Constant CREDENTIAL_INFORMATION_PATH. */
    static final String CREDENTIAL_INFORMATION_PATH =
        PROPERTIES.getPropertyValue("CREDENTIAL_INFORMATION_PATH");

    private final XForwardedForFactory xForwardedForFactory = XForwardedForFactoryImpl.getInstance();

    /** The voter material admin client. */
    private VoterMaterialAdminClient voterMaterialAdminClient;

    private TrackIdGenerator trackIdGenerator;

    private static final Logger LOG = LoggerFactory.getLogger(VoterInformationResource.class);
    
    @Inject
    VoterInformationResource(VoterMaterialAdminClient voterMaterialAdminClient,
                             TrackIdGenerator trackIdGenerator) {
        this.voterMaterialAdminClient = voterMaterialAdminClient;
        this.trackIdGenerator = trackIdGenerator;
    }

    /**
     * Search voting cards.
     *
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param termVotingcardId
     *            the votingcard id
     * @param offset
     *            the offset
     * @param sizeLimit
     *            the size limit
     * @return the response
     */
    @GET
    @Path(GET_VOTING_CARDS_BY_QUERY)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVotingCardsByQuery(@PathParam(PATH_PARAMETER_TENANT_ID) final String tenantId,
            @PathParam(PATH_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @QueryParam(QUERY_PARAMETER_SEARCH_WITH_VOTING_CARD_ID) final String termVotingcardId,
            @QueryParam(QUERY_PARAMETER_OFFSET) final String offset,
            @QueryParam(QUERY_PARAMETER_SIZE) final String sizeLimit, @Context final HttpServletRequest request) {
        String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
        String trackingId = trackIdGenerator.generate();

        String signature = request.getHeader(RestClientInterceptor.HEADER_SIGNATURE);
        String originator = request.getHeader(RestClientInterceptor.HEADER_ORIGINATOR);

        try {
        	JsonObject processResponse = RetrofitConsumer.processResponse(
        			voterMaterialAdminClient.getVotingCardsByQuery(CREDENTIAL_INFORMATION_PATH, tenantId,
        	                electionEventId, termVotingcardId, offset, sizeLimit, xForwardedFor, trackingId, originator, signature));
	        return Response.ok().entity(processResponse.toString()).build();
        } catch (RetrofitException rfE) {
     		LOG.error("Error trying to get voting cards by query.", rfE);
			return Response.status(rfE.getHttpCode()).build();
		}
    }
}
