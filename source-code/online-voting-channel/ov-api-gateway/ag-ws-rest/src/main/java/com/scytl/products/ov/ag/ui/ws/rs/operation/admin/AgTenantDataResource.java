/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.operation.admin;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.ag.services.infrastructure.remote.admin.AuthenticationAdminClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.ElectionInformationAdminClient;
import com.scytl.products.ov.ag.services.infrastructure.remote.admin.VoteVerificationAdminClient;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactory;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactoryImpl;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import com.scytl.products.ov.commons.tracking.TrackIdGenerator;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

/**
 * Resource to operate with tenant information related
 */
@Stateless(name = "ag-tenantDataResource")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path(AgTenantDataResource.RESOURCE_PATH)
public class AgTenantDataResource {

    static final String CHECK_TENANT_ACTIVATION = "activatetenant/tenant/{tenantId}";

    static final String RESOURCE_PATH = "/tenantdata";

    private VoteVerificationAdminClient voteVerificationAdminClient;

    private ElectionInformationAdminClient electionInformationAdminClient;

    private AuthenticationAdminClient authenticationAdminClient;

    // The properties file reader.
    private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

    // The uri base of the vote verification services.
    static final String PATH_TENANT_DATA = PROPERTIES.getPropertyValue("TENANT_DATA_PATH");

    private static final String TENANT_PARAMETER = "tenantId";

    private final XForwardedForFactory xForwardedForFactory = XForwardedForFactoryImpl.getInstance();

    private TrackIdGenerator trackIdGenerator;
    
    private static final Logger LOG = LoggerFactory.getLogger(AgTenantDataResource.class);
    
    @Inject
    AgTenantDataResource(AuthenticationAdminClient authenticationAdminClient,
                         ElectionInformationAdminClient electionInformationAdminClient,
                         VoteVerificationAdminClient voteVerificationAdminClient,
                         TrackIdGenerator trackIdGenerator) {
        this.authenticationAdminClient = authenticationAdminClient;
        this.electionInformationAdminClient = electionInformationAdminClient;
        this.voteVerificationAdminClient = voteVerificationAdminClient;
        this.trackIdGenerator = trackIdGenerator;
    }

    /**
     * Check if a tenant has been activated for the  contexts where the operation is available
     * @param tenantId - identifier of the tenant
     * @return
     */
    @GET
    @Path(CHECK_TENANT_ACTIVATION)
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkTenantActivation(@PathParam(TENANT_PARAMETER) final String tenantId,
                                          @Context final HttpServletRequest request){
        String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
        String trackingId = trackIdGenerator.generate();
        try {
	        JsonObject au = RetrofitConsumer.processResponse(authenticationAdminClient.checkTenantActivation(PATH_TENANT_DATA,tenantId,
	            xForwardedFor, trackingId));
	        JsonObject ei = RetrofitConsumer.processResponse(electionInformationAdminClient.checkTenantActivation(PATH_TENANT_DATA,tenantId,
	            xForwardedFor, trackingId));
	        JsonObject vv = RetrofitConsumer.processResponse(voteVerificationAdminClient.checkTenantActivation(PATH_TENANT_DATA,tenantId,
	            xForwardedFor, trackingId));
	
	        JsonArray array = new JsonArray();
	        array.add(au);
	        array.add(ei);
	        array.add(vv);
	
	        return Response.ok(array.toString()).build();
        } catch (RetrofitException rfE) {
        	LOG.error("Error trying to check tenant activation.", rfE);
			return Response.status(rfE.getHttpCode()).build();
        }
    }
}
