/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.operation.admin;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

import com.scytl.products.ov.ag.services.infrastructure.remote.admin.VoterMaterialAdminClient;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactory;
import com.scytl.products.ov.ag.ui.ws.rs.proxy.XForwardedForFactoryImpl;
import com.scytl.products.ov.commons.infrastructure.remote.client.InputStreamTypedOutput;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import com.scytl.products.ov.commons.tracking.TrackIdGenerator;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

/**
 * Web service for handling voter information data resource.
 */
@Stateless(name = "ag-VoterInformationDataResource")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path(VoterInformationDataResource.RESOURCE_PATH)
public class VoterInformationDataResource {

    static final String SAVE_VOTER_INFORMATION_DATA =
        "tenant/{tenantId}/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/adminboard/{adminBoardId}";

    static final String RESOURCE_PATH = "/vm/voterinformationdata";

    private static final String QUERY_PARAMETER_ADMIN_BOARD_ID = "adminBoardId";

    // The name of the query parameter tenantId
    private static final String QUERY_PARAMETER_TENANT_ID = "tenantId";

    // The name of the query parameter electionEventId
    private static final String QUERY_PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    // The name of the query parameter votingCardSetId
    private static final String QUERY_PARAMETER_VOTING_CARD_SET_ID = "votingCardSetId";

    // The properties file reader.
    private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

    // The path to the resource voter information data.
    static final String VOTER_INFORMATION_DATA_PATH = PROPERTIES.getPropertyValue("VOTER_INFORMATION_DATA_PATH");

    private final XForwardedForFactory xForwardedForFactory = XForwardedForFactoryImpl.getInstance();

    private VoterMaterialAdminClient voterMaterialAdminClient;

    private TrackIdGenerator trackIdGenerator;

    private static final Logger LOG = LoggerFactory.getLogger(VoterInformationDataResource.class);
    
    @Inject
    VoterInformationDataResource(VoterMaterialAdminClient voterMaterialAdminClient,
                                 TrackIdGenerator trackIdGenerator) {
        this.voterMaterialAdminClient = voterMaterialAdminClient;
        this.trackIdGenerator = trackIdGenerator;
    }

    @POST
    @Path(SAVE_VOTER_INFORMATION_DATA)
    @Consumes("text/csv")
    public Response saveVoterInformationData(@PathParam(QUERY_PARAMETER_TENANT_ID) final String tenantId,
            @PathParam(QUERY_PARAMETER_ELECTION_EVENT_ID) final String electionEventId,
            @PathParam(QUERY_PARAMETER_VOTING_CARD_SET_ID) final String votingCardSetId,
            @PathParam(QUERY_PARAMETER_ADMIN_BOARD_ID) final String adminBoardId,
            @NotNull final InputStream info,
            @Context final HttpServletRequest request) {
        String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
        String trackingId = trackIdGenerator.generate();

        RequestBody body = new InputStreamTypedOutput("text/csv", info);

        try (ResponseBody responseBody = RetrofitConsumer.processResponse(
        			voterMaterialAdminClient.saveVoterInformationData(VOTER_INFORMATION_DATA_PATH,
        		            tenantId, electionEventId, votingCardSetId, adminBoardId, xForwardedFor, trackingId, body))) {
	        return Response.ok().build();
        } catch (RetrofitException rfE) {
     		LOG.error("Error trying to save voter information data.", rfE);
			return Response.status(rfE.getHttpCode()).build();
		}
    }
}
