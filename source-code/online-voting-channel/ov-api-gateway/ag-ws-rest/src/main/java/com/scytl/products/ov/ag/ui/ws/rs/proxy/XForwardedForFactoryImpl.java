/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ag.ui.ws.rs.proxy;

import javax.servlet.http.HttpServletRequest;

/**
 * Implementation of {@link XForwardedForFactory}.
 */
public final class XForwardedForFactoryImpl implements XForwardedForFactory {
    public static final String HEADER = "X-Forwarded-For";

    private static final XForwardedForFactoryImpl INSTANCE = new XForwardedForFactoryImpl();

    private XForwardedForFactoryImpl() {
    }

    /**
     * Returns the instance.
     *
     * @return the instance.
     */
    public static XForwardedForFactoryImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public String newXForwardedFor(final HttpServletRequest request) {
        String remoteAddresses = request.getHeader(HEADER);
        if (remoteAddresses == null) {
            remoteAddresses = request.getRemoteAddr();
        }
        String localAddress = request.getLocalAddr();
        return remoteAddresses + ',' + localAddress;
    }
}
