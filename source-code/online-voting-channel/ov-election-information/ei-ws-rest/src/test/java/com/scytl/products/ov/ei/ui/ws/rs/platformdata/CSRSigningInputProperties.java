/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.platformdata;

import java.time.ZonedDateTime;

import com.scytl.products.ov.commons.crypto.configuration.CertificateParameters;

public final class CSRSigningInputProperties {

    private final ZonedDateTime _notBefore;

    private final ZonedDateTime _notAfter;

    private final CertificateParameters.Type _type;

    public CSRSigningInputProperties(final ZonedDateTime notBefore, final ZonedDateTime notAfter,
            final CertificateParameters.Type type) {
        _notBefore = notBefore;
        _notAfter = notAfter;
        _type = type;
    }

    public ZonedDateTime getNotBefore() {
        return _notBefore;
    }

    public ZonedDateTime getNotAfter() {
        return _notAfter;
    }

    public CertificateParameters.Type getType() {
        return _type;
    }
}
