/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.application.execution;

import org.junit.Test;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;

/**
 * Junit test for the class {@link ValidationResource}
 */
public class ValidationResourceTest {

    /**
     * Test method for
     * {@link ValidationResource#validate(com.scytl.products.ov.ei.services.domain.model.vote.Vote)}
     * .
     * 
     * @throws ApplicationException
     */
    @Test
    public void testValidatetenantIdNull() throws ApplicationException {
        final Vote validateInput = new Vote();
        validateInput.setTenantId(null);
    }
}
