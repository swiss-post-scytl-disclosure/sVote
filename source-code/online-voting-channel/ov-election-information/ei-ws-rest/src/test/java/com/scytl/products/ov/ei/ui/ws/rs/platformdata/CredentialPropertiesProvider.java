/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.platformdata;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import com.scytl.products.ov.commons.crypto.configuration.CredentialProperties;
import com.scytl.products.ov.commons.util.ConfigObjectMapper;

public class CredentialPropertiesProvider {
	private ConfigurationInput configurationInput;

	public CredentialPropertiesProvider() {
		URL url = this.getClass().getResource("/test_keys_config.json");

		ConfigObjectMapper _configObjectMapper = new ConfigObjectMapper();
		try {
			configurationInput = _configObjectMapper.fromJSONFileToJava(new File(url.toURI()), ConfigurationInput.class);
		} catch (IOException | URISyntaxException e) {
			throw new RuntimeException("An error occurred while reading the internal configuration file");
		}
	}

	public CredentialProperties getPlatformRootCredentialPropertiesFromClassPath() {
		return configurationInput.getPlatformRootCA();
	}

	public CredentialProperties getTenantCredentialPropertiesFromClassPath() {
		return configurationInput.getTenantCA();
	}

	public CredentialProperties getSystemCredentialPropertiesFromClassPath() {
		return configurationInput.getSystemServicesEncryptionCert();
	}

	public CredentialProperties getServicesLoggingSigningCredentialPropertiesFromClassPath() {
		return configurationInput.getLoggingServicesSignerCert();
	}

	public CredentialProperties getServicesLoggingEncryptionCredentialPropertiesFromClassPath() {
		return configurationInput.getLoggingServicesEncryptionCert();
	}
}