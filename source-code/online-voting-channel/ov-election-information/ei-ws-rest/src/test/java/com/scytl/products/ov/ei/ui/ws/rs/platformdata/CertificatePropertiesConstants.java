/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.platformdata;

public class CertificatePropertiesConstants {

	public static final String SUBJECT_COMMON_NAME_PROPERTY_NAME = "subject.common.name";

	public static final String SUBJECT_ORGANIZATIONAL_UNIT_PROPERTY_NAME = "subject.organizational.unit";

	public static final String SUBJECT_ORGANIZATION_PROPERTY_NAME = "subject.organization";

	public static final String SUBJECT_COUNTRY_PROPERTY_NAME = "subject.country";

	public static final String PRIVATE_KEY_ALIAS = "privateKey";

	public static final String START_DATE = "start";

	public static final String END_DATE = "end";
}
