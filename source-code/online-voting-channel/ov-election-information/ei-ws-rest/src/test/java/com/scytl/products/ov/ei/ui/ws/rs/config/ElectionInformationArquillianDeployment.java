/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.config;

import static org.mockito.Mockito.mock;

import com.scytl.cryptolib.asymmetric.service.AsymmetricService;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.cryptolib.proofs.service.ProofsService;
import com.scytl.cryptolib.scytl.keystore.service.ScytlKeyStoreService;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.dto.mixing.MixingPayload;
import com.scytl.products.ov.commons.logging.producers.SecureLoggerProducer;
import com.scytl.products.ov.commons.logging.producers.SplunkableLoggerProducer;
import com.scytl.products.ov.commons.logging.producers.TransactionInfoProviderProducer;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.ApplicationExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.DuplicateEntryExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.GlobalExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.ResourceNotFoundExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.RetrofitErrorHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.ValidationExceptionHandler;
import com.scytl.products.ov.commons.beans.domain.model.ballotbox.BallotBoxId;
import com.scytl.products.ov.ei.services.domain.service.ballotbox.CleansedBallotBoxService;
import com.scytl.products.ov.ei.services.domain.service.ballotbox.CleansedBallotBoxServiceImpl;
import com.scytl.products.ov.ei.services.infrastructure.health.HealthCheckRegistryProducer;

import java.io.IOException;
import java.io.OutputStream;
import java.security.Security;
import java.util.stream.Stream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;

@ArquillianSuiteDeployment
public class ElectionInformationArquillianDeployment {

    private static SystemPropertiesLoader spl = new SystemPropertiesLoader();

    public static class CleansedBallotBoxServiceTestImpl implements CleansedBallotBoxService {
        @Override
        public void writeEncryptedBallotBox(OutputStream stream, String tenantId, String electionEventId,
                                            String ballotBoxId) throws IOException {
            stream.write("cleansed-bb".getBytes());
        }

        @Override
        public Stream<MixingPayload> getMixingPayloads(BallotBoxId ballotBoxId) {
            return Stream.of(mock(MixingPayload.class));
        }

        @Override
        public void storeCleansedVote(Vote vote) {
        }

        @Override
        public void storeSuccessfulVote(String tenantId, String electionEventId, String ballotBoxId,
                                        String votingCardId, String receipt) {
        }
    }

    @Deployment
    public static WebArchive buildDeployable() {
        beforeDeployment();
        WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "ei.war");
        return webArchive
                .addPackage("com.scytl.products.oscore")
                .addPackages(true,
                Filters.exclude(ScytlKeyStoreService.class, AsymmetricService.class, PrimitivesService.class,
                    ProofsService.class),
                "com.scytl.cryptolib")
                .addPackages(true, "org.h2")
                .addPackages(true, "com.scytl.products.ov.ei.services")
                .addPackages(true, "com.scytl.products.ov.ei.ui")
                .addPackages(true, "com.scytl.products.ov.commons.logging.service")
                .addPackages(true, "com.scytl.products.ov.commons.tracking")
                .addPackages(true, "com.fasterxml.jackson.jaxrs")
                .addPackages(true, "com.scytl.math")
                .addPackages(true,
                Filters.exclude(ResourceNotFoundExceptionHandler.class, GlobalExceptionHandler.class,
                    ValidationExceptionHandler.class, ApplicationExceptionHandler.class,
                    DuplicateEntryExceptionHandler.class, RetrofitErrorHandler.class, BouncyCastleProvider.class,
                    ScytlKeyStoreService.class),
                "com.scytl.products.ov.commons")
                .addClasses(SecureLoggerProducer.class, SecureLoggingWriter.class, TransactionInfoProviderProducer.class,
                SplunkableLoggerProducer.class, org.slf4j.Logger.class)
                .deleteClass(HealthCheckRegistryProducer.class)
                .addAsResource("log4j.xml")
                .addAsWebInfResource("beans.xml")
                .addAsManifestResource("test-persistence.xml", "persistence.xml")
                .deleteClass(CleansedBallotBoxServiceImpl.class);
    }

    private static void beforeDeployment(){
        spl.setProperties();
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
}
