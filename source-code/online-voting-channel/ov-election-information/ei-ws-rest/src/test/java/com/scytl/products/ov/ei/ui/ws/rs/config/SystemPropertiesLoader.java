/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.config;

import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

public class SystemPropertiesLoader {

    public static final String FILE_NAME = "arq_system.properties";

    public static final String ABSOLUTEPATH = Paths.get("target").toFile().getAbsolutePath();

    public void setProperties() {
        Properties props = load();
        if (props != null) {
            for (Map.Entry<Object, Object> entry : props.entrySet()) {
                String string = entry.getValue().toString();
                String replace = string.replace("{testFolder}", ABSOLUTEPATH);
                System.setProperty(entry.getKey().toString(), replace);
            }
        }
    }

    public Properties load() {
        try (InputStream propsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(FILE_NAME)) {
            if (propsStream != null) {
                Properties props = new Properties();
                props.load(propsStream);
                return props;
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new RuntimeException("Could not load properties", e);
        }
    }
}