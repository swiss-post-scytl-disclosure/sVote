/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.application.operation;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scytl.products.ov.commons.streaming.JsonStreamingOutput;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.Test;

public class JsonStreamingOutputTest {
    @Test
    public void produceAJSONArray() throws IOException {
        JsonStreamingOutput<Integer> jso =
                new JsonStreamingOutput<>(IntStream.range(0, 10).boxed());

        try (OutputStream os = new ByteArrayOutputStream()) {
            jso.write(os);

            // Read the stream back.
            String jsonString = os.toString();
            ObjectMapper objectMapper = new ObjectMapper();
            JavaType type = objectMapper.getTypeFactory()
                    .constructCollectionType(List.class, Integer.class);
            List<Integer> intList = objectMapper.readValue(jsonString, type);

            assertThat(intList.get(0), instanceOf(Integer.class));
            assertThat(intList.get(0), is(0));
        }
    }
}
