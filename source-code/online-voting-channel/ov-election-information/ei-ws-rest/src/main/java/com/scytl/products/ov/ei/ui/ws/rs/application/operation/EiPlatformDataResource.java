/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.ui.ws.rs.application.operation;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.cryptolib.certificates.bean.X509CertificateType;
import com.scytl.products.ov.commons.beans.domain.model.platform.PlatformInstallationData;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.validation.CertificateValidationService;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;
import com.scytl.products.ov.commons.domain.model.platform.PlatformCARepository;
import com.scytl.products.ov.commons.domain.model.platform.PlatformInstallationDataHandler;
import com.scytl.products.ov.commons.logging.config.SecureLoggerInitializer;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreRepository;
import com.scytl.products.ov.ei.services.domain.model.platform.EiCertificateValidationService;
import com.scytl.products.ov.ei.services.domain.model.platform.EiLoggingKeystoreEntity;
import com.scytl.products.ov.ei.services.domain.model.platform.EiLoggingKeystoreRepository;
import com.scytl.products.ov.ei.services.domain.model.platform.EiPlatformCARepository;
import com.scytl.products.ov.ei.services.domain.model.platform.EiPlatformCaEntity;
import com.scytl.products.ov.ei.services.infrastructure.log.EiLoggingInitializationState;

/**
 * Endpoint for upload the information during the installation of the platform in the system
 */
@Path("platformdata")
@Stateless(name = "ei-platformDataResource")
public class EiPlatformDataResource {

	private static final Logger LOG = LoggerFactory.getLogger(EiPlatformDataResource.class);

    private static final String CONTEXT = "EI";

    private static final String ENCRYPTION_PW_PROPERTIES_KEY = "EI_log_encryption";

    private static final String SIGNING_PW_PROPERTIES_KEY = "EI_log_signing";   

    @EJB
    @EiPlatformCARepository
    PlatformCARepository platformRepository;

    @EJB
    @EiCertificateValidationService
    CertificateValidationService certificateValidationService;

    @EJB
    @EiLoggingKeystoreRepository
    LoggingKeystoreRepository loggingKeystoreRepository;

    @EJB
    EiLoggingInitializationState loggingInitializationState;

    /**
     * Installs the logging keystores and platform CA in the service. Additionally, this method also attempts to
     * initialize the secure logging system.
     *
     * @param data
     *            all the platform data.
     * @throws CryptographicOperationException
     * @throws DuplicateEntryException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response savePlatformData(final PlatformInstallationData data)
            throws CryptographicOperationException, DuplicateEntryException {

        try {

            String platformName = PlatformInstallationDataHandler.savePlatformCertificateChain(data, platformRepository,
                certificateValidationService, new EiPlatformCaEntity(), new EiPlatformCaEntity());

            String encryptionKey = data.getLoggingEncryptionKeystoreBase64();
            String signingKey = data.getLoggingSigningKeystoreBase64();

            EiLoggingKeystoreEntity encryptionKeystore = new EiLoggingKeystoreEntity();
            encryptionKeystore.setPlatformName(platformName);
            encryptionKeystore.setKeystoreContent(encryptionKey);
            encryptionKeystore.setKeyType(X509CertificateType.ENCRYPT.name());
            loggingKeystoreRepository.save(encryptionKeystore);

            EiLoggingKeystoreEntity signingKeystore = new EiLoggingKeystoreEntity();
            signingKeystore.setPlatformName(platformName);
            signingKeystore.setKeystoreContent(signingKey);
            signingKeystore.setKeyType(X509CertificateType.SIGN.name());
            loggingKeystoreRepository.save(signingKeystore);

            SecureLoggerInitializer secureLoggerInitializer = new SecureLoggerInitializer(loggingInitializationState,
                loggingKeystoreRepository, CONTEXT, ENCRYPTION_PW_PROPERTIES_KEY, SIGNING_PW_PROPERTIES_KEY);

            secureLoggerInitializer.initializeFromUploadData(encryptionKey, signingKey);
            LOG.info("EI - logging initialized successfully");
            loggingInitializationState.setInitialized(true);

            return Response.ok().build();

        } catch (Exception e) {
            throw new IllegalStateException("EI - error while trying to initialize logging", e);
        }
    }
}
