Given REST call *store_vote* with URL */ei-ws-rest/votes/*
Given REST call *store_vote* with Method *POST*
Given REST call *store_vote* with Header *requestid* / *100*
Given REST call *store_vote* with Header *Content-Type* / *application/json*
Given REST call *store_vote* with JSON Body:
{
	"tenantId": "100",
	"ballotId": "100",
	"encryptedOptions": "132412342134;asdfasdfasdf.AVeryLongStringWithTheEncryptedOptionsafasdf12312423asdfnmlourjvlmaldjfoqwer14ad34fas1avjsjjj;.",
	"partialChoiceCodes": "a1ds23f1as3df13asdf3aasdfasfdGeneratedPartialChoiceCodesadsfasdfasdfasdf",
	"verificationCardPublicKey": "ELGAMALPublicKey",
	"verificationCardPKSignature": "ShdBnw/SS/KykomtFT8HlqFfBlCDFusjPqek0kpUnACY71VKv7y5fn4bcr1zOyF3OiIjwrRunzG681OTs/x4YJJ5l5mS415zfiAbGxSSwk8l6N8xO5EKyb9J2pTF",
	"signature": "adsfadsfadsfw/AdsfS/Kasdfa2sdaftFT8HlqFfBlCDFusjPqek0kpUnACY71VKv7y5fn4bcr1zOyF3OiIjwrRunzG681OTs/x4YJJ5l5mS415zfiAbGxSSwk8l6N8xO5EKyb9J2pTF",
	"certificate": "-----BEGIN CERTIFICATE-----MIIDLjCCAhagAwIBAgIUGQCUu+T3D5YHCzUd2nRXiSZM6KswDQYJKoZIhvcNAQELBQAwPTENMAsGA1UEAwwEcm9vdDEJMAcGA1UECwwAMQkwBwYDVQQKDAAxCTAHBgNVBAcMADELMAkGA1UEBhMCRVMwHhcNMTUwNTI2MTExNjQ0WhcNMTYwNTI2MTExNjQ0WjBDMRMwEQYDVQQDDApzZXJ2ZXJhdXRoMQkwBwYDVQQLDAAxCTAHBgNVBAoMADEJMAcGA1UEBwwAMQswCQYDVQQGEwJFUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK+EZfZtdejMF2tB24TDOLMGTcWiwtcyr35rvjjbDv22ECauR5Lbb9sNsZkSrJyBjox81YqaCHjrAguG4BzgL5o77Eu0b+8ZuHPBfInB9j+PgTzuApucYRtA6Qe67fUq7n3NLb8xf19FHmE91ht3VVuUeooiYy+koxZ9hlMCcHAQy4RxJ+DjKG6n+UXT830RvEO5jJq7NTTyVa3Tk4h+AbZdg0HHFA3QYc1CNUqgX4lVjciNtZzwwf2SSRCvvF8yhzYGc1JCIQsmZ5lUORW3k75zE279c3XzzJqyZK9r29F1k/EBdDNNVSyv4vS24RJrtTmDFdQxYqnPkgan+ZrishECAwEAAaMgMB4wDgYDVR0PAQH/BAQDAgbAMAwGA1UdEwEB/wQCMAAwDQYJKoZIhvcNAQELBQADggEBAFbGTqg3kPQj4ygKnjJPctqtSHeDg9GwDPavNUL317ElwhHOFmH4EwjdW94IA0e6b4wKmEbDoQqfIzFnNG15gDEh2JQQneS7j2g+U8v+qsn+lfAMTkPBbYbXrzrfXjWjERXTXK/lAzsOBZK28DCLhkdW59szAWKq3HqN7fcS7HsnTyyElq9ghIf+0jILdiZrHW6n/1whEaaTQwoq4g9Zydnrp/Z9LLlNVyzDs67kA0YnYtV7+s9vLeikKw9Qj8IgRMiYBcoQZTJBVBL06vVqNKzCd51jjyuqFmBtd7KYjq5JBNyl9ZBTx4yYVoCv1zzsB3j2D2jJU3qIpCO+mKj0EXk=-----END CERTIFICATE-----",
	"credentialId": "100",
	"ballotBoxId": "100"
}
When REST call *store_vote* execute
Then REST call *store_vote* response Status Code is *200*
Then REST call *store_vote* response Body contains *value ==~ /\w+/*
