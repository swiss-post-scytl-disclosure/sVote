/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 4/10/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.ei.services.domain.model.validation;

/**
 * Class used for validation purposes
 */
public class ElectionValidationRequest {

    private final String tenantId;

    private final String electionEventId;

    private final String ballotBoxId;

    private final boolean validatedWithGracePeriod;

    private ElectionValidationRequest(final String tenantId, final String electionEventId,final String ballotBoxId,final  boolean validatedWithGracePeriod) {
        this.tenantId = tenantId;
        this.electionEventId = electionEventId;
        this.ballotBoxId = ballotBoxId;
        this.validatedWithGracePeriod = validatedWithGracePeriod;
    }

    public static  ElectionValidationRequest create(final String tenantId, final String electionEventId,final String ballotBoxId,final  boolean validateWithGracePeriod){
        return  new ElectionValidationRequest(tenantId, electionEventId, ballotBoxId, validateWithGracePeriod);
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public boolean isValidatedWithGracePeriod() {
        return validatedWithGracePeriod;
    }
}
