/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.rule;

import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.domain.model.rule.AbstractRule;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.ei.services.domain.model.validation.ElectionValidationRequest;
import com.scytl.products.ov.ei.services.domain.service.election.ElectionService;

/**
 * Rule which validates if the election is active when the vote is about to be saved in the Ballot Box. It takes into
 * account the gracePeriod
 */
public class ElectionDatesRule implements AbstractRule<Vote> {

    @Inject
    private ElectionService electionService;

    /**
     * This method validates whether the ballot box is still active before storing the vote
     * 
     * @param vote
     *            - the vote to be validated.
     * @return A ValidationError containing information about the execution of the rule. If fails, the dates of election
     *         are added as additional information.
     */
    @Override
    public ValidationError execute(Vote vote) {
        ElectionValidationRequest request =
            ElectionValidationRequest
                .create(vote.getTenantId(), vote.getElectionEventId(), vote.getBallotBoxId(), true);
        return electionService.validateIfElectionIsOpen(request);
    }

    /**
     * @see com.scytl.products.ov.commons.domain.model.rule.AbstractRule#getName()
     */
    @Override
    public String getName() {
        return RuleNames.VOTE_ELECTION_DATES.getText();
    }
}
