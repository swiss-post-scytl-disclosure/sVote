/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.certificate;

import javax.ejb.Stateless;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Stateless
public class X509CertificateService {

	private final CertificateFactory factory;

	public X509CertificateService() throws CertificateException {
		factory = CertificateFactory.getInstance("X.509");
	}

	public X509Certificate generateCertificate(InputStream inputStream) throws CertificateException {
		return (X509Certificate) factory.generateCertificate(inputStream);
	}
}
