/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.common.csv;

import java.io.OutputStream;

import com.scytl.products.ov.commons.infrastructure.csv.AbstractCSVWriter;
import com.scytl.products.ov.commons.infrastructure.csv.CSVConstants;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.SuccessfulVoteItem;

public class SuccessfulVotesItemWriter extends AbstractCSVWriter<SuccessfulVoteItem> {

    public SuccessfulVotesItemWriter(final OutputStream outputStream) {
        super(outputStream, CSVConstants.DEFAULT_CHARSET, CSVConstants.SEMICOLON_SEPARATOR,
            CSVConstants.NO_QUOTE_CHARACTER, CSVConstants.NO_ESCAPE_CHARACTER);
    }

    @Override
    protected String[] extractValues(SuccessfulVoteItem object) {
        return new String[] {object.getVotingCardId(), object.getTimestamp().toString(), object.getReceipt() };
    }
}
