/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.interfaces.ballot.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Dto class which represents the information of a ballot configuration.
 */
public class BallotDto {

	/* The ballot id. */
	@NotNull
	private String ballotId;

	/* The election event id. */
	@NotNull
	private String electionEventId;

	/* List of the election to be included in a ballot configuration. */
	private List<BallotElectionDto> elections;

	/* The tenant id of the ballot */
	@NotNull
	private String tenantId;

	/**
	 * Returns the current value of the field electionEventId.
	 *
	 * @return Returns the electionEventId.
	 */
	public String getElectionEventId() {
		return electionEventId;
	}

	/**
	 * Sets the value of the field electionEventId.
	 *
	 * @param electionEventId The electionEventId to set.
	 */
	public void setElectionEventId(String electionEventId) {
		this.electionEventId = electionEventId;
	}

	/**
	 * Returns the current value of the field elections.
	 *
	 * @return Returns the elections.
	 */
	public List<BallotElectionDto> getElections() {
		return elections;
	}

	/**
	 * Sets the value of the field elections.
	 *
	 * @param elections The elections to set.
	 */
	public void setElections(List<BallotElectionDto> elections) {
		this.elections = elections;
	}

	/**
	 * Returns the current value of the field tenantId.
	 *
	 * @return Returns the tenantId.
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Sets the value of the field tenantId.
	 *
	 * @param tenantId The tenantId to set.
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * Returns the current value of the field ballotId.
	 *
	 * @return Returns the ballotId.
	 */
	public String getBallotId() {
		return ballotId;
	}

	/**
	 * Sets the value of the field ballotId.
	 *
	 * @param ballotId The ballotId to set.
	 */
	public void setBallotId(String ballotId) {
		this.ballotId = ballotId;
	}
}
