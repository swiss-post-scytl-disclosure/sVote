/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.validation;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for storing vote validation result.
 */
@Local
public interface VoteValidationRepository extends BaseRepository<VoteValidation, Integer> {

	/**
	 * Returns the result of finding the vote validation by the input parameters.
	 * 
	 * @param tenantId the tenant id.
	 * @param electionEventId the election event id.
	 * @param votingCardId the voting card id.
	 * @param voteHash the hash of the vote.
	 * @return the vote validation if found.
	 * @throws ResourceNotFoundException
	 */
	VoteValidation findByTenantIdElectionEventIdVotingCardId(String tenantId, String electionEventId,
			String votingCardId, String voteHash) throws ResourceNotFoundException;
}
