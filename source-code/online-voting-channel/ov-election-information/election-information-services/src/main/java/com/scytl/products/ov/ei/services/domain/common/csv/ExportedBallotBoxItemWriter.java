/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.common.csv;

import java.io.OutputStream;

import com.scytl.products.ov.commons.infrastructure.csv.AbstractCSVWriter;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.ExportedBallotBoxItem;

/**
 * Writer in csv for every item to be downloaded of a ballot box
 */
public class ExportedBallotBoxItemWriter
        extends AbstractCSVWriter<ExportedBallotBoxItem> {


    public ExportedBallotBoxItemWriter(final OutputStream outputStream) {
        super(outputStream);
    }

    /**
     * extract the values of a ballot box item into an array of Strings
     * @param exportedBallotBoxItem - ballot box item from which the info is extracted
     * @return
     */
    @Override
    public String[] extractValues(
            final ExportedBallotBoxItem exportedBallotBoxItem) {

        return new String[] {exportedBallotBoxItem.getVote(),
                exportedBallotBoxItem.getVoteCastCode(),
                exportedBallotBoxItem.getSignature(),
                exportedBallotBoxItem.getVoteComputationResults(),
                exportedBallotBoxItem.getCastCodeComputationResults(),
                exportedBallotBoxItem.getTenantId(),
                exportedBallotBoxItem.getElectionEventId(),
                exportedBallotBoxItem.getVotingCardId(),
                exportedBallotBoxItem.getBallotId(),
                exportedBallotBoxItem.getBallotBoxId() };

    }

}
