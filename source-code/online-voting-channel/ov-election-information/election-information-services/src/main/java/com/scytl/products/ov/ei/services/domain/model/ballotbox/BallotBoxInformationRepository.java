/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballotbox;

import java.io.IOException;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Provides extra operations on the ballot box information repository.
 */
@Local
public interface BallotBoxInformationRepository extends BaseRepository<BallotBoxInformation, Integer> {

	/**
	 * Searches for a ballot with the given tenant, election event and ballot box id.
	 * 
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the tenant.
	 * @param ballotBoxId - the external identifier of the ballot.
	 * @return a entity representing the ballot.
	 */
	BallotBoxInformation findByTenantIdElectionEventIdBallotBoxId(String tenantId, String electionEventId,
			String ballotBoxId) throws ResourceNotFoundException;

	
	/**
	 * Adds "Ballot Box Information" for a ballot box to the repository
	 *
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the electionEvent.
	 * @param ballotBoxId - the identifier of the ballot box.
	 * @param jsonContent - JSON serialized content to add
	 * @return void
	 */
	 void addBallotBoxInformation(String tenantId, String electionEventId,
			String ballotBoxId, String jsonContent) throws DuplicateEntryException, IOException;
}
