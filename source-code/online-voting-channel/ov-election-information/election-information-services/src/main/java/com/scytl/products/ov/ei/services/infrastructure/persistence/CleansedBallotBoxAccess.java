/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.CleansedBallotBox;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.CleansedBallotBoxRepository;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@javax.ejb.ApplicationException(rollback = true)
public class CleansedBallotBoxAccess {

    @EJB
    private CleansedBallotBoxRepository cleansedBallotBoxRepository;

    public CleansedBallotBox save(CleansedBallotBox ballotBox) throws DuplicateEntryException {
        return cleansedBallotBoxRepository.save(ballotBox);
    }
}
