/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.ballotbox;

import com.scytl.products.ov.commons.beans.domain.model.ballotbox.BallotBoxId;
import com.scytl.products.ov.commons.beans.domain.model.messaging.PayloadSignatureException;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.CleansedBallotBoxServiceException;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.dto.mixing.MixingPayload;

import java.io.IOException;
import java.io.OutputStream;
import java.util.stream.Stream;

/**
 * Interface defining operations to handle ballot box.
 */
public interface CleansedBallotBoxService {

    /**
     * Writes the specified signed encrypted ballot box to a given stream.
     *
     * @param stream
     *            the stream
     * @param tenantId
     *            the tenant identifier
     * @param electionEventId
     *            the election event identifier
     * @param ballotBoxId
     *            the ballot box identifier
     * @throws IOException
     *             I/O error occurred.
     */
    void writeEncryptedBallotBox(OutputStream stream, String tenantId, String electionEventId, String ballotBoxId)
            throws IOException;

    /**
     * Retrieves a full ballot box as a stream of signed payloads for mixing, with a signature and
     * the public counterpart of the key used to generate it, so that the integrity of the vote set
     * can be verified.
     *
     * @param ballotBoxId an identifier to reference a ballot box.
     * @return a collection of signed payloads representing the whole ballot box
     * @throws PayloadSignatureException the a payload could not be signed
     * @throws ResourceNotFoundException if the ballot box or another internal entity were
     *         not found
     * @throws CleansedBallotBoxServiceException 
     */
    Stream<MixingPayload> getMixingPayloads(BallotBoxId ballotBoxId)
            throws PayloadSignatureException, ResourceNotFoundException, CleansedBallotBoxServiceException;

    /**
     * Stores the vote as cleansed
     * 
     * @param vote
     *            to be stored as cleansed
     * @throws DuplicateEntryException
     *             if the cleansed vote already exists
     */
    void storeCleansedVote(Vote vote) throws DuplicateEntryException;

    /**
     * Store an entry of a successful vote. This entry is composed by the voting
     * card ID, a timestamp and the vote receipt.
     * 
     * @param votingCardId
     *            the voting card ID
     * @param receipt
     *            the vote receipt
     * @throws DuplicateEntryException
     */
    void storeSuccessfulVote(String tenantId, String electionEventId, String ballotBoxId, String votingCardId,
            String receipt) throws DuplicateEntryException;
}
