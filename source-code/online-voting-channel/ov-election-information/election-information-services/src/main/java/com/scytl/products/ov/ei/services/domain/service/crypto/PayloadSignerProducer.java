/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.crypto;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.products.ov.commons.sign.CryptolibPayloadSigner;
import com.scytl.products.ov.commons.sign.PayloadSigner;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class PayloadSignerProducer {

    @Inject
    private AsymmetricServiceAPI asymmetricService;

    @Produces
    public PayloadSigner getPayloadSigner() {
        return new CryptolibPayloadSigner(asymmetricService);
    }
}
