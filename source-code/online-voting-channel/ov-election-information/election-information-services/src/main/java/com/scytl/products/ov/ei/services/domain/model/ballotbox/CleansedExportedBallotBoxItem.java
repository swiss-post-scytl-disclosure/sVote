/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballotbox;

public class CleansedExportedBallotBoxItem {

    private final String vote;

    private CleansedExportedBallotBoxItem(final String vote) {
        super();
        this.vote = vote;
    }

    public String getVote() {
        return vote;
    }

    public static class CleansedExportedBallotBoxItemBuilder {

        private String vote;

        public CleansedExportedBallotBoxItemBuilder() {

        }

        public CleansedExportedBallotBoxItemBuilder setVote(final String vote) {
            this.vote = vote;
            return this;
        }

        public CleansedExportedBallotBoxItem build() {
            return new CleansedExportedBallotBoxItem(vote);
        }

    }

}
