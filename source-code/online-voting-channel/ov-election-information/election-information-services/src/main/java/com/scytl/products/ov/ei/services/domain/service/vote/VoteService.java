/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.vote;

import com.scytl.products.ov.commons.beans.domain.model.receipt.Receipt;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.exceptions.ValidationException;
import com.scytl.products.ov.commons.dto.VoteAndComputeResults;

/**
 * Interface that performs operations regarding the votes of a ballot box
 */
public interface VoteService {

    /**
     * Requests the receipt of a vote for a given tenant, election event and
     * voting card
     * 
     * @param tenantId
     * @param electionEventId
     * @param votingCardId
     * @return
     * @throws ResourceNotFoundException
     * @throws ApplicationException
     */
    Receipt getVoteReceipt(String tenantId, String electionEventId, String votingCardId)
            throws ResourceNotFoundException, ApplicationException;

    /**
     * Saves a vote with it's asociated token and returns a verifiable receipt
     * in exchange
     * 
     * @param vote
     *            the vote to be saved
     * @param authenticationToken
     *            the token used to send the vote
     * @return the receipt asociated to the vote storage
     * @throws ValidationException
     *             if there is any problem validating the vote
     * @throws DuplicateEntryException
     *             if there was already a vote for this voter in the ballot box
     * @throws ApplicationException
     *             if there is an error generating the receipt
     * @throws ResourceNotFoundException
     *             basic information needed to process this vote could not be
     *             found
     */
    Receipt saveVoteGenerateReceipt(VoteAndComputeResults vote, String authenticationToken)
            throws DuplicateEntryException, ValidationException, ApplicationException, ResourceNotFoundException;

    VoteAndComputeResults retrieveVote(String tenantId, String electionEventId, String votingCardId)
            throws ApplicationException, ResourceNotFoundException;
}
