/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.validation;

import java.util.ArrayList;
import java.util.List;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.domain.model.rule.AbstractRule;

/**
 * Entity holding the result of a validation.
 */
public class ValidationResult {

    /**
     * The final result of the validation. If no validation failed, then the
     * result is true. If there are rules that failed, result is false.
     */
    private boolean result;

    /**
     * The list with failed rules, if any.
     */
    private List<AbstractRule<Vote>> failedRulesList = new ArrayList<>();

    /**
     * Returns the current value of the field result.
     *
     * @return Returns the result.
     */
    public boolean isResult() {
        return result;
    }

    /**
     * Sets the value of the field result.
     *
     * @param result
     *            The result to set.
     */
    public void setResult(boolean result) {
        this.result = result;
    }

    /**
     * Returns the current value of the field failedRulesList.
     *
     * @return Returns the failedRulesList.
     */
    public List<AbstractRule<Vote>> getFailedRulesList() {
        return failedRulesList;
    }

    /**
     * Sets the value of the field failedRulesList. The given list is not set if
     * it is null, because objects of this class can be formatted as json, and
     * for null values the attribute is not formatted at all.
     *
     * @param failedRulesList
     *            The failedRulesList to set.
     */
    public void setFailedRulesList(List<AbstractRule<Vote>> failedRulesList) {
        if (failedRulesList != null) {
            this.failedRulesList = failedRulesList;
        }
    }
}
