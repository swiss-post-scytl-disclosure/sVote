/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImpl;
import com.scytl.products.ov.ei.services.domain.model.vote.SuccessfulVote;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

@Stateless
public class SuccessfulVotesRepositoryImpl extends BaseRepositoryImpl<SuccessfulVote, Integer>
        implements SuccessfulVotesRepository {

    private static final String PARAMETER_TENANT_ID = "tenantId";

    private static final String PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    private static final String PARAMETER_BALLOT_BOX_ID = "ballotBoxId";

    @Override
    public List<SuccessfulVote> getSuccessfulVotes(String tenantId, String electionEventId, String ballotBoxId,
            int firstElement, int maxResult) {
        TypedQuery<SuccessfulVote> getSuccessfulVotesQuery = entityManager.createQuery(
            "SELECT s FROM SuccessfulVote s WHERE s.tenantId = :tenantId AND s.electionEventId=:electionEventId AND s.ballotBoxId=:ballotBoxId ORDER BY s.id DESC",
            SuccessfulVote.class);
        getSuccessfulVotesQuery.setFirstResult(firstElement);
        getSuccessfulVotesQuery.setMaxResults(maxResult);
        getSuccessfulVotesQuery.setParameter(PARAMETER_TENANT_ID, tenantId);
        getSuccessfulVotesQuery.setParameter(PARAMETER_ELECTION_EVENT_ID, electionEventId);
        getSuccessfulVotesQuery.setParameter(PARAMETER_BALLOT_BOX_ID, ballotBoxId);
        return getSuccessfulVotesQuery.getResultList();
    }
}
