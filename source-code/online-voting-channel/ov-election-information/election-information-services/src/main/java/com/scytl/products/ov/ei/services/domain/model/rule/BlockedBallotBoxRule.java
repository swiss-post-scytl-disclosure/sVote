/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.rule;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.domain.model.rule.AbstractRule;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.ei.services.domain.service.ballotbox.BallotBoxService;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jbarrio
 * @date 23/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public class BlockedBallotBoxRule implements AbstractRule<Vote> {

	private static final Logger LOG = LoggerFactory.getLogger("std");
	
	@Inject
	private BallotBoxService ballotBoxService;

	/**
	 * This method validates whether the ballot box is still active before storing the vote
	 *
	 * @param vote
	 *            - the vote to be validated.
	 * @return A ValidationError containing information about the execution of the rule. If fails, the dates of election
	 *         are added as additional information.
	 */
	@Override
	public ValidationError execute(Vote vote) {
		try {

			return ballotBoxService.validateBallotBoxNotBlocked(vote.getTenantId(), vote.getElectionEventId(), vote.getBallotBoxId());
		}catch (ResourceNotFoundException e) {
			LOG.error("Error trying to validate ballot box not blocked.", e);
			ValidationError error = new ValidationError();
			error.setValidationErrorType(ValidationErrorType.RESOURCE_NOT_FOUND);
			return error;
		}

	}

	/**
	 * @see com.scytl.products.ov.commons.domain.model.rule.AbstractRule#getName()
	 */
	@Override
	public String getName() {
		return RuleNames.VOTE_BLOCKED_BALLOT_BOX.getText();
	}

}
