/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.content;

import com.scytl.products.ov.commons.util.JsonUtils;

import javax.json.JsonObject;

public class ElectionInformationContentDynamic {
    private final JsonObject value;
    private static final String ELECTION_INFORMATION_PARAMS = "electionInformationParams";

    public ElectionInformationContentDynamic(String value) {
        this.value = JsonUtils.getJsonObject(value);
    }

    public int numVotesPerVotingCard() {
        final String attr = "numVotesPerVotingCard";
        JsonObject intermediate = value.getJsonObject(ELECTION_INFORMATION_PARAMS);
        return Integer.parseInt(intermediate.getString(attr));
    }

    public int numVotesPerAuthToken() {
        final String attr = "numVotesPerAuthToken";
        JsonObject intermediate = value.getJsonObject(ELECTION_INFORMATION_PARAMS);
        return Integer.parseInt(intermediate.getString(attr));
    }
}
