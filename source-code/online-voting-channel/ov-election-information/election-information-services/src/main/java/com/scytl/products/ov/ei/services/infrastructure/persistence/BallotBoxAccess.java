/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ValidationException;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBox;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBoxRepository;
import com.scytl.products.ov.ei.services.domain.service.vote.VoteValidationService;

@Stateless
@javax.ejb.ApplicationException(rollback = true)
public class BallotBoxAccess {

    @EJB
    private BallotBoxRepository ballotBoxRepository;

    @Inject
    private VoteValidationService voteValidationService;

    public BallotBox save(BallotBox ballotBox) throws DuplicateEntryException, ValidationException {
        checkIfVoteIsValid(ballotBox);
        return insert(ballotBox);
    }

    private void checkIfVoteIsValid(BallotBox ballotBox) throws ValidationException {
        voteValidationService.isValid(ballotBox);
    }

    BallotBox insert(BallotBox ballotBox) throws DuplicateEntryException {
        return ballotBoxRepository.save(ballotBox);
    }

}
