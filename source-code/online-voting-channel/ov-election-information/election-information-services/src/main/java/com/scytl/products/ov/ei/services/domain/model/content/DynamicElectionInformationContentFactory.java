/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.content;

public class DynamicElectionInformationContentFactory {
    public ElectionInformationContentDynamic aNew(String json) {
        return new ElectionInformationContentDynamic(json);
    }
}
