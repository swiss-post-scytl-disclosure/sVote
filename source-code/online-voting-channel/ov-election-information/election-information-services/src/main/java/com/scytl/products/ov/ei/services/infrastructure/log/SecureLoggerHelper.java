/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.log;

import java.io.IOException;

import javax.inject.Inject;
import javax.json.JsonException;
import javax.json.JsonObject;

import org.slf4j.Logger;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.logging.service.AuthTokenHashService;
import com.scytl.products.ov.commons.logging.service.VoteHashService;
import com.scytl.products.ov.commons.ui.ws.rs.ObjectMappers;
import com.scytl.products.ov.commons.util.JsonUtils;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation;

/**
 * Helper methods for secure logging purposes.
 */
public class SecureLoggerHelper {

    @Inject
    private AuthTokenHashService authTokenHashService;

    @Inject
    private VoteHashService voteHashService;

    @Inject
    private ConfirmationMessageHashService confirmationMessageHashService;

    @Inject
    private Logger LOG;

    public JsonObject getAuthToken(String authenticationToken) {
        JsonObject authTokenJsonObject = null;
        if (authenticationToken != null) {
            try {
                authTokenJsonObject = JsonUtils.getJsonObject(authenticationToken);
                return authTokenJsonObject;
            } catch (JsonException | IllegalStateException e) {
                // error creating auth token hash
                LOG.error("Error getting auth token for secure logging", e);
            }
        }
        return authTokenJsonObject;
    }

    /**
     * Computes the hash of the auth token object.
     * 
     * @param authenticationToken
     *            the auth token.
     * @return the result of hashing the auth token.
     */
    public String getAuthTokenHash(JsonObject authenticationToken) {
        String authTokenHash = "";
        try {
            if (authenticationToken != null) {
                authTokenHash = authTokenHashService.hash(authenticationToken);
            }
        } catch (GeneralCryptoLibException e) {
            // error creating auth token hash
            LOG.error("Error generating auth token hash for secure logging", e);
        }
        return authTokenHash;
    }

    /**
     * Computes the hash of the auth token object.
     *
     * @param authenticationToken
     *            the auth token.
     * @return the result of hashing the auth token.
     */
    public String getAuthTokenHash(String authenticationToken) {
        String authTokenHash = "";
        try {
            if (authenticationToken != null) {
                authTokenHash = authTokenHashService.hash(getAuthToken(authenticationToken));
            }
        } catch (GeneralCryptoLibException e) {
            // error creating auth token hash
            LOG.error("Error generating auth token hash for secure logging", e);
        }
        return authTokenHash;
    }

    /**
     * Computes the hash of the auth token object.
     *
     * @param authenticationToken
     *            the auth token.
     * @return the result of hashing the auth token.
     */
    public String getAuthTokenHash(AuthenticationToken authenticationToken) {
        String authTokenHash = "";
        try {
            // we really have to avoid constantly changing formats
            if (authenticationToken != null) {
                String authenticationTokenString = ObjectMappers.toJson(authenticationToken);
                authTokenHash = authTokenHashService.hash(JsonUtils.getJsonObject(authenticationTokenString));
            }
        } catch (IOException | GeneralCryptoLibException e) {
            // error creating auth token hash
            LOG.error("Error generating auth token hash for secure logging", e);
        }
        return authTokenHash;
    }

    /**
     * Computes the hash of the vote object.
     * 
     * @param vote
     *            the vote.
     * @return the result of hashing the vote.
     */
    public String getVoteHash(Vote vote) {
        String voteHash = "";
        try {
            voteHash = voteHashService.hash(vote);
        } catch (JsonException | IllegalStateException | GeneralCryptoLibException e) {
            // error creating vote hash
            LOG.error("Error generating vote hash for secure logging", e);
        }
        return voteHash;
    }

    /**
     * Returns the confirmation message hash.
     * 
     * @param confirmationInformation
     *            confirmation information.
     * @return confirmation message hash.
     */
    public String getConfirmationMessageHash(ConfirmationInformation confirmationInformation) {
        return confirmationMessageHashService.hash(confirmationInformation);
    }
}
