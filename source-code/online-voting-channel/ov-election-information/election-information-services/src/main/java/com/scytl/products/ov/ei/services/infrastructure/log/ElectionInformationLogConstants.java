/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.log;

/**
 * Constants for logging in election information.
 */
public class ElectionInformationLogConstants {
	
	/**
	 * Ballot id - additional information.
	 */
	public static final String INFO_BALLOT_ID = "#b_id";

	/**
	 * Ballot box id - additional information.
	 */
	public static final String INFO_BALLOT_BOX_ID = "#bb_id";
	
	/**
	 * Ballot box is of type test - additional information.
	 */
	public static final String INFO_BALLOT_BOX_TEST = "#bb_test";

	/**
	 * Error description - additional information.
	 */
	public static final String INFO_ERR_DESC = "#err_desc";

	/**
	 * Track id - additional information.
	 */
	public static final String INFO_TRACK_ID = "#request_id";

	/**
	 * Hash of the vote
	 */
	public static final String INFO_HASH_VOTE = "#hashVote";

	/**
	 * TimeStamp if the authentication token
	 */
	public static final String INFO_TIMESTAMP_AT = "#timestamp_at";

	/**
	 * Election dates
	 */
	public static final String INFO_ELECTION_DATE = "#election_dates";

	/**
	 * Credential id
	 */
	public static final String INFO_CREDENTIAL_ID = "#c_id";

	/**
	 * Receipt value
	 */
	public static final String INFO_RECEIPT = "#voteReceipt";

	/**
	 * Confirmation message hash
	 */
	public static final String INFO_HASH_CONFIRMATION_MESSAGE = "#hashCM";

	/**
	 * Verification card id - additional information.
	 */
	public static final String INFO_VERIFICATION_CARD_ID = "#verifc_id";

    /**
     * Electoral authority id - additional information.
     */
    public static final String INFO_ELECTORAL_AUTHORITY_ID = "#ea_id";

	public static final String FIRST_ELEMENT = "#first";

	public static final String LAST_ELEMENT = "#last";

	/**
	 * Non-public constructor
	 */	
	private ElectionInformationLogConstants() {		
	}

}
