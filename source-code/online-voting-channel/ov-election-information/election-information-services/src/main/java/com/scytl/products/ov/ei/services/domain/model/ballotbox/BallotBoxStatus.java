/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballotbox;

public enum BallotBoxStatus {

    BLOCKED("BLOCKED"),NONE("NONE");

    private String value;

    BallotBoxStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }

    public static BallotBoxStatus getEnum(String value) {
        for(BallotBoxStatus v : values())
            if(v.getValue().equalsIgnoreCase(value)) return v;
        return NONE;
    }


}
