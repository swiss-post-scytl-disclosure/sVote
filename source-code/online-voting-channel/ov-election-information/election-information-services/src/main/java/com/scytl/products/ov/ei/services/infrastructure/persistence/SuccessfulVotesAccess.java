/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.ei.services.domain.model.vote.SuccessfulVote;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@javax.ejb.ApplicationException(rollback = true)
public class SuccessfulVotesAccess {

    @EJB
    private SuccessfulVotesRepository successfulVotesRepository;

    public SuccessfulVote save(SuccessfulVote successfulVote) throws DuplicateEntryException {
        return successfulVotesRepository.save(successfulVote);
    }
}