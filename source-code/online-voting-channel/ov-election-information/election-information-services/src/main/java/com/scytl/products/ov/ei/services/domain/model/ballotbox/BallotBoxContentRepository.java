/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballotbox;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for handling ballot box content entities.
 */
@Local
public interface BallotBoxContentRepository extends BaseRepository<BallotBoxContent, Integer> {

    /**
     * Searches for an ballot box content with the given tenant, election event,
     * ballot box id.
     *
     * @param tenantId
     *            - the identifier of the tenant.
     * @param electionEventId
     *            - the identifier of the electionEvent.
     * @param ballotBoxId
     *            - the identifier of the ballot box.
     * @return a entity representing the authentication content.
     */
    BallotBoxContent findByTenantIdElectionEventIdBallotBoxId(String tenantId, String electionEventId,
            String ballotBoxId) throws ResourceNotFoundException;

    String findFirstBallotBoxForElection(String tenantId, String electionEventId) throws ResourceNotFoundException;

}
