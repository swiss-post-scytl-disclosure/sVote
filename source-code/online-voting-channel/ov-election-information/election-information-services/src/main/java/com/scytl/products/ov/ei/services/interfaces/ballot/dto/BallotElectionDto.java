/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.interfaces.ballot.dto;

import javax.validation.constraints.NotNull;

/**
 * This class represents a dto class with the identifier of an election and a contest (if there is any).
 */
public class BallotElectionDto {

	/* The election id. */
	@NotNull
	private String electionId;

	/* The contest id. */
	private String contestId;

	/**
	 * Returns the current value of the field electionId.
	 *
	 * @return Returns the electionId.
	 */
	public String getElectionId() {
		return electionId;
	}

	/**
	 * Sets the value of the field electionId.
	 *
	 * @param electionId The electionId to set.
	 */
	public void setElectionId(String electionId) {
		this.electionId = electionId;
	}

	/**
	 * Returns the current value of the field contestId.
	 *
	 * @return Returns the contestId.
	 */
	public String getContestId() {
		return contestId;
	}

	/**
	 * Sets the value of the field contestId.
	 *
	 * @param contestId The contestId to set.
	 */
	public void setContestId(String contestId) {
		this.contestId = contestId;
	}
}
