/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.confirmation;

import java.util.ArrayList;
import java.util.Collection;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.logging.I18nLoggerMessages;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformationResult;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationMessageValidation;

/**
 * Service which validates the confirmation message sent by the client.
 */
public class ConfirmationMessageValidationServiceImpl implements ConfirmationMessageValidationService {
    
    private final Collection<ConfirmationMessageValidation> validations = new ArrayList<>();

    private static final Logger LOG = LoggerFactory.getLogger(ConfirmationMessageSignatureValidation.class);

	private static final I18nLoggerMessages I18N = I18nLoggerMessages.getInstance();

	@Inject
	@Any
	void setValidations(Instance<ConfirmationMessageValidation> instance) {
	    for (ConfirmationMessageValidation validation : instance) {
	        validations.add(validation);
	    }
	}

	@Override
	public ConfirmationInformationResult validateConfirmationMessage(String tenantId, String electionEventId,
			String votingCardId, ConfirmationInformation confirmationInformation, AuthenticationToken authenticationToken) {

		LOG.info(I18N.getMessage("ConfirmationMessageValidationService.validate.start"));

		// result of validation
		ConfirmationInformationResult confirmationInformationResult = new ConfirmationInformationResult();
		confirmationInformationResult.setValid(true);
		confirmationInformationResult.setElectionEventId(electionEventId);
		confirmationInformationResult.setVotingCardId(votingCardId);

		// execute validations
		for (ConfirmationMessageValidation validation : validations) {
			ValidationError validationErrorResult = validation.execute(tenantId, electionEventId, votingCardId,
				confirmationInformation, authenticationToken);
			if (validationErrorResult.getValidationErrorType().equals(ValidationErrorType.SUCCESS)) {
				LOG.info(I18N.getMessage("ConfirmationMessageValidationService.validate.validationSuccess"),
					validation.getClass().getSimpleName());
			} else {
				LOG.info(I18N.getMessage("ConfirmationMessageValidationService.validate.validationFail"),
					validation.getClass().getSimpleName());
				confirmationInformationResult.setValid(false);
				confirmationInformationResult.setValidationError(validationErrorResult);
				break;
			}
		}

		LOG.info(I18N.getMessage("ConfirmationMessageValidationService.validate.resultOfValidation"),
			confirmationInformationResult.isValid());

		return confirmationInformationResult;
	}
}
