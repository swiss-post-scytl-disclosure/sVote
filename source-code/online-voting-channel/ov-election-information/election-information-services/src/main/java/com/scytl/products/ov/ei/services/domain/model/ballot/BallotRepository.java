/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballot;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Provides extra operations on the ballot repository.
 */
@Local
public interface BallotRepository extends BaseRepository<Ballot, Integer> {

	/**
	 * Searches for a ballot with the given id and tenant.
	 *
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param ballotId - the external identifier of the ballot.
	 * @return a entity representing the ballot.
	 * @throws ResourceNotFoundException if ballot is not found.
	 */
	Ballot findByTenantIdElectionEventIdBallotId(String tenantId, String electionEventId, String ballotId)
			throws ResourceNotFoundException;
}
