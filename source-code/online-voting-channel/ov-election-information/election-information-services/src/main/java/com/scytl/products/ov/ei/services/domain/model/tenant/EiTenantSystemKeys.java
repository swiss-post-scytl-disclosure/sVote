/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.tenant;

import javax.ejb.Singleton;

import com.scytl.products.ov.commons.beans.domain.model.tenant.TenantSystemKeys;

@Singleton
public class EiTenantSystemKeys extends TenantSystemKeys {

}
