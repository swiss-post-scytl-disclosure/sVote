/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.crypto;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.products.ov.commons.crypto.InputDataFormatterService;
import com.scytl.products.ov.commons.crypto.PrivateKeyForObjectRepository;
import com.scytl.products.ov.commons.crypto.SignatureForObjectService;

/**
 * Producer for ballot box signature service.
 */
public class BallotBoxSignatureServiceProducer {

	@Inject
	private PrivateKeyForObjectRepository privateKeyRepository;

	@Inject
	private AsymmetricServiceAPI asymmetricService;

	@Inject
	private InputDataFormatterService inputDataFormatterService;

	/**
	 * Returns an instance of the ballot box signature service.
	 * 
	 * @return an instance of the ballot box signature service.
	 */
	@Produces
	public SignatureForObjectService getInstance() {
		return new SignatureForObjectService(asymmetricService, privateKeyRepository, inputDataFormatterService);
	}
}
