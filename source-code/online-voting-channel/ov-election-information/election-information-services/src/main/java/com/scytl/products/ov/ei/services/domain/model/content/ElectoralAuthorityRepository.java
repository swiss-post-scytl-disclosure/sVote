/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.content;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for handling Electoral Authorities entities
 */
@Local
public interface ElectoralAuthorityRepository extends BaseRepository<ElectoralAuthorityEntity,Integer> {

    /**
     * Searches electoral authority associated to this tenant, election event and electoral authority ids.
     *
     * @param tenantId - the identifier of the tenant.
     * @param electionEventId - the identifier of the electionEvent.
     * @param electoralAuthorityId - the identifier of the associated electoralAuthority.
     * @return a entity representing the electoral authority.
     */
    ElectoralAuthorityEntity findByTenantIdElectionEventIdElectoralAuthorityId(String tenantId,String electionEventId, String electoralAuthorityId) throws ResourceNotFoundException;

}
