/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.vote;

import java.io.IOException;

import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.dto.CastCodeAndComputeResults;

public interface VoteCastCodeService {

    /**
     * Saves the cast code that has been derived from the vote confirmation code
     * 
     * @param tenantId
     * @param electionEventId
     * @param votingCardId
     * @param voteCastCode
     * @throws ApplicationException
     * @throws IOException
     */
    void save(String tenantId, String electionEventId, String votingCardId, CastCodeAndComputeResults voteCastCode)
            throws ApplicationException, IOException;

}
