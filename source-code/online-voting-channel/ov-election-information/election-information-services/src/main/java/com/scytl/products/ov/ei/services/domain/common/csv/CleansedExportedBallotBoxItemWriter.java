/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.common.csv;

import com.scytl.products.ov.commons.infrastructure.csv.AbstractCSVWriter;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.CleansedExportedBallotBoxItem;

import java.io.OutputStream;

/**
 * Writer in csv for every item to be downloaded of a ballot box
 */
public class CleansedExportedBallotBoxItemWriter
        extends AbstractCSVWriter<CleansedExportedBallotBoxItem> {


    public CleansedExportedBallotBoxItemWriter(final OutputStream outputStream) {
        super(outputStream);
    }

    /**
     * extract the values of a ballot box item into an array of Strings
     * @param cleansedExportedBallotBoxItem - ballot box item from which the info is extracted
     * @return
     */
    @Override
    public String[] extractValues(
            final CleansedExportedBallotBoxItem cleansedExportedBallotBoxItem) {

        return new String[] {cleansedExportedBallotBoxItem.getVote()};

    }

}
