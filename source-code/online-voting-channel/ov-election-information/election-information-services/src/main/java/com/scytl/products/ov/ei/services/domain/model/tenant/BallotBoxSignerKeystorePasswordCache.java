/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.tenant;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Local;
import javax.ejb.Singleton;

import com.scytl.products.ov.commons.cache.Cache;
import com.scytl.products.ov.commons.cache.KeyStorePasswordCache;

@Singleton
@Local(value = Cache.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class BallotBoxSignerKeystorePasswordCache
        extends KeyStorePasswordCache {

	public BallotBoxSignerKeystorePasswordCache() {
		super("BallotBoxSignerKeystorePassword");
	}

}
