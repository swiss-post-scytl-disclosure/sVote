/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Enum with the log events for this context.
 */
public enum ElectionInformationLogEvents implements LogEvent {

	BALLOT_FOUND("BALLRET", "000", "Ballot found"),
	BALLOT_NOT_FOUND("BALLRET", "600", "Ballot not found"),

	BALLOT_BOX_FOUND("BALLBOXRET", "000", "Ballot Box Voter Data found"),
	BALLOT_BOX_NOT_FOUND("BALLBOXRET", "601", "Ballot Box Voter Data not found"),

	VOTE_FOUND("CCRET", "000", "Vote found"),
	VOTE_HASH_FOUND("CCRET", "000", "Vote hash found"),
	VOTE_NOT_FOUND("CCRET", "602", "Vote not found"),

	VOTE_CAST_FOUND("VCCRETDB", "000", "Vote Cast Code found"),
	VOTE_CAST_NOT_FOUND("VCCRETDB", "604", "Vote Cast Code not found"),

	VOTE_VALID("VOTVA", "000", "Successful Vote Validation"),
	INVALID_ENCRYPTED_VOTE_SIGNATURE("VOTVA", "605", "Encrypted Vote signature not valid"),
	INVALID_CREDENTIAL_ID("VOTVA", "606", "CredentialID who signs the vote is not the same from the request"),
	INVALID_VOTE_ID("VOTVA", "607", "ID in the Vote not consistent with that provided"),
	INVALID_SCHNORR_PROOF("VOTVA", "609", "Schnorr proof not valid"),
	INVALID_CREDENTIAL_ID_SIGNING_CERT("VOTVA", "610", "CredentialID signing certificate not valid"),
	VOTE_OUT_OF_DATE("VOTVA", "611", "Vote has been received after election closed"),
	INVALID_VOTE_STRUCTURE("VOTVA", "612", "Validations over the structure of the vote failed"),
	INVALID_ELECTION_EVENT_ID("VOTVA", "613", "ElectionEventID in the Vote not consistent with that provided"),
	VOTE_ALREADY_SENT("VOTVA", "614", "The VotingCardID has already sent a vote"),
	INVALID_VOTE_CORRECTNESS("VOTVA","630", "Validations of the the vote correctness failed"),
	BALLOT_BOX_NOT_EMPTY("BBUP", "615", "Ballot box is not empty"),

	RECEIPT_CORRECTLY_GENERATED("RECGEN","000","Receipt correctly generated"),
	RECEIPT_CORRECTLY_SIGNED("RECGEN","000","Receipt correctly signed"),
	ERROR_COMPUTING_RECEIPT("RECGEN","616","Error while computing the Receipt"),
	ERROR_SIGNING_RECEIPT("RECGEN","617"," Error while signing the Receipt"),

	VOTE_CORRECTLY_STORED("VRSTOR","000","Vote and receipt correctly stored"),
	ERROR_SAVING_VOTE("VRSTOR","618","Error during the storage of the vote"),
	VOTE_INVALID("VRSTOR", "627", "Vote is invalid because vote validation fails. Can not be stored."),

	CONFIRMATION_MESSAGE_VALID("CMVAL", "000", "Successful Confirmation Message Validation"),
	ENCRYPTED_VOTES_FOUND("CMVAL", "000", "Encryption votes found"),

	CONFIRMATION_MESSAGE_INVALID_CREDENTIAL_ID("CMVAL", "619", "The Credential ID in the certifiate not consistent with that provided in the request"),
	CONFIRMATION_MESSAGE_INVALID_VOTING_CARD_ID("CMVAL", "620", "VotingCardId in Confirmation Message signature not consistent with that provided in the request"),
	CONFIRMATION_MESSAGE_INVALID_SIGNATURE("CMVAL", "621", "Confirmation Message signature not valid"),
	CONFIRMATION_MESSAGE_INVALID_CONFIRMATION_MESSAGE("CMVAL", "622", "Invalid Confirmation Message"),
	CONFIRMATION_MESSAGE_ALREADY_CONFIRMED("CMVAL", "623", "The vote has already been confirmed"),
	CONFIRMATION_MESSAGE_NOT_PENDING_TO_CONFIRM("CMVAL", "624", "There is no vote pending to confirm"),
	CONFIRMATION_MESSAGE_CAN_NOT_BE_CONFIRMED("CMVAL", "625", "The vote can not be confirmed"),
	CONFIRMATION_MESSAGE_INVALID_CREDENTIAL_ID_CERTIFICATE_CHAIN("CMVAL", "629", "Credential ID certifiate chain not valid"),
    CONFIRMATION_MESSAGE_INVALID_MATHEMATICAL_GROUP("CMVAL", "647", "Confirmation Message mathematical group not valid"),
    CONFIRMATION_MESSAGE_INVALID_ELECTION_DATES("CMVAL", "648", "Confirmation Message election dates not valid"),
	CONFIRMATION_MESSAGE_CONFIRMATION_NOT_REQUIRED("CMVAL", "658","Confirmation Message vote confirmation not required"),

	VOTE_CAST_CODE_SAVED("VCCSTOR","000","Vote Cast Code stored"),
	ERROR_SAVING_VOTE_CAST_CODE("VCCSTOR","626","Error while storing the Vote Cast Code"),

	BALLOT_BOX_EXPORT_REQUESTED("BBEXP","000","Ballot box export attempted"),
	BALLOT_BOX_EXPORT_READY("BBEXP","000","Ballot box export ready"),
	BALLOT_BOX_EXPORT_FAILED("BBEXP","637","Ballot box export failed"),
	ELECTION_EVENT_ALREADY_CLOSED("ATVAL", "630", "ElectionEvent is already closed"),
	ELECTION_EVENT_NOT_STARTED("ATVAL", "631", "ElectionEvent is not yet started"),
	BALLOT_TEXT_FOUND("BALLRET", "632", "Ballot text found"),
    BALLOT_TEXT_NOT_FOUND("BALLRET", "633", "Ballot text not found"),

    INVALID_ELECTIONEVENT_ROOTCA_CERT("VOTVA", "634", "Election root CA is not a valid certificate authority"),

    ELECTORAL_AUTHORITY_FOUND("EARET", "635", "Electoral Authority found"),
    ELECTORAL_AUTHORITY_NOT_FOUND("EARET", "636", "Electoral Authority  not found"),

	BALLOT_SAVED("BALLSTOR", "637", "Ballot saved successfully"),
    ERROR_SAVING_BALLOT("BALLSTOR", "638", "Failed to save ballot"),

    BALLOT_TEXT_SAVED("BALLSTOR", "639", "Ballot text saved successfully"),
    ERROR_SAVING_BALLOT_TEXT("BALLSTOR", "640", "Failed to save ballot text"),

    BALLOT_BOX_INFORMATION_SAVED("BBINFSTOR", "641", "Ballot box information saved successfully"),
    ERROR_SAVING_BALLOT_BOX_INFORMATION("BBINFSTOR", "642", "Failed to save ballot box information"),

    BALLOT_BOX_CONTENT_SAVED("BBCNTSTOR", "643", "Ballot box content saved successfully"),
    ERROR_SAVING_BALLOT_BOX_CONTENT("BBCNTSTOR", "644", "Failed to save ballot box content"),
    ELECTORAL_AUTHORITY_SAVED("EASTOR", "645", "Electoral Authority saved successfully"),
    ERROR_SAVING_ELECTORAL_AUTHORITY("EASTOR", "646", "Error saving Electoral Authority"),

    BALLOT_BOX_INFORMATION_UPDATED("BBINFSTOR", "647", "Ballot box information updated successfully"),
    ERROR_UPDATING_BALLOT_BOX_INFORMATION("BBINFSTOR", "648", "Failed to update ballot box information"),

    BALLOT_BOX_STATUS_REQUESTED("BBINFSTOR", "649", "Ballot box status requested"),

    BALLOT_BOX_BLOCKED("BBINFSTOR", "650", "Ballot box is blocked"),
    CONFIRMATION_MESSAGE_BLOCKED_BALLOT_BOX("CMVAL", "651", "Ballot box is blocked"),
    BLOCK_BALLOT_BOX_OK("BBBLOCK", "652", "Ballot box has been blocked"),
    UNBLOCK_BALLOT_BOX_OK("BBBLOCK", "653", "Ballot box has been unblocked"),
	BALLOT_BOX_TO_BLOCK_NOT_FOUND("BBBLOCK", "654", "Ballot Box to block not found"),
    ERROR_BLOCKING_BALLOT_BOX("BBBLOCK", "655", "Error blocking ballot box"),
    BALLOT_BOX_TO_UNBLOCK_NOT_FOUND("BBBLOCK", "656", "Ballot Box to block not found"),
    ERROR_UNBLOCKING_BALLOT_BOX("BBBLOCK", "657", "Error unblocking ballot box"),
    
    INVALID_NUMBER_ENCRYPTED_WRITEINS("VOTVA", "659", "The encrypted writeins did not have the expected number of elements")
    ;


	private final String layer;

	private final String action;

	private final String outcome;

	private final String info;

	ElectionInformationLogEvents(final String action, final String outcome, final String info) {
		this.layer = "";
		this.action = action;
		this.outcome = outcome;
		this.info = info;
	}

	/**
	 * @see ElectionInformationLogEvents#getAction()
	 */
	@Override
	public String getAction() {
		return action;
	}

	/**
	 * @see ElectionInformationLogEvents#getOutcome()
	 */
	@Override
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @see ElectionInformationLogEvents#getInfo()
	 */
	@Override
	public String getInfo() {
		return info;
	}

	/**
	 * @see ElectionInformationLogEvents#getLayer()
	 */
	@Override
	public String getLayer() {
		return layer;
	}
}
