/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballotbox;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scytl.products.ov.commons.domain.model.Constants;

/**
 * The Class BallotBoxInformation.
 */
@Entity
@Table(name = "BALLOT_BOX_INFORMATION",uniqueConstraints = @UniqueConstraint(name="BALLOT_BOX_INFORMATION_UK1",
        columnNames = {"BALLOT_BOX_ID", "ELECTION_EVENT_ID", "TENANT_ID"}))

public class BallotBoxInformation {

    /**
     * The identifier for this ballot box information.
     */
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ballotBoxInformationSeq")
    @SequenceGenerator(name = "ballotBoxInformationSeq", sequenceName = "BALLOT_BOX_INFORMATION_SEQ")
    private Integer id;

    /** The object containing the ballot box information. */
    @Lob
    @Column(name = "JSON", length = Integer.MAX_VALUE)
    @NotNull
    private String json;

    /** A provided External Id which will identify the Ballot combined. */
    @Column(name = "BALLOT_BOX_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    @JsonProperty("id")
    private String ballotBoxId;

    /**
     * The identifier of a tenant for the current election event.
     */
    @Column(name = "TENANT_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    @JsonIgnore
    private String tenantId;

    /**
     * The identifier of an election event for the current ballot box.
     */
    @Column(name = "ELECTION_EVENT_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    @JsonIgnore
    private String electionEventId;

    /** The ballot box information signature. */
    @Lob
    @Column(name = "SIGNATURE", length = Integer.MAX_VALUE)
    @NotNull
    private String signature;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private BallotBoxStatus status;

    /**
     * Returns the value of the field id.
     *
     * @return the id
     */
    @JsonIgnore
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Returns the value of the field json.
     *
     * @return the json
     */
    public String getJson() {
        return json;
    }

    /**
     * Sets the value of the field JSON.
     *
     * @param json
     *            the new json
     */
    public void setJson(String json) {
        this.json = json;
    }

    /**
     * Returns the value of the field ballot box id.
     *
     * @return the ballot box id
     */
    public String getBallotBoxId() {
        return ballotBoxId;
    }

    /**
     * Sets the value of the field ballotBoxId.
     *
     * @param ballotBoxId
     *            the new ballot box id
     */
    public void setBallotBoxId(String ballotBoxId) {
        this.ballotBoxId = ballotBoxId;
    }

    /**
     * Returns the value of the field tenantId.
     *
     * @return the tenant id
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * Sets the value of the field tenantId.
     *
     * @param tenantId
     *            the new tenant id
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * Returns the current value of the field electionEventId.
     *
     * @return Returns the electionEventId.
     */
    public String getElectionEventId() {
        return electionEventId;
    }

    /**
     * Sets the value of the field electionEventId.
     *
     * @param electionEventId
     *            The electionEventId to set.
     */
    public void setElectionEventId(String electionEventId) {
        this.electionEventId = electionEventId;
    }

    /**
     * Returns the current value of the field signature.
     *
     * @return Returns the signature.
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets the value of the field signature.
     *
     * @param signature
     *            The signature to set.
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * Sets new status.
     *
     * @param status
     *            New value of status.
     */
    public void setStatus(BallotBoxStatus status) {
        this.status = status;
    }

    /**
     * Gets status.
     *
     * @return Value of status.
     */
    public BallotBoxStatus getStatus() {
        return status;
    }
}
