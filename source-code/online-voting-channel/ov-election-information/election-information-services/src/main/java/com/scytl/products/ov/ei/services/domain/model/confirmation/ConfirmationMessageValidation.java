/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.confirmation;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.validation.ValidationError;

/**
 * Interface to provide functionalities for confirmation message validations.
 */
public interface ConfirmationMessageValidation {

	/**
	 * Executes a specific validation.
	 * 
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @param votingCardId - the voting card identifier.
	 * @param confirmationInformation - the confirmation information to be validated.
	 * @param authenticationToken - the authentication token.
	 * @return A ValidationError describing if the rule is satisfied or not.
	 */
			ValidationError execute(String tenantId, String electionEventId, String votingCardId,
					ConfirmationInformation confirmationInformation, AuthenticationToken authenticationToken);
}
