/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.rule;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.domain.model.rule.AbstractRule;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogConstants;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogEvents;
import com.scytl.products.ov.ei.services.infrastructure.log.SecureLoggerHelper;

/**
 * Decorator of the max number of allowed votes rule.
 */
@Decorator
public class MaxNumberOfAllowedVotesDecorator implements AbstractRule<Vote> {

    @Inject
    @Delegate
    private MaxNumberOfAllowedVotes maxNumberOfAllowedVotes;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;

    @Inject
    private TrackIdInstance trackId;

    @Inject
    private SecureLoggerHelper secureLoggerHelper;

    /**
     * @see com.scytl.products.ov.commons.domain.model.rule.AbstractRule#execute(java.lang.Object)
     */
    @Override
    public ValidationError execute(Vote vote) {
        ValidationError result = maxNumberOfAllowedVotes.execute(vote);
        if (!result.getValidationErrorType().equals(ValidationErrorType.SUCCESS)) {
            String authTokenHash = secureLoggerHelper.getAuthTokenHash(vote.getAuthenticationToken());
            String voteHash = secureLoggerHelper.getVoteHash(vote);
            secureLoggerWriter.log(
                Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(ElectionInformationLogEvents.VOTE_ALREADY_SENT)
                    .objectId(authTokenHash).user(vote.getVotingCardId()).electionEvent(vote.getElectionEventId())
                    .additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(ElectionInformationLogConstants.INFO_HASH_VOTE, voteHash)
                    .additionalInfo(ElectionInformationLogConstants.INFO_ERR_DESC, "The vote was already sent")
                    .createLogInfo());
        }
        return result;
    }

    /**
     * @see com.scytl.products.ov.commons.domain.model.rule.AbstractRule#getName()
     */
    @Override
    public String getName() {
        return maxNumberOfAllowedVotes.getName();
    }

}
