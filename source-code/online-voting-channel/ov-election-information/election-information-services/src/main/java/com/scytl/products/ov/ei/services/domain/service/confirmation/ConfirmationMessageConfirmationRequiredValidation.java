/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.confirmation;

import javax.inject.Inject;
import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.util.JsonUtils;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBoxInformation;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBoxInformationRepository;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationMessageValidation;

public class ConfirmationMessageConfirmationRequiredValidation implements ConfirmationMessageValidation {

    private static final String CONFIRMATION_REQUIRED_ATTRIBUTE = "confirmationRequired";

    @Inject
    private BallotBoxInformationRepository ballotBoxInformationRepository;

    private static final Logger LOG = LoggerFactory.getLogger(ConfirmationMessageConfirmationRequiredValidation.class);

    /**
     * Validate if the current ballot box allows vote confirmation
     * 
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @param votingCardId
     *            - the voting card identifier.
     * @param confirmationInformation
     *            - the confirmation information to be validated.
     * @param authenticationToken
     *            - the authentication token.
     * @return
     */

    @Override
    public ValidationError execute(String tenantId, String electionEventId, String votingCardId,
            ConfirmationInformation confirmationInformation, AuthenticationToken authenticationToken) {

        ValidationError result = new ValidationError();
        result.setValidationErrorType(ValidationErrorType.SUCCESS);

        try {

            BallotBoxInformation ballotBoxInformation =
                ballotBoxInformationRepository.findByTenantIdElectionEventIdBallotBoxId(tenantId, electionEventId,
                    authenticationToken.getVoterInformation().getBallotBoxId());
            JsonObject ballotBoxInformationJson = JsonUtils.getJsonObject(ballotBoxInformation.getJson());
            Boolean confirmationRequired = ballotBoxInformationJson.getBoolean(CONFIRMATION_REQUIRED_ATTRIBUTE);

            if (!confirmationRequired) {
                LOG.error("Vote confirmation is not allowed due to election is configured in one phase.");
                result.setValidationErrorType(ValidationErrorType.CONFIRMATION_NOT_REQUIRED);
            }

        } catch (ResourceNotFoundException resourceNotFoundException) {
            LOG.error("Error validating confirmationRequired attribute. Ballot box with id {} not found",
                authenticationToken.getVoterInformation().getBallotBoxId(), resourceNotFoundException);
            result.setValidationErrorType(ValidationErrorType.RESOURCE_NOT_FOUND);
        }

        return result;
    }
}
