/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.crypto;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;
import com.scytl.products.ov.commons.crypto.KeystoreForObjectOpener;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBoxKeystoreRepository;
import com.scytl.products.ov.ei.services.infrastructure.persistence.BallotBoxPasswordRepository;

/**
 * The producer for ballot box keystore opener.
 */
public class BallotBoxKeystoreOpenerProducer {

	@Inject
	private ScytlKeyStoreServiceAPI storesService;

	@Inject
	private BallotBoxKeystoreRepository keystoreRepository;

	@Inject
	private BallotBoxPasswordRepository passwordRepository;

	/**
	 * Returns an instance of this service.
	 * 
	 * @return returns a KeystoreForObjectOpener.
	 */
	@Produces
	@BallotBoxKeystoreOpener
	public KeystoreForObjectOpener getInstance() {
		return new KeystoreForObjectOpener(storesService, keystoreRepository, passwordRepository);
	}
}
