/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.confirmation;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationMessageValidation;
import com.scytl.products.ov.ei.services.domain.service.ballotbox.BallotBoxService;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jbarrio
 * @date 23/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public class ConfirmationMessageBlockedBallotBoxValidation implements ConfirmationMessageValidation {

	private static final Logger LOG = LoggerFactory.getLogger(ConfirmationMessageBlockedBallotBoxValidation.class);
	 
	@Inject
	private BallotBoxService ballotBoxService;

	/**
	 * This method validates whether the ballot box is still active before cast the vote.
	 *
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @param votingCardId - the voting card identifier.
	 * @param confirmationInformation - the confirmation information to be validated.
	 * @param authenticationToken - the authentication token.
	 * @return ValidationError with information about validation execution.
	 */
	@Override
	public ValidationError execute(String tenantId, String electionEventId, String votingCardId,
	                               ConfirmationInformation confirmationInformation, AuthenticationToken authenticationToken) {
		String ballotBoxId = authenticationToken.getVoterInformation().getBallotBoxId();
		try {

			return ballotBoxService.validateBallotBoxNotBlocked(tenantId, electionEventId, ballotBoxId);
		}catch (ResourceNotFoundException rE) {
			LOG.info("Error trying to validate ballot box not blocked.", rE);
			return new ValidationError(ValidationErrorType.RESOURCE_NOT_FOUND);
		}
	}
}
