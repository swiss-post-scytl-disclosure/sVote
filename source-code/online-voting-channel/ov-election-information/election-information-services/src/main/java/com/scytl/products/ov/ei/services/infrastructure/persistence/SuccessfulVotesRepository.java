/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import com.scytl.products.ov.commons.domain.model.BaseRepository;
import com.scytl.products.ov.ei.services.domain.model.vote.SuccessfulVote;
import java.util.List;
import javax.ejb.Local;

@Local
public interface SuccessfulVotesRepository extends BaseRepository<SuccessfulVote,Integer> {

    /**
     * Get a successful votes entry list
     */
    List<SuccessfulVote> getSuccessfulVotes(String tenantId, String electionEventId, String ballotBoxId,
                                            int firstElement, int maxResult);
}