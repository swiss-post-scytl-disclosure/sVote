/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.vote;

import java.time.ZonedDateTime;

public class FailedVote {
    private String votingCardId;
    private ZonedDateTime timestamp;
    private String validationError;
    private String receipt;

    public String getVotingCardId() {
        return votingCardId;
    }

    public void setVotingCardId(String votingCardId) {
        this.votingCardId = votingCardId;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getValidationError() {
        return validationError;
    }

    public void setValidationError(String validationError) {
        this.validationError = validationError;
    }
}