/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.log;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.domain.model.tenant.TenantKeystoreRepository;
import com.scytl.products.ov.commons.logging.config.SecureLoggerInitializer;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreRepository;
import com.scytl.products.ov.commons.tenant.TenantActivator;
import com.scytl.products.ov.ei.services.domain.model.platform.EiLoggingKeystoreRepository;
import com.scytl.products.ov.ei.services.domain.model.tenant.EiTenantKeystoreRepository;
import com.scytl.products.ov.ei.services.domain.model.tenant.EiTenantSystemKeys;

/**
 * Defines any steps to be performed when the EI context is first initialized and destroyed.
 */
public class EiContextListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private static final String CONTEXT = "EI";

    private static final String ENCRYPTION_PW_PROPERTIES_KEY = "EI_log_encryption";

    private static final String SIGNING_PW_PROPERTIES_KEY = "EI_log_signing";
    
    @EJB
    @EiLoggingKeystoreRepository
    LoggingKeystoreRepository loggingKeystoreRepository;

    @EJB
    EiLoggingInitializationState eiLoggingInitializationState;

    @EJB
    @EiTenantKeystoreRepository
    TenantKeystoreRepository tenantKeystoreRepository;

    @EJB
    private EiTenantSystemKeys eiTenantSystemKeys;

    /**
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {

        LOG.info(CONTEXT + " - context initialized, will attempt to initialize logging if data exists in DB");

        SecureLoggerInitializer secureLoggerInitializer = new SecureLoggerInitializer(eiLoggingInitializationState,
            loggingKeystoreRepository, CONTEXT, ENCRYPTION_PW_PROPERTIES_KEY, SIGNING_PW_PROPERTIES_KEY);

        if (secureLoggerInitializer.initializeIfDataExistsInDB()) {
            eiLoggingInitializationState.setInitialized(true);
            LOG.info(CONTEXT + " - initialized logging correctly from DB");
        } else {
            LOG.info(CONTEXT + " - logging not initialized - did not find keystores in DB");
        }

        LOG.info(CONTEXT + " - will attempt to activate tenant if data exists in DB");

        TenantActivator tenantActivator = new TenantActivator(tenantKeystoreRepository, eiTenantSystemKeys, CONTEXT);

        List<String> tenantIDsActivatedTenants = tenantActivator.activateTenantsFromDbAndFiles();

        LOG.info(CONTEXT + " - completed tenant activation process");
        for (String tenantID : tenantIDsActivatedTenants) {
            LOG.info(CONTEXT + " - activated tenant: " + tenantID);
        }
    }

    /**
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(final ServletContextEvent sce) {

    }
}
