/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.crypto;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.products.ov.commons.crypto.KeystoreForObjectOpener;
import com.scytl.products.ov.commons.crypto.PrivateKeyForObjectRepository;
import com.scytl.products.ov.ei.services.infrastructure.persistence.BallotBoxPasswordRepository;

/**
 * The producer for ballot box private key repository.
 */
public class BallotBoxPrivateKeyRepositoryProducer {

	@Inject
	@BallotBoxKeystoreOpener
	private KeystoreForObjectOpener keystoreOpener;

	@Inject
	private BallotBoxPasswordRepository passwordRepository;

	@Produces
	public PrivateKeyForObjectRepository getInstance() {
		return new PrivateKeyForObjectRepository(keystoreOpener, passwordRepository);
	}
}
