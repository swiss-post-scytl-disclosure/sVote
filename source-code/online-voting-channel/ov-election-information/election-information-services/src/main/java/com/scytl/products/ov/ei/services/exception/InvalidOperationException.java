/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.exception;

public class InvalidOperationException extends Exception{

    private static final long serialVersionUID = -5384243973472887418L;

    public InvalidOperationException(String message, Throwable cause){
        super(message,cause);
    }
    public InvalidOperationException(String message){
        super(message);
    }

}
