/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.ballotbox;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import com.scytl.products.ov.commons.beans.domain.model.EntityId;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.validation.ValidationError;

/**
 * Interface defining operations to handle ballot box.
 */
public interface BallotBoxService {

    /**
     * Check if all ballot boxes for a given tenant and election event are
     * empty.
     *
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @param ballotBoxId
     *            - the ballot box identifier.
     * @return the result of the validation.
     */
    com.scytl.products.ov.commons.validation.ValidationResult checkIfBallotBoxesAreEmpty(String tenantId,
            String electionEventId, String ballotBoxId);

    /**
     * Writes the specified signed encrypted ballot box to a given stream.
     *
     * @param stream the stream
     * @param tenantId the tenant identifier
     * @param electionEventId the election event identifier
     * @param ballotBoxId the ballot box identifier
     * @param test ballot box is for testing
     * @throws IOException I/O error occurred.
     */
    void writeEncryptedBallotBox(OutputStream stream,
                                 String tenantId, String electionEventId, String ballotBoxId, boolean test) throws IOException;



    /**
	 * Check if a ballot box is a test ballot box
	 *
	 * @param tenantId - tenant identifier
	 * @param electionEventId - election event identifier
	 * @param ballotBoxId - ballot box identifier
	 * @return if the ballot box is a test ballot box or not
	 * @throws ResourceNotFoundException
	 */
	boolean checkIfTest(String tenantId, String electionEventId, String ballotBoxId) throws ResourceNotFoundException;

	/**
     * Blocks a ballot boxes if the provided ids exist in the database
     *
     * @param tenantId - tenantIdentifier
     * @param electionEventId - election event Identifier
     * @param ballotBoxIds - ballot box identifiers
     * @throws ResourceNotFoundException
     */
    void blockBallotBoxes(String tenantId, String electionEventId, Collection<EntityId> ballotBoxIds) throws ResourceNotFoundException, EntryPersistenceException;

	/**
     * Unblocks a ballot boxes if the provided ids exist in the database
     *
     * @param tenantId - tenantIdentifier
     * @param electionEventId - election event Identifier
     * @param ballotBoxIds - ballot box identifiers
     * @throws ResourceNotFoundException
     */
    void unblockBallotBoxes(String tenantId, String electionEventId, Collection<EntityId> ballotBoxIds) throws ResourceNotFoundException, EntryPersistenceException;

	/**
	 * Check if ballot box is blocked
	 *
	 * @param tenantId - tenant identifier
	 * @param electionEventId - election event identifier
	 * @param ballotBoxId - ballot box identifier
	 * @throws ResourceNotFoundException
	 */
	boolean isBlockedBallotBox(String tenantId, String electionEventId, String ballotBoxId)
			throws ResourceNotFoundException;

	/**
	 * Validates if ballot box is not blocked
	 *
	 * @param tenantId - tenant identifier
	 * @param electionEventId - election event identifier
	 * @param ballotBoxId - ballot box identifier
	 */
	ValidationError validateBallotBoxNotBlocked(String tenantId, String electionEventId, String ballotBoxId) throws ResourceNotFoundException;

}
