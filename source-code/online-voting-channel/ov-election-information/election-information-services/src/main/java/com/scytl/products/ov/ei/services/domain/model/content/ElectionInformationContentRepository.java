/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.content;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for handling election information content entities.
 */
@Local
public interface ElectionInformationContentRepository extends BaseRepository<ElectionInformationContent, Integer> {

	/**
	 * Searches for an election information content with the given tenant and election event.
	 *
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the electionEvent.
	 * @return a entity representing the authentication content.
	 */
	ElectionInformationContent findByTenantIdElectionEventId(String tenantId, String electionEventId)
			throws ResourceNotFoundException;

}
