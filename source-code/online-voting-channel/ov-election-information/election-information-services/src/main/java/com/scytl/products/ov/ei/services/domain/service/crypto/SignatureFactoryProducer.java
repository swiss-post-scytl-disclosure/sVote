/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.crypto;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.scytl.products.ov.commons.signature.SignatureFactory;
import com.scytl.products.ov.commons.signature.SignatureFactoryImpl;

/**
 * The producer for the signature factory implementation.
 */
public class SignatureFactoryProducer {

    @Produces
    @ApplicationScoped
    public SignatureFactory getInstance() {
        return SignatureFactoryImpl.newInstance();
    }
}
