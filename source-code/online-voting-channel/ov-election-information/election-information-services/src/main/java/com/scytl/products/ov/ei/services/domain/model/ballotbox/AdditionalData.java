/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballotbox;

/**
 * Class for representing key-value pairs with additional information for vote.
 */
public class AdditionalData {
	// the key
	private String key;

	// the value
	private String value;

	/**
	 * Constructor.
	 * 
	 * @param key - the key.
	 * @param value - the value.
	 */
	public AdditionalData(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * Returns the current value of the field key.
	 *
	 * @return Returns the key.
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the value of the field key.
	 *
	 * @param key The key to set.
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Returns the current value of the field value.
	 *
	 * @return Returns the value.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value of the field value.
	 *
	 * @param value The value to set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
