/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBoxContent;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBoxContentRepository;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogConstants;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogEvents;

/**
 * Decorator of the ballot box content repository.
 */
@Decorator
public abstract class BallotBoxContentRepositoryDecorator implements BallotBoxContentRepository {

    @Inject
    @Delegate
    private BallotBoxContentRepository ballotBoxContentRepository;

    @Inject
    private TrackIdInstance trackId;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;

    @Override
    public BallotBoxContent save(final BallotBoxContent ballotBoxContent) throws DuplicateEntryException {
        try {
            final BallotBoxContent saveBallotBoxInformation = ballotBoxContentRepository.save(ballotBoxContent);
            secureLoggerWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(ElectionInformationLogEvents.BALLOT_BOX_CONTENT_SAVED)
                    .objectId(ballotBoxContent.getBallotBoxId()).user("admin")
                    .electionEvent(ballotBoxContent.getElectionEventId())
                    .additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_BOX_ID,
                        ballotBoxContent.getBallotBoxId())
                    .createLogInfo());
            return saveBallotBoxInformation;
        } catch (DuplicateEntryException ex) {
            secureLoggerWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(ElectionInformationLogEvents.ERROR_SAVING_BALLOT_BOX_CONTENT)
                    .objectId(ballotBoxContent.getBallotBoxId()).user("admin")
                    .electionEvent(ballotBoxContent.getElectionEventId())
                    .additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_BOX_ID,
                        ballotBoxContent.getBallotBoxId())
                    .additionalInfo(ElectionInformationLogConstants.INFO_ERR_DESC, ex.getMessage())
                    .createLogInfo());
            throw ex;
        }
    }
}
