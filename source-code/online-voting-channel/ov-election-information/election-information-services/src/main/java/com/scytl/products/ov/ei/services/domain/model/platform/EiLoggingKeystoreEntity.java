/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.platform;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreEntity;


/**
 * @see com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreEntity
 */
@Entity
@Table(name = "EI_LOGGING_KEYSTORES")
public class EiLoggingKeystoreEntity extends LoggingKeystoreEntity {


    /**
     * The identifier for this entity.
     */
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "eiPlatformLoggingKsSeq")
    @SequenceGenerator(name = "eiPlatformLoggingKsSeq", sequenceName = "EI_LOGGING_KEYSTORES_SEQ")
    @JsonIgnore
    private Long id;


    /**
     * Gets The identifier for this entity..
     *
     * @return Value of The identifier for this entity..
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the identifier for this entity..
     *
     * @param id New value of The identifier for this entity..
     */
    public void setId(Long id) {
        this.id = id;
    }
}