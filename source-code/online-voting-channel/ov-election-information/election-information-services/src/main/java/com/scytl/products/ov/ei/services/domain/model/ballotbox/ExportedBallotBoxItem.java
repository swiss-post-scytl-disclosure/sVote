/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.ballotbox;

public class ExportedBallotBoxItem {

    private final String tenantId;

    private final String electionEventId;

    private final String votingCardId;

    private final String ballotId;

    private final String ballotBoxId;

    private final String vote;
    
    private final String voteComputationResults;

    private final String voteCastCode;
    
    private final String castCodeComputationResults;

    private final String signature;

    private ExportedBallotBoxItem(final String tenantId,
            final String electionEventId, final String votingCardId,
            final String ballotId, final String ballotBoxId,
            final String vote, final String voteComputationResults, final String voteCastCode,
            final String castCodeComputationResults, final String signature) {
        super();
        this.tenantId = tenantId;
        this.electionEventId = electionEventId;
        this.votingCardId = votingCardId;
        this.ballotId = ballotId;
        this.ballotBoxId = ballotBoxId;
        this.vote = vote;
        this.voteComputationResults = voteComputationResults;
        this.voteCastCode = voteCastCode;
        this.castCodeComputationResults = castCodeComputationResults;
        this.signature = signature;
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public String getVotingCardId() {
        return votingCardId;
    }

    public String getBallotId() {
        return ballotId;
    }

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public String getVote() {
        return vote;
    }

    public String getVoteCastCode() {
        return voteCastCode;
    }

    public String getSignature() {
        return signature;
    }

    public String getCastCodeComputationResults() {
        return castCodeComputationResults;
    }

    public String getVoteComputationResults() {
        return voteComputationResults;
    }

    public static class ExportedBallotBoxItemBuilder {

        private String tenantId;

        private String electionEventId;

        private String votingCardId;

        private String ballotId;

        private String ballotBoxId;

        private String vote;
        
        private String voteComputationResults;

        private String voteCastCode;
        
        private String castCodeComputationResults;

        private String signature;

        public ExportedBallotBoxItemBuilder() {

        }

        public ExportedBallotBoxItemBuilder setTenantId(
                final String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ExportedBallotBoxItemBuilder setElectionEventId(
                final String electionEventId) {
            this.electionEventId = electionEventId;
            return this;
        }

        public ExportedBallotBoxItemBuilder setVotingCardId(
                final String votingCardId) {
            this.votingCardId = votingCardId;
            return this;
        }

        public ExportedBallotBoxItemBuilder setBallotId(
                final String ballotId) {
            this.ballotId = ballotId;
            return this;
        }

        public ExportedBallotBoxItemBuilder setBallotBoxId(
                final String ballotBoxId) {
            this.ballotBoxId = ballotBoxId;
            return this;
        }

        public ExportedBallotBoxItemBuilder setVote(final String vote) {
            this.vote = vote;
            return this;
        }

        public ExportedBallotBoxItemBuilder setVoteCastCode(
                final String voteCastCode) {
            this.voteCastCode = voteCastCode;
            return this;
        }
        
        public ExportedBallotBoxItemBuilder setCastCodeComputationResults(final String castCodeComputationResults) {
            this.castCodeComputationResults = castCodeComputationResults;
            return this;
        }        
        
        public ExportedBallotBoxItemBuilder setVoteComputationResults(final String voteComputationResults) {
            this.voteComputationResults = voteComputationResults;
            return this;
        }

        public ExportedBallotBoxItemBuilder setSignature(
                final String signature) {
            this.signature = signature;
            return this;
        }

        public ExportedBallotBoxItem build() {
            return new ExportedBallotBoxItem(tenantId, electionEventId,
                votingCardId, ballotId, ballotBoxId, vote, voteComputationResults, voteCastCode, castCodeComputationResults,
                signature);
        }

    }

}
