/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.ei.services.domain.model.ballot.BallotRepository;
import com.scytl.products.ov.ei.services.domain.model.ballot.BallotText;
import com.scytl.products.ov.ei.services.domain.model.ballot.BallotTextRepository;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogConstants;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogEvents;

/**
 * Decorator for the ballottext repository.
 */
@Decorator
public abstract class BallotTextRepositoryDecorator implements BallotTextRepository {

	@Inject
	@Delegate
	private BallotTextRepository ballotTextRepository;

	@Inject
	private TrackIdInstance trackId;

	@Inject
	private SecureLoggingWriter secureLoggerWriter;

	/**
	 * @see BallotRepository#findByTenantIdElectionEventIdBallotId(String,
	 *      String, String)
	 */
	@Override
	public BallotText findByTenantIdElectionEventIdBallotId(String tenantId, String electionEventId, String ballotId)
			throws ResourceNotFoundException {
		try {
			BallotText result = ballotTextRepository.findByTenantIdElectionEventIdBallotId(tenantId, electionEventId, ballotId);
			secureLoggerWriter.log(Level.INFO,
				new LogContent.LogContentBuilder().logEvent(ElectionInformationLogEvents.BALLOT_TEXT_FOUND).objectId(ballotId)
					.user("admin").electionEvent(electionEventId)
					.additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
					.additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_ID, ballotId).createLogInfo());

			return result;
		} catch (ResourceNotFoundException e) {
			secureLoggerWriter.log(Level.ERROR, new LogContent.LogContentBuilder()
				.logEvent(ElectionInformationLogEvents.BALLOT_TEXT_NOT_FOUND).objectId(ballotId).user("admin")
				.electionEvent(electionEventId)
				.additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
				.additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_ID, ballotId)
				.additionalInfo(ElectionInformationLogConstants.INFO_ERR_DESC, "Resource not found in the BALLOTTEXT table.")
				.createLogInfo());
			throw e;
		}
	}

	@Override
	public BallotText save(final BallotText ballotText) throws DuplicateEntryException {
		try {
			BallotText savedBallotText = ballotTextRepository.save(ballotText);
			secureLoggerWriter.log(Level.INFO,
				new LogContent.LogContentBuilder().logEvent(ElectionInformationLogEvents.BALLOT_TEXT_SAVED)
					.objectId(ballotText.getBallotId()).user("admin").electionEvent(ballotText.getElectionEventId())
					.additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
					.additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_ID, ballotText.getBallotId())
					.createLogInfo());

			return savedBallotText;
		} catch (DuplicateEntryException ex) {
			secureLoggerWriter.log(Level.INFO,
				new LogContent.LogContentBuilder().logEvent(ElectionInformationLogEvents.ERROR_SAVING_BALLOT_TEXT)
					.objectId(ballotText.getBallotId()).user("admin").electionEvent(ballotText.getElectionEventId())
					.additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
					.additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_ID, ballotText.getBallotId())
					.additionalInfo(ElectionInformationLogConstants.INFO_ERR_DESC, ex.getMessage())
					.createLogInfo());
			throw ex;
		}
	}
}
