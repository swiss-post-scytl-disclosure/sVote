/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.votingcard;

import java.io.OutputStream;

import com.scytl.products.ov.commons.infrastructure.csv.AbstractCSVWriter;

public class VotingCardWriter extends AbstractCSVWriter<VotingCard> {

	public VotingCardWriter(OutputStream outputStream) {
		super(outputStream);
	}

	@Override
	protected String[] extractValues(final VotingCard votingCardItem) {
		return new String[] { votingCardItem.getVotingCardId() };
	}

}
