/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.confirmation;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationMessageValidation;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogConstants;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogEvents;
import com.scytl.products.ov.ei.services.infrastructure.log.SecureLoggerHelper;

@Decorator
public class ConfirmationMessageConfirmationRequiredValidationDecorator implements ConfirmationMessageValidation {

    @Inject
    @Delegate
    private ConfirmationMessageConfirmationRequiredValidation confirmationMessageConfirmationRequiredValidation;

    @Inject
    private TrackIdInstance trackId;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;

    @Inject
    private SecureLoggerHelper secureLoggerHelper;

    /**
     * @see com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationMessageValidation#execute(java.lang.String,
     *      java.lang.String, java.lang.String,
     *      com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation,
     *      com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken)
     */
    @Override
    public ValidationError execute(String tenantId, String electionEventId, String votingCardId,
            ConfirmationInformation confirmationInformation, AuthenticationToken authenticationToken) {
        ValidationError result = confirmationMessageConfirmationRequiredValidation.execute(tenantId, electionEventId,
            votingCardId, confirmationInformation, authenticationToken);

        if (!ValidationErrorType.SUCCESS.equals(result.getValidationErrorType())) {
            String authTokenHash = secureLoggerHelper.getAuthTokenHash(authenticationToken);
            String confirmationMessageHash = secureLoggerHelper.getConfirmationMessageHash(confirmationInformation);
            String ballotBoxId = authenticationToken.getVoterInformation().getBallotBoxId();
            secureLoggerWriter.log(Level.ERROR, new LogContent.LogContentBuilder()
                .logEvent(ElectionInformationLogEvents.CONFIRMATION_MESSAGE_CONFIRMATION_NOT_REQUIRED)
                .objectId(authTokenHash).user(votingCardId).electionEvent(electionEventId)
                .additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                .additionalInfo(ElectionInformationLogConstants.INFO_HASH_CONFIRMATION_MESSAGE, confirmationMessageHash)
                .additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_BOX_ID, ballotBoxId)
                .additionalInfo(ElectionInformationLogConstants.INFO_CREDENTIAL_ID,
                    confirmationInformation.getCredentialId())
                .additionalInfo(ElectionInformationLogConstants.INFO_ERR_DESC,
                    "Error trying to confirm a vote in 1 phase election")
                .createLogInfo());
        }

        return result;
    }

}
