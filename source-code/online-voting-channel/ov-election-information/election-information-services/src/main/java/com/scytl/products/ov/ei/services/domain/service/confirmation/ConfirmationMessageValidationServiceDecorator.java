/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.confirmation;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformationResult;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogConstants;
import com.scytl.products.ov.ei.services.infrastructure.log.ElectionInformationLogEvents;
import com.scytl.products.ov.ei.services.infrastructure.log.SecureLoggerHelper;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

/**
 * Decorator of the confirmation message validation service.
 */
@Decorator
public class ConfirmationMessageValidationServiceDecorator implements ConfirmationMessageValidationService {

	@Inject
	@Delegate
	private ConfirmationMessageValidationService confirmationMessageValidationService;

	@Inject
	private TrackIdInstance trackId;

	@Inject
	private SecureLoggingWriter secureLoggerWriter;

	@Inject
	private SecureLoggerHelper secureLoggerHelper;

	/**
	 * @see com.scytl.products.ov.ei.services.domain.service.confirmation.ConfirmationMessageValidationService#validateConfirmationMessage(java.lang.String,
	 *      java.lang.String, java.lang.String,
	 *      com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation,
	 *      com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken)
	 */
	@Override
	public ConfirmationInformationResult validateConfirmationMessage(String tenantId, String electionEventId,
			String votingCardId, ConfirmationInformation confirmationInformation, AuthenticationToken authenticationToken) {
		ConfirmationInformationResult result = confirmationMessageValidationService.validateConfirmationMessage(tenantId,
			electionEventId, votingCardId, confirmationInformation, authenticationToken);

		String authTokenHash = secureLoggerHelper.getAuthTokenHash(authenticationToken);
		String confirmationMessageHash = secureLoggerHelper.getConfirmationMessageHash(confirmationInformation);
		if (result.isValid()) {
			secureLoggerWriter.log(Level.INFO,
				new LogContent.LogContentBuilder().logEvent(ElectionInformationLogEvents.CONFIRMATION_MESSAGE_VALID)
					.objectId(authTokenHash).user(votingCardId).electionEvent(electionEventId)
					.additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
					.additionalInfo(ElectionInformationLogConstants.INFO_HASH_CONFIRMATION_MESSAGE, confirmationMessageHash)
					.createLogInfo());
		} else {
			String ballotBoxId = authenticationToken.getVoterInformation().getBallotBoxId();
			secureLoggerWriter
				.log(Level.ERROR,
					new LogContent.LogContentBuilder()
						.logEvent(ElectionInformationLogEvents.CONFIRMATION_MESSAGE_INVALID_CONFIRMATION_MESSAGE)
						.objectId(authTokenHash).user(votingCardId).electionEvent(electionEventId)
						.additionalInfo(ElectionInformationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
						.additionalInfo(ElectionInformationLogConstants.INFO_HASH_CONFIRMATION_MESSAGE,
							confirmationMessageHash)
				.additionalInfo(ElectionInformationLogConstants.INFO_BALLOT_BOX_ID, ballotBoxId)
				.additionalInfo(ElectionInformationLogConstants.INFO_ERR_DESC, "Invalid Confirmation message")
				.createLogInfo());
		}

		return result;
	}

}
