
--------------------------------------------------------
--  File created - Wednesday-July-15-2015   
--------------------------------------------------------
-- DROP TABLE "BALLOT" cascade constraints;
-- DROP TABLE "BALLOT_BOX" cascade constraints;
-- DROP TABLE "BALLOT_BOX_CONTENT" cascade constraints;
-- DROP TABLE "BALLOT_BOX_INFORMATION" cascade constraints;
-- DROP TABLE "BALLOT_TEXT" cascade constraints;
-- DROP TABLE "ELECTION_INFORMATION_CONTENT" cascade constraints;
-- DROP TABLE "VOTE_CAST_CODE" cascade constraints;
-- DROP SEQUENCE "BALLOT_BOX_CONTENT_SEQ";
-- DROP SEQUENCE "BALLOT_BOX_INFORMATION_SEQ";
-- DROP SEQUENCE "EI_ELECTORAL_AUTHORITY_SEQ";
-- DROP SEQUENCE "BALLOT_BOX_SEQ";
-- DROP SEQUENCE "BALLOT_TEXT_SEQ";
-- DROP SEQUENCE "BALLOT_SEQ";
-- DROP SEQUENCE "EI_CONTENT_SEQ";
-- DROP SEQUENCE "VOTE_CAST_CODE_SEQ";
--------------------------------------------------------
--  DDL for Sequence BALLOT_BOX_CONTENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BALLOT_BOX_CONTENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence BALLOT_BOX_INFORMATION_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BALLOT_BOX_INFORMATION_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
   --------------------------------------------------------
--  DDL for Sequence EI_ELECTORAL_AUTHORITY_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "EI_ELECTORAL_AUTHORITY_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence BALLOT_BOX_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BALLOT_BOX_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence BALLOT_BOX_TEXT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BALLOT_TEXT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence BALLOT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BALLOT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

--------------------------------------------------------
--  DDL for Sequence EI_CONTENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "EI_CONTENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence VOTE_CAST_CODE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "VOTE_CAST_CODE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

--------------------------------------------------------
--  DDL for Table BALLOT
--------------------------------------------------------

  CREATE TABLE "BALLOT" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
	"BALLOT_ID" VARCHAR2(100 CHAR), 
	"JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table BALLOT_BOX
--------------------------------------------------------

  CREATE TABLE "BALLOT_BOX" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
	"VOTING_CARD_ID" VARCHAR2(100 CHAR), 
	"BALLOT_ID" VARCHAR2(100 CHAR), 
	"BALLOT_BOX_ID" VARCHAR2(100 CHAR), 
	"VOTE" CLOB
   ) ;

--------------------------------------------------------
--  DDL for Table BALLOT_BOX_CONTENT
--------------------------------------------------------

  CREATE TABLE "BALLOT_BOX_CONTENT" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
	"BALLOT_BOX_ID" VARCHAR2(100 CHAR), 
	"JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table BALLOT_BOX_INFORMATION
--------------------------------------------------------

  CREATE TABLE "BALLOT_BOX_INFORMATION" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
	"BALLOT_BOX_ID" VARCHAR2(100 CHAR), 
	"JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table EI_ELECTORAL_AUTHORITY
--------------------------------------------------------

  CREATE TABLE "EI_ELECTORAL_AUTHORITY" 
   (	"ID" NUMBER(*,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 BYTE), 
	"ELECTORAL_AUTHORITY_ID" VARCHAR2(100 BYTE), 
	"JSON" CLOB
   ) ;   
--------------------------------------------------------
--  DDL for Table BALLOT_TEXT
--------------------------------------------------------

  CREATE TABLE "BALLOT_TEXT" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"BALLOT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
	"JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table ELECTION_INFORMATION_CONTENT
--------------------------------------------------------

  CREATE TABLE "ELECTION_INFORMATION_CONTENT" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
	"JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table VOTE_CAST_CODE
--------------------------------------------------------

  CREATE TABLE "VOTE_CAST_CODE" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 CHAR), 
	"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
	"VOTING_CARD_ID" VARCHAR2(100 CHAR), 
	"VOTE_CAST_CODE" VARCHAR2(100 CHAR), 
	"SIGNATURE" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Index BALLOT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_PK" ON "BALLOT" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index BALLOT_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_UK1" ON "BALLOT" ("TENANT_ID", "ELECTION_EVENT_ID", "BALLOT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index BALLOT_BOX_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_BOX_PK" ON "BALLOT_BOX" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index BALLOT_BOX_CONTENT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_BOX_CONTENT_PK" ON "BALLOT_BOX_CONTENT" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index BALLOT_BOX_CONTENT_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_BOX_CONTENT_UK1" ON "BALLOT_BOX_CONTENT" ("TENANT_ID", "ELECTION_EVENT_ID", "BALLOT_BOX_ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C007090
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C007090" ON "BALLOT_BOX_INFORMATION" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index EI_ELECTORAL_AUTHORITY_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "EI_ELECTORAL_AUTHORITY_UK1" ON "EI_ELECTORAL_AUTHORITY" ("TENANT_ID", "ELECTION_EVENT_ID", "ELECTORAL_AUTHORITY_ID") 
  ;  
--------------------------------------------------------
--  DDL for Index BALLOT_BOX_INFORMATION_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_BOX_INFORMATION_UK1" ON "BALLOT_BOX_INFORMATION" ("BALLOT_BOX_ID", "ELECTION_EVENT_ID", "TENANT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index BALLOT_TEXT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_TEXT_PK" ON "BALLOT_TEXT" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index BALLOT_TEXT_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "BALLOT_TEXT_UK1" ON "BALLOT_TEXT" ("TENANT_ID", "BALLOT_ID", "ELECTION_EVENT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C007037
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C007037" ON "ELECTION_INFORMATION_CONTENT" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index EI_CONTENT_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "EI_CONTENT_UK1" ON "ELECTION_INFORMATION_CONTENT" ("TENANT_ID", "ELECTION_EVENT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C007045
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C007045" ON "VOTE_CAST_CODE" ("ID")
  ;
--------------------------------------------------------
--  Constraints for Table BALLOT
--------------------------------------------------------

  ALTER TABLE "BALLOT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT" MODIFY ("BALLOT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "BALLOT" ADD CONSTRAINT "BALLOT_PK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "BALLOT" ADD CONSTRAINT "BALLOT_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID", "BALLOT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table BALLOT_BOX
--------------------------------------------------------

  ALTER TABLE "BALLOT_BOX" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX" MODIFY ("VOTING_CARD_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX" MODIFY ("BALLOT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX" MODIFY ("BALLOT_BOX_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX" MODIFY ("VOTE" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX" ADD CONSTRAINT "BALLOT_BOX_PK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "BALLOT_BOX" ADD CONSTRAINT "BALLOT_BOX_UK_SINGLE_VOTING" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID","VOTING_CARD_ID", "BALLOT_ID", "BALLOT_BOX_ID");

--------------------------------------------------------
--  Constraints for Table BALLOT_BOX_CONTENT
--------------------------------------------------------

  ALTER TABLE "BALLOT_BOX_CONTENT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_CONTENT" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_CONTENT" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_CONTENT" MODIFY ("BALLOT_BOX_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_CONTENT" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_CONTENT" ADD CONSTRAINT "BALLOT_BOX_CONTENT_PK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "BALLOT_BOX_CONTENT" ADD CONSTRAINT "BALLOT_BOX_CONTENT_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID", "BALLOT_BOX_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table BALLOT_BOX_INFORMATION
--------------------------------------------------------

  ALTER TABLE "BALLOT_BOX_INFORMATION" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_INFORMATION" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_INFORMATION" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_INFORMATION" MODIFY ("BALLOT_BOX_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_INFORMATION" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_BOX_INFORMATION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "BALLOT_BOX_INFORMATION" ADD CONSTRAINT "BALLOT_BOX_INFORMATION_UK1" UNIQUE ("BALLOT_BOX_ID", "ELECTION_EVENT_ID", "TENANT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table EI_ELECTORAL_AUTHORITY
--------------------------------------------------------

  ALTER TABLE "EI_ELECTORAL_AUTHORITY" MODIFY ("ID" NOT NULL ENABLE);  
  ALTER TABLE "EI_ELECTORAL_AUTHORITY" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "EI_ELECTORAL_AUTHORITY" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "EI_ELECTORAL_AUTHORITY" MODIFY ("ELECTORAL_AUTHORITY_ID" NOT NULL ENABLE);
  ALTER TABLE "EI_ELECTORAL_AUTHORITY" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "EI_ELECTORAL_AUTHORITY" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "EI_ELECTORAL_AUTHORITY" ADD CONSTRAINT "EI_ELECTORAL_AUTHORITY_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID", "ELECTORAL_AUTHORITY_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table BALLOT_TEXT
--------------------------------------------------------

  ALTER TABLE "BALLOT_TEXT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_TEXT" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_TEXT" MODIFY ("BALLOT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_TEXT" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_TEXT" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "BALLOT_TEXT" ADD CONSTRAINT "BALLOT_TEXT_PK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "BALLOT_TEXT" ADD CONSTRAINT "BALLOT_TEXT_UK1" UNIQUE ("TENANT_ID", "BALLOT_ID", "ELECTION_EVENT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table ELECTION_INFORMATION_CONTENT
--------------------------------------------------------

  ALTER TABLE "ELECTION_INFORMATION_CONTENT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ELECTION_INFORMATION_CONTENT" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "ELECTION_INFORMATION_CONTENT" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "ELECTION_INFORMATION_CONTENT" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "ELECTION_INFORMATION_CONTENT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ELECTION_INFORMATION_CONTENT" ADD CONSTRAINT "EI_CONTENT_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table VOTE_CAST_CODE
--------------------------------------------------------

  ALTER TABLE "VOTE_CAST_CODE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "VOTE_CAST_CODE" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "VOTE_CAST_CODE" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "VOTE_CAST_CODE" MODIFY ("VOTING_CARD_ID" NOT NULL ENABLE);
  ALTER TABLE "VOTE_CAST_CODE" MODIFY ("VOTE_CAST_CODE" NOT NULL ENABLE);
  ALTER TABLE "VOTE_CAST_CODE" MODIFY ("SIGNATURE" NOT NULL ENABLE);
  ALTER TABLE "VOTE_CAST_CODE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "VOTE_CAST_CODE" ADD CONSTRAINT "CAST_CODE_UK_SINGLE_VOTING" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID", "VOTING_CARD_ID");

  
--------------------------------------------------------
--  VOTE_VALIDATION
--------------------------------------------------------
CREATE SEQUENCE  "VOTE_VALIDATION_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

CREATE TABLE "VOTE_VALIDATION" 
(	
"ID" NUMBER(11,0), 
"TENANT_ID" VARCHAR2(100 CHAR), 
"ELECTION_EVENT_ID" VARCHAR2(100 CHAR), 
"VOTING_CARD_ID" VARCHAR2(100 CHAR), 
"VOTE_HASH" VARCHAR2(150 CHAR)
) ;

CREATE UNIQUE INDEX "VOTE_VALIDATION_PK" ON "VOTE_VALIDATION" ("ID") 
;

CREATE UNIQUE INDEX "VOTE_VALIDATION_UK1" ON "VOTE_VALIDATION" ("TENANT_ID", "ELECTION_EVENT_ID", "VOTING_CARD_ID", "VOTE_HASH") 
;


ALTER TABLE "VOTE_VALIDATION" MODIFY ("ID" NOT NULL ENABLE);
ALTER TABLE "VOTE_VALIDATION" MODIFY ("TENANT_ID" NOT NULL ENABLE);
ALTER TABLE "VOTE_VALIDATION" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
ALTER TABLE "VOTE_VALIDATION" MODIFY ("VOTING_CARD_ID" NOT NULL ENABLE);
ALTER TABLE "VOTE_VALIDATION" MODIFY ("VOTE_HASH" NOT NULL ENABLE);
ALTER TABLE "VOTE_VALIDATION" ADD CONSTRAINT "VOTE_VALIDATION_PK" PRIMARY KEY ("ID") ENABLE;
ALTER TABLE "VOTE_VALIDATION" ADD CONSTRAINT "VOTE_VALIDATION_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID", "VOTING_CARD_ID", "VOTE_HASH") ENABLE;


-- SIGNATURE
ALTER TABLE BALLOT ADD ("SIGNATURE" CLOB);
--ALTER TABLE "BALLOT" MODIFY ("SIGNATURE" NOT NULL ENABLE);

ALTER TABLE BALLOT_TEXT ADD ("SIGNATURE" CLOB);
--ALTER TABLE "BALLOT_TEXT" MODIFY ("SIGNATURE" NOT NULL ENABLE);

ALTER TABLE BALLOT_BOX_INFORMATION ADD ("SIGNATURE" CLOB);
--ALTER TABLE "BALLOT_BOX_INFORMATION" MODIFY ("SIGNATURE" NOT NULL ENABLE);

