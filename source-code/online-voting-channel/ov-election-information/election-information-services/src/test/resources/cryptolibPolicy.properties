#
# Copyright 2018 Scytl Secure Electronic Voting SA
#
# All rights reserved
#
# See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
#

## Release 2.0.1

##===========
## Primitives
##===========

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
primitives.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
primitives.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: algorithm - hash - provider
# Options: [ MGF1_SHA256_BC | MGF1_SHA256_DEFAULT]
primitives.kdfderivation=MGF1_SHA256_BC

# Key defines: algorithm - iterations - hash - salt bit length - provider - key bit length
# Options: [ PBKDF2_32000_SHA256_256_SUNJCE_KL128 | PBKDF2_32000_SHA256_256_BC_KL128]
# NOTE: PBKDF2_32000_SHA256_256_SUNJCE_KL128 is supported since JCE 1.8
primitives.pbkdfderivation=PBKDF2_32000_SHA256_256_BC_KL128

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
primitives.pbkdfderivation.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
primitives.pbkdfderivation.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: algorithm - provider
# Options: [SHA256_SUN | SHA256_BC | SHA256_DEFAULT | SHA512_224_BC | SHA512_224_DEFAULT]
primitives.messagedigest=SHA256_SUN

##==========
## Symmetric
##==========

# Key defines: algorithm - key size - provider
# Options: [AES_128_BC | AES_128_SUNJCE]
symmetric.encryptionsecretkey=AES_128_SUNJCE

# Key defines: algorithm - initialization vector bit length - authentication tag size - provider
# Options: [AESwithGCMandNOPADDING_96_128_BC, AESwithGCMandNOPADDING_96_128_DEFAULT]
symmetric.cipher=AESwithGCMandNOPADDING_96_128_BC

# Key defines: algorithm - key size - provider
# Options: [HMACwithSHA256_256]
symmetric.macsecretkey=HMACwithSHA256_256

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
symmetric.key.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
symmetric.key.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
symmetric.cipher.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
symmetric.cipher.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: algorithm - provider
# Options: [HMACwithSHA256_SUN, HMACwithSHA256_BC, HMACwithSHA256_DEFAULT]
symmetric.mac=HMACwithSHA256_SUN

##===========
## Asymmetric
##===========

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_SUN_RSA_SIGN | RSA_3072_F4_SUN_RSA_SIGN | RSA_4096_F4_SUN_RSA_SIGN | RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC | RSA_2048_OPENSSL_RSA_SIGN | RSA_3072_OPENSSL_RSA_SIGN | RSA_4096_OPENSSL_RSA_SIGN]
asymmetric.signingkeypair=RSA_2048_F4_SUN_RSA_SIGN

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_SUN_RSA_SIGN | RSA_3072_F4_SUN_RSA_SIGN | RSA_4096_F4_SUN_RSA_SIGN | RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC | RSA_2048_OPENSSL_RSA_SIGN | RSA_3072_OPENSSL_RSA_SIGN | RSA_4096_OPENSSL_RSA_SIGN]
asymmetric.encryptionkeypair=RSA_2048_F4_SUN_RSA_SIGN

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
asymmetric.keypair.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
asymmetric.keypair.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: algorithm - provider
# Options: [ RSAwithRSAKEMANDKDF1ANDSHA256_* | RSAwithRSAKEMANDKDF2ANDSHA256_*]
# (where ? = BC, SUN, or DEFAULT and * = BC or DEFAULT)
asymmetric.cipher=RSAwithRSAKEMANDKDF1ANDSHA256_BC

# Key defines: PRNG for LINUX systems
# Options: [NATIVE_PRNG_SUN]
asymmetric.cipher.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: PRNG for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
asymmetric.cipher.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: algorithm - key size - provider
# Options: [AES_128_BC | AES_128_SUNJCE]
asymmetric.cipher.symmetric.encryptionsecretkey=AES_128_SUNJCE

# Key defines: algorithm - initialization vector size - authentication tag size - provider
# Options: [AESwithGCMandNOPADDING_96_128_BC, AESwithGCMandNOPADDING_96_128_DEFAULT]
asymmetric.cipher.symmetric.cipher=AESwithGCMandNOPADDING_96_128_BC

# Key defines: algorithm - padding md - padding mgf - padding mgf md - padding salt size - padding trailer field - provider
# Options: [SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC,SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_DEFAULT]
asymmetric.signer=SHA256withRSAandPSS_SHA256_MGF1_SHA256_32_1_BC

# Key defines: PRNG for LINUX systems
# Options: [NATIVE_PRNG_SUN]
asymmetric.signer.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: PRNG for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
asymmetric.signer.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: signature algorithm - digest algorithm - transform method - canonicalization method - mechanism type - provider
# Options: [SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG, SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_DEFAULT]
asymmetric.xml.signer=SHA256withRSA_SHA256_ENVELOPED_EXCLUSIVE_DOM_XMLDSIG

# Key defines: algorithm - padding md - padding mgf - padding mgf md - padding salt size - padding trailer field - provider
# Options: [SHA256withRSA_SHA256_BC,SHA256withRSA_SHA256_DEFAULT]
asymmetric.cms.signer=SHA256withRSA_SHA256_BC

##=============
## Certificates
##=============

# Key defines: hash and algorithm - provider
# Options: [SHA256_WITH_RSA_BC]
certificates.x509certificate=SHA256_WITH_RSA_BC

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
certificates.x509certificate.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
certificates.x509certificate.securerandom.windows=PRNG_SUN_MSCAPI

##======================
## ElGamal - Homomorphic
##======================

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
elgamal.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
elgamal.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: group type - L the bit length of p - N the bit length of q (for ZP)
# Options: [ZP_2048_224 | ZP_2048_256 | QR_2048]
elgamal.grouptype=QR_2048

##=======
## Proofs
##=======

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
proofs.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
proofs.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: character set to be supported the proofs hash builder
# Options: [UTF8]
proofs.hashbuilder.charset=UTF8

# Key defines: hash to be used by the proofs hash builder
# Options: [SHA256_SUN | SHA256_BC | SHA256_DEFAULT | SHA512_224_BC | SHA512_224_DEFAULT]
proofs.hashbuilder.messagedigest=SHA256_SUN

##=======
## Stores
##=======

# Key defines: provider
# Options: [ SUN | BC | DEFAULT ]
stores.keystore=SUN

##===============
## Scytl-Keystore
##===============

# Key defines: type - provider
# Options: [PKCS12_BC | PKCS12_SUN_JSSE | PKCS12_DEFAULT]
scytl.keystore.p12=PKCS12_SUN_JSSE

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
primitives.securerandom.linux.p12=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
primitives.securerandom.windows.p12=PRNG_SUN_MSCAPI

# Key defines: algorithm - hash - provider
# Options: [ MGF1_SHA256_BC | MGF1_SHA256_DEFAULT]
primitives.kdfderivation.p12=MGF1_SHA256_BC

# Key defines: algorithm - iterations - hash - salt bit length - provider - key bit length
# Options: [ PBKDF2_32000_SHA256_256_SUNJCE_KL128 | PBKDF2_32000_SHA256_256_BC_KL128]
# NOTE: PBKDF2_32000_SHA256_256_SUNJCE_KL128 is supported since JCE 1.8
primitives.pbkdfderivation.p12=PBKDF2_32000_SHA256_256_BC_KL128

# Key defines: algorithm - provider for LINUX systems
# Options: [NATIVE_PRNG_SUN]
primitives.pbkdfderivation.securerandom.p12.linux=NATIVE_PRNG_SUN

# Key defines: algorithm - provider for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
primitives.pbkdfderivation.securerandom.p12.windows=PRNG_SUN_MSCAPI

#=================
# Digital Envelope
#=================

# Key defines: PRNG for LINUX systems
# Options: [NATIVE_PRNG_SUN]
digital.envelope.securerandom.linux=NATIVE_PRNG_SUN

# Key defines: PRNG for WINDOWS systems
# Options: [PRNG_SUN_MSCAPI]
digital.envelope.securerandom.windows=PRNG_SUN_MSCAPI

# Key defines: algorithm - key size - provider
# Options: [AES_128_BC | AES_128_SUNJCE]
digital.envelope.symmetric.encryptionsecretkey=AES_128_SUNJCE

# Key defines: algorithm - key size - provider
# Options: [HMACwithSHA256_256]
digital.envelope.symmetric.macsecretkey=HMACwithSHA256_256

# Key defines: algorithm - provider
# Options: [HMACwithSHA256_SUN, HMACwithSHA256_BC, HMACwithSHA256_DEFAULT]
digital.envelope.symmetric.mac=HMACwithSHA256_SUN

# Key defines: algorithm - initialization vector size - authentication tag size - provider
# Options: [AESwithGCMandNOPADDING_96_128_BC, AESwithGCMandNOPADDING_96_128_DEFAULT]
digital.envelope.symmetric.cipher=AESwithGCMandNOPADDING_96_128_BC

# Key defines: algorithm - provider
# Options: [RSAwithRSAKEMANDKDF1ANDSHA256_BC]
digital.envelope.asymmetric.cipher=RSAwithRSAKEMANDKDF1ANDSHA256_BC

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_SUN_RSA_SIGN | RSA_3072_F4_SUN_RSA_SIGN | RSA_4096_F4_SUN_RSA_SIGN | RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC]
digital.envelope.asymmetric.signingkeypair=RSA_2048_F4_SUN_RSA_SIGN

# Key defines: algorithm - key size - public exponent - provider
# Options: [RSA_2048_F4_SUN_RSA_SIGN | RSA_3072_F4_SUN_RSA_SIGN | RSA_4096_F4_SUN_RSA_SIGN | RSA_2048_F4_BC | RSA_3072_F4_BC | RSA_4096_F4_BC]
digital.envelope.asymmetric.encryptionkeypair=RSA_2048_F4_SUN_RSA_SIGN

