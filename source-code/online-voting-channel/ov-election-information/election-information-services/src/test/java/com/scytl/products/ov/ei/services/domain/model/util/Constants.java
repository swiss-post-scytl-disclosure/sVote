/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.util;

/**
 * Class containing constants for testing purposes
 */
public class Constants {


    public static final String TENANT_ID = "100";

    public static final String BALLOT_ID = "100";

    public static final String BALLOT_BOX_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String VOTING_CARD_ID = "100";


}
