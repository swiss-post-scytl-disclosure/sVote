/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.rule;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.SemanticErrorException;
import com.scytl.products.ov.commons.beans.exceptions.SyntaxErrorException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.validation.ValidationError;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class VoteIdsRuleImplTest {

    @InjectMocks
    @Spy
    private VoteIdsRule voteIdsRule = new VoteIdsRule();

    @Mock
    private Logger LOG;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Test
    public void validVoteAndToken() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.SUCCESS));
    }

    @Test
    public void invalidAuthTokenJson() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"voterInformation\"{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a\n01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doThrow(SyntaxErrorException.class).when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidAuthToken() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a\n01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doThrow(SyntaxErrorException.class).when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidAuthToken2() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\"\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidNullVote() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = null;

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidNullAuthToken() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString = null;
        vote.setAuthenticationToken(authTokenString);

        doThrow(SyntaxErrorException.class).when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidNullVoterInformation() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidTenantId() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("XXXX");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidNullElectionEventIdInVote()
            throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId(null);
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidNullElectionEventIdInAuthToken()
            throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidEmptyElectionEventIdInAuthToken()
            throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidVotingCardId() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98c\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidBallotBoxId() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c86");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidBallotId() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0ed");
        vote.setCredentialId("0edc375a02584cb09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }

    @Test
    public void invalidCredentialId() throws ApplicationException, SyntaxErrorException, SemanticErrorException {
        Vote vote = new Vote();
        vote.setTenantId("100");
        vote.setElectionEventId("f86b9967f5f14a01b9f70c6277c38329");
        vote.setVotingCardId("14d96e761e571a08769dc4337a2ce98b");
        vote.setBallotBoxId("b972e500fe554ed7b24e54814e9b3c85");
        vote.setBallotId("0edd4256df7d4b17a148f218080abea9");
        vote.setCredentialId("b09f35a9b956bc768f");

        String authTokenString =
            "{\"id\":\"f4fqq5pbp+CQwy68GJEX0A==\",\"voterInformation\":{\"tenantId\":\"100\",\"electionEventId\":\"f86b9967f5f14a01b9f70c6277c38329\","
                + "\"votingCardId\":\"14d96e761e571a08769dc4337a2ce98b\",\"ballotId\":\"0edd4256df7d4b17a148f218080abea9\",\"credentialId\":\"0edc375a02584cb09f35a9b956bc768f\",\"verificationCardId\":\"4f55fa5d99484d22b983004206b12444\",\"ballotBoxId\":\"b972e500fe554ed7b24e54814e9b3c85\",\"votingCardSetId\":\"8ba6e4decda64a5eb43c28657df7be3d\",\"verificationCardSetId\":\"4e509620593647aab6df2aa175c833e7\"},\"timestamp\":\"1452850289632\",\"signature\":\"WKIQj8tibxtOoFtVe6/bBFXSv9ZdYhAF6bPymCkqUBsN25RYo0DIy9jWz2G6yt91c2UJkWwXzZCoJDE53l2YvcuUfeeAs66FV2q4Et1sNLa+wiNN4LL1cNx1wc7+9pgRUL15ItO8fN9XM1y6fF61aGBOng9KOvKX7YbkHPzl3/+7vkhF/5oMS3nj871yPUpx4Yxwg7RpSKvFXdAeW3iRBeYbN2BXmc8euWmF80YD5KJUd3aGJ/Z5O/oGWCP2YHzF1OVZzAe2hs8TY1k88D0pc+s8VUAohG3Qf59iuVD4ov1KurrRsJmP9F/KtDeh9qIqMRpJAYSKPZ+c2MQUcOfLkw==\"}";
        vote.setAuthenticationToken(authTokenString);

        doNothing().when(voteIdsRule).validateAuthToken(any());

        ValidationError result = voteIdsRule.execute(vote);
        assertTrue(result.getValidationErrorType().equals(ValidationErrorType.FAILED));
    }
}
