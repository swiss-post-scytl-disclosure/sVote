/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImplTest;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.ei.services.domain.model.ballotbox.BallotBoxInformation;

/**
 * Test class for the ballot box info repository
 */
@RunWith(MockitoJUnitRunner.class)
public class BallotBoxInformationRepositoryImplTest extends BaseRepositoryImplTest<BallotBoxInformation, Integer> {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Mock
	private TypedQuery<BallotBoxInformation> queryMock;

	@Mock
	private SecureLoggingWriter secureLoggerWriter;

	@Mock
	private TrackIdInstance trackId;

	@InjectMocks
	private static BallotBoxInformationRepositoryImpl ballotBoxInformationRepository =
		new BallotBoxInformationRepositoryImpl();

	/**
	 * Creates a new object of the testing class.
	 */
	public BallotBoxInformationRepositoryImplTest() throws InstantiationException, IllegalAccessException {
		super(BallotBoxInformation.class, ballotBoxInformationRepository.getClass());
	}

	@Before
	public void setup() {
		doNothing().when(secureLoggerWriter).log(any(Level.class), any(LogContent.class));
	}

	@Test
	public void findByExternalIdAndTenantId() throws ResourceNotFoundException {
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxInformation.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenReturn(new BallotBoxInformation());

		String tenantId = "2";
		String ballotBoxId = "2";
		String electionEventId = "2";
		assertNotNull(ballotBoxInformationRepository.findByTenantIdElectionEventIdBallotBoxId(tenantId, electionEventId,
			ballotBoxId));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findNonExistentBallotBoxInfo() throws ResourceNotFoundException {
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxInformation.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenThrow(ResourceNotFoundException.class);

		expectedException.expect(ResourceNotFoundException.class);

		String tenantId = "2";
		String ballotBoxId = "2";
		String electionEventId = "2";
		ballotBoxInformationRepository.findByTenantIdElectionEventIdBallotBoxId(tenantId, electionEventId, ballotBoxId);
	}
}
