/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImplTest;
import com.scytl.products.ov.ei.services.domain.model.ballot.BallotText;
import com.scytl.products.ov.ei.services.domain.model.ballot.BallotTextRepository;

/**
 * Junit tests for the class {@link BallotTextRepositoryImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class BallotTextRepositoryImplTest extends BaseRepositoryImplTest<BallotText, Integer> {

	@Mock
	private TypedQuery<BallotText> queryMock;

	@InjectMocks
	private static BallotTextRepository ballotTextRepository = new BallotTextRepositoryImpl();

	/**
	 * Creates a new object of the testing class.
	 */
	public BallotTextRepositoryImplTest() throws InstantiationException, IllegalAccessException {
		super(BallotText.class, ballotTextRepository.getClass());
	}

	@Test
	public void findByBallotIdElectionEventIdTenantId() throws ResourceNotFoundException {
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotText.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenReturn(new BallotText());

		String ballotId = "2";
		String tenantId = "2";
		String electionEventId = "2";
		assertNotNull(ballotTextRepository.findByTenantIdElectionEventIdBallotId(tenantId, electionEventId, ballotId));
	}

	@Test
	public void findByBallotIdElectionEventIdTenantIdException() throws ResourceNotFoundException {
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotText.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenThrow(new NoResultException());

		expectedException.expect(ResourceNotFoundException.class);

		String ballotId = "1";
		String tenantId = "1";
		String electionEventId = "1";
		ballotTextRepository.findByTenantIdElectionEventIdBallotId(tenantId, electionEventId, ballotId);
	}
}
