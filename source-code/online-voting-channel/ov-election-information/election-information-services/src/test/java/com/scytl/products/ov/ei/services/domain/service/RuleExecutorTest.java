/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.domain.model.rule.AbstractRule;
import com.scytl.products.ov.commons.domain.service.RuleExecutor;

/**
 * Junit tests for the class {@link RuleExecutor}.
 */
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
public class RuleExecutorTest<T> {
    @Mock
    private Logger logger;

    @SuppressWarnings("rawtypes")
    @InjectMocks
    private RuleExecutor ruleExecutor = new RuleExecutor();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private List<AbstractRule<T>> rulesList;

    private Vote vote;

    @Test
    public void executeThrowsApplicationException() throws ApplicationException {
        expectedException.expect(ApplicationException.class);

        rulesList = new ArrayList<AbstractRule<T>>();
        ruleExecutor.execute(rulesList, vote);
    }

    @Test
    public void executeWithEmptyList4ListNull() throws ApplicationException {
        vote = new Vote();
        assertTrue(ruleExecutor.execute(rulesList, vote).getValidationErrorType().equals(ValidationErrorType.SUCCESS));
    }

    @Test
    public void executeWithEmptyList4ListEmpty() throws ApplicationException {
        vote = new Vote();
        rulesList = new ArrayList<AbstractRule<T>>();
        assertTrue(ruleExecutor.execute(rulesList, vote).getValidationErrorType().equals(ValidationErrorType.SUCCESS));
    }
}
