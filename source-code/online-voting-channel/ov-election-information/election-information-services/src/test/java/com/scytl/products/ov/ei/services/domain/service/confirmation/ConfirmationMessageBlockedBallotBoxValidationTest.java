/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.service.confirmation;

import static com.scytl.products.ov.ei.services.domain.model.util.Constants.BALLOT_BOX_ID;
import static com.scytl.products.ov.ei.services.domain.model.util.Constants.ELECTION_EVENT_ID;
import static com.scytl.products.ov.ei.services.domain.model.util.Constants.TENANT_ID;
import static com.scytl.products.ov.ei.services.domain.model.util.Constants.VOTING_CARD_ID;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.beans.domain.model.Information.VoterInformation;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.ei.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.ei.services.domain.service.ballotbox.BallotBoxService;

@RunWith(MockitoJUnitRunner.class)
public class ConfirmationMessageBlockedBallotBoxValidationTest {

	@Mock
	private Logger LOG;
	 
	@InjectMocks
    private ConfirmationMessageBlockedBallotBoxValidation validation =
        new ConfirmationMessageBlockedBallotBoxValidation();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());
       
        doNothing().when(LOG).error(anyString());
    }

    @Mock
    private AuthenticationToken token;

    @Mock
    private ConfirmationInformation confirmationInformation;

    @Mock
    private BallotBoxService ballotBoxService;

    @Rule
    public ExpectedException expectedException;

    
    @Test
    public void validate() throws ResourceNotFoundException {

        final VoterInformation voterInformationMock = mock(VoterInformation.class);
        when(token.getVoterInformation()).thenReturn(voterInformationMock);
        when(voterInformationMock.getBallotBoxId()).thenReturn(BALLOT_BOX_ID);
        when(ballotBoxService.validateBallotBoxNotBlocked(anyString(), anyString(), anyString()))
            .thenReturn(new ValidationError(ValidationErrorType.SUCCESS));

        final ValidationError execute =
            validation.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformation, token);
        assertThat(execute.getValidationErrorType(), is(ValidationErrorType.SUCCESS));

    }

    @Test
    public void validateNotFound() throws ResourceNotFoundException {

        final VoterInformation voterInformationMock = mock(VoterInformation.class);
        when(token.getVoterInformation()).thenReturn(voterInformationMock);
        when(voterInformationMock.getBallotBoxId()).thenReturn(BALLOT_BOX_ID);
        when(ballotBoxService.validateBallotBoxNotBlocked(anyString(), anyString(), anyString()))
            .thenThrow(new ResourceNotFoundException("exception"));

        final ValidationError execute =
            validation.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformation, token);
        assertThat(execute.getValidationErrorType(), is(ValidationErrorType.RESOURCE_NOT_FOUND));

    }

}
