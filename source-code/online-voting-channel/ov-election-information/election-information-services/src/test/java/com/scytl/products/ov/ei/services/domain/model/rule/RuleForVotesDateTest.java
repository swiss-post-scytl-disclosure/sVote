/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ei.services.domain.model.rule;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.ei.services.domain.model.validation.ElectionValidationRequest;
import com.scytl.products.ov.ei.services.domain.service.election.ElectionService;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RuleForVotesDateTest {

    @Mock
    private ElectionService electionService;

    @InjectMocks
    private ElectionDatesRule rule = new ElectionDatesRule();

    private static String ballotBoxId = "73783638d2984205b821f38e82e265c5";

    private static String votingCardId = "73553638d2984205b821f38e82e265c5";

    private String authenticationToken =
        "{\"id\": \"lnniWSgf+XDd4dasaIX9rQ==\",\"voterInformation\": {\"electionEventId\": \"100\","
            + "\"votingCardId\": \"100\",\"ballotId\": \"100\",\"verificationCardId\": \"100\",\"tenantId\":\"100\",\"ballotBoxId\": \"100\",\"votingCardSetId\": \"100\",\"credentialId\":\"100\","
            + "\"verificationCardSetId\": \"100\"},\"timestamp\": \"1430759337499\",\"signature\": \"base64encodedSignature==\"}";

    private Vote vote;

    @Before
    public void setup() {
        vote = new Vote();
        vote.setBallotBoxId(ballotBoxId);
        vote.setAuthenticationToken(authenticationToken);
        vote.setVotingCardId(votingCardId);
    }

    @Test
    public void electionInDatesTest() throws ResourceNotFoundException {
        ValidationError validationError = new ValidationError();
        validationError.setValidationErrorType(ValidationErrorType.SUCCESS);
        when(electionService.validateIfElectionIsOpen(any(ElectionValidationRequest.class))).thenReturn(
            validationError);
        assertTrue(rule.execute(vote).getValidationErrorType().equals(ValidationErrorType.SUCCESS));
    }

    @Test
    public void electionBeforeDateFrom() throws ResourceNotFoundException, GeneralCryptoLibException {
        ValidationError validationError = new ValidationError();
        validationError.setValidationErrorType(ValidationErrorType.ELECTION_OVER_DATE);
        when(electionService.validateIfElectionIsOpen(any(ElectionValidationRequest.class))).thenReturn(
            validationError);
        assertTrue(rule.execute(vote).getValidationErrorType().equals(ValidationErrorType.ELECTION_OVER_DATE));
    }

    @Test
    public void electionAfterDateTo() throws ResourceNotFoundException {
        ValidationError validationError = new ValidationError();
        validationError.setValidationErrorType(ValidationErrorType.ELECTION_OVER_DATE);
        when(electionService.validateIfElectionIsOpen(any(ElectionValidationRequest.class))).thenReturn(
            validationError);
        assertTrue(rule.execute(vote).getValidationErrorType().equals(ValidationErrorType.ELECTION_OVER_DATE));
    }

}
