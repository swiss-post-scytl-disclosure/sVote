/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.health;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;
import java.util.Objects;

/**
 * This class groups all results into a global 'status'.
 */
public class HealthCheckStatus {

    private Map<HealthCheckValidationType, HealthCheck.HealthCheckResult> results;

    private HealthCheckStatus() {
        /* needed for jackson */}

    public HealthCheckStatus(final Map<HealthCheckValidationType, HealthCheck.HealthCheckResult> results) {
        Objects.requireNonNull(results);
        this.results = results;
    }

    @JsonIgnore
    public boolean isHealthy() {
        // status is healthy if ALL checks are 'ok'
        return results.values().stream().allMatch(HealthCheck.HealthCheckResult::getHealthy);
    }

    public Map<HealthCheckValidationType, HealthCheck.HealthCheckResult> getResults() {
        return results;
    }
}
