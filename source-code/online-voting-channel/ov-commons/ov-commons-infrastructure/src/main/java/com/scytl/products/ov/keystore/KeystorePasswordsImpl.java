/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import java.io.IOException;
import java.util.Properties;

/**
 * @author jbarrio
 * @date 16/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
class KeystorePasswordsImpl extends AbstractKeystorePasswords {

	private KeystorePasswordsImpl(final Properties properties) throws IOException {
		super( properties );
	}

	static KeystorePasswordsImpl getInstance(final Properties properties) throws IOException {
		return new KeystorePasswordsImpl( properties );
	}

	@Override
	protected KeystorePassword getKeystorePasswordInstance(final String value) {
		return new KeystorePasswordImpl( value );
	}

}
