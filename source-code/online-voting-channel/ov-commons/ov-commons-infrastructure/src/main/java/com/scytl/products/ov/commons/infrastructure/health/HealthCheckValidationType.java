/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.health;

/**
 * Enum class with the names of the different validations that can be performed in a call to a check health endpoint
 */
public enum HealthCheckValidationType {

    /**
     * It will check if the connection to the database is working
     */
    DATABASE("database"),
    /**
     * Ot will check if the logging has been initialized in the service
     */
    LOGGING_INITIALIZED("logging.init_state"),

    STATUS("status");

    private String validationName;

    HealthCheckValidationType(final String validationName) {
        this.validationName = validationName;
    }

    /**
     * Gets the validationName.
     *
     * @return Value of validationName.
     */
    public String getValidationName() {
        return validationName;
    }
}
