/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.remote.client;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import com.scytl.products.ov.commons.beans.domain.model.certificate.CertificateEntity;

public interface CertificateServiceClient {

    /** The Constant PARAMETER_ADMIN_BOARD_ID. */
    String PARAMETER_ADMIN_BOARD_ID = "adminBoardId";

    /** The Constant TENANT_ID */
    String PARAMETER_TENANT_ID = "tenantId";

    /**
     * The constant PARAMETER_PATH_CERTIFICATE_DATA
     */
    String PARAMETER_PATH_CERTIFICATE_DATA = "pathCertificateData";

    /**
     * The constant PARAMETER_PATH_TENANT_DATA_DATA
     */
    String PARAMETER_PATH_TENANT_DATA = "pathTenantData";

    @GET("{pathCertificateData}/name/{adminBoardId}")
    Call<CertificateEntity> getAdminCertificate(
            @Path(value = PARAMETER_PATH_CERTIFICATE_DATA, encoded = true) String pathCertificateData,
            @Path(PARAMETER_ADMIN_BOARD_ID) String adminBoardId);

    @GET("{pathTenantData}/tenant/{tenantId}")
    Call<CertificateEntity> getTenantCACertificate(
            @Path(value = PARAMETER_PATH_TENANT_DATA, encoded = true) String pathTenantData,
            @Path(PARAMETER_TENANT_ID) String tenantId);

}
