/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.remote.service;

import com.scytl.products.ov.commons.beans.domain.model.certificate.CertificateEntity;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;

/**
 * Service for retrieving certificates that are not stored in a different deployment
 *  (meaning that they are accessible only by calling another microservice).
 */
public interface RemoteCertificateService {

    /**
     * Gets the public certificate of an admin board
     * 
     * @param id
     *            identifier of the admin board whose certificate is being searched
     * @return the admin board Certificate
     * @throws RetrofitException 
     */
    CertificateEntity getAdminBoardCertificate(String id) throws RetrofitException;

    /**
     * Gets the tenant CA certificate
     * @param tenantId - tenant
     * @return
     * @throws RetrofitException 
     */
    CertificateEntity getTenantCACertificate(String tenantId) throws RetrofitException;

}
