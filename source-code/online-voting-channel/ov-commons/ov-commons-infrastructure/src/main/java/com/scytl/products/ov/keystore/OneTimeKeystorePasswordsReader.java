/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import com.scytl.products.ov.commons.infrastructure.remote.client.NodeIdentifier;

import java.io.IOException;
import java.util.Properties;

/**
 * @author jbarrio
 * @date 15/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
class OneTimeKeystorePasswordsReader extends KeystorePasswordsReaderImpl {

	protected OneTimeKeystorePasswordsReader(final KeystoreResources keystoreResources, final NodeIdentifier nodeIdentifier) {
		super( keystoreResources, nodeIdentifier );
	}

	static OneTimeKeystorePasswordsReader getInstance(final KeystoreResources keystoreResources, final NodeIdentifier nodeIdentifier) {
		return new OneTimeKeystorePasswordsReader( keystoreResources, nodeIdentifier );
	}

	@Override
	protected KeystorePasswords getKeystorePasswordsInstance(final Properties passwords) throws IOException {
		return OneTimeKeystorePasswords.getInstance( passwords );
	}

	/**
	 * If first-time read, the passwords will be read and the object with self-destroy immediately afterwards,
	 * together with its underlying resource. All subsequent read attempts will raise an IllegalStateException.
	 *
	 * @return Passwords for the node this object was created for, if found; null if not found.
	 * @throws IOException When reading the passwords, or if the underlying resource could not be destroyed afterwards.
	 * @throws IllegalAccessException If this object has already been destroyed.
	 */
	@Override
	public KeystorePasswords read() {
		final KeystorePasswords keystorePasswords = super.read();
		this.destroy();
		return keystorePasswords;
	}

}
