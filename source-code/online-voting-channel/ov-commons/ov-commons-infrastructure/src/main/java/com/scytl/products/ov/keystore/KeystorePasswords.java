/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import javax.security.auth.Destroyable;

/**
 * @author jbarrio
 * @date 16/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public interface KeystorePasswords extends Destroyable {

	@Override
	void destroy();

}
