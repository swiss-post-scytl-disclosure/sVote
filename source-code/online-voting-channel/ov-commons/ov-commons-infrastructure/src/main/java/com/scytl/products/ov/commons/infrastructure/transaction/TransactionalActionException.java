/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.transaction;

/**
 * Exception for wrapping application exception thrown by some transactional
 * action.
 */
public final class TransactionalActionException extends Exception {
    private static final long serialVersionUID = -3388579683095867784L;

    /**
     * Constructor.
     *
     * @param cause
     */
    public TransactionalActionException(final Throwable cause) {
        super(cause);
    }	
}
