/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.exception;

public class InfrastructureConfigException extends RuntimeException {

	private static final long serialVersionUID = 3732794419588175489L;

	public InfrastructureConfigException(Throwable cause) {
        super(cause);
    }

    public InfrastructureConfigException(String message) {
        super(message);
    }
    
    public InfrastructureConfigException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
}
