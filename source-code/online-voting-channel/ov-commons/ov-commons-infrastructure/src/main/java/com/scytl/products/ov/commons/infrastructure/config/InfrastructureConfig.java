/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Singleton class to deal with module configuration properties.
 */
public class InfrastructureConfig {

    private static final String PROPERTIES_RESOURCE = "/ov-commons-infrastructure.properties";

    private static final Logger LOG = LoggerFactory.getLogger(InfrastructureConfig.class);
    
    private static final InfrastructureConfig INSTANCE = new InfrastructureConfig();

    private final String systemReadTimeOut;

    private final String systemWriteTimeOut;

    private final String systemConnectionTimeOut;

    private final String restKeystoreFilepath;

    private final String restKeystorePassword;

    private final String restKeytrustAlgorithm;

    private final String restSslProtocol;

    private final String restKeystoreType;

    private final String filterCertificatesPath;

    private final String filterCertificatesPrefix;

    private final String filterCertificatesPathFromHome;

    private InfrastructureConfig() {
        systemReadTimeOut = System.getProperty("read.time.out");
        systemWriteTimeOut = System.getProperty("write.time.out");
        systemConnectionTimeOut = System.getProperty("connection.time.out");
		Properties properties = readProperties();
		restKeystoreFilepath = properties.getProperty("rest.keystore.filepath");
		restKeystorePassword = properties.getProperty("rest.keystore.password");
		restKeytrustAlgorithm = properties.getProperty("rest.keyTrustAlgorithm");
		restSslProtocol = properties.getProperty("rest.sslProtocol");
		restKeystoreType = properties.getProperty("rest.keyStoreType");
		filterCertificatesPath = properties.getProperty("filter.certificates.path");
		filterCertificatesPrefix = properties.getProperty("filter.certificates.prefix");
		filterCertificatesPathFromHome = properties.getProperty("filter.certificates.path.from.home");
    }
    
    public static InfrastructureConfig getInstance() {
        return INSTANCE;
    }

    private static Properties readProperties() {
        Properties properties = new Properties();
        try (InputStream stream = InfrastructureConfig.class.getResourceAsStream(PROPERTIES_RESOURCE)) {
            if (stream != null) {
                properties.load(stream);
            } else {
                LOG.warn("Sorry, unable to find " + PROPERTIES_RESOURCE);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read properties.", e);
        }
        return properties;
    }

    public String getRestKeystoreFilepath() {
        return restKeystoreFilepath;
    }

    public String getRestKeystorePassword() {
        return restKeystorePassword;
    }

    public String getRestKeytrustAlgorithm() {
        return restKeytrustAlgorithm;
    }

    public String getRestSslProtocol() {
        return restSslProtocol;
    }

    public String getRestKeystoreType() {
        return restKeystoreType;
    }

	public boolean isSslActivated() {
        return restKeystoreFilepath != null
            && !restKeystoreFilepath.isEmpty()
            && restKeystorePassword != null
            && !restKeystorePassword.isEmpty();
	}

    public long getSystemReadTimeOut() {
        return systemReadTimeOut != null
                ? Long.parseLong(systemReadTimeOut)
                : 60L;
    }

    public long getSystemWriteTimeOut() {
        return systemWriteTimeOut != null
                ? Long.parseLong(systemWriteTimeOut)
                : 60L;
    }

    public long getSystemConnectionTimeOut() {
        return systemConnectionTimeOut != null
                ? Long.parseLong(systemConnectionTimeOut)
                : 60L;
    }

    public String getFilterCertificatesPath() {
        return filterCertificatesPath;
    }

    public String getFilterCertificatesPrefix() {
        return filterCertificatesPrefix;
    }

    public boolean isFilterCertificatesPathFromHome() {
        return filterCertificatesPathFromHome != null
            && "y".equalsIgnoreCase(filterCertificatesPathFromHome.trim());
    }
}
