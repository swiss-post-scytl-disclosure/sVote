/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.csv;

import java.nio.charset.Charset;

/**
 * Constants to be used into Csv readers and writers
 */
public final class CSVConstants {

    public static final char SEMICOLON_SEPARATOR = ';';

    public static final char COMMA_SEPARATOR = ',';

    public static final char PIPE_SEPARATOR = '|';

    public static final char DEFAULT_SEPARATOR = PIPE_SEPARATOR;

    public static final char DEFAULT_QUOTE = '"';

    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    public static final char NO_QUOTE_CHARACTER = '\u0000';

    public static final char NO_ESCAPE_CHARACTER = '\u0000';

    private CSVConstants() {
    }
}
