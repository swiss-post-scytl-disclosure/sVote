/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.transaction;

/**
 * Rollback policy defines which exceptions cause transaction rollback.
 */
public interface RollbackPolicy {
    /**
     * Returns whether the policy implies rollback in case of the specified
     * exception.
     *
     * @param e
     *            the exception
     * @return the policy implies rollback.
     */
    boolean impliesRollback(Exception e);
}
