/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.transaction;

/**
 * Transactional action.
 * <p>
 * <b>WARNING:</b> Implementations MUST NEITHER pass the provided instance of
 * {@link TransactionContext} to other transaction aware components NOR use it
 * in different threads.
 */
public interface TransactionalAction<T> {
    /**
     * Executes the action in the specified transaction context.
     *
     * @param context
     *            the context
     * @return the action result
     * @throws Exception
     *             the execution failed.
     */
    T execute(TransactionContext context) throws TransactionalActionException;
}
