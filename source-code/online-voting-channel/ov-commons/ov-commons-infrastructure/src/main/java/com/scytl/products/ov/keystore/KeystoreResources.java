/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import java.io.InputStream;
import java.net.URL;

/**
 * @author jbarrio
 * @date 13/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
interface KeystoreResources {

	String             getResourcePath(     String resource     );
	URL                getResourceURL(      String resourcePath );
	InputStream        getResourceAsStream( String resourcePath );

	KeystoreProperties getKeystoreProperties();

}
