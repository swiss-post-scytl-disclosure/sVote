/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.exception;

public class OvCommonsInfrastructureException extends Exception {

    private static final long serialVersionUID = -8088251323669004674L;

    public OvCommonsInfrastructureException(String message, Throwable cause) {
        super(message, cause);
    }
}
