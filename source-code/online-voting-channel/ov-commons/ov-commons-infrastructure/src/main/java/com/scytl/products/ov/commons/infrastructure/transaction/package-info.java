/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * This package provides a transaction controller to define the transaction
 * boundaries at runtime.
 */
package com.scytl.products.ov.commons.infrastructure.transaction;
