/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import java.io.IOException;
import java.util.Properties;

/**
 * @author jbarrio
 * @date 16/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
class OneTimeKeystorePasswords extends AbstractKeystorePasswords {

	protected OneTimeKeystorePasswords(final Properties properties) throws IOException {
		super( properties );
	}
	
	static OneTimeKeystorePasswords getInstance(final Properties properties) throws IOException {
		return new OneTimeKeystorePasswords( properties );
	}

	@Override
	protected KeystorePassword getKeystorePasswordInstance(final String value) {
		return new OneTimeKeystorePassword( value );
	}

}
