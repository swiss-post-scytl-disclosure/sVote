/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.remote.client;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class RetrofitConsumer {

	/**
	 * Non-public constructor
	 */
	private RetrofitConsumer() {
		
	}
	
	public static <T> Response<T> executeCall(Call<T> call) throws RetrofitException {
		Response<T> execute;
		try {
			execute = call.execute();
		} catch (IOException e) {
			throw new RetrofitException(404, e.getMessage(), e);
		}
		if (!execute.isSuccessful()) {
			throw new RetrofitException(execute.code(), execute.errorBody());
		}
		return execute;
	}	
	
	public static <T> T processResponse(Call<T> call) throws RetrofitException {
		Response<T> execute = executeCall(call);
		return execute.body();
	}
}
