/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import javax.security.auth.Destroyable;

/**
 * @author jbarrio
 * @date 30/11/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public interface KeystorePasswordsReader extends Destroyable {

	/**
	 * Reads the passwords associated with the node this object was created for. Implementations may or may not
	 * also destroy the object in the process (and possibly its underlying resource).
	 *
	 * @return Passwords for the node this object was created for, if found; null if not found.
	 */
	KeystorePasswords read();

	/**
	 * Destroys the object, together with its underlying resource if possible.
	 */
	@Override
	void destroy();

}
