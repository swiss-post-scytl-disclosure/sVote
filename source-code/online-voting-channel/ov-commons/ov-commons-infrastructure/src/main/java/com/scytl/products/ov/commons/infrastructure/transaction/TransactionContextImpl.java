/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.transaction;

import javax.ejb.EJBContext;

/**
 * Implementation of {@link TransactionContext}.
 */
class TransactionContextImpl implements TransactionContext {
    private final EJBContext context;

    private final boolean transactional;

    /**
     * Constructor.
     *
     * @param context
     * @param transactional
     */
    public TransactionContextImpl(final EJBContext context,
            final boolean transactional) {
        this.context = context;
        this.transactional = transactional;
    }

    @Override
    public boolean isRollbackOnly() {
        checkTransactional();
        return context.getRollbackOnly();
    }

    @Override
    public boolean isTransactional() {
        return transactional;
    }

    @Override
    public void setRollbackOnly() {
        checkTransactional();
        context.setRollbackOnly();
    }

    private void checkTransactional() throws IllegalStateException {
        if (!isTransactional()) {
            throw new IllegalStateException(
                "Context is not transactional.");
        }
    }
}
