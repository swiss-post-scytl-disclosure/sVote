/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

/**
 * @author jbarrio
 * @date 14/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public class KeystoreReaderFactory {

	public KeystoreReader getInstance() {
		return KeystoreReaderImpl.getInstance();
	}
}
