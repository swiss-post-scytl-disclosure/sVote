/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.exception;

public class RestClientException extends RuntimeException {

	private static final long serialVersionUID = 5798774659744716331L;

	public RestClientException(Throwable cause) {
        super(cause);
    }

    public RestClientException(String cause) {
        super(cause);
    }
    
    public RestClientException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
