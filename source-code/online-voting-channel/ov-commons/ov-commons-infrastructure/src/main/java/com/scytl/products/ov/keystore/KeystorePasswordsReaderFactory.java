/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import com.scytl.products.ov.commons.infrastructure.remote.client.NodeIdentifier;

/**
 * @author jbarrio
 * @date 14/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public class KeystorePasswordsReaderFactory {

	public KeystorePasswordsReader getInstance(final NodeIdentifier nodeIdentifier)
	{
		final KeystoreResources keystoreResources = KeystoreResourcesImpl.getInstance();
		if (keystoreResources.getKeystoreProperties().isOneTimePasswordsFile()) {
			return OneTimeKeystorePasswordsReader.getInstance( keystoreResources, nodeIdentifier );
		} else {
			return KeystorePasswordsReaderImpl.getInstance( keystoreResources, nodeIdentifier );
		}
	}

}
