/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import com.scytl.products.ov.keystore.hook.KeystoreLocation;

/**
 * @author jbarrio
 * @date 13/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */

class KeystoreResourcesImpl extends AbstractKeystoreResources {

	private static final String DEFAULT_RESOURCES_PATH = "/";
	private static final String DEFAULT_KEYSTORE_PROPERTIES_PATH = "keystore.properties";

	private KeystoreResourcesImpl(final String resourcesPath, final String keystorePropertiesPath) {
		super( KeystoreLocation.class, resourcesPath, keystorePropertiesPath );
	}

	static KeystoreResourcesImpl getInstance() {
		return new KeystoreResourcesImpl( DEFAULT_RESOURCES_PATH, DEFAULT_KEYSTORE_PROPERTIES_PATH );
	}

}
