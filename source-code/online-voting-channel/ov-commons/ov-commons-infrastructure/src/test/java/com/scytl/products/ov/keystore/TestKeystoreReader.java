/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

/**
 * @author jbarrio
 * @date 13/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public class TestKeystoreReader extends AbstractKeystoreReader {

	private TestKeystoreReader() {
		super( TestKeystoreResources.getInstance() );
	}

	public static TestKeystoreReader getInstance() {
		return new TestKeystoreReader();
	}

}
