/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * @author jbarrio
 * @date 28/11/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
abstract class SecurityUtils {

	private SecurityUtils() {}

	static String privateKeyToString(final PrivateKey privateKey) {
		final RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;
		return ("Modulus: "+ rsaPrivateKey.getModulus() + "\nExponent: " + rsaPrivateKey.getPrivateExponent() + "\n");
	}

	static String certificateToString(final Certificate certificate) {
		final RSAPublicKey rsaPublicKey = (RSAPublicKey) certificate.getPublicKey();
		return ("Modulus: "+ rsaPublicKey.getModulus() + "\nExponent: " + rsaPublicKey.getPublicExponent() + "\n");
	}

}
