/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.health;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HealthCheckTest {

    private HealthCheck sut = spy(HealthCheck.class);

    @Test
    public void whenCheckThrowsResultShouldBeUnhealthy() {

        //given
        doThrow(new RuntimeException("exception")).when(sut).check();

        //when
        HealthCheck.HealthCheckResult result = sut.execute();

        //then
        Assert.assertFalse(result.getHealthy());
    }

    @Test
    public void executeShouldReturnSameResultAsCheck() {
        HealthCheck.HealthCheckResult mockResult = mock(HealthCheck.HealthCheckResult.class);
        when(mockResult.getHealthy()).thenReturn(false);

        //given
        when(sut.check()).thenReturn(mockResult);

        //when
        HealthCheck.HealthCheckResult result = sut.execute();

        //then
        Assert.assertEquals(mockResult.getHealthy(), result.getHealthy());

    }
}
