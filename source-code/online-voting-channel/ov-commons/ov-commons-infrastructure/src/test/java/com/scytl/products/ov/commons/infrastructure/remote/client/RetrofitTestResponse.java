/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.remote.client; /**
 * @author rmarquez
 * @date 29/06/16 17:19
 * Copyright (C) 2016
 * All rights reserved.
 */

import javax.validation.constraints.NotNull;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;


public interface RetrofitTestResponse {

    @GET(".")
    Call<ResponseBody> getResponse();

    @GET("withQueryParams/{query_param_1}/{query_param_2}")
    Call<ResponseBody> getResponseWithQueryParams(@Path("query_param_1") String queryParam1,
            @Path("query_param_2") String queryParam2);

    @POST("postString")
    Call<ResponseBody> postString(@NotNull @Body RequestBody body);

    @POST("testPojo")
    Call<ResponseBody> postPojo(@NotNull @Body TestPojo testPojo);

}
