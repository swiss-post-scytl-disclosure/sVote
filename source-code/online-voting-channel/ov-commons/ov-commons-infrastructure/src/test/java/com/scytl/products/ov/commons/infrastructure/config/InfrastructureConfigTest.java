/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
* @author rmarquez
* @date 13/07/16 11:01
* Copyright (C) 2016
* All rights reserved.
*/
package com.scytl.products.ov.commons.infrastructure.config;

import java.security.Security;
import java.util.Properties;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.products.ov.commons.infrastructure.exception.OvCommonsInfrastructureException;

public class InfrastructureConfigTest {

    @BeforeClass
    public static void runOnceSetup() {
        Security.addProvider( new BouncyCastleProvider() );
    }

    @Test
    public void test() throws OvCommonsInfrastructureException {
        InfrastructureConfig infrastructureConfig = InfrastructureConfig.getInstance();

        Assert.assertNotNull(infrastructureConfig.getRestKeystoreFilepath());
        Assert.assertNotNull(infrastructureConfig.getRestSslProtocol());

        Assert.assertTrue(infrastructureConfig.isSslActivated());

        // Default value set as 60 seconds.
        Assert.assertEquals(60L, infrastructureConfig.getSystemConnectionTimeOut());

    }

    @Test
    public void testSystem() {

        setSystemProperties();

        Properties properties = System.getProperties();

        if (properties.isEmpty()) {
            Assert.fail();
        }

    }

    private void setSystemProperties() {
        System.setProperty("myUsername", "testUser");
        System.setProperty("myPassword", "testPassword");
    }
}
