/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import com.scytl.products.ov.keystore.hook.test.TestKeystoreLocation;

/**
 * @author jbarrio
 * @date 13/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
class TestKeystoreResources extends AbstractKeystoreResources {

	private static final String TEST_RESOURCES_PATH = "/";
	private static final String TEST_KEYSTORE_PROPERTIES_PATH = "test-keystore.properties";

	private TestKeystoreResources(final String resourcesPath, final String keystorePropertiesPath) {
		super( TestKeystoreLocation.class, resourcesPath, keystorePropertiesPath );
	}

	static TestKeystoreResources getInstance() {
		return new TestKeystoreResources( TEST_RESOURCES_PATH, TEST_KEYSTORE_PROPERTIES_PATH );
	}

}
