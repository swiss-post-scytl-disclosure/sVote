/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.keystore;

import com.scytl.products.ov.commons.infrastructure.remote.client.NodeIdentifier;

/**
 * @author jbarrio
 * @date 13/12/16
 *
 * Copyright (C) 2016 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 */
public class TestKeystorePasswordsReader extends AbstractKeystorePasswordsReader implements KeystorePasswordsReader {

	private static final String TEST_FILENAME_PREFIX = "test-keystore-passwords";

	private TestKeystorePasswordsReader(final NodeIdentifier nodeIdentifier) {
		super( TestKeystoreResources.getInstance(), TEST_FILENAME_PREFIX, nodeIdentifier );
	}

	public static TestKeystorePasswordsReader getInstance(final NodeIdentifier nodeIdentifier) {
		return new TestKeystorePasswordsReader( nodeIdentifier );
	}

	/**
	 * Only the object is destroyed. The underlying resource will not be physically destroyed.
	 */
	@Override
	public void destroy() {
		if (this.destroyed) {
			final String errorMsg = "Object already destroyed";
			LOG.debug( errorMsg );
		} else {
			this.destroyed = true;
		}
	}

}
