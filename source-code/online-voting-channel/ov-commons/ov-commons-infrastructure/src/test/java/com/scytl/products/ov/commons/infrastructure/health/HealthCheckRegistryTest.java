/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.infrastructure.health;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HealthCheckRegistryTest {

    private HealthCheckRegistry sut = new HealthCheckRegistry();

    @Test
    public void whenRegisterHealthCheckThenItShouldRunHealthCheck() {

        //given
        HealthCheck mockHealthCheck = mock(HealthCheck.class);


        //when
        sut.register(HealthCheckValidationType.DATABASE, mockHealthCheck);
        sut.runAllChecks();

        //then
        verify(mockHealthCheck, times(1)).execute();
    }

    @Test
    public void whenUnregisterHealthCheckThenItShouldNotRunHealthCheck() {
        //given
        HealthCheck mockHealthCheck = mock(HealthCheck.class);

        sut.register(HealthCheckValidationType.LOGGING_INITIALIZED, mockHealthCheck);
        //when

        sut.unregister(HealthCheckValidationType.LOGGING_INITIALIZED);
        sut.runAllChecks();

        //then
        verify(mockHealthCheck, times(0)).execute();

    }

    @Test
    public void testRunOnlySomeChecks(){

        //given
        HealthCheck mockHealthCheck = mock(HealthCheck.class);

        sut.register(HealthCheckValidationType.LOGGING_INITIALIZED, mockHealthCheck);
        sut.register(HealthCheckValidationType.DATABASE, mockHealthCheck);

        sut.runChecksDifferentFrom(Arrays.asList(HealthCheckValidationType.LOGGING_INITIALIZED));

        //then
        verify(mockHealthCheck, times(1)).execute();

    }

}
