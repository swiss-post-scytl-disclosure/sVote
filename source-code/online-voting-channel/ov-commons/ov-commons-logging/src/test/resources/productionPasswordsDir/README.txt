This directory "productionPasswordsDir" represents the directory in which the logging
keystore passwords will exist in, on the production environment. When the application
starts up, it deletes the files from this directory once they have been used, therefore
there must exist some process that places files in this directory before attempting to
start up the system.