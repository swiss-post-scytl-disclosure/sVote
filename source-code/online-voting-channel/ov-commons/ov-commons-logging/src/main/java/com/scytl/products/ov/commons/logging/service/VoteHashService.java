/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.service;

import java.util.Base64;

import javax.inject.Inject;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.crypto.InputDataFormatterService;

/**
 * Computes the hash of a vote.
 */
public class VoteHashService {

    @Inject
    private InputDataFormatterService inputDataFormatterService;

    /**
     * @see HashService#hash(Object)
     */
    public String hash(Vote vote) throws GeneralCryptoLibException {
        // this is for avoid concurrency problems due to this operation is not
        // thread safe.
        PrimitivesServiceAPI primitivesService = new PrimitivesService();

        byte[] hashBytes =
            primitivesService.getHash(inputDataFormatterService.concatenate(vote.getFieldsAsStringArray()));
        return Base64.getEncoder().encodeToString(hashBytes);
    }
}
