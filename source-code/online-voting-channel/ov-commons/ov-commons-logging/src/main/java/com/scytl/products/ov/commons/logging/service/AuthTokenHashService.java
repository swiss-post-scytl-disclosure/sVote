/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.service;

import javax.inject.Inject;
import javax.json.JsonObject;

import org.apache.commons.codec.binary.Base64;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.products.ov.commons.crypto.InputDataFormatterService;

/**
 * Computes the hash of the authentication token.
 */
public class AuthTokenHashService {

    private static final String SIGNATURE = "signature";

    private static final String TIMESTAMP = "timestamp";

    private static final String VOTING_CARD_SET_ID = "votingCardSetId";

    private static final String VERIFICATION_CARD_SET_ID = "verificationCardSetId";

    private static final String BALLOT_BOX_ID = "ballotBoxId";

    private static final String VERIFICATION_CARD_ID = "verificationCardId";

    private static final String CREDENTIAL_ID = "credentialId";

    private static final String BALLOT_ID = "ballotId";

    private static final String VOTING_CARD_ID = "votingCardId";

    private static final String ELECTION_EVENT_ID = "electionEventId";

    private static final String TENANT_ID = "tenantId";

    private static final String VOTER_INFORMATION = "voterInformation";

    private static final String ID = "id";

    @Inject
    private InputDataFormatterService inputDataFormatterService;

    public String hash(JsonObject authTokenJsonObject) throws GeneralCryptoLibException {
        // this is for avoid concurrency problems due to this operation is not
        // thread safe.
        PrimitivesServiceAPI primitivesService = new PrimitivesService();

        // auth token fields
        String id = authTokenJsonObject.getString(ID);

        JsonObject voterInformation = authTokenJsonObject.getJsonObject(VOTER_INFORMATION);
        String tenantId = voterInformation.getString(TENANT_ID);
        String electionEventId = voterInformation.getString(ELECTION_EVENT_ID);
        String votingCardId = voterInformation.getString(VOTING_CARD_ID);
        String ballotId = voterInformation.getString(BALLOT_ID);
        String credentialId = voterInformation.getString(CREDENTIAL_ID);
        String verificationCardId = voterInformation.getString(VERIFICATION_CARD_ID);
        String ballotBoxId = voterInformation.getString(BALLOT_BOX_ID);
        String verificationCardSetId = voterInformation.getString(VERIFICATION_CARD_SET_ID);
        String votingCardSetId = voterInformation.getString(VOTING_CARD_SET_ID);

        String timestamp = authTokenJsonObject.getString(TIMESTAMP);
        String signature = authTokenJsonObject.getString(SIGNATURE);

        // hash of concatenation of all auth token fields
		return Base64.encodeBase64String(primitivesService.getHash(inputDataFormatterService.concatenate(id, tenantId,
				electionEventId, votingCardId, ballotId, credentialId, verificationCardId, ballotBoxId,
				verificationCardSetId, votingCardSetId, timestamp, signature)));
    }
}
