/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.producers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;

/**
 * Producer for transaction info provider.
 */
public class TransactionInfoProviderProducer {

	/**
	 * Produces a new instance of transaction info provider.
	 * 
	 * @return a transaction info provider.
	 */
	@Produces
	@ApplicationScoped
	public TransactionInfoProvider produceTransactionInfoProvider() {
		return new TransactionInfoProvider();
	}

}
