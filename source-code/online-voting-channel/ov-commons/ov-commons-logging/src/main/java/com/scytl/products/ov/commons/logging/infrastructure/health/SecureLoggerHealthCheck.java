/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.infrastructure.health;


import com.scytl.products.ov.commons.beans.domain.model.platform.LoggingInitializationState;
import com.scytl.products.ov.commons.infrastructure.health.HealthCheck;

/**
 * Class that checks if SecureLogger is working. For now only checks if it was initialized.
 */
public class SecureLoggerHealthCheck extends HealthCheck {

    private final LoggingInitializationState initializationState;

    public SecureLoggerHealthCheck(final LoggingInitializationState initializationState) {
        this.initializationState = initializationState;
    }

    @Override
    protected HealthCheckResult check() {
        if (initializationState.getInitialized()) {
            return HealthCheckResult.healthy();
        } else {
            return HealthCheckResult.unhealthy("Secure Logging not initialized");
        }
    }
}
