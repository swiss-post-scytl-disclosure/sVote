/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.config;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import com.scytl.slogger.DeferredSecureAppender;

/**
 * Base class implementing the configure method to configure secure loggers.
 */
public class BaseLoggerConfig implements LoggerConfig {

    @Override
    public void configure(String loggerName,
            PrivateKey signaturePrivateKey, PrivateKey cipherPrivateKey,
            PublicKey cipherPublicKey) {
        Logger logger = Logger.getLogger(loggerName);
        Enumeration<?> appenders = logger.getAllAppenders();
        while (appenders.hasMoreElements()) {
            Object element = appenders.nextElement();
            if (element instanceof DeferredSecureAppender) {
                DeferredSecureAppender appender =
                    (DeferredSecureAppender) element;
                appender.activateOptions(signaturePrivateKey,
                    cipherPrivateKey, cipherPublicKey);
            }
        }
    }
}
