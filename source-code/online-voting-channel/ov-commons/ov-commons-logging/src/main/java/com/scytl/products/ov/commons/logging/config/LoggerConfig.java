/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.config;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 */
public interface LoggerConfig {

    /**
     * Configures the specified secure logger by activating it's appender with
     * the supplied cryptographic keys.
     * 
     * @param loggerName
     *            the logger name
     * @param signaturePrivateKey
     *            the signature private key
     * @param cipherPrivateKey
     *            the cipher private key
     * @param cipherPublicKey
     *            the cipher public key.
     */
    void configure(String loggerName, PrivateKey signaturePrivateKey,
            PrivateKey cipherPrivateKey, PublicKey cipherPublicKey);
}
