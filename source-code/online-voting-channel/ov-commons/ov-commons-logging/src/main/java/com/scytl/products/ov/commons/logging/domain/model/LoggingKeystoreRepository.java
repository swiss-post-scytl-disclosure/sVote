/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.domain.model;

import javax.ejb.Local;

import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for handling the logging keystores in each context
 */
@Local
public interface LoggingKeystoreRepository extends BaseRepository<LoggingKeystoreEntity, Long> {

    /**
     * Checks if a keystore for a given key type exists.
     *
     * @param keyType
     *            type of the keystore
     * @return true if the data exists, false otherwise
     */
    boolean checkIfKeystoreExists(String keyType);

    /**
     * Returns a keystore for a given key type.
     *
     * @param keyType
     *            type of the keystore
     * @return the logging keystore
     */
    LoggingKeystoreEntity getLoggingKeystoreByType(String keyType);
}
