/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.producers;


import com.scytl.products.oscore.logging.api.factory.LoggingFactory;
import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.LoggingWriter;
import com.scytl.products.oscore.logging.core.factory.LoggingFactoryLog4j;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * Logger producer.
 */
public class SplunkableLoggerProducer {

    /**
     * Splunk logger name.
     */
    public static final String LOGGER_NAME_SPLUNKABLE = "splunkable";


    private LoggingWriter loggingWriter;


    @Inject
    private MessageFormatter messageFormatter;
    /**
     * Instantiates a splunkable logger.
     *
     * @return a splunkable logger writer.
     */
    @Produces
    public LoggingWriter getSplunkLoggingWriterInstance() {
        final LoggingFactory loggerFactory = new LoggingFactoryLog4j(messageFormatter);
        loggingWriter = loggerFactory.getLogger(LOGGER_NAME_SPLUNKABLE);
        return loggingWriter;
    }


}