/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.producers;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.products.ov.commons.logging.config.Audit;

import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.factory.SecureLoggingFactoryLog4j;

/**
 * Logger producer.
 */
public class SecureLoggerProducer {

	/**
	 * Audit logger name.
	 */
	public static final String LOGGER_NAME_AUDIT_LOGGER = "AuditLogger";

	/**
	 * Secure logger name.
	 */
	public static final String LOGGER_NAME_SECURE_LOGGER = "SecureLogger";



	private SecureLoggingWriter secureLoggingWriter;

	private SecureLoggingWriter auditLoggingWriter;

	@Inject
	private MessageFormatter messageFormatter;



	/**
	 * Instantiates a secure logger.
	 * 
	 * @return a secure logger writer.
	 */
	@Produces
	public SecureLoggingWriter getSecureLoggingWriterInstance() {
		final SecureLoggingFactoryLog4j loggerFactory = new SecureLoggingFactoryLog4j(messageFormatter);
		secureLoggingWriter = loggerFactory.getLogger(LOGGER_NAME_SECURE_LOGGER);
		return secureLoggingWriter;
	}

	/**
	 * Instantiates an audit logger.
	 * 
	 * @return an audit logger writer.
	 */
	@Produces
	@Audit
	public SecureLoggingWriter getAuditLoggingWriterInstance() {
		final SecureLoggingFactoryLog4j loggerFactory = new SecureLoggingFactoryLog4j(messageFormatter);
		auditLoggingWriter = loggerFactory.getLogger(LOGGER_NAME_AUDIT_LOGGER);
		return auditLoggingWriter;
	}
}
