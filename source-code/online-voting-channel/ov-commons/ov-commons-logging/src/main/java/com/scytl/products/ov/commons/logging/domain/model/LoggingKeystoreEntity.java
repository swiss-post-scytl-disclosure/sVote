/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.domain.model;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  class for defining an entity representing a logging keystore
 */
@MappedSuperclass
public class LoggingKeystoreEntity {

	/**
	 * The column length 100.
	 */
	public static final int COLUMN_LENGTH_100 = 100;

	/**the platform name**/
	@Column(name = "PLATFORM_NAME")
	@NotNull
	@Size(max = COLUMN_LENGTH_100)
	private String platformName;

	/**
	 * The key type
	 */
	@Column(name = "KEY_TYPE")
	@NotNull
	@Size(max = COLUMN_LENGTH_100)
	private String keyType;

	/**
	 * The content of the keystore
	 */
	@Column(name = "KEYSTORE_CONTENT")
	@NotNull
	@Lob
	private String keystoreContent;


	/**
	 * Gets platformName.
	 *
	 * @return Value of platformName.
	 */
	public String getPlatformName() {
		return platformName;
	}

	/**
	 * Sets new platformName.
	 *
	 * @param platformName New value of platformName.
	 */
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	/**
	 * Gets keyType.
	 *
	 * @return Value of keyType.
	 */
	public String getKeyType() {
		return keyType;
	}

	/**
	 * Sets new keystoreContent.
	 *
	 * @param keystoreContent New value of keystoreContent.
	 */
	public void setKeystoreContent(String keystoreContent) {
		this.keystoreContent = keystoreContent;
	}

	/**
	 * Sets new keyType.
	 *
	 * @param keyType New value of keyType.
	 */
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}

	/**
	 * Gets keystoreContent.
	 *
	 * @return Value of keystoreContent.
	 */
	public String getKeystoreContent() {
		return keystoreContent;
	}
}
