/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging.constants;

/**
 * Constants used in loggers.
 */
public final class MessageFormatterConstants {

	/**
	 * Application.
	 */
	public static final String APPLICATION_OV = "OV";

	/**
	 * Component VMC.
	 */
	public static final String COMPONENT_VMC = "VMC";

	/**
	 * Component AC.
	 */
	public static final String COMPONENT_AC = "AC";

	/**
	 * Component VWC.
	 */
	public static final String COMPONENT_VWC = "VWC";

	/**
	 * Component EIC.
	 */
	public static final String COMPONENT_EIC = "EIC";

	/**
	 * Component VVC.
	 */
	public static final String COMPONENT_VVC = "VVC";

	/**
	 * Component VVC.
	 */
	public static final String COMPONENT_EAC = "EAC";
	
	/**
	 * Component MDC
	 */
	public static final String COMPONENT_MDC = "MDC";

	/**
	 * Component CC ORCHESTRATOR
	 */
	public static final String COMPONENT_OR = "OR";

	/**
	 * Non-public constructor
	 */
	private MessageFormatterConstants() {
	}
}
