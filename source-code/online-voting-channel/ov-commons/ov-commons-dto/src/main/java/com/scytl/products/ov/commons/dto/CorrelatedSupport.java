/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import java.util.UUID;

/**
 * Basic implementation of {@link Correlated}:
 */
public class CorrelatedSupport implements Correlated {
    private UUID correlationId;

    /**
     * Constructor.
     */
    public CorrelatedSupport() {
    }

    /**
     * Constructor.
     * 
     * @param correlationId
     *            the correlation identifier.
     */
    public CorrelatedSupport(UUID correlationId) {
        setCorrelationId(correlationId);
    }

    @Override
    public UUID getCorrelationId() {
        return correlationId;
    }

    @Override
    public void setCorrelationId(UUID correlationId) {
        this.correlationId = correlationId;
    }
}
