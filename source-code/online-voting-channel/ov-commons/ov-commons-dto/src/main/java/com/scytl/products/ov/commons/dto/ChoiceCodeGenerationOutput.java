/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

public class ChoiceCodeGenerationOutput {
    
    private String verificationCardId;
    
    private String computedBallotCastingKey;

    private String computedRepresentations;
    
    private String choiceCodesKeyCommitmentJson;

    private String ballotCastingKeyExponentCommitmentJson;

    private String encryptedBallotCastingKey;
    
    public ChoiceCodeGenerationOutput() {
        super();
    }

    public ChoiceCodeGenerationOutput(String verificationCardId, String encryptedBallotCastingKey,
            String computedBallotCastingKey, String computedRespresentations, String choiceCodesKeyCommitmentJson,
            String ballotCastingKeyCommitmentJson) {
        super();
        this.verificationCardId = verificationCardId;
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
        this.computedBallotCastingKey = computedBallotCastingKey;
        this.computedRepresentations = computedRespresentations;
        this.choiceCodesKeyCommitmentJson = choiceCodesKeyCommitmentJson;
        this.ballotCastingKeyExponentCommitmentJson = ballotCastingKeyCommitmentJson;
    }

    public String getVerificationCardId() {
        return verificationCardId;
    }

    public void setVerificationCardId(String verificationCardId) {
        this.verificationCardId = verificationCardId;
    }

    public String getComputedBallotCastingKey() {
        return computedBallotCastingKey;
    }

    public void setComputedBallotCastingKey(String computedBallotCastingKey) {
        this.computedBallotCastingKey = computedBallotCastingKey;
    }

    public String getComputedRepresentations() {
        return computedRepresentations;
    }

    public void setComputedRepresentations(String computedRepresentations) {
        this.computedRepresentations = computedRepresentations;
    }

    public String getBallotCastingKeyCommitmentJson() {
        return ballotCastingKeyExponentCommitmentJson;
    }

    public void setBallotCastingKeyCommitmentJson(String ballotCastingKeyCommitmentJson) {
        this.ballotCastingKeyExponentCommitmentJson = ballotCastingKeyCommitmentJson;
    }

    public String getChoiceCodesKeyCommitmentJson() {
        return choiceCodesKeyCommitmentJson;
    }

    public void setChoiceCodesKeyCommitmentJson(String choiceCodesKeyCommitmentJson) {
        this.choiceCodesKeyCommitmentJson = choiceCodesKeyCommitmentJson;
    }

    public String getEncryptedBallotCastingKey() {
        return encryptedBallotCastingKey;
    }

    public void setEncryptedBallotCastingKey(String encryptedBallotCastingKey) {
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
    }

}


