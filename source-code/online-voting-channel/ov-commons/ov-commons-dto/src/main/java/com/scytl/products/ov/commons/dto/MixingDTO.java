/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import java.util.Set;

import com.scytl.products.ov.commons.beans.domain.model.messaging.StreamSerializable;
import com.scytl.products.ov.commons.dto.mixing.MixingPayload;

/**
 * Data transfer object for the mixing-and-decryption process. It carries
 * payloads in and out of the mixing nodes.
 */
public interface MixingDTO extends StreamSerializable {
    /**
     * Updates the DTO with a new payload and the metadata of the control
     * component node that processed it.
     *
     * @param visitedQueue
     *            the queue the DTO was received from
     * @param visitedNode
     *            the node that processed the payload in the DTO
     * @param newPayload
     *            the processed data and the corresponding signature.
     */
    void update(String visitedQueue, String visitedNode, MixingPayload newPayload);

    /**
     * @return the error that caused the mixing and decryption of the current
     *         payload to fail.
     */
    MixingError getError();

    /**
     * @return the payload the message is carrying.
     */
    MixingPayload getPayload();

    /**
     * @return a tracking ID for this message
     */
    String getRequestId();

    /**
     * @return the queues this mixing DTO has to be sent to
     */
    Set<String> getQueuesToVisit();

    /**
     * @return the name of the queue this mixing DTO has just been through
     */
    String getLastQueueName();

    /**
     * @return the name of the node this mixing DTO has been processed by
     */
    String getLastNodeName();

    /**
     * Specifies an error caused while trying to process the DTO
     *
     * @param error
     *            the error that prevented the DTO from being processed.
     */
    void setError(Throwable error);
}
