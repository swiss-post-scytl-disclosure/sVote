/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto.mixing;

import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.products.ov.commons.beans.domain.model.messaging.Payload;
import com.scytl.products.ov.commons.beans.domain.model.vote.EncryptedVote;
import com.scytl.products.ov.commons.beans.domain.model.vote.VoteSetId;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * The payload for a mixing and decryption process.
 * 
 * NOTE: the votes are arranged in a list so that they preserve their insertion order.
 */
public interface MixingPayload extends Payload, Serializable {

    /**
     * @return the key used for re-encrypting the votes during the shuffle.
     */
    ElGamalPublicKey getVoteEncryptionKey();

    /**
     * @return a reference to the original set of votes of a cleansed ballot box
     */
    VoteSetId getVoteSetId();

    /**
     * @return the votes to be mixed / the decrypted votes after the shuffle.
     */
    List<EncryptedVote> getVotes();

    /**
     * @return the electoral authority ID
     */
    String getElectoralAuthorityId();

    /**
     * @return the Zp group of the encrypted votes
     */
    ZpSubgroup getEncryptionParameters();

    /**
     * @return the JSONs of the vote decryption proofs
     */
    List<String> getDecryptionProofs();

    /**
     * @return the votes after being shuffled
     */
    List<EncryptedVote> getShuffledVotes();

    /**
     * @return the JSON of the vote shuffling proofs
     */
    String getShuffleProof();

    /**
     * @return the votes from the output of the previous mixing process
     */
    List<EncryptedVote> getPreviousVotes();

    /**
     * @return the vote encryption key that was used in the previous mixing process
     */
    ElGamalPublicKey getPreviousVoteEncryptionKey();

    /**
     * @return a list of JSON representations of the generated commitment parameters
     */
    List<String> getCommitmentParameters();

    /**
     * @return the date this DTO was created
     */
    ZonedDateTime getTimestamp();
}
