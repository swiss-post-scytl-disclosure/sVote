/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

/**
 * 
 * Class that contains all computation results of choice codes 
 * for the vote and the ballot casting key (aka. vote cast code)
 *
 */
public class ComputeResults {
    
    private String computationResults;
    
    private String decryptionResults;
    
    public String getComputationResults() {
        return computationResults;
    }

    public void setComputationResults(String computationResults) {
        this.computationResults = computationResults;
    }

    public String getDecryptionResults() {
        return decryptionResults;
    }

    public void setDecryptionResults(String decryptionResults) {
        this.decryptionResults = decryptionResults;
    }

}


