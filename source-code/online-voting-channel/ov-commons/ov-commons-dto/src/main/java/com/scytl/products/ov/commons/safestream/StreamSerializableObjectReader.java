/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.safestream;

import com.scytl.products.ov.commons.beans.domain.model.messaging.SafeStreamDeserializationException;
import com.scytl.products.ov.commons.beans.domain.model.messaging.StreamSerializable;

public interface StreamSerializableObjectReader<T extends StreamSerializable> {

    T read(byte[] serializadObject) throws SafeStreamDeserializationException;
    
    T read(byte[] serializadObject, int offset, int length) throws SafeStreamDeserializationException;
}
