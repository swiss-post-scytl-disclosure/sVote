/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import java.util.UUID;

/**
 * Correlated entity.
 */
public interface Correlated {
    /**
     * Returns the correlation identifier.
     * 
     * @return the correlation identifier.
     */
    UUID getCorrelationId();

    /**
     * Sets the correlation identifier.
     * 
     * @param correlationId the correlation identifier.
     */
    void setCorrelationId(UUID correlationId);
}
