/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

public class ChoiceCodeAndComputeResults extends ComputeResults {
    
    /**
     * The choice codes.
     */
    private String choiceCodes;

    /**
     * Returns the current value of the field choiceCodes.
     *
     * @return Returns the choiceCodes.
     */
    public String getChoiceCodes() {
        return choiceCodes;
    }

    /**
     * Sets the value of the field choiceCodes.
     *
     * @param choiceCodes The choiceCodes to set.
     */
    public void setChoiceCodes(String choiceCodes) {
        this.choiceCodes = choiceCodes;
    }



}


