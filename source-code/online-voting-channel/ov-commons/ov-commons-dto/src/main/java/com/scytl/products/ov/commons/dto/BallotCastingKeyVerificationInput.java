/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

public class BallotCastingKeyVerificationInput {

    private String confirmationMessage;

    private String votingCardId;

    public String getConfirmationMessage() {
        return confirmationMessage;
    }

    public void setConfirmationMessage(String confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    public String getVotingCardId() {
        return votingCardId;
    }

    public void setVotingCardId(String votingCardId) {
        this.votingCardId = votingCardId;
    }

}
