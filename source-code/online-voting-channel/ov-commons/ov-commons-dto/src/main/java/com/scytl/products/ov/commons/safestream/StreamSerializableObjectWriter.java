/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.safestream;

import java.io.IOException;

import com.scytl.products.ov.commons.beans.domain.model.messaging.StreamSerializable;

public interface StreamSerializableObjectWriter {

    byte[] write(StreamSerializable streamSerializableObject) throws IOException;
    
    byte[] write(StreamSerializable streamSerializableObject, int offset) throws IOException;
}
