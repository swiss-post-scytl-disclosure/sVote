/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import java.util.List;
import java.util.Map;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalComputationsValues;

/**
 * <p>
 * Codec to encode and to decode lists and maps of
 * {@link ElGamalComputationsValues}.
 * <p>
 * Implementation must be thread safe.
 */
public interface ElGamalComputationsValuesCodec {
    /**
     * Decodes the list from a given encoding.
     * 
     * @param encoding
     *            the encoding
     * @return the list
     * @throws GeneralCryptoLibException
     *             the encoding is malformed.
     */
    List<ElGamalComputationsValues> decodeList(String encoding)
            throws GeneralCryptoLibException;

    /**
     * Decodes the map from a given encoding.
     * 
     * @param encoding
     *            the encoding
     * @return the map
     * @throws GeneralCryptoLibException
     *             the encoding is malformed.
     */
    Map<ElGamalComputationsValues, ElGamalComputationsValues> decodeMap(
            String encoding)
            throws GeneralCryptoLibException;

    /**
     * Decodes the instance from a given encoding.
     * 
     * @param encoding
     *            the encoding
     * @return the instance
     * @throws GeneralCryptoLibException
     *             the encoding is malformed.
     */
    ElGamalComputationsValues decodeSingle(String encoding)
            throws GeneralCryptoLibException;

    /**
     * Encodes a given list.
     * 
     * @param list
     *            the list
     * @return the encoding.
     */
    String encodeList(List<ElGamalComputationsValues> list);

    /**
     * Encodes a given map.
     * 
     * @param map
     *            the map
     * @return the encoding.
     */
    String encodeMap(
            Map<ElGamalComputationsValues, ElGamalComputationsValues> map);

    /**
     * Encodes a given instance
     * 
     * @param values
     *            the instance
     * @return the encoding.
     */
    String encodeSingle(ElGamalComputationsValues values);
}
