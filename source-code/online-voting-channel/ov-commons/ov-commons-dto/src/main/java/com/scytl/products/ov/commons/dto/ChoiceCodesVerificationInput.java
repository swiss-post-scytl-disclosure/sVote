/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

public class ChoiceCodesVerificationInput {

    private String vote;

    private String elGamalElectoralPublicKey;

    private String compressesedChoiceCodesEncryptionPK;

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getElGamalElectoralPublicKey() {
        return elGamalElectoralPublicKey;
    }

    public void setElGamalElectoralPublicKey(String elGamalElectoralPublicKey) {
        this.elGamalElectoralPublicKey = elGamalElectoralPublicKey;
    }

    public String getCompressesedChoiceCodesEncryptionPK() {
        return compressesedChoiceCodesEncryptionPK;
    }

    public void setCompressesedChoiceCodesEncryptionPK(String compressesedChoiceCodesEncryptionPK) {
        this.compressesedChoiceCodesEncryptionPK = compressesedChoiceCodesEncryptionPK;
    }

}
