/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;

public class VoteAndComputeResults {
    
    private Vote vote;
    
    private ComputeResults computeResults;

    public Vote getVote() {
        return vote;
    }

    public void setVote(Vote vote) {
        this.vote = vote;
    }

    public ComputeResults getComputeResults() {
        return computeResults;
    }

    public void setComputeResults(ComputeResults computeResults) {
        this.computeResults = computeResults;
    }

}


