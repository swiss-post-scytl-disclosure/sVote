/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChoiceCodeGenerationInput {

    private String verificationCardId;

    private String encryptedRepresentations;

    private String encryptedHashedBallotCastingKey;
    
    private String encryptedBallotCastingKey;

    /**
     * Creates an object used as the input for choice code generation requests.
     * 
     * @param verificationCardId
     *            the verification card identifier
     * @param encryptedBallotCastingKey
     *            an encrypted ballot casting key
     * @param encryptedRepresentations
     *            the encrypted voting option representations as a new-line
     *            separated collection of strings
     */
    @JsonCreator
    public ChoiceCodeGenerationInput(@JsonProperty("verificationCardId") String verificationCardId,
            @JsonProperty("encryptedBallotCastingKey") String encryptedBallotCastingKey,
            @JsonProperty("encryptedHashedBallotCastingKey") String encryptedHashedBallotCastingKey,
            @JsonProperty("encryptedRepresentations") String encryptedRepresentations) {
        this.verificationCardId = verificationCardId;
        this.encryptedHashedBallotCastingKey = encryptedHashedBallotCastingKey;
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
        this.encryptedRepresentations = encryptedRepresentations;
    }

    public String getEncryptedRepresentations() {
        return encryptedRepresentations;
    }

    public void setEncryptedRepresentations(String encryptedRepresentations) {
        this.encryptedRepresentations = encryptedRepresentations;
    }

    public String getVerificationCardId() {
        return verificationCardId;
    }

    public void setVerificationCardId(String verificationCardId) {
        this.verificationCardId = verificationCardId;
    }

    public String getEncryptedBallotCastingKey() {
        return encryptedBallotCastingKey;
    }

    public void setEncryptedBallotCastingKey(String encryptedBallotCastingKey) {
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
    }

    public String getEncryptedHashedBallotCastingKey() {
        return encryptedHashedBallotCastingKey;
    }

    public void setEncryptedHashedBallotCastingKey(String encryptedHashedBallotCastingKey) {
        this.encryptedHashedBallotCastingKey = encryptedHashedBallotCastingKey;
    }
}
