/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MixingErrorTest {
    @Test
    public void testConstructorAndUpdater() {
        MixingError sut = new MixingError("errorMessage", "errorClass", "stackTrace", 1);

        String anotherErrorMessage = "Another error message";
        sut.update(new RuntimeException(anotherErrorMessage));

        assertEquals(anotherErrorMessage, sut.getErrorMessage());
    }
}
