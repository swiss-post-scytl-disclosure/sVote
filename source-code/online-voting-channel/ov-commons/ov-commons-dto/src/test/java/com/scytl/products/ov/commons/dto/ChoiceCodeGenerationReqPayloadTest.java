/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scytl.products.ov.commons.beans.domain.model.messaging.PayloadSignature;

public class ChoiceCodeGenerationReqPayloadTest {

    private static final String JSON_FILE_NAME = "/choiceCodeGenerationRequestPayload.json";

    @Test
    public void deserialise() throws JsonParseException, JsonMappingException, IOException {

        ChoiceCodeGenerationReqPayload sut;
        try (InputStream is = getClass().getResourceAsStream(JSON_FILE_NAME)) {
            assertNotNull(is);
            sut = new ObjectMapper().readValue(is, ChoiceCodeGenerationReqPayload.class);
        }

        assertEquals("9b1", sut.getTenantId());
        assertEquals("989", sut.getElectionEventId());
        assertEquals("9a0", sut.getVerificationCardSetId());
        assertEquals(3, sut.getChoiceCodeGenerationInputList().size());

        ChoiceCodeGenerationInput input = sut.getChoiceCodeGenerationInputList().get(0);
        assertEquals("A", input.getEncryptedBallotCastingKey());
        assertEquals("0", input.getVerificationCardId());
        assertEquals("--", input.getEncryptedRepresentations());

        PayloadSignature signature = sut.getSignature();
        assertEquals(256, signature.getSignatureContents().length);
        assertEquals(new BigInteger("1257830645702366600860980085094321681416228141133"),
            signature.getCertificateChain()[0].getSerialNumber());
    }
}
