/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.verify;

import java.io.IOException;
import java.security.PublicKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.asymmetric.service.AsymmetricService;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.products.ov.commons.sign.beans.SignedRequestContent;

public class RequestVerifier {

    public boolean verifySignature(final SignedRequestContent signedRequestContent, final byte[] signatureBytes,
            final PublicKey publicKey) throws IOException, GeneralCryptoLibException {

        if (signatureBytes == null)
            return false;

        if (publicKey == null)
            return false;

        AsymmetricService asymmetricService = new AsymmetricService();

        byte[] objectBytes = signedRequestContent.getBytes();

        PrimitivesServiceAPI primitivesService = new PrimitivesService();
        byte[] objectHash = primitivesService.getHash(objectBytes);

        return asymmetricService.verifySignature(signatureBytes, publicKey, objectHash);

    }

}
