/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.exception;

public class OvCommonsSignException extends Exception {

    private static final long serialVersionUID = -3700474181681984568L;

    public OvCommonsSignException(String message) {
        super(message);
    }

}
