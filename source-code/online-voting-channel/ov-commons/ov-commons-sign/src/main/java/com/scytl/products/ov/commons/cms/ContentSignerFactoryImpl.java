/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.cms;

import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;

import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.operator.ContentSigner;

import com.scytl.products.ov.commons.signature.SignatureFactory;

/**
 * Implementation of {@link ContentSignerFactory}.
 */
class ContentSignerFactoryImpl implements ContentSignerFactory {
    private final SignatureFactory factory;

    private final AlgorithmIdentifier identifier;
    
    private final SecureRandom prng;

    /**
     * Constructor.
     *
     * @param factory
     * @param identifier
     */
    public ContentSignerFactoryImpl(final SignatureFactory factory,
            final AlgorithmIdentifier identifier, SecureRandom prng) {
        this.factory = factory;
        this.identifier = identifier;
        this.prng = prng;
    }

    @Override
    public ContentSigner newContentSigner(final PrivateKey key)
            throws InvalidKeyException {
        Signature signature = factory.newSignature();
        signature.initSign(key, prng);
        return new ContentSignerImpl(signature, identifier);
    }
}
