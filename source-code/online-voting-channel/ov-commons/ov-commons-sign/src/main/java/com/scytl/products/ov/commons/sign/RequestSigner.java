/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.sign;

import java.io.IOException;
import java.security.PrivateKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.asymmetric.service.AsymmetricService;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.products.ov.commons.exception.OvCommonsSignException;
import com.scytl.products.ov.commons.sign.beans.SignedRequestContent;

public class RequestSigner {

    public byte[] sign(final SignedRequestContent signedRequestContent, final PrivateKey privateKey)
            throws IOException, GeneralCryptoLibException, OvCommonsSignException {

        if (signedRequestContent == null || privateKey == null) {
            throw new OvCommonsSignException("Nothing to sign or invalid key.");
        }

        AsymmetricService asymmetricService = new AsymmetricService();

        byte[] objectBytes = signedRequestContent.getBytes();

        PrimitivesServiceAPI primitivesService = new PrimitivesService();
        byte[] objectHash = primitivesService.getHash(objectBytes);

        return asymmetricService.sign(privateKey, objectHash);
    }

}
