/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.sign.beans;

/**
 * The Class Constants.
 */
public class SignatureFieldsConstants {

    /** The Constant SIG_FIELD_TENANTID. */
    public static final String SIG_FIELD_TENANTID = "tenantId";

    /** The Constant SIG_FIELD_EEVENTID. */
    public static final String SIG_FIELD_EEVENTID = "electionEventId";

    /** The Constant SIG_FIELD_BBOXID. */
    public static final String SIG_FIELD_BBOXID = "ballotBoxId";

    /** The Constant SIG_FIELD_VERSION. */
    public static final String SIG_FIELD_VERSION = "version";

    /** The Constant SIG_FIELD_TIMESTAMP. */
    public static final String SIG_FIELD_TIMESTAMP = "timestamp";

    /** The Constant SIG_FIELD_FILENAME. */
    public static final String SIG_FIELD_FILENAME = "filename";

    /** The Constant SIG_FIELD_SIGNED. */
    public static final String SIG_FIELD_SIGNED = "signed";

    /** The Constant SIG_FIELD_SIGNATURE. */
    public static final String SIG_FIELD_SIGNATURE = "signature";

    /** The Constant SIG_FIELD_COMPONENT. */
    public static final String SIG_FIELD_COMPONENT = "component";
    
    /**
	 * Non-public constructor
	 */
	private SignatureFieldsConstants() {
	}
}
