/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.cms;

import java.io.IOException;
import java.io.OutputStream;
import java.security.Signature;
import java.security.SignatureException;

/**
 * Implementation of {@link OutputStream} stream used by
 * {@link ContentSignerImpl}.
 */
class ContentSignerOutputStream extends OutputStream {
    private final Signature signature;

    /**
     * Constructor.
     *
     * @param signature
     */
    public ContentSignerOutputStream(final Signature signature) {
        this.signature = signature;
    }

    @Override
    public void write(final int b) throws IOException {
        try {
            signature.update((byte) b);
        } catch (SignatureException e) {
            throw new IOException("Failed to update signature.", e);
        }
    }
}
