/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.verify;

import java.security.cert.X509Certificate;

import com.scytl.products.ov.commons.beans.domain.model.messaging.Payload;
import com.scytl.products.ov.commons.beans.domain.model.messaging.PayloadVerificationException;

/**
 * A service that verifies signatures of payloads.
 */
public interface PayloadVerifier {

    /**
     * Checks whether a signature is valid for the supplied public key and
     * payload.
     *
     * @param payload
     *            the payload to check the signature against
     * @param rootCertificate
     *            the CA certificate that issued the chain of certificates
     *            leading to the key used to sign the payload
     * @return whether the signature is valid or not
     * @throws PayloadVerificationException
     *             if the verification of the payload could not be performed
     */
    boolean isValid(Payload payload, X509Certificate rootCertificate) throws PayloadVerificationException;
}
