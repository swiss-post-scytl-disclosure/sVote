/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.sign;

import com.scytl.products.ov.commons.beans.domain.model.messaging.Payload;
import com.scytl.products.ov.commons.beans.domain.model.messaging.PayloadSignature;
import com.scytl.products.ov.commons.beans.domain.model.messaging.PayloadSignatureException;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/**
 * A service that signs payloads.
 */
public interface PayloadSigner {

    /**
     * Generates the signature of a set of votes, and attaches to it a
     * certificate chain with the public key for further validation.
     *
     * @param payload
     *            the payload to sign
     * @param signingKey
     *            the key used to sign
     * @param certificateChain
     *            the certificate chain containing the public key used to verify
     *            the signature
     * @return the signature for the supplied set of votes
     * @throws PayloadSignatureException
     *             if the signature generation could not be performed
     */
    PayloadSignature sign(Payload payload, PrivateKey signingKey, X509Certificate[] certificateChain)
            throws PayloadSignatureException;

}
