/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.cms;

import java.security.InvalidKeyException;
import java.security.PrivateKey;

import org.bouncycastle.operator.ContentSigner;

/**
 * Factory of {@link ContentSigner}.
 * <p>
 * Implementation must be thread-safe.
 */
interface ContentSignerFactory {
    /**
     * Creates a new {@link ContentSigner} instance for a given private key.
     *
     * @param key
     *            the key
     * @return the instance.
     * @throws InvalidKeyException
     *             the key is invalid.
     */
    ContentSigner newContentSigner(PrivateKey key)
            throws InvalidKeyException;
}
