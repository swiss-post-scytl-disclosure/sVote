/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.configuration;

import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Wraps the different {@link SignatureAlgorithm} that are supported.
 */
public enum JSONSignatureConfiguration {

	RSA_PSS_SHA256(SignatureAlgorithm.PS256);

	private SignatureAlgorithm algorithm;

	JSONSignatureConfiguration(SignatureAlgorithm algorithm) {

		this.algorithm = algorithm;
	}

	public SignatureAlgorithm getAlgorithm() {
		return algorithm;
	}
}
