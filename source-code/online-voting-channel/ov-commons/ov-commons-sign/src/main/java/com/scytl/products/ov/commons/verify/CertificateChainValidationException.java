/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.verify;

/**
 * A certificate chain could not be validated.
 */
public class CertificateChainValidationException extends Exception {

    private static final long serialVersionUID = -3382992499835661968L;
    
    public CertificateChainValidationException(Throwable cause) {
        super(cause);
    }
}
