/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * This package provides API for creating digital signatures compatible with
 * PKCS7.
 */
package com.scytl.products.ov.commons.cms;
