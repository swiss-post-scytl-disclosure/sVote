/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.signature;

import java.security.Signature;

/**
 * Factory of {@link Signature}.
 * <p>
 * This factory is introduced to make it possible working with {@link Signature}
 * API and to control the used cryptography algorithms applying the current
 * {@code cryptolibPolicy.propeties}.
 * <p>
 * Implementation must be thread-safe.
 */
public interface SignatureFactory {
    /**
     * Creates a new {@link Signature} instance
     *
     * @return the new instance.
     */
    Signature newSignature();
}
