/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TestConstructorBean {

	private String fieldOne;

	private Integer fieldTwo;

	@JsonCreator
	public TestConstructorBean(@JsonProperty("fieldOne") String fieldOne, @JsonProperty("fieldTwo") Integer fieldTwo) {
		this.fieldOne = fieldOne;
		this.fieldTwo = fieldTwo;
	}

	public Integer getFieldTwo() {
		return fieldTwo;
	}

	public String getFieldOne() {
		return fieldOne;
	}

}
