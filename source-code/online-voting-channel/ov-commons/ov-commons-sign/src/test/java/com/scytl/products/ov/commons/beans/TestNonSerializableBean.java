/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

@SuppressWarnings("unused")
public class TestNonSerializableBean {

    private String fieldOne;

	private Integer fieldTwo;

	public void setFieldTwo(Integer fieldTwo) {
		this.fieldTwo = fieldTwo;
	}

	public void setFieldOne(String fieldOne) {
		this.fieldOne = fieldOne;
	}

}
