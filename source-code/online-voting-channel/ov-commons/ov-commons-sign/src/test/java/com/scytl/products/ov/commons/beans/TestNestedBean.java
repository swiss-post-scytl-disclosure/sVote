/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

public class TestNestedBean {

	private TestBean bean;

	private Integer index;

	public TestBean getBean() {
		return bean;
	}

	public void setBean(TestBean bean) {
		this.bean = bean;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
}
