/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.asymmetric.service.AsymmetricService;
import com.scytl.products.ov.commons.sign.CSVSigner;
import com.scytl.products.ov.commons.verify.CSVVerifier;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyPair;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CSVSignAndVerifyITest {

	private static CSVSigner signer;

	private static CSVVerifier verifier;

	private static KeyPair keyPairForSigning;

	private static Path sourceCSV = Paths.get("src/test/resources/example.csv");

	private static Path outputCSV = Paths.get("target/example.csv");

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@BeforeClass
	public static void setUp() throws GeneralCryptoLibException, IOException {

		signer = new CSVSigner();
		verifier = new CSVVerifier();

		AsymmetricService asymmetricService = new AsymmetricService();
		keyPairForSigning = asymmetricService.getKeyPairForSigning();
		Files.copy(sourceCSV, outputCSV, StandardCopyOption.REPLACE_EXISTING);
	}

	@Test
	public void sign_and_verify_a_given_csv_file() throws IOException, GeneralCryptoLibException {

		signer.sign(keyPairForSigning.getPrivate(), outputCSV);
		boolean verified = verifier.verify(keyPairForSigning.getPublic(), outputCSV);

		assertThat(verified, is(true));
	}

}
