/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.cms;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.Test;

import com.scytl.cryptolib.asymmetric.signer.configuration.ConfigDigitalSignerAlgorithmAndSpec;
import com.scytl.cryptolib.commons.constants.Constants;

/**
 * Tests of {@link DigitalSignerPolicyImpl}.
 */
public class DigitalSignerPolicyImplTest {
    @Test
    public void testGetDigitalSignerAlgorithmAndSpec() throws IOException {
        Properties properties = new Properties();
        try (InputStream stream = getClass().getResourceAsStream(
            '/' + Constants.CRYPTOLIB_POLICY_PROPERTIES_FILE_PATH)) {
            properties.load(stream);
        }
        String property = properties.getProperty("asymmetric.cms.signer");
        ConfigDigitalSignerAlgorithmAndSpec expected =
            ConfigDigitalSignerAlgorithmAndSpec.valueOf(property);
        ConfigDigitalSignerAlgorithmAndSpec actual =
            DigitalSignerPolicyImpl.newInstance()
                .getDigitalSignerAlgorithmAndSpec();
        assertEquals(expected, actual);
    }
}
