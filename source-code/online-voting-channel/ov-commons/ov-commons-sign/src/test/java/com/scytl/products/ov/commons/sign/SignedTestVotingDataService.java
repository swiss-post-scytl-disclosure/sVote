/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.sign;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.beans.Ballot;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import javax.naming.InvalidNameException;

/**
 * Voting data generator with vote signing.
 */
public class SignedTestVotingDataService extends TestVotingDataService {

    private final PublicKey verificationKey;

    private final PrivateKey signingKey;

    private final X509Certificate[] certificateChain;

    public SignedTestVotingDataService(Ballot ballot, AsymmetricServiceAPI asymmetricService,
            TestCertificateGenerator testCertificateGenerator)
            throws GeneralCryptoLibException, InvalidNameException {
        super(ballot);

        // Create signing data.

        KeyPair signingKeyPair = asymmetricService.getKeyPairForSigning();
        signingKey = signingKeyPair.getPrivate();
        verificationKey = signingKeyPair.getPublic();

        X509Certificate platformCACertificate = testCertificateGenerator.getRootCertificate();
        X509Certificate validationCertificate = testCertificateGenerator
                .createSigningLeafCertificate(signingKeyPair, testCertificateGenerator.getRootKeyPair().getPrivate(), platformCACertificate, "Signing certificate");

        certificateChain = new X509Certificate[] {validationCertificate, platformCACertificate};
    }

    public PublicKey getVerificationKey() {
        return verificationKey;
    }

    public PrivateKey getSigningKey() {
        return signingKey;
    }

    public X509Certificate[] getCertificateChain() {
        return certificateChain;
    }
}
