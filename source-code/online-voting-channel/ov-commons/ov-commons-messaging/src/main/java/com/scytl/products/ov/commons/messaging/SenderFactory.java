/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Factory of {@link Sender}.
 */
@ThreadSafe
interface SenderFactory {
    /**
     * Creates a new sender.
     * 
     * @return a new sender
     * @throws MessagingException
     *             failed to create a sender.
     */
    Sender newSender() throws MessagingException;
}
