/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import java.util.concurrent.Executor;

/**
 * Implementation of {@link Executor} which executes the commands in the current
 * thread.
 */
class CurrentThreadExecutor implements Executor {
    private static final CurrentThreadExecutor INSTANCE =
        new CurrentThreadExecutor();

    private CurrentThreadExecutor() {
    }

    /**
     * Returns the instance.
     * 
     * @return the instance.
     */
    public static CurrentThreadExecutor getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Runnable command) {
        command.run();
    }
}
