/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

/**
 * Queue.
 */
public final class Queue extends Destination {
    /**
     * Constructor.
     * 
     * @param name
     *            the name.
     */
    public Queue(String name) {
        super(name);
    }

}
