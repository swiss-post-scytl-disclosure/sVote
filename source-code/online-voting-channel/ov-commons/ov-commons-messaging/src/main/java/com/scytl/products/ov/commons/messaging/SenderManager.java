/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Sender manager.
 */
@ThreadSafe
interface SenderManager extends Destroyable {
    /**
     * Acquires a sender.
     * 
     * @return a sender
     * @throws MessagingException
     *             failed to acquire a sender.
     */
    Sender acquireSender() throws MessagingException;

    /**
     * Releases a sender.
     * 
     * @param sender
     *            the sender
     * @throws MessagingException
     *             failed to release the sender.
     */
    void releaseSender(Sender sender) throws MessagingException;
}
