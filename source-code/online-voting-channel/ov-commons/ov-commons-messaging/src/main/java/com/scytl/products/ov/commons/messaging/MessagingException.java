/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

/**
 * General messaging exception.
 */
@SuppressWarnings("serial")
public class MessagingException extends Exception {

    /**
     * Constructor.
     * 
     * @param message
     */
    public MessagingException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message
     * @param cause
     */
    public MessagingException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param cause
     *            the underlying exception
     */
    public MessagingException(Throwable cause) {
        super(cause);
    }
}
