/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import javax.annotation.concurrent.ThreadSafe;

import com.scytl.products.ov.commons.beans.domain.model.messaging.StreamSerializable;

/**
 * Codec to encode and to decode messages.
 */
@ThreadSafe
interface Codec {
    /**
     * Decodes the message from given bytes
     * 
     * @param bytes
     *            the bytes
     * @return the message
     * @throws InvalidMessageException
     *             failed to decode the message.
     */
    Object decode(byte[] bytes) throws InvalidMessageException;

    /**
     * Encodes a given message. The message must be one of the following:
     * <ul>
     * <li>{@code byte[]}</li>
     * <li>{@link StreamSerializable}</li>
     * </ul>
     * otherwise {@link InvalidMessageException} is thrown.
     * 
     * @param message
     *            the message
     * @return the encoded message
     * @throws InvalidMessageException
     *             the message is invalid
     */
    byte[] encode(Object message) throws InvalidMessageException;
}
