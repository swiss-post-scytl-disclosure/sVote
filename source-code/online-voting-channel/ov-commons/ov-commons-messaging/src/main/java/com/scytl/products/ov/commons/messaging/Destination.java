/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import static java.util.Objects.requireNonNull;

import javax.annotation.concurrent.Immutable;

/**
 * Destination.
 */
@Immutable
public abstract class Destination {
    private final String name;

    /**
     * Constructor. For internal use only.
     * 
     * @param name
     *            the name
     */
    Destination(String name) {
        this.name = requireNonNull(name, "Name is null.");
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Destination other = (Destination) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /**
     * Returns the name.
     * 
     * @return the name.
     */
    public final String name() {
        return name;
    }

    @Override
    public final String toString() {
        return getClass().getSimpleName() + " [name=" + name + "]";
    }
}
