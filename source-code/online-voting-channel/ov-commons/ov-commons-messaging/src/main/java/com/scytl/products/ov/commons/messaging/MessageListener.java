/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

/**
 * <p>
 * Message listener.
 * <p>
 * The concrete message type in the {@link #onMessage(Object)} method can be one
 * of the types listed in the contract of
 * {@link MessagingService#send(Destination, Object)}.
 * <p>
 * An instance of this interface can be called concurrently if it is used by
 * more than one receiver or the executor passed to
 * {@link MessagingService#createReceiver(Destination, MessageListener, java.util.concurrent.Executor)}
 * allows parallel execution.
 */
@FunctionalInterface
public interface MessageListener {
    /**
     * <p>
     * Message received.
     * 
     * @param message
     *            the message.
     */
    void onMessage(Object message);
}
