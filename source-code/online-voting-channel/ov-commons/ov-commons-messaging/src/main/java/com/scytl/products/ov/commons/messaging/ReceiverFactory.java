/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import java.util.concurrent.Executor;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Factory for {@link Receiver}:
 */
@ThreadSafe
interface ReceiverFactory {
    /**
     * Creates a new receiver for given destination, listener and executor.
     * 
     * @param destination
     *            the destination
     * @param listener
     *            the listener
     * @return a new receiver
     * @throws MessagingException
     *             failed to create a receiver.
     */
    Receiver newReceiver(Destination destination, MessageListener listener,
            Executor executor)
            throws MessagingException;
}
