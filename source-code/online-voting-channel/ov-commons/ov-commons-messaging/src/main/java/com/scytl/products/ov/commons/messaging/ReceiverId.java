/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import javax.annotation.concurrent.Immutable;

/**
 * Receiver identifier.
 */
@Immutable
final class ReceiverId {
    private final Destination destination;

    private final MessageListener listener;

    /**
     * Constructor.
     * 
     * @param destination
     * @param listener
     */
    public ReceiverId(Destination destination, MessageListener listener) {
        this.destination = destination;
        this.listener = listener;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ReceiverId other = (ReceiverId) obj;
        if (listener == null) {
            if (other.listener != null) {
                return false;
            }
        } else if (listener != other.listener) {
            return false;
        }
        if (destination == null) {
            if (other.destination != null) {
                return false;
            }
        } else if (!destination.equals(other.destination)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((listener == null) ? 0 : listener.hashCode());
        result = prime * result
            + ((destination == null) ? 0 : destination.hashCode());
        return result;
    }
}
