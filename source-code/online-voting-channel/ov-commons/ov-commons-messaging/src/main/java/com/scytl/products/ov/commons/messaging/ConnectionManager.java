/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import javax.annotation.concurrent.ThreadSafe;

import com.rabbitmq.client.Connection;

/**
 * Connection manager.
 */
@ThreadSafe
interface ConnectionManager extends Destroyable {
    /**
     * Returns a connection. Client should not close the returned connection.
     * 
     * @return a connection
     * @throws MessagingException
     *             failed to get a connection.
     */
    Connection getConnection() throws MessagingException;
}
