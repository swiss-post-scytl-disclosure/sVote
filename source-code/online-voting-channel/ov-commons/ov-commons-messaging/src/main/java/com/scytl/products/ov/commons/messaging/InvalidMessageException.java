/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

/**
 * Message is invalid.
 */
@SuppressWarnings("serial")
public final class InvalidMessageException extends MessagingException {

    /**
     * Constructor.
     * 
     * @param message
     */
    public InvalidMessageException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message
     * @param cause
     */
    public InvalidMessageException(String message, Throwable cause) {
        super(message, cause);
    }
}
