/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

/**
 * Destroyable object. Once destroyed the object cannot be used, any attempt to
 * use a destroyed object leads to an exception.
 */
interface Destroyable {
    /**
     * Destroys the object and releases all allocated resource.
     * 
     * @throws MessagingException
     */
    void destroy() throws MessagingException;
}
