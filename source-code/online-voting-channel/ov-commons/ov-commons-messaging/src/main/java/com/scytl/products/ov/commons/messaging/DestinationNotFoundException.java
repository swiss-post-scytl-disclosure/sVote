/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

/**
 * Destination does not exist.
 */
@SuppressWarnings("serial")
public final class DestinationNotFoundException
        extends MessagingException {

    /**
     * Constructor.
     * 
     * @param message
     */
    public DestinationNotFoundException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message
     * @param cause
     */
    public DestinationNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
