/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

/**
 * Topc.
 */
public final class Topic extends Destination {
    /**
     * Constructor.
     * 
     * @param name
     *            the name.
     */
    public Topic(String name) {
        super(name);
    }
}
