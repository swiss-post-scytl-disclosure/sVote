/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;

/**
 * Tests of the {@link CurrentThreadExecutor}:
 */
public class CurrentThreadExecutorTest {
    @Test
    public void testExecute() {
        AtomicReference<Thread> reference = new AtomicReference<Thread>();
        Runnable command = () -> reference.set(Thread.currentThread());
        CurrentThreadExecutor.getInstance().execute(command);
        assertEquals(reference.get(), Thread.currentThread());
    }
}
