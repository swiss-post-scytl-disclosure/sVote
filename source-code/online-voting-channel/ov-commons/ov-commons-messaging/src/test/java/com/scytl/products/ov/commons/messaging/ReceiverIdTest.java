/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.Test;

/**
 * Tests of {@link ReceiverId}.
 */
public class ReceiverIdTest {
    @Test
    public void testEqualsObject() {
        Destination queue1 = new Queue("queue");
        Destination queue2 = new Queue("queue");
        Destination queue3 = new Queue("anotherQueue");

        MessageListener listener1 = mock(MessageListener.class);
        MessageListener listener2 = mock(MessageListener.class);

        ReceiverId id1 = new ReceiverId(queue1, listener1);
        ReceiverId id2 = new ReceiverId(queue2, listener1);
        ReceiverId id3 = new ReceiverId(queue3, listener1);
        ReceiverId id4 = new ReceiverId(queue1, listener2);

        assertTrue(id1.equals(id1));
        assertTrue(id1.equals(id2));
        assertFalse(id1.equals(id3));
        assertFalse(id1.equals(id4));
    }
}
