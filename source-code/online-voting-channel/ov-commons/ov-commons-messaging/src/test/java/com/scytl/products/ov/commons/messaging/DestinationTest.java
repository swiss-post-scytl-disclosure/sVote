/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.messaging;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests of {@link Destination}.
 */
public class DestinationTest {
    @Test
    public void testEqualsObject() {
        Destination destination1 = new Queue("queue");
        Destination destination2 = new Queue("queue");
        Destination destination3 = new Queue("queue3");

        Destination destination4 = new Topic("topic");
        Destination destination5 = new Topic("topic");
        Destination destination6 = new Topic("topic6");

        Destination destination7 = new Topic("queue");

        assertTrue(destination1.equals(destination1));
        assertTrue(destination1.equals(destination2));
        assertFalse(destination1.equals(destination3));

        assertTrue(destination4.equals(destination4));
        assertTrue(destination4.equals(destination5));
        assertFalse(destination4.equals(destination6));

        assertFalse(destination1.equals(destination7));
    }
}
