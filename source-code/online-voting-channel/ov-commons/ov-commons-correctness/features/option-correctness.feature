Feature: Full vote correctness evaluator on non-encrypted values
	
	Scenario: No rule specified
		When evaluating correctness for a ballot containing selection vo1, vo2
		Then the correctness should pass
		Then finish correctness test
	
	Scenario: 2 voting sections, minimum selections is not reached in one section, FAIL
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 3 and with non-blank options vo1, vo2, vo3
		Given a voting section with name section2, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo4, vo5, vo6
		When evaluating correctness for a ballot containing selection vo5
		Then the correctness should fail
		Then the error should be MIN_ERROR, section1
		Then the error should be MAX_ERROR, section1
		Then the error should be MAX_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test
	
	Scenario: 2 voting sections, minimum selections is not reached in both of the sections, FAIL
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 3 and with non-blank options vo1, vo2, vo3
		Given a voting section with name section2, with minimum non-blank selections 2 and maximum selections 2 and with non-blank options vo4, vo5, vo6
		When evaluating correctness for a ballot containing selection vo4
		Then the correctness should fail
		Then the error should be MIN_ERROR, section1
		Then the error should be MIN_ERROR, section2
		Then the error should be MAX_ERROR, section1
		Then the error should be MAX_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test
	
	Scenario: 1 voting section, min blank options respected with full selection PASS
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 5, and with blank options vo1, vo2, vo3 and with non-blank options vo4, vo5, vo6, vo7
		When evaluating correctness for a ballot containing selection vo4, vo5, vo1, vo2, vo3
		Then the correctness should pass
		Then finish correctness test

	Scenario: 1 voting section, min non-blank options not respected FAIL
		Given allow full blank false
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1 and with non-blank options vo2, vo3, vo4, vo5
		When evaluating correctness for a ballot containing selection vo1, vo2
		Then the correctness should fail
		Then the error should be MIN_NON_BLANK_ERROR, section1
		Then the error should be MAX_ERROR, section1
		Then the error does not contain any additional messages
		Then finish correctness test
 
	Scenario: 1 voting section, min non-blank options respected with some selections there is allow blank FAIL
		Given allow full blank true
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1, vo2, vo3 and with non-blank options vo4, vo5
		When evaluating correctness for a ballot containing selection vo1, vo4
		Then the correctness should fail
		Then the error should be MIN_NON_BLANK_ERROR, section1
		Then the error should be MAX_ERROR, section1
		Then the error does not contain any additional messages
		Then finish correctness test
		
	Scenario: 1 voting section, min non-blank options not respected but there is allow blank PASS
		Given allow full blank true
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1, vo2, vo3 and with non-blank options vo4, vo5
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3
		Then the correctness should pass
		Then finish correctness test
		
	Scenario: 1 voting section, min non-blank options not respected but there is allow blank FAIL
		Given allow full blank false
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1, vo2, vo3 and with non-blank options vo4, vo5
		When evaluating correctness for a ballot containing selection vo1, vo2, vo4
		Then the correctness should fail
		Then the error should be MIN_NON_BLANK_ERROR, section1
		Then the error does not contain any additional messages
		Then finish correctness test
		
	Scenario: 1 voting section, min blank options not respected but there is allow blank FAIL
		Given allow full blank false
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1, vo2, vo3 and with non-blank options vo4, vo5
		When evaluating correctness for a ballot containing selection vo1, vo2
		Then the correctness should fail
		Then the error should be MIN_NON_BLANK_ERROR, section1
		Then the error should be MAX_ERROR, section1
		Then the error does not contain any additional messages
		Then finish correctness test
		
	Scenario: 3 voting section, min selection not respected in selection that has no blank FAIL
		Given allow full blank false
		Given a voting section with name ed339b83ba544b28a97931f41b85088a, with minimum non-blank selections 1 and maximum selections 1 and with non-blank options 2, 11
		Given a voting section with name 11fb4d75dabb4f1ca180f690ed4e76f8, with minimum non-blank selections 0 and maximum selections 1, and with blank options 29 and with non-blank options 5, 17
		Given a voting section with name 76cf7bc5e07b4013ab8945a87b7758a3, with minimum non-blank selections 0 and maximum selections 1, and with blank options 13 and with non-blank options 3, 23
		When evaluating correctness for a ballot containing selection 29, 13
		Then the correctness should fail
		Then the error should be MIN_ERROR, ed339b83ba544b28a97931f41b85088a
		Then the error should be MAX_ERROR, ed339b83ba544b28a97931f41b85088a
		Then the error does not contain any additional messages
		Then finish correctness test
		
	Scenario: 2 voting section, min non-blank options not respected in one of them FAIL
		Given allow full blank false
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1 and with non-blank options vo2, vo3, vo4, vo5
		Given a voting section with name section2, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo6 and with non-blank options vo7, vo8, vo9, vo10
		When evaluating correctness for a ballot containing selection vo2, vo3, vo6, vo7
		Then the correctness should fail
		Then the error should be MIN_NON_BLANK_ERROR, section2
		Then the error should be MAX_ERROR, section1
		Then the error should be MAX_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test

	Scenario: 2 voting section, min non-blank options not respected in one of them even if there is allow blank FAIL
		Given allow full blank true
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1, vo12, vo13 and with non-blank options vo2, vo3, vo4, vo5
		Given a voting section with name section2, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo6, vo61, vo62 and with non-blank options vo7, vo8, vo9, vo10
		When evaluating correctness for a ballot containing selection vo1, vo12, vo3, vo6, vo61, vo62
		Then the error should be MIN_NON_BLANK_ERROR, section1
		Then the error should be MIN_NON_BLANK_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test
	
	Scenario: 2 voting section, min blank options are not respected in either of them because no selections but there is allow blank PASS
		Given allow full blank true
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1, vo12, vo13 and with non-blank options vo2, vo3, vo4, vo5
		Given a voting section with name section2, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo6, vo61, vo62 and with non-blank options vo7, vo8, vo9, vo10
		When evaluating correctness for a ballot containing selection vo1, vo12, vo13, vo6, vo61, vo62
		Then the correctness should pass
		Then finish correctness test
	
	Scenario: 2 voting section, full blank is false, but min non-blank selections are 0, only blanks selected PASS
		Given allow full blank false
		Given a voting section with name section1, with minimum non-blank selections 0 and maximum selections 1, and with blank options vo1 and with non-blank options vo2, vo3
		Given a voting section with name section2, with minimum non-blank selections 0 and maximum selections 1, and with blank options vo6 and with non-blank options vo7, vo8
		When evaluating correctness for a ballot containing selection vo1, vo6
		Then the correctness should pass
		Then finish correctness test

	Scenario: 2 voting section, min non-blank options not respected in one of them even if there is allow blank FAIL
		Given allow full blank true
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo1, vo12, vo13 and with non-blank options vo2, vo3, vo4, vo5
		Given a voting section with name section2, with minimum non-blank selections 2 and maximum selections 3, and with blank options vo6, vo61, vo62 and with non-blank options vo7, vo8, vo9, vo10
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3, vo6, vo61, vo62
		Then the error should be MIN_NON_BLANK_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test
	
	Scenario: Correctness fails if selection is not present in the available voting options and the minimum selections is not reached in one of the 2 different voting sections
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 3 and with non-blank options vo1, vo2, vo3
		Given a voting section with name section2, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo4, vo5, vo6
		When evaluating correctness for a ballot containing selection vo8
		Then the correctness should fail
		Then the error should be MIN_ERROR, section1
		Then the error should be MIN_ERROR, section2
		Then the error should be MAX_ERROR, section1
		Then the error should be MAX_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test
		
	Scenario: Correctness fails if selection is not present in the available voting options and the minimum selections is not reached in both of the 2 different voting sections
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3 and with non-blank options vo1, vo2, vo3
		Given a voting section with name section2, with minimum non-blank selections 2 and maximum selections 2 and with non-blank options vo4, vo5, vo6
		When evaluating correctness for a ballot containing selection vo7, vo8
		Then the correctness should fail
		Then the error should be MIN_ERROR, section1
		Then the error should be MIN_ERROR, section2
		Then the error should be MAX_ERROR, section1
		Then the error should be MAX_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test
	
	Scenario: Correctness fails if the maximum selections is exceeded in one of the 2 different voting sections
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo1, vo2, vo3
		Given a voting section with name section2, with minimum non-blank selections 1 and maximum selections 1 and with non-blank options vo4, vo5, vo6
		When evaluating correctness for a ballot containing selection vo5, vo1, vo2, vo6
		Then the correctness should fail
		Then the error should be MAX_ERROR, section2
		Then finish correctness test
        
	Scenario: Correctness fails if the maximum selections is exceeded in both of the 2 different voting sections
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 1 and with non-blank options vo1, vo2, vo3
		Given a voting section with name section2, with minimum non-blank selections 1 and maximum selections 1 and with non-blank options vo4, vo5, vo6
		When evaluating correctness for a ballot containing selection vo5, vo1, vo2, vo6
		Then the correctness should fail
		Then the error should be MAX_ERROR, section1
		Then the error should be MAX_ERROR, section2
		Then finish correctness test
    
    Scenario: 1 voting section + 3 candidates + Accumulation 1, PASS 
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 3 and with non-blank options vo1, vo2, vo3
		# Given accumulation not allowed (accumulation 1)
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3
		Then the correctness should pass
		Then the error does not contain any additional messages
		Then finish correctness test

	Scenario: 1 voting section + 3 candidates + Accumulation 2, PASS 
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 3 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6
		# Given accumulation allowed (accumulation 2)
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3
		Then the correctness should pass
		Then the error does not contain any additional messages
		Then finish correctness test

	Scenario: 2 voting sections + Accumulation 2, PASS 
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 5 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6, vo11, vo12, vo13, vo14, vo15, vo16
		Given a voting section with name section2, with minimum non-blank selections 1 and maximum selections 3 and with non-blank options vo21, vo22, vo23, vo24, vo25, vo26, vo31, vo32, vo33, vo34, vo35, vo36
		# Given accumulation allowed (accumulation 2)
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3, vo4, vo5, vo21, vo32, vo22
		Then the correctness should pass
		Then finish correctness test

	Scenario: Fusion + Accumulation 1, PASS
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6
		Given fusion with accumulation 1 is defined for section1 with values [vo1, vo2, vo3], [vo4, vo5, vo6]
		When evaluating correctness for a ballot containing selection vo1, vo4
		Then the correctness should pass
		Then finish correctness test
       
   	Scenario: Fusion + Accumulation 1, fusion violation, FAIL
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6
		Given fusion with accumulation 1 is defined for section1 with values [vo1, vo2, vo3], [vo4, vo5, vo6]
		When evaluating correctness for a ballot containing selection vo1, vo2
		Then the correctness should fail
   		Then the error should be ACCUMULATION_ERROR, vo1
		Then the error should be ACCUMULATION_ERROR, vo2
		Then finish correctness test
        
    Scenario: Fusion + Accumulation 2, voting the same candidate on different lists, PASS
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6, vo7, vo8, vo9, vo10, vo11, vo12
		Given fusion with accumulation 2 is defined for section1 with values [vo1, vo2, vo3, vo7, vo8, vo9], [vo4, vo5, vo6, vo10, vo11, vo12]
		When evaluating correctness for a ballot containing selection vo1, vo2
		Then the correctness should pass
		Then finish correctness test
		
    Scenario: Fusion + Accumulation 2, voting the same candidate on different lists, FAIL
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 4 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6, vo7, vo8, vo9, vo10, vo11, vo12
		Given fusion with accumulation 2 is defined for section1 with values [vo1, vo2, vo3, vo7, vo8, vo9], [vo4, vo5, vo6, vo10, vo11, vo12]
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3, vo4
		Then the correctness should fail
		Then the error should be ACCUMULATION_ERROR, vo1
		Then the error should be ACCUMULATION_ERROR, vo2
		Then the error should be ACCUMULATION_ERROR, vo3
		Then the error does not contain any additional messages
		Then finish correctness test
		
    Scenario: Fusion + Accumulation 2, voting the same candidate on different lists, FAIL
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 4 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6, vo7, vo8, vo9
		Given fusion with accumulation 2 is defined for section1 with values [vo1, vo2, vo3, vo7, vo8, vo9], [vo4, vo5, vo6]
		Given a voting section with name section2, with minimum non-blank selections 1 and maximum selections 4 and with non-blank options vo11, vo12, vo13, vo14, vo15, vo16, vo17, vo18, vo19
		Given fusion with accumulation 2 is defined for section2 with values [vo11, vo12, vo13, vo17, vo18, vo19], [vo14, vo15, vo16]
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3, vo7, vo8, vo11, vo12, vo13, vo17, vo18
		Then the correctness should fail
		Then the error should be ACCUMULATION_ERROR, vo1
		Then the error should be ACCUMULATION_ERROR, vo2
		Then the error should be ACCUMULATION_ERROR, vo3
		Then the error should be ACCUMULATION_ERROR, vo7
		Then the error should be ACCUMULATION_ERROR, vo8
		Then the error should be ACCUMULATION_ERROR, vo11
		Then the error should be ACCUMULATION_ERROR, vo12
		Then the error should be ACCUMULATION_ERROR, vo13
		Then the error should be ACCUMULATION_ERROR, vo17
		Then the error should be ACCUMULATION_ERROR, vo18
		Then the error should be MAX_ERROR, section1
		Then the error should be MAX_ERROR, section2
		Then the error does not contain any additional messages
		Then finish correctness test

    Scenario: Fusion + Accumulation 2, voting the same candidate on different lists, PASS
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 6 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6, vo7, vo8, vo9
		Given fusion with accumulation 3 is defined for section1 with values [vo1, vo2, vo3, vo7, vo8, vo9], [vo4, vo5, vo6]
		Given a voting section with name section2, with minimum non-blank selections 1 and maximum selections 6 and with non-blank options vo11, vo12, vo13, vo14, vo15, vo16, vo17, vo18, vo19
		Given fusion with accumulation 3 is defined for section2 with values [vo11, vo12, vo13, vo17, vo18, vo19], [vo14, vo15, vo16]
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3, vo4, vo5, vo6, vo11, vo12, vo13, vo14, vo15, vo16
		Then the correctness should pass
		Then finish correctness test

	Scenario: Fusion + Accumulation 2, fusion violation, FAIL
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6, vo7, vo8, vo9, vo10, vo11, vo12
		Given fusion with accumulation 2 is defined for section1 with values [vo1, vo2, vo3, vo7, vo8, vo9], [vo4, vo5, vo6, vo10, vo11, vo12]
		When evaluating correctness for a ballot containing selection vo1, vo2, vo8, vo11
		Then the correctness should fail
		Then the error should be ACCUMULATION_ERROR, vo1
		Then the error should be ACCUMULATION_ERROR, vo2
		Then the error should be ACCUMULATION_ERROR, vo8
		Then the error should be MAX_ERROR, section1
		Then the error does not contain any additional messages
		Then finish correctness test

	Scenario: Fusion + Accumulation 2, maximum selections is exceeded, FAIL
		Given a voting section with name section1, with minimum non-blank selections 1 and maximum selections 2 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6, vo7, vo8, vo9, vo10, vo11, vo12
		Given fusion with accumulation 2 is defined for section1 with values [vo1, vo2, vo3, vo7, vo8, vo9], [vo4, vo5, vo6, vo10, vo11, vo12]
		When evaluating correctness for a ballot containing selection vo1, vo2, vo11
		Then the correctness should fail
		Then the error should be MAX_ERROR, section1
		Then finish correctness test
		
	Scenario: Fusion + Accumulation 2, maximum selections is exceeded + invalid selection + fusion violation, FAIL 
		Given a voting section with name section1, with minimum non-blank selections 2 and maximum selections 3 and with non-blank options vo1, vo2, vo3, vo4, vo5, vo6
		Given fusion with accumulation 2 is defined for section1 with values [vo1, vo2, vo3], [vo4, vo5, vo6]
		When evaluating correctness for a ballot containing selection vo1, vo2, vo3, vo11, vo21
		Then the correctness should fail
		Then the error should be ACCUMULATION_ERROR, vo1
		Then the error should be ACCUMULATION_ERROR, vo2
		Then the error should be ACCUMULATION_ERROR, vo3
		Then the error does not contain any additional messages
		Then finish correctness test

