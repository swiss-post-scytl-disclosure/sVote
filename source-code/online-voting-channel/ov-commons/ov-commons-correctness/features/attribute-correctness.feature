Feature: Full vote correctness evaluator on encrypted values

	Scenario: No rule specified
		When the selection contains an option with attributes unusedAttributeId11, unusedAttributeId12, attributeId4
		When evaluating correctness on the encryped options
		Then the server-side correctness should pass
		Then finish server-side correctness test
		
	Scenario: Correctness is successful if the submitted options are satisfying the attributes
		Given a contest with name contest1
		Given a question with correctness attribute attributeId1 with maximum selection of 2
		Given a question with correctness attribute attributeId2 with maximum selection of 1
		Given a contest with name contest2
		Given a question with correctness attribute attributeId3 with maximum selection of 1
		Given a question with correctness attribute attributeId4 with maximum selection of 2
		When the selection contains an option with attributes unusedAttributeId1, unusedAttributeId2, attributeId1
		When the selection contains an option with attributes unusedAttributeId3, unusedAttributeId4, attributeId1
		When the selection contains an option with attributes unusedAttributeId5, unusedAttributeId6, attributeId2
		When the selection contains an option with attributes unusedAttributeId7, unusedAttributeId8, attributeId3
		When the selection contains an option with attributes unusedAttributeId9, unusedAttributeId10, attributeId4
		When the selection contains an option with attributes unusedAttributeId11, unusedAttributeId12, attributeId4
		When evaluating correctness on the encryped options
		Then the server-side correctness should pass
		Then finish server-side correctness test
	
	Scenario: Correctness fails if the submitted options are not satisfying the attributes
		Given a contest with name contest1
		Given a question with correctness attribute attributeId1 with maximum selection of 2
		Given a question with correctness attribute attributeId2 with maximum selection of 1
		Given a contest with name contest2
		Given a question with correctness attribute attributeId3 with maximum selection of 2
		Given a question with correctness attribute attributeId4 with maximum selection of 1
		When the selection contains an option with attributes unusedAttributeId1, unusedAttributeId2, attributeId1
		When the selection contains an option with attributes unusedAttributeId3, unusedAttributeId4, attributeId2
		When the selection contains an option with attributes unusedAttributeId5, unusedAttributeId6, attributeId2
		When the selection contains an option with attributes unusedAttributeId7, unusedAttributeId8, attributeId3
		When the selection contains an option with attributes unusedAttributeId9, unusedAttributeId10, attributeId4
		When the selection contains an option with attributes unusedAttributeId11, unusedAttributeId12, attributeId4
		When evaluating correctness on the encryped options
		Then the server-side correctness should fail
		Then the attribute correctness error should be MIN_ERROR, contest1
		Then the attribute correctness error should be MAX_ERROR, contest1
		Then the attribute correctness error should be MIN_ERROR, contest2
		Then the attribute correctness error should be MAX_ERROR, contest2
		Then the attribute correctness error does not contain any additional messages
		Then finish server-side correctness test
        
	
