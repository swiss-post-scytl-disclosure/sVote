/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.script.ScriptException;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.correctness.builder.attributes.AttributesCorrectnessBuilder;
import com.scytl.products.ov.commons.correctness.feedback.CorrectnessFeedback;
import com.scytl.products.ov.commons.correctness.internal.AttributeCorrectnessEvaluator;
import com.scytl.products.ov.commons.correctness.internal.CorrectnessEvaluator;

public class AttributeVoteCorrectnessTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(AttributeVoteCorrectnessTest.class);
	
	private CorrectnessEvaluator<List<String>> ce = new AttributeCorrectnessEvaluator(LOGGER);
	
	@Test
	public void evaluateList() throws NoSuchMethodException, ScriptException{
		AttributesCorrectnessBuilder cb = new AttributesCorrectnessBuilder();
		cb.addContest("contestId1").addQuestion("attributeId1", 2).addQuestion("attributeId2", 1).build()
		.addContest("contestId2").addQuestion("attributeId3", 1).addQuestion("attributeId4", 2).build();
		List<List<String>> selections = new ArrayList<>();
		String[] selectedVotingOption1Attributes = { "a", "b", "attributeId1" };
		String[] selectedVotingOption2Attributes = { "c", "d", "attributeId1" };
		String[] selectedVotingOption3Attributes = { "e", "f", "attributeId2" };
		String[] selectedVotingOption4Attributes = { "g", "h", "attributeId3" };
		String[] selectedVotingOption5Attributes = { "i", "j", "attributeId4" };
		String[] selectedVotingOption6Attributes = { "k", "l", "attributeId4" };
		selections.add(Arrays.asList(selectedVotingOption1Attributes));
		selections.add(Arrays.asList(selectedVotingOption2Attributes));
		selections.add(Arrays.asList(selectedVotingOption3Attributes));
		selections.add(Arrays.asList(selectedVotingOption4Attributes));
		selections.add(Arrays.asList(selectedVotingOption5Attributes));
		selections.add(Arrays.asList(selectedVotingOption6Attributes));
		LOGGER.info(cb.buildWithSafeEncoding());
		CorrectnessFeedback checkCorrectness = ce.checkCorrectness(selections, cb);
		Assert.assertTrue(checkCorrectness.getResult());
		Assert.assertNull(checkCorrectness.getErrors());
	}

}
