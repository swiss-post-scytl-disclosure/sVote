/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness;

import com.scytl.products.ov.commons.correctness.builder.options.VotingOptionCorrectnessBuilder;

public class WrongCorrectnessBuilder extends VotingOptionCorrectnessBuilder {

	@Override
	public String build() {
		return "function evaluateCorrectness(selection){var p = d;};";
	}
	@Override
	public String buildWithSafeEncoding() {
		return "function evaluateCorrectness(selection){var p = d;};";
	}
}
