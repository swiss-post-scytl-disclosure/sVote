/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness.internal;

import javax.script.ScriptException;

import org.slf4j.Logger;

import com.scytl.products.ov.commons.correctness.builder.CorrectnessBuilder;

public class OptionCorrectnessEvaluator extends CorrectnessEvaluator<String> {

	public OptionCorrectnessEvaluator(Logger logger) {
		super(logger);
	}

	public OptionCorrectnessEvaluator(Logger logger, boolean isBase64Encoded) {
		super(logger, isBase64Encoded);
	}

	public OptionCorrectnessEvaluator(Logger logger, CorrectnessBuilder correctness, boolean isBase64Encoded) throws ScriptException {
		super(logger, correctness, isBase64Encoded);
	}

	public OptionCorrectnessEvaluator(Logger logger, String correctness) throws ScriptException {
		super(logger, correctness);
	}
	
}
