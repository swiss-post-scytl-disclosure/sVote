/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness.builder.attributes;

import java.util.HashSet;
import java.util.Set;

import com.scytl.products.ov.commons.correctness.builder.CorrectnessBuilder;
import com.scytl.products.ov.commons.correctness.builder.CorrectnessUtil;

public class AttributesCorrectnessBuilder implements CorrectnessBuilder{
	
	private Set<ContestCorrectionBuilder> contests = new HashSet<>();

	@Override
	public String build() {
		StringBuilder sb = new StringBuilder();
		sb.append("function " + CorrectnessUtil.EVALUATE_CORRECTNESS + "(selection, callbackFunction){"
				+ "if (typeof(callbackFunction)==='undefined') { callbackFunction = function (type, reference, extra) {};};");

		sb.append("var result = true;");
		for (ContestCorrectionBuilder contestCorrectionBuilder : contests) {
			contestCorrectionBuilder.generateCorrectnessCode(sb);
		}
		sb.append("return result;");
		sb.append("}");
		return sb.toString();
	}

	public ContestCorrectionBuilder addContest(String contestId) {
		ContestCorrectionBuilder contest = new ContestCorrectionBuilder(contestId, this);
		contests.add(contest);
		return contest;
	}

	@Override
	public String buildWithSafeEncoding() {
		return build();
	}

}
