/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness.builder;

public interface CorrectnessBuilder {

	/**
	 * This build method results in an executable code, that is <b>not</b>
	 * escaping control characters that are present in the configuration
	 * provided. Should be used always, except in the cases where free text can
	 * occur in the provided configuration (eg. correctness is based on
	 * aliases).
	 * 
	 * @return The executable script without control character escaping.
	 */
	String build();

	/**
	 * This build method results in an executable code, that <b>is</b> escaping
	 * control characters that are present in the configuration provided. Should
	 * be used in the cases where free text can occur in the provided
	 * configuration (eg. correctness is based on aliases).
	 * 
	 * @return The executable script with control character escaping.
	 */
	String buildWithSafeEncoding();
}
