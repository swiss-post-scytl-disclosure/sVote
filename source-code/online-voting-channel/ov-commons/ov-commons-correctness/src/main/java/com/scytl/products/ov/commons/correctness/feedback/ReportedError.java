/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness.feedback;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.scytl.products.ov.commons.correctness.CorrectnessError;

public class ReportedError {
	private String errorType;
	private String reference;

	public ReportedError(@JsonProperty(value = "type") String errorType, @JsonProperty(value = "reference") String reference) {
		super();
		this.errorType = errorType;
		this.reference = reference;
	}

	public CorrectnessError getErrorType() {
		return CorrectnessError.valueOf(errorType);
	}

	public String getReference() {
		return reference;
	}

}
