/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness.internal;

import java.util.List;

import javax.script.ScriptException;

import org.slf4j.Logger;

public class AttributeCorrectnessEvaluator extends CorrectnessEvaluator<List<String>>{

	public AttributeCorrectnessEvaluator(Logger logger) {
		super(logger, false);
	}

	public AttributeCorrectnessEvaluator(Logger logger, String correctness) throws ScriptException {
		super(logger, correctness);
	}
}
