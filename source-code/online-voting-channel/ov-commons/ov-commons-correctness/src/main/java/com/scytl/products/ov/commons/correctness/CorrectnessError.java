/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.correctness;

public enum CorrectnessError {
	SELECTION_ERROR, MULTIPLE_SELECTION_ERROR, MIN_ERROR, MAX_ERROR, MIN_NON_BLANK_ERROR, ACCUMULATION_ERROR;
}
