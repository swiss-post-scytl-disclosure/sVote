/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

/**
 * An object to represent ballot box information. It will be serialized to a
 * json file in order to be used by other modules.
 */
public class BallotBox {

    private boolean test;

    private String eeid;

    private String bid;

    private String id;

    private String alias;

    private EncryptionParameters encryptionParameters;

    private String ballotBoxCert;

    private String electoralAuthorityId;

    private String startDate;

    private String endDate;

    private String gracePeriod;

    private boolean confirmationRequired;
    
    private String writeInAlphabet;

    /**
     *
     */
    public BallotBox() {
    }

    /**
     * Returns the current value of the field isTest.
     *
     * @return Returns the isTest.
     */
    public boolean isTest() {
        return test;
    }

    /**
     * Sets the value of the field isTest.
     *
     * @param test
     *            The isTest to set.
     */
    public void setTest(boolean test) {
    	this.test = test;
    }

    /**
     * @return Returns the encryptionParameters.
     */
    public EncryptionParameters getEncryptionParameters() {
        return encryptionParameters;
    }

    /**
     * @param encryptionParameters
     *            The encryptionParameters to set.
     */
    public void setEncryptionParameters(final EncryptionParameters encryptionParameters) {
    	this.encryptionParameters = encryptionParameters;
    }

    /**
     * @return Returns the eeid.
     */
    public String getEeid() {
        return eeid;
    }

    /**
     * @param eeid
     *            The eeid to set.
     */
    public void setEeid(final String eeid) {
    	this.eeid = eeid;
    }

    /**
     * @return Returns the bid.
     */
    public String getBid() {
        return bid;
    }

    /**
     * @param bid
     *            The bid to set.
     */
    public void setBid(final String bid) {
    	this.bid = bid;
    }

    /**
     * @return Returns the ballotBoxCert.
     */
    public String getBallotBoxCert() {
        return ballotBoxCert;
    }

    /**
     * @param ballotBoxCert
     *            The ballotBoxCert to set.
     */
    public void setBallotBoxCert(final String ballotBoxCert) {
    	this.ballotBoxCert = ballotBoxCert;
    }

    /**
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            The id to set.
     */
    public void setId(final String id) {
    	this.id = id;
    }

    /**
     * @return Returns the startDate.
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            The startDate to set.
     */
    public void setStartDate(final String startDate) {
    	this.startDate = startDate;
    }

    /**
     * @return Returns the endDate.
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            The endDate to set.
     */
    public void setEndDate(final String endDate) {
    	this.endDate = endDate;
    }

    /**
     * Returns the current value of the field electoralAuthorityId.
     *
     * @return Returns the electoralAuthorityId.
     */
    public String getElectoralAuthorityId() {
        return electoralAuthorityId;
    }

    /**
     * Sets the value of the field electoralAuthorityId.
     *
     * @param electoralAuthorityId
     *            The electoralAuthorityId to set.
     */
    public void setElectoralAuthorityId(String electoralAuthorityId) {
    	this.electoralAuthorityId = electoralAuthorityId;
    }

    /**
     * Sets new _gracePeriod.
     *
     * @param gracePeriod
     *            New value of _gracePeriod.
     */
    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    /**
     * Gets _gracePeriod.
     *
     * @return Value of _gracePeriod.
     */
    public String getGracePeriod() {
        return gracePeriod;
    }

    /**
     * Sets new _confirmationRequired.
     *
     * @param confirmationRequired
     *            New value of _confirmationRequired.
     */
    public void setConfirmationRequired(boolean confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    /**
     * Gets _confirmationRequired.
     *
     * @return Value of _confirmationRequired.
     */
    public boolean getConfirmationRequired() {
        return confirmationRequired;
    }

    /**
     * Sets new _alias.
     *
     * @param alias
     *            New value of _alias.
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * Gets _alias.
     *
     * @return Value of _alias.
     */
    public String getAlias() {
        return alias;
    }

	public String getWriteInAlphabet() {
		return writeInAlphabet;
	}

	public void setWriteInAlphabet(String writeInAlphabet) {
		this.writeInAlphabet = writeInAlphabet;
	}

}
