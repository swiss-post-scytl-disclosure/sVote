/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class SecureRandomIntegerException extends RuntimeException {

	private static final long serialVersionUID = 8612615573539948538L;

	public SecureRandomIntegerException(String message) {
		super(message);
	}
	
	public SecureRandomIntegerException(Throwable cause) {
		super(cause);
	}
	
	public SecureRandomIntegerException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
