/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

/**
 * Bean representing the public part of the key stored on the shares.
 */
public class SharesPublicKey {

    private String electoralAuthorityId;

    private String publicKey;

    public String getElectoralAuthorityId() {
        return electoralAuthorityId;
    }

    public void setElectoralAuthorityId(String electoralAuthorityId) {
        this.electoralAuthorityId = electoralAuthorityId;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

}
