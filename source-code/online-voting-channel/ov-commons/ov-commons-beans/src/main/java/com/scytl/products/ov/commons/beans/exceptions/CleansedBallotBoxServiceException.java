/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class CleansedBallotBoxServiceException extends Exception {

	private static final long serialVersionUID = 2658922365557470022L;

	public CleansedBallotBoxServiceException(Throwable cause) {
        super(cause);
    }

    public CleansedBallotBoxServiceException(String message) {
        super(message);
    }
    
    public CleansedBallotBoxServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
