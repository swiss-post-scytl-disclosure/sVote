/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.validation;

import java.util.List;

public class CertificateValidationResult {

	private boolean valid;

	private List<String> validationErrorMessages;

	/**
	 * Gets isValidated.
	 *
	 * @return Value of isValidated.
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Sets new validationErrorMessages.
	 *
	 * @param validationErrorMessages New value of validationErrorMessages.
	 */
	public void setValidationErrorMessages(List<String> validationErrorMessages) {
		this.validationErrorMessages = validationErrorMessages;
	}

	/**
	 * Sets new isValidated.
	 *
	 * @param valid New value of isValidated.
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * Gets validationErrorMessages.
	 *
	 * @return Value of validationErrorMessages.
	 */
	public List<String> getValidationErrorMessages() {
		return validationErrorMessages;
	}
}
