/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Encapsulates the information contained within a ballot.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ballot {

    private final String id;

    private final ElectionEvent electionEvent;

    private final List<Contest> contests;

    @JsonCreator
    public Ballot(@JsonProperty("id") final String id, @JsonProperty("electionEvent") final ElectionEvent electionEvent,
            @JsonProperty("contests") final List<Contest> contests) {

        this.id = id;
        this.electionEvent = electionEvent;
        this.contests = contests;
    }

    public String getId() {
        return id;
    }

    public ElectionEvent getElectionEvent() {
        return electionEvent;
    }

    public List<Contest> getContests() {
        return contests;
    }

    @JsonIgnore
    public boolean isEnriched() {
        for (Contest contest : getContests()) {
            for (ElectionOption electionOption : contest.getOptions()) {
                if (StringUtils.isEmpty(electionOption.getRepresentation())
                    || StringUtils.isBlank(electionOption.getRepresentation())) {
                    return false;
                }
            }
        }
        return true;
    }
}
