/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class VoteRepositoryException extends Exception {

	private static final long serialVersionUID = 608860693382896490L;

	public VoteRepositoryException(Throwable cause) {
        super(cause);
    }

    public VoteRepositoryException(String message) {
        super(message);
    }
    
    public VoteRepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
