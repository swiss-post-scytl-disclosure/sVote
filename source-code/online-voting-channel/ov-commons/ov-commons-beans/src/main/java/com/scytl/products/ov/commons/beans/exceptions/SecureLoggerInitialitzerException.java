/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class SecureLoggerInitialitzerException extends RuntimeException {

	private static final long serialVersionUID = 8992476170094741068L;

	public SecureLoggerInitialitzerException(String message) {
		super(message);
	}
	
	public SecureLoggerInitialitzerException(Throwable cause) {
		super(cause);
	}
	
	public SecureLoggerInitialitzerException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
