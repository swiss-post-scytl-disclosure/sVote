/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class VoterInformationRepositoryImplException extends RuntimeException {

	private static final long serialVersionUID = 8365942281410289817L;

	public VoterInformationRepositoryImplException(String message) {
		super(message);
	}
	
	public VoterInformationRepositoryImplException(Throwable cause) {
		super(cause);
	}
	
	public VoterInformationRepositoryImplException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
