/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.messaging;

/**
 * The signature of a payload could not be completed.
 */
public class PayloadSignatureException extends Exception {

    private static final long serialVersionUID = -6738655844708081827L;

    public PayloadSignatureException(Throwable cause) {
        super("The payload could not be signed", cause);
    }

    public PayloadSignatureException(String cause) {
        super(cause);
    }
}
