/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.vote;

import com.scytl.cryptolib.elgamal.cryptoapi.Ciphertext;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An encrypted vote created from a ciphertext.
 */
public class CiphertextEncryptedVote extends SerializableEncryptedVote {

    private static final long serialVersionUID = -4076480117841308514L;

    private final BigInteger gamma;

    private final List<BigInteger> phis;

    public CiphertextEncryptedVote(Ciphertext ciphertext) {
        gamma = ciphertext.getGamma().getValue();

        phis = ciphertext.getPhis().stream().map(ZpGroupElement::getValue)
                .collect(Collectors.toList());
    }

    public CiphertextEncryptedVote(BigInteger gamma, List<BigInteger> phis) {
        this.gamma = gamma;
        this.phis = phis;
    }

    @Override
    public BigInteger getGamma() {
        return gamma;
    }

    @Override
    public List<BigInteger> getPhis() {
        return Collections.unmodifiableList(phis);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((gamma == null) ? 0 : gamma.hashCode());
		result = prime * result + ((phis == null) ? 0 : phis.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CiphertextEncryptedVote other = (CiphertextEncryptedVote) obj;
		if (gamma == null) {
			if (other.gamma != null)
				return false;
		} else if (!gamma.equals(other.gamma))
			return false;
		if (phis == null) {
			if (other.phis != null)
				return false;
		} else if (!phis.equals(other.phis))
			return false;
		return true;
	}    
    
}
