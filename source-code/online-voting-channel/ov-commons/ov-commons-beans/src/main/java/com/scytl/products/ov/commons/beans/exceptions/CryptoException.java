/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class CryptoException  extends RuntimeException {

    private static final long serialVersionUID = 3831243369456177044L;

    public CryptoException(final String message) {
        super(message);
    }

    public CryptoException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CryptoException(final Throwable cause) {
        super(cause);
    }
}
