/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by riglesias on 16/03/15.
 */
package com.scytl.products.ov.commons.beans.domain.model.receipt;

/**
 * Class with the value of the receipt generated when a ballot is stored.
 */
public class Receipt {

    private String receipt;

    // The signature of the receipt (signed over receipt value + timestamp + election event id)
    private String signature;

    /**
     * Returns the current value of the field receipt.
     *
     * @return Returns the receipt.
     */
    public String getReceipt() {
        return receipt;
    }

    /**
     * Sets the value of the field receipt.
     *
     * @param receipt
     *            The receipt to set.
     */
    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    /**
     * Returns the current value of the field signature.
     *
     * @return Returns the signature.
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets the value of the field signature.
     *
     * @param signature
     *            The signature to set.
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }
}
