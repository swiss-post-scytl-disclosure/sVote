/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.messaging;

import com.scytl.products.ov.commons.beans.domain.model.vote.VoteSetId;

/**
 * The payload cannot be used as the signature and the contents do not match.
 */
public class InvalidSignatureException extends Exception {

    private static final long serialVersionUID = 7006873886931014849L;

    private static final String ERROR_MESSAGE_TEMPLATE =
        "The signature of payload for %s %s does not match its contents";

    public InvalidSignatureException(VoteSetId voteSetId) {
        this("vote set", voteSetId.toString());
    }

    public InvalidSignatureException(String entityName, String entityId) {
        super(String.format(ERROR_MESSAGE_TEMPLATE, entityName, entityId));
    }
}
