/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

/**
 * An object to represent election information. It will be serialized to a json file in order to be used by other modules.
 */
public class ElectionInformationContents {

	private String electionEventId;

	private String electionRootCA;

	private String servicesCA;

	private String credentialsCA;

	private String adminBoard;

	private String authoritiesCA;

	private ElectionInformationParams electionInformationParams;

	/**
	 * 
	 */
	public ElectionInformationContents() {
	}

	/**
	 * @return Returns the electionEventId.
	 */
	public String getElectionEventId() {
		return electionEventId;
	}

	/**
	 * @param electionEventId The electionEventId to set.
	 */
	public void setElectionEventId(final String electionEventId) {
		this.electionEventId = electionEventId;
	}

	/**
	 * @return Returns the electionRootCA.
	 */
	public String getElectionRootCA() {
		return electionRootCA;
	}

	/**
	 * @param electionRootCA The electionRootCA to set.
	 */
	public void setElectionRootCA(final String electionRootCA) {
		this.electionRootCA = electionRootCA;
	}

	/**
	 * @return Returns the servicesCA.
	 */
	public String getServicesCA() {
		return servicesCA;
	}

	/**
	 * @param servicesCA The servicesCA to set.
	 */
	public void setServicesCA(final String servicesCA) {
		this.servicesCA = servicesCA;
	}

	/**
	 * @return Returns the credentialsCA.
	 */
	public String getCredentialsCA() {
		return credentialsCA;
	}

	/**
	 * @param credentialsCA The credentialsCA to set.
	 */
	public void setCredentialsCA(final String credentialsCA) {
		this.credentialsCA = credentialsCA;
	}

	/**
	 * @return Returns the electionInformationParams.
	 */
	public ElectionInformationParams getElectionInformationParams() {
		return electionInformationParams;
	}

	/**
	 * @param electionInformationParams The electionInformationParams to set.
	 */
	public void setElectionInformationParams(final ElectionInformationParams electionInformationParams) {
		this.electionInformationParams = electionInformationParams;
	}

	/**
	 * @return Returns the adminBoard.
	 */
	public String getAdminBoard() {
		return adminBoard;
	}

	/**
	 * @param adminBoard The adminBoard to set.
	 */
	public void setAdminBoard(final String adminBoard) {
		this.adminBoard = adminBoard;
	}

	/**
	 * @return Returns the authoritiesCA.
	 */
	public String getAuthoritiesCA() {
		return authoritiesCA;
	}

	/**
	 * @param authoritiesCA The authoritiesCA to set.
	 */
	public void setAuthoritiesCA(final String authoritiesCA) {
		this.authoritiesCA = authoritiesCA;
	}

}
