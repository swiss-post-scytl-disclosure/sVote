/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class BlockedBallotBoxException extends Exception {

    private static final long serialVersionUID = 4789978202904548782L;
    
    // resource which provokes the expection
    private String resource;

    // error code of the exception
    private String errorCode;

    // field which provokes the exception
    private String field;

    /**
     * Constructs a new exception with the specified detail message.
     * 
     * @param message the detail message.
     */
    public BlockedBallotBoxException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message,resource and errorCode.
     *
     * @param message The detail message.
     * @param resource The resource which has provoked the exception.
     * @param errorCode The error code of the exception.
     * @param field The field affected by the error.
     */
    public BlockedBallotBoxException(String message, String resource, String errorCode, String field) {
        super(message);
        this.resource = resource;
        this.errorCode = errorCode;
        this.field = field;
    }

    /**
     * Gets the value of field resource.
     *
     * @return
     */
    public String getResource() {
        return resource;
    }

    /**
     * gets the value of field errorCode.
     * 
     * @return
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Return the field which provoked the Exception.
     * 
     * @return
     */
    public String getField() {
        return field;
    }

}
