/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

public class VerificationCardPublicKeyAndSignature {

    private String publicKey;

    private String signature;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(final String publicKey) {
    	this.publicKey = publicKey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
    	this.signature = signature;
    }

}
