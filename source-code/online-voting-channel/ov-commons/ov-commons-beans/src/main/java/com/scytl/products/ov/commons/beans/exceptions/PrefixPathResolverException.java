/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class PrefixPathResolverException extends RuntimeException {

	private static final long serialVersionUID = 4662448405865212162L;

	public PrefixPathResolverException(Throwable cause) {
        super(cause);
    }

    public PrefixPathResolverException(String message) {
        super(message);
    }
    
    public PrefixPathResolverException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
