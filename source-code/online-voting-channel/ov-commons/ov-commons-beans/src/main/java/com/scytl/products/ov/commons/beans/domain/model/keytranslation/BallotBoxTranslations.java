/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.keytranslation;

import java.util.List;

public class BallotBoxTranslations {

    private List<BallotBoxTranslation> ballotBoxTranslations;


    /**
     * Sets new electionEventTranslations.
     *
     * @param ballotBoxTranslations New value of electionEventTranslations.
     */
    public void setBallotBoxTranslations(List<BallotBoxTranslation> ballotBoxTranslations) {
        this.ballotBoxTranslations = ballotBoxTranslations;
    }

    /**
     * Gets electionEventTranslations.
     *
     * @return Value of electionEventTranslations.
     */
    public List<BallotBoxTranslation> getBallotBoxTranslations() {
        return ballotBoxTranslations;
    }
}
