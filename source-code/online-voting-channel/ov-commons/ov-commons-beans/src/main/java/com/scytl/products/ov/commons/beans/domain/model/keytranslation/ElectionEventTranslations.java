/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.keytranslation;

import java.util.List;

public class ElectionEventTranslations {

    private List<KeyTranslation> electionEventTranslations;


    /**
     * Sets new electionEventTranslations.
     *
     * @param electionEventTranslations New value of electionEventTranslations.
     */
    public void setElectionEventTranslations(List<KeyTranslation> electionEventTranslations) {
        this.electionEventTranslations = electionEventTranslations;
    }

    /**
     * Gets electionEventTranslations.
     *
     * @return Value of electionEventTranslations.
     */
    public List<KeyTranslation> getElectionEventTranslations() {
        return electionEventTranslations;
    }
}
