/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class SecureRandomStringException extends RuntimeException {

	private static final long serialVersionUID = -3794185233369189122L;

	public SecureRandomStringException(String message) {
		super(message);
	}
	
	public SecureRandomStringException(Throwable cause) {
		super(cause);
	}
	
	public SecureRandomStringException(String message, Throwable cause) {
		super(message, cause);
	}	
}
