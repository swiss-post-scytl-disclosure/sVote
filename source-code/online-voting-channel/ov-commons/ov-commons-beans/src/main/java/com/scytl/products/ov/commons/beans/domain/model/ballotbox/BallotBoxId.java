/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.ballotbox;

import java.io.Serializable;

/**
 * A reference to a ballot box.
 */
public interface BallotBoxId extends Serializable {

    /**
     * @return the identifier of the tenant that owns the election event
     */
    String getTenantId();

    /**
     * @return the identifier of the election event the ballot box belongs to
     */
    String getElectionEventId();

    /**
     * @return the unique identifier of the ballot box within its election event
     */
    String getId();
}
