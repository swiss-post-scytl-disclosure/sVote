/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class ConfirmationMessageMathematicalGroupValidationException extends RuntimeException {

	private static final long serialVersionUID = -4091024142745802797L;

	public ConfirmationMessageMathematicalGroupValidationException(Throwable cause) {
        super(cause);
    }

    public ConfirmationMessageMathematicalGroupValidationException(String message) {
        super(message);
    }
    
    public ConfirmationMessageMathematicalGroupValidationException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
}
