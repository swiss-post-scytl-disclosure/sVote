/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.messaging;

import java.util.HashMap;
import java.util.Map;

/**
 * Classes that are supported by MsgPack serialization/deserialization.
 */
public enum StreamSerializableClassType {

    KEY_CREATION_DTO("com.scytl.products.ov.commons.dto.KeyCreationDTO"),
    MIXING_DTO_IMPL("com.scytl.products.ov.commons.dto.MixingDTOImpl"),
    CHOICE_CODES_VERIFICATION_RES_PAYLOAD("com.scytl.products.ov.commons.dto.ChoiceCodesVerificationResPayload"),
    CHOICE_CODES_VERIFICATION_DECRYPT_RES_PAYLOAD("com.scytl.products.ov.commons.dto.ChoiceCodesVerificationDecryptResPayload"),
    CHOICE_CODE_GENERATION_RES_PAYLOAD("com.scytl.products.ov.commons.dto.ChoiceCodeGenerationResPayload"),
    CHOICE_CODE_GENERATION_REQ_PAYLOAD("com.scytl.products.ov.commons.dto.ChoiceCodeGenerationReqPayload"),
    CHOICE_CODE_VERIFICATION_DTO("com.scytl.products.ov.commons.dto.ChoiceCodeVerificationDTO"),
    CHOICE_CODE_GENERATION_DTO("com.scytl.products.ov.commons.dto.ChoiceCodeGenerationDTO"),
    PARTIAL_CODES_INPUT("com.scytl.products.ov.commons.dto.PartialCodesInput"),
    CC_PUBLIC_KEY("com.scytl.products.ov.commons.dto.CCPublicKey"),
    MIXING_PAYLOAD_IMPL("com.scytl.products.ov.commons.dto.mixing.MixingPayloadImpl");

    private String className;

    private static final Map<String, StreamSerializableClassType> map = new HashMap<>();

    static {
        // Index all entries by class name, so that valueOf() can find a class
        // name's element.
        for (StreamSerializableClassType classType : StreamSerializableClassType.values()) {
            map.put(classType.className, classType);
        }
    }

    private StreamSerializableClassType(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public static Class fromClassName(String className) throws ClassNotFoundException {
        StreamSerializableClassType classType = map.get(className);

        if (null == classType) {
            throw new IllegalArgumentException(
                    String.format("Class %s is not supported by the stream serializer", className));
        }

        return Class.forName(className);
    }
}
