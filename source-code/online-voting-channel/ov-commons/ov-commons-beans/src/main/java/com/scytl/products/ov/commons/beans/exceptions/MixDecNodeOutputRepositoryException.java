/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class MixDecNodeOutputRepositoryException extends Exception {

	private static final long serialVersionUID = 6828919932084028638L;

	public MixDecNodeOutputRepositoryException(Throwable cause) {
        super(cause);
    }

    public MixDecNodeOutputRepositoryException(String message) {
        super(message);
    }
    
    public MixDecNodeOutputRepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
