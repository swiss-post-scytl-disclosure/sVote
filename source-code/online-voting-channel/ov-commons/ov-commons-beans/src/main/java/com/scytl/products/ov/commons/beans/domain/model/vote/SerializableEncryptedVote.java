/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.vote;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * An encrypted vote that can be serialised (to allow it to move across components) and compared (to
 * allow sets thereof to be sorted for predictable signatures).
 */
public abstract class SerializableEncryptedVote implements EncryptedVote, Serializable {
    public static final String DELIMITER = ";";

    private static final long serialVersionUID = 8046142652960758759L;

    /**
     * Provides a standard representation for encrypted votes.
     *
     * @return the gamma and the phis, semi-colon separated.
     */
    @Override
    public String toString() {
        return String.join(DELIMITER, getGamma().toString(),
                getPhis().stream().map(BigInteger::toString)
                        .collect(Collectors.joining(DELIMITER)));
    }

    @Override
    public boolean equals(Object other) {
        if (null == other) {
            return false;
        }

        if (this == other) {
            return true;
        }

        if (other.getClass() != getClass()) {
            return false;
        }

        EncryptedVote otherEncryptedVote = (EncryptedVote) other;
        return getGamma().equals(otherEncryptedVote.getGamma()) && getPhis()
                .equals(otherEncryptedVote.getPhis());
    }

    @Override
    public int hashCode() {
        return Objects.hash(toString());
    }
}
