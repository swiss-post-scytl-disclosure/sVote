/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class VoteCastCodeRepositoryException extends Exception {

	private static final long serialVersionUID = -3420274252661357260L;

	public VoteCastCodeRepositoryException(Throwable cause) {
        super(cause);
    }

    public VoteCastCodeRepositoryException(String message) {
        super(message);
    }
    
    public VoteCastCodeRepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
