/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.tenant;

public class TenantActivationData {

    private String tenantID;

    private String systemKeystorePassword;

    public String getTenantID() {
        return tenantID;
    }

    public void setTenantID(final String tenantID) {
        this.tenantID = tenantID;
    }

    public String getSystemKeystorePassword() {
        return systemKeystorePassword;
    }

    public void setSystemKeystorePassword(final String systemKeystorePassword) {
        this.systemKeystorePassword = systemKeystorePassword;
    }
}
