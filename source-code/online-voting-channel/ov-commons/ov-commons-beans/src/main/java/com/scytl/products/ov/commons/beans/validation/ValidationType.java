/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.validation;

public enum ValidationType {

	TENANT_ACTIVATION, SECURE_LOGGER_INITIALIZATION, CONTEXT_ON_LINE
}
