/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.ballotbox;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Default implementation of a ballot box identifier.
 */
public class BallotBoxIdImpl implements BallotBoxId {

    private static final long serialVersionUID = 6368889285991914900L;

    private final String tenantId;

    private final String electionEventId;

    private final String id;

    @JsonCreator
    public BallotBoxIdImpl(@JsonProperty("tenantId") String tenantId,
            @JsonProperty("electionEventId") String electionEventId, @JsonProperty("ballotBoxId") String id) {
        this.tenantId = tenantId;
        this.electionEventId = electionEventId;
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("%s-%s-%s", tenantId, electionEventId, id);
    }

    @Override
    public String getTenantId() {
        return tenantId;
    }

    @Override
    public String getElectionEventId() {
        return electionEventId;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object other) {
        if (null == other) {
            return false;
        }

        if (!(other instanceof BallotBoxId)) {
            return false;
        }

        return hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = (31 * result) + tenantId.hashCode();
        result = (31 * result) + electionEventId.hashCode();
        result = (31 * result) + id.hashCode();

        return result;
    }
}
