/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.messaging;

import java.io.Serializable;
import java.security.cert.X509Certificate;

/**
 * Represents a signature for a payload.
 */
public interface PayloadSignature extends Serializable{

    /**
     * Gets a certificate chain whose last element is the public key used to validate the signature.
     *
     * @return the certificate chain
     */
    X509Certificate[] getCertificateChain();

    /**
     * @return the byte array representing the signature
     */
    byte[] getSignatureContents();
}