/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

public class VoteVerificationContextData {

    private String codesSecretKeyKeyStore;

    private String codesSecretKeyPassword;

    private String electoralAuthorityId;

    private String electionEventId;

    private String verificationCardSetId;

    private EncryptionParameters encryptionParameters;

    private String nonCombinedChoiceCodesEncryptionPublicKeys;

    public String getCodesSecretKeyKeyStore() {
        return codesSecretKeyKeyStore;
    }

    public void setCodesSecretKeyKeyStore(final String codesSecretKeyKeyStore) {
    	this.codesSecretKeyKeyStore = codesSecretKeyKeyStore;
    }

    public String getCodesSecretKeyPassword() {
        return codesSecretKeyPassword;
    }

    public void setCodesSecretKeyPassword(final String codesSecretKeyPassword) {
    	this.codesSecretKeyPassword = codesSecretKeyPassword;
    }

    public String getElectoralAuthorityId() {
        return electoralAuthorityId;
    }

    public void setElectoralAuthorityId(String electoralAuthorityId) {
    	this.electoralAuthorityId = electoralAuthorityId;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public void setElectionEventId(final String electionEventId) {
    	this.electionEventId = electionEventId;
    }

    public String getVerificationCardSetId() {
        return verificationCardSetId;
    }

    public void setVerificationCardSetId(final String verificationCardSetId) {
    	this.verificationCardSetId = verificationCardSetId;
    }

    public EncryptionParameters getEncryptionParameters() {
        return encryptionParameters;
    }

    public void setEncryptionParameters(final EncryptionParameters encryptionParameters) {
        this.encryptionParameters = encryptionParameters;
    }

    public String getNonCombinedChoiceCodesEncryptionPublicKeys() {
        return nonCombinedChoiceCodesEncryptionPublicKeys;
    }

    public void setNonCombinedChoiceCodesEncryptionPublicKeys(String nonCombinedChoiceCodesEncryptionPublicKeys) {
    	this.nonCombinedChoiceCodesEncryptionPublicKeys = nonCombinedChoiceCodesEncryptionPublicKeys;
    }
}
