/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.keytranslation;

public class BallotBoxTranslation extends KeyTranslation {

    private String ballotBoxId;



    /**
     * Gets BallotBoxId.
     *
     * @return Value of tenantId.
     */
    public String getBallotBoxId() {
        return ballotBoxId;
    }


    /**
     * Sets new BallotBoxId.
     *
     * @param ballotBoxId New value of BallotBoxId.
     */
    public void setBallotBoxId(String ballotBoxId) {
        this.ballotBoxId = ballotBoxId;
    }

}
