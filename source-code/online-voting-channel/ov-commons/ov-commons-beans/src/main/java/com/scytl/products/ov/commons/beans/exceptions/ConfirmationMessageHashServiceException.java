/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

/**
 * Confirmation Message Hash Service Exception
 */
public class ConfirmationMessageHashServiceException extends RuntimeException {
    
	private static final long serialVersionUID = 4194795434117283777L;

	public ConfirmationMessageHashServiceException(final Throwable cause) {
        super(cause);
    }

	public ConfirmationMessageHashServiceException(String message) {
        super(message);
    }

    public ConfirmationMessageHashServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
