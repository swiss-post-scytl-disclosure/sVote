/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class BallotBoxTranslationRepositoryException extends Exception {

	private static final long serialVersionUID = 7903825884620342281L;

	public BallotBoxTranslationRepositoryException(Throwable cause) {
        super(cause);
    }

    public BallotBoxTranslationRepositoryException(String message) {
        super(message);
    }
    
    public BallotBoxTranslationRepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
