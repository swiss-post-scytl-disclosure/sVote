/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.confirmation;

/**
 * The class representing the cast code message
 */
public class TraceableConfirmationMessage extends ConfirmationMessage {

    private String authenticationTokenSignature;

    private String votingCardId;

    public String getAuthenticationTokenSignature() {
        return authenticationTokenSignature;
    }

    public void setAuthenticationTokenSignature(String authenticationTokenSignature) {
        this.authenticationTokenSignature = authenticationTokenSignature;
    }

    public String getVotingCardId() {
        return votingCardId;
    }

    public void setVotingCardId(String votingCardId) {
        this.votingCardId = votingCardId;
    }

}
