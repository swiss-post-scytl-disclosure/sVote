/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 3/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.commons.beans.domain.model.authentication;


/**
 * Class representing the authentication information for an election event
 */
public class AuthenticationData {

    private AuthenticationContent authenticationContent;

    private String certificates;


    /**
     * Gets authenticationContent.
     *
     * @return Value of authenticationContent.
     */
    public AuthenticationContent getAuthenticationContent() {
        return authenticationContent;
    }

    /**
     * Sets new certificates.
     *
     * @param certificates New value of certificates.
     */
    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    /**
     * Sets new authenticationContent.
     *
     * @param authenticationContent New value of authenticationContent.
     */
    public void setAuthenticationContent(AuthenticationContent authenticationContent) {
        this.authenticationContent = authenticationContent;
    }

    /**
     * Gets certificates.
     *
     * @return Value of certificates.
     */
    public String getCertificates() {
        return certificates;
    }
}
