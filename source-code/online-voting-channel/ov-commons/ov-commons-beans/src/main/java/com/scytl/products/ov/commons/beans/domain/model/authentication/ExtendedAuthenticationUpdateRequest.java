/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.authentication;

public class ExtendedAuthenticationUpdateRequest {

    private String signature;

    private String certificate;

    /**
     * Gets certificate.
     *
     * @return Value of certificate.
     */
    public String getCertificate() {
        return certificate;
    }

    /**
     * Gets signature.
     *
     * @return Value of signature.
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets new signature.
     *
     * @param signature
     *            New value of signature.
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * Sets new certificate.
     *
     * @param certificate
     *            New value of certificate.
     */
    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }
}
