/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.tenant;

/**
 * Encapsulates a single piece of data.
 */
public class TenantInstallationData {

    private String encodedData;

    /**
     * Gets the value of the encoded data
     *
     * @return
     */
    public String getEncodedData() {
        return encodedData;
    }

    /**
     * Sets a new value for the encoded data
     *
     * @param encodedData
     */
    public void setEncodedData(final String encodedData) {
        this.encodedData = encodedData;
    }
}
