/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 29/04/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.commons.beans.exceptions;

/**
 * Authentication Token Repository Exception
 */
public class AuthTokenRepositoryException extends Exception {
    
	private static final long serialVersionUID = -7600894241581995072L;

    public AuthTokenRepositoryException(final Throwable cause) {
        super(cause);
    }

	public AuthTokenRepositoryException(String message) {
        super(message);
    }

    public AuthTokenRepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
