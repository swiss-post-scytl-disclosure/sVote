/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.compute;

import java.util.Map;

public class ComputeInput {

    private String encryptedRepresentations;

    private Map<String, String> verificationCardIdsToBallotCastingKeys;

    public String getEncryptedRepresentations() {
        return encryptedRepresentations;
    }

    public void setEncryptedRepresentations(String encryptedRepresentations) {
        this.encryptedRepresentations = encryptedRepresentations;
    }

    public Map<String, String> getVerificationCardIdsToBallotCastingKeys() {
        return verificationCardIdsToBallotCastingKeys;
    }

    public void setVerificationCardIdsToBallotCastingKeys(
            Map<String, String> verificationCardIdsToBallotCastingKeys) {
        this.verificationCardIdsToBallotCastingKeys = verificationCardIdsToBallotCastingKeys;
    }
}
