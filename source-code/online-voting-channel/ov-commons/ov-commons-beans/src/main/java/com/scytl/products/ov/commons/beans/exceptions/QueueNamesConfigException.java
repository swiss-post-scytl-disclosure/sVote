/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class QueueNamesConfigException extends RuntimeException {
	
	private static final long serialVersionUID = 6298788495270219324L;

	public QueueNamesConfigException(Throwable cause) {
        super(cause);
    }

    public QueueNamesConfigException(String message) {
        super(message);
    }
    
    public QueueNamesConfigException(final String message, final Throwable cause) {
        super(message, cause);
    }
   
}
