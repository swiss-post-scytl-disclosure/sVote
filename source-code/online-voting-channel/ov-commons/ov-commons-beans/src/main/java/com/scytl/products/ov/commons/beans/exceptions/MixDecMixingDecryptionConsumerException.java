/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class MixDecMixingDecryptionConsumerException extends RuntimeException {

	private static final long serialVersionUID = 7580333411370708054L;

	public MixDecMixingDecryptionConsumerException(String message) {
		super(message);
	}
	
	public MixDecMixingDecryptionConsumerException(Throwable cause) {
		super(cause);
	}
	
	public MixDecMixingDecryptionConsumerException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
