/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class SecureRandomBytesException extends RuntimeException {

	private static final long serialVersionUID = -6953162503070021322L;

	public SecureRandomBytesException(String message) {
		super(message);
	}
	
	public SecureRandomBytesException(Throwable cause) {
		super(cause);
	}
	
	public SecureRandomBytesException(String message, Throwable cause) {
		super(message, cause);
	}	
}
