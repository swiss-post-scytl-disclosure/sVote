/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class ConfigObjectMapperException extends RuntimeException {

	private static final long serialVersionUID = -770251491690246586L;

	public ConfigObjectMapperException(Throwable cause) {
        super(cause);
    }

    public ConfigObjectMapperException(String message) {
        super(message);
    }
    
    public ConfigObjectMapperException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
