/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.compute;

import java.util.ArrayList;
import java.util.List;

public class VerificationCardIdToNodesContributions {

    private String verificationCardId;

    private List<String> optionRepresentationsContributions = new ArrayList<>();
    
    private String encryptedBallotCastingKey;

    private List<String> ballotCastingKeyContributions = new ArrayList<>();
    
    private List<String> derivedKeys = new ArrayList<>();

    public String getVerificationCardId() {
        return verificationCardId;
    }

    public void setVerificationCardId(String verificationCardId) {
        this.verificationCardId = verificationCardId;
    }

    public void addOptionRepresentationContribution(String optionRepresentationContribution) {
        this.optionRepresentationsContributions.add(optionRepresentationContribution);
    }

    public String getOptionRepresentationContribution(int index) {
        return optionRepresentationsContributions.get(index);
    }

    public List<String> getOptionRepresentationsContributions() {
        return optionRepresentationsContributions;
    }

    public List<String> getBallotCastingKeyContributions() {
        return ballotCastingKeyContributions;
    }

    public void setBallotCastingKeyContributions(List<String> ballotCastingKeyContributions) {
        this.ballotCastingKeyContributions = ballotCastingKeyContributions;
    }

    public List<String> getDerivedKeys() {
        return derivedKeys;
    }

    public void setDerivedKeys(List<String> derivedKeys) {
        this.derivedKeys = derivedKeys;
    }
    
    public String getEncryptedBallotCastingKey() {
        return encryptedBallotCastingKey;
    }
    
    public void setEncryptedBallotCastingKey(
            String encryptedBallotCastingKey) {
        this.encryptedBallotCastingKey = encryptedBallotCastingKey;
    }

}
