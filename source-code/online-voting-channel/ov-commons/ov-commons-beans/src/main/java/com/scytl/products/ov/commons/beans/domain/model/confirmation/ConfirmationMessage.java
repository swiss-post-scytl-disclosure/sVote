/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.confirmation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Class representing a confirmation message
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmationMessage {

    // confirmation key calculated in the client side
    private String confirmationKey;

    // signature of the confirmation key
    private String signature;

    /**
     * Gets confirmationKey.
     *
     * @return Value of confirmationKey.
     */
    public String getConfirmationKey() {
        return confirmationKey;
    }

    /**
     * Sets new confirmationKey.
     *
     * @param confirmationKey
     *            New value of confirmationKey.
     */
    public void setConfirmationKey(String confirmationKey) {
        this.confirmationKey = confirmationKey;
    }

    /**
     * Sets new signature.
     *
     * @param signature
     *            New value of signature.
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * Gets signature.
     *
     * @return Value of signature.
     */
    public String getSignature() {
        return signature;
    }
}
