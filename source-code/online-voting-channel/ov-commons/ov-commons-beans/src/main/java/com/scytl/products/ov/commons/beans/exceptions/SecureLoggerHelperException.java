/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class SecureLoggerHelperException extends RuntimeException {

	private static final long serialVersionUID = -5821181520584306193L;

	public SecureLoggerHelperException(String message) {
		super(message);
	}
	
	public SecureLoggerHelperException(Throwable cause) {
		super(cause);
	}
	
	public SecureLoggerHelperException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
