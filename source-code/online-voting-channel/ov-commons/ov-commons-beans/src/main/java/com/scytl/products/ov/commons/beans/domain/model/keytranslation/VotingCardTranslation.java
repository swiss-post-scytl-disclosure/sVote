/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.keytranslation;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity that contains the information about the election event translation.
 */
public class VotingCardTranslation {

	/**
	 * The identifier of this election event.
	 */
	private Integer id;

	/**
	 * The provided identifier.
	 */
	private String alias;

	/**
	 * The identifier of the tenant.
	 */
	private String tenantId;

	/**
	 * The identifier of the election event.
	 */
	private String electionEventId;

	/**
	 * The identifier of the voting card.
	 */
	private String votingCardId;

	/**
	 * The alias of the voting card set.
	 */
	private String votingCardSetAlias;

	/**
	 * The alias of the verification card set.
	 */
	private String verificationCardId;

	/**
	 * Returns the current value of the field id.
	 *
	 * @return Returns the id.
	 */
	@JsonIgnore
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the value of the field id.
	 *
	 * @param id The id to set.
	 */
	@JsonIgnore
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Returns the current value of the field alias.
	 *
	 * @return Returns the alias.
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the value of the field alias.
	 *
	 * @param alias The alias to set.
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Returns the current value of the field tenantId.
	 *
	 * @return Returns the tenantId.
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Sets the value of the field tenantId.
	 *
	 * @param tenantId The tenantId to set.
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * Returns the current value of the field electionEventId.
	 *
	 * @return Returns the electionEventId.
	 */
	public String getElectionEventId() {
		return electionEventId;
	}

	/**
	 * Sets the value of the field electionEventId.
	 *
	 * @param electionEventId The electionEventId to set.
	 */
	public void setElectionEventId(String electionEventId) {
		this.electionEventId = electionEventId;
	}

	/**
	 * Returns the current value of the field votingCardId.
	 *
	 * @return Returns the votingCardId.
	 */
	public String getVotingCardId() {
		return votingCardId;
	}

	/**
	 * Sets the value of the field votingCardId.
	 *
	 * @param votingCardId The votingCardId to set.
	 */
	public void setVotingCardId(String votingCardId) {
		this.votingCardId = votingCardId;
	}

	/**
	 * Returns the current value of the field votingCardSetAlias.
	 *
	 * @return Returns the votingCardSetAlias.
	 */
	@JsonIgnore
	public String getVotingCardSetAlias() {
		return votingCardSetAlias;
	}

	/**
	 * Sets the value of the field votingCardSetAlias.
	 *
	 * @param votingCardSetAlias The votingCardSetAlias to set.
	 */
	public void setVotingCardSetAlias(String votingCardSetAlias) {
		this.votingCardSetAlias = votingCardSetAlias;
	}

	/**
	 * Returns the current value of the field verificationCardId.
	 *
	 * @return Returns the verificationCardId.
	 */
	@JsonIgnore
	public String getVerificationCardId() {
		return verificationCardId;
	}

	/**
	 * Sets the value of the field verificationCardId.
	 *
	 * @param verificationCardId The verificationCardId to set.
	 */
	public void setVerificationCardId(String verificationCardId) {
		this.verificationCardId = verificationCardId;
	}

}