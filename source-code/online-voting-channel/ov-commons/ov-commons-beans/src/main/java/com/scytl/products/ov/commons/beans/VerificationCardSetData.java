/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

public class VerificationCardSetData {

    private String verificationCardSetId;

    private String choicesCodesEncryptionPublicKey;

    private String verificationCardIssuerCert;

    private String voteCastCodeSignerCert;

    private String electionEventId;

    public String getVerificationCardSetId() {
        return verificationCardSetId;
    }

    public void setVerificationCardSetId(final String verificationCardSetId) {
    	this.verificationCardSetId = verificationCardSetId;
    }

    public String getChoicesCodesEncryptionPublicKey() {
        return choicesCodesEncryptionPublicKey;
    }

    public void setChoicesCodesEncryptionPublicKey(final String choicesCodesEncryptionPublicKey) {
    	this.choicesCodesEncryptionPublicKey = choicesCodesEncryptionPublicKey;
    }

    public String getVerificationCardIssuerCert() {
        return verificationCardIssuerCert;
    }

    public void setVerificationCardIssuerCert(final String verificationCardIssuerCert) {
    	this.verificationCardIssuerCert = verificationCardIssuerCert;
    }

    public String getVoteCastCodeSignerCert() {
        return voteCastCodeSignerCert;
    }

    public void setVoteCastCodeSignerCert(String voteCastCodeSignerCert) {
    	this.voteCastCodeSignerCert = voteCastCodeSignerCert;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public void setElectionEventId(final String electionEventId) {
        this.electionEventId = electionEventId;
    }

}
