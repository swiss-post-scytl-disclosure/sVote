/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.messaging;

/**
 * The signature of a payload could not be verified.
 */
public class PayloadVerificationException extends Exception {

    private static final long serialVersionUID = 5003672338761264586L;

    public PayloadVerificationException(Throwable cause) {
        super("The payload could not be verified", cause);
    }
}
