/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

/**
 * Bean inside the BallotBox class, representing information about the electoral authority.
 */
public class ElectoralAuthority {

	private String id;

	private String publicKey;

	/**
	 * @return Returns the id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id The id to set.
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * @return Returns the publicKey.
	 */
	public String getPublicKey() {
		return publicKey;
	}

	/**
	 * @param publicKey The publicKey to set.
	 */
	public void setPublicKey(final String publicKey) {
		this.publicKey = publicKey;
	}

}
