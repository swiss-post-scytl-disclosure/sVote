/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class CleansedBallotBoxRepositoryException extends Exception {

	private static final long serialVersionUID = -990713457163526110L;

	public CleansedBallotBoxRepositoryException(Throwable cause) {
        super(cause);
    }

    public CleansedBallotBoxRepositoryException(String message) {
        super(message);
    }
    
    public CleansedBallotBoxRepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
