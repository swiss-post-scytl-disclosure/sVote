/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.keytranslation;

public class KeyTranslation {

    private String tenantId;

    private String electionEventId;

    private String alias;


    /**
     * Gets tenantId.
     *
     * @return Value of tenantId.
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * Gets electionEventId.
     *
     * @return Value of electionEventId.
     */
    public String getElectionEventId() {
        return electionEventId;
    }

    /**
     * Sets new electionEventId.
     *
     * @param electionEventId New value of electionEventId.
     */
    public void setElectionEventId(String electionEventId) {
        this.electionEventId = electionEventId;
    }

    /**
     * Sets new tenantId.
     *
     * @param tenantId New value of tenantId.
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * Sets new alias.
     *
     * @param alias New value of alias.
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * Gets alias.
     *
     * @return Value of alias.
     */
    public String getAlias() {
        return alias;
    }
}
