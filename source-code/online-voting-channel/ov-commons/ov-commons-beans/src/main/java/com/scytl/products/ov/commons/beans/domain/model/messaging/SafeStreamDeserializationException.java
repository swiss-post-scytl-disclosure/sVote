/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.messaging;

/**
 * An error occurred when trying to deserialize an object serialized with SafeStream.
 */
public class SafeStreamDeserializationException extends Exception {

    private static final long serialVersionUID = -140165307911190422L;

    public SafeStreamDeserializationException(Throwable cause) {
        super(cause);
    }
}