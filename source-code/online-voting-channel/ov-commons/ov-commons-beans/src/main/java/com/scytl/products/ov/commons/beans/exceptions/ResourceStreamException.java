/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class ResourceStreamException extends RuntimeException {

	private static final long serialVersionUID = 2058471223984941357L;

	public ResourceStreamException(Throwable cause) {
        super(cause);
    }

    public ResourceStreamException(String message) {
        super(message);
    }
    
    public ResourceStreamException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
