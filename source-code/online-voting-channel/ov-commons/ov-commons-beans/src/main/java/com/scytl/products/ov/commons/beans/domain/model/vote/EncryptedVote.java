/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.vote;

import java.math.BigInteger;
import java.util.List;

/**
 * Simplified representation of an encrypted vote, consisting of a big integer representing the
 * gamma, and a set of big integers representing the phis.
 */
public interface EncryptedVote {
    /**
     * @return the encrypted vote's gamma
     */
    BigInteger getGamma();

    /**
     * @return the encrypted vote's phis
     */
    List<BigInteger> getPhis();
}
