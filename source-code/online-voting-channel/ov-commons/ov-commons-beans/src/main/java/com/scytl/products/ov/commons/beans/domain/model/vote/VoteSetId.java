/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.vote;

import com.scytl.products.ov.commons.beans.domain.model.ballotbox.BallotBoxId;
import java.io.Serializable;

/**
 * A reference to a set of votes in a ballot box. As a side note, it may be worth noting that a vote
 * set ID is not always a reflection of a set of votes in a ballot box, but merely a pointer to
 * where the votes originally came from.
 */
public interface VoteSetId extends Serializable {

    /**
     * @return a reference to the ballot box the votes come from
     */
    BallotBoxId getBallotBoxId();

    /**
     * Which of the ballot box divisions this vote set represents. The size of such ballot box
     * divisions is determined by {@code VoteSet.MAX_SIZE}.
     *
     * @return the index of the vote set
     */
    int getIndex();
}
