/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.constants;

/**
 * Contains all the "magic" numbers used in the model.
 */
public final class Constants {

	/**
	 * The column length 50.
	 */
	public static final int COLUMN_LENGTH_50 = 50;

	/**
	 * The column length 100.
	 */
	public static final int COLUMN_LENGTH_100 = 100;

	/**
	 * The column length 150.
	 */
	public static final int COLUMN_LENGTH_150 = 150;

	// Avoid instantiation.
	private Constants() {
	}
}
