/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 29/04/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.commons.beans.exceptions;

import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;

/**
 * Authentication Token Validation Exception
 */
public class ExtendedAuthValidationException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    private final ValidationErrorType errorType;

    public ExtendedAuthValidationException(final ValidationErrorType errorType) {
        this.errorType = errorType;
    }

    public ExtendedAuthValidationException(final ValidationErrorType errorType, Throwable t) {
        super(t);
        this.errorType = errorType;

    }

    public ValidationErrorType getErrorType() {
        return errorType;
    }
}
