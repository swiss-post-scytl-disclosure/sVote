/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 29/04/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.commons.beans.exceptions;

import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;

/**
 * Authentication Token Validation Exception
 */
public class AuthTokenValidationException extends RuntimeException {

    private static final long serialVersionUID = 3045584269314820919L;

    private final ValidationErrorType errorType;

    public AuthTokenValidationException(final ValidationErrorType errorType) {
        this.errorType = errorType;
    }

    public AuthTokenValidationException(final ValidationErrorType errorType, String message) {
        super(message);
        this.errorType = errorType;
    }

    public AuthTokenValidationException(final ValidationErrorType errorType, final Throwable throwable) {
        super(throwable);
        this.errorType = errorType;
    }

    public ValidationErrorType getErrorType() {
        return errorType;
    }
}
