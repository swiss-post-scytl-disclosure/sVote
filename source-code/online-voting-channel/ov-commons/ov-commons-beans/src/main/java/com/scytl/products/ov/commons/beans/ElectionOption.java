/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Encapsulates the information related to an election option.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ElectionOption {

    private final String id;

    private String representation;

    private final String attribute;

    @JsonCreator
    public ElectionOption(@JsonProperty("id") final String id,
            @JsonProperty("representation") final String representation,
            @JsonProperty("attribute") final String attribute) {
        this.id = id;
        this.representation = representation;
        this.attribute = attribute;
    }

    public String getId() {
        return this.id;
    }

    public void setRepresentation(final String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return this.representation;
    }

    public boolean hasRepresentation() {
        return (representation != null) && (representation.length() > 0);
    }

    public String getAttribute() {
        return this.attribute;
    }

}
