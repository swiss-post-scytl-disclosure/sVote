/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

/**
 * Exception to be catched after invoking a lambda expression.
 */
public class LambdaException extends RuntimeException {

	private static final long serialVersionUID = 1347248014980109621L;

	// resource which provokes the exception
	private final Exception cause;

	/**
	 * Constructs a new lambda exception with the specified exception as cause.
	 * 
	 * @param cause - the exception which is cause of this lambda exception.
	 */
	public LambdaException(Exception cause) {
		super();
		this.cause = cause;
	}

	/**
	 * Returns the current value of the field cause.
	 *
	 * @return Returns the cause.
	 */
	@Override
	public Exception getCause() {
		return cause;
	}

}
