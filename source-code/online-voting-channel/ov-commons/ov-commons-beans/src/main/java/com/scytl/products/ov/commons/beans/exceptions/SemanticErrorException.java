/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

import java.util.Set;

import javax.validation.ConstraintViolation;

/**
 * Exception class that can be thrown when there are validation exceptions.
 */
public class SemanticErrorException extends ValidationException {

	private static final long serialVersionUID = 5230615489167778810L;

	/**
	 * Constructor for semantic errors.
	 * 
	 * @param constraintViolations - the list of constraints violated.
	 */
	public SemanticErrorException(Set<? extends ConstraintViolation<?>> constraintViolations) {
		super(constraintViolations);
	}
}
