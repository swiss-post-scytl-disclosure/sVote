/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.platform;

/**
 * Used to represent the state of whether the logging system has been initialized or not.
 */
public class LoggingInitializationState {

    private boolean initialized;

    /**
     * Sets the current logging state to the received value.
     *
     * @param initialized
     *            the value to set the logging state to.
     */
    public void setInitialized(final boolean initialized) {
    	this.initialized = initialized;
    }

    /**
     * @return the current logging state.
     */
    public boolean getInitialized() {
        return initialized;
    }
}
