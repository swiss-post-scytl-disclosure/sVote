/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.exceptions;

public class AbstractKeystoreReaderException extends RuntimeException {

	private static final long serialVersionUID = 8214259381737828625L;

	public AbstractKeystoreReaderException(String message) {
		super(message);
	}
	
	public AbstractKeystoreReaderException(Throwable cause) {
		super(cause);
	}
	
	public AbstractKeystoreReaderException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
