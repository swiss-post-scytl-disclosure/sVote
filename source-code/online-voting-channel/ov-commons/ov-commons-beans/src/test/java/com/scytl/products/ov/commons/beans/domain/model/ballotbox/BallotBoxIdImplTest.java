/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.ballotbox;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BallotBoxIdImplTest {
    @Test
    public void considerEqualTwoInstanceWithTheSameValues() {
        BallotBoxId one = new BallotBoxIdImpl("1", "2", "3");
        BallotBoxId another = new BallotBoxIdImpl("1", "2", "3");

        // Ensure the objects are different.
        assertTrue(one != another);
        // Ensure the objects are considered to hold the same values.
        assertEquals(one.hashCode(), another.hashCode());
        assertTrue(one.equals(another));
    }
}
