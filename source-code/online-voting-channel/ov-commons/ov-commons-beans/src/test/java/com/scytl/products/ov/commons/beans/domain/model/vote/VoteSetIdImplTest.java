/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.vote;

import com.scytl.products.ov.commons.beans.domain.model.ballotbox.BallotBoxId;
import com.scytl.products.ov.commons.beans.domain.model.ballotbox.BallotBoxIdImpl;
import com.scytl.products.ov.commons.beans.domain.model.vote.VoteSetIdImpl;
import com.scytl.products.ov.commons.beans.domain.model.vote.VoteSetId;
import static org.junit.Assert.*;

import org.junit.Test;

public class VoteSetIdImplTest {
    @Test
    public void considerEqualTwoInstanceWithTheSameValues() {
        BallotBoxId ballotBoxId = new BallotBoxIdImpl("1", "2", "3");
        VoteSetId one = new VoteSetIdImpl(ballotBoxId, 0);
        VoteSetId same = new VoteSetIdImpl(ballotBoxId, 0);
        VoteSetId different = new VoteSetIdImpl(ballotBoxId, 1);

        // Ensure the objects are different.
        assertTrue(one != same);
        assertTrue(one != different);
        // Ensure objects referring to the same data set are considered equal.
        assertEquals(one.hashCode(), same.hashCode());
        assertNotEquals(one.hashCode(), different.hashCode());
        assertTrue(one.equals(same));
        assertFalse(one.equals(different));
    }
}
