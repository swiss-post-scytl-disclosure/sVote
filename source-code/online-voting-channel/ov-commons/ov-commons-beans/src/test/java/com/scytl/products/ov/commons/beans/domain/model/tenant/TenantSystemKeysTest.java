/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans.domain.model.tenant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TenantSystemKeysTest {

    @Test
    public void whenGetTenantThatDoesNotExistThenFalse() {

        TenantSystemKeys tenantSystemKeys = new TenantSystemKeys();

        String errorMsg = "Expected false because tenant does not exist";
        assertFalse(errorMsg, tenantSystemKeys.getInitialized("XXXXXXXXX"));
    }

    @Test
    public void whenGetTenantThatExistsThenTrue() {

        TenantSystemKeys tenantSystemKeys = new TenantSystemKeys();

        String tenantID = "100";
        Map<String, PrivateKey> privateKeys100 = new HashMap<>();

        tenantSystemKeys.setInitialized(tenantID, privateKeys100);

        String errorMsg = "Expected true because tenant exists";
        assertTrue(errorMsg, tenantSystemKeys.getInitialized(tenantID));
    }

    @Test
    public void whenGetTwosTenantThatExistThenTrue() {

        TenantSystemKeys tenantSystemKeys = new TenantSystemKeys();

        String tenant100ID = "100";
        Map<String, PrivateKey> privateKeys100 = new HashMap<>();

        String tenant200ID = "200";
        Map<String, PrivateKey> privateKeys200 = new HashMap<>();

        tenantSystemKeys.setInitialized(tenant100ID, privateKeys100);
        tenantSystemKeys.setInitialized(tenant200ID, privateKeys200);

        String errorMsg = "Expected true because tenant exists";
        assertTrue(errorMsg, tenantSystemKeys.getInitialized(tenant100ID));
        assertTrue(errorMsg, tenantSystemKeys.getInitialized(tenant200ID));
    }

    @Test
    public void whenInvalidateTenantThenItNoLongerRemoved() {

        TenantSystemKeys tenantSystemKeys = new TenantSystemKeys();

        String tenantID = "100";
        Map<String, PrivateKey> privateKeys100 = new HashMap<>();

        tenantSystemKeys.setInitialized(tenantID, privateKeys100);

        String errorMsg = "Expected true because tenant exists";
        assertTrue(errorMsg, tenantSystemKeys.getInitialized(tenantID));

        tenantSystemKeys.invalidate(tenantID);
        errorMsg = "Expected false because tenant was invalidated";
        assertFalse(errorMsg, tenantSystemKeys.getInitialized(tenantID));
    }
}
