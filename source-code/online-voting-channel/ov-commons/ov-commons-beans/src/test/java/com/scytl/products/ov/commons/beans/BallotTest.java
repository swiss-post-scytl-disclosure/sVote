/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.beans;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.junit.Test;

public class BallotTest {

    private final ObjectMapper _mapper = new ObjectMapper();

    @Test
    public void be_created_from_a_json_containing_more_fields_than_expected() throws IOException {

        final File jsonFile = Paths.get("src/test/resources", "l_c_ballot.json").toFile();

        final Ballot ballot = _mapper.readerFor(Ballot.class).readValue(jsonFile);

        assertThat(ballot, notNullValue());
    }

    @Test
    public void contain_defined_clauses() throws IOException {

        final File jsonFile = Paths.get("src/test/resources", "l_c_ballot.json").toFile();

      final Ballot ballot = _mapper.readerFor(Ballot.class).readValue(jsonFile);

        assertThat(ballot.getContests().get(0).getAttributes(), notNullValue());
    }

    @Test
    public void contain_defined_correctness_rule() throws IOException {

        final File jsonFile = Paths.get("src/test/resources", "l_c_ballot.json").toFile();

      final Ballot ballot = _mapper.readerFor(Ballot.class).readValue(jsonFile);

        assertThat(ballot.getContests().get(0).getDecryptedCorrectnessRule(), notNullValue());
    }
}
