/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.application;

/**
 * Exception class for handling a duplicate entry in database.
 *
 * @deprecated replaced by  {@link com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException}
 */
@Deprecated
public class DuplicateEntryException extends Exception {

	private static final long serialVersionUID = -4185830769944104259L;

	// resource which provokes the exception
	private String resource;

	// error code of the exception
	private String errorCode;

	/**
	 * Constructs a new exception with the specified detail message.
	 *
	 * @param message the detail message.
	 */
	public DuplicateEntryException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 *
	 * @param message the detail message.
	 * @param cause the cause of the exception.
	 */
	public DuplicateEntryException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new exception with the specified detail message, resource and errorCode.
	 *
	 * @param message The detail message.
	 * @param resource The resource which has provoked the exception.
	 * @param errorCode The error code of the exception.
	 */
	public DuplicateEntryException(String message, String resource, String errorCode) {
		super(message);
		this.resource = resource;
		this.errorCode = errorCode;
	}

	/**
	 * Gets the value of field resource.
	 *
	 * @return the resource.
	 */
	public String getResource() {
		return resource;
	}

	/**
	 * gets the value of field errorCode.
	 *
	 * @return the field error Code
	 */
	public String getErrorCode() {
		return errorCode;
	}
}
