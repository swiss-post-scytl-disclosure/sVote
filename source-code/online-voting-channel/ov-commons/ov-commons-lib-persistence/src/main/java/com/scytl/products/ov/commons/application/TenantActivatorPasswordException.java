/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.application;

public class TenantActivatorPasswordException extends RuntimeException {

	private static final long serialVersionUID = -8525920351401677634L;

	public TenantActivatorPasswordException(Throwable cause) {
        super(cause);
    }

    public TenantActivatorPasswordException(String message) {
        super(message);
    }
    
    public TenantActivatorPasswordException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
}
