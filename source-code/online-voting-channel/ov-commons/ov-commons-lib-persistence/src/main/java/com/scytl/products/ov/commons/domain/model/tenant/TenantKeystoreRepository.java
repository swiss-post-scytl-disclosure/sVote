/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.domain.model.tenant;

import javax.ejb.Local;

import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for handling the tenant keystores in each context
 */
@Local
public interface TenantKeystoreRepository extends BaseRepository<TenantKeystoreEntity, Long> {

    /**
     * Returns the tenant keystores for a given tenant identifier and key type.
     *
     * @param tenantId
     *            tenant identifier.
     * @param keyType
     *            key type to search.
     * @return the tenant keystore that matches with the parameters.
     */
    TenantKeystoreEntity getByTenantAndType(String tenantId, String keyType);

    /**
     * Check if a keystore of the specified type exists for the specified tenant.
     *
     * @param tenantId
     *            the ID of the tenant.
     * @param keyType
     *            the type of the private key that is stored within the keystore.
     * @return true if such a keystore is found, false otherwise.
     */
    boolean checkIfKeystoreExists(String tenantId, String keyType);
}
