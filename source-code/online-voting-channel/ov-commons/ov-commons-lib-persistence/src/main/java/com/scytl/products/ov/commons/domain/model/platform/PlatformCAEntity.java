/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.domain.model.platform;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  class for defining an entity representing a platform root CA
 */
@MappedSuperclass
public  class PlatformCAEntity {


    /**
     * The column length 100.
     */
    public static final int COLUMN_LENGTH_100 = 100;

    /**
     *  The platform name
     */
    @Column(name = "PLATFORM_NAME")
    @NotNull
    @Size(max = COLUMN_LENGTH_100)
    private String platformName;

    /**
     * The certificate name
     */
    @Column(name = "CERTIFICATE_NAME")
    @NotNull
    @Size(max = COLUMN_LENGTH_100)
    private String certificateName;

    /**
     * The content of the certificate
     */
    @Column(name = "CERTIFICATE_CONTENT")
    @NotNull
    @Lob
    private String certificateContent;


    /**
     * Sets new certificateName.
     *
     * @param certificateName New value of certificateName.
     */
    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    /**
     * Gets platformName.
     *
     * @return Value of platformName.
     */
    public String getPlatformName() {
        return platformName;
    }

    /**
     * Gets certificateName.
     *
     * @return Value of certificateName.
     */
    public String getCertificateName() {
        return certificateName;
    }

    /**
     * Sets new platformName.
     *
     * @param platformName New value of platformName.
     */
    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    /**
     * Gets certificateContent.
     *
     * @return Value of certificateContent.
     */
    public String getCertificateContent() {
        return certificateContent;
    }

    /**
     * Sets new certificateContent.
     *
     * @param certificateContent New value of certificateContent.
     */
    public void setCertificateContent(String certificateContent) {
        this.certificateContent = certificateContent;
    }
}
