/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.application;

public class TenantActivatorKeystoreException extends RuntimeException {

	private static final long serialVersionUID = 1920022423620443456L;

	public TenantActivatorKeystoreException(Throwable cause) {
        super(cause);
    }

    public TenantActivatorKeystoreException(String message) {
        super(message);
    }
    
    public TenantActivatorKeystoreException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
}
