/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.domain.model.platform;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

import javax.ejb.Local;

/**
 * Repository for handling the platform root ca in each context
 */
@Local
public interface PlatformCARepository extends BaseRepository<PlatformCAEntity,Long> {

    /**
     * Returns the root ca certificate
     * @return
     * @throws ResourceNotFoundException
     */
    public PlatformCAEntity getRootCACertificate() throws ResourceNotFoundException;
}
