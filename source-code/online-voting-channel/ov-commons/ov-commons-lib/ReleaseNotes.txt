Version 0.8.0
Date 07.08.2015

DESCRIPTION
This is the eighth release of a set of common things used in the Online Voting components.

WHAT'S NEW

Added new methods  in the JsonUtils class.
Updated cryptolib policies and cryptolib version.

Package utils:
The class Json Utils now include methods for adding and removing fields to JSON objects

Package crypto:
updated references to the stores packages of cryptolib


Version 00.07.00
Date 27.07.2015

DESCRIPTION:
This is the seventh release of a set of common things used in the Online Voting components.

WHAT'S NEW
Added path resolving components

Package path:
New interface and classes used to resolve path locations:
Absolute path resolver for root based paths (/ in unix, C: in windows)
Prefix path resolver for paths based on a rooted prefix (e.g. user home)

Version 0.6.0
Date 09.07.2015

DESCRIPTION:
This is the sixth release of a set of common things used in the Online Voting components.

WHAT'S NEW
Changed the version of JavaEE dependency from 7 to 6.
beans.xml added to enable CDI.

Package crypto:
New class Constants for constants used in the crypto part created.
New class Utils for basic operations for crypto stuff.
New class Producer for the crypto ProofsService created. This should be moved where the service is used.

Package domain:
Rule mechanism functionality moved here (AbstractRule, RuleRepository) used for defining a rule and handling a rule repository.
Class RuleExecutor used for executing rules moved here.

Package infrastructure:
BaseRepositoryImpl is not a Stateless anymore.
RESTClient adapted to work with JavaEE 6.

Package logging:
Method provider for default logger added to LoggerProvider. 
Sonar violations fixed.

Package ui:
New class Constants with constants used in the user interface created.

???Version 0.5.0
Date xx.05.2015

???Version 0.4.0
Date xx.05.2015

Version 0.3.0
Date xx.05.2015

DESCRIPTION:
This is the third release of a set of common things used in the Online Voting components.

WHAT'S NEW
Package application:
New constructor to create exception with message and cause (DuplicateEntryException, ResourceNotFoundException).

Package infrastructure:
In BaseRepositoryImpl class, throw exception with cause.
InfrastructureException removed as now specific exceptions are used (eg ResourceNotFoundException).

Version 0.2.0
Date 15.05.2015

DESCRIPTION:
This is the second release of a set of common things used in the Online Voting components.

WHAT'S NEW
Package infrastructure:
In BaseRepositoryImpl class, which offers basic operations (CRUD) with a RDBMS, the save method thrown DuplicateEntryException when there is a unique constraint violation for saving operations.

Package model:
Patterns for common patterns used in regexp pattern validations (with @Pattern annotation of javax.Validations).

Package application:
InconsistentDataException is removed since it is not used anymore in the application.
ValidationException is used when throwing exception in case of a validation fails.
SyntaxErrorException (extending from ValidationException) is used when throwing exception in case of a syntax validation errors.
SemanticErrorException (extending from ValidationException) is used when throwing exception in case of a semantic validation errors.
SyntaxErrorGroup and SemanticErrorGroup for grouping validations when a bean is validated with javax.Validations.

Package ui.ws.rs:
GlobalExceptionHandler now returns 404 Http Status Code for unknown resource.
ValidationExceptionHandler used for exception handling ValidationException.
Now, all handlers logs the thrown exception.  

Package util:
JsonUtils offers a method to convert json in string to JsonObject.
ValidationUtils offers a method to execute javax.Validations in a programmatically way by applying the set of validations defined in the class using annotations like @NotNull, @Size, @Pattern, etc.

====================================================

Version 0.1.0
Date 29.04.2015

DESCRIPTION:
This is the first version of a set of common things used in the Online Voting components.

WHAT'S NEW
Package infrastructure:
Abstract implementation class BaseRepositoryImpl offers basic operations (CRUD) with a RDBMS - for now only CR.
Simple REST client that generates a WebTarget for a given baseURI and a path.

Package model:
Interface BaseRepository that defines basic operations (CRUD) - for now only CR.
Class Constants defines basic constants used in the model, like length of a field.

Package application:
ApplicationException is used when throwing any general exception from the application.
ApplicationExceptionMessages contains standard message texts as constants that can be used when throwing an ApplicationException.
InconsistentDataException is used when throwing exceptions in case of data inconsistency.
ResourceNotFoundException is used when throwing exception in case of a resource is not found in a repository.

Package ui.ws.rs:
ParameterConstants contains all the names of standard parameters as constants that can be used.
GenericJsonMapper is used for mapping data between object model and json format.
ErrorCodes contains the standard error codes as constants that can be used.
HTTPStatus contains other standard HTTP status codes that are not defined in the HTTP standard (for now 422 for validation errors).
PATCH is an annotation for the corresponding HTTP method. It is used exactly like the existing ones (eg GET, PUT).
There are several classes which are used for error handling (Error, Message).
There are several classes which are used for exception handling in the implementation of resources depending on the type of exception (for now ApplicationException, ResourceNotFoundException) and one for the rest of them (GlobalExceptionHandler).

Package logging:
LoggerProvider has static method for returning the current instance of logger used. The configuration of the logger is in the logback.xml.
I18nLoggerMessages provides access to standard message texts in the system language based on a key that can be used for logging.

Package random:
Interface and implementation SecureRandom for 3 types (Bytes, Integer, String) that provide random value of that type (based on cryptolib).
RandomType contains the current types provided.

Package tracking:
TrackIdGenerator generates an identifier to be used for tracking requests in all the systems.
TrackIdInstance manages an given identifier by putting/getting it on/from MDC of slf4j so that can be added automatically to the log messages.

Package util:
DateUtils offers for now only one method for returning the current timestamp (time in ms) having 13 characters. Until 13 the timestamp is completed with 0 on the left.
PropertiesFileReader is an enum which offers the possibility of accessing properties based on the key from the Application.properties file located under src/main/resource/.
