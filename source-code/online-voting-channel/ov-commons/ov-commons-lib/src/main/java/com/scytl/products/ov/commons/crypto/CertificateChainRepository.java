/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

import java.security.cert.Certificate;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.scytl.keystore.cryptoapi.CryptoAPIScytlKeyStore;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;

/**
 * Repository for certificate chain instances
 */
public class CertificateChainRepository {

	private final KeystoreForObjectOpener keystoreOpener;


	private static final String EMPTY_ID = "";

	/**
	 * Create a keystore based certificate chain repository.
	 * 
	 * @param keystoreOpener the keystore opener that will be used to explore keystores for the required certificate chains
	 */
	public CertificateChainRepository(KeystoreForObjectOpener keystoreOpener) {
		this.keystoreOpener = keystoreOpener;
	}

	/**
	 * Find a certificate chain identified by the provided tenant id, election event id and alias.
	 * 
	 * @param tenant the tenant identifier.
	 * @param eeid the election event identifier.
	 * @param alias the certificate chain alias.
	 * @return a certificate array containing the certificate chain with the leaf certificate as first element.
	 * @throws CryptographicOperationException if an error occurs during the certificate chain location or retrieval.
	 * @throws ResourceNotFoundException if the certificate chain source can not be found.
	 */
	public Certificate[] findByTenantEEIDAlias(String tenant, String eeid, String alias)
			throws CryptographicOperationException, ResourceNotFoundException {
	    return findByTenantEEIDAlias(tenant, eeid, EMPTY_ID, alias);
	}
	
	/**
     * Find a certificate chain identified by the provided tenant id, election event id and alias.
     * 
     * @param tenant the tenant identifier.
     * @param eeid the election event identifier.
     * @param objectId the object id identified
     * @param alias the certificate chain alias.
     * @return a certificate array containing the certificate chain with the leaf certificate as first element.
     * @throws CryptographicOperationException if an error occurs during the certificate chain location or retrieval.
     * @throws ResourceNotFoundException if the certificate chain source can not be found.
     */
    public Certificate[] findByTenantEEIDAlias(String tenant, String eeid, String objectId, String alias)
            throws CryptographicOperationException, ResourceNotFoundException {
        CryptoAPIScytlKeyStore keystore = keystoreOpener.openForTenantEEIDObjectIdAlias(tenant, eeid, objectId, alias);

        Certificate[] certificates;
        try {
            certificates = keystore.getCertificateChain(alias);
        } catch (GeneralCryptoLibException e) {
            throw new CryptographicOperationException("Can not recover certificate chain from keystore:", e);
        }
        return certificates;
    }

}
