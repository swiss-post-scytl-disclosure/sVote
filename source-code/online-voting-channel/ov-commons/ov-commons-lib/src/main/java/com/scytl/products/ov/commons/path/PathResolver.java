/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.path;

import java.nio.file.Path;

/**
 * Interface for resolving path.
 */
public interface PathResolver {

    /**
     * Resolves a path, given one or more path strings, in the correct order.
     * 
     * @param pathStrings One or more path strings, in the correct order.
     * @return the resolved path.
     */
	Path resolve(final String... pathStrings);
}
