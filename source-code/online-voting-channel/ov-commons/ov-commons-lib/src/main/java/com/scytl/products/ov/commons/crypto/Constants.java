/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

/**
 * This class contains constants used in the cryptographic part.
 */
public final class Constants {

	/**
	 * The separator used in the encrypted options string.
	 */
	public static final String SEPARATOR_ENCRYPTED_OPTIONS = ";";

	/**
	 * The position of cyphertext C0 in the encrypted options.
	 */
	public static final int POSITION_C0 = 0;

	/**
	 * The position of cyphertext C1 in the encrypted options.
	 */
	public static final int POSITION_C1 = 1;

	// Avoid instantiation.
	private Constants() {
	}
}
