/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package  com.scytl.products.ov.commons.ui.ws.rs;

/**
 * Contains the general input parameter names used in applications.
 */
public final class ParameterConstants {

	/**
	 * The name of the parameter track id.
	 */
	public static final String TRACK_ID = "trackid";

	// Avoid instantiation
	private ParameterConstants() {
	}
}
