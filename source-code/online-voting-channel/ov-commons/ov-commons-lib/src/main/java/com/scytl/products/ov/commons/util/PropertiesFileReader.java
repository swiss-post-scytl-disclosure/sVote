/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.inject.Inject;

import org.slf4j.Logger;

/**
 * Utility class for reading the application properties file.
 */
public enum PropertiesFileReader {

	INSTANCE;

	private final Properties properties;

	@Inject
	private Logger LOG;

	// The name of the properties file of the application.
	private static final String FILE_NAME_APPLICATION_PROPERTIES = "Application.properties";

	PropertiesFileReader() {
		properties = new Properties();
		try {
			InputStream inputStream =
				Thread.currentThread().getContextClassLoader().getResourceAsStream(FILE_NAME_APPLICATION_PROPERTIES);
			properties.load(inputStream);
		} catch (IOException ioE) {
			LOG.error("Error trying to get resource " + FILE_NAME_APPLICATION_PROPERTIES, ioE);
		}
	}

	/**
	 * Obtains the instance of the propertiesFileReader.
	 * 
	 * @return Class instance
	 */
	public static PropertiesFileReader getInstance() {
		return INSTANCE;
	}

	/**
	 * Given a property name returns its value.
	 * 
	 * @param propertyName , name of the property
	 * @return the value of the property
	 */
	public String getPropertyValue(String propertyName) {
		return properties.getProperty(propertyName);
	}
}
