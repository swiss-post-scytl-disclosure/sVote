/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.path;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * {@link PathResolver} implementation that resolves a path by treating it as absolute path
 */
public class AbsolutePathResolver implements PathResolver {

    private final static Path ROOT = Paths.get(File.separator);

    /**
     * @see com.scytl.nsw.commons.path.PathResolver#resolve(java.lang.String)
     */
    @Override
    public Path resolve(final String... pathStrings) {
        Path resolvedPath = Paths.get(pathStrings[0].trim());
        if (!resolvedPath.isAbsolute() && !resolvedPath.startsWith(File.separator)) {
            resolvedPath = ROOT.resolve(resolvedPath);
        }

        for (int i = 1; i < pathStrings.length; i++) {
            resolvedPath = Paths.get(resolvedPath.toString(), pathStrings[i].trim());
        }

        return resolvedPath;
    }
}
