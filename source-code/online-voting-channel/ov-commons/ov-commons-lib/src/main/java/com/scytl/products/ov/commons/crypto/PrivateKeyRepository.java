/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

import java.security.PrivateKey;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.scytl.keystore.cryptoapi.CryptoAPIScytlKeyStore;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;

/**
 * Repository for handling Private Keys entities
 */
public class PrivateKeyRepository {

	private final KeystoreOpener keystoreOpener;

	private final PasswordRepository passwordRepository;

	/**
	 * Constructor for a keystore based private key repository.
	 * 
	 * @param keystoreOpener the keystore opener that will be used to locate and retrieve the keystore instance.
	 * @param passwordRepository the repository that will be used to retrieve the private key password.
	 */
	public PrivateKeyRepository(KeystoreOpener keystoreOpener, PasswordRepository passwordRepository) {
		this.keystoreOpener = keystoreOpener;
		this.passwordRepository = passwordRepository;
	}

	/**
	 * Searches for an private key with the given tenant, election event and alias.
	 *
	 * @param tenantId - the identifier of the tenant.
	 * @param eeid - the identifier of the electionEvent.
	 * @param alias - the private key alias
	 * @return a entity representing the private key.
	 * @throws CryptographicOperationException if there is an error recovering the private key
	 * @throws ResourceNotFoundException if the keystore containing the private key can not be found
	 */
	public PrivateKey findByTenantEEIDAlias(String tenant, String eeid, String alias)
			throws CryptographicOperationException, ResourceNotFoundException {
		CryptoAPIScytlKeyStore keystore = keystoreOpener.openForTenantEEIDAlias(tenant, eeid, alias);
		String password = passwordRepository.getByTenantEEIDAlias(tenant, eeid, alias);

		PrivateKey privateKey;
		try {
			privateKey = keystore.getPrivateKeyEntry(alias, password.toCharArray());
		} catch (GeneralCryptoLibException e) {
			throw new CryptographicOperationException("Can not recover private key from keystore: ", e);
		}
		return privateKey;
	}

}
