/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.domain.model;

/**
 * $Id$
 * @author dcheda
 * @date   May 11, 2015 4:59:46 PM
 *
 * Copyright (C) 2015 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 *
 */

/**
 * Patterns to validate values.
 */
public final class Patterns {

	/**
	 * Pattern for challenge which is a base64 string.
	 */
	public static final String CHALLENGE = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$";

	/**
	 * Pattern for a timestamp of 13 digits.
	 */
	public static final String TIMESTAMP = "\\d{13}";

	/**
	 * Pattern for IDs. Complements this with @NotNull and @Size.
	 */
	public static final String ID = "^[a-zA-Z0-9]*$";

	/**
	 * Pattern digits.
	 */
	public static final String DIGITS = "^[0-9]*$";

	private Patterns() {
		// To avoid instantiation
	}
}
