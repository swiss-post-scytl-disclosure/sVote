/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto.configuration;

import java.util.Map;

/**
 * A bean representing the 'configuration.json' file. This file contains  properties that allow the creation of
 * certificates for testing purposes.
 */
public class ConfigurationInput {

    /*
    This attribute contains all the config in order to create:
    1) electioneventca certificate
    2) authoritiesca certificate
    3) servicesca certificate
    4) credentialsca certificate
    5) adminboard certificate
     */
    private Map<String, CredentialProperties> configProperties;

    /*
    AuthTokenSigner certificate properties
     */
    private CredentialProperties authTokenSigner;

    /*
    BallotBox certificate properties
     */
    private CredentialProperties ballotBox;

    /*
    CredentialSign certificate properties
     */
    private CredentialProperties credentialSign;

    /*
    CredentialAuth certificate properties
     */
    private CredentialProperties credentialAuth;

    /*
    VerificationCardSet certificate properties
     */
    private CredentialProperties verificationCardSet;

    /*
    VerificationCard certificate properties
     */
    private CredentialProperties verificationCard;

    private CredentialProperties votingCardSet;

    public CredentialProperties getVerificationCard() {
        return verificationCard;
    }

    public void setVerificationCard(final CredentialProperties verificationCard) {
    	this.verificationCard = verificationCard;
    }

    public CredentialProperties getVerificationCardSet() {
        return verificationCardSet;
    }

    public void setVerificationCardSet(final CredentialProperties verificationCardSet) {
    	this.verificationCardSet = verificationCardSet;
    }

    /**
     * @return
     */
    public CredentialProperties getBallotBox() {
        return ballotBox;
    }

    /**
     * @param ballotBox
     */
    public void setBallotBox(final CredentialProperties ballotBox) {
    	this.ballotBox = ballotBox;
    }

    /**
     * @return Returns the authTokenSigner.
     */
    public CredentialProperties getAuthTokenSigner() {
        return authTokenSigner;
    }

    /**
     * @param authTokenSigner The authTokenSigner to set.
     */
    public void setAuthTokenSigner(final CredentialProperties authTokenSigner) {
    	this.authTokenSigner = authTokenSigner;
    }

    /**
     * @return Returns the configProperties.
     */
    public Map<String, CredentialProperties> getConfigProperties() {
        return configProperties;
    }

    /**
     * @param configProperties The configProperties to set.
     */
    public void setConfigProperties(final Map<String, CredentialProperties> configProperties) {
    	this.configProperties = configProperties;
    }

    public void setCredentialSign(final CredentialProperties credentialSign) {
    	this.credentialSign = credentialSign;
    }

    public CredentialProperties getCredentialSign() {
        return credentialSign;
    }

    public CredentialProperties getCredentialAuth() {
        return credentialAuth;
    }

    public void setCredentialAuth(final CredentialProperties credentialAuth) {
    	this.credentialAuth = credentialAuth;
    }

    public CredentialProperties getVotingCardSet() {
        return votingCardSet;
    }

    public void setVotingCardSet(final CredentialProperties votingCardSet) {
        this.votingCardSet = votingCardSet;
    }
}
