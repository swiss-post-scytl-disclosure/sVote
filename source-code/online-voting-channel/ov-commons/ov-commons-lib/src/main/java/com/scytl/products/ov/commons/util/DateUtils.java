/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.util;

import org.apache.commons.lang3.StringUtils;

/**
 * The util class to handle dates.
 */
public final class DateUtils {

	private static final int TIMESTAMP_LENGTH = 13;

	private static final String PAD_CHARACTER = "0";

	/**
	 * Non-public constructor
	 */
	private DateUtils() {
		
	}
	
	/**
	 * Returns the timestamp in miliseconds.
	 * 
	 * @return timestamp.
	 */
	public static String getTimestamp() {
		Long miliseconds = System.currentTimeMillis();
		return StringUtils.leftPad(String.valueOf(miliseconds), TIMESTAMP_LENGTH, PAD_CHARACTER);
	}
}
