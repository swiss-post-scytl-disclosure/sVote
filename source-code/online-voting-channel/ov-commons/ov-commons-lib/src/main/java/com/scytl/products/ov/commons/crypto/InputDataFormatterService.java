/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

/**
 * Interface for formatting input strings.
 */
public interface InputDataFormatterService {

	/**
	 * Concatenates the string data and returns an array of bytes.
	 * 
	 * @param data - the array of strings to be formatted.
	 * @return An array of byte arrays.
	 */
	public abstract byte[] concatenate(String... data);

}
