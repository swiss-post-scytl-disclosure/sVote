/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.random;

/**
 * The enum representing the different types of secure random.
 */
public enum RandomType {
	BYTES, STRING, INTEGER
}
