/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.logging;

import javax.enterprise.inject.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logger producer.
 */
public class LoggerProducer {

	/**
	 * Develop logger name.
	 */
	public static final String LOGGER_NAME_STANDARD = "std";

	/**
	 * Instantiates a develop logger.
	 * 
	 * @return a develop logger.
	 */
	@Produces
	public Logger getDevelopLoggingInstance() {
		return LoggerFactory.getLogger(LOGGER_NAME_STANDARD);
	}


}
