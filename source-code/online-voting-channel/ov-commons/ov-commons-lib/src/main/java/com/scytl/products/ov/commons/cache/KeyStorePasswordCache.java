/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.cache;

/**
 * Implementation of {@link Cache} which stores the key store passwords.
 */
public class KeyStorePasswordCache extends CacheSupport<String, String> {

	public KeyStorePasswordCache(String cacheName) {
		super(cacheName);
	}
}
