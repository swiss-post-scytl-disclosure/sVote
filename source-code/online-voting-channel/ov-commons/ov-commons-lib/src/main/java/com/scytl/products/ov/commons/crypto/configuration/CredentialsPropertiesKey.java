/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto.configuration;

/**
 * Enum with the object used as key for storing the different credentials properties in some other structure , for
 * instance a Map
 */
public enum CredentialsPropertiesKey {

    SERVICES_CA("servicesca"),
    ELECTION_CA("electioneventca"),
    CREDENTIALS_CA_JSON_TAG("credentialsca"),
    AUTHORITIES_CA("authoritiesca");

    private String name;

    CredentialsPropertiesKey(String name) {
        this.name = name;
    }

    /**
     * Gets name.
     *
     * @return Value of name.
     */
    public String getName() {
        return name;
    }
}
