/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.tracking;

import com.scytl.cryptolib.api.exceptions.CryptoLibException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.random.impl.SecureRandomString;

public class DefaultTrackIdGenerator implements TrackIdGenerator {

    private static final int LENGTH_IN_CHARS = 16;

    @Override
    public String generate() {
        return generate(LENGTH_IN_CHARS);
    }

    @Override
    public String generate(final int length) {
        try {
            return SecureRandomString.getInstance().getRandomValue(length);
        } catch (GeneralCryptoLibException e) {
            throw new CryptoLibException("Failed to generate random string.", e);
        }
    }
}
