/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.scytl.keystore.cryptoapi.CryptoAPIScytlKeyStore;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;

/**
 * Repository for handling Elgamal Private Keys entries for specific object like
 * a verification card set.
 */
public class ElgamalPrivateKeyForObjectRepository {
    private final KeystoreForObjectOpener keystoreOpener;

    private final PasswordForObjectRepository passwordRepository;

    /**
     * Constructor for a keystore based elgamal private key repository.
     *
     * @param keystoreOpener
     *            the keystore opener that will be used to locate and retrieve
     *            the keystore instance.
     * @param passwordRepository
     *            the repository that will be used to retrieve the keystore
     *            password.
     */
    public ElgamalPrivateKeyForObjectRepository(final KeystoreForObjectOpener keystoreOpener,
            final PasswordForObjectRepository passwordRepository) {
    	this.keystoreOpener = keystoreOpener;
        this.passwordRepository = passwordRepository;
    }

    /**
     * Searches for an elgamal private key with the given tenant, election
     * event, object identifier and alias.
     *
     * @param tenantId
     *            - the identifier of the tenant.
     * @param eeid
     *            - the identifier of the electionEvent.
     * @param objectId
     *            - the identifier of the object.
     * @param alias
     *            - the elgamal private key alias.
     * @return a entity representing the private key.
     * @throws CryptographicOperationException
     *             if there is an error recovering the elgamal private key.
     * @throws ResourceNotFoundException
     *             if the keystore containing the elgamal private key can not be
     *             found.
     */
    public ElGamalPrivateKey findByTenantEEIDObjectIdAliasGroup(final String tenantId, final String eeid,
            final String objectId, final String alias)
            throws CryptographicOperationException, ResourceNotFoundException {

        ElGamalPrivateKey privateKey;

        try {
            CryptoAPIScytlKeyStore store =
                keystoreOpener.openForTenantEEIDObjectIdAlias(tenantId, eeid, objectId, alias);

            char[] password =
                passwordRepository.getByTenantEEIDObjectIdAlias(tenantId, eeid, objectId, alias).toCharArray();

            privateKey = store.getElGamalPrivateKeyEntry(alias, password);
        } catch (GeneralCryptoLibException e) {
            throw new CryptographicOperationException("Can not recover elgamal private key from the keystore.", e);
        }

        return privateKey;
    }

}
