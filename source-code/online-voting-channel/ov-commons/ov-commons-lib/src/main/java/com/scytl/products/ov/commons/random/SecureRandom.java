/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.random;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Interface providing methods for generating random values.
 *
 * @param <T>
 *            the generic type used to define the type of random value.
 */
public interface SecureRandom<T> {

    /**
     * Returns the next random value for a defined length.
     *
     * @param length
     *            - the length of the random value.
     * @return the next random value for a defined length.
     * @throws GeneralCryptoLibException
     *             the general crypto lib exception
     */
    T getRandomValue(int length) throws GeneralCryptoLibException;
}
