/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto.impl;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;

import com.scytl.products.ov.commons.crypto.InputDataFormatterService;

/**
 * Service for formatting input data.
 */
public class InputDataFormatterStringServiceImpl implements InputDataFormatterService {

	/**
	 * Concatenates the string data and returns an array of bytes.
	 * 
	 * @param data - the array of strings to be formatted.
	 * @return An array of byte arrays.
	 * @see com.scytl.products.ov.commons.crypto.InputDataFormatterService#concatenate(java.lang.String)
	 */
	@Override
	public byte[] concatenate(String... data) {
		return StringUtils.join(data).getBytes(StandardCharsets.UTF_8);
	}

}
