/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.ui;

/**
 * Class for holding constants used in the rest web services.
 */
public final class Constants {

	/**
	 * The request id query parameter for logging purpose.
	 */
	public static final String PARAMETER_HEADER_X_REQUEST_ID = "X-Request-ID";


	// The name of the parameter value tenant id.
	public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

	// The name of the parameter value voting card id.
	public static final String PARAMETER_VALUE_VOTING_CARD_ID = "votingCardId";

	// The name of the parameter value election event id.
	public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

	// The name of the parameter value election event id.
	public static final String PARAMETER_VALUE_AUTHENTICATION_TOKEN = "authenticationToken";

	// Avoid instantiation.
	private Constants() {
	}
}
