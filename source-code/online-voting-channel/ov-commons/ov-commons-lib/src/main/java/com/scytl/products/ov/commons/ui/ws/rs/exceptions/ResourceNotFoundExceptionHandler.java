/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.ui.ws.rs.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.ws.rs.ErrorCodes;
import com.scytl.products.ov.commons.ui.ws.rs.Message;

/**
 * Created by riglesias on 24/03/15.
 */
@Provider
public class ResourceNotFoundExceptionHandler implements ExceptionMapper<ResourceNotFoundException> {

    // Label for the message system is not available.
	private static final String ERROR_MESSAGE_NOT_FOUND = "Resource not found!";

    private static final Logger LOG = LoggerFactory.getLogger(ResourceNotFoundExceptionHandler.class);

    /**
     * Generates a not found as a response of an ResourceNotFoundException.
     *
     * @param e
     *            - the exception.
     * @return the generated response.
     * @see javax.ws.rs.ext.ExceptionMapper#toResponse(Throwable)
     */
    @Override
    public Response toResponse(final ResourceNotFoundException e) {
        LOG.error("An error ocurred: ", e);

        final String errorCode = ErrorCodes.RESOURCE_NOT_FOUND;
        final Message message = new Message();
        message.setText(ERROR_MESSAGE_NOT_FOUND);
        message.addError("", "", errorCode);
        return Response.status(Response.Status.NOT_FOUND).entity(message)
            .build();
    }
}
