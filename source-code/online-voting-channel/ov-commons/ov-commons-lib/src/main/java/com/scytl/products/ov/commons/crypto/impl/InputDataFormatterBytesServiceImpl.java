/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto.impl;

import java.nio.charset.StandardCharsets;

/**
 * Service for formatting input data.
 */
public class InputDataFormatterBytesServiceImpl {

	private static final byte[] SEPARATOR = "|".getBytes(StandardCharsets.UTF_8);

	/**
	 * Concatenates the string data with a separator (|) and returns an array of bytes.
	 * 
	 * @param data - the array of strings to be formatted.
	 * @return An array of byte arrays.
	 */
	public byte[][] concatenate(String... data) {
		int signatureInputSize = (data.length * 2) - 1;
		byte[][] signatureInput = new byte[signatureInputSize][];
		for (int i = 0; i < data.length - 1; i++) {
			int pos = 2 * i;
			signatureInput[pos] = data[i].getBytes(StandardCharsets.UTF_8);
			signatureInput[pos + 1] = SEPARATOR;
		}
		signatureInput[signatureInputSize - 1] = data[data.length - 1].getBytes(StandardCharsets.UTF_8);
		return signatureInput;
	}

}
