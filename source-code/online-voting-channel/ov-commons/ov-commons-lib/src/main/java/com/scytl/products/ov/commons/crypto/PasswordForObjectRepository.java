/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * Interface for password repository for specific objects like ballot box.
 */
public interface PasswordForObjectRepository {

	/**
	 * Find a password string identified by this tenant id, election event id, object id and alias
	 * 
	 * @param tenantId the tenant identifier.
	 * @param eeId the election event identifier.
	 * @param objectId the object identifier.
	 * @param alias the password alias
	 * @return the password as string
	 * @throws ResourceNotFoundException if the password for this tenant id, election event id and alias can not be found.
	 */
	public String getByTenantEEIDObjectIdAlias(String tenantId, String eeId, String objectId, String alias)
			throws ResourceNotFoundException;
}
