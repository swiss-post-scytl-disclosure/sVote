/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.tracking;

public interface TrackIdGenerator {

    /**
     * Generate a new String random value with a default length of 16 chars
     * @return a new randomly generated value
     */
    String generate();

    /**
     * Generate a new String random value with length of 'length' chars
     * @return a new randomly generated value
     */
    String generate(int length);


}
