/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.util;

import org.apache.commons.io.IOUtils;

import com.scytl.products.ov.commons.beans.exceptions.ResourceStreamException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Utility class for handling resource files
 */
public class ResourceStreamUtils {

	/**
	 * Non-public constructor
	 */
	private ResourceStreamUtils() {
		
	}
	
	
	/**
     * Tries to read the content of a resource file and returns a String representation
     * 
     * @param resource
     * @return
     */
    public static String readResourceStream(final String resource) {

        try {

            final InputStream resourceAsStream =
                Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
            return IOUtils.toString(resourceAsStream);
        } catch (IOException ioE) {
            throw new ResourceStreamException("Error trying to read the content of a resource file.", ioE);
        }
    }
}
