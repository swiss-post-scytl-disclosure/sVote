/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

/**
 * Contains util methods for the cryptographic part.
 */
public final class Utils {

	// Avoid instatiation.
	private Utils() {
	}
	
	/**
	 * Returns the ciphertext from the given encrypted options.
	 * 
	 * @param encryptedOptions The encrypted options that contains the ciphertext.
	 * @return an array of strings representing the ciphertext.
	 */
	public static String[] getCiphertextElementsFromEncryptedOptions(String encryptedOptions) {
		return encryptedOptions.split(Constants.SEPARATOR_ENCRYPTED_OPTIONS);
	}

	/**
	 * Returns the cyphertext C0 from the given encrypted options.
	 * 
	 * @param encryptedOptions The encrypted options that contains the C0.
	 * @return a string representing the C0.
	 */
	public static String getC0FromEncryptedOptions(String encryptedOptions) {
		return encryptedOptions.split(Constants.SEPARATOR_ENCRYPTED_OPTIONS)[Constants.POSITION_C0];
	}

	/**
	 * Returns the cyphertext C1 from the given encrypted options.
	 * 
	 * @param encryptedOptions The encrypted options that contains the C1.
	 * @return a string representing the C1.
	 */
	public static String getC1FromEncryptedOptions(String encryptedOptions) {
		return encryptedOptions.split(Constants.SEPARATOR_ENCRYPTED_OPTIONS)[Constants.POSITION_C1];
	}

}
