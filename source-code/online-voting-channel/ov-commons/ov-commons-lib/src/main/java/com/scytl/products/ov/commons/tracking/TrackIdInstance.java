/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.tracking;

import javax.enterprise.context.RequestScoped;

import org.slf4j.MDC;

/**
 * The Class TrackIdInstance for setting or getting trackId in a request.
 */
@RequestScoped
public class TrackIdInstance {

	private String trackId;

	/**
	 * Sets the track id.
	 *
	 * @param trackId the new track id.
	 */
	public void setTrackId(String trackId) {
		this.trackId = trackId;
		MDC.put("trackId", trackId);
	}

	/**
	 * Gets the track id.
	 *
	 * @return the track id.
	 */
	public String getTrackId() {
		return trackId;
	}
}
