/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.validation;

import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;

/**
 * A class for storing errors from vote validations.
 */
public class ValidationError {

	private ValidationErrorType validationErrorType = ValidationErrorType.FAILED;

	private String[] errorArgs;

	public ValidationError(ValidationErrorType validationErrorType) {
		this.validationErrorType = validationErrorType;
	}

	public ValidationError() {
	}

	/**
	 * Returns the current value of the field validationErrorType.
	 *
	 * @return Returns the validationErrorType.
	 */
	public ValidationErrorType getValidationErrorType() {
		return validationErrorType;
	}

	/**
	 * Sets the value of the field validationErrorType.
	 *
	 * @param validationErrorType The validationErrorType to set.
	 */
	public void setValidationErrorType(ValidationErrorType validationErrorType) {
		this.validationErrorType = validationErrorType;
	}

	/**
	 * Returns the current value of the field errorArgs.
	 *
	 * @return Returns the errorArgs.
	 */
	public String[] getErrorArgs() {
		return errorArgs;
	}

	/**
	 * Sets the value of the field errorArgs.
	 *
	 * @param errorArgs The errorArgs to set.
	 */
	public void setErrorArgs(String[] errorArgs) {
		this.errorArgs = errorArgs;
	}

}
