/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto.configuration;

import java.time.ZonedDateTime;

import com.scytl.cryptolib.certificates.bean.X509DistinguishedName;

/**
 * Holds the Certificate parameters.
 */
public class CertificateParameters {
	public enum Type {
		ROOT, INTERMEDIATE, SIGN, ENCRYPTION
	}

	private Type type;

	private String userSubjectCn;

	private String userSubjectOrgUnit;

	private String userSubjectOrg;

	private String userSubjectCountry;

	private X509DistinguishedName userSubjectDn;

	private String userIssuerCn;

	private String userIssuerOrgUnit;

	private String userIssuerOrg;

	private String userIssuerCountry;

	private ZonedDateTime userNotBefore;

	private ZonedDateTime userNotAfter;

	/**
	 * @return Returns the type.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type
	 *            The type to set.
	 */
	public void setType(final Type type) {
		this.type = type;
	}

	/**
	 * @return Returns the userSubjectCn.
	 */
	public String getUserSubjectCn() {
		return userSubjectCn;
	}

	/**
	 * @param userSubjectCn
	 *            The userSubjectCn to set.
	 */
	public void setUserSubjectCn(final String userSubjectCn) {
		this.userSubjectCn = userSubjectCn;
	}

	/**
	 * @return Returns the userSubjectOrgUnit.
	 */
	public String getUserSubjectOrgUnit() {
		return userSubjectOrgUnit;
	}

	/**
	 * @param userSubjectOrgUnit
	 *            The userSubjectOrgUnit to set.
	 */
	public void setUserSubjectOrgUnit(final String userSubjectOrgUnit) {
		this.userSubjectOrgUnit = userSubjectOrgUnit;
	}

	/**
	 * @return Returns the userSubjectOrg.
	 */
	public String getUserSubjectOrg() {
		return userSubjectOrg;
	}

	/**
	 * @param userSubjectOrg
	 *            The userSubjectOrg to set.
	 */
	public void setUserSubjectOrg(final String userSubjectOrg) {
		this.userSubjectOrg = userSubjectOrg;
	}

	/**
	 * @return Returns the userSubjectCountry.
	 */
	public String getUserSubjectCountry() {
		return userSubjectCountry;
	}

	/**
	 * @param userSubjectCountry
	 *            The userSubjectCountry to set.
	 */
	public void setUserSubjectCountry(final String userSubjectCountry) {
		this.userSubjectCountry = userSubjectCountry;
	}

	/**
	 * @return Returns the userSubjectDn.
	 */
	public X509DistinguishedName getUserSubjectDn() {
		return userSubjectDn;
	}

	/**
	 * @param userSubjectDn
	 *            The userSubjectDn to set.
	 */
	public void setUserSubjectDn(final X509DistinguishedName userSubjectDn) {
		this.userSubjectDn = userSubjectDn;
	}

	/**
	 * @return Returns the userIssuerCn.
	 */
	public String getUserIssuerCn() {
		return userIssuerCn;
	}

	/**
	 * @param userIssuerCn
	 *            The userIssuerCn to set.
	 */
	public void setUserIssuerCn(final String userIssuerCn) {
		this.userIssuerCn = userIssuerCn;
	}

	/**
	 * @return Returns the userIssuerOrgUnit.
	 */
	public String getUserIssuerOrgUnit() {
		return userIssuerOrgUnit;
	}

	/**
	 * @param userIssuerOrgUnit
	 *            The userIssuerOrgUnit to set.
	 */
	public void setUserIssuerOrgUnit(final String userIssuerOrgUnit) {
		this.userIssuerOrgUnit = userIssuerOrgUnit;
	}

	/**
	 * @return Returns the userIssuerOrg.
	 */
	public String getUserIssuerOrg() {
		return userIssuerOrg;
	}

	/**
	 * @param userIssuerOrg
	 *            The userIssuerOrg to set.
	 */
	public void setUserIssuerOrg(final String userIssuerOrg) {
		this.userIssuerOrg = userIssuerOrg;
	}

	/**
	 * @return Returns the userIssuerCountry.
	 */
	public String getUserIssuerCountry() {
		return userIssuerCountry;
	}

	/**
	 * @param userIssuerCountry
	 *            The userIssuerCountry to set.
	 */
	public void setUserIssuerCountry(final String userIssuerCountry) {
		this.userIssuerCountry = userIssuerCountry;
	}

	/**
	 * @return Returns the userNotBefore.
	 */
	public ZonedDateTime getUserNotBefore() {
		return userNotBefore;
	}

	/**
	 * @param userNotBefore
	 *            The userNotBefore to set.
	 */
	public void setUserNotBefore(final ZonedDateTime userNotBefore) {
		this.userNotBefore = userNotBefore;
	}

	/**
	 * @return Returns the userNotAfter.
	 */
	public ZonedDateTime getUserNotAfter() {
		return userNotAfter;
	}

	/**
	 * @param userNotAfter
	 *            The userNotAfter to set.
	 */
	public void setUserNotAfter(final ZonedDateTime userNotAfter) {
		this.userNotAfter = userNotAfter;
	}

}
