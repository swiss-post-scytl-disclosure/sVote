/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.ui.ws.rs;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * Provider class that extends the default Produces/Consumes of
 * {@link JacksonJaxbJsonProvider}.
 *
 * By default Jackson has a tailored matching strategy using
 * the wildcard "{*}/{*}" for the Produces/Consumes mediaType.

 * If any other provider registered within the Context of the application
 * uses as default "application/json" , it will be preferred over Jackson
 * to read/write json.
 *
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JacksonProviderJsonMediaType extends JacksonJaxbJsonProvider{



}
