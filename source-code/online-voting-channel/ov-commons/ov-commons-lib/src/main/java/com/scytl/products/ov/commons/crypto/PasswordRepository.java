/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.crypto;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

/**
 * Repository for password strings.
 */
public class PasswordRepository {

	/**
	 * Find a password string identified by this tenant id, election event id and alias
	 * 
	 * @param tenant the tenant identifier.
	 * @param eeid the election event identifier
	 * @param alias the password alias
	 * @return the password as string
	 * @throws ResourceNotFoundException if the password for this tenant id, election event id and alias can not be found.
	 */
	public String getByTenantEEIDAlias(String tenant, String eeid, String alias) throws ResourceNotFoundException {
		// This implementation ignores tenant and eeid for key identification. Implement this feature when required.
		String password = PropertiesFileReader.getInstance().getPropertyValue(alias + ".pwd");
		if (password == null) {
			throw new ResourceNotFoundException("Can not find password for tenant " + tenant + " election event id " + eeid
				+ " and alias " + alias);
		}
		return password;
	}
}
