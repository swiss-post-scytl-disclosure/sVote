/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.domain.model.rule;

import com.scytl.products.ov.commons.validation.ValidationError;

/**
 * Abstract definition of a rule.
 *
 * @param <T> - the generic type of the object which is subject to be validated by the rule.
 */
public interface AbstractRule<T> {

	/**
	 * Applies a rule on the given object.
	 * 
	 * @param object - the object to which to apply the rule.
	 * @return A ValidationErrror containing information about the execution of the rule.
	 */
			ValidationError execute(T object);

	/**
	 * Returns the current value of the field name.
	 *
	 * @return Returns the name.
	 */
			String getName();
}
