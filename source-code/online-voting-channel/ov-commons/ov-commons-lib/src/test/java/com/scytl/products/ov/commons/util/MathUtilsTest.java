/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.util;

import org.junit.Test;

import java.math.BigInteger;

import static com.scytl.products.ov.commons.util.MathUtils.isqrt;
import static org.junit.Assert.assertEquals;

public class MathUtilsTest {

    private static BigInteger BIG_NUM = new BigInteger("8923872359823935902735983409345098230598340958390583095480358934095830584304945830954834082735902735902735902735092735409273540982");

    @Test
    public void testExactRootSquare() {
        assertEquals(BigInteger.valueOf(5), isqrt(BigInteger.valueOf(25)));
    }

    @Test
    public void testExactRootSquareBigNumber() {
        BigInteger squared = BIG_NUM.multiply(BIG_NUM);
        assertEquals(BIG_NUM, isqrt(squared));
    }

    @Test
    public void testNotExactRootSquare() {
        assertEquals(BigInteger.valueOf(4), isqrt(BigInteger.valueOf(23)));
    }

    @Test
    public void testNotExactRootSquare2() {
        assertEquals(BigInteger.valueOf(4), isqrt(BigInteger.valueOf(24)));
    }

    @Test(expected = ArithmeticException.class)
    public void testNegativeRootSquare() {
        isqrt(BigInteger.valueOf(-2));
    }
}
