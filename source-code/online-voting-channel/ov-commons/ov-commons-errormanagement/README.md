# sVote error management module

The error management module helps manage errors produced during the execution of discrete parts of the code.
An error management policy defines the behaviour of a function when it does not end successfully.
The usual case for this situation will be to periodically retry the action until successful.

As an example, the following snippet illustrates a function that will be called a few times before considering it has failed.
This might help in an environment with faulty network connections.
```java
final retries = 10;
PersistentErrorManagementPolicy policy = new PersistentErrorManagementPolicy(retries);
try {
    policy.manage(() -> quoteService.getQuote());
} catch (HttpException e) {
    System.out.printf('The quote could not be retrieved even after %d attempts', retries);
}
```

An implementation can also define a number of exceptions that should not be managed, letting the function fail immediately instead.
```java
TimedErrorManagementPolicy policy = new TimedErrorManagementPolicy(5, ChronoUnit.MINUTES, HttpException.class);
try {
    policy.manage(() -> quoteService.getQuote());
} catch (HttpException e) {
    System.out.printf('The HTTP service is not available. No retries will be attempted');
} catch (OtherException other) {
// [...]
```

## Fallible function

The function to run should be encapsulated as `FallibleFunction`, a functional interface that throws Exception.
Users of the module are encouraged to subclass both `ExecutionPolicy` and `FallibleFunction` so that it throws a more concrete exception.
