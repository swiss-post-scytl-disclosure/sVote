/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.errormanagement;

/**
 * A function whose execution is managed through a failure management policy has, alas, failed.
 */
public class ManagedExecutionException extends Exception {
    public ManagedExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
