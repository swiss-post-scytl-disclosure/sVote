/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.commons.errormanagement;

/**
 * Sending the message was retried unsuccessfully too many times.
 */
public class OutOfRetriesException extends ManagedExecutionException {

    private static final long serialVersionUID = 9186672107632563661L;

    /**
     * @param lastCause the last exception thrown before running out of retries
     * @param retries the number of times the action has been attempted before finally
     *         failing
     */
    public OutOfRetriesException(Throwable lastCause, int retries) {
        super(String.format("The action did not succeed after %d retries", retries), lastCause);
    }
}
