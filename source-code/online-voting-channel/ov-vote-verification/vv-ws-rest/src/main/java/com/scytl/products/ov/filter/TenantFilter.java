/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.util.UriParser;
import com.scytl.products.ov.vv.domain.model.tenant.VvTenantSystemKeys;

public final class TenantFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private static final String SEARCH_STRING = "/tenant/";

    private static final String PART_OF_LOGGING_REQUEST = "/platformdata/";

    private static final String PART_OF_ACTIVATE_TENANT_REQUEST = "/tenantdata/";

    private final UriParser uriParser = new UriParser(SEARCH_STRING);

    private static final String PART_OF_CHECK_RESOURCE = "/check";

    @Inject
    private VvTenantSystemKeys vvTenantSystemKeys;

    /**
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {

        LOG.info("Initializing filter - name: " + filterConfig.getFilterName() + ", path: "
            + filterConfig.getServletContext().getContextPath() + ", container: "
            + filterConfig.getServletContext().getServerInfo());
    }
    
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {


        String uri;
        if (request instanceof HttpServletRequest) {
            uri = ((HttpServletRequest) request).getRequestURI();
        } else {
            LOG.info("The request is not a HttpServletRequest. Aborting");
            return;
        }

        HttpServletResponse httpServletResponse;
        if (response instanceof HttpServletResponse) {
            httpServletResponse = (HttpServletResponse) response;
        } else {
            LOG.info("The response is not the expected type. Expected a HttpServletResponse. Aborting.");
            return;
        }

        String tenantID = uriParser.getValue(uri);

        if (vvTenantSystemKeys.getInitialized(tenantID)) {

            filterChain.doFilter(request, response);
            return;

        } else if (uri.contains(PART_OF_ACTIVATE_TENANT_REQUEST)) {

            LOG.info("Forwarding tenant request");
            filterChain.doFilter(request, response);
            return;

        } else if (uri.contains(PART_OF_LOGGING_REQUEST)) {

            LOG.info("Forwarding platform request");
            filterChain.doFilter(request, response);
            return;


        } else if (uri.contains(PART_OF_CHECK_RESOURCE)){

            LOG.info("Forwarding to check health resource");
            filterChain.doFilter(request, response);
            return;

        } else if (tenantID == "") {

            LOG.info("Request Ignored. Failed to find tenant ID in request");
            httpServletResponse.setStatus(Response.Status.PRECONDITION_FAILED.getStatusCode());
            return;

        } else {

            LOG.info("Request Ignored. Tenant not activated. Tenant: " + tenantID);
            httpServletResponse.setStatus(Response.Status.PRECONDITION_FAILED.getStatusCode());
            return;
        }
    }
    
    @Override
    public void destroy() {
        LOG.info("VV - destroying filter");
    }
}
