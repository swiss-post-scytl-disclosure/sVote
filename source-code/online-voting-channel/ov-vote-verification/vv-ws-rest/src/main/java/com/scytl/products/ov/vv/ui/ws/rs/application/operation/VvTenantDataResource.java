/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.ui.ws.rs.application.operation;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.cryptolib.certificates.bean.X509CertificateType;
import com.scytl.products.ov.commons.beans.domain.model.tenant.TenantActivationData;
import com.scytl.products.ov.commons.beans.domain.model.tenant.TenantInstallationData;
import com.scytl.products.ov.commons.beans.validation.ContextValidationResult;
import com.scytl.products.ov.commons.beans.validation.ValidationType;
import com.scytl.products.ov.commons.domain.model.tenant.TenantKeystoreRepository;
import com.scytl.products.ov.commons.tenant.TenantActivator;
import com.scytl.products.ov.vv.domain.model.tenant.VvTenantKeystoreEntity;
import com.scytl.products.ov.vv.domain.model.tenant.VvTenantKeystoreRepository;
import com.scytl.products.ov.vv.domain.model.tenant.VvTenantSystemKeys;

/**
 * Endpoint for upload the information during the installation of the tenant in the system
 */
@Path("tenantdata")
@Stateless(name = "vv-tenantDataResource")
public class VvTenantDataResource {

	private static final Logger LOG = LoggerFactory.getLogger(VvTenantDataResource.class);
	
    private static final String TENANT_PARAMETER = "tenantId";

    private static final String CONTEXT = "VV";

    @EJB
    @VvTenantKeystoreRepository
    TenantKeystoreRepository tenantKeystoreRepository;

    @EJB
    private VvTenantSystemKeys vvTenantSystemKeys;

    /**
     * Installs the tenant keystores in the Service
     *
     * @param data,
     *            json object containing the information
     */
    @POST
    @Path("/tenant/{tenantId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveTenantData(@PathParam(TENANT_PARAMETER) final String tenantId,
            final TenantInstallationData data) {

        try {

            LOG.info("VV - install tenant request received");

            VvTenantKeystoreEntity tenantKeystoreEntity = new VvTenantKeystoreEntity();
            tenantKeystoreEntity.setKeystoreContent(data.getEncodedData());
            tenantKeystoreEntity.setTenantId(tenantId);
            tenantKeystoreEntity.setKeyType(X509CertificateType.ENCRYPT.name());

            tenantKeystoreRepository.save(tenantKeystoreEntity);

            TenantActivator tenantActivator =
                new TenantActivator(tenantKeystoreRepository, vvTenantSystemKeys, CONTEXT);
            tenantActivator.activateUsingReceivedKeystore(tenantId, data.getEncodedData());

            return Response.ok().build();

        } catch (Exception e) {
            String errorMsg = "VV - error while trying to install a tenant: " + e.getMessage();
            LOG.error(errorMsg);
            throw new IllegalStateException(errorMsg, e);
        }
    }

    @POST
    @Path("activatetenant")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response activateTenant(final TenantActivationData tenantActivationData) {

        try {

            LOG.info("VV - activate tenant request received");

            TenantActivator tenantActivator =
                new TenantActivator(tenantKeystoreRepository, vvTenantSystemKeys, CONTEXT);
            tenantActivator.activateFromDB(tenantActivationData);

            return Response.ok().build();

        } catch (Exception e) {
            String errorMsg = "VV - error while trying to install a tenant: " + e.getMessage();
            LOG.error(errorMsg);
            throw new IllegalStateException(errorMsg, e);
        }
    }

    /**
     * Check if a tenant has been activated for this context
     * 
     * @param tenantId
     *            - identifier of the tenant
     * @return
     */
    @GET
    @Path("activatetenant/tenant/{tenantId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkTenantActivation(@PathParam(TENANT_PARAMETER) final String tenantId) {

        ContextValidationResult validation = new ContextValidationResult.Builder().setContextName(CONTEXT)
            .setValidationType(ValidationType.TENANT_ACTIVATION).setResult(vvTenantSystemKeys.getInitialized(tenantId))
            .build();
        return Response.ok(validation).build();
    }
}
