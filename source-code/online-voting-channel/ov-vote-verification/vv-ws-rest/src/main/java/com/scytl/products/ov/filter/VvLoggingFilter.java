/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.filter;

import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.domain.model.platform.LoggingInitializationState;
import com.scytl.products.ov.commons.logging.config.LoggingFilter;
import com.scytl.products.ov.vv.infrastructure.log.VvLoggingInitializationState;

/**
 * Configurable filter that can filter requests to a number (configurable list) of services.
 * <P>
 */
public final class VvLoggingFilter extends LoggingFilter {

    @Inject
    private VvLoggingInitializationState loggingInitializationState;

    @Override
    public LoggingInitializationState getLoggingInitializationState() {
        return loggingInitializationState;
    }

    @Override
    public String filterName() {
        return "VV";
    }
}
