Given REST call *generate_choice_codes_404_2* with URL */vv-ws-rest/choicecodes/tenant/100/electionevent/0000/verificationcard/100*
Given REST call *generate_choice_codes_404_2* with Header *requestid* / *100*
Given REST call *generate_choice_codes_404_2* with Method *POST*
Given REST call *generate_choice_codes_404_2* with Header *Content-Type* / *application/json*
Given REST call *generate_choice_codes_404_2* with JSON Body:
{
  "encryptedOptions": "132412342134;asdfasdfasdf.AVeryLongStringWithTheEncryptedOptionsafasdf12312423asdfnmlourjvlmaldjfoqwer14ad34fas1avjsjjj;.",
  "partialChoiceCodes":"a1ds23f1as3df13asdf3aasdfasfdGeneratedPartialChoiceCodesadsfasdfasdfasdf"
}

When REST call *generate_choice_codes_404_2* execute

Then REST call *generate_choice_codes_404_2* response Status Code is *404*