Given REST call *generate_choice_codes_400_2* with URL */vv-ws-rest/choicecodes/tenant/100/electionevent/100/verificationcard/100*
Given REST call *generate_choice_codes_400_2* with Header *requestid* / *100*
Given REST call *generate_choice_codes_400_2* with Method *POST*
Given REST call *generate_choice_codes_400_2* with Header *Content-Type* / *application/json*

When REST call *generate_choice_codes_400_2* execute

Then REST call *generate_choice_codes_400_2* response Status Code is *400*