Given REST call *generate_choice_codes* with URL */vv-ws-rest/choicecodes/tenant/100/electionevent/100/verificationcard/100*
Given REST call *generate_choice_codes* with Header *requestid* / *100*
Given REST call *generate_choice_codes* with Method *POST*
Given REST call *generate_choice_codes* with Header *Content-Type* / *application/json*
Given REST call *generate_choice_codes* with JSON Body:
{
  "encryptedOptions": "132412342134;asdfasdfasdf.AVeryLongStringWithTheEncryptedOptionsafasdf12312423asdfnmlourjvlmaldjfoqwer14ad34fas1avjsjjj;.",
  "partialChoiceCodes":"a1ds23f1as3df13asdf3aasdfasfdGeneratedPartialChoiceCodesadsfasdfasdfasdf",
}

When REST call *generate_choice_codes* execute

Then REST call *generate_choice_codes* response Status Code is *200*
Then REST call *generate_choice_codes* response Body contains *choiceCodes*
