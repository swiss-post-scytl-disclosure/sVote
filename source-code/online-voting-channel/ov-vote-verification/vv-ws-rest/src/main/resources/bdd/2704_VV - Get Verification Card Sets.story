Given REST call *get_verification_card_sets* with URL */vv-ws-rest/verificationsets/tenant/100/electionevent/100/verificationcardset/100*
Given REST call *get_verification_card_sets* with Method *GET*
Given REST call *get_verification_card_sets* with Header *requestid* / *100*
Given REST call *get_verification_card_sets* with Header *Content-Type* / *application/json*

When REST call *get_verification_card_sets* execute

Then REST call *get_verification_card_sets* response Status Code is *200*
Then REST call *get_verification_card_sets* response Body contains *id ==~ /\w+/*
Then REST call *get_verification_card_sets* response Body contains *electionEventId ==~ /\w+/*
Then REST call *get_verification_card_sets* response Body contains *choiceCodesEncryptionPK ==~ /\w+/*
Then REST call *get_verification_card_sets* response Body contains *certificates.verificationCardIssuer*
Then REST call *get_verification_card_sets* response Body contains *certificates.voteCastCodeSigner*