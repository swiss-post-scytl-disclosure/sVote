Given REST call *generate_choice_codes_400_1* with URL */vv-ws-rest/choicecodes/tenant/100/electionevent/100/verificationcard/100*
Given REST call *generate_choice_codes_400_1* with Method *POST*
Given REST call *generate_choice_codes_400_1* with Header *Content-Type* / *application/json*
Given REST call *generate_choice_codes_400_1* with JSON Body:
{
  "encryptedOptions": "132412342134;asdfasdfasdf.AVeryLongStringWithTheEncryptedOptionsafasdf12312423asdfnmlourjvlmaldjfoqwer14ad34fas1avjsjjj;.",
  "partialChoiceCodes":"a1ds23f1as3df13asdf3aasdfasfdGeneratedPartialChoiceCodesadsfasdfasdfasdf"
}

When REST call *generate_choice_codes_400_1* execute

Then REST call *generate_choice_codes_400_1* response Status Code is *400*