/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.ui.ws.rs.application.operation;

import com.scytl.products.ov.commons.test.CryptoUtils;
import java.security.KeyPair;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;
import com.scytl.products.ov.commons.beans.domain.model.certificate.CertificateEntity;
import com.scytl.products.ov.commons.crypto.configuration.CertificateParameters;
import com.scytl.products.ov.commons.infrastructure.remote.service.RemoteCertificateService;
import com.scytl.products.ov.commons.infrastructure.remote.service.RemoteCertificateServiceImpl;

import com.scytl.products.ov.vv.infrastructure.remote.VvRemoteCertificateService;

@Stateless(name = "vvRemoteCertificateService")
@VvRemoteCertificateService
public class HideVvRemoteCertificateServiceImpl extends RemoteCertificateServiceImpl implements RemoteCertificateService{

    private KeyPair keyPair;

    protected static final String ELECTION_EVENT_ID = "67cd59d8183f47a3a4cc64abfcc2916b";

    protected static final String TENANT_ID = "100";

    protected static final String PLATFORM_NAME = "";

    protected static final String CERTIFICATE_NAME = "";
    
    @Override
    public CertificateEntity getAdminBoardCertificate(String id) {
        try {
            CryptoAPIX509Certificate cert =
                CryptoUtils.createCryptoAPIx509Certificate("certificate",
                    CertificateParameters.Type.SIGN, keyPair);
            String certificateContent = new String(cert.getPemEncoded(), "UTF-8");
            CertificateEntity certificateEntity = new CertificateEntity();
            certificateEntity.setCertificateContent(certificateContent);
            certificateEntity.setCertificateName(CERTIFICATE_NAME);
            certificateEntity.setElectionEventId(ELECTION_EVENT_ID);
            certificateEntity.setPlatformName(PLATFORM_NAME);
            certificateEntity.setTenantId(TENANT_ID);

            return certificateEntity;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostConstruct
    @Override
    public void intializeElectionInformationAdminClient() {
        keyPair = VoteVerificationArquillianDeployment.keyPair;
    }

}
