/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.ui.ws.rs.application.operation;

import java.io.FileNotFoundException;
import java.security.KeyPair;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.exporter.ArchiveExportException;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import com.scytl.cryptolib.scytl.keystore.service.ScytlKeyStoreService;
import com.scytl.products.ov.commons.logging.config.LoggingFilter;
import com.scytl.products.ov.commons.test.CryptoUtils;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.ApplicationExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.DuplicateEntryExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.GlobalExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.ResourceNotFoundExceptionHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.RetrofitErrorHandler;
import com.scytl.products.ov.commons.ui.ws.rs.exceptions.ValidationExceptionHandler;
import com.scytl.products.ov.filter.TenantFilter;
import com.scytl.products.ov.vv.infrastructure.health.HealthCheckRegistryProducer;
import com.scytl.products.ov.vv.infrastructure.remote.VvRemoteCertificateServiceImpl;
import com.scytl.products.ov.vv.service.CastCodesServiceImpl;
import com.scytl.products.ov.vv.service.ChoiceCodesServiceImpl;
import com.scytl.products.ov.vv.service.crypto.CertificateChainValidatorProducer;
import com.scytl.products.ov.vv.service.crypto.PrimitivesServiceProducer;
import com.scytl.products.ov.vv.ui.ws.rs.application.operation.VoteVerificationArquillianTest.PrimitivesServiceMock;

@ArquillianSuiteDeployment
public class VoteVerificationArquillianDeployment {

    public static KeyPair keyPair;

    static {
        Security.addProvider(new BouncyCastleProvider());
        keyPair = CryptoUtils.getKeyPairForSigning();
    }

    @Deployment
    public static WebArchive deploy() throws ArchiveExportException, IllegalArgumentException, FileNotFoundException {

        WebArchive webArchive =
            ShrinkWrap.create(WebArchive.class, "vv-ws-rest.war").addClasses(LoggingFilter.class, TenantFilter.class)
                .addPackages(true, Filters.exclude(CastCodesServiceImpl.class, PrimitivesServiceProducer.class),
                    "com.scytl.products.ov.vv")
                .addPackages(true, "org.h2")
                .addPackages(true,
                    Filters.exclude(ResourceNotFoundExceptionHandler.class, GlobalExceptionHandler.class,
                        ValidationExceptionHandler.class, ApplicationExceptionHandler.class,
                        DuplicateEntryExceptionHandler.class, RetrofitErrorHandler.class, BouncyCastleProvider.class,
                        ScytlKeyStoreService.class),
                    "com.scytl.products.ov.commons")
                .deleteClass(VvRemoteCertificateServiceImpl.class).deleteClass(ChoiceCodesServiceImpl.class)
                .addClass(HideVvRemoteCertificateServiceImpl.class).addClass(PrimitivesServiceMock.class)
                .deleteClass(HealthCheckRegistryProducer.class).addPackages(true, "com.fasterxml.jackson.jaxrs")
                .deleteClass(CertificateChainValidatorProducer.class)
                .addPackages(true, "org.bouncycastle.jce")
                .addAsManifestResource("test-persistence.xml", "persistence.xml").addAsWebInfResource("beans.xml");

        return webArchive;
    }
}
