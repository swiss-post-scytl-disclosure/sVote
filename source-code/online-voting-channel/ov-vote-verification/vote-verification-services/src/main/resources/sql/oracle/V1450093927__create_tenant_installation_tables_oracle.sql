--------------------------------------------------------
--  DDL for Sequence TENANT_KEYSTORES_SEQ
--------------------------------------------------------

CREATE SEQUENCE  "VV_TENANT_KEYSTORES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;


--------------------------------------------------------
--  DDL for Table TENANT_KEYSTORES
--------------------------------------------------------

CREATE TABLE "VV_TENANT_KEYSTORES"
(
"ID" NUMBER(11,0),
"PLATFORM_NAME" VARCHAR2(100 BYTE),
"TENANT_ID" VARCHAR2(100 BYTE),
"KEY_TYPE" VARCHAR2(100 BYTE),
"KEYSTORE_CONTENT" CLOB
) ;


--------------------------------------------------------
--  DDL for Index TENANT_KEYSTORES
--------------------------------------------------------

CREATE UNIQUE INDEX "VV_TENANT_KEYSTORES_UK" ON "VV_TENANT_KEYSTORES" ("PLATFORM_NAME","KEY_TYPE","TENANT_ID");



--------------------------------------------------------
--  Constraints for Table LOGGIN_KEYSTORES
--------------------------------------------------------

ALTER TABLE "VV_TENANT_KEYSTORES" ADD CONSTRAINT "VV_TENANT_KEYSTORES_UK" UNIQUE ("PLATFORM_NAME", "KEY_TYPE","TENANT_ID") ENABLE;
ALTER TABLE "VV_TENANT_KEYSTORES" ADD PRIMARY KEY ("ID") ENABLE;
ALTER TABLE "VV_TENANT_KEYSTORES" MODIFY ("TENANT_ID" NOT NULL ENABLE);
ALTER TABLE "VV_TENANT_KEYSTORES" MODIFY ("KEY_TYPE" NOT NULL ENABLE);
ALTER TABLE "VV_TENANT_KEYSTORES" MODIFY ("KEYSTORE_CONTENT" NOT NULL ENABLE);




