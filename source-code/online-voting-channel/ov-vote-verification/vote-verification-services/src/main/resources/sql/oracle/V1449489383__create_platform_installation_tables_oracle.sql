
--------------------------------------------------------
--  DDL for Sequence PLATFORM_CERTIFICATES_SEQ
--------------------------------------------------------
CREATE SEQUENCE  "VV_PLATFORM_CERTIFICATES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  "VV_LOGGING_KEYSTORES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;


--------------------------------------------------------
--  DDL for Table PLATFORMCA_CERTIFICATES
--------------------------------------------------------

  CREATE TABLE "VV_PLATFORM_CA_CERTIFICATES"
   (
   	"ID" NUMBER(11,0),
    "PLATFORM_NAME" VARCHAR2(100 BYTE),
	"CERTIFICATE_NAME" VARCHAR2(100 BYTE),
	"CERTIFICATE_CONTENT" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table LOGGING_KEYSTORES
--------------------------------------------------------

  CREATE TABLE "VV_LOGGING_KEYSTORES"
   (
    "ID" NUMBER(11,0),
    "PLATFORM_NAME" VARCHAR2(100 BYTE),
    "KEY_TYPE" VARCHAR2(100 BYTE),
	"KEYSTORE_CONTENT" CLOB
   ) ;


--------------------------------------------------------
--  DDL for Index PLATFORMCA_CERTIFICATES_UK
--------------------------------------------------------

CREATE UNIQUE INDEX "VV_PLATFORM_CA_CERTIFICATES_UK" ON "VV_PLATFORM_CA_CERTIFICATES" ("PLATFORM_NAME","CERTIFICATE_NAME");

--------------------------------------------------------
--  DDL for Index LOGGING_KEYSTORES_UK
--------------------------------------------------------

CREATE UNIQUE INDEX "VV_LOGGING_KEYSTORES_UK" ON "VV_LOGGING_KEYSTORES" ("PLATFORM_NAME","KEY_TYPE");


--------------------------------------------------------
--  Constraints for Table PLATFORMCA_CERTIFICATES
--------------------------------------------------------

  ALTER TABLE "VV_PLATFORM_CA_CERTIFICATES" ADD CONSTRAINT "VV_PLATFORMCA_CERTIFICATES_UK" UNIQUE ("PLATFORM_NAME", "CERTIFICATE_NAME") ENABLE;
  ALTER TABLE "VV_PLATFORM_CA_CERTIFICATES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "VV_PLATFORM_CA_CERTIFICATES" MODIFY ("PLATFORM_NAME" NOT NULL ENABLE);
  ALTER TABLE "VV_PLATFORM_CA_CERTIFICATES" MODIFY ("CERTIFICATE_CONTENT" NOT NULL ENABLE);
  ALTER TABLE "VV_PLATFORM_CA_CERTIFICATES" MODIFY ("CERTIFICATE_NAME" NOT NULL ENABLE);


--------------------------------------------------------
--  Constraints for Table PLATFORMCA_CERTIFICATES
--------------------------------------------------------

  ALTER TABLE "VV_LOGGING_KEYSTORES" ADD CONSTRAINT "VV_LOGGING_KEYSTORES_UK" UNIQUE ("PLATFORM_NAME", "KEY_TYPE") ENABLE;
  ALTER TABLE "VV_LOGGING_KEYSTORES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "VV_LOGGING_KEYSTORES" MODIFY ("PLATFORM_NAME" NOT NULL ENABLE);
  ALTER TABLE "VV_LOGGING_KEYSTORES" MODIFY ("KEY_TYPE" NOT NULL ENABLE);
  ALTER TABLE "VV_LOGGING_KEYSTORES" MODIFY ("KEYSTORE_CONTENT" NOT NULL ENABLE);

