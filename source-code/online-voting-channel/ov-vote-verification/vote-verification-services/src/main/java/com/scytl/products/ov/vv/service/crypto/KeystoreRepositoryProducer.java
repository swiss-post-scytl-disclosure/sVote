/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.products.ov.commons.crypto.KeystoreForObjectRepository;
import com.scytl.products.ov.vv.domain.model.content.CodesSecretKeystoreRepository;

/**
 * Password repository producer for different keystore passwords
 */

public class KeystoreRepositoryProducer {

    @Inject
    private CodesSecretKeystoreRepository codesSecretKeystoreRepository;

    @Produces
    @Secret
    public KeystoreForObjectRepository getSecretInstance() {

        return codesSecretKeystoreRepository;
    }

}
