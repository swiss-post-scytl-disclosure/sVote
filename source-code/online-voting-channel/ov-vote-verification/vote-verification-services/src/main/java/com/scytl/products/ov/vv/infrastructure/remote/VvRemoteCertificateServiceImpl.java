/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.remote;

import javax.ejb.Stateless;

import com.scytl.products.ov.commons.infrastructure.remote.service.RemoteCertificateService;
import com.scytl.products.ov.commons.infrastructure.remote.service.RemoteCertificateServiceImpl;

@Stateless(name = "vvRemoteCertificateService")
@VvRemoteCertificateService
public class VvRemoteCertificateServiceImpl extends RemoteCertificateServiceImpl implements RemoteCertificateService{
}
