/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.verification.Verification;
import com.scytl.products.ov.vv.domain.model.verification.VerificationRepository;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogConstants;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogEvents;

/**
 * Implementation of the repository with JPA
 */
@Decorator
public abstract class VerificationRepositoryDecorator implements VerificationRepository {

    @Inject
    @Delegate
    private VerificationRepository verificationRepository;

    @Inject
    private SecureLoggingWriter secureLoggingWriter;

    @Inject
    private TrackIdInstance trackId;

    @Override
    public Verification save(final Verification entity) throws DuplicateEntryException {
        try {
            final Verification verification = verificationRepository.save(entity);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_DATA_SAVED)
                    .objectId(entity.getVerificationCardId()).user("admin")
                    .electionEvent(entity.getElectionEventId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .createLogInfo());
            return verification;
        } catch (DuplicateEntryException ex) {
            secureLoggingWriter
                .log(Level.ERROR,
                    new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.ERROR_SAVING_VERIFICATION_CARD_DATA)
                        .objectId(entity.getVerificationCardId()).user("admin")
                        .electionEvent(entity.getElectionEventId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC, ex.getMessage())
                        .createLogInfo());
            throw ex;
        }
    }

    @Override
    public Verification findByTenantIdElectionEventIdVerificationCardId(final String tenantId,
                                                                        final String electionEventId,
                                                                        final String verificationCardId)
        throws ResourceNotFoundException {
        try {
            Verification result = verificationRepository.findByTenantIdElectionEventIdVerificationCardId(tenantId,
                electionEventId, verificationCardId);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_DATA_FOUND)
                    .objectId(verificationCardId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_ID, verificationCardId)
                    .createLogInfo());
            return result;
        } catch (ResourceNotFoundException e) {
            secureLoggingWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_DATA_NOT_FOUND)
                    .objectId(verificationCardId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_ID, verificationCardId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC,
                        "Resource not found in the VERIFICATION_DATA table.")
                    .createLogInfo());
            throw e;
        }
    }

    @Override
    public boolean hasWithTenantIdElectionEventIdVerificationCardId(final String tenantId, final String electionEventId,
                                                                    final String verificationCardId) {
        return verificationRepository
            .hasWithTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId, verificationCardId);
    }
}
