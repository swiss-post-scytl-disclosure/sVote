/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.log;

/**
 * Constants for logging in vote verification.
 */
public class VoteVerificationLogConstants {

	/**
     * Verification card id - additional information.
     */
    public static final String INFO_VERIFICATION_CARD_ID = "#verifc_id";

    /**
     * Verification card set id - additional information.
     */
    public static final String INFO_VERIFICATION_CARD_SET_ID = "#verifc_set_id";

    /**
     * Track id - additional information.
     */
    public static final String INFO_TRACK_ID = "#request_id";

    /**
     * Error desc id - additional information.
     */
    public static final String INFO_ERR_DESC = "#err_desc";

    /**
     * Vote hash - additional info.
     */
    public static final String INFO_HASH_VOTE = "#hashVote";

    /**
     * Electoral authority id - additional information.
     */
    public static final String INFO_ELECTORAL_AUTHORITY_ID = "#ea_id";

    public static final String CONTROL_COMPONENT_ID = "#ccx_id";

    public static final String VERIFICATION_CARD_ID = "#verifc_id";

    public static final String HASH_PCC = "#hash_pCC";

    /**
	 * Non-public constructor
	 */
	private VoteVerificationLogConstants() {
    }
}
