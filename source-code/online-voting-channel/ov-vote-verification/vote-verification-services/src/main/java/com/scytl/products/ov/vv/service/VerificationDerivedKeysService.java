/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.vv.domain.model.verification.VerificationDerivedKeys;
import com.scytl.products.ov.vv.domain.model.verification.VerificationDerivedKeysRepository;

@Stateless
public class VerificationDerivedKeysService {

	private static final Logger LOG = LoggerFactory.getLogger(VerificationDerivedKeysService.class);
	
    @EJB
    private VerificationDerivedKeysRepository verificationDerivedKeysRepository;

    public void save(String tenantId, String electionEventId, String verificationCardId,
            String choiceCodeDerivedKeyCommitment, String ballotCastingKeyDerivedExponentCommitment)
            throws DuplicateEntryException {

        LOG.info("Saving derived key for tenantId {} electionEventId {} verificationCardId {} ", tenantId,
            electionEventId, verificationCardId);

        VerificationDerivedKeys entity = new VerificationDerivedKeys();
        entity.setTenantId(tenantId);
        entity.setElectionEventId(electionEventId);
        entity.setVerificationCardId(verificationCardId);
        entity.setCcodeDerivedKeyCommitment(choiceCodeDerivedKeyCommitment);
        entity.setBckDerivedExpCommitment(ballotCastingKeyDerivedExponentCommitment);

        verificationDerivedKeysRepository.save(entity);
    }

}
