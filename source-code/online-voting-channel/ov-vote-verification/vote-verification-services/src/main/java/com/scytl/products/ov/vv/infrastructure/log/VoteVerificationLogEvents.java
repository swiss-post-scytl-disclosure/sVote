/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Enum with the log events for this context.
 */
public enum VoteVerificationLogEvents implements LogEvent {

	VERIFICATION_CARD_DATA_FOUND("VCDRET", "000", "Verification Card Data found"),
	VERIFICATION_CARD_DATA_NOT_FOUND("VCDRET", "400", "Verification Card Data not found"),
	
	VERIFICATION_CARD_SET_DATA_FOUND("VCSDRET", "000", "Verification Card Set Data found"),
	VERIFICATION_CARD_SET_DATA_NOT_FOUND("VCSDRET", "401", "Verification Card Set Data not found"),
	
	CHOICE_CODES_RETRIEVED("CCRET", "000"," Choice Codes correctly retrieved"),
	ERROR_DECRYPTING_PARTIAL_CHOICE_CODES("CCRET", "402","Partial Choice Codes can not be decrypted"),
	ERROR_COMPUTING_LONG_CHOICE_CODES("CCRET", "403", "Error while computing the the Long Choice Codes"),
	ERROR_RETRIEVING_CHOICE_CODES("CCRET", "404", "Choice Codes can not be retrieved"),
	ERROR_VOTE_VALIDATION("CCRET", "405", "Validations over the structure of the vote failed"),
	
	VALID_VOTE("VOTVA", "000", "Successful vote validation"),
	INVALID_EXPONENTIATION_PROOF("VOTVA", "406", "Exponentiation proof not valid"),
	INVALID_PLAINTEXT_PROOF("VOTVA", "407", "Plaintext equality proof not valid"),
	INVALID_VERIFICATION_CARD_PK_SIGNATURE("VOTVA", "411", "Verification Card Public Key signature not valid"),
    INVALID_WRITEINS("VOTVA", "422", "Writeins not valid"),
	
	VOTE_CAST_CODE_RETRIEVED("VCCRETM", "000", "Vote Cast Code retrieved"),
	ERROR_COMPUTING_LONG_VOTE_CAST_CODE("VCCRETM", "408", "Error while computing the Long Vote Cast Code"),
	ERROR_RETRIEVING_VOTE_CAST_CODE("VCCRETM", "409", "Vote Cast Code can not be retrieved"),
	ERROR_INVALID_SIGNATURE_VOTE_CAST_CODE("VCCRETM", "410", "Vote Cast Code signature not valid"),
    VOTE_CAST_CODE_SIGNATURE_VALIDATED("VCCRETM", "412", "Vote Cast Code signature validate"),

	VERIFICATION_CONTENT_DATA_FOUND("VCSDRET", "000", "Verification Content Data found"),
	VERIFICATION_CONTENT_DATA_NOT_FOUND("VCSDRET", "401", "Verification Content Data not found"),

    ELECTORAL_AUTHORITY_FOUND("EARET", "402", "Electoral Authority found"),
    ELECTORAL_AUTHORITY_NOT_FOUND("EARET", "403", "Electoral Authority  not found"),

	CODES_MAPPING_DATA_SAVED("CMDSTOR", "000", "Codes mapping data saved successfully"),
	ERROR_SAVING_CODES_MAPPING_DATA("CMDSTOR", "413", "Error saving codes mapping data"),

    VERIFICATION_CARD_DATA_SAVED("VCDSTOR", "414", "Verification card data saved successfully"),
    ERROR_SAVING_VERIFICATION_CARD_DATA("VCDSTOR", "415", "Error saving verification card data"),
    VERIFICATION_CARD_SET_DATA_SAVED("VCSSTOR", "416", "Verification card set data saved successfully"),
    ERROR_SAVING_VERIFICATION_CARD_SET_DATA("VCSSTOR", "417", "Error saving verification card set data"),
    VERIFICATION_CONTENT_DATA_SAVED("VCSDSTOR", "418", "Verification Content Data saved"),
    ERROR_SAVING_VERIFICATION_CONTENT_SET_DATA("VCSDSTOR", "419", "Error saving Verification Content Data"),
    ELECTORAL_AUTHORITY_SAVED("EASTOR", "420", "Electoral Authority saved successfully"),
    ERROR_SAVING_ELECTORAL_AUTHORITY("EASTOR", "421", "Error saving Electoral Authority"),
    
    VERIFICATION_CARD_DERIVED_KEYS_FOUND("VCDRET", "000", "Verification Card Derived keys found"),
    VERIFICATION_CARD_DERIVED_KEYS_NOT_FOUND("VCDRET", "422", "Verification Card Derived Keys not found"),
    PRIVATE_KEY_SUCCESFULLY_COMPUTED("PVCCCOMP", "000", "Private key successfully computed"),
    CONTROL_COMPONENT_COMPUTE_CONTRIBUTION_SUCCESFULLY_RECIEVED("COMPCREQ", "000", "Control component computation contribution correctly received"),
    PARTIAL_VOTE_CAST_CODE_CANNOT_BE_COMPUTED("PVCCREQ","X166", "Partial Vote Cast Code cannot be computed"),
    PARTIAL_DECRYPTION_SUCESSFULLY_CONSOLIDATED("PDCON","000","Partial decryption successfuly consolidated"),
    PARTIAL_CHOICE_CODE_CONSOLIDATED("PCCCON","000","Partial Choice Codes correctly consolidated"), 
    PARTIAL_VOTE_CAST_CODE_CORRECTLY_CONSOLIDATED("PVCCON  ","000","Partial Vote Cast Code correctly consolidated"),
    PARTIAL_CODES_CANNOT_BE_CONSOLIDATED("CCON","X139","Partial Codes cannot be consolidated"), 
    PROOF_NOT_VALID_PDC("PDCON","X152","Proof not valid"),
    PROOF_NOT_VALID_COMP("PROOFCOMP","X137","Proof not valid"), 
    PARTIAL_DECRYPTIONS_CANNOT_BE_CONSOLIDATED("PDCON","X156","Partial Decryptions cannot be consolidated"),
    FAILED_CONFIRMATION_MESSAGE_VALIDATION("PVCCREQ","X165","Confirmation Message validation has failed"),
    PARTIAL_DECRYPTION_CORRECTLY_RECEIVED("PDREQ","000","Partial decryption correctly received"),
    ; 

	private final String layer;

	private final String action;

	private final String outcome;

	private final String info;

	VoteVerificationLogEvents(final String action, final String outcome, final String info) {
		this.layer = "";
		this.action = action;
		this.outcome = outcome;
		this.info = info;
	}

	/**
	 * @see VoteVerificationLogEvents#getAction()
	 */
	@Override
	public String getAction() {
		return action;
	}

	/**
	 * @see VoteVerificationLogEvents#getOutcome()
	 */
	@Override
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @see VoteVerificationLogEvents#getInfo()
	 */
	@Override
	public String getInfo() {
		return info;
	}

	/**
	 * @see VoteVerificationLogEvents#getLayer()
	 */
	@Override
	public String getLayer() {
		return layer;
	}
}
