/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.verification;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Provides operations on the verification repository.
 */
@Local
public interface VerificationRepository extends BaseRepository<Verification, Integer> {

	/**
	 * Returns a verification data for a given tenant, election event and verification card.
	 *
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @param verificationCardId - the verification card identifier.
	 * @return The verification data.
	 * @throws ResourceNotFoundException if the verification data is not found.
	 */
	Verification findByTenantIdElectionEventIdVerificationCardId(String tenantId, String electionEventId, String verificationCardId)
			throws ResourceNotFoundException;
	
	/**
     * Returns whether exists a verification data for given tenant, election event and verification card.
     *
     * @param tenantId - the tenant identifier
     * @param electionEventId - the election event identifier
     * @param verificationCardId - the verification card identifier
     * @return the data exists.
     */
    boolean hasWithTenantIdElectionEventIdVerificationCardId(String tenantId, String electionEventId, String verificationCardId);
}
