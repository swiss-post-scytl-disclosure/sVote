/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.content.CodesMappingEntity;
import com.scytl.products.ov.vv.domain.model.content.CodesMappingRepository;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogConstants;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogEvents;

/**
 * Implementation of the repository with JPA
 */
@Decorator
public abstract class CodesMappingRepositoryDecorator implements CodesMappingRepository {

    @Inject
    @Delegate
    private CodesMappingRepository codesMappingRepository;

    @Inject
    private SecureLoggingWriter secureLoggingWriter;

    @Inject
    private TrackIdInstance trackId;

    @Override
    public CodesMappingEntity save(final CodesMappingEntity entity) throws DuplicateEntryException {
        try {
            final CodesMappingEntity codesMappingEntity = codesMappingRepository.save(entity);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.CODES_MAPPING_DATA_SAVED)
                    .objectId(entity.getVerificationCardId()).user("admin")
                    .electionEvent(entity.getElectionEventId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .createLogInfo());
            return codesMappingEntity;
        } catch (DuplicateEntryException ex) {
            secureLoggingWriter
                .log(Level.ERROR,
                    new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.ERROR_SAVING_CODES_MAPPING_DATA)
                        .objectId(entity.getVerificationCardId()).user("admin")
                        .electionEvent(entity.getElectionEventId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC, ex.getMessage())
                        .createLogInfo());
            throw ex;
        }
    }

    @Override
    public CodesMappingEntity findByTenantIdElectionEventIdVerificationCardId(final String tenantId,
                                                                              final String electionEventId,
                                                                              final String verificationCardId)
        throws ResourceNotFoundException {
        return codesMappingRepository.findByTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId, verificationCardId);
    }

    @Override
    public boolean hasWithTenantIdElectionEventIdVerificationCardId(final String tenantId, final String electionEventId,
                                                                    final String verificationCardId) {
        return codesMappingRepository.hasWithTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId, verificationCardId);
    }
}
