/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.products.ov.commons.verify.PayloadSigningCertificateValidator;
import com.scytl.products.ov.commons.verify.CryptolibPayloadVerifier;
import com.scytl.products.ov.commons.verify.PayloadVerifier;

/**
 * Producer of Payload signature verifier
 */
public class PayloadVerifierProducer {
    
    @Inject
    private AsymmetricServiceAPI asymmetricServiceAPI;
    
    @Inject
    private PayloadSigningCertificateValidator certificateChainValidator;
    
    @Produces
    @ApplicationScoped
    public PayloadVerifier payloadVerifier() {
        return new CryptolibPayloadVerifier(asymmetricServiceAPI, certificateChainValidator);
    }

}


