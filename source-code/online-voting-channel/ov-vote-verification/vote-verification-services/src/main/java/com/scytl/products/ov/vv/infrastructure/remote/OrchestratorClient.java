/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.remote;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MediaType;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;

import com.scytl.products.ov.commons.dto.PartialCodesInput;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

/**
 * Defines the methods to access via REST to a set of operations.
 */
public interface OrchestratorClient {

    String PATH_ORCHESTRATOR = "pathOrchestrator";

    String TENANT_ID = "tenantId";

    String ELECTION_EVENT_ID = "electionEventId";

    String VERIFICATION_CARD_SET_ID = "verificationCardSetId";

    String VERIFICATION_CARD_ID = "verificationCardId";

    /**
     * Requests the Control Components contributions for the decryption of the
     * partial choice codes
     *
     * @param pathOrchestrator
     *            the path to the orchestrator
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param verificationCardSetId
     *            the verification card set id
     * @param verificationCardId
     *            the verification card id
     * @return the choice code nodes contributions necessary for decryption
     * @throws ResourceNotFoundException
     *             the resource not found exception
     */
    @POST("{pathOrchestrator}/tenant/{tenantId}/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}/decryptContributions")
    @Headers("Accept:" + MediaType.APPLICATION_JSON)
    Call<ResponseBody> getChoiceCodeNodesDecryptContributions(
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Path(PATH_ORCHESTRATOR) String pathOrchestrator, @Path(TENANT_ID) String tenantId,
            @Path(ELECTION_EVENT_ID) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID) String verificationCardSetId,
            @Path(VERIFICATION_CARD_ID) String verificationCardId, @NotNull @Body RequestBody gamma)
            throws ResourceNotFoundException;

    /**
     * Requests the Control Components contributions for the computation of the
     * partial choice codes
     *
     * @param pathOrchestrator
     *            the path to the orchestrator
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param verificationCardSetId
     *            the verification card set id
     * @param verificationCardId
     *            the verification card id
     * @return the choice code nodes contributions necessary for computation
     */
    @POST("{pathOrchestrator}/tenant/{tenantId}/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}/computeVotingContributions")
    @Headers("Accept:" + MediaType.APPLICATION_OCTET_STREAM)
    @Streaming
    Call<ResponseBody> getChoiceCodeNodesComputeContributions(
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Path(PATH_ORCHESTRATOR) String pathOrchestrator, @Path(TENANT_ID) String tenantId,
            @Path(ELECTION_EVENT_ID) String electionEventId,
            @Path(VERIFICATION_CARD_SET_ID) String verificationCardSetId,
            @Path(VERIFICATION_CARD_ID) String verificationCardId,
            @NotNull @Body PartialCodesInput partialCodesInput);

}
