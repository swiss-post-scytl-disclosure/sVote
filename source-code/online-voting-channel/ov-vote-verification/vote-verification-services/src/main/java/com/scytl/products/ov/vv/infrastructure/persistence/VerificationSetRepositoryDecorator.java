/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.verificationset.VerificationSetEntity;
import com.scytl.products.ov.vv.domain.model.verificationset.VerificationSetRepository;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogConstants;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogEvents;

/**
 * Implementation of the repository with JPA
 */
@Decorator
public abstract class VerificationSetRepositoryDecorator implements VerificationSetRepository {

    @Inject
    @Delegate
    private VerificationSetRepository verificationSetRepository;

    @Inject
    private SecureLoggingWriter secureLoggingWriter;

    @Inject
    private TrackIdInstance trackId;

    @Override
    public VerificationSetEntity save(final VerificationSetEntity entity) throws DuplicateEntryException {
        try {
            final VerificationSetEntity verificationSet = verificationSetRepository.save(entity);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_SET_DATA_SAVED)
                    .objectId(entity.getVerificationCardSetId()).user("admin")
                    .electionEvent(entity.getElectionEventId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .createLogInfo());
            return verificationSet;
        } catch (DuplicateEntryException ex) {
            secureLoggingWriter
                .log(Level.ERROR,
                    new LogContent.LogContentBuilder()
                        .logEvent(VoteVerificationLogEvents.ERROR_SAVING_VERIFICATION_CARD_SET_DATA)
                        .objectId(entity.getVerificationCardSetId()).user("admin")
                        .electionEvent(entity.getElectionEventId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC, ex.getMessage())
                        .createLogInfo());
            throw ex;
        }
    }

    @Override
    public VerificationSetEntity findByTenantIdElectionEventIdVerificationCardSetId(final String tenantId,
                                                                                    final String electionEventId,
                                                                                    final String verificationCardSetId)
        throws ResourceNotFoundException {
        try {
            final VerificationSetEntity verificationSetEntity = verificationSetRepository
                .findByTenantIdElectionEventIdVerificationCardSetId(tenantId, electionEventId, verificationCardSetId);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_SET_DATA_FOUND)
                    .objectId(verificationCardSetId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_SET_ID, verificationCardSetId)
                    .createLogInfo());
            return verificationSetEntity;
        } catch (ResourceNotFoundException e) {
            secureLoggingWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_SET_DATA_NOT_FOUND)
                    .objectId(verificationCardSetId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_SET_ID, verificationCardSetId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC,
                        "Resource not found in the VERIFICATION_SET_DATA table.")
                    .createLogInfo());
            throw e;
        }
    }
}
