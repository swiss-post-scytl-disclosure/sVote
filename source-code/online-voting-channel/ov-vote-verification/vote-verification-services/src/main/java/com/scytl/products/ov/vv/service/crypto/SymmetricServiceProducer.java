/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.symmetric.SymmetricServiceAPI;
import com.scytl.cryptolib.symmetric.service.SymmetricServiceFactoryHelper;

/**
 * Symmetric Service Producer.
 */
public class SymmetricServiceProducer {

    private static final String MAX_ELEMENTS_CRYPTO_POOL = "symmetric.max.elements.crypto.pool";

    /**
     * Returns a symmetric service instance.
     * 
     * @return a symmetric service.
     */
    @Produces
    @ApplicationScoped
    public SymmetricServiceAPI getInstance() {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(Integer.parseInt(System.getProperty(MAX_ELEMENTS_CRYPTO_POOL)));
        try {
            return SymmetricServiceFactoryHelper.getFactoryOfThreadSafeServices(config).create();
        } catch (GeneralCryptoLibException e) {
            throw new IllegalStateException("Exception while trying to create SymmetricService", e);
        }
    }
}
