/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.primitives.service.PrimitivesServiceFactoryHelper;

/**
 * Primitives Service Producer.
 */
public class PrimitivesServiceProducer {

    private static final String MAX_ELEMENTS_CRYPTO_POOL = "primitives.max.elements.crypto.pool";

    /**
     * Returns a primitives service instance.
     * 
     * @return a primitives service .
     */
    @Produces
    @ApplicationScoped
    public PrimitivesServiceAPI getInstance() {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(Integer.parseInt(System.getProperty(MAX_ELEMENTS_CRYPTO_POOL)));
        try {
            return PrimitivesServiceFactoryHelper.getFactoryOfThreadSafeServices(config).create();
        } catch (GeneralCryptoLibException e) {
            throw new IllegalStateException("Exception while trying to create PrimitivesService", e);
        }
    }
}
