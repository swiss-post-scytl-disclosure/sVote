/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.verification.VerificationDerivedKeys;
import com.scytl.products.ov.vv.domain.model.verification.VerificationDerivedKeysRepository;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogConstants;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogEvents;

/**
 * Implementation of the repository with JPA
 */
@Decorator
public abstract class VerificationDerivedKeysRepositoryDecorator implements VerificationDerivedKeysRepository {

    @Inject
    @Delegate
    private VerificationDerivedKeysRepository verificationDerivedKeysRepository;

    @Inject
    private SecureLoggingWriter secureLoggingWriter;

    @Inject
    private TrackIdInstance trackId;

    @Override
    public VerificationDerivedKeys findByTenantIdElectionEventIdVerificationCardId(final String tenantId,
            final String electionEventId, final String verificationCardId) throws ResourceNotFoundException {
        try {
            VerificationDerivedKeys result = verificationDerivedKeysRepository
                .findByTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId, verificationCardId);

            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_DERIVED_KEYS_FOUND)
                    .objectId(verificationCardId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_ID, verificationCardId)
                    .createLogInfo());
            return result;
        } catch (ResourceNotFoundException e) {
            secureLoggingWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CARD_DERIVED_KEYS_NOT_FOUND)
                    .objectId(verificationCardId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_ID, verificationCardId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC,
                        "Resource not found in the VERIFICATION_DERIVED_KEYS table.")
                    .createLogInfo());
            throw e;
        }
    }
}
