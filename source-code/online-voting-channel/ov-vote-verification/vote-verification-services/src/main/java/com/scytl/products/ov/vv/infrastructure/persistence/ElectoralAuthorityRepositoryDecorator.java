/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.content.ElectoralAuthorityEntity;
import com.scytl.products.ov.vv.domain.model.content.ElectoralAuthorityRepository;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogConstants;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogEvents;

/**
 * Electoral Authority repository decorator that adds secure logging
 */
@Decorator
public abstract class ElectoralAuthorityRepositoryDecorator implements ElectoralAuthorityRepository {

    @Inject
    @Delegate
    private ElectoralAuthorityRepository electoralAuthorityRepository;

    @Inject
    private SecureLoggingWriter secureLoggingWriter;

    @Inject
    private TrackIdInstance trackId;

    @Override
    public ElectoralAuthorityEntity save(final ElectoralAuthorityEntity entity) throws DuplicateEntryException {
        try {
            final ElectoralAuthorityEntity codesMappingEntity = electoralAuthorityRepository.save(entity);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.ELECTORAL_AUTHORITY_SAVED)
                    .objectId(entity.getElectoralAuthorityId()).user("admin")
                    .electionEvent(entity.getElectionEventId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .createLogInfo());
            return codesMappingEntity;
        } catch (DuplicateEntryException ex) {
            secureLoggingWriter
                .log(Level.ERROR,
                    new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.ERROR_SAVING_ELECTORAL_AUTHORITY)
                        .objectId(entity.getElectoralAuthorityId()).user("admin")
                        .electionEvent(entity.getElectionEventId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC, ex.getMessage())
                        .createLogInfo());
            throw ex;
        }
    }

    @Override
    public ElectoralAuthorityEntity findByTenantIdElectionEventIdElectoralAuthorityId(final String tenantId,
                                                                                      final String electionEventId,
                                                                                      final String electoralAuthorityId)
        throws ResourceNotFoundException {
        try {
            final ElectoralAuthorityEntity authorityEntity =
                electoralAuthorityRepository
                    .findByTenantIdElectionEventIdElectoralAuthorityId(tenantId, electionEventId, electoralAuthorityId);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.ELECTORAL_AUTHORITY_FOUND)
                    .objectId(electoralAuthorityId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_ELECTORAL_AUTHORITY_ID, electoralAuthorityId)
                    .createLogInfo());
            return authorityEntity;
        } catch (ResourceNotFoundException ex) {
            secureLoggingWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.ELECTORAL_AUTHORITY_NOT_FOUND)
                    .objectId(electoralAuthorityId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_ELECTORAL_AUTHORITY_ID, electoralAuthorityId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC,
                        VoteVerificationLogEvents.ELECTORAL_AUTHORITY_NOT_FOUND.getInfo())
                    .createLogInfo());
            throw ex;
        }
    }
}
