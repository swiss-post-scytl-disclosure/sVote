/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.rule;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.domain.model.rule.AbstractRule;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.vv.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogConstants;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogEvents;

/**
 * Decorator of the encrypted options id rule.
 */
@Decorator
public class VerifyVerificationCardPKSignatureRuleDecorator implements AbstractRule<Vote> {

    @Inject
    @Delegate
    private VerifyVerificationCardPKSignatureRule encryptedPartialChoiceCodesRule;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;

    @Inject
    private SecureLoggerHelper secureLoggerHelper;

    @Inject
    private TrackIdInstance trackId;

    /**
     * @see com.scytl.products.ov.commons.domain.model.rule.AbstractRule#execute(java.lang.Object)
     */
    @Override
    public ValidationError execute(Vote vote) {
        ValidationError result = encryptedPartialChoiceCodesRule.execute(vote);
        if (!result.getValidationErrorType().equals(ValidationErrorType.SUCCESS)) {
            String voteHash = secureLoggerHelper.hash(vote);
            secureLoggerWriter.log(Level.ERROR, new LogContent.LogContentBuilder()
                .logEvent(VoteVerificationLogEvents.INVALID_VERIFICATION_CARD_PK_SIGNATURE)
                .objectId(vote.getVerificationCardId()).user(vote.getVerificationCardId())
                .electionEvent(vote.getElectionEventId())
                .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                .additionalInfo(VoteVerificationLogConstants.INFO_HASH_VOTE, voteHash)
                .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_ID, vote.getVerificationCardId())
                .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_SET_ID,
                    vote.getVerificationCardSetId())
                .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC, result.getErrorArgs()[0]).createLogInfo());
        }
        return result;
    }

    /**
     * @see com.scytl.products.ov.commons.domain.model.rule.AbstractRule#getName()
     */
    @Override
    public String getName() {
        return encryptedPartialChoiceCodesRule.getName();
    }
}
