/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.verificationset;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * The Class representing verification set data.
 */
public class VerificationSet {

	/**
	 * The identifier of this verification set.
	 */
	@JsonProperty("id")
	private String id;

	/**
	 * The choices codes public key and certificates information for verification purposes.
	 */
	@JsonProperty("data")
	private VerificationSetData data;

	@JsonProperty("signature")
	private String signature;

	/**
	 * Gets the value of field id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the value of field id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the current value of the field data.
	 *
	 * @return Returns the data.
	 */
	public VerificationSetData getData() {
		return data;
	}

	/**
	 * Sets the value of the field data.
	 *
	 * @param data The data to set.
	 */
	public void setData(VerificationSetData data) {
		this.data = data;
	}

	/**
	 * Returns the current value of the field signature.
	 *
	 * @return Returns the signature.
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets the value of the field signature.
	 *
	 * @param signature The signature to set.
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}
}
