/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
<<<<<<< HEAD
 * @author riglesias & rpadurean
 * @date Jul 07 , 2015 12:38 
 *
 * Copyright (C) 2015 Scytl Secure Electronic Voting SA 
 *
 * All rights reserved.
 *
 */
package com.scytl.products.ov.vv.domain.model.rule;

/**
 * The rules defined in this context.
 */
public enum RuleNames {

	VOTE_PLAINTEXT_EQUALITY_PROOF("plaintext_equality_proof"),
	VOTE_EXPONENTIATION_PROOF("exponentiation_proof"), 
	VOTE_VERIFICATION_CARD_PUBLIC_KEY_SIGNATURE_VALIDATION("verification_card_public_key_signature_validation");

	/**
	 * The actual name of the rule.
	 */
	private String text;

	private RuleNames(String name) {
		this.text = name;
	}

	/**
	 * Returns the current text of the field text.
	 *
	 * @return Returns the text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns the enum element for a given text.
	 * @param ruleName2Find The rule to be searched in the enum.
	 * @return a RulesName element having the given text.
	 */
	public static RuleNames getRuleName4Text(String ruleName2Find) {
		RuleNames result = null;
		for (RuleNames ruleName : RuleNames.values()) {
			if (ruleName.getText().equals(ruleName2Find)) {
				result = ruleName;
				break;
			}
		}
		return result;
	}
}
