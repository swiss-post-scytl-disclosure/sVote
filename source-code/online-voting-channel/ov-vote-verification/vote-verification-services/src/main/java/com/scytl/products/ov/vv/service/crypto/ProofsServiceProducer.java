/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.cryptolib.proofs.service.ProofsServiceFactoryHelper;

/**
 * This is a producer for proofs service in order to have a thread safe one.
 */
public class ProofsServiceProducer {

    private static final String MAX_ELEMENTS_CRYPTO_POOL = "proofs.max.elements.crypto.pool";

    /**
     * Returns a instance of the service.
     * 
     * @return an instance of proofs service.
     */
    @Produces
    @ApplicationScoped
    public ProofsServiceAPI getInstance() {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(Integer.parseInt(System.getProperty(MAX_ELEMENTS_CRYPTO_POOL)));
        try {
            return ProofsServiceFactoryHelper.getFactoryOfThreadSafeServices(config).create();
        } catch (GeneralCryptoLibException e) {
            throw new IllegalStateException("Exception while trying to create ProofsService", e);
        }
    }
}
