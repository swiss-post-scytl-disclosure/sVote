/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.verificationset;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scytl.products.ov.commons.beans.errors.SemanticErrorGroup;
import com.scytl.products.ov.commons.beans.errors.SyntaxErrorGroup;
import com.scytl.products.ov.commons.domain.model.Constants;

/**
 * Entity which contains the information of verification set data
 */
@Entity
@Table(name = "VERIFICATION_SET_DATA")
public class VerificationSetEntity {

	/**
	 * The identifier of this verification set.
	 */
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "verificationSetSeq")
	@SequenceGenerator(name = "verificationSetSeq", sequenceName = "VERIFICATION_SET_DATA_SEQ")
	@JsonIgnore
	private Integer id;

	/**
	 * The identifier of the tenant, to which this verification belongs.
	 */
	@Column(name = "TENANT_ID")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Size(max = Constants.COLUMN_LENGTH_100, groups = SemanticErrorGroup.class)
	private String tenantId;

	/** The external id provided. */
	@Column(name = "VERIFICATION_CARD_SET_ID")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Size(max = Constants.COLUMN_LENGTH_100, groups = SemanticErrorGroup.class)
	@JsonProperty(value = "id")
	private String verificationCardSetId;

	/**
	 * The identifier of the election event, to which this verification belongs.
	 */
	@Column(name = "ELECTION_EVENT_ID")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Size(max = Constants.COLUMN_LENGTH_100, groups = SemanticErrorGroup.class)
	private String electionEventId;

	/**
	 * The choices codes public key value information and certificates
	 */
	@Column(name = "JSON")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Lob
	private String JSON;

	/**
	 * The verification card set signature.
	 */
	@Column(name = "SIGNATURE")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Lob
	private String signature;

	/**
	 * Returns the current value of the field id.
	 *
	 * @return Returns the id.
	 */
	@JsonIgnore
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the value of the field id.
	 *
	 * @param id The id to set.
	 */
	@JsonIgnore
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Returns the current value of the field tenantId.
	 *
	 * @return Returns the tenantId.
	 */
	@JsonIgnore
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Sets the value of the field tenantId.
	 *
	 * @param tenantId The tenantId to set.
	 */
	@JsonProperty
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * Returns the current value of the field electionEventId.
	 *
	 * @return Returns the electionEventId.
	 */
	@JsonIgnore
	public String getElectionEventId() {
		return electionEventId;
	}

	/**
	 * Sets the value of the field electionEventId.
	 *
	 * @param electionEventId The electionEventId to set.
	 */
	@JsonProperty
	public void setElectionEventId(String electionEventId) {
		this.electionEventId = electionEventId;
	}

	/**
	 * Gets the value of the field verificationSetId.
	 *
	 * @return the verification set id
	 */
	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	/**
	 * Sets the value of field verificationCardSetId.
	 *
	 * @param verificationCardSetId the new verification card set id
	 */
	public void setVerificationCardSetId(String verificationCardSetId) {
		this.verificationCardSetId = verificationCardSetId;
	}

	/**
	 * Gets the value of field JSON
	 *
	 * @return
	 */
	@JsonProperty("data")
	public String getJSON() {
		return JSON;
	}

	/**
	 * Sets teh value of field JSON
	 * 
	 * @param JSON
	 */
	public void setJSON(String JSON) {
		this.JSON = JSON;
	}

	/**
	 * Returns the current value of the field signature.
	 *
	 * @return Returns the signature.
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets the value of the field signature.
	 *
	 * @param signature The signature to set.
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

}
