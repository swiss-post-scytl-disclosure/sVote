/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.dto.ChoiceCodesVerificationResPayload;
import com.scytl.products.ov.commons.validation.ValidationResult;

/**
 *
 */
public interface ValidationService {

    /**
     * This method validates the given vote using all the rules that exist for a
     * vote.
     * 
     * @param vote
     *            to be validated.
     * @return a ValidationResult containing the result and the list of the
     *         rules that failed if any.
     * @throws ApplicationException
     *             if the execution of the rules fails.
     */
    ValidationResult validate(Vote vote) throws ApplicationException;

    /**
     * Validates choice code computation proof
     * 
     * @param tenantId
     * @param electionEventId
     * @param verificationCardId
     * @param choiceCodesComputationResult
     * @return validation result
     * @throws GeneralCryptoLibException
     * @throws ResourceNotFoundException
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    ValidationResult validate(String tenantId, String electionEventId, String verificationCardId,
            ChoiceCodesVerificationResPayload choiceCodesComputationResult)
            throws GeneralCryptoLibException, ResourceNotFoundException, JsonParseException, JsonMappingException,
            IOException;

}
