/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service;

import java.io.IOException;

import com.scytl.products.ov.commons.beans.domain.model.confirmation.TraceableConfirmationMessage;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;
import com.scytl.products.ov.commons.dto.CastCodeAndComputeResults;

/**
 * Interface for cast code generation.
 */
public interface CastCodeService {

    /**
     * Calculates the Long codes regarding the encrypted choice codes received.
     *
     * @param tenantId
     *            - tenant identifier.
     * @param eeid
     *            - election event identifier.
     * @param verificationCardId
     *            - verification card id
     * @param confirmationMessage
     *            - the confirmation message
     * @return The generated cast code message and all computation outputs
     * @throws ResourceNotFoundException
     * @throws CryptographicOperationException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    CastCodeAndComputeResults retrieveCastCodeAndSignature(String tenantId, String eeid, String verificationCardId,
            TraceableConfirmationMessage confirmationMessage)
            throws ResourceNotFoundException, CryptographicOperationException, ClassNotFoundException, IOException;

}
