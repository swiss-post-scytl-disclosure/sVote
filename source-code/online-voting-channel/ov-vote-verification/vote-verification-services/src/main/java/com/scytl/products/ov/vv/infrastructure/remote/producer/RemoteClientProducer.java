/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.remote.producer;

import com.scytl.products.ov.commons.infrastructure.exception.OvCommonsInfrastructureException;
import com.scytl.products.ov.commons.infrastructure.exception.RestClientException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RestClientConnectionManager;

import retrofit2.Retrofit;

/**
 * "Producer" class that centralizes instantiation of all (retrofit) remote
 * service client interfaces
 */
public class RemoteClientProducer {

    public static <T> T createRestClient(String url, Class<T> clazz) {
        Retrofit client;
        try {
            client = RestClientConnectionManager.getInstance().getRestClient(url);
        } catch (OvCommonsInfrastructureException e) {
            throw new RestClientException("Error trying to obtain a REST client.", e);
        }
        return client.create(clazz);
    }
}
