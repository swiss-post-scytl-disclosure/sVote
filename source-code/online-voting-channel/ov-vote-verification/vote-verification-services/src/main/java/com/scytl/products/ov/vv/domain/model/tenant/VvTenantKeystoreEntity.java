/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.tenant;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scytl.products.ov.commons.domain.model.tenant.TenantKeystoreEntity;

/**
 * @see TenantKeystoreEntity
 */
@Entity
@Table(name = "VV_TENANT_KEYSTORES")
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
public class VvTenantKeystoreEntity extends TenantKeystoreEntity {


    /**
     * The identifier for this entity.
     */
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "vvTenantKeystoreSeq")
    @SequenceGenerator(name = "vvTenantKeystoreSeq", sequenceName = "VV_TENANT_KEYSTORES_SEQ")
    @JsonIgnore
    private Long id;


    /**
     * Sets the identifier for this entity..
     *
     * @param id New value of The identifier for this entity..
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets The identifier for this entity..
     *
     * @return Value of The identifier for this entity..
     */
    public Long getId() {
        return id;
    }
}
