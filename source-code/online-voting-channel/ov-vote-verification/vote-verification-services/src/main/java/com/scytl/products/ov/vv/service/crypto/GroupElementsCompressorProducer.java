/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.inject.Produces;

import com.scytl.cryptolib.mathematical.groups.activity.GroupElementsCompressor;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;

/**
 * Symmetric Service Producer.
 */
public class GroupElementsCompressorProducer {

    /**
     * Returns a group element compressor instance.
     * 
     * @return a symmetric service.
     */
    @Produces
    public GroupElementsCompressor<ZpGroupElement> getInstance() {
        return new GroupElementsCompressor<>();
    }
}
