/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.content;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

import javax.ejb.Local;

/**
 * Repository for handling VerificationContent entities
 */
@Local
public interface VerificationContentRepository extends BaseRepository<VerificationContentEntity,Integer> {

    /**
     * Searches for a verification content with the given tenant, election event and verification card set ids.
     *
     * @param tenantId - the identifier of the tenant.
     * @param electionEventId - the identifier of the electionEvent.
     * @param verificationCardSetId - the identifier of the verificationCardSet.
     * @return a entity representing the verification content.
     */
    VerificationContentEntity findByTenantIdElectionEventIdVerificationCardSetId(String tenantId,String electionEventId, String verificationCardSetId) throws ResourceNotFoundException;

}
