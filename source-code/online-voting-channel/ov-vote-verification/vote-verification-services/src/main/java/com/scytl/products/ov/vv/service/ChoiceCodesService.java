/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service;

import java.io.IOException;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;
import com.scytl.products.ov.commons.dto.ChoiceCodeAndComputeResults;
import com.scytl.products.ov.commons.dto.VoteAndComputeResults;

/**
 * Interface for choice code generation.
 */
public interface ChoiceCodesService {

    /**
     * Generates the choice codes.
     * 
     * @param tenantId
     *            the tenant id.
     * @param electionEventId
     *            the election event id.
     * @param verificationCardId
     *            the verification card id.
     * @param voteAndComputeResults
     *            the vote with previously compute results if any
     * @return the corresponding choice codes.
     * @throws ResourceNotFoundException
     * @throws CryptographicOperationException
     */
    ChoiceCodeAndComputeResults generateChoiceCodes(String tenantId, String electionEventId, String verificationCardId,
            VoteAndComputeResults voteAndComputeResults)
            throws ResourceNotFoundException, GeneralCryptoLibException, IOException, CryptographicOperationException;

}
