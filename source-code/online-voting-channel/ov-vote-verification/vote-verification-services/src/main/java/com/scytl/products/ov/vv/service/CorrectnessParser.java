/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.scytl.products.ov.commons.ui.ws.rs.ObjectMappers;

/**
 * Utility class that parses a string (that may contain an array of arrays of
 * strings, represented as a string) into its object representation.
 */
public class CorrectnessParser {

    /**
     * The result of parsing and formating the received string.
     * <P>
     * This method expects strings of the following format:
     * "[["6c"],[],["71","96","b9"],["71","96","b9"],["71","96","b9"],[],["f7","77","1c"]]"
     * <P>
     * Note: the string above contains double quotes inside of the string, these
     * are escaped however for the sake of readability, the escape characters
     * are not shown here.
     * <P>
     * The above string would result in the following List:
     * <ol>
     * <li>["6c"]</li>
     * <li>[]</li>
     * <li>["71","96","b9"</li>
     * <li>["71","96","b9"]</li>
     * <li>["71","96","b9"]</li>
     * <li>[]</li>
     * <li>["f7","77","1c"]</li>
     * </ol>
     *
     * @param correctnessIds
     *            the array (possibly of arrays) of strings, represented as a
     *            single string
     * @return a list of strings.
     */
    public List<String[]> parse(final String correctnessIds) {

        try {
            return Arrays.asList(ObjectMappers.fromJson(correctnessIds, String[][].class));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
