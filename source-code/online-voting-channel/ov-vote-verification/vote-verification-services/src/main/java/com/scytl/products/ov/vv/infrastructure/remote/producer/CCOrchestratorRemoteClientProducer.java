/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.remote.producer;

import com.scytl.products.ov.vv.infrastructure.remote.OrchestratorClient;

import javax.enterprise.inject.Produces;

public class CCOrchestratorRemoteClientProducer extends RemoteClientProducer {

    public static final String URI_ORCHESTRATOR = System.getProperty("ORCHESTRATOR_CONTEXT_URL");

    @Produces
    OrchestratorClient ccOrchestratorClient() {
        return createRestClient(URI_ORCHESTRATOR, OrchestratorClient.class);
    }
}
