/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;
import com.scytl.products.ov.commons.crypto.KeystoreForObjectOpener;
import com.scytl.products.ov.commons.crypto.KeystoreForObjectRepository;
import com.scytl.products.ov.commons.crypto.PasswordForObjectRepository;

public class KeystoreOpenerProducer {

    @Inject
    @Produced
    private ScytlKeyStoreServiceAPI storesService;

    @Inject
    @Secret
    private KeystoreForObjectRepository secretKeystoreRepository;

    @Inject
    @Secret
    private PasswordForObjectRepository secretPasswordRepository;

    @Produces
    @Secret
    public KeystoreForObjectOpener getSecretInstance() {

        return new KeystoreForObjectOpener(storesService, secretKeystoreRepository, secretPasswordRepository);
    }
}
