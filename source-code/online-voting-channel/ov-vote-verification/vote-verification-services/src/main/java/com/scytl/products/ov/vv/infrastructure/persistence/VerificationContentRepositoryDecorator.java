/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.content.VerificationContentEntity;
import com.scytl.products.ov.vv.domain.model.content.VerificationContentRepository;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogConstants;
import com.scytl.products.ov.vv.infrastructure.log.VoteVerificationLogEvents;

/**
 * Implementation of the repository with JPA
 */
@Decorator
public abstract class VerificationContentRepositoryDecorator implements VerificationContentRepository {

    @Inject
    @Delegate
    private VerificationContentRepository verificationContentRepository;

    @Inject
    private SecureLoggingWriter secureLoggingWriter;

    @Inject
    private TrackIdInstance trackId;

    @Override
    public VerificationContentEntity find(final Integer verificationContentEntityId) {
        return verificationContentRepository.find(verificationContentEntityId);
    }

    @Override
    public VerificationContentEntity save(final VerificationContentEntity entity) throws DuplicateEntryException {
        try {
            final VerificationContentEntity verificationSet = verificationContentRepository.save(entity);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CONTENT_DATA_SAVED)
                    .objectId(entity.getVerificationCardSetId()).user("admin")
                    .electionEvent(entity.getElectionEventId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .createLogInfo());
            return verificationSet;
        } catch (DuplicateEntryException ex) {
            secureLoggingWriter
                .log(Level.ERROR,
                    new LogContent.LogContentBuilder()
                        .logEvent(VoteVerificationLogEvents.ERROR_SAVING_VERIFICATION_CONTENT_SET_DATA)
                        .objectId(entity.getVerificationCardSetId()).user("admin")
                        .electionEvent(entity.getElectionEventId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                        .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC, ex.getMessage())
                        .createLogInfo());
            throw ex;
        }
    }

    @Override
    public VerificationContentEntity findByTenantIdElectionEventIdVerificationCardSetId(final String tenantId,
                                                                                    final String electionEventId,
                                                                                    final String verificationCardSetId)
        throws ResourceNotFoundException {
        try {
            final VerificationContentEntity verificationSetEntity = verificationContentRepository
                .findByTenantIdElectionEventIdVerificationCardSetId(tenantId, electionEventId, verificationCardSetId);
            secureLoggingWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CONTENT_DATA_FOUND)
                    .objectId(verificationCardSetId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_SET_ID, verificationCardSetId)
                    .createLogInfo());
            return verificationSetEntity;
        } catch (ResourceNotFoundException e) {
            secureLoggingWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(VoteVerificationLogEvents.VERIFICATION_CONTENT_DATA_NOT_FOUND)
                    .objectId(verificationCardSetId).user("").electionEvent(electionEventId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
                    .additionalInfo(VoteVerificationLogConstants.INFO_VERIFICATION_CARD_SET_ID, verificationCardSetId)
                    .additionalInfo(VoteVerificationLogConstants.INFO_ERR_DESC,
                        "Resource not found in the VERIFICATION_CONTENT table.")
                    .createLogInfo());
            throw e;
        }
    }
}
