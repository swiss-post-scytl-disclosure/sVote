/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.verificationset;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Provides operations on the verification set repository.
 */
@Local
public interface VerificationSetRepository extends BaseRepository<VerificationSetEntity, Integer> {

	/**
	 * Returns a verification data for a given tenant, election event and voting card.
	 *
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @param verificationCardSetId - the voting card set identifier.
	 * @return The verification data.
	 * @throws com.scytl.products.ov.commons.application.ResourceNotFoundException if the verification data is not found.
	 */
	VerificationSetEntity findByTenantIdElectionEventIdVerificationCardSetId(String tenantId, String electionEventId,
			String verificationCardSetId) throws ResourceNotFoundException;
}
