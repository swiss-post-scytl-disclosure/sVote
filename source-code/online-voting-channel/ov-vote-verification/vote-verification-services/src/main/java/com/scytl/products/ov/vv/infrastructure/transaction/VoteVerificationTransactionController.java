/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.transaction;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Local;
import javax.ejb.Singleton;

import com.scytl.products.ov.commons.infrastructure.transaction.AbstractTransactionController;
import com.scytl.products.ov.commons.infrastructure.transaction.TransactionController;

/**
 * Implementation of {@link TransactionController} as a singleton stateless
 * session bean.
 */
@Singleton
@Local(TransactionController.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class VoteVerificationTransactionController
        extends AbstractTransactionController {
}
