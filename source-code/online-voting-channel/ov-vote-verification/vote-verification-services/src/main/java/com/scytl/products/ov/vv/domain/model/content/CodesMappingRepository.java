/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.content;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for handling CodesMapping entities
 */
@Local
public interface CodesMappingRepository extends BaseRepository<CodesMappingEntity, Integer>{

    /**
     * Searches for a codes mapping with the given tenant, election event and verification card ids.
     *
     * @param tenantId - the identifier of the tenant.
     * @param electionEventId - the identifier of the electionEvent.
     * @param verificationCardId - the identifier of the verificationCard.
     * @return a entity representing the codes mapping.
     */
    CodesMappingEntity findByTenantIdElectionEventIdVerificationCardId(String tenantId,String electionEventId, String verificationCardId) throws ResourceNotFoundException;

    /**
     * Returns whether exists a codes mapping with the specified tenant identifier, election event identifier and verification card identifier.
     *
     * @param tenantId - the identifier of the tenant
     * @param electionEventId - the identifier of the electionEvent
     * @param verificationCardId - the identifier of the verificationCard
     * @return the mapping exists
     */
    boolean hasWithTenantIdElectionEventIdVerificationCardId(String tenantId,String electionEventId, String verificationCardId);
}
