/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.service.ElGamalServiceFactoryHelper;

/**
 * Elgamal Service Producer.
 */
public class ElgamalServiceProducer {

    private static final String MAX_ELEMENTS_CRYPTO_POOL = "elgamal.max.elements.crypto.pool";

    /**
     * Returns a elgamal service instance.
     * 
     * @return a elgamal service.
     */
    @Produces
    @ApplicationScoped
    public ElGamalServiceAPI getInstance() {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(Integer.parseInt(System.getProperty(MAX_ELEMENTS_CRYPTO_POOL)));
        try {
            return ElGamalServiceFactoryHelper.getFactoryOfThreadSafeServices(config).create();
        } catch (GeneralCryptoLibException e) {
            throw new IllegalStateException("Exception while trying to create ElGamalService", e);
        }
    }
}
