/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.service.crypto;

import javax.enterprise.inject.Produces;

import com.scytl.products.ov.commons.verify.PayloadSigningCertificateValidator;
import com.scytl.products.ov.commons.verify.CryptolibPayloadSigningCertificateValidator;

public class CertificateChainValidatorProducer {
    
    @Produces
    public PayloadSigningCertificateValidator certificateChainValidator() {
        return new CryptolibPayloadSigningCertificateValidator();
    }

}


