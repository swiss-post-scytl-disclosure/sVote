/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.platform.PlatformCAEntity;
import com.scytl.products.ov.commons.domain.model.platform.PlatformCARepository;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImpl;
import com.scytl.products.ov.vv.domain.model.platform.VvPlatformCARepository;
import com.scytl.products.ov.vv.domain.model.platform.VvPlatformCaEntity;

/**
 * Implementation of the repository with JPA
 */
@Stateless
@VvPlatformCARepository
public class VvPlatformCARepositoryImpl extends BaseRepositoryImpl<PlatformCAEntity, Long>
        implements PlatformCARepository {

    /**
     * @see PlatformCARepository#getRootCACertificate()
     */
	@Override
    public PlatformCAEntity getRootCACertificate() throws ResourceNotFoundException {
        TypedQuery<VvPlatformCaEntity> query =
            entityManager.createQuery("SELECT a FROM VvPlatformCaEntity a", VvPlatformCaEntity.class);
        try {
            return query.getResultList().get(0);
        } catch (NoResultException e) {

            throw new ResourceNotFoundException("", e);
        }
    }
}
