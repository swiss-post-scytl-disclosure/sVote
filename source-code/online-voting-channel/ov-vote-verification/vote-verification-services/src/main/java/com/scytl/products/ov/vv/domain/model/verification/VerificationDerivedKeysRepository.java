/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.verification;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

@Local
public interface VerificationDerivedKeysRepository extends BaseRepository<VerificationDerivedKeys, Integer> {

    /**
     * Find verification derived key by tenant, eeId, and verificationCardId
     * @param tenantId
     * @param electionEventId
     * @param verificationCardId
     * @return verification derived key
     * @throws ResourceNotFoundException 
     */
    VerificationDerivedKeys findByTenantIdElectionEventIdVerificationCardId(String tenantId, String electionEventId,
            String verificationCardId) throws ResourceNotFoundException;

}


