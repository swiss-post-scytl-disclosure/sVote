/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.choicecode;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.products.ov.commons.dto.ChoiceCodesVerificationResPayload;

public class CodesComputeResults {

    private Map<BigInteger, ZpGroupElement> combinedPartialChoiceCodes;

    private List<ChoiceCodesVerificationResPayload> computationResults;

    public CodesComputeResults(Map<BigInteger, ZpGroupElement> combinedPartialChoiceCodes,
            List<ChoiceCodesVerificationResPayload> computeResults) {
        this.combinedPartialChoiceCodes = combinedPartialChoiceCodes;
        this.setComputationResults(computeResults);
    }

    public Map<BigInteger, ZpGroupElement> getCombinedPartialChoiceCodes() {
        return combinedPartialChoiceCodes;
    }

    public void setCombinedPartialChoiceCodes(Map<BigInteger, ZpGroupElement> combinedPartialChoiceCodes) {
        this.combinedPartialChoiceCodes = combinedPartialChoiceCodes;
    }

    public List<ChoiceCodesVerificationResPayload> getComputationResults() {
        return computationResults;
    }

    public void setComputationResults(List<ChoiceCodesVerificationResPayload> computationResults) {
        this.computationResults = computationResults;
    }

}
