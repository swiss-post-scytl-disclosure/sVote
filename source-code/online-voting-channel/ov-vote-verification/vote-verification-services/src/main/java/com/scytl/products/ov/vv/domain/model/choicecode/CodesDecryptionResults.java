/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.domain.model.choicecode;

import java.util.List;

import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.products.ov.commons.dto.ChoiceCodesVerificationDecryptResPayload;

public class CodesDecryptionResults {

    private List<ZpGroupElement> combinedZpGroupElementLists;

    private List<ChoiceCodesVerificationDecryptResPayload> decryptResult;

    public CodesDecryptionResults(List<ZpGroupElement> combineZpGroupElementLists,
            List<ChoiceCodesVerificationDecryptResPayload> decryptResult) {
        this.setCombinedZpGroupElementLists(combineZpGroupElementLists);
        this.setDecryptResult(decryptResult);
    }

    public List<ZpGroupElement> getCombinedZpGroupElementLists() {
        return combinedZpGroupElementLists;
    }

    public void setCombinedZpGroupElementLists(List<ZpGroupElement> combineZpGroupElementLists) {
        this.combinedZpGroupElementLists = combineZpGroupElementLists;
    }

    public List<ChoiceCodesVerificationDecryptResPayload> getDecryptResult() {
        return decryptResult;
    }

    public void setDecryptResult(List<ChoiceCodesVerificationDecryptResPayload> decryptResult) {
        this.decryptResult = decryptResult;
    }

}
