/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImplTest;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.verification.Verification;
import com.scytl.products.ov.vv.domain.model.verification.VerificationRepository;

/**
 * Junits for the class {@link VerificationRepository}
 */
@RunWith(MockitoJUnitRunner.class)
public class VerificationRepositoryImplTest extends BaseRepositoryImplTest<Verification, Integer> {

	@Mock
	private TypedQuery<Verification> queryMock;

	@Mock
	private TrackIdInstance trackId;

	@Mock
	private SecureLoggingWriter secureLoggerWriter;

	/**
	 * Creates a new object of the testing class.
	 */
	public VerificationRepositoryImplTest() throws InstantiationException, IllegalAccessException {
		super(Verification.class, verificationRepository.getClass());
	}

	@InjectMocks
	private static VerificationRepository verificationRepository = new VerificationRepositoryImpl();

	@Test
	public void testFindByTenantIdElectionEventIdVerificationCardId() throws ResourceNotFoundException {
		String tenantId = "1";
		String electionEventId = "1";
		String verificationCardId = "1";
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(Verification.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenReturn(new Verification());

		assertNotNull(verificationRepository.findByTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId,
			verificationCardId));
	}

	@Test
	public void testFindByTenantIdElectionEventIdVerificationCardIdNotFound() throws ResourceNotFoundException {
		String tenantId = "2";
		String electionEventId = "2";
		String verificationCardId = "2";
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(Verification.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenThrow(new NoResultException());

		expectedException.expect(ResourceNotFoundException.class);

		assertNotNull(verificationRepository.findByTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId,
			verificationCardId));
	}
}
