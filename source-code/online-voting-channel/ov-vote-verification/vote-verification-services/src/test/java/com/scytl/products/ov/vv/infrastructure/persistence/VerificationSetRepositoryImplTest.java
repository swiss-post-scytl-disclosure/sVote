/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vv.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImplTest;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vv.domain.model.verificationset.VerificationSetEntity;
import com.scytl.products.ov.vv.domain.model.verificationset.VerificationSetRepository;

/**
 * Junits for the class {@link com.scytl.products.ov.vv.domain.model.verificationset.VerificationSetRepository}
 */
@RunWith(MockitoJUnitRunner.class)
public class VerificationSetRepositoryImplTest extends BaseRepositoryImplTest<VerificationSetEntity, Integer> {
	@Mock
	private TypedQuery<VerificationSetEntity> queryMock;
	
	@Mock
	private TrackIdInstance trackId;

	@Mock
	private SecureLoggingWriter secureLoggerWriter;

	/**
	 * Creates a new object of the testing class.
	 */
	public VerificationSetRepositoryImplTest() throws InstantiationException, IllegalAccessException {
		super(VerificationSetEntity.class, verificationSetRepository.getClass());
	}

	@InjectMocks
	private static VerificationSetRepository verificationSetRepository = new VerificationSetRepositoryImpl();

	@Test
	public void testFindByTenantIdElectionEventIdVerificationCardId() throws ResourceNotFoundException {
		String tenantId = "1";
		String electionEventId = "1";
		String verificationCardIdSet = "1";
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(VerificationSetEntity.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenReturn(new VerificationSetEntity());

		assertNotNull(verificationSetRepository.findByTenantIdElectionEventIdVerificationCardSetId(tenantId,
			electionEventId, verificationCardIdSet));
	}

	@Test
	public void testFindByTenantIdElectionEventIdVerificationCardIdNotFound() throws ResourceNotFoundException {
		String tenantId = "2";
		String electionEventId = "2";
		String verificationCardIdSet = "2";
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(VerificationSetEntity.class))).thenReturn(queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenThrow(new NoResultException());

		expectedException.expect(ResourceNotFoundException.class);

		assertNotNull(verificationSetRepository.findByTenantIdElectionEventIdVerificationCardSetId(tenantId,
			electionEventId, verificationCardIdSet));
	}
}
