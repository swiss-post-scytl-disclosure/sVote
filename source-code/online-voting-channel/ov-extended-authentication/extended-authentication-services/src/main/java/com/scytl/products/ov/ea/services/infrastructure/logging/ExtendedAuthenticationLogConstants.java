/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.logging;

/**
 * Constants for logging for this context.
 */
public class ExtendedAuthenticationLogConstants {

	/**
     * Track id - additional information.
     */
    public static final String INFO_TRACK_ID = "#track_id";

    /**
     * Error description - additional information.
     */
    public static final String ERR_DESC = "#err_desc";

    /**
     * The Admin Board ID associated with the event.
     */
    public static final String INFO_ADMIN_BOARD_ID = "#adminboard_id";

    /**
     * Additional information: Credential Id.
     */
    public static final String INFO_CREDENTIAL_ID = "#credential_id";
    
    /**
     * Non-public constructor
     */
 	private ExtendedAuthenticationLogConstants() {    	
    }
}
