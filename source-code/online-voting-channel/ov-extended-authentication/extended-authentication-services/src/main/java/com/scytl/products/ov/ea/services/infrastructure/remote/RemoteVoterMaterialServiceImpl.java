/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Optional;

import com.scytl.products.ov.commons.beans.domain.model.Information.VoterInformation;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import com.scytl.products.ov.ea.services.infrastructure.remote.client.VoterMaterialServiceClient;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

/**
 * Remote Service for handling voter material
 */

@Stateless
public class RemoteVoterMaterialServiceImpl implements VoterMaterialService {

    private static final String PATH_VOTER_INFORMATION = "informations";

    private VoterMaterialServiceClient voterMaterialServiceClient;

    private static final Logger LOG = LoggerFactory.getLogger("std");

    @Inject
    protected TrackIdInstance trackIdInstance;

    @Inject
    RemoteVoterMaterialServiceImpl(final VoterMaterialServiceClient voterMaterialServiceClient) {
        this.voterMaterialServiceClient = voterMaterialServiceClient;
    }

    @Override
    public Optional<VoterInformation> getVoterInformationByCredentialId(final String tenantId,
                        final String electionEventId, final String credentialId) {
        try {
            return Optional.of(RetrofitConsumer.processResponse(
            		voterMaterialServiceClient.getVoterInformationByCredentialId(PATH_VOTER_INFORMATION,
                trackIdInstance.getTrackId(), tenantId, electionEventId, credentialId)));
        } catch (RetrofitException e) {
            LOG.error(String.format(
                "Failed to get VoterInformation associated with credential [tenantId=%s, electionEventId=%s, credentialId=%s]",
                tenantId, electionEventId, credentialId), e);
            return Optional.empty();
        }
    }
}
