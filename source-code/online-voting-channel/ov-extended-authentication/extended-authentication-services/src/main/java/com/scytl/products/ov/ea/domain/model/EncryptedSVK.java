/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.model;

/**
 * Class for storing encrypted start voting key.
 */
public class EncryptedSVK {

	private String encryptedSVK;

	/**
	 * Constructor.
	 * 
	 * @param encryptedSVK the encrypted start voting key.
	 */
	public EncryptedSVK(String encryptedSVK) {
		super();
		this.encryptedSVK = encryptedSVK;
	}

	/**
	 * Returns the current value of the field encryptedSVK.
	 *
	 * @return Returns the encryptedSVK.
	 */
	public String getEncryptedSVK() {
		return encryptedSVK;
	}
}
