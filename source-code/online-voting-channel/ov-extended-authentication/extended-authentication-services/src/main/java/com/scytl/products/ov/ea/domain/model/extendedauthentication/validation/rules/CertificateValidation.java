/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 4/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.ea.domain.model.extendedauthentication.validation.rules;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.ExtendedAuthValidationException;

/**
 * Perform a specific validation with the received certificate during the process of updating the Extended
 * Authentication
 */
public interface CertificateValidation {

    /**
     * Validates a certificate using the information provided in the Authentication token
     * @param certificate
     * @param token
     * @return
     *
     * @throws ExtendedAuthValidationException when the validation over the certificate is not correct
     */
    void validateCertificate(String certificate, AuthenticationToken token) throws ExtendedAuthValidationException;
}
