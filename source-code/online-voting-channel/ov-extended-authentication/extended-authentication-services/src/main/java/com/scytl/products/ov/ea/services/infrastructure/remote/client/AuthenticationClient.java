/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.remote.client;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

import javax.validation.constraints.NotNull;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationData;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.commons.validation.ValidationResult;

/**
 * The Interface of authentication client for admin.
 */
public interface AuthenticationClient {

    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    /** The Constant PARAMETER_TENANT_DATA_PATH. */
    String PARAMETER_PATH_AUTHENTICATION_DATA = "pathAuthenticationData";

    /** The Constant X_FORWARDED_FOR. */
    String PARAMETER_X_FORWARDED_FOR = "X-Forwarded-For";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    String PARAMETER_VALUE_VOTING_CARD_ID = "votingCardId";

    /** The Constant PARAMETER_PATH_VALIDATION_TOKENS. */
    String PARAMETER_PATH_VALIDATION_TOKENS = "pathValidationTokens";

    /** The Constant PARAMETER_AUTHENTICATION_TOKEN. */
    String PARAMETER_AUTHENTICATION_TOKEN = "authenticationToken";


    @GET("{pathAuthenticationData}/tenant/{tenantId}/electionevent/{electionEventId}")
    Call<AuthenticationData> getAuthenticationData(@Path(PARAMETER_PATH_AUTHENTICATION_DATA) String pathBallotboxstatus,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId);

    /**
     * Validate authentication token.
     *
     * @param pathValidationTokens
     *            the path validation tokens
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param votingCardId
     *            the voting card id
     * @param trackId
     *            the track id
     * @param authenticationToken
     *            the authentication token
     * @return the validation result
     */
    @POST("{pathValidationTokens}/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}")
    Call<ValidationResult> validateAuthenticationToken(@Path(PARAMETER_PATH_VALIDATION_TOKENS) String pathValidationTokens,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VOTING_CARD_ID) String votingCardId,
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Header(PARAMETER_AUTHENTICATION_TOKEN) String authenticationToken);

    @POST("tokens/tenant/{tenantId}/electionevent/{electionEventId}/chain/validate")
    Call<ValidationResult> validateCertificateChain(@Path(PARAMETER_VALUE_TENANT_ID) final String tenantId,
                                              @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) final String electionEventId,
                                              @NotNull @Body final CertificateChainValidationRequest certificateChainValidationRequest);
}
