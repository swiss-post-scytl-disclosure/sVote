/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.model.extendedauthentication;

import com.scytl.products.ov.commons.verify.CSVVerifier;

import javax.enterprise.inject.Produces;

public class CSVVerifierProducer{

    @Produces
    @EaCsvVerifier
    public CSVVerifier getCSVVerifier(){
        return new CSVVerifier();
    }    
    
}
