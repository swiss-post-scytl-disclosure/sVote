/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 7/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.ea.domain.model.extendedauthentication.validation;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.AuthTokenRepositoryException;
import com.scytl.products.ov.commons.validation.ValidationResult;

import javax.ejb.Local;

/**
 * Service for validating the authentication token
 */
@Local
public interface AuthenticationTokenService {


    /**
     * Validates an auntenticationToken
     * @param tenantId
     * @param electionEventId
     * @param token
     * @return
     * @throws AuthTokenRepositoryException 
     */
    ValidationResult validateToken(String tenantId, String electionEventId, AuthenticationToken token) throws AuthTokenRepositoryException;
}
