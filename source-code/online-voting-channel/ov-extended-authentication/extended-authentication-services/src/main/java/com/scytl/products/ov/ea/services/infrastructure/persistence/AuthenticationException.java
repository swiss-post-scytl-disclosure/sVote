/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.persistence;

import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;

public class AuthenticationException extends Exception {

	private static final long serialVersionUID = 1L;
	private final int remainingAttempts;

	public AuthenticationException(EntryPersistenceException e, int remainingAttempts) {
		super(e);
		this.remainingAttempts = remainingAttempts;
	}

	public AuthenticationException(String string, int remainingAttempts) {
		super(string);
		this.remainingAttempts = remainingAttempts;
	}

	public int getRemainingAttempts() {
		return remainingAttempts;
	}

}
