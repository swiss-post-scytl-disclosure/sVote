/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 3/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.products.ov.ea.domain.actions;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.domain.model.authentication.ExtendedAuthenticationUpdate;
import com.scytl.products.ov.commons.beans.domain.model.authentication.ExtendedAuthenticationUpdateRequest;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.ea.domain.model.extendedauthentication.validation.ExtendedAuthValidationService;

/**
 * This class defines a DDD action to validate an Extended Authentication entity.
 */
@Stateless
public class ValidateExtendedAuthUpdateAction {

    @Inject
    private ExtendedAuthValidationService extendedAuthValidationService;

    /**
     * Validates the information received prior to trying the update of an extended authentication entry. The
     * validations affect the authentication token, the verification of signature of the request, and also the
     * credential ID provided both in the token and certificate used for verification. It also verifies the certificate
     * chain.
     *
     * @param tenantId
     *            Tenant ID.
     * @param electionEventId
     *            Election Event ID.
     * @param authenticationToken
     *            Authentication Token.
     * @param extendedAuthenticationUpdateRequest
     *            Extended Authentication request for update.
     * @return The ExtendedAuthenticationUpdate object.
     * @throws ResourceNotFoundException
     * @throws ApplicationException
     */
    public ExtendedAuthenticationUpdate validate(String tenantId, String electionEventId,
            AuthenticationToken authenticationToken,
            ExtendedAuthenticationUpdateRequest extendedAuthenticationUpdateRequest)
            throws ResourceNotFoundException, ApplicationException {

        ExtendedAuthenticationUpdate extendedAuthenticationUpdate
                = extendedAuthValidationService.verifySignature(extendedAuthenticationUpdateRequest, authenticationToken);

        extendedAuthValidationService.validateToken(tenantId, electionEventId, authenticationToken);

        extendedAuthValidationService.validateTokenWithAuthIdAndCredentialId(authenticationToken,
            extendedAuthenticationUpdate, tenantId, electionEventId);

        extendedAuthValidationService.validateCertificate(extendedAuthenticationUpdateRequest.getCertificate(),
            authenticationToken);

        extendedAuthValidationService.validateCertificateChain(tenantId, electionEventId,
            extendedAuthenticationUpdateRequest.getCertificate(), authenticationToken);

        return extendedAuthenticationUpdate;
    }
}
