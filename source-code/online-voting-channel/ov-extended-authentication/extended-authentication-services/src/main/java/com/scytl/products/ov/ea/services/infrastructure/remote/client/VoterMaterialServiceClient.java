/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.remote.client;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

import javax.validation.constraints.NotNull;

import com.scytl.products.ov.commons.beans.domain.model.Information.VoterInformation;

public interface VoterMaterialServiceClient {

    /** The Constant PARAMETER_X_REQUEST_ID */
    String PARAMETER_X_REQUEST_ID = "X-Request-ID";

    /** The Constant TENANT_ID */
    String PARAMETER_TENANT_ID = "tenantId";

    /** The Constant ELECTION_EVENT_ID */
    String PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    /** The Constant PARAMETER_CREDENTIAL_ID. */
    String PARAMETER_CREDENTIAL_ID = "credentialId";

    /**
     * The constant PARAMETER_PATH_VOTER_INFORMATION_DATA
     */
    String PARAMETER_PATH_VOTER_INFORMATION_DATA = "pathVoterInformationData";

    @GET("{pathVoterInformationData}/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}")
    Call<VoterInformation> getVoterInformationByCredentialId(
        @Path(value = PARAMETER_PATH_VOTER_INFORMATION_DATA, encoded = true) String pathVoterInformationData,
        @NotNull @Header(PARAMETER_X_REQUEST_ID) String trackingId,
        @Path(PARAMETER_TENANT_ID) String tenantId,
        @Path(PARAMETER_ELECTION_EVENT_ID) String electionEventId,
        @Path(PARAMETER_CREDENTIAL_ID) String credentialId);
}
