/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.remote;

import java.util.Optional;

import com.scytl.products.ov.commons.beans.domain.model.Information.VoterInformation;

/**
 * Service for accessing the voterMaterialService material remote service
 *
 */
public interface VoterMaterialService {

    /**
     * Gets the voter information associated with the specified credential
     *
     * @param tenantId the tenant identifier
     * @param electionEventId the election event identifier
     * @param credentialId the credential identifier
     * @return the Optional with VoterInformation or Optional.empty in case voter information was not found
     */
    Optional<VoterInformation> getVoterInformationByCredentialId(final String tenantId,
                                                                 final String electionEventId, final String credentialId);

}
