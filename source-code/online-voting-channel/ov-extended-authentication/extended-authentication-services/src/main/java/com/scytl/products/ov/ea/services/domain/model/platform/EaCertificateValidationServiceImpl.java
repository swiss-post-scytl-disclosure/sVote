/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.domain.model.platform;

import com.scytl.products.ov.commons.beans.validation.CertificateValidationServiceImpl;

import javax.ejb.Stateless;

@Stateless
@EaCertificateValidationService
public class EaCertificateValidationServiceImpl extends CertificateValidationServiceImpl {

}
