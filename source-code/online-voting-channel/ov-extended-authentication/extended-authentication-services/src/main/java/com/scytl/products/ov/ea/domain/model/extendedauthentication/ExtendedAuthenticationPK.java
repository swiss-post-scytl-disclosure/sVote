/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.model.extendedauthentication;

import java.io.Serializable;

public class ExtendedAuthenticationPK implements Serializable{

	private static final long serialVersionUID = 4915015458000887699L;

	private String authId;

	private String tenantId;

	private String electionEvent;

	public ExtendedAuthenticationPK() {
		super();
	}

	public ExtendedAuthenticationPK(String authId, String tenantId, String electionEvent) {
		super();
		this.authId = authId;
		this.tenantId = tenantId;
		this.electionEvent = electionEvent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((electionEvent == null) ? 0 : electionEvent.hashCode());
		result = prime * result + ((tenantId == null) ? 0 : tenantId.hashCode());
		result = prime * result + ((authId == null) ? 0 : authId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ExtendedAuthenticationPK)) {
			return false;
		}
		ExtendedAuthenticationPK other = (ExtendedAuthenticationPK) obj;
		if (electionEvent == null) {
			if (other.electionEvent != null) {
				return false;
			}
		} else if (!electionEvent.equals(other.electionEvent)) {
			return false;
		}
		if (tenantId == null) {
			if (other.tenantId != null) {
				return false;
			}
		} else if (!tenantId.equals(other.tenantId)) {
			return false;
		}
		if (authId == null) {
			if (other.authId != null) {
				return false;
			}
		} else if (!authId.equals(other.authId)) {
			return false;
		}
		return true;
	}
	
	
}
