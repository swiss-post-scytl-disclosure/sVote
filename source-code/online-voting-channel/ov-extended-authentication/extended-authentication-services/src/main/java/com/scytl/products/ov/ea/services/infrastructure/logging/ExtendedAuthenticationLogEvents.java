/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.logging;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Enum with the log events for EXTENDED AUTHENTICATION. Module identifier = 7xx.
 */
public enum ExtendedAuthenticationLogEvents implements LogEvent {

    EASAVE_SUCCESS("EASAVE", "000", "Extended Authentication saved successfully."), //
    EASAVE_ERROR_SAVING("EASAVE", "701", "Failed to save Extended Authentication."), //
    //
    EAUPDATE_SUCCESS("EAUPDATE", "000", "Extended authentication updated successfully."), //
    EAUPDATE_ERROR_UPDATING("EAUPDATE", "702", "Failed to update authentication data."), //
    //
    EARENEW_SAVE_SUCCESS("EARENEW", "000", "Extended authentication data saved successfully"), //
    EARENEW_ERROR_SAVING_DATA("EARENEW", "703", "Failed to save authentication data"), //
    EARENEW_ERROR_INVALID_VALIDATION_TOKEN("EARENEW", "704", "Renew credentials validation: invalid token."), //
    EARENEW_ERROR_INVALID_VALIDATION_CERTIFICATE("EARENEW", "705",
            "Renew credentials validation: invalid certificate."), //
    EARENEW_ERROR_INVALID_VALIDATION_CREDENTIAL_ID("EARENEW", "706",
            "Renew credentials validation: invalid credential ID."), //
    EARENEW_ERROR_INVALID_VALIDATION_CERTIFICATE_CHAIN("EARENEW", "707",
            "Renew credentials validation: invalid certificate chain."), //
    EARENEW_ERROR_INVALID_VALIDATION_SIGNATURE("EARENEW", "708", "Renew credentials validation: invalid signature."), //
    //
    EAAUTH_SUCCESS("EAAUTH", "000", "Extended authentication request successful"), //
    EAAUTH_ERROR_AUTHID_NOT_FOUND("EAAUTH", "709", "AuthID not found"), //
    EAAUTH_ERROR_INVALID_ATTEMPT("EAAUTH", "710", "Invalid attempt."), //
    EAAUTH_ERROR_MAX_ATTEMPTS_REACHED("EAAUTH", "713", "Max authentication attempts reached"), //
    //
    EASIGVER_SIGNATURE_VERIFICATION_SUCCESS("EASIGVER", "000",
            "Extended authentication signature verified successfully."), //
    EASIGVER_SIGNATURE_VERIFICATION_ERROR("EASIGVER", "711",
            "Error verifying the signature of an Extended authentication."), //
    //
    EAMULSTO_SUCCESS("EAMULSTO", "000", "Multiple Extended Authentication data has been stored successfully."),
    EAMULSTO_ERROR("EAMULSTO", "712", "Failed to save all instances of Multiple Extended Authentication data.");

    private final String layer;

    private final String action;

    private final String outcome;

    private final String info;

    ExtendedAuthenticationLogEvents(final String action, final String outcome, final String info) {
        this.layer = "";
        this.action = action;
        this.outcome = outcome;
        this.info = info;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public String getOutcome() {
        return outcome;
    }

    @Override
    public String getInfo() {
        return info;
    }

    @Override
    public String getLayer() {
        return layer;
    }
}
