/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.csv;

public class FailedAuthenticationVotingCardItem {

    private String credentialId;
    private String votingCardId;

    public FailedAuthenticationVotingCardItem(final String credentialId,
                                              final String votingCardId) {
        this.credentialId = credentialId;
        this.votingCardId = votingCardId;
    }

    String getCredentialId() {
        return credentialId;
    }

    public String getVotingCardId() {
        return votingCardId;
    }
}
