/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.model.extendedauthentication;

public class AllowedAttemptsExceededException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MSG = "Invalid authentication: max allowed number of attempts reached.";
	
	public AllowedAttemptsExceededException() {
		super(DEFAULT_MSG);
	}
	
	public AllowedAttemptsExceededException(final String message) {
		super(message);
	}

	public AllowedAttemptsExceededException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public AllowedAttemptsExceededException(final Throwable cause) {
		super(cause);
	}
}
