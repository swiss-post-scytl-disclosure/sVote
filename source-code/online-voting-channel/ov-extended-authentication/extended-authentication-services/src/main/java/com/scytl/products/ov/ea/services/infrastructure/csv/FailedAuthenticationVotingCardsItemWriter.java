/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.csv;

import java.io.OutputStream;

import com.scytl.products.ov.commons.infrastructure.csv.AbstractCSVWriter;

/**
 * The Class ExportedPartialVotingCardStateItemWriter.
 */
public class FailedAuthenticationVotingCardsItemWriter
        extends AbstractCSVWriter<FailedAuthenticationVotingCardItem> {

    private static final String MAX_ALLOWED_NUMBER_OF_ATTEMPTS = "MAX_ALLOWED_LOGIN_ATTEMPTS_REACHED";

    /**
     * Instantiates a new exported partial voting card state item writer.
     *
     * @param outputStream
     *            the output stream
     */
    public FailedAuthenticationVotingCardsItemWriter(
            OutputStream outputStream) {
        super(outputStream);
    }

    /* (non-Javadoc)
     * @see com.scytl.products.ov.vw.services.domain.common.csv.AbstractCSVWriter#extractValues(java.lang.Object)
     */
    @Override
    protected String[] extractValues(
            final FailedAuthenticationVotingCardItem failedAuthenticationVotingCardItem) {

        return new String[] {
            failedAuthenticationVotingCardItem.getVotingCardId(),
            MAX_ALLOWED_NUMBER_OF_ATTEMPTS };
    }

}
