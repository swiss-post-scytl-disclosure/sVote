/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.actions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.domain.model.authentication.ExtendedAuthenticationUpdateRequest;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.scytl.products.ov.commons.beans.domain.model.authentication.ExtendedAuthenticationUpdate;
import com.scytl.products.ov.ea.domain.model.extendedauthentication.validation.ExtendedAuthValidationService;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import java.security.Security;

@RunWith(MockitoJUnitRunner.class)
public class ValidateExtendedAuthUpdateActionTest {

    @BeforeClass
    public static void setup() {
        MockitoAnnotations.initMocks(ValidateExtendedAuthUpdateActionTest.class);
        Security.addProvider(new BouncyCastleProvider());
    }

    @Mock
    private ExtendedAuthValidationService mockExtendedAuthValidationService;

    @InjectMocks
    private ValidateExtendedAuthUpdateAction validateExtendedAuthUpdateAction = new ValidateExtendedAuthUpdateAction();

    @Test
    public void validateHappyPath() throws ApplicationException, ResourceNotFoundException {

        ExtendedAuthenticationUpdate mockExtendedAuthenticationUpdate = new ExtendedAuthenticationUpdate();

        ExtendedAuthenticationUpdateRequest mockExtendedAuthenticationUpdateRequest =
            new ExtendedAuthenticationUpdateRequest();

        when(mockExtendedAuthValidationService.verifySignature(anyObject(), anyObject()))
            .thenReturn(mockExtendedAuthenticationUpdate);

        when(mockExtendedAuthValidationService.validateToken(anyString(), anyString(),
            anyObject())).thenReturn(true);
        Mockito.doNothing().when(mockExtendedAuthValidationService).validateTokenWithAuthIdAndCredentialId(anyObject(),
            anyObject(), anyString(), anyString());
        when(mockExtendedAuthValidationService.validateCertificate(anyString(), anyObject())).thenReturn(true);
        Mockito.doNothing().when(mockExtendedAuthValidationService).validateCertificateChain(anyString(), anyString(),
            anyString(), anyObject());

        assertThat(validateExtendedAuthUpdateAction.validate("", "", new AuthenticationToken(),
            mockExtendedAuthenticationUpdateRequest), instanceOf(ExtendedAuthenticationUpdate.class));

    }



}
