/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.crypto;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.isA;

public class AsymmetricServiceProducerTest {


    public static final String SOME_NUMBER = "50";

    private static final String MAX_ELEMENTS_CRYPTO_POOL = "asymmetric.max.elements.crypto.pool";



    @Rule
    public TestRule restoreSystemProperties = new RestoreSystemProperties();

    @Test
    public void testCreation(){

        System.setProperty(MAX_ELEMENTS_CRYPTO_POOL, SOME_NUMBER);
        AsymmetricServiceProducer producer = new AsymmetricServiceProducer();
        assertThat(producer.getInstance(), isA(AsymmetricServiceAPI.class));

    }
}
