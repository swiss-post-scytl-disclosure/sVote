/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.persistence;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.ea.services.domain.model.platform.EaLoggingKeystoreEntity;

@RunWith(MockitoJUnitRunner.class)
public class LoggingKeystoreRepositoryTest {

    public static final String SIGN = "SIGN";


    @Mock
    private EntityManager manager;

    @Mock
    private TypedQuery<EaLoggingKeystoreEntity> typedQuery;

    @InjectMocks
    EaLoggingKeystoreRepositoryImpl classUnderTest = new EaLoggingKeystoreRepositoryImpl();


    @Before
    public void init() {

        MockitoAnnotations.initMocks(this.getClass());

    }

    @Test
    public void findLogingKeystore() throws ResourceNotFoundException {

        List<EaLoggingKeystoreEntity> list = Arrays.asList(new EaLoggingKeystoreEntity());
        when(manager.createQuery(anyString(), eq(EaLoggingKeystoreEntity.class))).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(list);
        Assert.assertNotNull(classUnderTest.getLoggingKeystoreByType(SIGN));
    }


    @Test
    public void checkIfExistsOk() throws ResourceNotFoundException {

        List<EaLoggingKeystoreEntity> list = Arrays.asList(new EaLoggingKeystoreEntity());
        when(manager.createQuery(anyString(), eq(EaLoggingKeystoreEntity.class))).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(list);
        Assert.assertTrue(classUnderTest.checkIfKeystoreExists(SIGN));
    }

    @Test
    public void checkIfExistsNotFound() throws ResourceNotFoundException {


        List<EaLoggingKeystoreEntity> list = new ArrayList<>();
        when(manager.createQuery(anyString(), eq(EaLoggingKeystoreEntity.class))).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(list);
        Assert.assertFalse(classUnderTest.checkIfKeystoreExists(SIGN));

    }



}
