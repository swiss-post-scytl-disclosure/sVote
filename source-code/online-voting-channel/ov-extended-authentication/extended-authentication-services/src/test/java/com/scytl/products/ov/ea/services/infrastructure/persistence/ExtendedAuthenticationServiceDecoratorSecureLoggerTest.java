/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.persistence;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.ea.domain.model.EncryptedSVK;
import com.scytl.products.ov.ea.domain.model.extendedauthentication.AllowedAttemptsExceededException;
import com.scytl.products.ov.ea.services.infrastructure.logging.ExtendedAuthenticationLogConstants;
import com.scytl.products.ov.ea.services.infrastructure.logging.ExtendedAuthenticationLogEvents;
import com.scytl.products.ov.ea.services.infrastructure.persistence.AuthenticationException;
import com.scytl.products.ov.ea.services.infrastructure.persistence.ExtendedAuthenticationService;
import com.scytl.products.ov.ea.services.infrastructure.persistence.ExtendedAuthenticationServiceDecorator;

@RunWith(MockitoJUnitRunner.class)
public class ExtendedAuthenticationServiceDecoratorSecureLoggerTest extends ExtendedAuthenticationTest {

    private static final String TENANT_ID = "tenantId";

    private static final String ELECTION_EVENT_ID = "electionEventId";

    @Spy
    private SecureLoggingWriter secureLoggerWriter = getSecureLoggingWriter();

    @Mock
    private TrackIdInstance trackIdInstance;

    @InjectMocks
    private ExtendedAuthenticationServiceDecorator extendedAuthenticationServiceDecorator;

    @Mock
    private ExtendedAuthenticationService extendedAuthenticationService;

    @Mock
    private TransactionInfoProvider transactionInfoProvider;

    @Mock
    private Logger LOG;

    @BeforeClass
    public static void setup() {
        ExtendedAuthenticationTest.setup();
        MockitoAnnotations.initMocks(ExtendedAuthenticationServiceDecoratorSecureLoggerTest.class);
    }

    @Before
    public void before() {
        this.setupSecureLogger();
        when(trackIdInstance.getTrackId()).thenReturn("trackId");
    }

    @Test
    public void testAuthenticateSuccess()
            throws ApplicationException, ResourceNotFoundException, AuthenticationException,
            AllowedAttemptsExceededException, IOException, GeneralCryptoLibException {

        EncryptedSVK encryptedStartVotingKey = new EncryptedSVK("encryptedSVK");

        when(extendedAuthenticationService.authenticate(any(), any(), any(), any()))
            .thenReturn(encryptedStartVotingKey);

        extendedAuthenticationServiceDecorator.authenticate(TENANT_ID, "voCId2", "eP", ELECTION_EVENT_ID);

        assertSecureLogContainsMessage("INFO", ExtendedAuthenticationLogEvents.EAAUTH_SUCCESS.getInfo(),
            ExtendedAuthenticationLogConstants.INFO_TRACK_ID);

    }

}
