/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * $Id$
 *
 * @author riglesias
 * @date 4/11/16
 *
 *
 * All rights reserved.
 */
package com.scytl.product.ov.ea.services.domain.validation;

import com.scytl.cryptolib.certificates.cryptoapi.CryptoAPIX509Certificate;

import java.security.PrivateKey;

public class AuthTokenCryptoInfo {

    private CryptoAPIX509Certificate certificate;

    private PrivateKey privateKey;

    /**
     * Sets new privateKey.
     *
     * @param privateKey
     *            New value of privateKey.
     */
    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    /**
     * Sets new certificate.
     *
     * @param certificate
     *            New value of certificate.
     */
    public void setCertificate(CryptoAPIX509Certificate certificate) {
        this.certificate = certificate;
    }

    /**
     * Gets certificate.
     *
     * @return Value of certificate.
     */
    public CryptoAPIX509Certificate getCertificate() {
        return certificate;
    }

    /**
     * Gets privateKey.
     *
     * @return Value of privateKey.
     */
    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
