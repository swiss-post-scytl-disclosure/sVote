/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.services.infrastructure.persistence;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.ea.services.domain.model.platform.EaPlatformCaEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlatformCARepositoryImplTest {


    @Mock
    private EntityManager manager;

    @Mock
    private TypedQuery<EaPlatformCaEntity> typedQuery;

    @InjectMocks
    EaPlatformCARepositoryImpl classUnderTest = new EaPlatformCARepositoryImpl();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init(){

        MockitoAnnotations.initMocks(this.getClass());

    }
    @Test
    public void findPlatformCA() throws ResourceNotFoundException {

        List<EaPlatformCaEntity> list = Arrays.asList(new EaPlatformCaEntity());
        when(manager.createQuery(anyString(),eq(EaPlatformCaEntity.class))).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(list);
        Assert.assertNotNull(classUnderTest.getRootCACertificate());
    }

    @Test
    public void findPlatformCAResourceNotFoundException() throws ResourceNotFoundException {

        expectedException.expect(ResourceNotFoundException.class);
        when(manager.createQuery(anyString(),eq(EaPlatformCaEntity.class))).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenThrow(new NoResultException("exception"));
        Assert.assertNotNull(classUnderTest.getRootCACertificate());
    }




}
