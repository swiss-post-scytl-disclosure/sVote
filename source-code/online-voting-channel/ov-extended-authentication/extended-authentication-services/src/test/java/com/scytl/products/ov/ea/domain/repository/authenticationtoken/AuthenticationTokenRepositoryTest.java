/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.repository.authenticationtoken;

import static com.scytl.products.ov.ea.domain.utils.BeanUtils.createAuthenticationToken;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.AuthTokenRepositoryException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.validation.ValidationResult;

import retrofit2.Call;
import retrofit2.Response;
import com.scytl.products.ov.ea.services.infrastructure.remote.client.AuthenticationClient;

/**
 * Test class for the Authentication token repository
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenRepositoryTest {

    private static final String TENANT_ID = "100";

    private static final String ELECTION_EVENT_ID = "100";

    private static final String TRACK_ID = "trackId";

    @Mock
    private AuthenticationClient client;

    @Mock
    private TrackIdInstance trackIdInstance;

    @InjectMocks
    private AuthenticationTokenRepository authenticationTokenRepository = new AuthenticationTokenRepository(client);

    @Rule
    public TestRule restoreSystemProperties = new RestoreSystemProperties();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());

    }

    @SuppressWarnings("unchecked")
	@Test
    public void getTokenOK() throws IOException, AuthTokenRepositoryException {

        when(trackIdInstance.getTrackId()).thenReturn(TRACK_ID);

        AuthenticationToken token = createAuthenticationToken();
        final ValidationResult result = new ValidationResult();
        result.setResult(true);

		Call<ValidationResult> callMock = (Call<ValidationResult>) Mockito.mock(Call.class);
        when(callMock.execute()).thenReturn(Response.success(new ValidationResult(true)));
        
        when(client.validateAuthenticationToken(anyString(), anyString(), anyString(), anyString(), anyString(),
                anyString())).thenReturn(callMock);
        final ValidationResult validationResult = authenticationTokenRepository.validateToken(TENANT_ID, ELECTION_EVENT_ID, token);
        assertTrue(validationResult.isResult());

    }
}
