/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.domain.crypto;

import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;




public class StoresServiceProducerTest {


    public static final String SOME_NUMBER = "50";

    private static final String MAX_ELEMENTS_CRYPTO_POOL = "stores.max.elements.crypto.pool";


    @Rule
    public TestRule restoreSystemProperties = new RestoreSystemProperties();

    @Test
    public void testCreation(){


        System.setProperty(MAX_ELEMENTS_CRYPTO_POOL, SOME_NUMBER);
        StoresServiceProducer producer = new StoresServiceProducer();
        assertThat(producer.getInstance(), isA(ScytlKeyStoreServiceAPI.class));
    }
}
