/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.ui.ws.rs.application.operation;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.security.Security;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.HttpRequestService;
import com.scytl.products.ov.ea.domain.model.extendedauthentication.AllowedAttemptsExceededException;
import com.scytl.products.ov.ea.services.infrastructure.persistence.AuthenticationException;
import com.scytl.products.ov.ea.services.infrastructure.persistence.ExtendedAuthenticationServiceImpl;

public class ExtendedAuthDataResourceTest extends ExtendedAuthDataResource {

    public static final String TRACK_ID = "trackId";

    public static final String TENANT_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String ADMIN_BOARD_ID = "100";

    public static final int OK = 200;

    public static final int PRECONDITION_FAILED = 412;

    public static final int EXPECTED_ERROR_CODE = 500;

    public ExtendedAuthDataResourceTest() {
        super();

        this.extendedAuthenticationService = mock(ExtendedAuthenticationServiceImpl.class);
        this.trackIdInstance = mock(TrackIdInstance.class);
        this.secureLoggingWriter = mock(SecureLoggingWriter.class);
        this.transactionInfoProvider = mock(TransactionInfoProvider.class);
        this.httpRequestService = mock(HttpRequestService.class);
    }

    @BeforeClass
    public static void init() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void SuccessfulResponse()
            throws ResourceNotFoundException, AllowedAttemptsExceededException, AuthenticationException,
            ApplicationException, IOException, GeneralCryptoLibException {
        when(extendedAuthenticationService.saveExtendedAuthenticationFromFile(any(), any(), any(), any()))
            .thenReturn(true);
        InputStream inputStream = mock(InputStream.class);

        HttpServletRequest request = mock(HttpServletRequest.class);
        Response response = saveExtendedAuthenticationData(TRACK_ID, TENANT_ID, ELECTION_EVENT_ID, ADMIN_BOARD_ID,
            inputStream, request);
        assertThat(response.getStatus(), is(OK));

    }

    @Test
    public void signatureVerificationFails()
            throws ResourceNotFoundException, AllowedAttemptsExceededException, AuthenticationException,
            ApplicationException, IOException, GeneralCryptoLibException {

        InputStream inputStream = mock(InputStream.class);

        HttpServletRequest request = mock(HttpServletRequest.class);
        Response response = saveExtendedAuthenticationData(TRACK_ID, TENANT_ID, ELECTION_EVENT_ID, ADMIN_BOARD_ID,
            inputStream, request);
        assertThat(response.getStatus(), is(PRECONDITION_FAILED));

    }

}
