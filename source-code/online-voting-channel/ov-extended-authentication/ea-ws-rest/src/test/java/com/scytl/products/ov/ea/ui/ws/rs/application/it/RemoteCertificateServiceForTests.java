/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.ui.ws.rs.application.it;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import com.scytl.products.ov.commons.beans.domain.model.certificate.CertificateEntity;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import com.scytl.products.ov.commons.infrastructure.remote.service.RemoteCertificateService;
import com.scytl.products.ov.commons.infrastructure.remote.service.RemoteCertificateServiceImpl;
import com.scytl.products.ov.ea.services.infrastructure.remote.EaRemoteCertificateService;


/**
 * Remote Repository for handling certificates
 */
@Stateless(name = "eaRemoteCertificateService")
@EaRemoteCertificateService
public class RemoteCertificateServiceForTests extends RemoteCertificateServiceImpl implements RemoteCertificateService{

    @Override
    @PostConstruct
    public void intializeElectionInformationAdminClient() throws RetrofitException {
        SystemPropertiesLoader spl = new SystemPropertiesLoader();
        spl.setProperties();
        super.intializeElectionInformationAdminClient();
    }
    @Override
    public CertificateEntity getAdminBoardCertificate(String id) throws RetrofitException {
        return super.getAdminBoardCertificate(id);
    }
    @Override
    public CertificateEntity getTenantCACertificate(String tenantId) throws RetrofitException {
        return super.getTenantCACertificate(tenantId);
    }
}
