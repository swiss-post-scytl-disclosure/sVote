/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.ui.ws.rs.application.it;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TestEAResponse {

    private String responseCode;

    private Integer numberOfRemainingAttempts;

    private String encryptedSVK;

    public TestEAResponse(String responseCode, Integer numberOfRemainingAttempts, String encryptedSVK) {
        this.responseCode = responseCode;
        this.numberOfRemainingAttempts = numberOfRemainingAttempts;
        this.encryptedSVK = encryptedSVK;
    }

    public TestEAResponse() {
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getNumberOfRemainingAttempts() {
        return numberOfRemainingAttempts;
    }

    public void setNumberOfRemainingAttempts(Integer numberOfRemainingAttempts) {
        this.numberOfRemainingAttempts = numberOfRemainingAttempts;
    }

    public String getEncryptedSVK() {
        return encryptedSVK;
    }

    public void setEncryptedSVK(String encryptedSVK) {
        this.encryptedSVK = encryptedSVK;
    }
}
