/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.ui.ws.rs.application.operation;

import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import com.scytl.products.ov.commons.verify.CSVVerifier;
import com.scytl.products.ov.ea.domain.model.extendedauthentication.CSVVerifierProducer;

public class CSVVerifierProducerTest {


    @Test
    public void testCreation(){

        CSVVerifierProducer verifierProducer = new CSVVerifierProducer();
        assertThat(verifierProducer.getCSVVerifier(), isA(CSVVerifier.class));
    }
}
