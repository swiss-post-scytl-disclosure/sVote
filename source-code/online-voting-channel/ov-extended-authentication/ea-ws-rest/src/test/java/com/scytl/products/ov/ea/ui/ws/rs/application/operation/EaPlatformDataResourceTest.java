/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.ea.ui.ws.rs.application.operation;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.certificates.configuration.X509CertificateValidationResult;
import com.scytl.cryptolib.certificates.configuration.X509CertificateValidationType;
import com.scytl.products.ov.commons.beans.domain.model.platform.PlatformInstallationData;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.validation.CertificateValidationService;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;
import com.scytl.products.ov.commons.domain.model.platform.PlatformCARepository;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreRepository;
import com.scytl.products.ov.ea.services.infrastructure.logging.EaLoggingInitializationState;

public class EaPlatformDataResourceTest {
    private String platform = "-----BEGIN CERTIFICATE-----"
            + "MIIDYjCCAkqgAwIBAgIVAK1iv49QAkCz0vpq/cfHtrFdPv9BMA0GCSqGSIb3DQEB"
            + "CwUAMFgxFjAUBgNVBAMMDVNjeXRsIFJvb3QgQ0ExFjAUBgNVBAsMDU9ubGluZSBW"
            + "b3RpbmcxDjAMBgNVBAoMBVNjeXRsMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkVTMB4X"
            + "DTE2MTAyMDAwMDAwMFoXDTE4MTAyMDIzMDAwMFowWDEWMBQGA1UEAwwNU2N5dGwg"
            + "Um9vdCBDQTEWMBQGA1UECwwNT25saW5lIFZvdGluZzEOMAwGA1UECgwFU2N5dGwx"
            + "CTAHBgNVBAcMADELMAkGA1UEBhMCRVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw"
            + "ggEKAoIBAQDKit/euH0XFMSot/CEDW9MN0Uoxxo6lKFb6gtyraQUoS1hSawXTRyr"
            + "sMzjEbnZQY1lPWgFAZCMzSVK7RZVetiqng3uTmWFll5/XtyxR+P4GD0Q6OW0icFr"
            + "oDAQ4AD/YMEVX7T/dWsnh1j/NIg1sXsTLjNZyswXiXlhe71wrEFb61/NtxFu//rn"
            + "ZtPL/oQn/MLmXtrQZVPEQ1KDhPpAbvruNyqkThWyNvoEf3pUMeYuwMwNqcgPiOj3"
            + "1z9orIeMCnTagquJc3KbEE2/WFPaWmETl4w+gpUcafxmFvsSXijvmaMW86sc+GT2"
            + "L5NjFpewSXG0vghg7f167tC9uIZBQ2rhAgMBAAGjIzAhMA8GA1UdEwEB/wQFMAMB"
            + "Af8wDgYDVR0PAQH/BAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQAEU1EcIPEUQOf8"
            + "Do0Z+R3JwRo6YmknCbATSDNmjGHLLwsaJn0SnI+fQXLdw1eE8XR3WnmF2Oi0ZDO+"
            + "6wFo+cZYFHtaCoO4TqpJjyJt9lDXOGv0+tTkmHRZdGfceVhbg1aX3tEL5a3Nkyt5"
            + "dolPZmtMqLVUsxOQOU+/q6S9cRaSeSukEc/xgydeOAJ3LBrAGCGUXMNxqf/g7u8n"
            + "jQ3QEOydTxfZh7UKndufnFSzP8RlPYAJsoknwmsl/SgSCNcaGNJoTBAUKw30xATe"
            + "NzP7MQZ+wvdix18dshSyCemV5cdTNT4ZpEh8DUuc80KrCKOIHSeydpUUe2Khvdb8" + "cD7sazdu" + "-----END CERTIFICATE-----";

    private final String loggingPasswordsFileName = "logging_EA.properties";

    private static final String ENCRYPTION_KEYSTORE_BASE64 =
        "MIIOTgIBAzCCDggGCSqGSIb3DQEHAaCCDfkEgg31MIIN8TCCBXYGCSqGSIb3DQEHAaCCBWcEggVjMIIFXzCCBVsGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBSpXfE17r1SFLsQ9lkYzcmmiQYL7gICBAAEggTIO7oSKQtZwlNoOnOu7lnliwmzzu+3NYNMaeTCp1kmuhoqDIfthCcTqiljsCYa4W67sV2i1weoAsWqKO6Q458nrDSfRnKcF6wK19MEkyMxgQ5OJ9ra9cYZm0Fqic/kFA0RRpC/HcQKl1X/DYRyuNcRXsX3sXa5TnqrBNBP/0I7a0G80rbfir8Pf8gUFP1UjGNVFeiV1gwTgnwz0WjRMXoo+JaEZWOnP6ldilY7D2RyatfRMdx1275u31jt/mgK15NjNq7JBJHyZDBfHSpX9g2uy8abnZhnU9jVVHEBxtQLPRScsKTP8MwY7h9pSiJlyHQQIvDlOwyN9R4eqkGrREIoW9ImNbyrFneqJm0mA1AKI5N41mHNMB24UUlq7vlu180JEp6oKbqAPj0gssE5m3wtRQA4PWH6XOZlqaNsJ37xtAYrOR85fTdYHaDMsj0TTCJERn2DeiJWT7e1TtgqmjLkFrC6V9JEnGfwYQTgrgyZotzZh5I8g9MQC2Eicf2hyoYUOhkQKaPvpJaVaELsAwnm9lsRbq51of/O6GhLsbOlGz3Asw/+QfQ6CiFXxRH+q2URhtcJMZjpu1VuIOtL1PmTeoBtVwHW3B5HS4i397O18wz5e2HHukXz8J9p8B/8JIaQ65uaTbYQpyE6c24mGIRdWwunqVd6q/hveHxX1osTbb04ajuxeLCBTb8sPd/YTo88Q9mkLhripmUJ6naKnPqVUgbYfWUff/5F1AosahYLHKD8laYqSoUp8WdirDKlsBrkwWc4nDP50oZFQ4JuL60LXnw/aD3iCa5zJULxUm99J+aAzjuntHKhSiCWmxU70VRa2ygsJtuCfoPjcWktXUiyM8IZNNEmLVLEpYfXYmKBbVwfxKj3nPK36YOm+OiHM7S0Pu8QcHsy96Bl5yX6CHKVehG/JDg/5VBjr22So7HDMDojYFf731ExANFM+htg36VdRdGtlB5Qw6FpXYGoCngGyJG9wo6oRyuzUpslzXR4D+zOriI4xtqjpYAnSvohrRt3THSu/SmyaAl9yfy/+0lAE8E4pZoVKDNIt1D/1auNBbS6/quIdha0CI3ztXiEaag3S9S6kiulXIpll1a0L4ZYCES4jIJ9zFW3f34Tq8vVQ2sE6GIU1qfO49K+8zQxATNrHnOj1FHU0/TdO+zcSns/sXqBa18UrMH2MHdUBGUraTYkeJT9Rehivods8JvNDt+x1wNRMJVtY3G5YTmVmqkYiGSOsWkjCLqhrqRsLQWJBaLN8nrjzy0w3JjLq9gkI39juhWyAoDu4ywbkRK2kWAjsUwC93vfVFShqiyUrY5XMoaFRZanRYd7FNPyzPI7EHNkPsSTRwD9X4X/HxNlX7fAME436s7nwM3Zmbjgw+fys1QjX1gutCX0EeNXgk5lXwRuDUo4WxS2gukeFOBf54d06ZrbYKPhK67MTwdBzYETxMcHg9/5gm7N0Bl4IQs0rt5IYRbniFpPEzC0OlSdU9d8hWUrMcXzl+JHquwpPGpgFr1VJDXfm6G9l63L2ULJuI4rMojAcniUI8DDf9CIvwUP30VjgEOxkzgStYeXXF5bZYVt1MWja5t1b0cNaMyE8WuIIaScWYLQVS2qAZZOdIUdIuEKOtbvKlxUMU4wKQYJKoZIhvcNAQkUMRweGgBlAG4AYwByAHkAcAB0AGkAbwBuAGsAZQB5MCEGCSqGSIb3DQEJFTEUBBJUaW1lIDE0NTM3OTU2MTgxMDMwgghzBgkqhkiG9w0BBwaggghkMIIIYAIBADCCCFkGCSqGSIb3DQEHATAoBgoqhkiG9w0BDAEGMBoEFJViri47BUKOXhqA3/u1e/65VMhZAgIEAICCCCDdQzB1Bgi8Bw8pZBvPLNtAX5m9dfRKBjKQJi+Y+i6xoct3P2yx/XIlE0TAE11fSufkewlEsmoURllASk87deRYN/SoakKMSmmT+MG+4jp2KLUSDLjKup/TNulEoOu5nzR+iKeNxl5JVPoptwKkgnCucZUd7qjvSArGjxDyt8vHZaJF4oaGyN7jIbi6vsne58hpdeWJo7Ltxt/hRSu7DLWZkwfkjVdVlei9vgQyC0viLbDGQAYzsHYCDaJBOYq/EG9OqU8YhEZ+CcQ6eDpss8A67/uvgZytZczYKBNDgvhG2QhWgGpmCbF1qshUQrGFk5qWdCvefzvEIyHAZ4DuPCXb3ws4N0zJXp0+yQfgqGuVliwRPjcsi4wlgXj1/z430EWGVPnTvBLXBLEnHJ2jpDbEIQDkYHlVrmxSZCVejh4WybQEVhYLSNmcNgzybNk7lBWeucbkjFhwvnWRiLT8f5xI76LCL1R2w6806MQJZCjiN/Xpq0oaFpyVrBOgbzr9BTCTdA1LgB1R6XVvWfUaKqxNu9wMUeGJiIazHp50bqDM3lV00FG+kYgjsqUl1RKUQwN1t9lN5uFpxqxPoBLucSSi3928DRUH7/e99aK6vA5+S7AEVBY9AD7FBUHxM0hryriOCiQWcgo9/I7y+n+tLfuqhp0C03JA807Q89/XuzzevdRGGxHjg4mJ6xhW9jYaDPbpsUxCWaYaIEdKT0aZyK6jvPsiFz6rEaINdKwuHIrQTh1Vgw+hvE++soUYqS5sP4E9jFxw9wK50o2An4ozXOxSAui9TL4/AIrIz1XFMDPhNjqOeVOzLGfDfhowbUz+9GcJktIBd34KusnQ0ZaIpdwmBwwkQVIPNPv66GPWbCGIYSAVt60mFiePGZ0rpDYTWS7gGyu4ZzKtUFdGZ1V5wGnrNbfneuKmHWWxGxUM2EGiDc6vKyN9NpURWKckOv5ZRC+CANSCosmAN3CZ/vgVE1+Sqs4avwGwJ1tdQYNsA32bpqkw9xFSn04/NTuM552jYNZBRWQNi8hYLW6kpjsAMPsPQSzcHSpd1EHMLVHrPJjHM+w3LyQKAZQaeocCG0L8OAO7V0mvk09KYngs2/jEOiNJ21D/8chJS7+uM9zLA1T+BsSfWwMi1do1p3pQN+zNfH35JMybS+EsJmV/lWqklYgN+9f+Kn9ubJ8ErwwQ+4ovJHXxjVWJopvjKr3mRf2SfKlYznoIUhP8v0v6AQ94/c99KgMwbfT4itG2lLpIpSn1OKHVCmPUReTYDIj4A+n5sSCMwWIvEB5GNFVlhwyvSgYU0ZVxNK1hADz4vbJpYUobixZsh+cwMKAN++Oz7vdhy7cMYlyxCtsYQd8AiqlpoWaXQNNtl2cUNq8V6PM9rXw775o0uze+L2wmZW+LauXzb+LyjKm8v51pdFqMzaW3Azw+gOC00OkB0s7glOFmrqx1XsaRb5I82LFJc+KFSZ3IYyHLyVLrn28Y+YclAt3Bf4sMwbFXabZk8erVs8gq2c6ULOWfU4vdwkSUidkNADaQ4dlF0Vin9Pkt7xabHmm56fdKFUlR/UVqxCNt6o1Uk6u0EoYTthGC9ohsa7bmMpg22w94AGz471Zinu9wUvEbcBS46LrCLcto+Qit9CFdBw31aTnB7i+Ds3rSodyaMq8UH+NH3119CUZPqDmTfAIQP1SYjF62GeJ+sjnz7Xm9eR/s2wRUc53Vh1/bcMblkCDpQ7h7DHtkdOfFxB1CU6r4ygZBufHBdDYteRZ3iYACzM9rBqIfQ3g50ujdN9idlDJnwk5w8Zesiv03TIZNzMJEjKrn7OpPEkmccUYGHy2BkrHBmXDoKpxsZo6+JA02juWZCBkUOWZLrtscRyJ3XV1Hi5gTLU473OWJgLoTX3gtlyzyrZANTQbfHw+OhSBSFlACcOwBroa5K5HdSJ+MWG3MU6YwGjfL3oPB7nrlcN53zzVCeW7cOa5btYZ9EEjCBBCy3aN06CB6QcNmbOd1YdL74Du+7nVZwG0ahWV9GGg4kx+4Gkpiv5H6GaPNtWuLT9M+x4rw9jcLZc66G7iZDrtkCaY3jEbVpITV7/zB5dp4Fv+/cR9B+HhLZQxnqE6wxK/vBW8N3HqTSXTb8Gi+l8dYoN8/WugzMoH5oVnRmh7qYAoJyWCXI/Cb7Z9CJmlrLl3wVwNthYRwzLdN8TlVXAkJu/c3M+NTSPrKr21KjtVtlNjQHik5NgDzlT5Cu+xnLe6vrnzWAIrx4h9VvUkwGHBn0MmWFIr3p5gVJeit9fRGrOLAAhkU1LqqXUKaeRKVhnks+l79SOXaSSYm9tCccz8D2YvrW6b9amBxFJFqSQTyouXr7lNHE78AVd+APQzdXdq9vgl8oarkygfoWuGuwNojigjP3d59iluqCc1q6mJXDGheY1qgmnzPYmCuZMvS32AZPsCGsVx/mexDWzMuYOROeVqlxly3tJKN4G2d122V5ywjaZew/Rf6vdo92CGVfBV6k5SPoNrNmosFHrxSd93CGgyuQqSYx66/Ul4Jxq9R8fZOh/1hlR3GDtuin2r9nwIvIjxchAaGzz+s6CaDrcppLACnIp2Y30mZXM16nafp8kS2c9crqf0r6HNES4AGta4zCUaXmd+i40Jixz48Si+JbWpmcLq5GS9QhqjQneXWG/pxBvzorRDfjtQYIHoJWe9LH0Mgrv4GK6K/kBsir6bmkOw8NgJ8YRN4a2SjG0Nj4Z9PCoflasxjzOArjvJcpAExPxgvmOMEgARYyNy/mm6gwVxaMD0wITAJBgUrDgMCGgUABBQob0jNhW/JtHkJ0gbSPykVy6M6oAQUaAZdQgguD9UWehJ6p019/9xTs54CAgQA";

    private static final String SIGNING_KEYSTORE_BASE64 =
        "MIIOPgIBAzCCDfgGCSqGSIb3DQEHAaCCDekEgg3lMIIN4TCCBW4GCSqGSIb3DQEHAaCCBV8EggVbMIIFVzCCBVMGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBQR4qKZyPp00m1BHbzSFDLYlKqXeAICBAAEggTIhy78ZH5OzN3/loiO4Sap0Le3Wm9Lcf7W0wCo7pl0k37XHx5JLzY6MkiSuAJBoyTUxsgbQikwtTt1wiypBJp2NzKwM1X8pTJCnqkRhlbkYbvUTNOInMeIZbxFVKYpQ4s5varzERM6mIkp3NlhrbwXFinMioT9M7kRcsgCX97ecuRk0NHTbjYJeN8exXF6FYg9aEyRu9aO/N/gR9cXzfAo0h8eiuq6VzEZZP0k6dAswaNqMzy4zXIa84UdVZUjs1Oln/znsgcR7tzc6ji4ixoIlEdOkTvWqv5I4LR+EEeH5eDASo/lcbCa1og6wJDAuoeM5vcB6w9c9ljgDrFaiNgBNTI9Giqi1KiO71D7D44tKWHbHKiD9ShomebOmijgc33Y0Izf6tYAp+0Jfuj3+BYLPk8ZhvMytZcMv0qOP7aPOkoHjyoinTJB+okiTQnLLFJzq0kkPT/aWoA+Ixu57pV/zxvCYR+UpGLuGfpzk/Db6bv2zXHVgbJBhBNS+5UQQ1/e8NrM2AnAbQmQ0LxQrjL7HCNx/hcds08y3WRbJqKJtf7BkvuIwy2SB7tQTBmNPSWNaOYRbgTZwsS4qnJRKbSpU2VIfC+1MnsmpwAku7LHillXBoQFf+XMNrbJ1xzE8MYLAmjbzshxZVugIxS6E8GnykzVKHy0tG3EyplQc++yH05ulJ4wpTccIQWl/6TBvkRmCc/u40aeh/xmJVH3G8Y+MFxomH5+ol35/gWNqvTrpq52ZgMVCnseiOqA5c2Bnkm08iuKnMrDNmJuwl5pSueAPXANYAJen3dBUVscPJdzaogDCre+QkWct64YtOSG+P5Zux3t3OClCMY8O+6qIut9wn8UWZ910JIQjdPhjp+F2Pb8zjLrIHJjp8mPzLeOinAnDVg05OWtY8KLIL2bMm/M9P5AZE6+f7A7Hq0ou5xgEwRD3rwHwCCsGvDrCWNC/yB464M9H5iz8M///oMorhV6KAADb28oM+Alv5AXf+DQ94dLBY0mBRGFOZ9py1yiAMf3NLPzDMVhRMChSLlLr3zexqlxWR6EtiOr0X+dqnRlpw3FJ47idqJI6DhPIjsTwZfR0olf2q3VnCwSVBvxo3G6SeFeKgJKvNzwvUV2bRU+pRIuHEzR2gXU/BC5t/eP+JZa1xDOp3rbEUXMVUI4GEYH3CP4qawIwg17nNaQzT3QRF+cuFdG+WK4rc1ujVuVtaNxC4ZVf52j7d8U1u++tXHHWir3dWbszsRwQHNiN7khrgatGkHhCZXr36TFHBZr3HE5iLBNqHv4Cs2duhoTlAhjE8mmzY5bJnIYb3pggwJL9O0OZXKbcVNE0ugceYDsdQzPPFYMxtmimHYYoLx6rvHjjTWzWbKbddLufNhz7P8D21p5QjHVwKqhU88+yrHh3kBhZfA3QwFh7bSPZ+nU9BcMbn/lX28DBXaGQFcwufoDA6SzCsY2++scWdfgHL+NlRRsOf6Xkg4lJewoRr9ow9BMYbEPTexg/hmopNiTXQTgTIkaYuVP8Ckib0AYvLaSlws4jAOsGWycYolF9rTb6I2dqLZKr1RgBtedGQjJyd/WcQ/DB1XeSlJU74DGeYxZ6/TZeQV60CQB72rplbKYSKHh8z6LZzUmk65lMUYwIQYJKoZIhvcNAQkUMRQeEgBzAGkAZwBuAGUAcgBrAGUAeTAhBgkqhkiG9w0BCRUxFAQSVGltZSAxNDUzNzk1NjE4MDcyMIIIawYJKoZIhvcNAQcGoIIIXDCCCFgCAQAwgghRBgkqhkiG9w0BBwEwKAYKKoZIhvcNAQwBBjAaBBS+CdnFGEs3Ic8uZWItGo68dDkRlgICBACAgggYYIiztW4l8vKQsqjH5NvI2JGsnx7f6olocnGXHT8URkw0aaoMS+Gx5aJlLgJcXEhlvuVgqOUSM7DkFKaTyz4Ggry5fUrgGL+n0qSTAK+eVtNjNc6izRb+FiqT3yKcWfOX5JwQYlb1dUXbxhEKxH4zR4RFHH702Ouh3T5e4HL1OmMyb7A8NyJdG11S2Voeby+Pf7wrCiKKAvY+BVzwAVnCsrMFyJR0Dei8f/8GfzHoMwDcRogF4/G+OFYPn1VxJj0Ff1ltuzmengOrQAKVq/sN/e02VJ72flaEE0keY2k2Tm8hnIjQ9Cvw3n5Nnuvc0xNYAYUWiUoHfyKqay46g7qBJZwLYKY+t4zJx0Aoa9EXeArDzHJdaEaNQ3C/97tCxCl68YFY9lM0xrtYgU9xHFCGsIjqpJbbVjzWbDexrgH2AOLzRq28psKufeecYeWnJqIQqY4i8fVCzgc8lt72dt71CBLn+pF+7B64my9MbufoV5Zic5y7prFJhMGdQK6hGBPNd0lRqO9Q3JVIvSo13NVjtoz6XEN1UBCr5MhI3VTJmevCnQWS2B3Db+3w0APJAqTGQtF2Mwt0s85YwodbKm4bzoMAZmq5Dprex6Py4RxQJu8PaXH4JnadfQAO8HM9Q4NSsI7gNCxKid5/F8t3T3atp7G6B/2jofEI3YlIA4VQSW8wuYp0oaR9/noqhUshppOVSLHut6pUPoHTb7tbyDQtT18UryCbZQ9IHjGMCHmRXJHt5zv/9yyZWWECRkmwnahWO4W4N55RGgOkfSPzbP31rhcWt/75iW5lHwvQN7whKOkmob/14Bf9OHQXCOtUay1/M4VfbENcTAlqthutD2tqrzlMAllz8/KoH6BYJ7GvuFIx9dSYnEP+tNnQSgBOAr6Dy4NqlfLK8ZE+fzAlAM158n17fL1x2iCd1BBROuLpWtt9SQV/j8VXvoaQB1pU0qeFkz1b1pgKrl6fFZsf8XMstxpAwzCer9Cn1UB2P2gJwMuxDpXlTpRk0gtQlU0qG+rgYRKLr5EVNjPG0nTURfpHcv++D5biuxoBpr3NHG3KKiNF4t9JKMl7TiRL7TjZIXRdg08yLe5Fhmbr5d4S9Uj3Wr+y+pRZQ0LZ55vzutkJPT1XX66DGmxMwteHkFuzP7x4SQAK5LJ5WnhQpQVX5JNEUVNg3aIA+lDd7X7194ZA4D8qLyx4u+/suSQPpR49G7bWkhu/mlEh3tOngP+cPtrwDKpZoABhqsazycS3dvFRqLH2JKdivN9Jsvx6PnKt/j2gUoWHtvv45UfwakOWvI3LOmqxzzVfz1dRfWiswqvN50O4D9fV6FBHKvKTmzQuPnCF2KpSGxKGkQK902hTm8ymr8KLxt+SK2QPmdQgkLjS/8G+FHdx8tg6WypGysw23PkchzQUVeTn9GMur4ugc3mYS4ynh/YMH92nkgd/ozzN/phwMzLt+coHx6MXl58sHca8d02aoNdD8/7iqe6NM+I/qONM5eVl+yf4hMOQYvzeyCFrVC62+ivL1n1uHFNWM7/q8KEIP9bZIUxsa4rgjEJ/H02Qzd1J8/FnllcdBqLXftPqiLc/C4Sq5WUUYLIf4kVZVQv+GyhWcvs0T7m2i0+ZkMo1NcepdyDn0G7HE9N3ToK0xDYjF3HS/Mx1yT9fJ6JaCXJmikxMjmM3j+PdFTs3cNK+ZvNOsMuD1XUwtZWzwLa1GulBkuU+RyiK4VddYy57i7j1sQ9RfJ9fo/EV+wPMxSdSSff7yJS9Nvnz8EN2vx2QqQETFSrJPsohiDFla2pzjuSwXyHbbFzRvsU6Fj+m8GzFcAQjMuQ1CE16yO89Hul2R1Rfb7S2tGfFpP5Eo5+O2ghi0txtiXfRuWmkMLqXr2WLMsf+jxhpcw38iZEft78HgaZfnjgGd1O4VRvS+4T3z9x925u01NUH5wwF+VgEQUuh632XYgfoISup+I3mWObhoUbnbX2cdDk9xKBPi4gbNAsm5dFlsh8TPLfWTGuyxq5OtJ15OL/NZxLyyTAcg9C/AeM347DSpHGS1DmU//NHvBYizFbHMBQDrcTiMnOmWVNyLgWt6pvr12NSGBppZnJOJNtZ1bC2vqapB63ZTZ5BedAgMfJSJctPGwge5w2p8mvbYpf8HDO42Mg6Fv5R9oMCKTFkaQCl3tDce4YmRxM7n62g/p4aGuvcYk4gnZNjfeMv0GO/832+6rafwO4RUqOqDJ6D8EgJRzXucZFt1XHIL1Ndd2OoV3Vs401mddaFNaHEpVwCSiltLFon9zZ2zTGOQhzC+wkd8YLjFfq7JQPcniLEBYZzuKKV1VJhxlSi9sMkD8qaUzolgm0QMic1PKhIlPK6bSb/cpnzXkgnrQSBDGfzY5oczx7SpqfRwFzHMcfAQWBqaGW/lN4gbWbXiscvgW0dVVbcI7XgcUO+F18aaox/UJlI8tbgCmEJWJqlCZm6zKmuAvLcNupF2B8YOu0k+8VEGI+DDc/hBpm2vZdIxdeEMwjR6A95Nz6I92OnRUegxeelPY1e0wSRC7HOnQO2P1tzGr3PBZbLUndXNuoJtdaZP2UFQBbEhgPCcpwJtGfO4ZsEJ9Ro6n/HXPrhjEXGFv7iUEmmpM0CWe4UM2oJm9HszaBDgBXgLl1VYJnlrFV9kdNmp3PMffreXBq8A9x6MUZdc1Yq9UJfqIaxRFUP5BhA9MzievJVlXyoC0br4vG9PkwuLMCy1vgvHF+i30Ahb29ciILuuJWSrbq/XvNq66mOgJbV8LkwPTAhMAkGBSsOAwIaBQAEFPR+VCpUZ09450wX5krLQdjrc1a/BBT+0Qgmzup8nTvSnCEb8NXl3RpdygICBAA=";

    private EaPlatformDataResource resource;

    @BeforeClass
    public static void init(){
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
    
    @Before
    public void setUp() {
        resource = new EaPlatformDataResource();
        resource.certificateValidationService = mock(CertificateValidationService.class);
        resource.platformRepository = mock(PlatformCARepository.class);
        resource.loggingKeystoreRepository = mock(LoggingKeystoreRepository.class);
        resource.loggingInitializationState = mock(EaLoggingInitializationState.class);
    }

    @Test
    public void testSuccessfulResponse() throws DuplicateEntryException, GeneralCryptoLibException, 
        CryptographicOperationException, URISyntaxException, IOException {
        prepareEnvironment();
        
        PlatformInstallationData data = new PlatformInstallationData();
        data.setLoggingEncryptionKeystoreBase64(ENCRYPTION_KEYSTORE_BASE64);
        data.setLoggingSigningKeystoreBase64(SIGNING_KEYSTORE_BASE64);
        data.setPlatformRootCaPEM(platform);
        data.setPlatformRootIssuerCaPEM("");
        X509CertificateValidationResult valid = new X509CertificateValidationResult(true, new X509CertificateValidationType[0] );
        when(resource.certificateValidationService.validateRootCertificate(any())).thenReturn(valid);
        Response savePlatformData = resource.savePlatformData(data);
        Assert.assertEquals(Status.OK.getStatusCode(), savePlatformData.getStatus());
    }
    
    @Test
    public void testUnsuccessfulResponse() throws DuplicateEntryException, GeneralCryptoLibException, 
        CryptographicOperationException, URISyntaxException, IOException {
        prepareEnvironment();
        
        PlatformInstallationData data = new PlatformInstallationData();
        data.setLoggingEncryptionKeystoreBase64("ERE" + ENCRYPTION_KEYSTORE_BASE64);
        data.setLoggingSigningKeystoreBase64(SIGNING_KEYSTORE_BASE64);
        data.setPlatformRootCaPEM(platform);
        data.setPlatformRootIssuerCaPEM("");
        X509CertificateValidationResult valid = new X509CertificateValidationResult(true, new X509CertificateValidationType[0] );
        when(resource.certificateValidationService.validateRootCertificate(any())).thenReturn(valid);
        try {
            resource.savePlatformData(data);
            Assert.fail();
        } catch (IllegalStateException e){
            Assert.assertEquals(e.getMessage(), "EA - error while trying to initialize logging");
        }
    }
    
    private void prepareEnvironment() throws URISyntaxException, IOException {

        String externalSourceOfLoggingPasswords =
            Paths.get(getClass().getClassLoader().getResource("passwords/logging_EA.properties").toURI())
                .toAbsolutePath().toString();
        File externalSourceOfLoggingPasswordsAsFile = new File(externalSourceOfLoggingPasswords);

        String destinationDirectoryAsString =
            Paths.get(getClass().getClassLoader().getResource("productionPasswordsDir").toURI()).toAbsolutePath()
                .toString();
        Path destinationFileAsPath = Paths.get(destinationDirectoryAsString, loggingPasswordsFileName);
        File desintationDirectoryAsFile = new File(destinationFileAsPath.toAbsolutePath().toString());

        FileUtils.copyFile(externalSourceOfLoggingPasswordsAsFile, desintationDirectoryAsFile);
        System.setProperty("loggingpasswordsdirectory", destinationDirectoryAsString);
    }
}
