/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.ui.ws.rs.application.operation;

import java.net.URL;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hamcrest.core.Is;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.scytl.products.ov.kt.domain.model.ballotbox.BallotBoxTranslation;

@RunWith(Arquillian.class)
@RunAsClient
public class KeyTranslationArquillianTest {

    @PersistenceContext(unitName = "persistenceUnitJdbc")
    EntityManager entityManager;

    @Resource
    UserTransaction userTransaction;

    private final String TENANT_ID = "100";

    private final String ELECTION_EVENT_ID = "123";

    private final String ALIAS = "alias";

    private final int NOT_FOUND = 404;

    private final int STATUS_OK = 200;

    private final String GET_BALLOT_BOX_TRANSLATION_BY_ALIAS_PATH =
        BallotBoxResource.RESOURCE_PATH + BallotBoxResource.GET_BALLOT_BOX_TRANSLATION_BY_ALIAS;

    @Deployment
    public static WebArchive deploy() {
        return ShrinkWrap.create(WebArchive.class, "kr-ws-rest.war").addClasses(org.slf4j.Logger.class)
            .addPackage("com.scytl.products.oscore").addPackage("com.scytl.cryptolib").addPackages(true, "org.h2")
            .addPackages(true, "com.fasterxml.jackson.jaxrs")
            .addPackages(true, Filters.exclude(".*Test.*"), "com.scytl.products.ov.kt")
            .addPackages(true, "com.scytl.products.ov.commons.logging")
            .addPackages(true, "com.scytl.products.ov.commons.tracking")
            .addPackages(true, "com.scytl.products.ov.commons.crypto")
            .addPackages(true, "com.scytl.products.ov.commons.util").addAsResource("log4j.xml")
            .addAsManifestResource("test-persistence.xml", "persistence.xml").addAsWebInfResource("beans.xml");
    }

    @Test
    public void testGetBallotBoxTranslationByAlias_NotFound(@ArquillianResource URL baseUrl) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(baseUrl.toString());

        Response response = webTarget.path(GET_BALLOT_BOX_TRANSLATION_BY_ALIAS_PATH)
            .resolveTemplate(BallotBoxResource.PARAMETER_VALUE_TENANT_ID, TENANT_ID)
            .resolveTemplate(BallotBoxResource.PARAMETER_VALUE_ELECTION_EVENT_ID, ELECTION_EVENT_ID)
            .resolveTemplate(BallotBoxResource.PARAMETER_VALUE_ALIAS, ALIAS).request(MediaType.APPLICATION_JSON).get();

        Assert.assertThat(response.getStatus(), Is.is(NOT_FOUND));
    }

    @Test
    public void testGetBallotBoxTranslationByAlias_Successful(@ArquillianResource URL baseUrl)
            throws SecurityException, IllegalStateException, NotSupportedException, SystemException, RollbackException,
            HeuristicMixedException, HeuristicRollbackException {
        saveBallotBoxTranslationMock();

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(baseUrl.toString());

        Response response = webTarget.path(GET_BALLOT_BOX_TRANSLATION_BY_ALIAS_PATH)
            .resolveTemplate(BallotBoxResource.PARAMETER_VALUE_TENANT_ID, TENANT_ID)
            .resolveTemplate(BallotBoxResource.PARAMETER_VALUE_ELECTION_EVENT_ID, ELECTION_EVENT_ID)
            .resolveTemplate(BallotBoxResource.PARAMETER_VALUE_ALIAS, ALIAS).request(MediaType.APPLICATION_JSON).get();

        Assert.assertThat(response.getStatus(), Is.is(STATUS_OK));

        deleteBallotBoxTranslationMock();
    }

    private void deleteBallotBoxTranslationMock()
            throws NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException,
            HeuristicMixedException, HeuristicRollbackException {
        userTransaction.begin();

        entityManager.createQuery("delete from BallotBoxTranslation").executeUpdate();

        userTransaction.commit();
    }

    private void saveBallotBoxTranslationMock()
            throws NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException,
            HeuristicMixedException, HeuristicRollbackException {
        BallotBoxTranslation ballotBoxTranslationMock = new BallotBoxTranslation();
        ballotBoxTranslationMock.setAlias(ALIAS);
        ballotBoxTranslationMock.setElectionEventId(ELECTION_EVENT_ID);
        ballotBoxTranslationMock.setTenantId(TENANT_ID);

        userTransaction.begin();

        entityManager.persist(ballotBoxTranslationMock);

        userTransaction.commit();
    }
}
