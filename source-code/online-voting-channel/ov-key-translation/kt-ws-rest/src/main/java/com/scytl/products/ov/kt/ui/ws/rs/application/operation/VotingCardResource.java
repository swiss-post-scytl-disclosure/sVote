/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.ui.ws.rs.application.operation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.ws.rs.ObjectMappers;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslation;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslationList;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslationRepository;
import com.scytl.products.ov.kt.services.VotingCardTranslationService;

/**
 * Web service for creating and getting voting card translation.
 */
@Path("/keytranslation/votingcard")
@Stateless
public class VotingCardResource {

    private static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    private static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    private static final String PARAMETER_VALUE_ALIAS = "alias";
    
    private static final Logger LOG = LoggerFactory.getLogger(VotingCardResource.class);

    @Inject
    private VotingCardTranslationRepository votingCardTranslationRepository;

    @Inject
    private VotingCardTranslationService votingCardTranslationService;

    /**
     * Returns an object containing information about voting card translation which matches with the provided parameters.
     *
     * @param tenantId - the tenant identifier.
     * @param electionEventId - the election event identifier.
     * @param alias - the voting card alias identifier.
     * @return http status 200 with ElectionEventTranslation object in json format if resource is found. Otherwise, http
     *         status 404 if not found.
     * @throws IOException if there are errors during conversion of vote to json format.
     */
    @Path("/tenant/{tenantId}/electionevent/{electionEventId}/alias/{alias}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVotingCardTranslation(@NotNull @PathParam(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                             @NotNull @PathParam(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
                                             @NotNull @PathParam(PARAMETER_VALUE_ALIAS) String alias) throws IOException {
        try {
            VotingCardTranslation vct = votingCardTranslationRepository.findByAlias(tenantId, electionEventId, alias);
            String vctJson = ObjectMappers.toJson(vct); 
            return Response.ok().entity(vctJson).build();
        } catch (ResourceNotFoundException e) {
            LOG.error("Voting card alias not found", e);
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    /**
     * Stores voting card information in the system.
     *
     * @param tenantId the tenant identifier.
     * @param listVCTJson The information to be stored.
     * @return The http response of execute the operation. HTTP status code 200 if the request has succeed.
     */
    @Path("/secured/tenant/{tenantId}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response uploadVotingCardTranslation(@NotNull @PathParam(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                                @NotNull VotingCardTranslationList listVCTJson) throws IOException {

        LOG.info("Received voting cards translation " + listVCTJson.getVotingCardTranslations().size());
        votingCardTranslationService.save(tenantId, listVCTJson.getVotingCardTranslations());
        return Response.ok().build();
    }

}
