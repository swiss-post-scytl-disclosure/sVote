/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.ui.ws.rs.application.operation;

import java.io.IOException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.ws.rs.ObjectMappers;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslation;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslationList;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslationRepository;
import com.scytl.products.ov.kt.services.ElectionEventTranslationService;

/**
 * Web service for creating and getting election event translation.
 */
@Path("/keytranslation/electionevent")
@Stateless
public class ElectionEventResource {

    private static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    private static final String PARAMETER_VALUE_ALIAS = "alias";

    private static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    private static final Logger LOG = LoggerFactory.getLogger(ElectionEventResource.class);
    
    @Inject
    private ElectionEventTranslationRepository electionEventTranslationRepository;

    @Inject
    private ElectionEventTranslationService electionEventTranslationService;
   

    /**
     * Returns an object containing information about election event translation which matches with the provided parameters.
     *
     * @param tenantId - the tenant identifier.
     * @param alias - the election event identifier.
     * @return http status 200 with ElectionEventTranslation object in json format if resource is found. Otherwise, http
     *         status 404 if not found.
     * @throws IOException if there are errors during conversion of vote to json format.
     */
    @Path("/tenant/{tenantId}/alias/{alias}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getElectionEventTranslation(@NotNull @PathParam(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                                @NotNull @PathParam(PARAMETER_VALUE_ALIAS) String alias) throws IOException {
        try {
            ElectionEventTranslation eet = electionEventTranslationRepository.findByAlias(tenantId, alias);
            String eetJson = ObjectMappers.toJson(eet);
            return Response.ok().entity(eetJson).build();
        } catch (ResourceNotFoundException e) {
            LOG.error("Election event alias not found", e);
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    /**
     * Stores election event translation information in the system.
     *
     * @param tenantId the tenant identifier.
     * @param listEETJson The information to be stored.
     * @return The http response of execute the operation. HTTP status code 200 if the request has succeed.
     */
    @Path("/secured/tenant/{tenantId}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response uploadElectionEventTranslation(@NotNull @PathParam(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                                   @NotNull ElectionEventTranslationList listEET) {

        LOG.info("Received election events translation " + listEET.getElectionEventTranslations().size());
        electionEventTranslationService.save(tenantId, listEET.getElectionEventTranslations());
        return Response.ok().build();
    }

    /**
     * Returns an object containing the alias (public identifier) for the requested election event.
     *
     * @param tenantId - the tenant identifier.
     * @param electionEventId - the election event internal identifier.
     * @return http status 200 with ElectionEventTranslation object in json format if resource is found. Otherwise, http
     *         status 404 if not found.
     * @throws IOException if there are errors during conversion of vote to json format.
     */
    @Path("/tenant/{tenantId}/electionEvent/{electionEventId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getElectionEventAlias(@NotNull @PathParam(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                          @NotNull @PathParam(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId) throws IOException {
        try {
            ElectionEventTranslation eet = electionEventTranslationRepository.findByElectionEventId(tenantId, electionEventId);
            return Response.ok(eet).build();
        } catch (ResourceNotFoundException e) {
            String msg = "Election event alias not found for id " +  electionEventId;
			LOG.error(msg);
            LOG.trace(msg, e);
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}
