/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.domain.model.electionevent;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Provides operations on the election event translation repository.
 */
@Local
public interface ElectionEventTranslationRepository extends BaseRepository<ElectionEventTranslation, Integer> {

	/**
	 * Returns a election event translation data for a given tenant and alias.
	 * 
	 * @param tenantId - the tenant identifier.
	 * @param alias - the election event identifier.
	 * @return The data of election event translation.
	 * @throws ResourceNotFoundException if the data is not found.
	 */
			ElectionEventTranslation findByAlias(String tenantId, String alias) throws ResourceNotFoundException;

	/**
	 * Stores the input stream in a database.
	 * 
	 * @param ee the input containing data about election event to be stored.
	 */
			void saveVOrUpdateEET(ElectionEventTranslation ee);

	/**
	 * Returns a election event translation data for a given tenant and election event id.
	 *
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the internal election event identifier.
	 * @return The data of election event translation.
	 * @throws ResourceNotFoundException if the data is not found.
	 */
	ElectionEventTranslation findByElectionEventId(String tenantId, String electionEventId) throws ResourceNotFoundException;
}
