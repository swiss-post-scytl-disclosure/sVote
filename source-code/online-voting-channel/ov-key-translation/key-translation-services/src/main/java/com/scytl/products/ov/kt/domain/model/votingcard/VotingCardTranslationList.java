/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.domain.model.votingcard;

import java.util.List;

/**
 * List of voting card translation.
 */
public class VotingCardTranslationList {

	private List<VotingCardTranslation> votingCardTranslations;

	/**
	 * Returns the current value of the field votingCardTranslations.
	 *
	 * @return Returns the votingCardTranslations.
	 */
	public List<VotingCardTranslation> getVotingCardTranslations() {
		return votingCardTranslations;
	}

	/**
	 * Sets the value of the field votingCardTranslations.
	 *
	 * @param votingCardTranslations The votingCardTranslations to set.
	 */
	public void setVotingCardTranslations(List<VotingCardTranslation> votingCardTranslations) {
		this.votingCardTranslations = votingCardTranslations;
	}

}
