/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.domain.model.ballotbox;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * List of voting card translation.
 */
public class BallotBoxTranslationList {

	private List<BallotBoxTranslation> ballotBoxTranslations;

	/**
	 * Returns the current value of the field ballotBoxTranslations.
	 *
	 * @return Returns the ballotBoxTranslations.
	 */
	@JsonProperty(value = "ballotBoxTranslations")
	public List<BallotBoxTranslation> getBallotBoxTranslations() {
		return ballotBoxTranslations;
	}

	/**
	 * Sets the value of the field ballotBoxTranslations.
	 *
	 * @param ballotBoxTranslations The ballotBoxTranslations to set.
	 */
	public void setBallotBoxTranslations(List<BallotBoxTranslation> ballotBoxTranslations) {
		this.ballotBoxTranslations = ballotBoxTranslations;
	}

}
