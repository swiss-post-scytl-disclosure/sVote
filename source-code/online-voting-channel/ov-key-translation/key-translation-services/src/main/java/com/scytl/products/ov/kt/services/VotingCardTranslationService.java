/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslation;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslationRepository;

/**
 * Helper repository to manage transactional operations.
 */
@Stateless
public class VotingCardTranslationService {

	@Inject
	private VotingCardTranslationRepository votingCardTranslationRepository;

	/**
	 * Stores the list of voting card translation.
	 * 
	 * @param tenantId the tentant id.
	 * @param listVCS the list of voting card translation.
	 */
	public void save(String tenantId, List<VotingCardTranslation> listVCS) {
		if (listVCS != null) {
			// store in db
			for (VotingCardTranslation vc : listVCS) {
				vc.setTenantId(tenantId);
				votingCardTranslationRepository.saveVOrUpdateVCT(vc);
			}
		}
	}
}
