/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.domain.model.votingcard;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Provides operations on the voting card translation repository.
 */
@Local
public interface VotingCardTranslationRepository extends BaseRepository<VotingCardTranslation, Integer> {

	/**
	 * Returns a voting card translation data for a given tenant, election event and alias.
	 * 
	 * @param tenantId - the tenant identifier.
	 * @param alias - the voting card identifier.
	 * @return The data of voting card translation.
	 * @throws ResourceNotFoundException if the data is not found.
	 */
			VotingCardTranslation findByAlias(String tenantId, String electionEventId, String alias)
					throws ResourceNotFoundException;

	/**
	 * Stores the input in a database.
	 * 
	 * @param tenantId the tenant id.
	 * @param vc the input containing data about voting card to be stored.
	 */
			void saveVOrUpdateVCT(VotingCardTranslation vc);

}
