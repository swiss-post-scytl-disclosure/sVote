/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services.infrastructure.persistence;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.BallotBoxTranslationRepositoryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;
import com.scytl.products.ov.kt.domain.model.ballotbox.BallotBoxTranslation;

/**
 * Provides operations on the voting card translation repository.
 */
@Local
public interface BallotBoxTranslationRepository extends BaseRepository<BallotBoxTranslation, Integer> {

    /**
     * Returns a voting card translation data for a given tenant, election event and alias.
     *
     * @param tenantId - the tenant identifier.
     * @param alias - the voting card identifier.
     * @return The data of voting card translation.
     * @throws ResourceNotFoundException if the data is not found.
     */
    BallotBoxTranslation findByAlias(String tenantId, String electionEventId, String alias)
        throws ResourceNotFoundException;

    /**
     * Stores the input in a database.
     *
     * @param translation the input containing data about ballot box translation to be stored.
     * @throws BallotBoxTranslationRepositoryException 
     */
    void saveOrUpdate(BallotBoxTranslation translation) throws BallotBoxTranslationRepositoryException;

    /**
     * Return the translation for this ballotbox
     * @param tenantId - the tenant identifier
     * @param electionEventId - the election event identifier
     * @param ballotBoxId - the balot box identifier
     * @return the alias translation
     */
    BallotBoxTranslation findByBallotBoxId(String tenantId, String electionEventId, String ballotBoxId) throws ResourceNotFoundException;
}
