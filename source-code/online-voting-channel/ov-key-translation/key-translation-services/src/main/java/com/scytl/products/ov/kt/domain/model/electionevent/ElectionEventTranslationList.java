/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.domain.model.electionevent;

import java.util.List;

/**
 * List of {@link ElectionEventTranslation}.
 */
public class ElectionEventTranslationList {

	private List<ElectionEventTranslation> electionEventTranslations;

	/**
	 * Returns the current value of the field electionEventTranslations.
	 *
	 * @return Returns the electionEventTranslations.
	 */
	public List<ElectionEventTranslation> getElectionEventTranslations() {
		return electionEventTranslations;
	}

	/**
	 * Sets the value of the field electionEventTranslations.
	 *
	 * @param electionEventTranslations The electionEventTranslations to set.
	 */
	public void setElectionEventTranslations(List<ElectionEventTranslation> electionEventTranslations) {
		this.electionEventTranslations = electionEventTranslations;
	}
}
