/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Objects;
import java.util.Optional;

import com.scytl.products.ov.commons.beans.exceptions.BallotBoxTranslationRepositoryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.kt.domain.model.ballotbox.BallotBoxTranslation;
import com.scytl.products.ov.kt.domain.model.ballotbox.BallotBoxTranslationList;
import com.scytl.products.ov.kt.services.infrastructure.persistence.BallotBoxTranslationRepository;

/**
 * Service to process BallotBox translations.
 */
public class BallotBoxTranslationService {

    @Inject
    private BallotBoxTranslationRepository repository;

    private static final Logger LOG = LoggerFactory.getLogger(BallotBoxTranslationService.class);

    /**
     * Stores the list of voting card translation.
     *
     * @param tenantId        the tenant id.
     * @param translationList the list of ballot box translations.
     */
    public void saveOrUpdate(String tenantId, BallotBoxTranslationList translationList) {
        requireNonNull(translationList, "Translation list cannot be null.");
        requireNonNull(translationList.getBallotBoxTranslations(), "Translations cannot be null.");

        LOG.info("Received '{}' Ballot Box translations ", translationList.getBallotBoxTranslations().size());
        for (BallotBoxTranslation t : translationList.getBallotBoxTranslations()) {
            requireNonNull(t.getElectionEventId(), "ElectionEventId is null.");
            requireNonNull(t.getBallotBoxId(), "BallotboxId is null.");
            requireNonNull(t.getAlias(), "Alias is null.");
            t.setTenantId(tenantId);
            
            try {
				repository.saveOrUpdate(t);
				LOG.info("Saved/Updated Ballot Box translation [tenantId={}, electionEventId={}, "
						+ "ballotBoxId={}, alias={}]", t.getTenantId(), t.getElectionEventId(), t.getBallotBoxId(),
						t.getAlias());
			} catch (BallotBoxTranslationRepositoryException bbE) {
				LOG.error("Error trying to Save/Update Ballot Box translation.", bbE);
			}
        }
    }

    public Optional<BallotBoxTranslation> findByAlias(final String tenantId, final String electionEventId, final String alias) {
        requireNonNull(tenantId, "TenantId cannot be null.");
        requireNonNull(electionEventId, "ElectionEventId cannot be null.");
        requireNonNull(alias, "Alias cannot be null.");

        try {
            return Optional.of(repository.findByAlias(tenantId, electionEventId, alias));
        } catch (ResourceNotFoundException e) {
            LOG.error("Ballot box translation not found for alias [tenantId={}, electionEventId={}, alias={}]", tenantId, electionEventId, alias, e);
            return Optional.empty();
        }
    }

    public Optional<BallotBoxTranslation> findByBallotBoxId(final String tenantId, final String electionEventId, final String bbid) {
        requireNonNull(tenantId, "TenantId cannot be null.");
        requireNonNull(electionEventId, "ElectionEventId cannot be null.");
        requireNonNull(bbid, "BallotBoxId cannot be null.");

        try {
            return Optional.of(repository.findByBallotBoxId(tenantId, electionEventId, bbid));
        } catch (ResourceNotFoundException e) {
            LOG.error("Ballot box translation not found for id [tenantId={}, electionEventId={}, bbid={}]", tenantId, electionEventId, bbid, e);
            return Optional.empty();
        }
    }

    private void requireNonNull(Object object, String message) {
        try {
            Objects.requireNonNull(object, message);
        } catch (NullPointerException e) {
        	LOG.error(message, e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}
