/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.domain.model.electionevent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scytl.products.ov.commons.beans.errors.SemanticErrorGroup;
import com.scytl.products.ov.commons.beans.errors.SyntaxErrorGroup;
import com.scytl.products.ov.commons.domain.model.Constants;

/**
 * Entity that contains the information about the election event translation.
 */
@Entity
@Table(name = "ELECTION_EVENT_TRANSLATION")
public class ElectionEventTranslation {

	/**
	 * The identifier of this election event.
	 */
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "EETranslSeq")
	@SequenceGenerator(name = "EETranslSeq", sequenceName = "EE_TRANSL_SEQ")
	@JsonIgnore
	private Integer id;

	/** The provided identifier. */
	@Column(name = "ALIAS")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Size(max = Constants.COLUMN_LENGTH_100, groups = SemanticErrorGroup.class)
	private String alias;

	/**
	 * The identifier of the tenant.
	 */
	@Column(name = "TENANT_ID")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Size(max = Constants.COLUMN_LENGTH_100, groups = SemanticErrorGroup.class)
	private String tenantId;

	/**
	 * The identifier of the election event.
	 */
	@Column(name = "ELECTION_EVENT_ID")
	@NotNull(groups = SyntaxErrorGroup.class)
	@Size(max = Constants.COLUMN_LENGTH_100, groups = SemanticErrorGroup.class)
	private String electionEventId;

	/**
	 * Returns the current value of the field id.
	 *
	 * @return Returns the id.
	 */
	@JsonIgnore
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the value of the field id.
	 *
	 * @param id The id to set.
	 */
	@JsonIgnore
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Returns the current value of the field alias.
	 *
	 * @return Returns the alias.
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the value of the field alias.
	 *
	 * @param alias The alias to set.
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Returns the current value of the field tenantId.
	 *
	 * @return Returns the tenantId.
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Sets the value of the field tenantId.
	 *
	 * @param tenantId The tenantId to set.
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * Returns the current value of the field electionEventId.
	 *
	 * @return Returns the electionEventId.
	 */
	public String getElectionEventId() {
		return electionEventId;
	}

	/**
	 * Sets the value of the field electionEventId.
	 *
	 * @param electionEventId The electionEventId to set.
	 */
	public void setElectionEventId(String electionEventId) {
		this.electionEventId = electionEventId;
	}
}
