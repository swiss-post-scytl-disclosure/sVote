/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslation;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslationRepository;

/**
 * Helper service to manage transactional operations.
 */
@Stateless
public class ElectionEventTranslationService {

	@Inject
	private ElectionEventTranslationRepository electionEventTranslationRepository;

	/**
	 * Stores the list of voting card translation.
	 * 
	 * @param tenantId the tenant id.
	 * @param listEET the list of election event translation.
	 */
	public void save(String tenantId, List<ElectionEventTranslation> listEET) {
		if (listEET != null) {
			// store in db
			for (ElectionEventTranslation ee : listEET) {
				ee.setTenantId(tenantId);
				electionEventTranslationRepository.saveVOrUpdateEET(ee);
			}
		}
	}
}
