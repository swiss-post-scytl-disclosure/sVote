/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services;


import java.util.Arrays;

import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslation;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslationList;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslationRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

@RunWith(MockitoJUnitRunner.class)
public class VotingCardTranslationServiceTest {




    private static String tenantId = "100";

    private static String electionEventId = "100";

    private static String alias = "alias";



    @InjectMocks
    private VotingCardTranslationService votingCardTranslationService = new VotingCardTranslationService();

    @Mock
    private Logger logger;

    @Mock
    private VotingCardTranslationRepository votingCardTranslationRepository;


    @Rule
    public ExpectedException expectedException =
            ExpectedException.none();

    @Before
    public void beforeTest(){
        MockitoAnnotations.initMocks(BallotBoxTranslationServiceTest.class);
    }


    @Test
    public void save(){

        votingCardTranslationService.save(tenantId,getTranslations().getVotingCardTranslations());
    }

    private VotingCardTranslationList getTranslations() {
        VotingCardTranslationList votingCardTranslationList = new VotingCardTranslationList();
        VotingCardTranslation translation = new VotingCardTranslation();
        translation.setTenantId(tenantId);
        translation.setAlias(alias);
        translation.setElectionEventId(electionEventId);
        votingCardTranslationList.setVotingCardTranslations(Arrays.asList(translation));
        return votingCardTranslationList;
    }


}
