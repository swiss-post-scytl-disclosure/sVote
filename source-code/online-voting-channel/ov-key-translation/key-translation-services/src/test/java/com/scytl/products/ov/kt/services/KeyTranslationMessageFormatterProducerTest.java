/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services;

import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;

import com.scytl.products.ov.kt.infrastructure.log.KeyTranslationMessageFormatterProducer;

@RunWith(MockitoJUnitRunner.class)
public class KeyTranslationMessageFormatterProducerTest {

    @InjectMocks
    private KeyTranslationMessageFormatterProducer producer = new KeyTranslationMessageFormatterProducer();

    @Before
    public void init(){
        MockitoAnnotations.initMocks(KeyTranslationMessageFormatterProducerTest.class);
    }

    @Test
    public void createFormatter() {

        final MessageFormatter messageFormatter = producer.getMessageFormatter();
        assertNotNull(messageFormatter);


    }

}
