/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services.infrastructure.persistence.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslation;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslationRepository;

/**
 * Test class for the election event translation repository
 */
@RunWith(MockitoJUnitRunner.class)
public class ElectionEventTranslationRepositoryTest {

    @InjectMocks
    @Spy
    private static ElectionEventTranslationRepository electionEventTranslationRepository =
        new ElectionEventTranslationRepositoryImpl();

    @Mock
    private TypedQuery<ElectionEventTranslation> queryMock;

    @Mock
    private EntityManager entityManagerMock;

    @Mock
    private Logger logger;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String TENANT_ID = "2";

    private static final String ELECTION_EVENT_ID = "2";

    private static final String ALIAS = "alias";

    @Test
    public void findByAlias() throws ResourceNotFoundException {
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(ElectionEventTranslation.class)))
            .thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenReturn(new ElectionEventTranslation());

        assertNotNull(electionEventTranslationRepository.findByAlias(TENANT_ID, ALIAS));
    }

    @Test
    public void findByAliasNotFound() throws ResourceNotFoundException {
        expectedException.expect(ResourceNotFoundException.class);
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(ElectionEventTranslation.class)))
            .thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenThrow(NoResultException.class);

        assertNotNull(electionEventTranslationRepository.findByAlias(TENANT_ID, ALIAS));
    }

    @Test
    public void findById() throws ResourceNotFoundException {
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(ElectionEventTranslation.class)))
            .thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenReturn(new ElectionEventTranslation());

        assertNotNull(electionEventTranslationRepository.findByElectionEventId(TENANT_ID, ELECTION_EVENT_ID));
    }

    @Test
    public void findByIdNotFound() throws ResourceNotFoundException {
        expectedException.expect(ResourceNotFoundException.class);
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(ElectionEventTranslation.class)))
            .thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenThrow(NoResultException.class);

        assertNotNull(electionEventTranslationRepository.findByElectionEventId(TENANT_ID, ELECTION_EVENT_ID));
    }

    @Test
    public void saveExisting() throws DuplicateEntryException {
        ElectionEventTranslation electionEventTranslation = new ElectionEventTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(ElectionEventTranslation.class)))
            .thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);

        doThrow(DuplicateEntryException.class).when(electionEventTranslationRepository).save(any());
        electionEventTranslationRepository.saveVOrUpdateEET(electionEventTranslation);

    }

    @Test
    public void saveExistingEntryPersistenceException() throws DuplicateEntryException, EntryPersistenceException {
        ElectionEventTranslation electionEventTranslation = new ElectionEventTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(ElectionEventTranslation.class)))
                .thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);

        doThrow(DuplicateEntryException.class).when(electionEventTranslationRepository).save(any());
        doThrow(EntryPersistenceException.class).when(electionEventTranslationRepository).update(any());
        electionEventTranslationRepository.saveVOrUpdateEET(electionEventTranslation);

    }

    @Test
    public void saveNew() throws DuplicateEntryException {
        ElectionEventTranslation electionEventTranslation = new ElectionEventTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(ElectionEventTranslation.class)))
            .thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        electionEventTranslationRepository.saveVOrUpdateEET(electionEventTranslation);

    }

}
