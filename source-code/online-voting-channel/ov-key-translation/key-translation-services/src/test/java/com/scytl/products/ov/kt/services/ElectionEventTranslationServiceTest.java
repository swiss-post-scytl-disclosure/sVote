/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services;


import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslation;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslationList;
import com.scytl.products.ov.kt.domain.model.electionevent.ElectionEventTranslationRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class ElectionEventTranslationServiceTest {




    private static String tenantId = "100";

    private static String electionEventId = "100";

    private static String alias = "alias";



    @InjectMocks
    private ElectionEventTranslationService electionEventTranslationService = new ElectionEventTranslationService();

    @Mock
    private Logger logger;

    @Mock
    private ElectionEventTranslationRepository electionEventTranslationRepository;


    @Rule
    public ExpectedException expectedException =
            ExpectedException.none();

    @Before
    public void beforeTest(){
        MockitoAnnotations.initMocks(BallotBoxTranslationServiceTest.class);
    }


    @Test
    public void save(){

        electionEventTranslationService.save(tenantId,getTranslations().getElectionEventTranslations());
    }

    private ElectionEventTranslationList getTranslations() {
        ElectionEventTranslationList electionEventTranslationList = new ElectionEventTranslationList();
        ElectionEventTranslation translation = new ElectionEventTranslation();
        translation.setTenantId(tenantId);
        translation.setAlias(alias);
        translation.setElectionEventId(electionEventId);
        electionEventTranslationList.setElectionEventTranslations(Arrays.asList(translation));
        return electionEventTranslationList;
    }


}
