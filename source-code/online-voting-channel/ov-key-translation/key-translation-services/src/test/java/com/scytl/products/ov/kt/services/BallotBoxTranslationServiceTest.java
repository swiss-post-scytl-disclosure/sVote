/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.kt.domain.model.ballotbox.BallotBoxTranslation;
import com.scytl.products.ov.kt.domain.model.ballotbox.BallotBoxTranslationList;
import com.scytl.products.ov.kt.services.infrastructure.persistence.BallotBoxTranslationRepository;

@RunWith(MockitoJUnitRunner.class)
public class BallotBoxTranslationServiceTest {



    @InjectMocks
    private BallotBoxTranslationService ballotBoxTranslationService = new BallotBoxTranslationService();

    @Mock
    private Logger logger;

    @Mock
    private BallotBoxTranslationRepository ballotBoxTranslationRepository;


    @Rule
    public ExpectedException expectedException =
            ExpectedException.none();

    @Before
    public void beforeTest(){
        MockitoAnnotations.initMocks(BallotBoxTranslationServiceTest.class);;
    }



    @Test
    public void saveOrUpdateNullList(){
        expectedException.expect(IllegalArgumentException.class);
        ballotBoxTranslationService.saveOrUpdate(null, null);

    }



    @Test
    public void saveOrUpdateNullTranslations(){
        expectedException.expect(IllegalArgumentException.class);
        BallotBoxTranslationList ballotBoxTranslationList = new BallotBoxTranslationList();

        ballotBoxTranslationService.saveOrUpdate(null, ballotBoxTranslationList);

    }

    @Test
    public void saveOrUpdateTranslations(){

        BallotBoxTranslationList list = new BallotBoxTranslationList();
        BallotBoxTranslation ballotBoxTranslation = new BallotBoxTranslation();
        ballotBoxTranslation.setAlias("alias");
        ballotBoxTranslation.setBallotBoxId("ballotBoxId");
        ballotBoxTranslation.setElectionEventId("id");
        ballotBoxTranslation.setTenantId("tenantId");
        list.setBallotBoxTranslations(Arrays.asList(ballotBoxTranslation));
        ballotBoxTranslationService.saveOrUpdate(ballotBoxTranslation.getTenantId(), list);

    }

    @Test
    public void findByAliasNullTenant(){
        expectedException.expect(IllegalArgumentException.class);
        ballotBoxTranslationService.findByAlias(null, null, null);

    }

    @Test
    public void findByAliasNullEeId(){
        expectedException.expect(IllegalArgumentException.class);
        ballotBoxTranslationService.findByAlias("tenant", null, null);

    }

    @Test
    public void findByAliasNullAlias(){
        expectedException.expect(IllegalArgumentException.class);
        ballotBoxTranslationService.findByAlias("tenant", "eeId", null);

    }


    @Test
    public void findByAliasNotFound() throws ResourceNotFoundException{

        when(ballotBoxTranslationRepository.findByAlias(anyString(),anyString(),anyString())).thenThrow(ResourceNotFoundException.class);
        final Optional<BallotBoxTranslation> byAlias = ballotBoxTranslationService.findByAlias("tenant", "eeId", "alias");
        assertThat(byAlias.isPresent(), is(false));


    }

    @Test
    public void findByAlias() throws ResourceNotFoundException{


        when(ballotBoxTranslationRepository.findByAlias(anyString(),anyString(),anyString())).thenReturn(new BallotBoxTranslation());
        final Optional<BallotBoxTranslation> byAlias = ballotBoxTranslationService.findByAlias("tenant", "eeId", "alias");
        assertThat(byAlias.isPresent(), is(true));


    }

    @Test
    public void findByBBIdNullTenant(){
        expectedException.expect(IllegalArgumentException.class);
        ballotBoxTranslationService.findByBallotBoxId(null, null, null);

    }

    @Test
    public void findByBBIdNullEeId(){
        expectedException.expect(IllegalArgumentException.class);
        ballotBoxTranslationService.findByBallotBoxId("tenant", null, null);

    }

    @Test
    public void findByBBIdNullBBId(){
        expectedException.expect(IllegalArgumentException.class);
        ballotBoxTranslationService.findByBallotBoxId("tenant", "eeId", null);

    }


    @Test
    public void findByBBIDNotFound() throws ResourceNotFoundException{

        when(ballotBoxTranslationRepository.findByBallotBoxId(anyString(),anyString(),anyString())).thenThrow(ResourceNotFoundException.class);
        final Optional<BallotBoxTranslation> byAlias = ballotBoxTranslationService.findByBallotBoxId("tenant", "eeId", "id");
        assertThat(byAlias.isPresent(), is(false));


    }

    @Test
    public void findByBBID() throws ResourceNotFoundException{


        when(ballotBoxTranslationRepository.findByBallotBoxId(anyString(),anyString(),anyString())).thenReturn(new BallotBoxTranslation());
        final Optional<BallotBoxTranslation> byAlias = ballotBoxTranslationService.findByBallotBoxId("tenant", "eeId", "id");
        assertThat(byAlias.isPresent(), is(true));


    }




}
