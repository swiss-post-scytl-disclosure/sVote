/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services.infrastructure.persistence.impl;


import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslation;
import com.scytl.products.ov.kt.domain.model.votingcard.VotingCardTranslationRepository;
import org.slf4j.Logger;

/**
 * Test class for voting card translation repository
 */
@RunWith(MockitoJUnitRunner.class)
public class VotingCardTranslationRepositoryTest {

    @InjectMocks
    @Spy
    private static VotingCardTranslationRepository votingCardTranslationRepository = new VotingCardTranslationRepositoryImpl();

    @Mock
    private TypedQuery<VotingCardTranslation> queryMock;

    @Mock
    private Logger log;

    @Mock
    private EntityManager entityManagerMock;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String TENANT_ID = "2";
    private static final String ELECTION_EVENT_ID = "2";
    private static final String ALIAS = "alias";




    @Test
    public void findByAlias() throws ResourceNotFoundException {
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(VotingCardTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenReturn(new VotingCardTranslation());


        assertNotNull(votingCardTranslationRepository.findByAlias(TENANT_ID, ELECTION_EVENT_ID,ALIAS));
    }

    @Test
    public void findByAliasNotFound() throws ResourceNotFoundException {
        expectedException.expect(ResourceNotFoundException.class);
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(VotingCardTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
            when(queryMock.getSingleResult()).thenThrow(NoResultException.class);


        assertNotNull(votingCardTranslationRepository.findByAlias(TENANT_ID, ELECTION_EVENT_ID,ALIAS));
    }





    @Test
    public void saveExisting() throws ResourceNotFoundException, DuplicateEntryException {
        VotingCardTranslation votingCardTranslation = new VotingCardTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(VotingCardTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doReturn(votingCardTranslation).when(votingCardTranslationRepository).findByAlias(anyString(), anyString(), anyString());
        votingCardTranslationRepository.saveVOrUpdateVCT(votingCardTranslation);
    }

    @Test
    public void saveNew() throws ResourceNotFoundException{
        VotingCardTranslation votingCardTranslation = new VotingCardTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(VotingCardTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doThrow(ResourceNotFoundException.class).when(votingCardTranslationRepository).findByAlias(anyString(), anyString(), anyString());
        votingCardTranslationRepository.saveVOrUpdateVCT(votingCardTranslation);
    }

    @Test
    public void saveDuplicateException() throws  DuplicateEntryException{

        VotingCardTranslation votingCardTranslation = new VotingCardTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(VotingCardTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doThrow(DuplicateEntryException.class).when(votingCardTranslationRepository).save(any());
        votingCardTranslationRepository.saveVOrUpdateVCT(votingCardTranslation);
    }



    @Test
    public void saveEntryPersistenceException() throws  EntryPersistenceException, DuplicateEntryException {

        VotingCardTranslation votingCardTranslation = new VotingCardTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(VotingCardTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doThrow(EntryPersistenceException.class).when(votingCardTranslationRepository).update(any());
        doThrow(DuplicateEntryException.class).when(votingCardTranslationRepository).save(any());
        votingCardTranslationRepository.saveVOrUpdateVCT(votingCardTranslation);
    }






}
