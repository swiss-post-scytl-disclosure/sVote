/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.kt.services.infrastructure.persistence.impl;


import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.scytl.products.ov.kt.services.infrastructure.persistence.BallotBoxTranslationRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.commons.beans.exceptions.BallotBoxTranslationRepositoryException;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.kt.domain.model.ballotbox.BallotBoxTranslation;
import com.scytl.products.ov.kt.services.infrastructure.persistence.impl.BallotBoxTranslationRepositoryImpl;

/**
 * Test class for ballot box translation repository
 */
@RunWith(MockitoJUnitRunner.class)
public class BallotBoxTranslationRepositoryTest {

    @InjectMocks
    @Spy
    private static BallotBoxTranslationRepository ballotBoxTranslationRepository = new BallotBoxTranslationRepositoryImpl();

    @Mock
    private TypedQuery<BallotBoxTranslation> queryMock;


    @Mock
    private EntityManager entityManagerMock;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String TENANT_ID = "2";
    private static final String ELECTION_EVENT_ID = "2";
    private static final String ALIAS = "alias";
    private static final String BALLOT_BOX_ID = "2";



    @Test
    public void findByAlias() throws ResourceNotFoundException {
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenReturn(new BallotBoxTranslation());


        assertNotNull(ballotBoxTranslationRepository.findByAlias(TENANT_ID, ELECTION_EVENT_ID,ALIAS));
    }

    @Test
    public void findByAliasNotFound() throws ResourceNotFoundException {
        expectedException.expect(ResourceNotFoundException.class);
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
            when(queryMock.getSingleResult()).thenThrow(NoResultException.class);


        assertNotNull(ballotBoxTranslationRepository.findByAlias(TENANT_ID, ELECTION_EVENT_ID,ALIAS));
    }


    @Test
    public void findById() throws ResourceNotFoundException {
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenReturn(new BallotBoxTranslation());


        assertNotNull(ballotBoxTranslationRepository.findByBallotBoxId(TENANT_ID, ELECTION_EVENT_ID,BALLOT_BOX_ID));
    }

    @Test
    public void findByIdNotFound() throws ResourceNotFoundException {
        expectedException.expect(ResourceNotFoundException.class);
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        when(queryMock.getSingleResult()).thenThrow(NoResultException.class);


        assertNotNull(ballotBoxTranslationRepository.findByBallotBoxId(TENANT_ID, ELECTION_EVENT_ID,BALLOT_BOX_ID));
    }



    @Test
    public void saveExisting() throws ResourceNotFoundException, BallotBoxTranslationRepositoryException{
        BallotBoxTranslation ballotBoxTranslation = new BallotBoxTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doReturn(ballotBoxTranslation).when(ballotBoxTranslationRepository).findByBallotBoxId(anyString(), anyString(), anyString());
        ballotBoxTranslationRepository.saveOrUpdate(ballotBoxTranslation);
    }

    @Test
    public void saveNew() throws ResourceNotFoundException, BallotBoxTranslationRepositoryException{
        BallotBoxTranslation ballotBoxTranslation = new BallotBoxTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doThrow(ResourceNotFoundException.class).when(ballotBoxTranslationRepository).findByBallotBoxId(anyString(), anyString(), anyString());
        ballotBoxTranslationRepository.saveOrUpdate(ballotBoxTranslation);
    }

    @Test
	public void saveDuplicateException()
			throws ResourceNotFoundException, DuplicateEntryException, BallotBoxTranslationRepositoryException {
        expectedException.expect(Exception.class);
        BallotBoxTranslation ballotBoxTranslation = new BallotBoxTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doThrow(ResourceNotFoundException.class).when(ballotBoxTranslationRepository).findByBallotBoxId(anyString(), anyString(), anyString());
        doThrow(DuplicateEntryException.class).when(ballotBoxTranslationRepository).save(any());
        ballotBoxTranslationRepository.saveOrUpdate(ballotBoxTranslation);
    }



    @Test
	public void saveEntyPersistenceException()
			throws ResourceNotFoundException, EntryPersistenceException, BallotBoxTranslationRepositoryException {
        expectedException.expect(Exception.class);
        BallotBoxTranslation ballotBoxTranslation = new BallotBoxTranslation();
        when(entityManagerMock.createQuery(Matchers.anyString(), eq(BallotBoxTranslation.class))).thenReturn(queryMock);
        when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
        doReturn(ballotBoxTranslation).when(ballotBoxTranslationRepository).findByBallotBoxId(anyString(), anyString(), anyString());
        doThrow(EntryPersistenceException.class).when(ballotBoxTranslationRepository).update(any());
        ballotBoxTranslationRepository.saveOrUpdate(ballotBoxTranslation);
    }






}
