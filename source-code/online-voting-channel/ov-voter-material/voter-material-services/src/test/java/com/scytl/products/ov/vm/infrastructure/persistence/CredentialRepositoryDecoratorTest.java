/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.infrastructure.persistence;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vm.domain.model.credential.Credential;
import com.scytl.products.ov.vm.domain.model.credential.CredentialRepository;

@RunWith(MockitoJUnitRunner.class)
public class CredentialRepositoryDecoratorTest {
	
	private final String TENANT_ID = "1";
	private final String ELECTION_EVENT_ID = "2";
	private final String CREDENTIAL_ID = "4";
	
	@Mock
	private CredentialRepository credentialRepository;

	@Mock
	private TrackIdInstance trackIdInstance;

	@Mock
	private SecureLoggingWriter secureLoggerWriter;
	
	@InjectMocks
	CredentialRepositoryDecorator sut = new CredentialRepositoryDecorator() {
		
		@Override
		public Credential update(Credential entity) throws EntryPersistenceException { return null; }
		
		@Override
		public Credential find(Integer id) { return null; }
	};

	@Test
	public void testFindByTenantIdElectionEventIdCredentialIdSuccessful() throws ResourceNotFoundException {
		Credential credentialMock = new Credential();
		credentialMock.setCredentialId("credentialId");
		
		Mockito.when(credentialRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID)).thenReturn(credentialMock);
		Credential credential = sut.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID);
		
		assertEquals(credentialMock, credential);
	}
	
	@Test
	public void testHasWithTenantIdElectionEventIdCredentialIdSuccessful() throws ResourceNotFoundException {
		Mockito.when(credentialRepository.hasWithTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID)).thenReturn(true);
		Boolean result = sut.hasWithTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID);
		
		assertTrue(result);
	}
	
	@Test
	public void testSaveSuccessful() throws DuplicateEntryException
	{
		Credential credentialMock = new Credential();
		credentialMock.setCredentialId("credentialId");
		
		sut.save(credentialMock);
	}
	
	@Test(expected=DuplicateEntryException.class)
	public void testSaveDuplicateEntryException() throws DuplicateEntryException
	{
		Credential credentialMock = new Credential();
		credentialMock.setCredentialId("credentialId");
		
		Mockito.when(credentialRepository.save(credentialMock)).thenThrow(DuplicateEntryException.class);
		sut.save(credentialMock);
	}
	

}
