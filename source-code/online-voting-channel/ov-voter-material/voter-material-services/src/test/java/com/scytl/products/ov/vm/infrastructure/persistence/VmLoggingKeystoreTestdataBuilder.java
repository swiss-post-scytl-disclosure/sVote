/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.infrastructure.persistence;

import javax.persistence.EntityManager;

import com.scytl.products.ov.vm.domain.model.platform.VmLoggingKeystoreEntity;

import de.akquinet.jbosscc.needle.db.testdata.AbstractTestdataBuilder;

public class VmLoggingKeystoreTestdataBuilder extends AbstractTestdataBuilder<VmLoggingKeystoreEntity> {
	
	private VmLoggingKeystoreEntity vmLoggingKeystoreEntity;
	
	public VmLoggingKeystoreTestdataBuilder(EntityManager entityManager)
	{
		super(entityManager);
	}
	
	public void setVmLoggingKeystoreEntity(VmLoggingKeystoreEntity vmLoggingKeystoreEntity)
	{
		this.vmLoggingKeystoreEntity = vmLoggingKeystoreEntity;
	}

	@Override
	public VmLoggingKeystoreEntity build() {
		return vmLoggingKeystoreEntity;
	}
}
