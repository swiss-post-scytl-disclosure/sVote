/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.infrastructure.persistence;


import javax.persistence.EntityManager;

import com.scytl.products.ov.vm.domain.model.information.VoterInformation;

import de.akquinet.jbosscc.needle.db.testdata.AbstractTestdataBuilder;

public class VoterInformationTestdataBuilder extends AbstractTestdataBuilder<VoterInformation> {
	
	private VoterInformation voterInformation;
	
	public VoterInformationTestdataBuilder(EntityManager entityManager) {
		super(entityManager);
	}

	public void setVoterInformation(VoterInformation voterInformation)
	{
		this.voterInformation = voterInformation;
	}

	@Override
	public VoterInformation build() {
		return voterInformation;
	}

}
