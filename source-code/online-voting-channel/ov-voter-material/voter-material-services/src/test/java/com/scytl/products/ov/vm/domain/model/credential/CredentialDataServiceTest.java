/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.domain.model.credential;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

@RunWith(MockitoJUnitRunner.class)
public class CredentialDataServiceTest {
	
	private final String TENANT_ID = "1";
	private final String ELECTION_EVENT_ID = "2";
	private final String VOTING_CARD_ID = "3";
	
	@Mock
	private Logger LOG;

	@Mock
	private TrackIdInstance trackIdInstance;

	@Mock
	private CredentialRepository credentialRepository;

	@Mock
	private SecureLoggingWriter secureLoggerWriter;
	
	@InjectMocks
	CredentialDataService sut = new CredentialDataService();

	@Test
	public void testGetCredentialDataSuccessful() throws ResourceNotFoundException {
		Credential credentialMock = new Credential();
		credentialMock.setCredentialId("credentialId");
		
		Mockito.when(credentialRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(credentialMock);
		Credential credential = sut.getCredentialData(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID);
		
		assertEquals(credentialMock.getCredentialId(), credential.getCredentialId());
	}
	
	@Test(expected=ResourceNotFoundException.class)
	public void testGetCredentialDataResourceNotFound() throws ResourceNotFoundException {
		Mockito.when(credentialRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenThrow(ResourceNotFoundException.class);
		sut.getCredentialData(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID);
	}

}
