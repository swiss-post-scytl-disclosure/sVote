/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.infrastructure.persistence;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vm.domain.model.credential.Credential;
import com.scytl.products.ov.vm.domain.model.credential.CredentialRepository;
import com.scytl.products.ov.vm.infrastructure.log.VoterMaterialLogConstants;
import com.scytl.products.ov.vm.infrastructure.log.VoterMaterialLogEvents;

/**
 * A decorator with logger for voterIinformation repository.
 */
@Decorator
public abstract class CredentialRepositoryDecorator implements CredentialRepository {

    @Inject
    @Delegate
    private CredentialRepository credentialRepository;

    @Inject
    private TrackIdInstance trackIdInstance;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;


    @Override
    public Credential save(final Credential entity) throws DuplicateEntryException {
        try {
            final Credential credential = credentialRepository.save(entity);
            secureLoggerWriter.log(Level.INFO,
                new LogContent.LogContentBuilder().logEvent(VoterMaterialLogEvents.CREDENTIAL_DATA_SAVED)
                    .objectId(entity.getCredentialId()).user("admin")
                    .electionEvent(entity.getElectionEventId())
                    .additionalInfo(VoterMaterialLogConstants.TRACK_ID, trackIdInstance.getTrackId()).createLogInfo());
            return credential;
        } catch (DuplicateEntryException ex) {
            secureLoggerWriter.log(Level.ERROR,
                new LogContent.LogContentBuilder().logEvent(VoterMaterialLogEvents.ERROR_SAVING_CREDENTIAL_DATA)
                    .objectId(entity.getCredentialId()).user("admin")
                    .electionEvent(entity.getElectionEventId())
                    .additionalInfo(VoterMaterialLogConstants.TRACK_ID, trackIdInstance.getTrackId())
                    .additionalInfo(VoterMaterialLogConstants.ERR_DESC, ex.getMessage())
                    .createLogInfo());
            throw ex;
        }
    }

    @Override
    public Credential findByTenantIdElectionEventIdCredentialId(final String tenantId, final String electionEventId,
                                                                final String credentialId)
        throws ResourceNotFoundException {
        return credentialRepository.findByTenantIdElectionEventIdCredentialId(tenantId, electionEventId, credentialId);
    }

    @Override
    public boolean hasWithTenantIdElectionEventIdCredentialId(final String tenantId, final String electionEventId,
                                                              final String credentialId) {
        return credentialRepository.hasWithTenantIdElectionEventIdCredentialId(tenantId, electionEventId, credentialId);
    }
}
