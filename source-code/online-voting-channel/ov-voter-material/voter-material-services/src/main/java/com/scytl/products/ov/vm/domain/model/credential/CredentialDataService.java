/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.domain.model.credential;

import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vm.infrastructure.log.VoterMaterialLogConstants;
import com.scytl.products.ov.vm.infrastructure.log.VoterMaterialLogEvents;

/**
 * Service for handling credential data.
 */
@Stateless
public class CredentialDataService {

    @Inject
    private Logger LOG;

    @Inject
    private TrackIdInstance trackIdInstance;

    @Inject
    private CredentialRepository credentialRepository;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;

    /**
     * Gets the credential data base on the tenant, election event and
     * credential identifier.
     * 
     * @param tenantId
     *            The tenant identifier.
     * @param electionEventId
     *            The election event identifier.
     * @param credentialId
     *            The credential identifier.
     * @return The credential data found.
     * @throws ResourceNotFoundException
     *             if resource not found.
     */
    public Credential getCredentialData(String tenantId, String electionEventId, String credentialId)
            throws ResourceNotFoundException {

        try {

            LOG.info("Getting the voter credential for tenant: {} election event: {} and credential: {}.", tenantId,
                electionEventId, credentialId);

            Credential credential =
                credentialRepository.findByTenantIdElectionEventIdCredentialId(tenantId, electionEventId, credentialId);

            LOG.info("Credential data for tenant: {} election event: {} and voting card: {} found.", tenantId,
                electionEventId, credentialId);

            secureLoggerWriter.log(Level.INFO, new LogContent.LogContentBuilder()
                .logEvent(VoterMaterialLogEvents.CREDENTIAL_DATA_FOUND).objectId(credentialId).user(credentialId)
                .electionEvent(electionEventId)
                .additionalInfo(VoterMaterialLogConstants.TRACK_ID, trackIdInstance.getTrackId())
                .additionalInfo(VoterMaterialLogConstants.CREDENTIAL_ID, credentialId)
                .createLogInfo());

            return credential;
        } catch (ResourceNotFoundException e) {
            // credential data not found
            secureLoggerWriter.log(Level.ERROR, new LogContent.LogContentBuilder()
                .logEvent(VoterMaterialLogEvents.CREDENTIAL_DATA_NOT_FOUND).objectId(credentialId).user(credentialId)
                .electionEvent(electionEventId)
                .additionalInfo(VoterMaterialLogConstants.TRACK_ID, trackIdInstance.getTrackId())
                .additionalInfo(VoterMaterialLogConstants.CREDENTIAL_ID, credentialId)
                .additionalInfo(VoterMaterialLogConstants.ERR_DESC, "Resource not found in the CREDENTIAL_DATA table.")
                .createLogInfo());

            throw e;
        }
    }

}
