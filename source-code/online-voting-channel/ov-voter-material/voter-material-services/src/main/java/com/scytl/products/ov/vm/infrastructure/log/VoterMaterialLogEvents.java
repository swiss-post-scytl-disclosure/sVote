/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.infrastructure.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Enum with the log events for this context.
 */
public enum VoterMaterialLogEvents implements LogEvent {

	VOTER_INFORMATION_FOUND("CHREQGEN", "000", "Voter information found"),
	VOTER_INFORMATION_NOT_FOUND("CHREQGEN", "100", "Voter information not found"),
	CREDENTIAL_DATA_NOT_FOUND("CHREQGEN", "101", "Credential data not found"),
	CREDENTIAL_DATA_FOUND("CHREQGEN", "102", "Credential data found"),
    CREDENTIAL_DATA_SAVED("CDSTOR", "103", "Credential data saved successfully"),
	ERROR_SAVING_CREDENTIAL_DATA("CDSTOR", "104", "Failed to save credential data");

	private final String layer;

	private final String action;

	private final String outcome;

	private final String info;

	VoterMaterialLogEvents(final String action, final String outcome, final String info) {
		this.layer = "";
		this.action = action;
		this.outcome = outcome;
		this.info = info;
	}

	/**
	 * @see VoterMaterialLogEvents#getAction()
	 */
	@Override
	public String getAction() {
		return action;
	}

	/**
	 * @see VoterMaterialLogEvents#getOutcome()
	 */
	@Override
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @see VoterMaterialLogEvents#getInfo()
	 */
	@Override
	public String getInfo() {
		return info;
	}

	/**
	 * @see VoterMaterialLogEvents#getLayer()
	 */
	@Override
	public String getLayer() {
		return layer;
	}
}
