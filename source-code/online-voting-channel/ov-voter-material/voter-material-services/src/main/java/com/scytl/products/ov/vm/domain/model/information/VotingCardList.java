/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.domain.model.information;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class VotingCardList.
 */
public class VotingCardList {

    /** The voting cards. */
	private final List<VotingCard> votingCards = new ArrayList<>();

    /** The pagination. */
    private ListPagination pagination;

    /**
     * Gets the voting cards.
     *
     * @return Returns the votingCards.
     */
    public List<VotingCard> getVotingCards() {
        return votingCards;
    }

    /**
     * Gets the pagination.
     *
     * @return Returns the pagination.
     */
    public ListPagination getPagination() {
        return pagination;
    }

    /**
     * Sets the pagination.
     *
     * @param pagination
     *            The pagination to set.
     */
    public void setPagination(final ListPagination pagination) {
        this.pagination = pagination;
    }
}
