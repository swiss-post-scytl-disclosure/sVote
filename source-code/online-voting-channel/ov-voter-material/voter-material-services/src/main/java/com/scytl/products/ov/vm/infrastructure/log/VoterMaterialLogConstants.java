/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.infrastructure.log;

/**
 * Constants for logging in voter material.
 */
public class VoterMaterialLogConstants {
	
	/**
	 * Credential id.
	 */
	public static final String CREDENTIAL_ID = "#c_id";

	/**
	 * Error description - additional information.
	 */
	public static final String ERR_DESC = "#err_desc";

	/**
	 * Track id - additional information.
	 */
	public static final String TRACK_ID = "#request_id";

	/**
	 * Non-public constructor
	 */	
	private VoterMaterialLogConstants() {
	}

}
