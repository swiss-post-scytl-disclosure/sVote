/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vm.ui.ws.rs.application.operation;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.scytl.products.ov.commons.verify.CSVVerifier;

/**
 * Produces a {@link CSVVerifier} instance to be published in the CDI context.
 */
public class CSVVerifierProducer {
    /**
     * Returns a {@link CSVVerifier} instance.
     *
     * @return the instance.
     */
    @Produces
    @ApplicationScoped
    public CSVVerifier getCSVVerifier() {
        return new CSVVerifier();
    }
}
