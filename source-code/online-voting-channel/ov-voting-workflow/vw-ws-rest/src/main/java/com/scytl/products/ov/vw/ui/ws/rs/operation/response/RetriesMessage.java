/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.ui.ws.rs.operation.response;

public class RetriesMessage {
    private String retriesLeft;

    public String getRetriesLeft() {
        return retriesLeft;
    }

    public void setRetriesLeft(String retriesLeft) {
        this.retriesLeft = retriesLeft;
    }
    
}
