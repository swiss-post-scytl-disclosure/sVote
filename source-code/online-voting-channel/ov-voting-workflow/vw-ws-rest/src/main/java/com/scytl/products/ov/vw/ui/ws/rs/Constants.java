/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.ui.ws.rs;

/**
 * Class for constants.
 */
public final class Constants {

	/**
	 * The request id to be used for logging purposes.
	 */
	public static final String PARAMETER_HEADER_REQUEST_ID = "requestid";

	/**
	 * The length in characters of the random generated string.
	 */
	public static final int LENGTH_IN_CHARS = 16;

	private Constants() {
		// To avoid instantiation
	}
}
