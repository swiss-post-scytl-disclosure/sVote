/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.ui.ws.rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * This class define the application path for initializing the rest engine.
 */
@ApplicationPath("")
public class RestApplication extends Application {

	/**
	 * Constructor for initialization of rest engine and security provider.
	 */
	public RestApplication() {
		//This constructor is intentionally left blank
	}
}
