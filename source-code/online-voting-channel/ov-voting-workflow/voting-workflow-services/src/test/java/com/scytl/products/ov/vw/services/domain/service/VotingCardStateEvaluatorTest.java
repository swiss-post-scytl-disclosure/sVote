/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.vw.services.domain.model.state.VotingCardStateEvaluator;
import com.scytl.products.ov.vw.services.domain.model.state.VotingCardStates;

@RunWith(MockitoJUnitRunner.class)
public class VotingCardStateEvaluatorTest {

    private VotingCardStateEvaluator sut;

    @Test
    public void when_confirmation_required_and_state_cast_then_cannot_block() {

        // given
        VotingCardStates cast = VotingCardStates.CAST;
        boolean confirmationRequired = true;
        // when
        sut = VotingCardStateEvaluator.forState(cast);
        boolean canBeBlocked = sut.canBeBlocked(confirmationRequired);

        // then
        Assert.assertEquals(false, canBeBlocked);
    }

    @Test
    public void when_confirmation_required_and_state_sent_but_not_cast_then_can_block() {

        // given
        VotingCardStates cast = VotingCardStates.SENT_BUT_NOT_CAST;
        boolean confirmationRequired = true;
        // when
        sut = VotingCardStateEvaluator.forState(cast);
        boolean canBeBlocked = sut.canBeBlocked(confirmationRequired);

        // then
        Assert.assertEquals(true, canBeBlocked);
    }

    @Test
    public void when_no_confirmation_required_and_state_sent_but_not_cast_then_cannot_block() {

        // given
        VotingCardStates cast = VotingCardStates.SENT_BUT_NOT_CAST;
        boolean confirmationRequired = false;
        // when
        sut = VotingCardStateEvaluator.forState(cast);
        boolean canBeBlocked = sut.canBeBlocked(confirmationRequired);

        // then
        Assert.assertEquals(false, canBeBlocked);
    }

    @Test
    public void when_no_confirmation_required_and_state_not_sent_then_can_block() {

        // given
        VotingCardStates cast = VotingCardStates.NOT_SENT;
        boolean confirmationRequired = false;
        // when
        sut = VotingCardStateEvaluator.forState(cast);
        boolean canBeBlocked = sut.canBeBlocked(confirmationRequired);

        // then
        Assert.assertEquals(true, canBeBlocked);
    }

}
