/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.persistence;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import javax.persistence.LockModeType;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImplTest;
import com.scytl.products.ov.vw.services.domain.model.state.VotingCardState;
import com.scytl.products.ov.vw.services.domain.model.state.VotingCardStatePK;
import com.scytl.products.ov.vw.services.domain.model.state.VotingCardStateRepository;

/**
 * The Class VotingCardStateRepositoryImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class VotingCardStateRepositoryImplTest extends BaseRepositoryImplTest<VotingCardState, Long> {

	/** The voting card state repository. */
	@InjectMocks
	private static VotingCardStateRepository votingCardStateRepository = new VotingCardStateRepositoryImpl();

	/** The query mock. */
	@Mock
	private TypedQuery<VotingCardState> queryMock;

	/** The session mock. */
	@Mock
	private Session sessionMock;
	
	/** The logger mock. */
	@Mock
	private Logger logger;

	/**
	 * Creates a new object of the testing class.
	 *
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public VotingCardStateRepositoryImplTest() throws InstantiationException, IllegalAccessException {
		super(VotingCardState.class, votingCardStateRepository.getClass());
	}

	/**
	 * Test acquire by tenant id election event id voting card id.
	 *
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	@Test
	public void testFindByTenantIdElectionEventIdVotingCardId() throws ResourceNotFoundException {

		String tenantId = "1";
		String electionEventId = "1";
		String votingCardId = "1";
		VotingCardStatePK pk = new VotingCardStatePK(tenantId, electionEventId, votingCardId);
		when(entityManagerMock.find(VotingCardState.class, pk, LockModeType.PESSIMISTIC_WRITE))
			.thenReturn(new VotingCardState());

		assertTrue(votingCardStateRepository.acquire(tenantId, electionEventId, votingCardId).isPresent());
	}

	/**
	 * Test acquire by tenant id election event id voting card id not found.
	 *
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	@Test
	public void testFindByTenantIdElectionEventIdVotingCardIdNotFound() throws ResourceNotFoundException {

		String tenantId = "2";
		String electionEventId = "2";
		String votingCardId = "2";
		VotingCardStatePK pk = new VotingCardStatePK(tenantId, electionEventId, votingCardId);
		when(entityManagerMock.find(VotingCardState.class, pk, LockModeType.PESSIMISTIC_WRITE)).thenThrow(new PersistenceException());

		assertFalse(votingCardStateRepository.acquire(tenantId, electionEventId, votingCardId).isPresent());
	}

}
