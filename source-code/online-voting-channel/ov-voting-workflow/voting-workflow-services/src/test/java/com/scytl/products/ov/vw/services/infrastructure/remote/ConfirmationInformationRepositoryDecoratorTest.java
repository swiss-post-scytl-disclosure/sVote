/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vw.infrastructure.log.SecureLoggerInformation;
import com.scytl.products.ov.vw.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformationRepository;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformationResult;
import com.scytl.products.ov.vw.services.domain.model.information.VoterInformation;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;
import okhttp3.ResponseBody;

@RunWith(MockitoJUnitRunner.class)
public class ConfirmationInformationRepositoryDecoratorTest {
	
	private final String TENANT_ID = "100";
	private final String ELECTION_EVENT_ID = "1";
	private final String VOTING_CARD_ID = "1";
	private final String TOKEN = "token";
	
	@Mock
	private ConfirmationInformationRepository confirmationInformationRepository;

	@Mock
	private TrackIdInstance trackIdInstance;

	@Mock
	private SecureLoggingWriter secureLoggerWriter;

	@Mock
	private SecureLoggerInformation secureLoggerInformation;
	
	@InjectMocks
	ConfirmationInformationRepositoryDecorator rut = new ConfirmationInformationRepositoryDecorator();

	@Test
	public void testValidateConfirmationMessageIsValidSuccessful() throws ResourceNotFoundException {
		ConfirmationInformation confirmationInformationMock = new ConfirmationInformation();
		ConfirmationInformationResult confirmationInformationResultMock = new ConfirmationInformationResult();
		confirmationInformationResultMock.setValid(true);
		
		when(confirmationInformationRepository.validateConfirmationMessage(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformationMock, TOKEN)).thenReturn(confirmationInformationResultMock);
		
		ConfirmationInformationResult confirmationInformationResult = rut.validateConfirmationMessage(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformationMock, TOKEN);
		
		assertTrue(confirmationInformationResult.isValid());
	}
	
	@Test
	public void testValidateConfirmationMessageIsNotValidSuccessful() throws ResourceNotFoundException {
		ConfirmationInformation confirmationInformationMock = new ConfirmationInformation();
		ConfirmationInformationResult confirmationInformationResultMock = new ConfirmationInformationResult();
		confirmationInformationResultMock.setValid(false);
		
		when(secureLoggerInformation.getAuthenticationTokenObject()).thenReturn(new AuthenticationToken(new VoterInformation(), "authTokenIdBase64", "timestamp", "signature"));
		when(confirmationInformationRepository.validateConfirmationMessage(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformationMock, TOKEN)).thenReturn(confirmationInformationResultMock);
		
		ConfirmationInformationResult confirmationInformationResult = rut.validateConfirmationMessage(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformationMock, TOKEN);
		
		assertFalse(confirmationInformationResult.isValid());
	}
	
	@Test(expected=RetrofitException.class)
	public void testValidateConfirmationMessageRetrofitError() throws ResourceNotFoundException {
		ConfirmationInformation confirmationInformationMock = new ConfirmationInformation();
		ConfirmationInformationResult confirmationInformationResultMock = new ConfirmationInformationResult();
		confirmationInformationResultMock.setValid(false);
		
		when(secureLoggerInformation.getAuthenticationTokenObject()).thenReturn(new AuthenticationToken(new VoterInformation(), "authTokenIdBase64", "timestamp", "signature"));
		
		RetrofitException retrofitErrorMock = new RetrofitException(404, ResponseBody.create(okhttp3.MediaType.parse("text/html"), new byte[0]));
		
		when(confirmationInformationRepository.validateConfirmationMessage(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformationMock, TOKEN)).thenThrow(retrofitErrorMock);
		
		rut.validateConfirmationMessage(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, confirmationInformationMock, TOKEN);
	}
	
	
}
