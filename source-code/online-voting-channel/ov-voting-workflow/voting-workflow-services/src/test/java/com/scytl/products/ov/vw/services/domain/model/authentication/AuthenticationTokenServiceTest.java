/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.authentication;

import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

import java.io.IOException;
import java.security.Security;

import javax.json.JsonObject;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.logging.service.AuthTokenHashService;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.ui.ws.rs.ObjectMappers;
import com.scytl.products.ov.commons.validation.ValidationError;
import com.scytl.products.ov.commons.validation.ValidationResult;
import com.scytl.products.ov.vw.services.domain.model.ballot.BallotRepository;
import com.scytl.products.ov.vw.services.domain.model.ballot.BallotTextRepository;
import com.scytl.products.ov.vw.services.domain.model.ballotbox.BallotBoxInformationRepository;
import com.scytl.products.ov.vw.services.domain.model.information.VoterInformation;
import com.scytl.products.ov.vw.services.domain.model.state.VotingCardState;
import com.scytl.products.ov.vw.services.domain.model.state.VotingCardStateRepository;
import com.scytl.products.ov.vw.services.domain.model.state.VotingCardStates;
import com.scytl.products.ov.vw.services.domain.model.verification.Verification;
import com.scytl.products.ov.vw.services.domain.model.verification.VerificationRepository;
import com.scytl.products.ov.vw.services.domain.model.verificationset.VerificationSet;
import com.scytl.products.ov.vw.services.domain.model.verificationset.VerificationSetRepository;
import com.scytl.products.ov.vw.services.domain.service.VotingCardStateService;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenServiceTest {
	
	private final String VERIFICATION_CARD_SET_ID = "1";
	private final String TENANT_ID = "100";
	private final String ELECTION_EVENT_ID = "1";
	private final String CREDENTIAL_ID = "1";
	private final String VOTING_CARD_ID = "1";
	private final String BALLOT_ID = "1";
	private final String BALLOT_BOX_ID = "1";
	private final String VERIFICATION_CARD_ID = "1";
	
	// The EJB instance for retrieving authentication tokens
    @Mock
    private AuthenticationTokenRepository authenticationTokenRepository;

    // The EJB instance for retrieving ballot information
    @Mock
    private BallotRepository ballotRepository;

    // The EJB instance for retrieving ballot information
    @Mock
    private BallotBoxInformationRepository ballotBoxInformationRepository;

    // The EJB instance for retrieving ballot information
    @Mock
    private VerificationRepository verificationRepository;

    // The EJB instance for retrieving ballot text
    @Mock
    private BallotTextRepository ballotTextRepository;

    // The EJB instance for retrieving verification card sets
    @Mock
    private VerificationSetRepository verificationSetRepository;

    // The voting card state service
    @Mock
    private VotingCardStateService votingCardStateService;

    // The voting card state service
    @Mock
    private VotingCardStateRepository votingCardStateRepository;

    @Mock
    private TrackIdInstance trackId;

    @Mock
    private SecureLoggingWriter secureLoggerWriter;

    @Mock
    private AuthTokenHashService authTokenHashService;
	
	@InjectMocks
	AuthenticationTokenService sut = new AuthenticationTokenService();
	
	@BeforeClass
	public static void setup() 	{
		MockitoAnnotations.initMocks(AuthenticationTokenServiceTest.class);
	    Security.addProvider(new BouncyCastleProvider());
	}
	
	@Test
	public void testValidateAuthenticationToken() throws IOException, ResourceNotFoundException, ApplicationException
	{
		AuthenticationToken authenticationToken = new AuthenticationToken();
		authenticationToken.setId("1");
		
		ValidationResult validationResultMock = new ValidationResult();
		validationResultMock.setResult(true);
		
		when(authenticationTokenRepository.validateAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, ObjectMappers.toJson(authenticationToken))).thenReturn(validationResultMock);
		sut.validateAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, ObjectMappers.toJson(authenticationToken));
		
		assertEquals(true, validationResultMock.isResult());
	}

	@Test
	public void testGetAuthenticationTokenSuccessful() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn("{}");
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenReturn(verificationMock);
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationSetMock);
		
		JsonObject authToken = 
				sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
		Assert.assertNotNull(authToken);
	}
	
    @Test(expected=ResourceNotFoundException.class)
	public void testGetAuthenticationToken_AuthTokenNotFound() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenThrow(new ResourceNotFoundException("exception"));
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn("{}");
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenReturn(verificationMock);
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationSetMock);
		
		sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
	}
	
	@Test(expected=ApplicationException.class)
	public void testGetAuthenticationToken_VoteCardStateNotFound() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenThrow(new ApplicationException("exception"));
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn("{}");
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenReturn(verificationMock);
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationSetMock);
		
		sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
	}
	

	@Test(expected=ResourceNotFoundException.class)
	public void testGetAuthenticationToken_BallotNotFound() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenThrow(new ResourceNotFoundException("exception"));
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn("{}");
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenReturn(verificationMock);
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationSetMock);
		
		sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
	}
	

	@Test(expected=ResourceNotFoundException.class)
	public void testGetAuthenticationToken_BallotTextNotFound() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenThrow(new ResourceNotFoundException("exception"));
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn("{}");
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenReturn(verificationMock);
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationSetMock);
		
		sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
	}
	

	@Test(expected=ResourceNotFoundException.class)
	public void testGetAuthenticationToken_BallotBoxInfoNotFound() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenThrow(new ResourceNotFoundException("exception"));
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenReturn(verificationMock);
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationSetMock);
		
		sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
	}
	

	@Test(expected=ResourceNotFoundException.class)
	public void testGetAuthenticationToken_VerificationNotFound() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn("{}");
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenThrow(new ResourceNotFoundException("exception"));
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationSetMock);
		
		sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
	}
	

	@Test(expected=ResourceNotFoundException.class)
	public void testGetAuthenticationToken_VerificationSetNotFound() throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, GeneralCryptoLibException, EntryPersistenceException {
		ChallengeInformation challengeInformation = new ChallengeInformation();
		
		VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);
		
		AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));
		
		VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);
		
		Verification verificationMock = new Verification();
		verificationMock.setId("1");
		verificationMock.setSignedVerificationPublicKey("1");
		verificationMock.setVerificationCardKeystore("1");
		
		VerificationSet verificationSetMock = new VerificationSet();
		verificationSetMock.setId("1");
		
		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation)).thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn("{}");
		when(verificationRepository.findByTenantElectionEventVotingCard(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_ID)).thenReturn(verificationMock);
		when(verificationSetRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenThrow(new ResourceNotFoundException("exception"));
		
		sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
	}

}
