--------------------------------------------------------
--  File created - Tuesday-July-14-2015   
--------------------------------------------------------
-- DROP TABLE "VOTING_CARD_STATE";
-- DROP SEQUENCE "VOTING_CARD_STATE_SEQ";
--------------------------------------------------------
--  DDL for Sequence VOTING_CARD_STATE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "VOTING_CARD_STATE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table VOTING_CARD_STATE
--------------------------------------------------------

  CREATE TABLE "VOTING_CARD_STATE" 
   (	"ID" NUMBER(20,0), 
	"TENANT_ID" VARCHAR2(100 BYTE), 
	"ELECTION_EVENT_ID" VARCHAR2(100 BYTE), 
	"VOTING_CARD_ID" VARCHAR2(100 BYTE), 
	"STATE" VARCHAR2(100 CHAR), 
	"ATTEMPTS" NUMBER(20,0)
   ) ;
--------------------------------------------------------
--  DDL for Index SYS_C007064
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C007064" ON "VOTING_CARD_STATE" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index VOTING_CARD_STATE_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "VOTING_CARD_STATE_UK1" ON "VOTING_CARD_STATE" ("TENANT_ID", "ELECTION_EVENT_ID", "VOTING_CARD_ID") 
  ;
--------------------------------------------------------
--  Constraints for Table VOTING_CARD_STATE
--------------------------------------------------------

  ALTER TABLE "VOTING_CARD_STATE" ADD CONSTRAINT "VOTING_CARD_STATE_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID", "VOTING_CARD_ID") ENABLE;
  ALTER TABLE "VOTING_CARD_STATE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "VOTING_CARD_STATE" MODIFY ("ATTEMPTS" NOT NULL ENABLE);
  ALTER TABLE "VOTING_CARD_STATE" MODIFY ("STATE" NOT NULL ENABLE);
  ALTER TABLE "VOTING_CARD_STATE" MODIFY ("VOTING_CARD_ID" NOT NULL ENABLE);
  ALTER TABLE "VOTING_CARD_STATE" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "VOTING_CARD_STATE" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "VOTING_CARD_STATE" MODIFY ("ID" NOT NULL ENABLE);
