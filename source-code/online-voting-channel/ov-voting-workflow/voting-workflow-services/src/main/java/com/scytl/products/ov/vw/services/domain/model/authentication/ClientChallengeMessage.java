/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.authentication;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.scytl.products.ov.commons.beans.errors.SemanticErrorGroup;
import com.scytl.products.ov.commons.beans.errors.SyntaxErrorGroup;
import com.scytl.products.ov.commons.domain.model.Patterns;


/**
 * Class representing the client challenge used for authentication purposes.
 */
public class ClientChallengeMessage {

	// String representing the challenge value
	@NotNull(groups = SyntaxErrorGroup.class)
	@Pattern(regexp = Patterns.CHALLENGE, groups = SemanticErrorGroup.class)
	private String clientChallenge;

	// Array of bytes representing the signature of the server challenge
	@NotNull(groups = SyntaxErrorGroup.class)
	private String signature;

	/**
	 * Returns the current value of the field clientChallenge.
	 *
	 * @return Returns the clientChallenge.
	 */
	public String getClientChallenge() {
		return clientChallenge;
	}

	/**
	 * Sets the value of the field clientChallenge.
	 *
	 * @param clientChallenge The clientChallenge to set.
	 */
	public void setClientChallenge(String clientChallenge) {
		this.clientChallenge = clientChallenge;
	}

	/**
	 * Returns the current value of the field signature.
	 *
	 * @return Returns the signature.
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets the value of the field signature.
	 *
	 * @param signature The signature to set.
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

}
