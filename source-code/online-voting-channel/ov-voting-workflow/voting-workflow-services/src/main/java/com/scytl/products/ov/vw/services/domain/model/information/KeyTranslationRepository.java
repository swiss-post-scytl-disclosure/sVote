/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.information;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * The Interface ElectionEventTranslationRepository.
 */
public interface KeyTranslationRepository {

    /**
     * Gets the election event translation object by the election event identifier
     *
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event identifier
     * @return the by tenant id election event id voting card id
     * @throws ResourceNotFoundException
     *             the resource not found exception
     */
    ElectionEventTranslation getByElectionEventId(String tenantId, String electionEventId) throws ResourceNotFoundException;
}
