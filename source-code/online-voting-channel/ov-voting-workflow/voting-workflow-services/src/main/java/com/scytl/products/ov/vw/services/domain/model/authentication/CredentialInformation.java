/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.authentication;

/**
 * Class representing an authentication information.
 */
public class CredentialInformation {

	// the credential data
	private Credential credentialData;

	// the server challenge message
	private ServerChallengeMessage serverChallengeMessage;

	// the authentication token certificate
	private String certificates;
	
	private String certificatesSignature;

	/**
	 * Returns the current value of the field credentialData.
	 *
	 * @return Returns the credentialData.
	 */
	public Credential getCredentialData() {
		return credentialData;
	}

	/**
	 * Sets the value of the field credentialData.
	 *
	 * @param credentialData The credentialData to set.
	 */
	public void setCredentialData(Credential credentialData) {
		this.credentialData = credentialData;
	}

	/**
	 * Returns the current value of the field serverChallengeMessage.
	 *
	 * @return Returns the serverChallengeMessage.
	 */
	public ServerChallengeMessage getServerChallengeMessage() {
		return serverChallengeMessage;
	}

	/**
	 * Sets the value of the field serverChallengeMessage.
	 *
	 * @param serverChallengeMessage The serverChallengeMessage to set.
	 */
	public void setServerChallengeMessage(ServerChallengeMessage serverChallengeMessage) {
		this.serverChallengeMessage = serverChallengeMessage;
	}

	/**
	 * Returns the current value of the field certificates.
	 *
	 * @return Returns the certificates.
	 */
	public String getCertificates() {
		return certificates;
	}

	/**
	 * Sets the value of the field certificates.
	 *
	 * @param certificates The certificates to set.
	 */
	public void setCertificates(String certificates) {
		this.certificates = certificates;
	}

	/**
	 * Returns the current value of the field certificatesSignature.
	 *
	 * @return Returns the certificatesSignature.
	 */
	public String getCertificatesSignature() {
		return certificatesSignature;
	}

	/**
	 * Sets the value of the field certificatesSignature.
	 *
	 * @param certificatesSignature The certificatesSignature to set.
	 */
	public void setCertificatesSignature(String certificatesSignature) {
		this.certificatesSignature = certificatesSignature;
	}

}
