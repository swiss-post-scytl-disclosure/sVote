/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformationRepository;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformationResult;

/**
 * Implementation of the repository using a REST client
 */
@Stateless
public class ConfirmationInformationRepositoryImpl implements ConfirmationInformationRepository {

	// The properties file reader.
	private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

	// The path to the resource authentication information.
	private static final String CONFIRMATION_PATH = PROPERTIES.getPropertyValue("CONFIRMATION_PATH");

	// Instance of the track Id which will be written in the logs
	@Inject
	private TrackIdInstance trackId;

	private ElectionInformationClient electionInformationClient;

	@Inject
	ConfirmationInformationRepositoryImpl(final ElectionInformationClient electionInformationClient) {
		this.electionInformationClient = electionInformationClient;
	}

	/**
	 * Validates a confirmation message.
	 * 
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the electionEventIdentifier.
	 * @param votingCardId - the voting card identifier.
	 * @param confirmationInformation - confirmation information to be validated
	 * @param token - authentication token
	 * @return Confirmation Information Result, with the result of the validation
	 */
	@Override
	public ConfirmationInformationResult validateConfirmationMessage(String tenantId, String electionEventId,
			String votingCardId, ConfirmationInformation confirmationInformation, String token) throws ResourceNotFoundException{
		return RetrofitConsumer.processResponse(electionInformationClient.validateConfirmationMessage(trackId.getTrackId(), CONFIRMATION_PATH, tenantId,
			electionEventId, votingCardId, confirmationInformation, token));
	}

}
