/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by riglesias on 11/03/15.
 */
package com.scytl.products.ov.vw.services.domain.model.validation;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Entity holding the result of a validation.
 */
public class ValidationResult {

	/**
	 * The final result of the validation. If no validation failed, then the result is true. If there are rules that failed,
	 * result is false.
	 */
	@JsonProperty("result")
	private boolean result;

	/**
	 * Returns the current value of the field result.
	 *
	 * @return Returns the result.
	 */
	public boolean isResult() {
		return result;
	}

	/**
	 * Sets the value of the field result.
	 *
	 * @param result The result to set.
	 */
	public void setResult(boolean result) {
		this.result = result;
	}
}
