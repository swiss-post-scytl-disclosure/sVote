/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.service;

import com.scytl.products.ov.commons.beans.domain.model.receipt.Receipt;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.exceptions.VoteRepositoryException;
import com.scytl.products.ov.vw.services.domain.model.vote.ValidationVoteResult;

import java.io.IOException;

/**
 * Vote Service class
 */

public interface VoteService {

    /**
     * Recovers the receipt of a vote
     *
     * @param tenantId
     * @param electionEventId
     * @param votingCardId
     * @return
     * @throws VoteRepositoryException 
     */
    Receipt obtainReceipt(String tenantId, String electionEventId, String votingCardId)
            throws ResourceNotFoundException, ApplicationException, VoteRepositoryException;

    ValidationVoteResult validateVoteAndStore(String tenantId, String electionEventId, String votingCardId,
            String verificationCardId, Vote vote, String authenticationTokenJsonString)
            throws ApplicationException, ResourceNotFoundException, DuplicateEntryException, IOException;

}
