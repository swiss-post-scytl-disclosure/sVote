/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.infrastructure.caching;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import javax.enterprise.inject.Produces;
import java.util.concurrent.TimeUnit;

/**
 * Produces a Guava Cache
 * If we were using Spring we could use the Spring caching infrastructure and have a more integrated solution
 */
public class GoogleGuavaCachingProducer {

    @Produces
    Cache<String, Object> createGuavaCache() {
        //return a cache for 100 items that expire after 60secs of "inactivity"
        return CacheBuilder.newBuilder()
            .maximumSize(100)
            .expireAfterAccess(60, TimeUnit.SECONDS)
            .build();
    }

}
