/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.confirmation;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * Repository which validates the confirmation message.
 */
@Local
public interface ConfirmationInformationRepository {

	/**
	 * Validates a confirmation message.
	 * 
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the electionEventIdentifier.
	 * @param votingCardId - the voting card identifier.
	 * @param confirmationInformation - confirmation information to be validated *
	 * @param token - authentication token
	 * @return Confirmation Information Result, with the result of the validation
	 * @throws ResourceNotFoundException 
	 */
	ConfirmationInformationResult validateConfirmationMessage(String tenantId, String electionEventId, String votingCardId,
			ConfirmationInformation confirmationInformation, String token) throws ResourceNotFoundException;

}
