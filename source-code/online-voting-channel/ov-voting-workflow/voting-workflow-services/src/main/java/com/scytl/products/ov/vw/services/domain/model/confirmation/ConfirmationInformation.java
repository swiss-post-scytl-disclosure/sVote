/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.confirmation;

import com.scytl.products.ov.commons.beans.domain.model.confirmation.ConfirmationMessage;

/**
 * Class representing the confirmation information sent by the client.
 */
public class ConfirmationInformation {

    // The credential id
    private String credentialId;

    // The confirmation message
    private ConfirmationMessage confirmationMessage;

    // certificate sent to verify signatures
    private String certificate;

    /**
     * Sets new certificate.
     *
     * @param certificate
     *            New value of certificate.
     */
    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    /**
     * Sets new credentialId.
     *
     * @param credentialId
     *            New value of credentialId.
     */
    public void setCredentialId(String credentialId) {
        this.credentialId = credentialId;
    }

    /**
     * Gets certificate.
     *
     * @return Value of certificate.
     */
    public String getCertificate() {
        return certificate;
    }

    /**
     * Sets new confirmationMessage.
     *
     * @param confirmationMessage
     *            New value of confirmationMessage.
     */
    public void setConfirmationMessage(ConfirmationMessage confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    /**
     * Gets credentialId.
     *
     * @return Value of credentialId.
     */
    public String getCredentialId() {
        return credentialId;
    }

    /**
     * Gets confirmationMessage.
     *
     * @return Value of confirmationMessage.
     */
    public ConfirmationMessage getConfirmationMessage() {
        return confirmationMessage;
    }
}
