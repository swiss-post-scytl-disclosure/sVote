/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.verification;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

import java.io.IOException;

/**
 * Repository for handling Verification data.
 */
@Local
public interface VerificationRepository {

	/**
	 * Searches for the verification data related to given parameters.
	 * 
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param verificationCardId - the identifier of the verification card.
	 * @return The associated verification data, if found.
	 * @throws ResourceNotFoundException if the verification is not found.
	 */
	Verification findByTenantElectionEventVotingCard(String tenantId, String electionEventId, String verificationCardId)
			throws ResourceNotFoundException,IOException;
}
