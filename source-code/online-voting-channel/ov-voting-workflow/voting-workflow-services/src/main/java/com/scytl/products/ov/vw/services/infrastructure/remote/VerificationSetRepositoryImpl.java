/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.logging.I18nLoggerMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;
import com.scytl.products.ov.vw.services.domain.model.verificationset.VerificationSet;
import com.scytl.products.ov.vw.services.domain.model.verificationset.VerificationSetRepository;

/**
 * Implementation of the VerificationRepository using a REST client.
 */
@Stateless(name = "vw-VerificationSetRepositoryImpl")
public class VerificationSetRepositoryImpl implements VerificationSetRepository {

	/**
	 * The properties file reader.
	 */
	private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

	/**
	 * The path to the resource verification.
	 */
	private static final String VERIFICATION_SET_PATH = PROPERTIES.getPropertyValue("VERIFICATION_SET_PATH");

	// Instance of the logger
	private static final Logger LOG = LoggerFactory.getLogger("std");

	// Instance of the I18N logger messages
	private static final I18nLoggerMessages I18N = I18nLoggerMessages.getInstance();

	@Inject
	private TrackIdInstance trackId;

	private VerificationClient verificationClient;

	@Inject
	VerificationSetRepositoryImpl(final VerificationClient verificationClient) {
		this.verificationClient = verificationClient;
	}

	/**
	 * Searches the associated verification data for the given parameters using a Rest client.
	 *
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param verificationCardSetId - the identifier of the verification card set.
	 * @return a Verification object if found.
	 * @throws ResourceNotFoundException if the resource is not found.
	 * @throws IOException when converting to json fails.
	 */
	@Override
	public VerificationSet findByTenantElectionEventVerificationCardSetId(String tenantId, String electionEventId,
			String verificationCardSetId) throws ResourceNotFoundException, IOException {
		LOG.info(I18N.getMessage("VerificationSetRepositoryImpl.findByTenantElectionEventVotingCardSet"),
			tenantId, electionEventId, verificationCardSetId);

		try {
			VerificationSet verificationSet =
					RetrofitConsumer.processResponse(verificationClient.findVerificationSetByTenantElectionEventVerificationCardSetId(trackId.getTrackId(),
					VERIFICATION_SET_PATH, tenantId, electionEventId, verificationCardSetId));
			LOG.info(
				I18N.getMessage("VerificationSetRepositoryImpl.findByTenantElectionEventVotingCardSet.found"), tenantId,
				electionEventId, verificationCardSetId);
			return verificationSet;
		} catch (ResourceNotFoundException e) {
			LOG.error(
				I18N.getMessage("VerificationSetRepositoryImpl.findByTenantElectionEventVotingCardSet.notFound"), tenantId,
				electionEventId, verificationCardSetId);
			throw e;
		}

	}

}
