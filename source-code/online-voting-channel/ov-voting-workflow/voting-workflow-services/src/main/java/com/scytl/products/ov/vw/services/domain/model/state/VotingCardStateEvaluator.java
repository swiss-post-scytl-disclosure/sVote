/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.state;

public class VotingCardStateEvaluator {

    final VotingCardStates state;

    private VotingCardStateEvaluator(final VotingCardStates state) {
        this.state = state;
    }

    public static VotingCardStateEvaluator forState(final VotingCardStates currentState) {
        return new VotingCardStateEvaluator(currentState);
    }

    public boolean votingCardStateRequiresConsistencyCheck() {
        return VotingCardStates.NOT_SENT.equals(this.state) || VotingCardStates.SENT_BUT_NOT_CAST.equals(this.state)
            || VotingCardStates.CAST.equals(this.state);
    }

    public boolean voted() {
        return VotingCardStates.SENT_BUT_NOT_CAST.equals(this.state) || VotingCardStates.CAST.equals(this.state);
    }

    public boolean votingCardDidNotVote() {
        return VotingCardStates.NOT_SENT.equals(this.state);
    }

    public boolean votingCardIsBlocked() {
        return VotingCardStates.BLOCKED.equals(this.state);
    }

    public boolean canBeBlocked(boolean confirmationRequired) {
        if (confirmationRequired) {
            // final state is cast, so do not allow blocking
            return !VotingCardStates.CAST.equals(this.state);
        } else {
            // final state is sent_but_not_cast, so do not allow blocking
            return !(VotingCardStates.SENT_BUT_NOT_CAST.equals(state) || VotingCardStates.CAST.equals(state));
        }
    }
}
