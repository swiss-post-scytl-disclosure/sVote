/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by riglesias on 16/03/15.
 */
package com.scytl.products.ov.vw.services.domain.model.vote;

import com.scytl.products.ov.commons.validation.ValidationError;

/**
 * Class with the value of the receipt generated when a ballot is stored.
 */
public class ValidationVoteResult {

	// True if the vote is valid. Otherwise, false.
	private boolean valid;

	// Details of validation vote result.
	private ValidationError validationError;

	// Choice codes
	private String choiceCodes;

	/**
	 * Returns the current value of the field valid.
	 *
	 * @return Returns the valid.
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Sets the value of the field valid.
	 *
	 * @param valid The valid to set.
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * Returns the current value of the field choiceCodes.
	 *
	 * @return Returns the choiceCodes.
	 */
	public String getChoiceCodes() {
		return choiceCodes;
	}

	/**
	 * Sets the value of the field choiceCodes.
	 *
	 * @param choiceCodes The choiceCodes to set.
	 */
	public void setChoiceCodes(String choiceCodes) {
		this.choiceCodes = choiceCodes;
	}

	/**
	 * Returns the current value of the field validationError.
	 *
	 * @return Returns the validationError.
	 */
	public ValidationError getValidationError() {
		return validationError;
	}

	/**
	 * Sets the value of the field validationError.
	 *
	 * @param validationError The validationError to set.
	 */
	public void setValidationError(ValidationError validationError) {
		this.validationError = validationError;
	}

}
