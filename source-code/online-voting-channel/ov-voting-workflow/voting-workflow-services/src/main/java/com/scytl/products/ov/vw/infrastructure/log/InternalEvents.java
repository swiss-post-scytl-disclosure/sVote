/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.infrastructure.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * The Enum InternalEvents.
 */
public enum InternalEvents implements LogEvent {

    /** The voter information not found. */
    VOTER_INFORMATION_NOT_FOUND("VOTINFO", "200", "Voter information not found"),
    VOTER_INFORMATION_FOUND("VOTINFO", "201", "Voter information found")
    ;

    /** The layer. */
    private final String layer;

    /** The action. */
    private final String action;

    /** The outcome. */
    private final String outcome;

    /** The info. */
    private final String info;

    /**
     * Instantiates a new internal events.
     *
     * @param action
     *            the action
     * @param outcome
     *            the outcome
     * @param info
     *            the info
     */
    InternalEvents(final String action, final String outcome,
            final String info) {
        layer = "";
        this.action = action;
        this.outcome = outcome;
        this.info = info;
    }

    /**
     * Gets the action.
     *
     * @return the action
     * @see VotingWorkflowLogEvents#getAction()
     */
    @Override
    public String getAction() {
        return action;
    }

    /**
     * Gets the outcome.
     *
     * @return the outcome
     * @see VotingWorkflowLogEvents#getOutcome()
     */
    @Override
    public String getOutcome() {
        return outcome;
    }

    /**
     * Gets the info.
     *
     * @return the info
     * @see VotingWorkflowLogEvents#getInfo()
     */
    @Override
    public String getInfo() {
        return info;
    }

    /**
     * Gets the layer.
     *
     * @return the layer
     * @see VotingWorkflowLogEvents#getLayer()
     */
    @Override
    public String getLayer() {
        return layer;
    }
}
