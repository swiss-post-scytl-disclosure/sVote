/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.infrastructure.log;

import javax.ejb.Singleton;

import com.scytl.products.ov.commons.beans.domain.model.platform.LoggingInitializationState;

@Singleton
public class VwLoggingInitializationState extends LoggingInitializationState {

}
