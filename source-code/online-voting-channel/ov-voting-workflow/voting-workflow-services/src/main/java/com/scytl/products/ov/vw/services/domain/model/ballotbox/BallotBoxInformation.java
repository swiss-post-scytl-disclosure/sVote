/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.ballotbox;

/**
 * The Class BallotBoxInformation.
 */
public class BallotBoxInformation {

	/** The object containing the ballot box information. */
	private String json;

	/** A provided External Id which will identify the Ballot combined. */

	private String ballotBoxId;

	/**
	 * The identifier of a tenant for the current election event.
	 */
	private String tenantId;

	/**
	 * Returns the value of the field json.
	 *
	 * @return the json
	 */
	public String getJson() {
		return json;
	}

	/**
	 * Sets the value of the field JSON.
	 *
	 * @param json the new json
	 */
	public void setJson(String json) {
		this.json = json;
	}

	/**
	 * Returns the value of the field ballot box id.
	 *
	 * @return the ballot box id
	 */
	public String getBallotBoxId() {
		return ballotBoxId;
	}

	/**
	 * Sets the value of the field ballotBoxId.
	 *
	 * @param ballotBoxId the new ballot box id
	 */
	public void setBallotBoxId(String ballotBoxId) {
		this.ballotBoxId = ballotBoxId;
	}

	/**
	 * Returns the value of the field tenantId.
	 *
	 * @return the tenant id
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Sets the value of the field tenantId.
	 *
	 * @param tenantId the new tenant id
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
