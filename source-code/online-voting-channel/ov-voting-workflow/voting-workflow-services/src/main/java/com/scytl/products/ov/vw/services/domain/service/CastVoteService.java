/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.service;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.vw.services.domain.model.confirmation.VoteCastResult;
import com.scytl.products.ov.vw.services.domain.model.information.VoterInformation;

/**
 * Service responsible for casting the votes.
 */
public interface CastVoteService {
    /**
     * Cast the vote of the specified voter using provided confirmation. It is
     * assumed that the vote has already been sent and is stored in the
     * database.
     *
     * @param authenticationToken
     *            the authentication token
     * @param voter
     *            the voter
     * @param confirmation
     *            the confirmation
     * @return the result
     * @throws ResourceNotFoundException 
     */
    VoteCastResult castVote(String authenticationToken,
            VoterInformation voter, ConfirmationInformation confirmation) throws ResourceNotFoundException;
}
