/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.verificationset;

import java.io.IOException;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * Repository for handling VerificationSet data.
 */
@Local
public interface VerificationSetRepository {

	/**
	 * Searches for the verification set data related to given parameters.
	 *
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param verificationCardSetId - the identifier of the verification card set.
	 * @return The associated verification data, if found.
	 * @throws ResourceNotFoundException if the verification is not found.
	 * @throws IOException when converting to json fails.
	 */
	VerificationSet findByTenantElectionEventVerificationCardSetId(String tenantId, String electionEventId,
			String verificationCardSetId) throws ResourceNotFoundException, IOException;
}
