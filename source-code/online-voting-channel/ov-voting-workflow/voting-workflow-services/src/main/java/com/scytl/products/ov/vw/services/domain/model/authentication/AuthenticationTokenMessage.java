/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.authentication;

import com.scytl.products.ov.commons.validation.ValidationError;

/**
 * Class representing an authentication token and a validation error.
 */
public class AuthenticationTokenMessage {

	private AuthenticationToken authenticationToken;

	private ValidationError validationError;

	/**
	 * Returns the current value of the field authenticationToken.
	 *
	 * @return Returns the authenticationToken.
	 */
	public AuthenticationToken getAuthenticationToken() {
		return authenticationToken;
	}

	/**
	 * Sets the value of the field authenticationToken.
	 *
	 * @param authenticationToken The authenticationToken to set.
	 */
	public void setAuthenticationToken(AuthenticationToken authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	/**
	 * Returns the current value of the field validationError.
	 *
	 * @return Returns the validationError.
	 */
	public ValidationError getValidationError() {
		return validationError;
	}

	/**
	 * Sets the value of the field validationError.
	 *
	 * @param validationError The validationError to set.
	 */
	public void setValidationError(ValidationError validationError) {
		this.validationError = validationError;
	}

}
