/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.confirmation;

import com.scytl.products.ov.commons.beans.domain.model.receipt.Receipt;
import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.vw.services.domain.model.authentication.AuthenticationToken;

/**
 * The class representing the vote message.
 */
public class VoteMessage {

    // The encrypted vote
    private Vote encryptedVote;

    // The receipt of the stored vote
    private Receipt receiptMessage;

    // The authentication token
    private AuthenticationToken authenticationToken;

    /**
     * Gets receiptMessage.
     *
     * @return Value of receiptMessage.
     */
    public Receipt getReceiptMessage() {
        return receiptMessage;
    }

    /**
     * Gets encryptedVote.
     *
     * @return Value of encryptedVote.
     */
    public Vote getEncryptedVote() {
        return encryptedVote;
    }

    /**
     * Gets authenticationToken.
     *
     * @return Value of authenticationToken.
     */
    public AuthenticationToken getAuthenticationToken() {
        return authenticationToken;
    }

    /**
     * Sets new receiptMessage.
     *
     * @param receiptMessage
     *            New value of receiptMessage.
     */
    public void setReceiptMessage(Receipt receiptMessage) {
        this.receiptMessage = receiptMessage;
    }

    /**
     * Sets new authenticationToken.
     *
     * @param authenticationToken
     *            New value of authenticationToken.
     */
    public void setAuthenticationToken(AuthenticationToken authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    /**
     * Sets new encryptedVote.
     *
     * @param encryptedVote
     *            New value of encryptedVote.
     */
    public void setEncryptedVote(Vote encryptedVote) {
        this.encryptedVote = encryptedVote;
    }

}
