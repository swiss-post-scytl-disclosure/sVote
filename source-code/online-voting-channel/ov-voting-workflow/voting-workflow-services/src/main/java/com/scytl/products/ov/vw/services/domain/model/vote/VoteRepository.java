/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.vote;

import java.io.IOException;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.domain.model.receipt.Receipt;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.exceptions.VoteRepositoryException;
import com.scytl.products.ov.commons.dto.VoteAndComputeResults;

/**
 * Provides operations on the vote repository.
 */
@Local
public interface VoteRepository {

    /**
     * Saves a vote in the repository.
     *
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id
     * @param vote
     *            - the vote information with computation proofs and signatures.
     * @param authenticationTokenJsonString
     *            - the authentication token in json format.
     * @return Returns a receipt generated in the storage operation.
     * @throws IOException
     *             if there are some problem during mapping to a json format.
     * @throws ApplicationException
     *             if there are validation problems in any of the input
     *             parameters.
     * @throws ResourceNotFoundException
     */
    Receipt save(String tenantId, String electionEventId, VoteAndComputeResults vote,
            String authenticationTokenJsonString) throws ApplicationException, ResourceNotFoundException;

    /**
     * Returns the vote as json string identified by tenant, election event,
     * voting card.
     * 
     * @param tenantId
     *            - the tenant id.
     * @param electionEventId
     *            - the election event id.
     * @param votingCardId
     *            - the voting card id.
     * @return the vote.
     * @throws ResourceNotFoundException
     *             if the vote is not found.
     * @throws VoteRepositoryException
     */
    VoteAndComputeResults findByTenantIdElectionEventIdVotingCardId(String tenantId, String electionEventId,
            String votingCardId) throws ResourceNotFoundException, VoteRepositoryException;

    /**
     * Request the receipt for a given voting card Id
     * 
     * @param tenantId
     * @param electionEventId
     * @param votingCardId
     * @return the receipt
     * @throws VoteRepositoryException
     */
    com.scytl.products.ov.commons.beans.domain.model.receipt.Receipt getReceiptByVotingCardId(String tenantId,
            String electionEventId, String votingCardId)
            throws ResourceNotFoundException, ApplicationException, VoteRepositoryException;

    /**
     * Checks if a vote exists for the specified tenant, electionEvent and
     * votingCard
     ** 
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id
     * @param votingCardId
     *            - the voting card id.
     * @return vote exists (true) or not (false).
     * @throws VoteRepositoryException
     */
    boolean voteExists(String tenantId, String electionEventId, String votingCardId) throws VoteRepositoryException;
}
