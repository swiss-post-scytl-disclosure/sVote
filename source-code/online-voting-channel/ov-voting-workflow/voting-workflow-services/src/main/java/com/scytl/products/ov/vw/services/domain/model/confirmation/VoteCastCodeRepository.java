/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.confirmation;

import com.scytl.products.ov.commons.beans.domain.model.confirmation.ConfirmationMessage;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.exceptions.VoteCastCodeRepositoryException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;
import com.scytl.products.ov.commons.dto.CastCodeAndComputeResults;

/**
 * The Interface VoteCastCodeRepository.
 */
public interface VoteCastCodeRepository {

    /**
     * Generate cast code.
     *
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param verificationCardId
     *            the verification card id
     * @param authenticationTokenSignature
     * @param votingCardId
     * @param confirmationMessage
     *            the confirmation message
     * @return the vote cast message
     * @throws ResourceNotFoundException
     *             if the cast codes can not be generated.
     */
    CastCodeAndComputeResults generateCastCode(String tenantId, String electionEventId, String verificationCardId,
            String votingCardId, String authenticationTokenSignature, ConfirmationMessage confirmationMessage)
            throws CryptographicOperationException;

    /**
     * Stores cast code.
     *
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param votingCardId
     *            the voting card id
     * @param voteCastMessage
     *            the vote cast message
     * @return true if the cast code is successfully stored. Otherwise, false.
     * @throws ResourceNotFoundException
     *             if the cast codes can not be stored.
     */
    boolean storesCastCode(String tenantId, String electionEventId, String votingCardId,
            CastCodeAndComputeResults voteCastMessage) throws ResourceNotFoundException;

    /**
     * Returns the vote cast codes taking into account a tenant, election event
     * and voting card.
     * 
     * @param tenantId
     *            - the identifier of the tenant.
     * @param electionEventId
     *            - the identifier of the election event.
     * @param votingCardId
     *            - the identifier of the voting card.
     * @return The vote cast codes generated.
     * @throws ResourceNotFoundException
     *             if the cast codes can not be recovered.
     */
    CastCodeAndComputeResults getCastCode(String tenantId, String electionEventId, String votingCardId)
            throws ResourceNotFoundException;

    boolean voteCastCodeExists(String tenantId, String electionEventId, String votingCardId)
            throws VoteCastCodeRepositoryException;
}
