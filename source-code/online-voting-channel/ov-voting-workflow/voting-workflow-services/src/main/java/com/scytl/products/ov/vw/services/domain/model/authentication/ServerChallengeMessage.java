/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.authentication;

/**
 * Class containing information of server challenge including challenge, timestamp and signature.
 */
public class ServerChallengeMessage {

	// String representing the challenge value
	private String serverChallenge;

	// String representing the current timestamp
	private String timestamp;

	// Array of bytes representing the signature of the server challenge
	private String signature;

	/**
	 * Returns the current value of the field serverChallenge.
	 *
	 * @return Returns the serverChallenge.
	 */
	public String getServerChallenge() {
		return serverChallenge;
	}

	/**
	 * Sets the value of the field serverChallenge.
	 *
	 * @param serverChallenge The serverChallenge to set.
	 */
	public void setServerChallenge(String serverChallenge) {
		this.serverChallenge = serverChallenge;
	}

	/**
	 * Returns the current value of the field timestamp.
	 *
	 * @return Returns the timestamp.
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the value of the field timestamp.
	 *
	 * @param timestamp The timestamp to set.
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Returns the current value of the field signature.
	 *
	 * @return Returns the signature.
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets the value of the field signature.
	 *
	 * @param signature The signature to set.
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}
}
