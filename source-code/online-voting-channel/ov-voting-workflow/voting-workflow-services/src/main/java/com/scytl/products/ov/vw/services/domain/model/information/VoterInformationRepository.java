/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.information;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * The Interface VoterInformationRepository.
 */
public interface VoterInformationRepository {

    /**
     * Gets the by tenant id election event id voting card id.
     *
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param votingCardId
     *            the voting card id
     * @return the by tenant id election event id voting card id
     * @throws ResourceNotFoundException
     *             the resource not found exception
     */
    VoterInformation getByTenantIdElectionEventIdVotingCardId(
            String tenantId, String electionEventId, String votingCardId)
                    throws ResourceNotFoundException;
}
