/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote.admin;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.vw.services.domain.model.information.VoterInformation;

/**
 * The Interface VoterMaterialAdminClient.
 */
public interface VoterMaterialAdminClient {

    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID =
        "electionEventId";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    public static final String PARAMETER_VALUE_VOTING_CARD_ID =
        "votingCardId";

    /** The Constant PARAMETER_PATH_VOTERINFORMATION. */
    public static final String PARAMETER_PATH_VOTERINFORMATION =
        "pathVoterinformation";

    /**
     * Gets the voter informations.
     *
     * @param pathVoterinformation
     *            the path voterinformation
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param votingCardId
     *            the voting card id
     * @return the voter informations
     */
    @GET("{pathVoterinformation}/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}")
    Call<VoterInformation> getVoterInformations(
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Path(PARAMETER_PATH_VOTERINFORMATION) String pathVoterinformation,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VOTING_CARD_ID) String votingCardId)
                    throws ResourceNotFoundException;
}
