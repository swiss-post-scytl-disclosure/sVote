/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.infrastructure.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Enum with the log events for this context.
 */
public enum VotingWorkflowLogEvents implements LogEvent {

	CREDENTIAL_DATA_NOT_FOUND( "CHREQGEN", "200", "Credential Data not found"),
	
	AUTH_TOKEN_NOT_GENERATED("ATREQGEN", "201", "Authentication token not generated"),
	
	ID_AUTH_TOKEN_NOT_CONSISTENT("ATREQGEN", "202", "ID in the Auth Token not consistent with that provided"),
	
	BALLOT_RESOURCE_NOT_FOUND("ATREQGEN", "203", "Resource (ballot) not found"),
	BALLOT_TEXT_RESOURCE_NOT_FOUND("ATREQGEN", "204", "Resource (ballot text) not found"),
	VERIFICATION_CARD_RESOURCE_NOT_FOUND("ATREQGEN", "205", "Resource (verification card) not found"),
	VERIFICATION_CARD_SET_RESOURCE_NOT_FOUND("ATREQGEN", "206", "Resource (verification card set) not found"),
	BALLOT_BOX_RESOURCE_NOT_FOUND("ATREQGEN", "207", "Resource (ballot box) not found"),
	
	CHOICE_CODES_VOTE_NOT_FOUND("CCRET","208", "Vote not found"),
	CHOICE_CODES_VOTE_FOUND("CCRET","000", "Vote found"),
	CHOICE_CODES_ELECTION_OVER("CCRET","209","Retrieval of Choice Codes after election is closed"),
	CHOICE_CODES_RETRIEVED("CCRET","000","Choice Codes correctly retrieved"),
	CHOICE_CODES_ERROR_RETRIEVING("CCRET","211","Choice Codes can not be retrieved"),
	
	DB_CAST_CODE_NOT_FOUND("VCCRETDB","213","Vote Cast Code not found"),
	DB_CAST_CODE_FOUND("VCCRETDB","000","Vote Cast Code found"),
	DB_CAST_CODE_ELECTION_OVER("VCCRETDB","210","Retrieval of Cast Code after election is closed"),
	DB_CAST_CODE_ELECTION_NOT_STARTED("VCCRETDB","231","Retrieval of Cast Code before election started"),
	
	AUTH_TOKEN_VALIDATION_OK("ATVAL","000","Successful Authentication Token validation"),
	AUTH_TOKEN_VALIDATION_ERROR("ATVAL","214","Authentication token Not valid"),
	
	VOTE_VALIDATION_OK("VOTVA", "000", "Successful vote validation"),
	VOTE_VALIDATION_ERROR_EI("VOTVA", "215", "Error validating the vote at the Election Information Context"),
	VOTE_VALIDATION_ERROR_VV("VOTVA", "216", "Error validating the vote at the Vote Verification Context"),
	VOTE_VALIDATION_INVALID_VOTING_CARD_STATE("VOTVA", "219", "The VotingCardID does not have the expected status"),
	
	VOTE_FOUND("VOTRET", "240", "Vote found"),
	VOTE_NOT_FOUND("VOTRET", "241", "Vote not found"),

    VOTE_STORING_OK("VRSTOR", "000", "Vote and Receipt correctly stored"),
    VOTE_STORING_ERROR("VRSTOR", "217", "Error during the storage of the vote and receipt"),

	VOTING_CARD_STATUS_UPDATING_ERROR("VRSTOR", "218", "Error updating the status of the VotingCardID"),
	VOTING_CARD_STATUS_UPDATING_OK("VRSTOR", "000", "Updating the status of the VotingCardID"), 
	VOTING_CARD_STATUS_FOUND("VRSTOR", "000", "Status of the VotingCardID found"), 
	VOTING_CARD_STATUS_NOT_FOUND("VRSTOR", "000", "Status of the VotingCardID not found"),
	VOTING_CARD_BLOCKING_OK("VCBLK", "301", "VotingCard blocked successfully"),
	VOTING_CARD_BLOCKING_ERROR("VCBLK", "302", "Error blocking VotingCard"),
	
	CONFIRMATION_MESSAGE_VALIDATION_INVALID_VOTING_CARD_STATE("CMVAL", "220", "The VotingCardID does not have the expected status"),
	CONFIRMATION_MESSAGE_VALIDATION_OK("CMVAL", "000", "Successful Confirmation Message Validation"),
	CONFIRMATION_MESSAGE_VALIDATION_STORING_ERROR("CMVAL", "221", "Error while storing the Confirmation Message"),
	CONFIRMATION_MESSAGE_INVALID("CMVAL", "222", "Confirmation Message not valid"),
	CONFIRMATION_MESSAGE_VALIDATION_INCREASE_ATTEMPTS("CMVAL", "228", "The number of confirmation attempts has been increased"),
	CONFIRMATION_MESSAGE_VALIDATION_MAX_ATTEMPTS("CMVAL", "229", "Maximum confirmation attempts reached"),
	CONFIRMATION_MESSAGE_VALIDATION_UPDATING_STATUS_ERROR("CMVAL", "223", "Error updating the status of the VotingCardID"),
	
	MAPPING_CAST_CODE_FOUND("VCCRETM", "000", "Vote Cast Code found"),
	MAPPING_CAST_CODE_INVALID("VCCRETM", "224", "Vote Cast Code invalid"),
	MAPPING_CAST_CODE_NOT_FOUND("VCCRETM", "225", "Vote Cast Code not found"),
	
	CAST_CODE_STORED("VCCSTOR", "000", "Vote Cast Code stored"),
	CAST_CODE_ERROR_UPDATING_VOTING_CARD_STATE("VCCSTOR", "226", "Error updating the status of the VotingCardID"),
	CAST_CODE_ERROR_STORING("VCCSTOR", "227", "Error while storing the Vote Cast Code"),
	
	SENT_BALLOT("ATREQGEN", "000", "Sent ballot to the voter"),
	SENT_CHOICE_CODES("ATREQGEN", "000", "Sent Choice Codes to the voter"),
	SENT_VOTE_CAST_CODE("ATREQGEN", "000", "Sent Vote Cast Code to the voter"),

	STOP_VOTING_PROCESS("VWERROR", "230", "Voter can not continue with the voting process"),

    CREDENTIAL_DATA_FOUND( "CHREQGEN", "231", "Credential Data found"),
    AUTH_TOKEN_GENERATED("ATREQGEN", "232", "Authentication token generated"),
    BALLOT_TEXT_RESOURCE_FOUND("ATREQGEN", "233", "Resource (ballot text) found"),
    BALLOT_RESOURCE_FOUND("ATREQGEN", "234", "Resource (ballot) found"),
    BALLOT_BOX_RESOURCE_FOUND("ATREQGEN", "235", "Resource (ballotbox) found"),
    VERIFICATION_CARD_RESOURCE_FOUND("ATREQGEN", "236", "Resource (verification card) found"),
    VERIFICATION_CARD_SET_RESOURCE_FOUND("ATREQGEN", "237", "Resource (verification card set) found"),
    ELECTIONEVENT_DATE_VALIDATION_ERROR("EEVA", "238", "Election event date validation failed"),
    ELECTIONEVENT_DATE_VALIDATION_SUCCESS("EEVA", "239", "Election event date validation succeeded"),
;

	private final String layer;

	private final String action;

	private final String outcome;

	private final String info;

	VotingWorkflowLogEvents(final String action, final String outcome, final String info) {
		this.layer = "";
		this.action = action;
		this.outcome = outcome;
		this.info = info;
	}

	/**
	 * @see VotingWorkflowLogEvents#getAction()
	 */
	@Override
	public String getAction() {
		return action;
	}

	/**
	 * @see VotingWorkflowLogEvents#getOutcome()
	 */
	@Override
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @see VotingWorkflowLogEvents#getInfo()
	 */
	@Override
	public String getInfo() {
		return info;
	}

	/**
	 * @see VotingWorkflowLogEvents#getLayer()
	 */
	@Override
	public String getLayer() {
		return layer;
	}
}
