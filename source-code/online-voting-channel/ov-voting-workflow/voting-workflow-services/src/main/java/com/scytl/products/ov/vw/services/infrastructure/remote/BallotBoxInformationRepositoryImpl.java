/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.logging.I18nLoggerMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;
import com.scytl.products.ov.vw.services.domain.model.ballotbox.BallotBoxInformationRepository;

/**
 * Implementation of the repository for handling BallotInformation Objects through a REST client.
 */
@Stateless(name = "vw-BallotBoxInformationRepositoryImpl")
public class BallotBoxInformationRepositoryImpl implements BallotBoxInformationRepository {

	// The properties file reader.
	private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

	// The path to the resource authentication information.
	private static final String BALLOT_BOX_PATH = PROPERTIES.getPropertyValue("BALLOT_BOX_PATH");

	private static final String ACCEPT_HEADER = MediaType.APPLICATION_JSON;

	private static final Logger LOG = LoggerFactory.getLogger("std");

	// Instance of the I18N logger messages
	private static final I18nLoggerMessages I18N = I18nLoggerMessages.getInstance();

	// Instance of the track Id which will be written in the logs
	@Inject
	private TrackIdInstance trackId;

	private ElectionInformationClient electionInformationClient;

	@Inject
	BallotBoxInformationRepositoryImpl(final ElectionInformationClient electionInformationClient) {
		this.electionInformationClient = electionInformationClient;
	}

	/**
	 * Gets the BallotBox information for a given tenant, election event and ballot box identifier.
	 *
	 * @param tenantId - identifier of the tenant
	 * @param electionEventId - the identifier of the election event.
	 * @param ballotBoxId - identifier of the ballot.
	 * @return a BallotBoxInformationObject.
	 */
	@Override
	public String getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(String tenantId, String electionEventId,
			String ballotBoxId) throws ResourceNotFoundException {

		LOG.info(I18N.getMessage("BallotBoxRepoImpl.getBallotBoxInfo"), tenantId, ballotBoxId);
		try {
			String result = RetrofitConsumer.processResponse(electionInformationClient.getBallotBoxInformation(ACCEPT_HEADER,trackId.getTrackId(), BALLOT_BOX_PATH,
				tenantId, electionEventId, ballotBoxId));
			LOG.info(I18N.getMessage("BallotBoxRepoImpl.getBallotBoxInfo.found"), tenantId, ballotBoxId);
			return result;
		} catch (ResourceNotFoundException e) {
			LOG.error(I18N.getMessage("BallotBoxRepoImpl.getBallotBoxInfo.notFound"), tenantId, ballotBoxId);
			throw e;

		}
	}

}
