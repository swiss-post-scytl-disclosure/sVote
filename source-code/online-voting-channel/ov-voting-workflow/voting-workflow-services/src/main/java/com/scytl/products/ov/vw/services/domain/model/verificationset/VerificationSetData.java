/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.verificationset;

/**
 * Data of a verification set.
 */
public class VerificationSetData {
	// the voting card set id
	private String id;

	// encryption public key
	private String choicesCodesEncryptionPublicKey;

	private String verificationCardSetId;

	// certificate
	private String verificationCardIssuerCert;

	// voteCast Code
	private String voteCastCodeSignerCert;

	// election event id
	private String electionEventId;

	/**
	 * Returns the current value of the field id.
	 *
	 * @return Returns the id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the value of the field id.
	 *
	 * @param id The id to set.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets choicesCodesEncryptionPublicKey.
	 *
	 * @return Value of choicesCodesEncryptionPublicKey.
	 */
	public String getChoicesCodesEncryptionPublicKey() {
		return choicesCodesEncryptionPublicKey;
	}

	/**
	 * Gets verificationCardSetId.
	 *
	 * @return Value of verificationCardSetId.
	 */
	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	/**
	 * Gets voteCastCodeSignerCert.
	 *
	 * @return Value of voteCastCodeSignerCert.
	 */
	public String getVoteCastCodeSignerCert() {
		return voteCastCodeSignerCert;
	}

	/**
	 * Sets new voteCastCodeSignerCert.
	 *
	 * @param voteCastCodeSignerCert New value of voteCastCodeSignerCert.
	 */
	public void setVoteCastCodeSignerCert(String voteCastCodeSignerCert) {
		this.voteCastCodeSignerCert = voteCastCodeSignerCert;
	}

	/**
	 * Sets new verificationCardIssuerCert.
	 *
	 * @param verificationCardIssuerCert New value of verificationCardIssuerCert.
	 */
	public void setVerificationCardIssuerCert(String verificationCardIssuerCert) {
		this.verificationCardIssuerCert = verificationCardIssuerCert;
	}

	/**
	 * Sets new verificationCardSetId.
	 *
	 * @param verificationCardSetId New value of verificationCardSetId.
	 */
	public void setVerificationCardSetId(String verificationCardSetId) {
		this.verificationCardSetId = verificationCardSetId;
	}

	/**
	 * Gets verificationCardIssuerCert.
	 *
	 * @return Value of verificationCardIssuerCert.
	 */
	public String getVerificationCardIssuerCert() {
		return verificationCardIssuerCert;
	}

	/**
	 * Sets new choicesCodesEncryptionPublicKey.
	 *
	 * @param choicesCodesEncryptionPublicKey New value of choicesCodesEncryptionPublicKey.
	 */
	public void setChoicesCodesEncryptionPublicKey(String choicesCodesEncryptionPublicKey) {
		this.choicesCodesEncryptionPublicKey = choicesCodesEncryptionPublicKey;
	}

	/**
	 * Sets new electionEventId.
	 *
	 * @param electionEventId New value of electionEventId.
	 */
	public void setElectionEventId(String electionEventId) {
		this.electionEventId = electionEventId;
	}

	/**
	 * Gets electionEventId.
	 *
	 * @return Value of electionEventId.
	 */
	public String getElectionEventId() {
		return electionEventId;
	}
}
