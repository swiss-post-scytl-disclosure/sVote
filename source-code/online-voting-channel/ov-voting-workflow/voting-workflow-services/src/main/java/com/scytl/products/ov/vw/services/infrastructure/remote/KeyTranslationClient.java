/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.vw.services.domain.model.information.ElectionEventTranslation;

/**
 * The Interface for vote verification client.
 */
public interface KeyTranslationClient {

    /**
     * The Constant PARAMETER_VALUE_TENANT_ID.
     */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /**
     * The Constant PARAMETER_VALUE_ELECTION_EVENT_ID.
     */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    /**
     * The Constant PARAMETER_VALUE_ALIAS.
     */
    public static final String PARAMETER_VALUE_ALIAS = "alias";

    /**
     * The Constant PARAMETER_KEY_TRANSLATION_ELECTION_PATH.
     */
    public static final String PARAMETER_KEY_TRANSLATION_ELECTION_PATH = "keyTranslationElectionPath";

    /**
     * Get the election event translation object by the election identifier.
     *
     * @param trackId         the track id for logging purposes
     * @param tenantId        the tenant id
     * @param electionEventId the election event id
     * @return the election event translation object
     * @throws ResourceNotFoundException when the webservice fails
     */
    @GET("keytranslation/{keyTranslationElectionPath}/tenant/{tenantId}/electionEvent/{electionEventId}")
    Call<ElectionEventTranslation> getByElectionEventId(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
                                                  @Path(PARAMETER_KEY_TRANSLATION_ELECTION_PATH) String keyTranslationPath,
                                                  @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                                  @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId)
        throws ResourceNotFoundException;

    /**
     * Get the election event translation object by the election alias.
     *
     * @param trackId          the track id for logging purposes
     * @param keyTranslationPath the key translation path
     * @param tenantId         the tenant id
     * @param alias            the election event alias
     * @return the election event translation object
     * @throws ResourceNotFoundException when the webservice fails
     */
    @GET("keytranslation/{keyTranslationElectionPath}/tenant/{tenantId}/alias/{alias}")
    Call<ElectionEventTranslation> getByElectionEventAlias(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
                                                     @Path(PARAMETER_KEY_TRANSLATION_ELECTION_PATH) String keyTranslationPath,
                                                     @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
                                                     @Path(PARAMETER_VALUE_ALIAS) String alias) throws ResourceNotFoundException;
}
