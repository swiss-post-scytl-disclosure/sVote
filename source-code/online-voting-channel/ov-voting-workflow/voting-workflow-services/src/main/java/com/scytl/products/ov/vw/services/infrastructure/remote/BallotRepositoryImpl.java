/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.logging.I18nLoggerMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;
import com.scytl.products.ov.vw.services.domain.model.ballot.BallotRepository;

/**
 * This the implementation of the ballot repository.
 */
@Stateless(name = "vw-BallotRepositoryImpl")
public class BallotRepositoryImpl implements BallotRepository {

	// Instance of the secure logger
	private static final Logger LOG = LoggerFactory.getLogger("std");

	// Instance of the I18N logger messages
	private static final I18nLoggerMessages I18N = I18nLoggerMessages.getInstance();

	// The properties file reader.
	private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

	// The path to the resource ballots
	private static final String PATH_BALLOTS = PROPERTIES.getPropertyValue("BALLOTS_PATH");

	// Instance of the track Id which will be written in the logs
	@Inject
	private TrackIdInstance trackId;

	private ElectionInformationClient electionInformationClient;

	@Inject
	BallotRepositoryImpl(final ElectionInformationClient electionInformationClient) {
		this.electionInformationClient = electionInformationClient;
	}

	/**
	 * Searches for a ballot identified by tenant, election event and ballot identifier. The implementation is a REST Client
	 * invoking to an get operation of a REST web service to obtain the ballot for the given parameters.
	 * 
	 * @param tenantId - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param ballotId - the identifier of the ballot.
	 * @return a Ballot of the tenant identified by tenant id, and identified by ballot id.
	 * @throws ResourceNotFoundException if ballot is not found.
	 */
	@Override
	public String findByTenantIdElectionEventIdBallotId(String tenantId, String electionEventId, String ballotId)
			throws ResourceNotFoundException {
		LOG.info(I18N.getMessage("BallotRepositoryImpl.findByTenantIdAndBallotId"), tenantId, ballotId);

		try {
			String result = RetrofitConsumer.processResponse(electionInformationClient.getBallot(trackId.getTrackId(), PATH_BALLOTS, tenantId,
				electionEventId, ballotId));
			LOG.info(I18N.getMessage("BallotRepositoryImpl.findByTenantIdAndBallotId.found"), tenantId, ballotId);
			return result;
		} catch (ResourceNotFoundException e) {
			LOG.info(I18N.getMessage("BallotRepositoryImpl.findByTenantIdAndBallotId.notFound"), tenantId, ballotId);
			throw e;
		}
	}

}
