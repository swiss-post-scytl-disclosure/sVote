/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.confirmation;

/**
 * The class representing the vote cast message.
 */
public class VoteCastMessage {

	// the calculated vote cast code
	private String voteCastCode;

	// the cast code signed
	private String signature;
	
	

	public VoteCastMessage(String voteCastCode, String signature) {
        this.voteCastCode = voteCastCode;
        this.signature = signature;
    }

    /**
	 * Sets new signature.
	 *
	 * @param signature New value of signature.
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * Gets signature.
	 *
	 * @return Value of signature.
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets new voteCastCode.
	 *
	 * @param voteCastCode New value of voteCastCode.
	 */
	public void setVoteCastCode(String voteCastCode) {
		this.voteCastCode = voteCastCode;
	}

	/**
	 * Gets voteCastCode.
	 *
	 * @return Value of voteCastCode.
	 */
	public String getVoteCastCode() {
		return voteCastCode;
	}
}