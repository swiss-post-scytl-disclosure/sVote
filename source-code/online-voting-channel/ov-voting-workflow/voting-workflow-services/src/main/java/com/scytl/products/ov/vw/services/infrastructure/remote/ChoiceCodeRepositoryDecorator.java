/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import static com.scytl.products.oscore.logging.api.domain.Level.ERROR;
import static com.scytl.products.oscore.logging.api.domain.Level.INFO;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.dto.ChoiceCodeAndComputeResults;
import com.scytl.products.ov.commons.dto.VoteAndComputeResults;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vw.infrastructure.log.VotingWorkflowLogConstants;
import com.scytl.products.ov.vw.infrastructure.log.VotingWorkflowLogEvents;
import com.scytl.products.ov.vw.services.domain.model.choicecode.ChoiceCodeRepository;

@Decorator
public class ChoiceCodeRepositoryDecorator implements ChoiceCodeRepository {

    @Delegate
    @Inject
    private ChoiceCodeRepository choiceCodeRepository;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;

    @Inject
    private TrackIdInstance trackIdInstance;

    @Override
    public ChoiceCodeAndComputeResults generateChoiceCodes(String tenantId, String electionEventId,
            String verificationCardId, VoteAndComputeResults vote) throws ResourceNotFoundException {

        try {

            ChoiceCodeAndComputeResults generateChoiceCodes =
                choiceCodeRepository.generateChoiceCodes(tenantId, electionEventId, verificationCardId, vote);

            secureLoggerWriter.log(INFO,
                new LogContent.LogContentBuilder().logEvent(VotingWorkflowLogEvents.CHOICE_CODES_RETRIEVED)
                    .electionEvent(electionEventId)
                    .additionalInfo(VotingWorkflowLogConstants.INFO_TRACK_ID, trackIdInstance.getTrackId())
                    .additionalInfo(VotingWorkflowLogConstants.INFO_VERIFICATION_CARD_ID, verificationCardId)
                    .createLogInfo());

            return generateChoiceCodes;

        } catch (ResourceNotFoundException e) {

            secureLoggerWriter.log(ERROR,
                new LogContent.LogContentBuilder().logEvent(VotingWorkflowLogEvents.STOP_VOTING_PROCESS)
                    .electionEvent(electionEventId)
                    .additionalInfo(VotingWorkflowLogConstants.INFO_TRACK_ID, trackIdInstance.getTrackId())
                    .additionalInfo(VotingWorkflowLogConstants.INFO_ERR_DESC,
                        "Error generating choice codes: " + ExceptionUtils.getRootCauseMessage(e))
                    .createLogInfo());

            throw e;
        }

    }

}
