/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.dto.ChoiceCodeAndComputeResults;
import com.scytl.products.ov.commons.dto.VoteAndComputeResults;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;
import com.scytl.products.ov.vw.services.domain.model.choicecode.ChoiceCodeRepository;

/**
 * Implementation of the ChoiceCodeRepository using a REST client.
 */
@Stateless
public class ChoiceCodeRepositoryImpl implements ChoiceCodeRepository {

    /**
     * The properties file reader.
     */
    private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

    /**
     * The path to the resource verification.
     */
    private static final String CHOICE_CODE_PATH = PROPERTIES.getPropertyValue("CHOICE_CODE_PATH");

    private static final String VOTES_PATH = PROPERTIES.getPropertyValue("VOTES_PATH");

    @Inject
    private TrackIdInstance trackId;

    private VerificationClient verificationClient;

    private ElectionInformationClient electionInformationClient;

    @Inject
    ChoiceCodeRepositoryImpl(final VerificationClient verificationClient,
            final ElectionInformationClient electionInformationClient) {
        this.verificationClient = verificationClient;
        this.electionInformationClient = electionInformationClient;
    }

    /**
     * Generates the choice codes taking into account a tenant, election event
     * and verification card for a given encrypted vote. This implementation is
     * based on a rest client which call to a web service rest operation.
     * 
     * @param tenantId
     *            - the identifier of the tenant.
     * @param electionEventId
     *            - the identifier of the election event.
     * @param verificationCardId
     *            - the identifier of the verification card.
     * @param vote
     *            - the encrypted vote.
     * @return The choice codes generated and all compute results.
     * @throws ResourceNotFoundException
     *             if the choice codes can not be generated.
     */
    @Override
    public ChoiceCodeAndComputeResults generateChoiceCodes(String tenantId, String electionEventId,
            String verificationCardId, VoteAndComputeResults vote) throws ResourceNotFoundException {
        return RetrofitConsumer.processResponse(verificationClient.generateChoiceCodes(trackId.getTrackId(),
            CHOICE_CODE_PATH, tenantId, electionEventId, verificationCardId, vote));
    }

}
