/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.infrastructure.log;

/**
 * Constants for logging in voting workflow.
 */
public class VotingWorkflowLogConstants {

	/**
	 * Error description - additional information - additional information.
	 */
	public static final String INFO_ERR_DESC = "#err_desc";

	/**
	 * Description - additional information - additional information.
	 */
	public static final String INFO_DESC = "#desc";

	/**
	 * Resource id: ballotID, verificationCardID, verificationCardSetID or ballotBoxID - additional information..
	 */
	public static final String INFO_RESOURCE_ID = "#resource_id";

	/**
	 * Status of the voting card - additional information.
	 */
	public static final String INFO_STATUS_VOTING_CARD = "#vc_idStatus";

	/**
	 * Track id - additional information.
	 */
	public static final String INFO_TRACK_ID = "#request_id";

	public static final String CONFIRMATION_ATTEMPTS = "#confirmAttempts";

	/**
	 * TimeStamp if the authentication token
	 */
	public static final String INFO_TIMESTAMP_AT = "#timestamp_at";

	/**
	 * Election dates
	 */
	public static final String INFO_ELECTION_DATE = "#election_dates";

	/**
	 * Ballot box id
	 */
	public static final String INFO_BALLOT_BOX_ID = "#bb_id";

	/**
	 * Verification card id
	 */
	public static final String INFO_VERIFICATION_CARD_ID = " #verifc_id";

	/**
	 * Vote hash
	 */
	public static final String INFO_VOTE_HASH = "#hashVote";

	/**
	 * Auth token id
	 */
	public static final String INFO_AUTH_TOKEN_ID = "#at_id";

	/**
	 * Receipt
	 */
	public static final String INFO_RECEIPT = "#voteReceipt";

	/**
	 * Confirmation message hash
	 */
	public static final String INFO_HASH_CONFIRMATION_MESSAGE = "#hashCM";

	/**
	 * Verification card set id
	 */
	public static final String INFO_VERIFICATION_CARD_SET_ID = "#verifcs_id";

	/**
	 * Non-public constructor
	 */
	private VotingWorkflowLogConstants() {		
	}

}
