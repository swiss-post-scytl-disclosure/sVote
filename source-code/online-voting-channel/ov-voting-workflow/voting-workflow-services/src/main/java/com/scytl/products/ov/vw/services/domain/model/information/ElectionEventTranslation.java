/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.information;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;

import com.scytl.products.ov.commons.beans.errors.SyntaxErrorGroup;

/**
 * Class representing an election event translation record.
 */
public class ElectionEventTranslation {

	/**
	 * The identifier of the entity.
	 */
	@JsonIgnore
	private Integer id;

	/**
	 * The tenant identifier.
	 */
	@NotNull(groups = SyntaxErrorGroup.class)
	private String tenantId;

	/**
	 * The election event identifier.
	 */
	@NotNull(groups = SyntaxErrorGroup.class)
	private String electionEventId;

	/**
	 * The election event alias.
	 */
	@NotNull(groups = SyntaxErrorGroup.class)
	private String alias;

	/**
	 * Returns the current value of the field id.
	 *
	 * @return Returns the id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the value of the field id.
	 *
	 * @param id The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Returns the current value of the field tenantId.
	 *
	 * @return Returns the tenantId.
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Sets the value of the field tenantId.
	 *
	 * @param tenantId The tenantId to set.
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * Returns the current value of the field electionEventId.
	 *
	 * @return Returns the electionEventId.
	 */
	public String getElectionEventId() {
		return electionEventId;
	}

	/**
	 * Sets the value of the field electionEventId.
	 *
	 * @param electionEventId The electionEventId to set.
	 */
	public void setElectionEventId(String electionEventId) {
		this.electionEventId = electionEventId;
	}

	/**
	 * Returns the current value of the field alias.
	 *
	 * @return Returns the election event alias.
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the value of the field alias.
	 *
	 * @param alias The election event alias to set.
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

}
