/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.commons.validation.ValidationResult;
import com.scytl.products.ov.vw.services.domain.model.authentication.AuthenticationTokenMessage;
import com.scytl.products.ov.vw.services.domain.model.authentication.ChallengeInformation;
import com.scytl.products.ov.vw.services.domain.model.authentication.CredentialInformation;

/**
 * The Interface of authentication client.
 */
public interface AuthenticationClient {

    /** The Constant PARAMETER_VALUE_TENANT_ID. */
    public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    public static final String PARAMETER_VALUE_VOTING_CARD_ID = "votingCardId";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    public static final String PARAMETER_VALUE_CREDENTIAL_ID = "credentialId";

    /** The Constant PARAMETER_PATH_TOKENS. */
    public static final String PARAMETER_PATH_TOKENS = "pathTokens";

    /** The Constant PARAMETER_PATH_VALIDATION_TOKENS. */
    public static final String PARAMETER_PATH_VALIDATION_TOKENS = "pathValidationTokens";

    /** The Constant PARAMETER_PATH_INFORMATIONS. */
    public static final String PARAMETER_PATH_INFORMATIONS = "pathInformations";

    /** The Constant PARAMETER_AUTHENTICATION_TOKEN. */
    public static final String PARAMETER_AUTHENTICATION_TOKEN = "authenticationToken";

    /**
     * Gets the authentication token message.
     *
     * @param pathTokens
     *            the path tokens
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param credentialId
     *            the credential id
     * @param trackId
     *            the track id
     * @param challengeInformation
     *            the challenge information
     * @return the authentication token
     * @throws ResourceNotFoundException
     *             the resource not found exception
     */
    @POST("{pathTokens}/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}")
    Call<AuthenticationTokenMessage> getAuthenticationToken(@Path(PARAMETER_PATH_TOKENS) String pathTokens,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_CREDENTIAL_ID) String credentialId,
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Body ChallengeInformation challengeInformation) throws ResourceNotFoundException;

    /**
     * Validate authentication token.
     *
     * @param pathValidationTokens
     *            the path validation tokens
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param votingCardId
     *            the voting card id
     * @param trackId
     *            the track id
     * @param authenticationToken
     *            the authentication token
     * @return the validation result
     * @throws ResourceNotFoundException
     *             the resource not found exception
     */
    @POST("{pathValidationTokens}/tenant/{tenantId}/electionevent/{electionEventId}/votingcard/{votingCardId}")
    Call<ValidationResult> validateAuthenticationToken(@Path(PARAMETER_PATH_VALIDATION_TOKENS) String pathValidationTokens,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_VOTING_CARD_ID) String votingCardId,
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Header(PARAMETER_AUTHENTICATION_TOKEN) String authenticationToken) throws ResourceNotFoundException;

    /**
     * Find by tenant election event voting card.
     *
     * @param trackId
     *            the track id
     * @param pathInformations
     *            the path informations
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param credentialId
     *            the credential id
     * @return the credential information
     * @throws ResourceNotFoundException
     *             the resource not found exception
     */
    @GET("{pathInformations}/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}")
    Call<CredentialInformation> findByTenantElectionEventCredential(
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Path(PARAMETER_PATH_INFORMATIONS) String pathInformations,
            @Path(PARAMETER_VALUE_TENANT_ID) String tenantId,
            @Path(PARAMETER_VALUE_ELECTION_EVENT_ID) String electionEventId,
            @Path(PARAMETER_VALUE_CREDENTIAL_ID) String credentialId) throws ResourceNotFoundException;

}
