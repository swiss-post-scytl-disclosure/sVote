/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.choicecode;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.dto.ChoiceCodeAndComputeResults;
import com.scytl.products.ov.commons.dto.VoteAndComputeResults;

/**
 * Interface for providing operations related with choice codes generation.
 */
public interface ChoiceCodeRepository {

    /**
     * Generates the choice codes taking into account a tenant, election event
     * and verification card for a given encrypted vote.
     *
     * @param tenantId
     *            - the identifier of the tenant.
     * @param electionEventId
     *            - the identifier of the election event.
     * @param verificationCardId
     *            - the identifier of the verification card.
     * @param VoteAndComputeResults
     *            - the vote used during the generation of choice codes.
     * @return choice codes generated and computation results
     * @throws ResourceNotFoundException
     *             if the choice codes can not be generated.
     */
    ChoiceCodeAndComputeResults generateChoiceCodes(String tenantId, String electionEventId, String verificationCardId,
            VoteAndComputeResults vote) throws ResourceNotFoundException;

}
