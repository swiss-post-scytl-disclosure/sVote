/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.ballotbox;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * The repository for handling BallotInformation Objects.
 */
@Local
public interface BallotBoxInformationRepository {

	/**
	 * Gets the BallotBox information for a given tenant, election event and ballot box identifier.
	 *
	 * @param tenantId - identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param ballotBoxId - identifier of the ballot.
	 * @return a BallotBoxInformationObject.
	 */
			String
			getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(String tenantId, String electionEventId, String ballotBoxId)
					throws ResourceNotFoundException;

}
