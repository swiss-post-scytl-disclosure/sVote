/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.platform;

import javax.ejb.Stateless;

import com.scytl.products.ov.commons.beans.validation.CertificateValidationServiceImpl;

@Stateless
@VwCertificateValidationService
public class VwCertificateValidationServiceImpl extends CertificateValidationServiceImpl {

}
