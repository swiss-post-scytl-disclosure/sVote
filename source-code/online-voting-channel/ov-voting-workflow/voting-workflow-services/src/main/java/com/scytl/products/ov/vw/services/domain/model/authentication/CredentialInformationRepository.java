/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.authentication;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * Repository for handling AuthenticationInformation.
 */
@Local
public interface CredentialInformationRepository {

    /**
     * Searches for the Authentication information related to given parameters.
     * 
     * @param tenantId
     *            - the identifier of the tenant.
     * @param electionEventId
     *            - the identifier of the election event.
     * @param credentialId
     *            - the identifier of the credential.
     * @return The associated AuthenticationInformation, if found.
     * @throws ResourceNotFoundException
     *             if the authentication information is not found.
     */
    CredentialInformation findByTenantElectionEventCredential(String tenantId, String electionEventId,
            String credentialId) throws ResourceNotFoundException, ApplicationException;
}
