/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import retrofit2.Call;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

import javax.validation.constraints.NotNull;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;

/**
 * Web service client interface for using verification codes when voting.
 */
public interface VerificationCodeClient {

    /** The Constant PARAMETER_VALUE_ELECTION_EVENT_ID. */
    public static final String PARAMETER_VALUE_ELECTION_EVENT_ALIAS = "electionEventAlias";

    /** The Constant PARAMETER_VALUE_VOTING_CARD_ID. */
    public static final String PARAMETER_VALUE_VOTING_CARD_ID = "votingCardId";

    /**
     * Sends a verification code to the voter.
     * @param trackId
     *          the tracking request id
     * @param electionEventAlias
     *            the election event ID.
     * @param votingCardId
     *            the voting card ID.
     * @return response indicating whether the the call was successful.
     * @throws ResourceNotFoundException if operation with the web service fails.
     */
    @POST("elections/{electionEventAlias}/sendOtp/{votingCardId}")
    Call<ResponseBody> sendVerificationCode(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
                                  @Path(PARAMETER_VALUE_ELECTION_EVENT_ALIAS) String electionEventAlias,
                                  @Path(PARAMETER_VALUE_VOTING_CARD_ID) String votingCardId) throws ResourceNotFoundException;

    /**
     * Checks the verification that was sent to the voter.
     *
     * @param trackId
     *            the tracking request id
     * @param electionEventAlias
     *            the election event ID.
     * @param votingCardId
     *            the voting card ID.
     * @param verificationCode
     *            the verification code.
     * @return response indicating whether the call was successful.
     * @throws ResourceNotFoundException if operation with the web service fails.
     */
    @POST("elections/{electionEventAlias}/checkOtp/{votingCardId}")
    Call<ResponseBody> checkVerificationCode(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
                                   @Path(PARAMETER_VALUE_ELECTION_EVENT_ALIAS) String electionEventAlias,
                                   @Path(PARAMETER_VALUE_VOTING_CARD_ID) String votingCardId,
                                   @NotNull @Body VerificationCode verificationCode)
            throws ResourceNotFoundException;
}
