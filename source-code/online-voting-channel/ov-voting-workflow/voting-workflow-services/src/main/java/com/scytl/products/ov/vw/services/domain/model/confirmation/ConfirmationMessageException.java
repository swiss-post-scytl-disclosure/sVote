/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.confirmation;

import com.scytl.products.ov.vw.services.domain.model.state.VotingCardStates;

public class ConfirmationMessageException extends Exception {

    private static final long serialVersionUID = -6993225853997980165L;

    private final ConfirmationMessageErrorType confirmationMessageErrorType;

    private final VotingCardStates votingCardState;

    private final long attempts;

    private final ConfirmationInformationResult confirmationInformationResult;

    private ConfirmationMessageException(ConfirmationInformationResult confirmationInformationResult, long attempts, VotingCardStates votingCardState, ConfirmationMessageErrorType confirmationMessageErrorType) {
        this.confirmationInformationResult = confirmationInformationResult;
        this.attempts = attempts;
        this.votingCardState = votingCardState;
        this.confirmationMessageErrorType = confirmationMessageErrorType;
    }

    public VotingCardStates getVotingCardState() {
        return votingCardState;
    }

    public ConfirmationMessageErrorType getConfirmationMessageErrorType() {
        return confirmationMessageErrorType;
    }

    public long getAttempts() {
        return attempts;
    }

    public ConfirmationInformationResult getConfirmationInformationResult() {
        return confirmationInformationResult;
    }

    public static class Builder{

        private  ConfirmationMessageErrorType confirmationMessageErrorType;

        private  VotingCardStates votingCardState;

        private  long attempts;

        private ConfirmationInformationResult confirmationInformationResult;

        public Builder setConfirmationMessageErrorType(ConfirmationMessageErrorType confirmationMessageErrorType) {
            this.confirmationMessageErrorType = confirmationMessageErrorType;
            return this;
        }

        public Builder setVotingCardState(VotingCardStates votingCardState) {
            this.votingCardState = votingCardState;
            return this;
        }

        public Builder setAttempts(long attempts) {
            this.attempts = attempts;
            return this;
        }

        public Builder setConfirmationInformationResult(ConfirmationInformationResult confirmationInformationResult) {
            this.confirmationInformationResult = confirmationInformationResult;
            return this;
        }

        public ConfirmationMessageException build(){
            return new ConfirmationMessageException(confirmationInformationResult, attempts, votingCardState, confirmationMessageErrorType);
        }
    }
}