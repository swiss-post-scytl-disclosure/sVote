/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.state;

import java.util.Optional;

import javax.persistence.LockTimeoutException;
import javax.persistence.PessimisticLockException;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;
import com.scytl.products.ov.vw.services.domain.common.csv.ExportedPartialVotingCardStateItemWriter;

/**
 * Provides operations on the voting card state repository.
 */
public interface VotingCardStateRepository
        extends BaseRepository<VotingCardState, Long> {

    /**
     * Returns a voting card state for a given tenant, election event and voting
     * card.
     * 
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @param votingCardId
     *            - the voting card identifier.
     * @return The voting card state.
     * @throws ResourceNotFoundException
     *             if voting card state is not found.
     */
    Optional<VotingCardState> acquire(
            String tenantId, String electionEventId, String votingCardId)
            throws PessimisticLockException, LockTimeoutException;

    /**
     * Find and write voting cards with inactive state.
     *
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param writer
     *            the writer
     */
    void findAndWriteVotingCardsWithInactiveState(final String tenantId,
            final String electionEventId,
            final ExportedPartialVotingCardStateItemWriter writer);
}
