/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.vw.infrastructure.log.SecureLoggerInformation;
import com.scytl.products.ov.vw.infrastructure.log.VotingWorkflowLogConstants;
import com.scytl.products.ov.vw.infrastructure.log.VotingWorkflowLogEvents;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformation;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformationRepository;
import com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformationResult;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;

/**
 * Decorator for confirmation information repository.
 */
@Decorator
public class ConfirmationInformationRepositoryDecorator implements ConfirmationInformationRepository {

	@Inject
	@Delegate
	private ConfirmationInformationRepository confirmationInformationRepository;

	@Inject
	private TrackIdInstance trackIdInstance;

	@Inject
	private SecureLoggingWriter secureLoggerWriter;

	@Inject
	private SecureLoggerInformation secureLoggerInformation;

	/**
	 * @throws ResourceNotFoundException 
	 * @see com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformationRepository#validateConfirmationMessage(java.lang.String,
	 *      java.lang.String, java.lang.String,
	 *      com.scytl.products.ov.vw.services.domain.model.confirmation.ConfirmationInformation, java.lang.String)
	 */
	@Override
	public ConfirmationInformationResult validateConfirmationMessage(String tenantId, String electionEventId,
			String votingCardId, ConfirmationInformation confirmationInformation, String token) throws ResourceNotFoundException {
		try {
			ConfirmationInformationResult result = confirmationInformationRepository.validateConfirmationMessage(tenantId,
				electionEventId, votingCardId, confirmationInformation, token);
			if (result.isValid()) {
				secureLoggerWriter.log(Level.INFO,
					new LogContent.LogContentBuilder().logEvent(VotingWorkflowLogEvents.CONFIRMATION_MESSAGE_VALIDATION_OK)
						.objectId(secureLoggerInformation.getAuthenticationTokenHash()).user(votingCardId)
						.electionEvent(electionEventId)
						.additionalInfo(VotingWorkflowLogConstants.INFO_TRACK_ID, trackIdInstance.getTrackId())
						.createLogInfo());
			} else {
				secureLoggerWriter.log(Level.ERROR,
					new LogContent.LogContentBuilder().logEvent(VotingWorkflowLogEvents.CONFIRMATION_MESSAGE_INVALID)
						.objectId(secureLoggerInformation.getAuthenticationTokenHash()).user(votingCardId)
						.electionEvent(electionEventId)
						.additionalInfo(VotingWorkflowLogConstants.INFO_TRACK_ID, trackIdInstance.getTrackId())
						.additionalInfo(VotingWorkflowLogConstants.INFO_HASH_CONFIRMATION_MESSAGE,
							secureLoggerInformation.getConfirmationMessageHash())
					.additionalInfo(VotingWorkflowLogConstants.INFO_BALLOT_BOX_ID,
						secureLoggerInformation.getAuthenticationTokenObject().getVoterInformation().getBallotBoxId())
					.additionalInfo(VotingWorkflowLogConstants.INFO_ERR_DESC, "Invalid Confirmation message")
					.createLogInfo());
			}
			return result;
		} catch (RetrofitException e) {
			secureLoggerWriter.log(Level.ERROR,
				new LogContent.LogContentBuilder().logEvent(VotingWorkflowLogEvents.CONFIRMATION_MESSAGE_INVALID)
					.objectId(secureLoggerInformation.getAuthenticationTokenHash()).user(votingCardId)
					.electionEvent(electionEventId)
					.additionalInfo(VotingWorkflowLogConstants.INFO_TRACK_ID, trackIdInstance.getTrackId())
					.additionalInfo(VotingWorkflowLogConstants.INFO_HASH_CONFIRMATION_MESSAGE,
						secureLoggerInformation.getConfirmationMessageHash())
				.additionalInfo(VotingWorkflowLogConstants.INFO_BALLOT_BOX_ID,
					secureLoggerInformation.getAuthenticationTokenObject().getVoterInformation().getBallotBoxId())
				.additionalInfo(VotingWorkflowLogConstants.INFO_ERR_DESC,
					"Error validating Confirmation message: " + ExceptionUtils.getRootCauseMessage(e)).createLogInfo());
			throw e;
		}
	}

}
