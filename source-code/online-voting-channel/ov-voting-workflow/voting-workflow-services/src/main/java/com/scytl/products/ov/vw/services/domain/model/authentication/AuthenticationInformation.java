/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.authentication;

import com.scytl.products.ov.vw.services.domain.model.verification.Verification;

/**
 * Class which represents the information returned in the second flow of authentication.
 */
public class AuthenticationInformation {

	// Object representing an authentication token.
	private AuthenticationToken authenticationToken;

	// Object representing a ballot in json format.
	private String ballot;

	// Object representing a ballot box in json format.
	private String ballotBox;

	// Object representing the verification card data
	private Verification verificationCard;

	/**
	 * Returns the authentication token.
	 *
	 * @return the authentication token
	 */
	public AuthenticationToken getAuthenticationToken() {
		return authenticationToken;
	}

	/**
	 * Sets the value of the authentication token field.
	 *
	 * @param authenticationToken the new authentication token
	 */
	public void setAuthenticationToken(AuthenticationToken authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	/**
	 * Returns the value of the verificationCard field.
	 *
	 * @return the verification card
	 */
	public Verification getVerificationCard() {
		return verificationCard;
	}

	/**
	 * Sets the value of the verificationCard field.
	 *
	 * @param verificationCard the new verification card
	 */
	public void setVerificationCard(Verification verificationCard) {
		this.verificationCard = verificationCard;
	}

	/**
	 * Returns the current value of the field ballot.
	 *
	 * @return Returns the ballot.
	 */
	public String getBallot() {
		return ballot;
	}

	/**
	 * Sets the value of the field ballot.
	 *
	 * @param ballot The ballot to set.
	 */
	public void setBallot(String ballot) {
		this.ballot = ballot;
	}

	/**
	 * Returns the current value of the field ballotBox.
	 *
	 * @return Returns the ballotBox.
	 */
	public String getBallotBox() {
		return ballotBox;
	}

	/**
	 * Sets the value of the field ballotBox.
	 *
	 * @param ballotBox The ballotBox to set.
	 */
	public void setBallotBox(String ballotBox) {
		this.ballotBox = ballotBox;
	}

}
