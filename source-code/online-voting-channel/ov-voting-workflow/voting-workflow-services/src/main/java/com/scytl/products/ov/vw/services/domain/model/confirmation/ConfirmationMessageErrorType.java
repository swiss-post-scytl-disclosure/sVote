/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.confirmation;

public enum  ConfirmationMessageErrorType {

    WRONG_BALLOT_CASTING_KEY,
    INVALID_VOTING_CARD_STATE,
    ATTEMPS_GREATER_THAN_ALLOWED,
    INVALID_CONFIRMATION_MESSAGE,
    CAST_CODE_FAILED_WRONG_BCK,
    CAST_CODE_FAILED_WRONG_ATTEMPTS_EXCEED;

}