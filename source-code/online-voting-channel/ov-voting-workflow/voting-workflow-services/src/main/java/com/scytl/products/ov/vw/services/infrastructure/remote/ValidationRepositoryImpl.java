/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.infrastructure.remote;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;
import com.scytl.products.ov.commons.validation.ValidationResult;
import com.scytl.products.ov.vw.services.domain.model.validation.ValidationRepository;
import com.scytl.products.ov.vw.services.domain.model.vote.ValidationVoteResult;

/**
 * Implementation of the repository.
 */
@Stateless
public class ValidationRepositoryImpl implements ValidationRepository {

    // PropertiesFile
    private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();

    // The path to the resource validations.
    private static final String VALIDATIONS_PATH = PROPERTIES.getPropertyValue("VALIDATIONS_PATH");

    // Instance of the track Id which will be written in the logs
    @Inject
    private TrackIdInstance trackId;

    private VerificationClient verificationClient;

    private ElectionInformationClient electionInformationClient;

    @Inject
    ValidationRepositoryImpl(final VerificationClient verificationClient,
                             final ElectionInformationClient electionInformationClient) {
        this.verificationClient = verificationClient;
        this.electionInformationClient = electionInformationClient;
    }

    /**
     * @throws ResourceNotFoundException
     *             when the remote call fails.
     * @see com.scytl.products.ov.vw.services.domain.model.validation.ValidationRepository#validateVoteInVV(String,
     *      String, Vote)
     */
    @Override
    public com.scytl.products.ov.commons.validation.ValidationResult validateVoteInVV(String tenantId,
            String electionEventId, Vote vote) throws ResourceNotFoundException {
        return RetrofitConsumer.processResponse(verificationClient.validateVote(trackId.getTrackId(), VALIDATIONS_PATH, tenantId, electionEventId, vote));
    }

    /**
     * @see com.scytl.products.ov.vw.services.domain.model.validation.ValidationRepository#validateVoteInEI(String,
     *      String, Vote)
     */
    @Override
    public com.scytl.products.ov.commons.validation.ValidationResult validateVoteInEI(String tenantId,
            String electionEventId, Vote vote) throws ResourceNotFoundException {
        return RetrofitConsumer.processResponse(electionInformationClient.validateVote(trackId.getTrackId(), VALIDATIONS_PATH, tenantId, electionEventId,
            vote));
    }

    /**
     * Validates if an election is in dates.
     *
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id.
     * @param ballotBoxId
     *            - the ballot box id.
     * @return the result of the validation.
     * @throws ResourceNotFoundException
     */
    @Override
    public ValidationResult validateElectionDatesInEI(String tenantId, String electionEventId, String ballotBoxId)
            throws ResourceNotFoundException {
        return RetrofitConsumer.processResponse(electionInformationClient.validateElectionInDates(trackId.getTrackId(), VALIDATIONS_PATH, tenantId,
            electionEventId, ballotBoxId));
    }

    @Override
    public ValidationVoteResult validateVote(String tenantId, String electionEventId, Vote vote)
            throws ResourceNotFoundException {
        // result of vote validation
        ValidationVoteResult validationVoteResult = new ValidationVoteResult();
        com.scytl.products.ov.commons.validation.ValidationResult validationResultEI =
            validateVoteInEI(tenantId, electionEventId, vote);
        validationVoteResult.setValidationError(validationResultEI.getValidationError());
        if (validationResultEI.isResult()) {
            com.scytl.products.ov.commons.validation.ValidationResult validationResultVV =
                validateVoteInVV(tenantId, electionEventId, vote);
            validationVoteResult.setValidationError(validationResultVV.getValidationError());
            validationVoteResult.setValid(validationResultVV.isResult());
        }
        return validationVoteResult;
    }
}
