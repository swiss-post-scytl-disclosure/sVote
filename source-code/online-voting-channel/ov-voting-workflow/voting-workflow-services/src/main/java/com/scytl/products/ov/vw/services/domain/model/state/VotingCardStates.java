/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.state;

/**
 * The state of a voting card to be checked during the client-server
 * authentication.
 */
public enum VotingCardStates {
    NONE, NOT_SENT, SENT_BUT_NOT_CAST, CHOICE_CODES_FAILED, WRONG_BALLOT_CASTING_KEY, CAST, BLOCKED, NOT_FOUND
}
