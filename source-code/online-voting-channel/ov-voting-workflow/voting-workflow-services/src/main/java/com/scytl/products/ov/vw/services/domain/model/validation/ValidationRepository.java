/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.vw.services.domain.model.validation;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.domain.model.vote.Vote;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.vw.services.domain.model.vote.ValidationVoteResult;

/**
 * Repository for handling validations.
 */
@Local
public interface ValidationRepository {

    /**
     * Sends a vote to be validated to the Vote Verification.
     *
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id
     * @param vote
     *            The vote to validate.
     * @return the result of the validation containing the response and the
     *         rules that failed.
     * @throws ResourceNotFoundException
     *             when the remote call fails.
     */
    com.scytl.products.ov.commons.validation.ValidationResult validateVoteInVV(String tenantId, String electionEventId,
            Vote vote) throws ResourceNotFoundException;

    /**
     * Sends a vote to be validated to the Election Information.
     *
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id
     * @param vote
     *            The vote to validate.
     * @return the result of the validation containing the response and the
     *         rules that failed.
     * @throws ResourceNotFoundException
     *             when the remote call fails.
     */
    com.scytl.products.ov.commons.validation.ValidationResult validateVoteInEI(String tenantId, String electionEventId,
            Vote vote) throws ResourceNotFoundException;

    /**
     * Validates the election dates given of a ballot box
     * 
     * @param tenantId
     *            - Tenant Identifier
     * @param electionEventId
     *            - electionEvent identifier
     * @param ballotBoxId
     *            - identifier of the ballot Box
     * @return the result of the validation.
     * @throws ResourceNotFoundException
     */
    com.scytl.products.ov.commons.validation.ValidationResult validateElectionDatesInEI(String tenantId,
            String electionEventId, String ballotBoxId) throws ResourceNotFoundException;

    /**
     * Validates the vote in both election information and vote verification
     * contexts.
     *
     * @param tenantId
     *            - Tenant Identifier
     * @param electionEventId
     *            - electionEvent identifier
     * @param vote
     *            the vote to be validated.
     * @return the result of the validation.
     * @throws ResourceNotFoundException
     */
    ValidationVoteResult validateVote(String tenantId, String electionEventId, Vote vote)
            throws ResourceNotFoundException;
}
