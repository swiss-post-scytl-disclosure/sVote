/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import java.security.cert.CertificateException;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.scytl.products.ov.commons.validation.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationContent;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.au.services.infrastructure.log.AuthenticationLogConstants;
import com.scytl.products.ov.au.services.infrastructure.log.AuthenticationLogEvents;
import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.logging.service.I18nLoggerMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

/**
 * Decorator for authentication token validation service.
 */
@Decorator
public class AuthenticationTokenValidationServiceDecorator implements AuthenticationTokenValidationService {

	@Inject
	@Delegate
	private AuthenticationTokenValidationService authenticationTokenValidationService;

	@Inject
	private TrackIdInstance trackId;

	@Inject
	private SecureLoggingWriter secureLoggerWriter;

	@Inject
	private SecureLoggerHelper secureLoggerHelper;

	private static final Logger LOG = LoggerFactory.getLogger("std");

	private static final I18nLoggerMessages I18N = I18nLoggerMessages.getInstance();

	/**
	 * @see com.scytl.products.ov.au.services.domain.service.validation.AuthenticationTokenValidationService#validate(java.lang.String,
	 *      java.lang.String, java.lang.String,
	 *      com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken)
	 */
	@Override
	public ValidationResult validate(String tenantId, String electionEventId, String votingCardId,
									 AuthenticationToken authenticationToken) throws ResourceNotFoundException, CertificateException {
		LOG.info(I18N.getMessage("AuthenticationTokenValidationService.validate.start"));
		try {
			ValidationResult validationResult =
				authenticationTokenValidationService.validate(tenantId, electionEventId, votingCardId, authenticationToken);
			if (validationResult.isResult()) {
				secureLoggerWriter.log(Level.INFO,
					new LogContent.LogContentBuilder().logEvent(AuthenticationLogEvents.SUCCESSFUL_AUTH_TOKEN_VALIDATION)
						.objectId(secureLoggerHelper.getAuthTokenHash(authenticationToken)).user(votingCardId)
						.electionEvent(electionEventId)
						.additionalInfo(AuthenticationLogConstants.INFO_TRACK_ID, trackId.getTrackId()).createLogInfo());
			}
			LOG.info(I18N.getMessage("AuthenticationTokenValidationService.validate.resultOfValidation"),
				validationResult.isResult());
			return validationResult;
		} catch (ResourceNotFoundException | CertificateException e) {
			LOG.info(I18N.getMessage("AuthenticationTokenValidationService.validate.resultOfValidation"), false);
			throw e;
		}
	}

	/**
	 * @see com.scytl.products.ov.au.services.domain.service.validation.AuthenticationTokenValidationService#getAuthenticationContent(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public AuthenticationContent getAuthenticationContent(String tenantId, String electionEventId)
			throws ResourceNotFoundException {
		return authenticationTokenValidationService.getAuthenticationContent(tenantId, electionEventId);
	}
}
