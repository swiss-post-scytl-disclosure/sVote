/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.exception;

/**
 * Exception for authentication token signing.
 */
public class AuthenticationTokenSigningException extends AuthenticationException {

	private static final long serialVersionUID = -7431923288549561274L;

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 *
	 * @param message the detail message.
	 * @param cause the cause of the exception.
	 */
	public AuthenticationTokenSigningException(String message, Throwable cause) {
		super(message, cause);
	}
}
