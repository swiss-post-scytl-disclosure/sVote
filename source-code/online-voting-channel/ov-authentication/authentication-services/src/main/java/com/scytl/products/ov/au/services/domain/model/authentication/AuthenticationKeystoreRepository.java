/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import javax.inject.Inject;

import com.scytl.products.ov.au.services.domain.service.authenticationcontent.AuthenticationContentService;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationContent;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.crypto.KeystoreRepository;

/**
 *
 */
public class AuthenticationKeystoreRepository implements KeystoreRepository {


	@Inject
	private AuthenticationContentService authenticationContentService;
	
	/**
	 * @see com.scytl.products.ov.commons.crypto.KeystoreRepository#getJsonByTenantEEID(java.lang.String, java.lang.String)
	 */
	@Override
	public String getJsonByTenantEEID(String tenantId, String electionEventId) throws ResourceNotFoundException {
		
		AuthenticationContent authenticationContent = authenticationContentService.getAuthenticationContent(tenantId, electionEventId);
		
		return authenticationContent.getKeystore().toString();
	}

}
