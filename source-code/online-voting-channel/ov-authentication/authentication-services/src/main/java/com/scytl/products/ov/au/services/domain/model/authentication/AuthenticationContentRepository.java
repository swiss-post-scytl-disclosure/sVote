/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

import javax.ejb.Local;

/**
 * Repository for handling AuthenticationContent entities
 */
@Local
public interface AuthenticationContentRepository extends BaseRepository<AuthenticationContentEntity,Integer> {

    /**
     * Searches for an authentication content with the given tenant, election event.
     *
     * @param tenantId - the identifier of the tenant.
     * @param electionEventId - the identifier of the electionEvent.
     * @return a entity representing the authentication content.
     */
    AuthenticationContentEntity findByTenantIdElectionEventId(String tenantId,String electionEventId) throws ResourceNotFoundException;

}
