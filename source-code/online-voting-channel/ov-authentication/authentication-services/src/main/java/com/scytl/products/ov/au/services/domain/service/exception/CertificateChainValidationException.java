/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.exception;

/**
 * Exception for certificate chain validation.
 */
public class CertificateChainValidationException extends AuthenticationException {

	private static final long serialVersionUID = 3947410817498850571L;

	/**
	 * Constructs a new exception with the specified detail message.
	 *
	 * @param message the detail message.
	 */
	public CertificateChainValidationException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 *
	 * @param message the detail message.
	 * @param cause the cause of the exception.
	 */
	public CertificateChainValidationException(String message, Throwable cause) {
		super(message, cause);
	}
}
