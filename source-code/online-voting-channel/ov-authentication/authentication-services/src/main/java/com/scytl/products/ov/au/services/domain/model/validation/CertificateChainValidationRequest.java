/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.validation;

/**
 * Class to represent a request of Certificate Chain validation.
 */
public class CertificateChainValidationRequest {

    /**
     * The certificate content to be validated.
     */
    private String certificateContent;

    public String getCertificateContent() {
        return certificateContent;
    }

    public void setCertificateContent(String certificateContent) {
        this.certificateContent = certificateContent;
    }
}
