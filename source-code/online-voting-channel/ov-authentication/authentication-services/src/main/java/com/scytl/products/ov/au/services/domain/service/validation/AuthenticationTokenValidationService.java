/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import java.security.cert.CertificateException;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationContent;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.validation.ValidationResult;

/**
 *
 */
public interface AuthenticationTokenValidationService {

	/**
	 * This method validates an authentication token by applying the set of validation rules.
	 *
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @param votingCardId - the voting card identifier.
	 * @param authenticationToken - the token to be validated.
	 * @return an AuthenticationTokenValidationResult containing the result of the validation.
	 */
			ValidationResult validate(String tenantId, String electionEventId, String votingCardId,
									  AuthenticationToken authenticationToken) throws ResourceNotFoundException, CertificateException;

	/**
	 * Returns the authentication content for a given tenant and election event
	 *
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @return an authentication content object
	 */
			AuthenticationContent getAuthenticationContent(String tenantId, String electionEventId)
					throws ResourceNotFoundException;

}
