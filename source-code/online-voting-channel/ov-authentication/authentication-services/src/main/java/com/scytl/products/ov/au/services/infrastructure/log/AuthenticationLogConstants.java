/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.log;

/**
 * Constants for logging in authentication.
 */
public class AuthenticationLogConstants {

	/**
	 * Credential id - additional information.
	 */
	public static final String INFO_CREDENTIAL_ID = "#c_id";

	/**
	 * Error description - additional information.
	 */
	public static final String INFO_ERR_DESC = "#err_desc";

	public static final String TIMESTAMP_AT = "#timestamp_at";

	/**
	 * Track id - additional information.
	 */
	public static final String INFO_TRACK_ID = "#request_id";

	/**
	 * Authentication token id - additional information.
	 */
	public static final String INFO_AUTH_TOKEN_ID = "#at_id";

	/**
	 * Election dates - additional information.
	 */
	public static final String INFO_ELECTION_DATES = "#election_dates";

	
	private AuthenticationLogConstants() {	  
	}
}
