/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.log;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;
import com.scytl.products.oscore.logging.core.formatter.SplunkFormatter;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.commons.logging.constants.MessageFormatterConstants;

/**
 * Message formatter producer for this context.
 */
public class AuthenticationMessageFormatterProducer {

	@Inject
	private TransactionInfoProvider transactionInfoProvider;

	/**
	 * Formatter of logger. Splunk formatter is used here.
	 * 
	 * @return the message formatter.
	 */
	@Produces
	public MessageFormatter getMessageFormatter() {
		return new SplunkFormatter(MessageFormatterConstants.APPLICATION_OV, MessageFormatterConstants.COMPONENT_AC,
			transactionInfoProvider);
	}
}
