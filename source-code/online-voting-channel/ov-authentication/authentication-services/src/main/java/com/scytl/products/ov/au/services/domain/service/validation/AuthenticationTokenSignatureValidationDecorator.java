/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import java.security.cert.CertificateException;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;


import com.scytl.products.ov.commons.beans.exceptions.AuthTokenValidationException;
import com.scytl.products.ov.commons.validation.ValidationResult;
import org.apache.commons.lang3.exception.ExceptionUtils;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.au.services.domain.model.validation.AuthenticationTokenValidation;
import com.scytl.products.ov.au.services.infrastructure.log.AuthenticationLogConstants;
import com.scytl.products.ov.au.services.infrastructure.log.AuthenticationLogEvents;
import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

/**
 * Decorator for authentication token validation.
 */
@Decorator
public abstract class AuthenticationTokenSignatureValidationDecorator implements AuthenticationTokenValidation {

	@Inject
	@Delegate
	private AuthenticationTokenSignatureValidation authenticationTokenSignatureValidation;

	@Inject
	private TrackIdInstance trackId;

	@Inject
	private SecureLoggingWriter secureLoggerWriter;

	@Inject
	private SecureLoggerHelper secureLoggerHelper;

	/**
	 * @see com.scytl.products.ov.au.services.domain.model.validation.AuthenticationTokenValidation#execute(java.lang.String,
	 *      java.lang.String, java.lang.String,
	 *      com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken)
	 */
	@Override
	public ValidationResult execute(String tenantId, String electionEventId, String votingCardId,
									AuthenticationToken authenticationToken) throws CertificateException, ResourceNotFoundException {

		ValidationResult result;
		try {
			result = authenticationTokenSignatureValidation.execute(tenantId, electionEventId, votingCardId,
					authenticationToken);
			return result;
		}catch (AuthTokenValidationException e)	{
				secureLoggerWriter.log(Level.ERROR,
					new LogContent.LogContentBuilder().logEvent(AuthenticationLogEvents.AUTH_TOKEN_SIGNATURE_NOT_VALID)
						.objectId(secureLoggerHelper.getAuthTokenHash(authenticationToken)).user(votingCardId)
						.electionEvent(electionEventId)
						.additionalInfo(AuthenticationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
						.additionalInfo(AuthenticationLogConstants.INFO_AUTH_TOKEN_ID, 
						    secureLoggerHelper.getAuthTokenId(authenticationToken))
						.additionalInfo(AuthenticationLogConstants.INFO_ERR_DESC, "Invalid signature").createLogInfo());
			throw e;

		} catch (ResourceNotFoundException e) {
			secureLoggerWriter.log(Level.ERROR,
				new LogContent.LogContentBuilder().logEvent(AuthenticationLogEvents.CERTIFICATES_NOT_FOUND)
					.objectId(secureLoggerHelper.getAuthTokenHash(authenticationToken)).user(votingCardId)
					.electionEvent(electionEventId)
					.additionalInfo(AuthenticationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
					.additionalInfo(AuthenticationLogConstants.INFO_AUTH_TOKEN_ID, 
					    secureLoggerHelper.getAuthTokenId(authenticationToken))
					.additionalInfo(AuthenticationLogConstants.INFO_ERR_DESC,
						"Auth token signature rule fails. Authentication Certs not found: "
							+ ExceptionUtils.getRootCauseMessage(e))
					.createLogInfo());
			throw e;
		} catch (CertificateException e) {
			secureLoggerWriter.log(Level.ERROR,
				new LogContent.LogContentBuilder().logEvent(AuthenticationLogEvents.AUTH_TOKEN_SIGNATURE_NOT_VALID)
					.objectId(secureLoggerHelper.getAuthTokenHash(authenticationToken)).user(votingCardId)
					.electionEvent(electionEventId)
					.additionalInfo(AuthenticationLogConstants.INFO_TRACK_ID, trackId.getTrackId())
					.additionalInfo(AuthenticationLogConstants.INFO_AUTH_TOKEN_ID, 
					    secureLoggerHelper.getAuthTokenId(authenticationToken))
					.additionalInfo(AuthenticationLogConstants.INFO_ERR_DESC,
						"Auth token signature rule fails. Error converting the certificate: "
							+ ExceptionUtils.getRootCauseMessage(e))
					.createLogInfo());
			throw e;
		}
	}

}
