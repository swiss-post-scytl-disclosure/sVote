/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.challenge;

import java.util.Objects;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.challenge.ChallengeInformation;
import com.scytl.products.ov.au.services.domain.model.validation.ChallengeInformationValidation;
import com.scytl.products.ov.au.services.infrastructure.log.AuthenticationLogConstants;
import com.scytl.products.ov.au.services.infrastructure.log.AuthenticationLogEvents;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.logging.service.I18nLoggerMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

/**
 * Implementation of {@link ChallengeInformationValidation} which checks that
 * credential identifier passed in URI matches one passed in
 * {@link ChallengeInformation}.
 */
public class ChallengeInformationCredentialIdValidation
        implements ChallengeInformationValidation {
    private static final I18nLoggerMessages I18N =
        I18nLoggerMessages.getInstance();


    @Inject
    private Logger logger;

    @Inject
    private SecureLoggingWriter secureLoggerWriter;

    @Inject
    private TrackIdInstance trackId;

    public static final int RULE_ORDER = 0;

    private int order;

    /**
     * Constructor.
     */
    public ChallengeInformationCredentialIdValidation() {
        this.order = RULE_ORDER;
    }

    /**
     * Constructor. For tests only.
     *
     * @param logger
     * @param secureLoggerWriter
     * @param trackId
     */
    ChallengeInformationCredentialIdValidation(final Logger logger,
            final SecureLoggingWriter secureLoggerWriter,
            final TrackIdInstance trackId) {
        this.logger = logger;
        this.secureLoggerWriter = secureLoggerWriter;
        this.trackId = trackId;
        this.order = RULE_ORDER;
    }

    @Override
    public boolean execute(final String tenantId,
            final String electionEventId, final String credentialId,
            final ChallengeInformation challengeInformation)
            throws ResourceNotFoundException {
        boolean valid = Objects.equals(credentialId,
            challengeInformation.getCredentialId());
        if (valid) {
            logger.info(
                I18N.getMessage(
                    "ChallengeInformationCredentialIdValidation.execute.credentialIdOK"),
                tenantId, electionEventId, credentialId);
        } else {
            logger.info(
                I18N.getMessage(
                    "ChallengeInformationCredentialIdValidation.execute.credentialIdInvalid"),
                tenantId, electionEventId, credentialId,
                challengeInformation.getCredentialId());
            LogContent content = new LogContent.LogContentBuilder()
                .logEvent(
                    AuthenticationLogEvents.CLIENT_CHALLENGE_CID_INVALID)
                .objectId(credentialId).user(credentialId)
                .electionEvent(electionEventId)
                .additionalInfo(AuthenticationLogConstants.INFO_TRACK_ID,
                    trackId.getTrackId())
                .additionalInfo(AuthenticationLogConstants.INFO_ERR_DESC,
                    "Invalid credential identifier: "
                        + challengeInformation.getCredentialId())
                .createLogInfo();
            secureLoggerWriter.log(Level.ERROR, content);
        }
        return valid;
    }

    @Override
    public int getOrder() {
        return order;
    }
}
