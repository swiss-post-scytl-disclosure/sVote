/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Enum with the log events for this context.
 */
public enum AuthenticationLogEvents implements LogEvent {

	SUCCESSFUL_SERVER_CHALLENGE_GENERATION("CHREQGEN", "000", "Successful server challenge generation"),
	ERROR_SERVER_CHALLENGE_GENERATION("CHREQGEN", "300", "Error during server challenge generation"),
	CERTIFICATES_NOT_FOUND("CHREQGEN", "301", "Certificates not found"),
	
	CHALLENGE_RESPONSE_VALIDATED("ATREQGEN", "000", "Challenge response validated"),
	CHALLENGE_RESPONSE_NOT_VALIDATED("ATREQGEN", "319", "Challenge response cannot be validated"),
	SERVER_CHALLENGE_EXPIRED("ATREQGEN", "302", "Server challenge has expired"),
	SERVER_CHALLENGE_SIGNATURE_INVALID("ATREQGEN", "303", "Server challenge signature not valid"),
	CLIENT_CHALLENGE_SIGNATURE_INVALID("ATREQGEN", "304", "Client challenge signature not valid"),
	CLIENT_CERTIFICATE_INVALID("ATREQGEN", "305", "Client certificate not valid"),
	CLIENT_CHALLENGE_CID_INVALID("ATREQGEN", "318", "credentialID in the challenge not consistent with that provided"),
	
	SUCCESSFUL_AUTH_TOKEN_GENERATION("ATREQGEN", "000", "Successful authentication token generation"),
	ELECTION_OUT_PERIOD("ATREQGEN", "306", "The election is out of period"),
	ERROR_AUTH_TOKEN_GENERATION("ATREQGEN", "307", "Error generating the authentication token"),
	ERROR_SIGNING_AUTH_TOKEN("ATREQGEN", "308", "Error while signing the auth token"),
	
	SUCCESSFUL_AUTH_TOKEN_VALIDATION("ATVAL", "000", "Successful Authentication Token validation"),
	AUTH_TOKEN_SIGNATURE_NOT_VALID("ATVAL", "309", "Authentication Token signature not valid"),
	AUTH_TOKEN_EEID_NOT_VALID("ATVAL", "310", "electionEventID in the Auth Token not consistent with that provided"),
	AUTH_TOKEN_VCID_NOT_VALID("ATVAL", "311", "votingCardID in the Auth Token not consistent with that provided"),
	AUTH_TOKEN_CID_NOT_VALID("ATVAL", "312", "credentialID in the Auth Token not consistent with that provided"),
	AUTH_TOKEN_VERIFCID_NOT_VALID("ATVAL", "313", "verificationCardID in the Auth Token not consistent with that provided"),
	AUTH_TOKEN_BID_NOT_VALID("ATVAL", "314", "ballotID in the Auth Token not consistent with that provided"),
	AUTH_TOKEN_BBID_NOT_VALID("ATVAL", "315", "ballotBoxID in the Auth Token not consistent with that provided"),
	AUTH_TOKEN_EXPIRED("ATVAL", "316", " Authentication Token has expired"),
	AUTH_TOKEN_TID_NOT_VALID("ATVAL", "317", "tenantID in the Auth Token not consistent with that provided"),

	AUTH_CONTENT_FOUND("CHREQGEN", "400", "Authentication content found."),
	AUTH_CONTENT_NOT_FOUND("CHREQGEN", "403", "Authentication content not found."),
	CREDENTIAL_DATA_FOUND("CHREQGEN", "401", "Credential data found."),
    CERTIFICATES_FOUND("CHREQGEN", "402", "Certificates found"),
    CERTIFICATE_CHAIN_VALIDATION_SUCCESS("CHREQGEN", "403", "Certificate chain validated with success." ),
    CERTIFICATE_CHAIN_VALIDATION_FAILED("CHREQGEN", "404", "Certificate chain failed to validate." ),
    AUTH_CONTENT_SAVED("ACSTOR", "405", "Authentication Content data saved"),
    ERROR_SAVING_AUTH_CONTENT("ACSTOR", "406", "Error saving Authentication Content data"),
    AUTH_CERTS_SAVED("ACERTSTOR", "407", "Authentication Certificates saved"),
    ERROR_SAVING_AUTH_CERTS("ACERTSTOR", "408", "Error saving Authentication Certificates"),
    ADMIN_BOARD_SAVED("ABSTOR", "409", "Administration Board saved successfully"),
    ERROR_SAVING_ADMIN_BOARD("ABSTOR", "410", "Error saving Administration Board"),

	CLIENT_CERTIFICATE_NAME_MISMATCH("ATREQGEN", "411", "Certificate Common Name does not match Credential");

	private final String layer;

	private final String action;

	private final String outcome;

	private final String info;

	AuthenticationLogEvents(final String action, final String outcome, final String info) {
		this.layer = "";
		this.action = action;
		this.outcome = outcome;
		this.info = info;
	}

	/**
	 * @see AuthenticationLogEvents#getAction()
	 */
	@Override
	public String getAction() {
		return action;
	}

	/**
	 * @see AuthenticationLogEvents#getOutcome()
	 */
	@Override
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @see AuthenticationLogEvents#getInfo()
	 */
	@Override
	public String getInfo() {
		return info;
	}

	/**
	 * @see AuthenticationLogEvents#getLayer()
	 */
	@Override
	public String getLayer() {
		return layer;
	}
}
