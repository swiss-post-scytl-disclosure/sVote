/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.tenant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scytl.products.ov.commons.domain.model.tenant.TenantKeystoreEntity;

import javax.persistence.*;

/**
 * @see com.scytl.products.ov.commons.domain.model.tenant.TenantKeystoreEntity
 */
@Entity
@Table(name = "AU_TENANT_KEYSTORES")
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
public class AuTenantKeystoreEntity extends TenantKeystoreEntity {


    /**
     * The identifier for this entity.
     */
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auTenantKeystoreSeq")
    @SequenceGenerator(name = "auTenantKeystoreSeq", sequenceName = "AU_TENANT_KEYSTORES_SEQ")
    @JsonIgnore
    private Long id;


    /**
     * Sets the identifier for this entity..
     *
     * @param id New value of The identifier for this entity..
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets The identifier for this entity..
     *
     * @return Value of The identifier for this entity..
     */
    public Long getId() {
        return id;
    }
}
