/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.validation;

import com.scytl.products.ov.au.services.domain.model.challenge.ChallengeInformation;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;

/**
 * Interface to provide functionalities for challenge information validations.
 */
public interface ChallengeInformationValidation {

    /**
     * Executes a specific validation.
     * 
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @param credentialId
     *            - the credential identifier.
     * @param challengeInformation
     *            - the challenge information to be validated.
     * @return true if the validation succeed. Otherwise, false.
     * @throws ResourceNotFoundException
     * @throws CryptographicOperationException
     */
    boolean execute(String tenantId, String electionEventId, String credentialId,
            ChallengeInformation challengeInformation)
            throws ResourceNotFoundException, CryptographicOperationException;

    int getOrder();
}
