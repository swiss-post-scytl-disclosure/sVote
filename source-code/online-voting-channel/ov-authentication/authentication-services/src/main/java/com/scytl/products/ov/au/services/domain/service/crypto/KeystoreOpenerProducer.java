/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.crypto;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;
import com.scytl.products.ov.commons.crypto.KeystoreForObjectOpener;
import com.scytl.products.ov.commons.crypto.KeystoreForObjectRepository;
import com.scytl.products.ov.commons.crypto.PasswordForObjectRepository;

/**
 *
 */

public class KeystoreOpenerProducer {

	@Inject
	private ScytlKeyStoreServiceAPI storesService;

	@Inject
	private KeystoreForObjectRepository keystoreRepository;

	@Inject
	private PasswordForObjectRepository passwordRepository;

	@Produces
	public KeystoreForObjectOpener getInstance() {

		return new KeystoreForObjectOpener(storesService, keystoreRepository, passwordRepository);
	}
}
