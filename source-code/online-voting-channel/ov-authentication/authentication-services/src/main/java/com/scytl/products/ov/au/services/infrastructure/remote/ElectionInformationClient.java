/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.remote;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.commons.validation.ValidationResult;

/**
 * Defines the methods to access via REST to a set of operations.
 */
public interface ElectionInformationClient {

	/** The Constant PATH_VALIDATIONS. */
	String PATH_VALIDATIONS = "pathValidations";

	/** The Constant BALLOT_BOX_ID. */
	String BALLOT_BOX_ID = "ballotBoxId";

	/** The Constant TENANT_ID. */
	String TENANT_ID = "tenantId";

	/** The Constant ELECTION_EVENT_ID. */
	String ELECTION_EVENT_ID = "electionEventId";

	/**
	 * The endpoint to validate if an election is in dates.
	 *
	 * @param requestId - the request id for logging purposes.
	 * @param pathBallotBoxes the path materials
	 * @param tenantId - the tenant id
	 * @param electionEventId - the election event id
	 * @param ballotBoxId - the ballot box id
	 * @return the validation error for a given tenant, election event and ballot box ids.
	 * @throws ResourceNotFoundException if the rest operation fails.
	 */
	@POST("{pathValidations}/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}")
	Call<ValidationResult> validateElectionInDates(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String requestId,
					@Path(PATH_VALIDATIONS) String pathBallotBoxes, @Path(TENANT_ID) String tenantId,
					@Path(ELECTION_EVENT_ID) String electionEventId, @Path(BALLOT_BOX_ID) String ballotBoxId)
							throws ResourceNotFoundException;
}
