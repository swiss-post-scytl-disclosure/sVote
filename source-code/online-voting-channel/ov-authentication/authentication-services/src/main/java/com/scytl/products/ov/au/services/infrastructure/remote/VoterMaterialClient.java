/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

import com.scytl.products.ov.au.services.domain.model.information.VoterInformation;
import com.scytl.products.ov.au.services.domain.model.material.Credential;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;

/**
 * Defines the methods to access via REST to a set of operations.
 */
public interface VoterMaterialClient {

    /** The Constant PATH_INFORMATIONS. */
    String PATH_INFORMATIONS = "pathInformations";

    /** The Constant PATH_MATERIALS. */
    String PATH_MATERIALS = "pathMaterials";

    /** The Constant CREDENTIAL_ID. */
    String CREDENTIAL_ID = "credentialId";

    /** The Constant TENANT_ID. */
    String TENANT_ID = "tenantId";

    /** The Constant ELECTION_EVENT_ID. */
    String ELECTION_EVENT_ID = "electionEventId";

    /**
     * The endpoint to get credential data from voter material context.
     *
     * @param requestId
     *            - the request id for logging purposes.
     * @param pathMaterials
     *            the path materials
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id
     * @param credentialId
     *            - the credential id
     * @return the credentials for a given tenant, election event and voting
     *         card.
     * @throws ResourceNotFoundException
     *             if the rest operation fails.
     */
    @GET("{pathMaterials}/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}")
    Call<Credential> getCredential(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String requestId,
            @Path(PATH_MATERIALS) String pathMaterials, @Path(TENANT_ID) String tenantId,
            @Path(ELECTION_EVENT_ID) String electionEventId, @Path(CREDENTIAL_ID) String credentialId)
            throws ResourceNotFoundException;

    /**
     * The endpoint to get voter information data from voter material context.
     *
     * @param requestId
     *            - the request id for logging purposes.
     * @param pathInformations
     *            - the path to the correspoding endpoint.
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id
     * @param credentialId
     *            - the credential id
     * @return the voter information for a given tenant, election event and
     *         voting card.
     * @throws ResourceNotFoundException
     *             if the rest operation fails.
     */
    @GET("{pathInformations}/tenant/{tenantId}/electionevent/{electionEventId}/credential/{credentialId}")
    Call<VoterInformation> getVoterInformation(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String requestId,
            @Path(PATH_INFORMATIONS) String pathInformations, @Path(TENANT_ID) String tenantId,
            @Path(ELECTION_EVENT_ID) String electionEventId, @Path(CREDENTIAL_ID) String credentialId)
            throws ResourceNotFoundException;
}
