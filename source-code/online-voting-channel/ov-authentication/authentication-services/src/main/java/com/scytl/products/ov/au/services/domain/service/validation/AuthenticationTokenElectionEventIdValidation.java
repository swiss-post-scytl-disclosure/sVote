/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.AuthTokenValidationException;
import com.scytl.products.ov.au.services.domain.model.validation.AuthenticationTokenValidation;
import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.validation.ValidationResult;

/**
 * This class implements the election event id validation of the authentication token.
 */
public class AuthenticationTokenElectionEventIdValidation implements AuthenticationTokenValidation {

	/**
	 * This method implements the validation of token election event id. That is, The election event id inside the
	 * authentication token is the same that the ones received in the input parameter.
	 * 
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @param votingCardId - the voting card identifier.
	 * @param authenticationToken - the authentication token to be validated.
	 * @return validationResult, if successfully validated. Otherwise, a exception is thrown.
	 */
	@Override
	public ValidationResult execute(String tenantId, String electionEventId, String votingCardId,
									AuthenticationToken authenticationToken) {
		// validation result
		if (!authenticationToken.getVoterInformation().getElectionEventId().equals(electionEventId)){
			throw new AuthTokenValidationException(ValidationErrorType.INVALID_EEID);
		}
		return new ValidationResult(true);
	}
}
