/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import com.scytl.products.ov.au.services.domain.service.exception.AuthenticationTokenGenerationException;
import com.scytl.products.ov.au.services.domain.service.exception.AuthenticationTokenSigningException;

public interface AuthenticationTokenFactory {

    /**
     * Builds an authentication token.
     * 
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @param credentialId
     *            - the credential identifier.
     * @return an authentication token initialized according with the parameters.
     * @throws AuthenticationTokenGenerationException
     * @throws AuthenticationTokenSigningException
     */
    AuthenticationTokenMessage buildAuthenticationToken(String tenantId, String electionEventId, String credentialId)
            throws AuthenticationTokenGenerationException, AuthenticationTokenSigningException;

}
