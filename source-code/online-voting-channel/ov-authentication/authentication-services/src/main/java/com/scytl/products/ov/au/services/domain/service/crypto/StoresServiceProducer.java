/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.crypto;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.scytl.keystore.ScytlKeyStoreServiceAPI;
import com.scytl.cryptolib.scytl.keystore.service.ScytlKeyStoreServiceFactoryHelper;

/**
 * Producer for scytl keystore store service.
 */
public class StoresServiceProducer {

    private static final String MAX_ELEMENTS_CRYPTO_POOL = "stores.max.elements.crypto.pool";

    /**
     * Returns a scytl keystore store service.
     * 
     * @return a scytl keystore store service instance.
     */
    @Produces
    @ApplicationScoped
    public ScytlKeyStoreServiceAPI getInstance() {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(Integer.parseInt(System.getProperty(MAX_ELEMENTS_CRYPTO_POOL)));
        try {
            return ScytlKeyStoreServiceFactoryHelper.getFactoryOfThreadSafeServices(config).create();
        } catch (GeneralCryptoLibException e) {
            throw new IllegalStateException("Exception while trying to create a ScytlKeyStoreService", e);
        }
    }
}
