/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.validation;

import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.validation.ValidationResult;

import java.security.cert.CertificateException;

/**
 * Interface to provide functionalities for authentication token validations.
 */
public interface AuthenticationTokenValidation {

	/**
	 * Executes a specific validation.
	 * 
	 * @param tenantId - the tenant identifier.
	 * @param electionEventId - the election event identifier.
	 * @param votingCardId - the voting card identifier.
	 * @param authenticationToken - the token to be validated.
	 * @return true if the validation succeed. Otherwise, false.
	 * @throws ResourceNotFoundException
	 */
	ValidationResult execute(String tenantId, String electionEventId, String votingCardId, AuthenticationToken authenticationToken)
			throws ResourceNotFoundException,CertificateException;
}
