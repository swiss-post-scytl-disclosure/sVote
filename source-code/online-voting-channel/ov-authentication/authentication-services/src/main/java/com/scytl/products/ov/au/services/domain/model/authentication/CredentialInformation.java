/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import com.scytl.products.ov.au.services.domain.model.challenge.ServerChallengeMessage;
import com.scytl.products.ov.au.services.domain.model.material.Credential;

/**
 * Class representing an credential information.
 */
public class CredentialInformation {

	// the credential data
	private final Credential credentialData;

	// the server challenge message
	private final ServerChallengeMessage serverChallengeMessage;

	// the authentication token certificate
	private final String certificates;

	private final String certificatesSignature;

	/**
	 * Constructs a new CredentialInformation for the given parameters.
	 *
	 * @param credentialData - the credential data.
	 * @param serverChallengeMessage - the server challenge
	 * @param certificates - the certificates to verify the sign
	 * @param certificatesSignature - the signature of the certificates
	 */
	public CredentialInformation(Credential credentialData, ServerChallengeMessage serverChallengeMessage,
			String certificates, String certificatesSignature) {
		this.credentialData = credentialData;
		this.serverChallengeMessage = serverChallengeMessage;
		this.certificates = certificates;
		this.certificatesSignature = certificatesSignature;
	}

	/**
	 * Return the value of field credential data
	 *
	 * @return
	 */
	public Credential getCredentialData() {
		return credentialData;
	}

	/**
	 * Return the value of the field server challenge message
	 *
	 * @return
	 */
	public ServerChallengeMessage getServerChallengeMessage() {
		return serverChallengeMessage;
	}

	/**
	 * Returns the value of field certificates
	 * 
	 * @return
	 */
	public String getCertificates() {
		return certificates;
	}

	/**
	 * Returns the current value of the field certificatesSignature.
	 *
	 * @return Returns the certificatesSignature.
	 */
	public String getCertificatesSignature() {
		return certificatesSignature;
	}
}
