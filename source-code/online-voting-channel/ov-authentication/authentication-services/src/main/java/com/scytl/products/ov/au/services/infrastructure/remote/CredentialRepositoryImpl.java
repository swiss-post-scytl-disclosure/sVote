/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.au.services.domain.model.material.Credential;
import com.scytl.products.ov.au.services.domain.model.material.CredentialRepository;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.logging.service.I18nLoggerMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;

/**
 * The implementation of the operations on the credential repository.
 */
@Stateless(name = "au-CredentialRepositoryImpl")
public class CredentialRepositoryImpl implements CredentialRepository {

	private static final Logger LOG = LoggerFactory.getLogger("std");
	
    private static final I18nLoggerMessages I18N = I18nLoggerMessages.getInstance();

    // The name of the property path credentials.
    private static final String PROPERTY_PATH_CREDENTIAL = "path.credentials";

    // The value of the property path materials.
    private static final String PATH_MATERIALS =
        PropertiesFileReader.getInstance().getPropertyValue(PROPERTY_PATH_CREDENTIAL);

    @Inject
    private TrackIdInstance trackId;

    private VoterMaterialClient voterMaterialClient;

    @Inject
    CredentialRepositoryImpl(final VoterMaterialClient voterMaterialClient) {
        this.voterMaterialClient = voterMaterialClient;
    }

    /**
     * Searches for voter credential data identified by the given tenant
     * identifier, credential identifier and election event identifier. The
     * implementation is a REST Client invoking to an get operation of a REST
     * web service to obtain the credential data for the given parameters. If
     * the web service returns a HTTP NOT FOUND status (404), then the method
     * throws a ResourceNotFoundException exception.
     * 
     * @param tenantId
     *            - the identifier of the tenant.
     * @param electionEventId
     *            - the identifier of the election event.
     * @param credentialId
     *            - the identifier of the credential.
     * @return a Credential object containing credential data of a voter.
     * @throws ResourceNotFoundException
     *             if no voter material is found.
     */
    @Override
    public Credential findByTenantIdElectionEventIdCredentialId(String tenantId, String electionEventId,
            String credentialId) throws ResourceNotFoundException {
        LOG.info(
            I18N.getMessage("CredentialRepositoryImpl.findByTenantIdElectionEventIdVotingCardId.findingCredential"),
            tenantId, electionEventId, credentialId);

        try {
            Credential credential = RetrofitConsumer.processResponse(voterMaterialClient.getCredential(trackId.getTrackId(), PATH_MATERIALS, tenantId,
                electionEventId, credentialId));
            LOG.info(
                I18N.getMessage("CredentialRepositoryImpl.findByTenantIdElectionEventIdVotingCardId.credentialFound"),
                tenantId, electionEventId, credentialId);
            return credential;
        } catch (RetrofitException e) {
            throw e;
        } catch (ResourceNotFoundException e) {
            LOG.error(
                I18N.getMessage(
                    "CredentialRepositoryImpl.findByTenantIdElectionEventIdVotingCardId.credentialNotFound"),
                tenantId, electionEventId, credentialId);
            throw e;
        }
    }
}
