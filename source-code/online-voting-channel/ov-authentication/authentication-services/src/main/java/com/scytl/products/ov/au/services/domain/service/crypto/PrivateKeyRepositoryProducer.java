/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.crypto;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.scytl.products.ov.commons.crypto.KeystoreForObjectOpener;
import com.scytl.products.ov.commons.crypto.PasswordForObjectRepository;
import com.scytl.products.ov.commons.crypto.PrivateKeyForObjectRepository;

/**
 *
 */

public class PrivateKeyRepositoryProducer {

	@Inject
	private KeystoreForObjectOpener keystoreOpener;
	
	@Inject
	private PasswordForObjectRepository passwordRepository;

	@Produces
	public PrivateKeyForObjectRepository getInstance(){
		
		return new PrivateKeyForObjectRepository(keystoreOpener, passwordRepository);
	}
}
