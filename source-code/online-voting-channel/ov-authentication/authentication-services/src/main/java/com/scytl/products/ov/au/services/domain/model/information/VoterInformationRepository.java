/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.information;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

/**
 * Interface defining the operations for handling voter information.
 */
@Local
public interface VoterInformationRepository {

    /**
     * Returns a voter information for a given tenant, election event and
     * credential identifier.
     *
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @param credentialId
     *            - the credential identifier.
     * @return The voter information.
     * @throws ResourceNotFoundException
     *             if no voter information is found.
     */
    VoterInformation findByTenantIdElectionEventIdCredentialId(String tenantId, String electionEventId,
            String credentialId) throws ResourceNotFoundException;

}
