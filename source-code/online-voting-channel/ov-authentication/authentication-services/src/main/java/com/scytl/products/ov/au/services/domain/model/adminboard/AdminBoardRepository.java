/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.adminboard;

import javax.ejb.Local;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.domain.model.BaseRepository;

/**
 * Repository for handling ADMIN_BOARD entities
 */
@Local
public interface AdminBoardRepository extends BaseRepository<AdminBoard,Integer> {

    /**
     * Searches for the authentication certs for  the given tenant, election event.
     *
     * @param tenantId - the identifier of the tenant.
     * @param electionEventId - the identifier of the electionEvent.
     * @return a entity representing the authentication certs.
     */
    AdminBoard findByTenantIdElectionEventId(String tenantId,String electionEventId) throws ResourceNotFoundException;



}
