/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.scytl.products.ov.au.services.domain.model.information.VoterInformation;
import com.scytl.products.ov.au.services.domain.model.information.VoterInformationRepository;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.logging.service.I18nLoggerMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

/**
 * Implementation of the voter informationRepository through a web service
 * client.
 */
@Stateless(name = "au-VoterInformationRepositoryImpl")
public class VoterInformationRepositoryImpl implements VoterInformationRepository {

	private static final Logger LOG = LoggerFactory.getLogger("std");

    private static final I18nLoggerMessages I18N = I18nLoggerMessages.getInstance();

    // The name of the property path informations.
    private static final String PROPERTY_PATH_INFORMATIONS = "path.informations";

    // The value of the property path informations.
    private static final String PATH_INFORMATIONS =
        PropertiesFileReader.getInstance().getPropertyValue(PROPERTY_PATH_INFORMATIONS);

    @Inject
    private TrackIdInstance trackId;

    private VoterMaterialClient voterMaterialClient;

    @Inject
    VoterInformationRepositoryImpl(final VoterMaterialClient voterMaterialClient) {
        this.voterMaterialClient = voterMaterialClient;
    }

    /**
     * Returns a voter information for a given tenant, election event and
     * credential. The implementation is a REST Client invoking to an get
     * operation of a REST web service to obtain the voter information for the
     * given parameters. If the web service returns a HTTP NOT FOUND status
     * (404), then the method throws a ResourceNotFoundException exception.
     * 
     * @param tenantId
     *            - the tenant identifier.
     * @param electionEventId
     *            - the election event identifier.
     * @param credentialId
     *            - the credential identifier.
     * @return The voter information.
     * @throws ResourceNotFoundException
     *             if no voter information is found.
     */
    @Override
    public VoterInformation findByTenantIdElectionEventIdCredentialId(String tenantId, String electionEventId,
            String credentialId) throws ResourceNotFoundException {
        LOG.info(
            I18N.getMessage(
                "VoterInformationRepositoryImpl.findByTenantIdElectionEventIdVotingCardId.findingVoterInformation"),
            tenantId, electionEventId, credentialId);

        try {
            VoterInformation voterInformation = RetrofitConsumer.processResponse(voterMaterialClient.getVoterInformation(trackId.getTrackId(),
                PATH_INFORMATIONS, tenantId, electionEventId, credentialId));
            LOG.info(
                I18N.getMessage(
                    "VoterInformationRepositoryImpl.findByTenantIdElectionEventIdVotingCardId.voterInformationFound"),
                tenantId, electionEventId, credentialId);
            return voterInformation;
        } catch (ResourceNotFoundException e) {
            LOG.error(
                I18N.getMessage(
                    "VoterInformationRepositoryImpl.findByTenantIdElectionEventIdVotingCardId.voterInformationNotFound"),
                tenantId, electionEventId, credentialId);
            throw e;
        }
    }
}
