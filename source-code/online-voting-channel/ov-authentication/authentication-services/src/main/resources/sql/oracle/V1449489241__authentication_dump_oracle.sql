--------------------------------------------------------
--  File created - Tuesday-July-14-2015   
--------------------------------------------------------
-- DROP TABLE "AUTHENTICATION_CERTS";
-- DROP TABLE "AUTHENTICATION_CONTENT";
-- DROP SEQUENCE "AUTHENTICATION_CERTS_SEQ";
-- DROP SEQUENCE "AUTHENTICATION_CONTENT_SEQ";
--------------------------------------------------------
--  DDL for Sequence AUTHENTICATION_CERTS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTHENTICATION_CERTS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence AUTHENTICATION_CONTENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTHENTICATION_CONTENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table AUTHENTICATION_CERTS
--------------------------------------------------------

  CREATE TABLE "AUTHENTICATION_CERTS" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 BYTE), 
	"ELECTION_EVENT_ID" VARCHAR2(100 BYTE), 
	"JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table AUTHENTICATION_CONTENT
--------------------------------------------------------

  CREATE TABLE "AUTHENTICATION_CONTENT" 
   (	"ID" NUMBER(11,0), 
	"TENANT_ID" VARCHAR2(100 BYTE), 
	"ELECTION_EVENT_ID" VARCHAR2(100 BYTE), 
	"JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Index SYS_C007050
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C007050" ON "AUTHENTICATION_CERTS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTHENTICATION_CERTS_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTHENTICATION_CERTS_UK1" ON "AUTHENTICATION_CERTS" ("TENANT_ID", "ELECTION_EVENT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C007056
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C007056" ON "AUTHENTICATION_CONTENT" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTHENTICATION_CONTENT_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTHENTICATION_CONTENT_UK1" ON "AUTHENTICATION_CONTENT" ("TENANT_ID", "ELECTION_EVENT_ID") 
  ;
--------------------------------------------------------
--  Constraints for Table AUTHENTICATION_CERTS
--------------------------------------------------------

  ALTER TABLE "AUTHENTICATION_CERTS" ADD CONSTRAINT "AUTHENTICATION_CERTS_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID") ENABLE;
  ALTER TABLE "AUTHENTICATION_CERTS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "AUTHENTICATION_CERTS" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "AUTHENTICATION_CERTS" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTHENTICATION_CERTS" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTHENTICATION_CERTS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AUTHENTICATION_CONTENT
--------------------------------------------------------

  ALTER TABLE "AUTHENTICATION_CONTENT" ADD CONSTRAINT "AUTHENTICATION_CONTENT_UK1" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID") ENABLE;
  ALTER TABLE "AUTHENTICATION_CONTENT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "AUTHENTICATION_CONTENT" MODIFY ("JSON" NOT NULL ENABLE);
  ALTER TABLE "AUTHENTICATION_CONTENT" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTHENTICATION_CONTENT" MODIFY ("TENANT_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTHENTICATION_CONTENT" MODIFY ("ID" NOT NULL ENABLE);

  
-- Signature
ALTER TABLE AUTHENTICATION_CERTS ADD ("SIGNATURE" CLOB);
--ALTER TABLE "AUTHENTICATION_CERTS" MODIFY ("SIGNATURE" NOT NULL ENABLE);