--------------------------------------------------------
--  DDL for Sequence AUTHENTICATION_CERTS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AU_ADMIN_BOARDS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;



    CREATE TABLE "AU_ADMIN_BOARDS"
      (	"ID" NUMBER(11,0),
   	    "TENANT_ID" VARCHAR2(100 BYTE),
   	    "ELECTION_EVENT_ID" VARCHAR2(100 BYTE),
   	    "ADMIN_BOARD_ID" VARCHAR2(100 BYTE)
      ) ;

    -------------------------------------------------------
    --  DDL for Index AUTHENTICATION_CERTS_UK1
    -------------------------------------------------------

   CREATE UNIQUE INDEX "AU_ADMIN_BOARDS_UK" ON "AU_ADMIN_BOARDS" ("TENANT_ID", "ELECTION_EVENT_ID","ADMIN_BOARD_ID");


    --------------------------------------------------------
    --  Constraints for Table AUTHENTICATION_CERTS
    --------------------------------------------------------

    ALTER TABLE "AU_ADMIN_BOARDS" ADD CONSTRAINT "AU_ADMIN_BOARDS_UK" UNIQUE ("TENANT_ID", "ELECTION_EVENT_ID","ADMIN_BOARD_ID") ENABLE;
    ALTER TABLE "AU_ADMIN_BOARDS" ADD PRIMARY KEY ("ID") ENABLE;
    ALTER TABLE "AU_ADMIN_BOARDS" MODIFY ("ADMIN_BOARD_ID" NOT NULL ENABLE);
    ALTER TABLE "AU_ADMIN_BOARDS" MODIFY ("ELECTION_EVENT_ID" NOT NULL ENABLE);
    ALTER TABLE "AU_ADMIN_BOARDS" MODIFY ("TENANT_ID" NOT NULL ENABLE);
    ALTER TABLE "AU_ADMIN_BOARDS" MODIFY ("ID" NOT NULL ENABLE);