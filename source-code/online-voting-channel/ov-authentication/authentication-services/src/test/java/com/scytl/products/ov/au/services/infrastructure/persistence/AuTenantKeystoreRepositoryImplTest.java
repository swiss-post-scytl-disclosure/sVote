/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.persistence;

import com.scytl.products.ov.au.services.domain.model.tenant.AuTenantKeystoreEntity;
import com.scytl.products.ov.commons.domain.model.tenant.TenantKeystoreEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuTenantKeystoreRepositoryImplTest {



    public static final String KEY_TYPE = "ENCRYPT";
    public static final String TENANT_ID = "100";
    @InjectMocks
    private AuTenantKeystoreRepositoryImpl sut = new AuTenantKeystoreRepositoryImpl();

    @Mock
    private EntityManager managerMock;


    @Mock
    private AuTenantKeystoreEntity auTenantKeystoreEntityMock;

    @Mock
    private TypedQuery<AuTenantKeystoreEntity> typedQueryMock;


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this.getClass());
    }


    @Test
    public void checkIfKeystoreExists(){


        when(managerMock.createQuery(anyString(), eq(AuTenantKeystoreEntity.class))).thenReturn(typedQueryMock);
        List<AuTenantKeystoreEntity> list = Arrays.asList(new AuTenantKeystoreEntity());
        when(typedQueryMock.getResultList()).thenReturn(list);
        assertTrue(sut.checkIfKeystoreExists(TENANT_ID, KEY_TYPE));


    }


    @Test
    public void checkIfKeystoreExistsReturnFalse(){

        when(managerMock.createQuery(anyString(), eq(AuTenantKeystoreEntity.class))).thenReturn(typedQueryMock);
        when(typedQueryMock.getResultList()).thenReturn(new ArrayList<>());
        assertFalse(sut.checkIfKeystoreExists(TENANT_ID, KEY_TYPE));


    }

    @Test
    public void getLoggingKeystore(){

        when(managerMock.createQuery(anyString(), eq(AuTenantKeystoreEntity.class))).thenReturn(typedQueryMock);
        List<AuTenantKeystoreEntity> list = Arrays.asList(auTenantKeystoreEntityMock);
        when(auTenantKeystoreEntityMock.getKeyType()).thenReturn(KEY_TYPE);
        when(typedQueryMock.getResultList()).thenReturn(list);
        final TenantKeystoreEntity byTenantAndType = sut.getByTenantAndType(TENANT_ID, KEY_TYPE);
        assertThat(byTenantAndType, notNullValue());
        assertThat(byTenantAndType.getKeyType(), is(KEY_TYPE));

    }
}
