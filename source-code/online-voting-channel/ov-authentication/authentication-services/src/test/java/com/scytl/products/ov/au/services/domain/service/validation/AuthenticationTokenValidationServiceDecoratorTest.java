/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;

import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.validation.ValidationResult;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.security.cert.CertificateException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Test class for the decorator
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenValidationServiceDecoratorTest {

    public static final String TENANT_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String VOTING_CARD_ID = "100";
    public static final String TRACK_ID = "trackId";

    @Mock
    private AuthenticationTokenValidationService authenticationTokenValidationService;

    @Mock
    private AuthenticationToken token;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private Logger logger;

    @Mock
    private TrackIdInstance trackIdInstance;

    @Mock
    private SecureLoggingWriter secureLoggingWriter;



    @Mock
    private SecureLoggerHelper secureLoggerHelper;

    @InjectMocks
    private AuthenticationTokenValidationServiceDecorator sut = new AuthenticationTokenValidationServiceDecorator();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());
        when(trackIdInstance.getTrackId()).thenReturn(TRACK_ID);

    }

    @Test
    public void validate() throws ResourceNotFoundException, CertificateException {

        when(authenticationTokenValidationService.validate(anyString(), anyString(), any(),
            any(AuthenticationToken.class))).thenReturn(new ValidationResult(true));
        assertTrue(sut.validate(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, token).isResult());

    }

    @Test
    public void validateException() throws ResourceNotFoundException, CertificateException {

        expectedException.expect(CertificateException.class);
        when(authenticationTokenValidationService.validate(anyString(), anyString(), any(),
                any(AuthenticationToken.class))).thenThrow(new CertificateException("exception"));
        assertFalse(sut.validate(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, token).isResult());

    }

    @Test
    public void getAuthenticationContent() throws ResourceNotFoundException {

        expectedException.expect(ResourceNotFoundException.class);
        when(authenticationTokenValidationService.getAuthenticationContent(anyString(), anyString()))
        .thenThrow(new ResourceNotFoundException("exception"));
        sut.getAuthenticationContent(TENANT_ID, ELECTION_EVENT_ID);
    }

}
