/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.log;


import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import com.scytl.products.oscore.logging.api.formatter.MessageFormatter;

public class AuthenticationMessageFormatterProducerTest {

    @Test
    public void testCreate(){

        AuthenticationMessageFormatterProducer producer = new AuthenticationMessageFormatterProducer();
        final MessageFormatter messageFormatter = producer.getMessageFormatter();

        assertThat(messageFormatter, notNullValue());

    }



}
