/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.persistence;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import com.scytl.products.ov.au.services.domain.model.platform.AuLoggingKeystoreEntity;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreEntity;

@RunWith(MockitoJUnitRunner.class)
public class AuLoggingKesytoreRepositoryTest {


    public static final String KEY_TYPE = "SIGN";
    @InjectMocks
    private AuLogginKeystoreRepositoryImpl sut = new AuLogginKeystoreRepositoryImpl();

    @Mock
    private EntityManager managerMock;


    @Mock
    private AuLoggingKeystoreEntity auLoggingKeystoreEntityMock;

    @Mock
    private TypedQuery<AuLoggingKeystoreEntity> typedQueryMock;


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this.getClass());
    }


    @Test
    public void checkIfKeystoreExists(){


        when(managerMock.createQuery(anyString(), eq(AuLoggingKeystoreEntity.class))).thenReturn(typedQueryMock);
        List<AuLoggingKeystoreEntity> list = Arrays.asList(new AuLoggingKeystoreEntity());
        when(typedQueryMock.getResultList()).thenReturn(list);
        assertTrue(sut.checkIfKeystoreExists(KEY_TYPE));


    }


    @Test
    public void checkIfKeystoreExistsReturnFalse(){

        when(managerMock.createQuery(anyString(), eq(AuLoggingKeystoreEntity.class))).thenReturn(typedQueryMock);
        when(typedQueryMock.getResultList()).thenReturn(new ArrayList<>());
        assertFalse(sut.checkIfKeystoreExists(KEY_TYPE));


    }

    @Test
    public void getLoggingKeystore(){

        when(managerMock.createQuery(anyString(), eq(AuLoggingKeystoreEntity.class))).thenReturn(typedQueryMock);
        List<AuLoggingKeystoreEntity> list = Arrays.asList(auLoggingKeystoreEntityMock);
        when(auLoggingKeystoreEntityMock.getKeyType()).thenReturn(KEY_TYPE);
        when(typedQueryMock.getResultList()).thenReturn(list);
        final LoggingKeystoreEntity loggingKeystoreByType = sut.getLoggingKeystoreByType(KEY_TYPE);
        assertThat(loggingKeystoreByType, notNullValue());
        assertThat(loggingKeystoreByType.getKeyType(), is(KEY_TYPE));

    }

}
