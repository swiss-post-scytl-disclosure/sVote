/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import com.scytl.products.ov.au.services.domain.service.authenticationcontent.AuthenticationContentService;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationContent;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.util.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.json.JsonObject;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationKeystoreRepositoryTest {


    public static final String ELECTION_EVENT_ID = "100";
    public static final String TENANT_ID = "100";

    @InjectMocks
    private AuthenticationKeystoreRepository authenticationKeystoreRepository = new AuthenticationKeystoreRepository();

    @Mock
    private AuthenticationContentService authenticationContentService ;

    @Mock
    private AuthenticationContent authenticationContent;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this.getClass());
    }

    @Test
    public void getAuthenticationKeystore() throws ResourceNotFoundException {


        final JsonObject jsonObject = JsonUtils.getJsonObject("{}");
        when(authenticationContentService.getAuthenticationContent(anyString(), anyString())).thenReturn(authenticationContent);
        when(authenticationContent.getKeystore()).thenReturn(jsonObject);
        final String jsonByTenantEEID = authenticationKeystoreRepository.getJsonByTenantEEID(TENANT_ID, ELECTION_EVENT_ID);
        Assert.assertNotNull(jsonByTenantEEID);
    }
}
