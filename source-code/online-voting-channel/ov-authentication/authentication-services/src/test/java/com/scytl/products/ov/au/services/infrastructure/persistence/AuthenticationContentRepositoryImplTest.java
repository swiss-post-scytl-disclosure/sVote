/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationContentEntity;
import com.scytl.products.ov.au.services.infrastructure.persistence.AuthenticationContentRepositoryImpl;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImplTest;

/**
 * Test class for AuthenticationContentRepository
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationContentRepositoryImplTest extends BaseRepositoryImplTest<AuthenticationContentEntity, Integer> {

	@Mock
	private TypedQuery<AuthenticationContentEntity> queryMock;

	@InjectMocks
	private static AuthenticationContentRepositoryImpl authenticationContentRepository =
		new AuthenticationContentRepositoryImpl();

	/**
	 * Creates a new object of the testing class.
	 */
	public AuthenticationContentRepositoryImplTest() throws InstantiationException, IllegalAccessException {
		super(AuthenticationContentEntity.class, authenticationContentRepository.getClass());
	}

	@Test
	public void findByTenantIdElectionEventId() throws ResourceNotFoundException {
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(AuthenticationContentEntity.class))).thenReturn(
			queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenReturn(new AuthenticationContentEntity());

		String tenantId = "2";
		String electionEventId = "2";
		assertNotNull(authenticationContentRepository.findByTenantIdElectionEventId(tenantId, electionEventId));
	}

	@Test
	public void findByTenantIdElectionEventIdNotFound() throws ResourceNotFoundException {
		when(entityManagerMock.createQuery(Matchers.anyString(), eq(AuthenticationContentEntity.class))).thenReturn(
			queryMock);
		when(queryMock.setParameter(Matchers.anyString(), Matchers.anyString())).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenThrow(new NoResultException());

		expectedException.expect(ResourceNotFoundException.class);

		String tenantId = "2";
		String electionEventId = "2";
		authenticationContentRepository.findByTenantIdElectionEventId(tenantId, electionEventId);
	}

}
