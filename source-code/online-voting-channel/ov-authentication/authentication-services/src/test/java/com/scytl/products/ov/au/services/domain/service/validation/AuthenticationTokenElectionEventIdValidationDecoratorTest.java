/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.security.cert.CertificateException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.au.services.domain.utils.BeanUtils;
import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.AuthTokenValidationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.validation.ValidationResult;

/**
 * Test class for the decorator of the validation
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenElectionEventIdValidationDecoratorTest {

    public static final String TENANT_ID = "100";
    public static final String ELECTION_EVENT_ID = "100";
    public static final String VOTING_CARD_ID = "100";

    @Mock
    private AuthenticationTokenElectionEventIdValidation authenticationTokenTenantIdValidationMock;

    @Mock
    private SecureLoggingWriter secureLoggingWriter;

    @Mock
    private SecureLoggerHelper helper;

    @Mock
    private Logger logger;


    @Mock
    private TrackIdInstance trackIdInstance;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());
    }

    @InjectMocks
    private AuthenticationTokenElectionEventIdValidationDecorator sut =
        new AuthenticationTokenElectionEventIdValidationDecorator() {
            @Override
            public ValidationResult execute(String tenantId, String electionEventId, String votingCardId,
                    AuthenticationToken authenticationToken) throws ResourceNotFoundException, CertificateException {
                return super.execute(tenantId, electionEventId, votingCardId, authenticationToken);
            }
        };

    @Test
    public void testValidation() throws ResourceNotFoundException, CertificateException {

        when(authenticationTokenTenantIdValidationMock.execute(anyString(), anyString(), anyString(),
            any(AuthenticationToken.class))).thenReturn(new ValidationResult(true));
        final ValidationResult execute = sut.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, new AuthenticationToken());
        assertTrue(execute.isResult());

    }

    @Test
    public void testValidationFails() throws ResourceNotFoundException, CertificateException {

        expectedException.expect(AuthTokenValidationException.class);
        final AuthenticationToken authenticationToken = BeanUtils.createAuthenticationToken();
        when(trackIdInstance.getTrackId()).thenReturn("trackId");
        when(authenticationTokenTenantIdValidationMock.execute(anyString(), anyString(), anyString(),
                any(AuthenticationToken.class))).thenThrow(new AuthTokenValidationException(null));
        sut.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, authenticationToken);


    }

}
