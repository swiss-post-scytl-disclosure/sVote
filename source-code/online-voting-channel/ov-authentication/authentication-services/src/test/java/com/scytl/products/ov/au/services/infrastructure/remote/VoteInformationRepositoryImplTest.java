/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.remote;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.io.IOException;

import com.scytl.products.ov.au.services.domain.model.information.VoterInformation;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

import retrofit2.Call;


/**
 * Test class for
 */
@RunWith(MockitoJUnitRunner.class)
public class VoteInformationRepositoryImplTest {

    private static final String TENANT_ID = "100";

    private static final String CREDENTIAL_ID = "100";

    private static final String ELECTION_EVENT_ID = "100";

    @Mock
    private TrackIdInstance trackIdMock;

    @Mock
    private VoterMaterialClient voterMaterialClient;

    @Mock
    private Logger logger;

    @Mock
    private VoterInformation voterInformationMock;

    @InjectMocks
    private VoterInformationRepositoryImpl voterInformationRepository = new VoterInformationRepositoryImpl(voterMaterialClient);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void findByTenantIdElectionEventIdCredentialId()
            throws ResourceNotFoundException, IOException {

        @SuppressWarnings("unchecked")
		Call<VoterInformation> callMock = (Call<VoterInformation>) Mockito.mock(Call.class);
        when(callMock.execute()).thenReturn(retrofit2.Response.success(voterInformationMock));

        when(voterMaterialClient.getVoterInformation(anyString(), anyString(), anyString(), anyString(), anyString()))
            .thenReturn(callMock);
        final VoterInformation byTenantIdElectionEventIdCredentialId = voterInformationRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID,
                CREDENTIAL_ID);
        assertNotNull(byTenantIdElectionEventIdCredentialId);
    }

    @Test
    public void findByTenantIdElectionEventIdCredentialIdThrowResourceNotFoundException()
            throws ResourceNotFoundException {

        expectedException.expect(ResourceNotFoundException.class);
        doThrow(ResourceNotFoundException.class)
            .when(voterMaterialClient).getVoterInformation(anyString(), anyString(), anyString(), anyString(), anyString());

        voterInformationRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID,
                CREDENTIAL_ID);
    }

}
