/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationCerts;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationCertsRepository;
import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationCersRepositoryDecoratorTest {


    public static final String TENANT_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String TRACK_ID = "trackId";

    @Mock
    private SecureLoggingWriter secureLoggingWriter;

    @Mock
    private SecureLoggerHelper helper;

    @Mock
    private Logger logger;

    @Mock
    private TrackIdInstance trackIdInstance;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private AuthenticationCerts authenticationCertsMock;

    @Mock
    private AuthenticationCertsRepository authenticationCertsRepository;

    @Before
    public void init() {

        MockitoAnnotations.initMocks(AuthenticationCersRepositoryDecoratorTest.class);
    }

    @InjectMocks
    private AuthenticationCertsRepositoryDecorator sut = new AuthenticationCertsRepositoryDecorator() {
        @Override
        public AuthenticationCerts findByTenantIdElectionEventId(String tenantId, String electionEventId)
                throws ResourceNotFoundException {
            return authenticationCertsRepository.findByTenantIdElectionEventId(tenantId, electionEventId);
        }

        @Override
        public AuthenticationCerts find(Integer integer) {
            return authenticationCertsRepository.find(integer);
        }

        @Override
        public AuthenticationCerts update(AuthenticationCerts entity) throws EntryPersistenceException {
            return authenticationCertsRepository.update(entity);
        }

        @Override
        public AuthenticationCerts save(final AuthenticationCerts entity) throws DuplicateEntryException {
            return super.save(entity);
        }
    };

    @Test
    public void save() throws ResourceNotFoundException, DuplicateEntryException {

        when(authenticationCertsRepository.save(any(AuthenticationCerts.class))).thenReturn(authenticationCertsMock);
        final AuthenticationCerts save = sut.save(authenticationCertsMock);
        assertNotNull(save);

    }

    @Test
    public void saveAndThrowException() throws ResourceNotFoundException, DuplicateEntryException {

        expectedException.expect(DuplicateEntryException.class);
        when(authenticationCertsRepository.save(any(AuthenticationCerts.class)))
            .thenThrow(new DuplicateEntryException("exception"));
        when(trackIdInstance.getTrackId()).thenReturn(TRACK_ID);
        sut.save(authenticationCertsMock);
    }

}
