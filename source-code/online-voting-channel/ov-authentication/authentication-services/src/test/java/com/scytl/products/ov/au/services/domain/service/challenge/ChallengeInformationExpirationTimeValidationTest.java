/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.challenge;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.domain.Level;
import com.scytl.products.oscore.logging.api.domain.LogContent;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.challenge.ChallengeInformation;
import com.scytl.products.ov.au.services.domain.model.challenge.ServerChallengeMessage;
import com.scytl.products.ov.au.services.domain.service.authenticationcontent.AuthenticationContentService;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationContent;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

@RunWith(MockitoJUnitRunner.class)
public class ChallengeInformationExpirationTimeValidationTest {

	public static final long A_LONG_TIME_AGO_IN_MILLISECONDS = 1000000L;
	@InjectMocks
	ChallengeInformationExpirationTimeValidation validation = new ChallengeInformationExpirationTimeValidation();

	@Mock
	ChallengeInformation challengeInformationMock;

	@Mock
	AuthenticationContentService authenticationContentServiceMock;

	@Mock
	AuthenticationContent authenticationContentMock;

	@Mock
	ServerChallengeMessage serverChallengeMessageMock;

	@Mock
	Logger logger;

	@Mock
	private SecureLoggingWriter secureLoggerWriter;

	@Mock
	private TrackIdInstance trackId;



	@Before
	public void setup() {
		doNothing().when(secureLoggerWriter).log(any(Level.class), any(LogContent.class));

		when(challengeInformationMock.getServerChallengeMessage()).thenReturn(serverChallengeMessageMock);
		when(trackId.getTrackId()).thenReturn("trackId");
	}

	@Test
	public void givenChallengeInformationWhenValidTimestampThenValidationSuccess() throws ResourceNotFoundException {
		String tenantId = "1";
		String electionEventId = "1";
		String votingCardId = "1";

		when(challengeInformationMock.getServerChallengeMessage().getTimestamp())
			.thenReturn(String.valueOf(System.currentTimeMillis()));
		when(authenticationContentServiceMock.getAuthenticationContent(anyString(), anyString()))
			.thenReturn(authenticationContentMock);
		when(authenticationContentMock.getChallengeExpirationTime()).thenReturn(111110);

		assertTrue(validation.execute(tenantId, electionEventId, votingCardId, challengeInformationMock));
	}

	@Test
	public void givenChallengeInformationWhenNegativeTimestampThenValidationFail() throws ResourceNotFoundException {
		String tenantId = "1";
		String electionEventId = "1";
		String votingCardId = "1";

		when(challengeInformationMock.getServerChallengeMessage().getTimestamp())
			.thenReturn(String.valueOf(Long.MIN_VALUE));
		when(authenticationContentServiceMock.getAuthenticationContent(anyString(), anyString()))
			.thenReturn(authenticationContentMock);
		when(authenticationContentMock.getChallengeExpirationTime()).thenReturn(0);

		assertFalse(validation.execute(tenantId, electionEventId, votingCardId, challengeInformationMock));
	}

	@Test
	public void givenChallengeInformationWhenNegativeDifferenceTimestampThenValidationFail()
			throws ResourceNotFoundException {
		String tenantId = "1";
		String electionEventId = "1";
		String votingCardId = "1";

		when(challengeInformationMock.getServerChallengeMessage().getTimestamp())
			.thenReturn(String.valueOf(Long.MAX_VALUE));
		when(authenticationContentServiceMock.getAuthenticationContent(anyString(), anyString()))
			.thenReturn(authenticationContentMock);
		when(authenticationContentMock.getChallengeExpirationTime()).thenReturn(0);

		assertFalse(validation.execute(tenantId, electionEventId, votingCardId, challengeInformationMock));
	}

	@Test
	public void givenChallengeInformationWhenExpiredTimestampThenValidationFail() throws ResourceNotFoundException {
		String tenantId = "1";
		String electionEventId = "1";
		String votingCardId = "1";

		when(challengeInformationMock.getServerChallengeMessage().getTimestamp())
			.thenReturn(String.valueOf(System.currentTimeMillis() - A_LONG_TIME_AGO_IN_MILLISECONDS));
		when(authenticationContentServiceMock.getAuthenticationContent(anyString(), anyString()))
			.thenReturn(authenticationContentMock);
		when(authenticationContentMock.getChallengeExpirationTime()).thenReturn(0);

		assertFalse(validation.execute(tenantId, electionEventId, votingCardId, challengeInformationMock));
	}
}
