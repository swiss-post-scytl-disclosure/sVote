/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.au.services.domain.utils.BeanUtils;
import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.AuthTokenValidationException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.validation.ValidationResult;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.cert.CertificateException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Test for the decorator class
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenTenantIdValidationDecoratorTest {

    public static final String TENANT_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String VOTING_CARD_ID = "100";

    @Mock
    private AuthenticationTokenTenantIdValidation authenticationTokenTenantIdValidation;

    @Mock
    private SecureLoggingWriter secureLoggingWriter;

    @Mock
    private SecureLoggerHelper secureLoggerHelper;

    @Mock
    private TrackIdInstance trackIdInstance;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());
    }

    @InjectMocks
    private AuthenticationTokenTenantIdValidationDecorator sut = new AuthenticationTokenTenantIdValidationDecorator() {
        @Override
        public ValidationResult execute(String tenantId, String electionEventId, String votingCardId, AuthenticationToken authenticationToken) throws ResourceNotFoundException,CertificateException{return super.execute(tenantId,electionEventId,votingCardId,authenticationToken);}};

    @Test
    public void validate() throws ResourceNotFoundException, CertificateException {
        when(authenticationTokenTenantIdValidation.execute(anyString(), anyString(), anyString(),
            any(AuthenticationToken.class))).thenReturn(new ValidationResult(true));

        assertTrue(sut.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, new AuthenticationToken()).isResult());

    }

    @Test
    public void testValidationFails() throws ResourceNotFoundException, CertificateException {

        expectedException.expect(AuthTokenValidationException.class);
        final AuthenticationToken authenticationToken = BeanUtils.createAuthenticationToken();
        when(trackIdInstance.getTrackId()).thenReturn("trackId");
        when(authenticationTokenTenantIdValidation.execute(anyString(), anyString(), anyString(),
            any(AuthenticationToken.class))).thenThrow(new AuthTokenValidationException(null));
        sut.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, authenticationToken);

    }

}
