/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.authenticationcontent;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import javax.inject.Inject;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationContentEntity;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationContentRepository;
import com.scytl.products.ov.au.services.domain.utils.AuthenticationUtils;
import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationContent;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

import java.io.IOException;

/**
 * Test Class for the Authentication Content Service
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationContentServiceTest {

    public static final String ELECTION_EVENT_ID = "100";

    public static final String TENANT_ID = "100";

    public static final String JSON = "{}";

    @Spy
    @InjectMocks
    @Inject
    private AuthenticationContentService authenticationContentService;

    @Mock
    private Logger LOG;

    // The authentication content repository.
    @Mock
    private AuthenticationContentRepository authenticationContentRepository;

    @Mock
    private SecureLoggingWriter secureLoggerWriter;

    @Mock
    private TrackIdInstance trackId;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void initMocks() {

        MockitoAnnotations.initMocks(this.getClass());

    }

    @Test
    public void getAuthenticationContent() throws ResourceNotFoundException, IOException, GeneralCryptoLibException {

        final AuthenticationContentEntity authenticationContentEntity = AuthenticationUtils.generateAuthenticationContentEntity();
        when(authenticationContentRepository.findByTenantIdElectionEventId(anyString(), anyString()))
            .thenReturn(authenticationContentEntity);
        final AuthenticationContent authenticationContent =
            authenticationContentService.getAuthenticationContent(TENANT_ID, ELECTION_EVENT_ID);
        assertNotNull(authenticationContent);

    }

    @Test
    public void getAuthenticationContentNotFound() throws ResourceNotFoundException {

        expectedException.expect(ResourceNotFoundException.class);
        when(authenticationContentRepository.findByTenantIdElectionEventId(anyString(), anyString()))
            .thenThrow(new ResourceNotFoundException("exception"));
            authenticationContentService.getAuthenticationContent(TENANT_ID, ELECTION_EVENT_ID);


    }

}
