/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.remote;

/**
 * Junits for the class {@linkVoterMaterialImpl}.
 */
// @RunWith(MockitoJUnitRunner.class)
public class VoterMaterialRepositoryImplTest {

	// @Rule
	// public ExpectedException expectedException = ExpectedException.none();
	//
	// @InjectMocks
	// @Spy
	// private CredentialRepositoryImpl voterMaterialRepository = new CredentialRepositoryImpl();
	//
	// @Mock
	// private RESTClient restClientMock;
	//
	// @Mock
	// private WebTarget targetMock;
	//
	// @Mock
	// private Builder builderMock;
	//
	// @Mock
	// private Response responseMock;
	//
	// @Mock
	// private TrackIdInstance trackId;
	//
	// @Test
	// public void findByTenantIdElectionEventIdVotingCardIdThrowResourceNotFoundException() throws
	// ResourceNotFoundException {
	// doReturn(restClientMock).when(voterMaterialRepository).makeRESTClient(any(String.class), any(String.class));
	// when(restClientMock.getWebTarget()).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(trackId.getTrackId()).thenReturn("1");
	// when(targetMock.request(any(MediaType.class))).thenReturn(builderMock);
	// when(builderMock.header(any(String.class), any(Object.class))).thenReturn(builderMock);
	// when(builderMock.get()).thenReturn(responseMock);
	// when(responseMock.getStatus()).thenReturn(Status.NOT_FOUND.getStatusCode());
	//
	// expectedException.expect(ResourceNotFoundException.class);
	// voterMaterialRepository.findByTenantIdElectionEventIdVotingCardId("", "", "");
	// }
	//
	// @Test
	// public void findByTenantIdElectionEventIdVotingCardId() throws ResourceNotFoundException {
	// doReturn(restClientMock).when(voterMaterialRepository).makeRESTClient(any(String.class), any(String.class));
	// when(restClientMock.getWebTarget()).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(targetMock.path(any(String.class))).thenReturn(targetMock);
	// when(trackId.getTrackId()).thenReturn("1");
	// when(targetMock.request(any(MediaType.class))).thenReturn(builderMock);
	// when(builderMock.header(any(String.class), any(Object.class))).thenReturn(builderMock);
	// Credential credentialDataMock = new Credential();
	// credentialDataMock.setId("1");
	// when(builderMock.get()).thenReturn(responseMock);
	// when(responseMock.readEntity(Credential.class)).thenReturn(credentialDataMock);
	//
	// Credential credential = voterMaterialRepository.findByTenantIdElectionEventIdVotingCardId("", "", "");
	// assertTrue(credential != null);
	// assertEquals(credentialDataMock.getId(), credential.getId());
	// }
}
