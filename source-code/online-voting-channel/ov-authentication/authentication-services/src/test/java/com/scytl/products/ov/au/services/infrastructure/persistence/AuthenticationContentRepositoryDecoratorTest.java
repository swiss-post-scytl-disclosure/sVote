/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.persistence;


import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationContentEntity;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationContentRepository;
import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationContentRepositoryDecoratorTest {


    @Mock
    private AuthenticationContentRepository authenticationContentRepositoryMock;

    public static final String TENANT_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String TRACK_ID = "trackId";

    @Mock
    private SecureLoggingWriter secureLoggingWriter;

    @Mock
    private SecureLoggerHelper helper;

    @Mock
    private Logger logger;

    @Mock
    private TrackIdInstance trackIdInstance;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    AuthenticationContentEntity authenticationContentMock;

    @Before
    public void init() {

        MockitoAnnotations.initMocks(this.getClass());
    }

    @InjectMocks
    private AuthenticationContentRepositoryDecorator sut = new AuthenticationContentRepositoryDecorator() {

        @Override
        public AuthenticationContentEntity findByTenantIdElectionEventId(String tenantId, String electionEventId)
                throws ResourceNotFoundException {
            return authenticationContentRepositoryMock.findByTenantIdElectionEventId(tenantId, electionEventId);
        }

        @Override
        public AuthenticationContentEntity find(Integer integer) {
            return authenticationContentRepositoryMock.find(integer);
        }

        @Override
        public AuthenticationContentEntity update(AuthenticationContentEntity entity) throws EntryPersistenceException {
            return authenticationContentRepositoryMock.update(entity);
        }

        @Override
        public AuthenticationContentEntity save(final AuthenticationContentEntity entity) throws DuplicateEntryException {
            return super.save(entity);
        }
    };

    @Test
    public void save() throws ResourceNotFoundException, DuplicateEntryException {

        when(authenticationContentRepositoryMock.save(any(AuthenticationContentEntity.class))).thenReturn(authenticationContentMock);
        final AuthenticationContentEntity save = sut.save(authenticationContentMock);
        assertNotNull(save);

    }

    @Test
    public void saveAndThrowException() throws ResourceNotFoundException, DuplicateEntryException {

        expectedException.expect(DuplicateEntryException.class);
        when(authenticationContentRepositoryMock.save(any(AuthenticationContentEntity.class)))
                .thenThrow(new DuplicateEntryException("exception"));
        when(trackIdInstance.getTrackId()).thenReturn(TRACK_ID);
        sut.save(authenticationContentMock);
    }
}
