/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.remote;


import com.scytl.products.ov.au.services.domain.model.material.Credential;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

import okhttp3.ResponseBody;
import retrofit2.Call;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class CredentialRepositoryImplTest {
    public static final String TENANT_ID = "100";

    public static final String CREDENTIAL_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    @Mock
    private TrackIdInstance trackIdMock;

    @Mock
    private VoterMaterialClient voterMaterialClient;

    @Mock
    private Logger logger;

    @Mock
    private Credential credentialMock;

    @InjectMocks
    private CredentialRepositoryImpl credentialRepository = new CredentialRepositoryImpl(voterMaterialClient);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void findByTenantIdElectionEventIdCredentialId()
            throws ResourceNotFoundException, IOException {

    	@SuppressWarnings("unchecked")
		Call<Credential> callMock = (Call<Credential>) Mockito.mock(Call.class);
        when(callMock.execute()).thenReturn(retrofit2.Response.success(credentialMock));
        
        when(voterMaterialClient.getCredential(anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenReturn(callMock);
        final Credential byTenantIdElectionEventIdCredentialId = credentialRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID,
                CREDENTIAL_ID);
        assertNotNull(byTenantIdElectionEventIdCredentialId);
    }

    @Test
    public void findByTenantIdElectionEventIdCredentialIdThrowResourceNotFoundException()
            throws ResourceNotFoundException {

        expectedException.expect(ResourceNotFoundException.class);
        doThrow(ResourceNotFoundException.class)
            .when(voterMaterialClient).getCredential(anyString(), anyString(), anyString(), anyString(), anyString());

        credentialRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID,
                CREDENTIAL_ID);
    }

    @Test
    public void findByTenantIdElectionEventIdCredentialIdThrowRetrofitError()
            throws ResourceNotFoundException, IOException {
    	
        @SuppressWarnings("unchecked")
		Call<Credential> callMock = (Call<Credential>) Mockito.mock(Call.class);
    	when(callMock.execute()).thenReturn(retrofit2.Response.error(404,  ResponseBody.create(okhttp3.MediaType.parse("text/html"), new byte[0])));

        expectedException.expect(RetrofitException.class);
        when(voterMaterialClient.getCredential(anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenReturn(callMock);
        credentialRepository.findByTenantIdElectionEventIdCredentialId(TENANT_ID, ELECTION_EVENT_ID,
                CREDENTIAL_ID);
    }
}
