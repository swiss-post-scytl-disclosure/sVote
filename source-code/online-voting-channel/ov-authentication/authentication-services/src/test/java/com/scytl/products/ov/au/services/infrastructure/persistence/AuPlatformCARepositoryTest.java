/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.persistence;

import com.scytl.products.ov.au.services.domain.model.platform.AuLoggingKeystoreEntity;
import com.scytl.products.ov.au.services.domain.model.platform.AuPlatformCaEntity;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

@RunWith(MockitoJUnitRunner.class)

public class AuPlatformCARepositoryTest {



    @InjectMocks
    private AuPlatformCARepositoryImpl sut = new AuPlatformCARepositoryImpl();

    @Mock
    private EntityManager managerMock;


    @Mock
    private AuLoggingKeystoreEntity auLoggingKeystoreEntityMock;

    @Mock
    private TypedQuery<AuPlatformCaEntity> typedQueryMock;

    @Mock
    private AuPlatformCaEntity platformCaEntityMock;


    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Before
    public void init(){

        MockitoAnnotations.initMocks(this.getClass());
        Mockito.when(managerMock.createQuery(Mockito.anyString(),Mockito.eq(AuPlatformCaEntity.class))).thenReturn(typedQueryMock);

    }


    @Test
    public void getPlatformCA() throws ResourceNotFoundException {

        expectedException.expect(ResourceNotFoundException.class);
        Mockito.when(typedQueryMock.getResultList()).thenThrow(new NoResultException("exception"));
        sut.getRootCACertificate();

    }
}
