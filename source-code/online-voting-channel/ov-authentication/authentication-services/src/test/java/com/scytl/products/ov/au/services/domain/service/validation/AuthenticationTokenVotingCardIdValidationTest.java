/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import com.scytl.products.ov.commons.validation.ValidationResult;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.commons.beans.domain.model.authentication.AuthenticationContent;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.au.services.domain.model.information.VoterInformation;
import com.scytl.products.ov.commons.beans.exceptions.AuthTokenValidationException;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenVotingCardIdValidationTest {

	AuthenticationTokenVotingCardIdValidation authenticationTokenVotingCardIdValidation =
		new AuthenticationTokenVotingCardIdValidation();

	@Mock
	private AuthenticationToken authenticationTokenMock;

	@Mock
	private VoterInformation voterInformationMock;

	@Mock
	private AuthenticationContent authenticationContentMock;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void givenAuthenticationTokenWhenVotingCardIdsAreEqualThenValidationSuccess() {
		String tenantId = "1";
		String electionEventId = "1";
		String votingCardId = "1";
		when(authenticationTokenMock.getVoterInformation()).thenReturn(voterInformationMock);
		when(voterInformationMock.getVotingCardId()).thenReturn(votingCardId);

		ValidationResult result = authenticationTokenVotingCardIdValidation.execute(tenantId, electionEventId, votingCardId,
			authenticationTokenMock);

		assertTrue(result.isResult());
	}

	@Test
	public void givenAuthenticationTokenWhenVotingCardIdsAreNotEqualThenValidationFail() {
		String tenantId = "1";
		String electionEventId = "1";
		String votingCardId = "1";
		when(authenticationTokenMock.getVoterInformation()).thenReturn(voterInformationMock);
		String tokenVotingCardId = "2";
		when(voterInformationMock.getVotingCardId()).thenReturn(tokenVotingCardId);

		expectedException.expect(AuthTokenValidationException.class);
		authenticationTokenVotingCardIdValidation.execute(tenantId, electionEventId, votingCardId,
			authenticationTokenMock);
	}
}
