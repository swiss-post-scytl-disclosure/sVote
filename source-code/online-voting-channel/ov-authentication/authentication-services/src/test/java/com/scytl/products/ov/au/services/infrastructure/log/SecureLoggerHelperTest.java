/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.log;


import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;
import com.scytl.products.ov.au.services.domain.utils.BeanUtils;
import com.scytl.products.ov.commons.logging.service.AuthTokenHashService;
import com.scytl.products.ov.commons.util.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import javax.json.JsonException;
import javax.json.JsonObject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SecureLoggerHelperTest {


    public static final String HASH_VALUE = "thisisahash";
    public static final String INVALID = "Invalid";
    public static final String NOT_A_TIMESTAMP = "asdv";
    public static final String EMPTY_STRING = "";
    @InjectMocks
    private SecureLoggerHelper sut = new SecureLoggerHelper();

    @Mock
    private AuthenticationToken authenticationTokenMock;

    @Mock
    private AuthTokenHashService authTokenHashService;

    @Mock
    private Logger logger;


    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Before
    public void init(){
        MockitoAnnotations.initMocks(this.getClass());
    }


    @Test
    public void getAuthTokenHash() throws GeneralCryptoLibException {

        final AuthenticationToken authenticationToken = BeanUtils.createAuthenticationToken();
        when(authTokenHashService.hash(any(JsonObject.class))).thenReturn(HASH_VALUE);
        final String authTokenHash = sut.getAuthTokenHash(authenticationToken);
        assertNotNull(authTokenHash);


    }

    @Test
    public void getAuthTokenHashThrowsException() throws GeneralCryptoLibException {

        final AuthenticationToken authenticationToken = BeanUtils.createAuthenticationToken();
        when(authTokenHashService.hash(any(JsonObject.class))).thenThrow(new JsonException("exception"));
        final String authTokenHash = sut.getAuthTokenHash(authenticationToken);
        assertTrue(StringUtils.isEmpty(authTokenHash));

    }


    @Test
    public void getAuthTokenId(){

        final AuthenticationToken authenticationToken = BeanUtils.createAuthenticationToken();
        final String authTokenId = sut.getAuthTokenId(authenticationToken);
        assertFalse(StringUtils.isEmpty(authTokenId));
    }

    @Test
    public void getTimestamp(){

        when(authenticationTokenMock.getTimestamp()).thenReturn(DateUtils.getTimestamp());
        final String authTokenTimestamp = sut.getAuthTokenTimestamp(authenticationTokenMock);
        assertFalse(StringUtils.isEmpty(authTokenTimestamp));
    }

    @Test
    public void getTimestampFromNullaco(){

        when(authenticationTokenMock.getTimestamp()).thenReturn(null);
        final String authTokenTimestamp = sut.getAuthTokenTimestamp(authenticationTokenMock);
        assertTrue(StringUtils.isEmpty(authTokenTimestamp));
    }


    @Test
    public void getTimestampWrongFormat(){

        when(authenticationTokenMock.getTimestamp()).thenReturn(NOT_A_TIMESTAMP);
        final String authTokenTimestamp = sut.getAuthTokenTimestamp(authenticationTokenMock);
        assertEquals(authTokenTimestamp, INVALID);
    }




}
