/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.commons.random.RandomType;

public class ChallengeTest {


    public static final int LENGTH = 16;

    @Test
    public void testChallenge() throws GeneralCryptoLibException {

        final Challenge challenge = new Challenge(RandomType.BYTES, LENGTH);
        assertNotNull(challenge);
        assertFalse(StringUtils.isEmpty(challenge.getChallengeValue()));
    }

    @Test
    public void testChallengeINT() throws GeneralCryptoLibException {

        final Challenge challenge = new Challenge(RandomType.INTEGER, LENGTH);
        assertNotNull(challenge);
        assertFalse(StringUtils.isEmpty(challenge.getChallengeValue()));
    }

    @Test
    public void testChallengeString() throws GeneralCryptoLibException {

        final Challenge challenge = new Challenge(RandomType.STRING, LENGTH);
        assertNotNull(challenge);
        assertFalse(StringUtils.isEmpty(challenge.getChallengeValue()));
    }
}
