/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import com.scytl.products.ov.au.services.domain.model.information.VoterInformation;
import com.scytl.products.ov.au.services.domain.model.information.VoterInformationRepository;
import com.scytl.products.ov.au.services.domain.service.exception.AuthenticationTokenGenerationException;
import com.scytl.products.ov.au.services.domain.service.exception.AuthenticationTokenSigningException;

import com.scytl.products.ov.au.services.infrastructure.remote.ValidationRepositoryImpl;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.beans.validation.CryptographicOperationException;
import com.scytl.products.ov.commons.crypto.SignatureForObjectService;
import com.scytl.products.ov.commons.validation.ValidationResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.charset.StandardCharsets;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenFactoryTest {

    public static final String CREDENTIAL_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String TENANT_ID = "100";

    @InjectMocks
    private AuthenticationTokenFactoryImpl factory = new AuthenticationTokenFactoryImpl();

    @Mock
    private VoterInformationRepository voterInformationRepositoryMock;

    @Mock
    private ValidationRepositoryImpl validationRepository;

    @Mock
    private VoterInformation voterInformationMock;

    @Mock
    private SignatureForObjectService signatureService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final byte[] signatureBytes = "signature".getBytes(StandardCharsets.UTF_8);

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());

    }

    @Test
    public void testAuhTokenCreation()
            throws AuthenticationTokenGenerationException, AuthenticationTokenSigningException,
            ResourceNotFoundException, CryptographicOperationException {

        when(voterInformationRepositoryMock.findByTenantIdElectionEventIdCredentialId(anyString(), anyString(),
            anyString())).thenReturn(voterInformationMock);
        when(validationRepository.validateElectionInDates(anyString(), anyString(), anyString()))
            .thenReturn(new ValidationResult(true));
        when(signatureService.sign(anyString(), anyString(), anyString(), anyString(), Matchers.anyVararg()))
            .thenReturn(signatureBytes);

        final AuthenticationTokenMessage authenticationTokenMessage =
            factory.buildAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID);
        Assert.assertNotNull(authenticationTokenMessage);
    }

    @Test
    public void testAuhTokenCreationCryptoLibException()
            throws AuthenticationTokenGenerationException, AuthenticationTokenSigningException,
            ResourceNotFoundException, CryptographicOperationException {

        expectedException.expect(AuthenticationTokenSigningException.class);
        when(voterInformationRepositoryMock.findByTenantIdElectionEventIdCredentialId(anyString(), anyString(),
            anyString())).thenReturn(voterInformationMock);
        when(validationRepository.validateElectionInDates(anyString(), anyString(), anyString()))
            .thenReturn(new ValidationResult(true));
        when(signatureService.sign(anyString(), anyString(), anyString(), anyString(), Matchers.anyVararg()))
            .thenThrow(new CryptographicOperationException("exception"));

        factory.buildAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID);

    }

    @Test
    public void testAuhTokenCreationCryptoGraphicOperationException()
            throws AuthenticationTokenGenerationException, AuthenticationTokenSigningException,
            ResourceNotFoundException, CryptographicOperationException {

        expectedException.expect(AuthenticationTokenSigningException.class);
        when(voterInformationRepositoryMock.findByTenantIdElectionEventIdCredentialId(anyString(), anyString(),
                anyString())).thenReturn(voterInformationMock);
        when(validationRepository.validateElectionInDates(anyString(), anyString(), anyString()))
                .thenReturn(new ValidationResult(true));
        when(signatureService.sign(anyString(), anyString(), anyString(), anyString(), Matchers.anyVararg()))
                .thenThrow(new CryptographicOperationException("exception"));

        factory.buildAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID);

    }

}
