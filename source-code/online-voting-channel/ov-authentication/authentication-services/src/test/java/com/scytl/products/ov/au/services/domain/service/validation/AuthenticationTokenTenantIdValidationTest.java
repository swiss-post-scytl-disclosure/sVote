/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.service.validation;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import com.scytl.products.ov.au.services.domain.model.information.VoterInformation;
import com.scytl.products.ov.commons.beans.exceptions.AuthTokenValidationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.au.services.domain.model.authentication.AuthenticationToken;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTokenTenantIdValidationTest {

    public static final String TENANT_ID = "100";

    public static final String ELECTION_EVENT_ID = "100";

    public static final String VOTING_CARD_ID = "100";

    public static final String OTHER_TENANT_ID = "OTHER_TENANT_ID";

    @InjectMocks
    private AuthenticationTokenTenantIdValidation validation = new AuthenticationTokenTenantIdValidation();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private Logger logger;

    @Mock
    private AuthenticationToken authenticationTokenMock;

    @Mock
    private VoterInformation voterInformation;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());
    }

    @Test
    public void validate() {
        when(voterInformation.getTenantId()).thenReturn(TENANT_ID);
        when(authenticationTokenMock.getVoterInformation()).thenReturn(voterInformation);
        assertTrue(
            validation.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, authenticationTokenMock).isResult());
    }

    @Test
    public void validateAndFail() {

        expectedException.expect(AuthTokenValidationException.class);
        when(voterInformation.getTenantId()).thenReturn(OTHER_TENANT_ID);
        when(authenticationTokenMock.getVoterInformation()).thenReturn(voterInformation);
        validation.execute(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, authenticationTokenMock);
    }

}
