/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.domain.model.authentication;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationKeyStoreForObjectRepositoryTest {

    public static final String ELECTION_EVENT_ID = "100";
    public static final String TENANT_ID = "100";
    public static final String OBJECT_ID = "";

    @InjectMocks
    private AuthenticationKeyStoreForObjectRepository sut = new AuthenticationKeyStoreForObjectRepository();

    @Mock
    private AuthenticationContentRepository authenticationContentRepository;

    @Mock
    private AuthenticationContentEntity authenticationContentEntityMock;

    private static final String JSON = "{\"authenticationTokenSignerKeystore\":\"asdfasdf\"}";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());
    }

    @Test
    public void getKeystore() throws ResourceNotFoundException {

        when(authenticationContentRepository.findByTenantIdElectionEventId(anyString(), anyString()))
            .thenReturn(authenticationContentEntityMock);
        when(authenticationContentEntityMock.getJson()).thenReturn(JSON);
        sut.getJsonByTenantEEIDObjectId(TENANT_ID, ELECTION_EVENT_ID, OBJECT_ID);



    }
}
