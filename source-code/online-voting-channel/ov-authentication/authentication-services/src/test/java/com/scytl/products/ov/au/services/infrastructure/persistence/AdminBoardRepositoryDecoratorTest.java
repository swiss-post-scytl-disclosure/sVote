/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.services.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.au.services.domain.model.adminboard.AdminBoard;
import com.scytl.products.ov.au.services.domain.model.adminboard.AdminBoardRepository;
import com.scytl.products.ov.au.services.infrastructure.log.SecureLoggerHelper;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;



@RunWith(MockitoJUnitRunner.class)
public class AdminBoardRepositoryDecoratorTest {


    public static final String ADMIN_BOARD_ID = "100";
    public static final String TENANT_ID = "100";
    public static final String ELECTION_EVENT_ID = "100";
    public static final String TRACK_ID = "trackId";


    @Mock
    private SecureLoggingWriter secureLoggingWriter;

    @Mock
    private SecureLoggerHelper helper;

    @Mock
    private Logger logger;


    @Mock
    private TrackIdInstance trackIdInstance;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private AdminBoardRepository adminBoardRepository;

    @Mock
    private AdminBoard adminBoardMock;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this.getClass());
    }


    @InjectMocks
    private AdminBoardRepositoryDecorator sut = new AdminBoardRepositoryDecorator() {
        @Override
        public AdminBoard findByTenantIdElectionEventId(String tenantId, String electionEventId) throws ResourceNotFoundException {
            return adminBoardRepository.findByTenantIdElectionEventId(tenantId, electionEventId);
        }

        @Override
        public AdminBoard find(Integer integer) {
            return adminBoardRepository.find(integer);
        }

        @Override
        public AdminBoard update(AdminBoard entity) throws EntryPersistenceException {
            return adminBoardRepository.update(entity);
        }

        @Override
        public AdminBoard save(final AdminBoard entity) throws DuplicateEntryException{
            return super.save(entity);
        }
    };



    @Test
    public void save() throws ResourceNotFoundException, DuplicateEntryException {

        when(adminBoardRepository.save(any(AdminBoard.class))).thenReturn(adminBoardMock);
        final AdminBoard save = sut.save(adminBoardMock);
        assertNotNull(save);

    }

    @Test
    public void saveAndThrowException() throws ResourceNotFoundException, DuplicateEntryException {

        expectedException.expect(DuplicateEntryException.class);
        when(adminBoardRepository.save(any(AdminBoard.class))).thenThrow(new DuplicateEntryException("exception"));
        when(trackIdInstance.getTrackId()).thenReturn(TRACK_ID);
        sut.save(adminBoardMock);
    }

}
