/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.ui.ws.rs.application.operation;

import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.au.services.domain.service.validation.AuthenticationTokenValidationService;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationExceptionMessages;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;

import java.io.IOException;
import java.security.cert.CertificateException;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.scytl.products.ov.commons.util.HttpRequestService;
import com.scytl.products.ov.commons.validation.ValidationResult;

@RunWith(MockitoJUnitRunner.class)
public class ValidationAuthenticationTokenResourceTest {
	
	private static String VALID_AUTH_TOKEN = "{\n"
			+ "    \"id\": \"A+nlb4FbFY3u0qBH4jcUfA==\",\n"
			+ "    \"voterInformation\": {\n"
			+ "      \"tenantId\": \"100\",\n"
			+ "      \"electionEventId\": \"3a2434c5a1004d71ac53b55d3ccdbfb8\",\n"
			+ "      \"votingCardId\": \"1f5e2153c3ab4657b79d9d4b61a228b7\",\n"
			+ "      \"ballotId\": \"b7e28ca876364dfa9a9315d795f59172\",\n"
			+ "      \"credentialId\": \"a82638e3471b5c3d8d3581a98c53c644\",\n"
			+ "      \"verificationCardId\": \"afeed1c301a34b718a2b4a847344b5d1\",\n"
			+ "      \"ballotBoxId\": \"089378cdc15c480b85560b7f9adcea64\",\n"
			+ "      \"votingCardSetId\": \"4ae2b45680974a9da06ebfc5b13a8685\",\n"
			+ "      \"verificationCardSetId\": \"07f2c3b8ac5b42ddae107f1f0fa3ddeb\"\n"
			+ "    },\n"
			+ "    \"timestamp\": \"1508419659591\",\n"
			+ "    \"signature\": \"TVRB62Zxh+FcJkenvunbT4vglRoXxXyWZXuY8LrTBobl6qADdHIIgdtpNBwkGib2SqquWImIQhqUzzEP3Kicizk31ctR6af7a5z9OO71vBeBns2tfe08nZsV4Niz1WNVa09mjV8skxVEyIqAkd3QIPk76RkCgqsei/rbYs248LGgN8VGHoafcn8hop0Vzrk1+PcL0a6DvAr6wCeDLT1DIEA6af8LTi76lnNR4J+EB4GeUmyRazx6TeqaNzxlZV9J1ep/QYCf6oZcQYYk5eEtIGaDbqD23RBLqhS4wNye/3TEhFpfJXZ2SgWFyN+ii81Opk1kGdnomIBTcqJJuRTNBQ==\"\n"
			+ "  }";
	
    @Mock
    private TrackIdInstance trackIdInstance;
    
    @Mock
    private AuthenticationTokenValidationService authenticationTokenValidationService;
    
    @Mock
    private TransactionInfoProvider transactionInfoProvider;
    
    @Mock
    private HttpRequestService httpRequestService;
    
    @Mock
    @Inject
    private Logger LOG;
    
	@InjectMocks
	private ValidationAuthenticationTokenResource sut;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testPassValidation() throws CertificateException, ApplicationException, ResourceNotFoundException, IOException {
    	ValidationResult correct = new ValidationResult(true);
    	Mockito.when(authenticationTokenValidationService.validate(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.any())).thenReturn(correct);
    	Response validateAuthenticationToken = sut.validateAuthenticationToken("test", "test", "test", "test", VALID_AUTH_TOKEN, null);
    	ValidationResult readEntity = (ValidationResult)validateAuthenticationToken.getEntity();
    	Assert.assertEquals(correct.isResult(), readEntity.isResult());
    }

    @Test
    public void testExtraFieldValidationFail() throws CertificateException, ApplicationException, ResourceNotFoundException, IOException {
    	ValidationResult correct = new ValidationResult(true);
    	thrown.expect(ApplicationException.class);
        thrown.expectMessage(ApplicationExceptionMessages.EXCEPTION_MESSAGE_AUTH_TOKEN_INVALID_FORMAT);
    	Mockito.when(authenticationTokenValidationService.validate(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.any())).thenReturn(correct);
    	String invalid = VALID_AUTH_TOKEN.concat("invalid content in the authentication header");
		sut.validateAuthenticationToken("test", "test", "test", "test", invalid, null);
		Assert.fail();
    }
}
