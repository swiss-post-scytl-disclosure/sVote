/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.ui.ws.rs.application.operation;

import com.scytl.products.ov.commons.beans.validation.ValidationErrorType;
import com.scytl.products.ov.commons.validation.ValidationError;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.scytl.products.ov.commons.ui.ws.rs.ObjectMappers;
import com.scytl.products.ov.commons.validation.ValidationResult;

public class AuthenticationTokenResourceTest {

    @Test
    public void testPassValidation() throws JsonProcessingException {

        ValidationResult valid = new ValidationResult(true);
        String json = ObjectMappers.toJson(valid);
        System.out.println(json);
        //{"result":true,"validationError":{"validationErrorType":"SUCCESS"}}
        String expectedValid="{\"result\":true,\"validationError\":{\"validationErrorType\":\"SUCCESS\"}}";
        Assert.assertEquals(expectedValid.replace("\\",""),json);


        ValidationResult invalid = new ValidationResult(false);
        ValidationError validationError = new ValidationError();
        validationError.setValidationErrorType(ValidationErrorType.INVALID_CERTIFICATE_CHAIN);
        invalid.setValidationError(validationError);
        json = ObjectMappers.toJson(invalid);
        System.out.println(json);
        //{"result":false,"validationError":{"validationErrorType":"INVALID_CERTIFICATE_CHAIN"}}
        String expectedInvalid= "{\"result\":false,\"validationError\":{\"validationErrorType\":\"INVALID_CERTIFICATE_CHAIN\"}}";
        Assert.assertEquals(expectedInvalid.replace("\\",""),json);

    }
}
