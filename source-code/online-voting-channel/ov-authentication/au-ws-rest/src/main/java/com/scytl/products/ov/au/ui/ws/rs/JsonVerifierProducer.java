/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.ui.ws.rs;

import com.scytl.products.ov.commons.verify.JSONVerifier;

import javax.enterprise.inject.Produces;

public class JsonVerifierProducer {




    @Produces
    public JSONVerifier getJsonVerifier(){
        return new JSONVerifier();
    }
}
