/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.au.ui.ws.rs.application.operation;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.cryptolib.certificates.bean.X509CertificateType;
import com.scytl.products.ov.au.services.domain.model.platform.AuCertificateValidationService;
import com.scytl.products.ov.au.services.domain.model.platform.AuLoggingKeystoreEntity;
import com.scytl.products.ov.au.services.domain.model.platform.AuLoggingKeystoreRepository;
import com.scytl.products.ov.au.services.domain.model.platform.AuPlatformCARepository;
import com.scytl.products.ov.au.services.domain.model.platform.AuPlatformCaEntity;
import com.scytl.products.ov.au.services.infrastructure.log.AuLoggingInitializationState;
import com.scytl.products.ov.commons.beans.domain.model.platform.PlatformInstallationData;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.validation.CertificateValidationService;
import com.scytl.products.ov.commons.domain.model.platform.PlatformCARepository;
import com.scytl.products.ov.commons.domain.model.platform.PlatformInstallationDataHandler;
import com.scytl.products.ov.commons.logging.config.SecureLoggerInitializer;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreRepository;

/**
 * Endpoint for upload the information during the installation of the platform in the system
 */
@Path("platformdata")
@Stateless(name = "au-platformDataResource")
public class AuPlatformDataResource {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private static final String CONTEXT = "AU";

    private static final String ENCRYPTION_PWS_PROPERTIES_KEY = "AU_log_encryption";

    private static final String SIGNING_PWS_PROPERTIES_KEY = "AU_log_signing";
        
    @EJB
    @AuPlatformCARepository
    PlatformCARepository platformRepository;

    @EJB
    @AuLoggingKeystoreRepository
    LoggingKeystoreRepository loggingKeystoreRepository;

    @EJB
    AuLoggingInitializationState auLoggingInitializationState;

    @EJB
    @AuCertificateValidationService
    CertificateValidationService certificateValidationService;

    /**
     * Installs the logging keystores and platform CA in the service. Additionally, this method also attempts to
     * initialize the secure logging system.
     *
     * @param data
     *            all the platform data.
     * @throws IllegalStateException
     * @throws DuplicateEntryException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response savePlatformData(final PlatformInstallationData data) throws DuplicateEntryException {

        try {
            String platformName = PlatformInstallationDataHandler.savePlatformCertificateChain(data, platformRepository,
                certificateValidationService, new AuPlatformCaEntity(), new AuPlatformCaEntity());

            String encryptionKey = data.getLoggingEncryptionKeystoreBase64();
            String signingKey = data.getLoggingSigningKeystoreBase64();

            AuLoggingKeystoreEntity encryptionKeystore = new AuLoggingKeystoreEntity();
            encryptionKeystore.setPlatformName(platformName);
            encryptionKeystore.setKeystoreContent(encryptionKey);
            encryptionKeystore.setKeyType(X509CertificateType.ENCRYPT.name());
            loggingKeystoreRepository.save(encryptionKeystore);

            AuLoggingKeystoreEntity signingKeystore = new AuLoggingKeystoreEntity();
            signingKeystore.setPlatformName(platformName);
            signingKeystore.setKeystoreContent(signingKey);
            signingKeystore.setKeyType(X509CertificateType.SIGN.name());
            loggingKeystoreRepository.save(signingKeystore);

            SecureLoggerInitializer secureLoggerInitializer = new SecureLoggerInitializer(auLoggingInitializationState,
                loggingKeystoreRepository, CONTEXT, ENCRYPTION_PWS_PROPERTIES_KEY, SIGNING_PWS_PROPERTIES_KEY);

            secureLoggerInitializer.initializeFromUploadData(encryptionKey, signingKey);
            LOG.info("AU - logging initialized successfully");
            auLoggingInitializationState.setInitialized(true);

            return Response.ok().build();

        } catch (Exception e) {
            throw new IllegalStateException("AU - error while trying to initialize logging", e);
        }
    }
}
