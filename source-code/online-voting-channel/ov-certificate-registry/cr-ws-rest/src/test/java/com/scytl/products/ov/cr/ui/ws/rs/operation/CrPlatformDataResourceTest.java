/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.cr.ui.ws.rs.operation;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.commons.beans.domain.model.platform.PlatformInstallationData;
import com.scytl.products.ov.commons.beans.validation.CertificateValidationServiceImpl;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreRepository;
import com.scytl.products.ov.commons.util.HttpRequestService;
import com.scytl.products.ov.cr.services.domain.model.certificate.CertificateRepository;

@Ignore
public class CrPlatformDataResourceTest {

	private CrPlatformDataResource crPlatformDataResource;

	private String platform = "-----BEGIN CERTIFICATE-----"
			+ "MIIDYjCCAkqgAwIBAgIVAK1iv49QAkCz0vpq/cfHtrFdPv9BMA0GCSqGSIb3DQEB"
			+ "CwUAMFgxFjAUBgNVBAMMDVNjeXRsIFJvb3QgQ0ExFjAUBgNVBAsMDU9ubGluZSBW"
			+ "b3RpbmcxDjAMBgNVBAoMBVNjeXRsMQkwBwYDVQQHDAAxCzAJBgNVBAYTAkVTMB4X"
			+ "DTE2MTAyMDAwMDAwMFoXDTE4MTAyMDIzMDAwMFowWDEWMBQGA1UEAwwNU2N5dGwg"
			+ "Um9vdCBDQTEWMBQGA1UECwwNT25saW5lIFZvdGluZzEOMAwGA1UECgwFU2N5dGwx"
			+ "CTAHBgNVBAcMADELMAkGA1UEBhMCRVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw"
			+ "ggEKAoIBAQDKit/euH0XFMSot/CEDW9MN0Uoxxo6lKFb6gtyraQUoS1hSawXTRyr"
			+ "sMzjEbnZQY1lPWgFAZCMzSVK7RZVetiqng3uTmWFll5/XtyxR+P4GD0Q6OW0icFr"
			+ "oDAQ4AD/YMEVX7T/dWsnh1j/NIg1sXsTLjNZyswXiXlhe71wrEFb61/NtxFu//rn"
			+ "ZtPL/oQn/MLmXtrQZVPEQ1KDhPpAbvruNyqkThWyNvoEf3pUMeYuwMwNqcgPiOj3"
			+ "1z9orIeMCnTagquJc3KbEE2/WFPaWmETl4w+gpUcafxmFvsSXijvmaMW86sc+GT2"
			+ "L5NjFpewSXG0vghg7f167tC9uIZBQ2rhAgMBAAGjIzAhMA8GA1UdEwEB/wQFMAMB"
			+ "Af8wDgYDVR0PAQH/BAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQAEU1EcIPEUQOf8"
			+ "Do0Z+R3JwRo6YmknCbATSDNmjGHLLwsaJn0SnI+fQXLdw1eE8XR3WnmF2Oi0ZDO+"
			+ "6wFo+cZYFHtaCoO4TqpJjyJt9lDXOGv0+tTkmHRZdGfceVhbg1aX3tEL5a3Nkyt5"
			+ "dolPZmtMqLVUsxOQOU+/q6S9cRaSeSukEc/xgydeOAJ3LBrAGCGUXMNxqf/g7u8n"
			+ "jQ3QEOydTxfZh7UKndufnFSzP8RlPYAJsoknwmsl/SgSCNcaGNJoTBAUKw30xATe"
			+ "NzP7MQZ+wvdix18dshSyCemV5cdTNT4ZpEh8DUuc80KrCKOIHSeydpUUe2Khvdb8" + "cD7sazdu"
			+ "-----END CERTIFICATE-----";
	
	private String platformrecertified = "-----BEGIN CERTIFICATE-----"
			+ "MIIDgDCCAmigAwIBAgIJALW8ggDCbmv+MA0GCSqGSIb3DQEBCwUAMHoxCzAJBgNV"
			+ "BAYTAkZSMSowKAYDVQQKDCFNSU5JU1RFUkUgREVTIEFGRkFJUkVTIEVUUkFOR0VS"
			+ "RVMxHDAaBgNVBAsMEzAwMDIgMTIwMDA2MDEwMDAwMjUxITAfBgNVBAMMGFZPVEUg"
			+ "TUFFREkgUVVBTElGIENFUlQgMTAeFw0xNjEwMTQxMjMwMzZaFw0xNzEwMTQxMjMw"
			+ "MzZaMGAxGjAYBgNVBAMMEXN3aXNzcG9zdCBSb290IENBMRYwFAYDVQQLDA1Pbmxp"
			+ "bmUgVm90aW5nMRIwEAYDVQQKDAlTd2lzc1Bvc3QxCTAHBgNVBAcMADELMAkGA1UE"
			+ "BhMCQ0gwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCFvnVSzHfyBzty"
			+ "kMeIroPTKUCyoBJGW4gdizxz/HItDbVJ/JF7zELSVGCO+r/JXXR077hzFBX3EVBa"
			+ "ExcTtNGAJdTV0z1FjwA9PTRNr4zlDGQZ2q9x3HWpv9IpgEVkWYky17ieUSGHZ2/1"
			+ "WYZYEpQCP3Jo80QJBFjctNoiiNAVp9S61sbGwEUXhYW7mfzIVki2oudRZtKvG+CV"
			+ "7eW+cup8IRByN2PSIoyla4OmO3ZlhuqXM+VqVz76xNHNGD4ELLMq8Si0Iak78akz"
			+ "7Y8eLhfOgAyfZzclaR7MCH6hRW9i9Dr+/fLM9pqg22o+UsGn475upxU0nriHGefH"
			+ "8TgDlNGzAgMBAAGjIzAhMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEG"
			+ "MA0GCSqGSIb3DQEBCwUAA4IBAQBPu3jgcBue+YnokZjFOTZkmU54yzNsvKX08OS8"
			+ "oOUyfbYb2nGZ6ErvjWokbnNVQJporFiszqIxgWoh74XAamFJyGmH+4lhZyYKYiuE"
			+ "UPHGFz5QIIw79uIufVK0ZEdHZ3JvvpLA6U8wOtsj+3OWkaXZ2ynlrZxYitWJmG2x"
			+ "tcVAiLF32GNBI2LptaWVkVzZxU0oGFxI+W8fl4uu6wA9dMAZzBRV4pLjPbO5bzeO"
			+ "9FbnpS4FcRpzsXij/wbcsmjYYHhE2mJJan8ozLXAq9ZmsNRskmmDnajHDx5RGB+A"
			+ "FSK50YMbV4QbgdDwTg/FBfj+wZS46ra2Bxs1uYgOR0qL6xUF" + "-----END CERTIFICATE-----";

	private String maedi_ca = "-----BEGIN CERTIFICATE-----"
			+ "MIIFVzCCAz+gAwIBAgIBJjANBgkqhkiG9w0BAQsFADB8MQswCQYDVQQGEwJGUjEq"
			+ "MCgGA1UECgwhTUlOSVNURVJFIERFUyBBRkZBSVJFUyBFVFJBTkdFUkVTMRwwGgYD"
			+ "VQQLDBMwMDAyIDEyMDAwNjAxMDAwMDI1MSMwIQYDVQQDDBpBQyBRVUFMSUYgSU5G"
			+ "UkFTVFJVQ1RVUkUgMjAeFw0xNjA5MTkxMjM4NDdaFw0xOTA5MjAxMjM4NDdaMHox"
			+ "CzAJBgNVBAYTAkZSMSowKAYDVQQKDCFNSU5JU1RFUkUgREVTIEFGRkFJUkVTIEVU"
			+ "UkFOR0VSRVMxHDAaBgNVBAsMEzAwMDIgMTIwMDA2MDEwMDAwMjUxITAfBgNVBAMM"
			+ "GFZPVEUgTUFFREkgUVVBTElGIENFUlQgMTCCASIwDQYJKoZIhvcNAQEBBQADggEP"
			+ "ADCCAQoCggEBAKpLiXXg5MU/d+N2WZXBZHgfHLBmRiFVYlN+zNY62a4/B2tAOcAS"
			+ "okzWpjvUD3IWTYtsAqwLYNqmu/4yUtgRW+tZcspqKELzRkunRwNkQY2GXOSbbeCp"
			+ "WJbYfMNy/GNM5AFdZ2jfNQ16abWhvb1spa5zaMQ3aUdzjyUjxCuGaQeBzPIguQKO"
			+ "4il0fD1EZN1fAXtSqvscLDJDRvyfi/d31o/N+xVpKMqmWrC3i52YbucjZegsTRxt"
			+ "5+By6yRTkagQ15P8T9VyXCGKzrmm1rS6VG34M+Tv0bxPixdpazr2setU2Z/IDV/7"
			+ "KZ4fJWY5cp3jcU8skyMXGbX27+oE6zAm+EkCAwEAAaOB5TCB4jAPBgNVHRMBAf8E"
			+ "BTADAQH/MB0GA1UdDgQWBBRi1YYxIe8J6kWZvNyr+1nZIpxwXDAfBgNVHSMEGDAW"
			+ "gBTheTaIhCB7OhhL1Skw/GPTQEkxRDAaBgNVHSAEEzARMA8GDSqBegGBVkUDAQUB"
			+ "AQEwDgYDVR0PAQH/BAQDAgEGMGMGA1UdHwRcMFowWKBWoFSGUmh0dHA6Ly9jcmwu"
			+ "cHJlcC5kaXBsb21hdGllLmdvdXYuZnIvQUNfSW5mcmFzdHJ1Y3R1cmVfMi9Dcmwv"
			+ "QUNfSU5GUkFTVFJVQ1RVUkVfMi5jcmwwDQYJKoZIhvcNAQELBQADggIBAHQIa69b"
			+ "WIA/BxiBiGxCNNUIHHyT5CrY4b6czyM25TCt+hUcmed1XVvGCkdjQoGvcUSqt7qq"
			+ "wG7kc32+Vd3d3lWjtxtwse0CnXeb+wpbVICPEUNq/CpKIWE89wfpSSwRLQ7f9iIo"
			+ "RGiDJ9wsaN0XiX/pusdy+rv4y0xTaqqPniTq87izvSIf0Wl88UjFmjczYUkcK6up"
			+ "awHgujabGlZPcaofTDL4Ymvinx7NQTLpTTs/2Iaklbh/jcXQSLORiEAJ0Wo/XWaK"
			+ "moNOPh2XwVk+xsZM6OalHaUrba8nMLRphiiSNcwa93YBCeqMFHTk5Qwzhxi+GGEw"
			+ "zj8a5yomM06OTLDr2eCBg04gjr0JVCmeQwAqmczV72z1z/H8A0Qjx8sGRTYHzRZa"
			+ "0f/lfNjURE9gLnYqt8IcmhYic5p0ze9fS5VYsMH2M9Lt3Z7+atYGsk8XDCmzYE+r"
			+ "GEHxFlcRGXxgtzlNuZfo0Ol7M1KXt6cQ8y32fNQxcYqDNYAFJw+5MM3u6kcHgM8q"
			+ "H9LSTwA4yfcCiP9DJIN76kuu45MNz/U5IOvmRZOZiah8kZD+qxtSfCJgPgWWBPLI"
			+ "2B2deW2g47pg86BZby15vZoi4W85MwrW5hnQJrHFVxrJPYW7hA+7QanaXpsDyuxd"
			+ "+YWCKcbGrobLgk1LZFb6qnaECrYiq8K9pCLh" + "-----END CERTIFICATE-----";
	
	private HttpServletRequest request;

	@Before
	public void setUp() {
		crPlatformDataResource = new CrPlatformDataResource();
		crPlatformDataResource.loggingKeystoreRepository = mock(LoggingKeystoreRepository.class);
		crPlatformDataResource.certificateValidationService = new CertificateValidationServiceImpl();
		crPlatformDataResource.certificateRepository = mock(CertificateRepository.class);
		crPlatformDataResource.transactionInfoProvider = mock(TransactionInfoProvider.class);
		crPlatformDataResource.httpRequestService = mock(HttpRequestService.class);
		request = mock(HttpServletRequest.class);
		when(crPlatformDataResource.httpRequestService.getIpServer(request)).thenReturn("127.0.0.1");
		when(crPlatformDataResource.httpRequestService.getIpClientAddress(request)).thenReturn("127.0.0.1");
	}

	@Test
	public void testPlatformCertificateChain() {
		PlatformInstallationData data = new PlatformInstallationData();
		try {
			data.setPlatformRootCaPEM(platformrecertified);
			data.setPlatformRootIssuerCaPEM(maedi_ca);
			crPlatformDataResource.savePlatformData(data, request);
		} catch (Exception e) {
			Assert.assertTrue(e.getCause().getMessage().contains("CR - error while trying to load properties"));
		}

	}

	@Test
	public void testInvalidPlatformCertificateChain() {
		PlatformInstallationData data = new PlatformInstallationData();
		try {
			data.setPlatformRootCaPEM(maedi_ca);
			data.setPlatformRootIssuerCaPEM(platform);
			crPlatformDataResource.savePlatformData(data, request);
			Assert.fail();
		} catch (Exception e) {
			Assert.assertEquals("Certificate validation failed for the following validation types: [SIGNATURE]",
					e.getCause().getMessage());
		}

	}

	@Test
	public void testSelfSignedPlatformCertificate() {
		PlatformInstallationData data = new PlatformInstallationData();
		try {
			data.setPlatformRootCaPEM(platform);
			crPlatformDataResource.savePlatformData(data, request);
		} catch (Exception e) {
			Assert.assertTrue(e.getCause().getMessage().contains("CR - error while trying to load properties"));
		}

	}

	@Test
	public void testInvalidSelfSignedPlatformCertificate() {
		PlatformInstallationData data = new PlatformInstallationData();
		try {
			data.setPlatformRootCaPEM(platformrecertified);
			crPlatformDataResource.savePlatformData(data, request);
			Assert.fail();
		} catch (Exception e) {
			Assert.assertEquals("Certificate validation failed for the following validation types: [SIGNATURE]",
					e.getCause().getMessage());
		}

	}

}
