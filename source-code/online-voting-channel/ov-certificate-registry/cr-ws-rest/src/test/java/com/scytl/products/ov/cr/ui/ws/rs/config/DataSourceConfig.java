/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * Created by lsantamaria on 3/02/17.
 */
package com.scytl.products.ov.cr.ui.ws.rs.config;

import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
@DataSourceDefinition(user = "sa", password = "", className = "org.h2.Driver", name = "crDB", databaseName = "crDB", url = "jdbc:h2:./target/h2-test;MODE=ORACLE", properties={"logSql=true"})
public class DataSourceConfig {

}
