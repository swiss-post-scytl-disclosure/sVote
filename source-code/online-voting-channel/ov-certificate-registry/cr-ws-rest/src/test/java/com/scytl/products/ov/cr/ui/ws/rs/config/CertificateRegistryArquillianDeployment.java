/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.cr.ui.ws.rs.config;

import com.scytl.products.ov.cr.ui.ws.rs.operation.SystemPropertiesLoader;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;


@ArquillianSuiteDeployment
public class CertificateRegistryArquillianDeployment {

    private static SystemPropertiesLoader spl = new SystemPropertiesLoader();

    @Deployment
    public static WebArchive buildDeployable() {
        beforeDeployment();
        WebArchive webArchive =  ShrinkWrap.create(WebArchive.class, "cr-ws-rest.war")
                .addClasses(org.slf4j.Logger.class)
                .addPackage("com.scytl.products.oscore")
                .addPackages(true, "com.scytl.cryptolib")
                .addPackages(true, "org.h2")
                .addPackages(true, "com.fasterxml.jackson.jaxrs")
                .addPackages(true, "com.scytl.products.ov.cr")
                .addPackages(true, "com.scytl.products.ov.commons")
                .deletePackages(true, "com.scytl.products.ov.commons.infrastructure.health")
                .addAsResource("log4j.xml")
                .addAsManifestResource("test-persistence.xml", "persistence.xml")
                .addAsWebInfResource("beans.xml");
        return webArchive;
    }

    private static void beforeDeployment(){
        spl.setProperties();
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
}
