
--------------------------------------------------------
--  DDL for Sequence PLATFORM_CERTIFICATES_SEQ
--------------------------------------------------------
CREATE SEQUENCE  "CR_LOGGING_KEYSTORES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;



--------------------------------------------------------
--  DDL for Table LOGGING_KEYSTORES
--------------------------------------------------------

  CREATE TABLE "CR_LOGGING_KEYSTORES"
   (
    "ID" NUMBER(11,0),
    "PLATFORM_NAME" VARCHAR2(100 BYTE),
    "KEY_TYPE" VARCHAR2(100 BYTE),
	"KEYSTORE_CONTENT" CLOB
   ) ;



--------------------------------------------------------
--  DDL for Index LOGGING_KEYSTORES_UK
--------------------------------------------------------

CREATE UNIQUE INDEX "CR_LOGGING_KEYSTORES_UK" ON "CR_LOGGING_KEYSTORES" ("PLATFORM_NAME","KEY_TYPE");



--------------------------------------------------------
--  Constraints for Table LOGGING_KEYSTORES
--------------------------------------------------------

  ALTER TABLE "CR_LOGGING_KEYSTORES" ADD CONSTRAINT "CR_LOGGING_KEYSTORES_UK" UNIQUE ("PLATFORM_NAME", "KEY_TYPE") ENABLE;
  ALTER TABLE "CR_LOGGING_KEYSTORES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CR_LOGGING_KEYSTORES" MODIFY ("PLATFORM_NAME" NOT NULL ENABLE);
  ALTER TABLE "CR_LOGGING_KEYSTORES" MODIFY ("KEY_TYPE" NOT NULL ENABLE);
  ALTER TABLE "CR_LOGGING_KEYSTORES" MODIFY ("KEYSTORE_CONTENT" NOT NULL ENABLE);

