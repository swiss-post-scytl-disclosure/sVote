/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.cr.services.infrastructure.log;

import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.logging.config.SecureLoggerInitializer;
import com.scytl.products.ov.commons.logging.domain.model.LoggingKeystoreRepository;
import com.scytl.products.ov.cr.services.domain.model.platform.CrLoggingKeystoreRepository;

/**
 * Defines any steps to be performed when the CR context is first initialized and destroyed.
 */
public class CrContextListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private static final String CONTEXT = "CR";

    private static final String ENCRYPTION_PW_PROPERTIES_KEY = "CR_log_encryption";

    private static final String SIGNING_PW_PROPERTIES_KEY = "CR_log_signing";

    @EJB
    @CrLoggingKeystoreRepository
    LoggingKeystoreRepository loggingKeystoreRepository;

    @EJB
    CrLoggingInitializationState crLoggingInitializationState;

    /**
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {

        LOG.info(CONTEXT + " - context initialized, will attempt to initialize logging if data exists in DB");

        SecureLoggerInitializer secureLoggerInitializer = new SecureLoggerInitializer(crLoggingInitializationState,
            loggingKeystoreRepository, CONTEXT, ENCRYPTION_PW_PROPERTIES_KEY, SIGNING_PW_PROPERTIES_KEY);

        if (secureLoggerInitializer.initializeIfDataExistsInDB()) {
            LOG.info(CONTEXT + " - initialized logging correctly from DB");
            crLoggingInitializationState.setInitialized(true);
        } else {
            LOG.info(CONTEXT + " - logging not initialized - did not find keystores in DB");
        }
    }

    /**
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(final ServletContextEvent sce) {

    }
}
