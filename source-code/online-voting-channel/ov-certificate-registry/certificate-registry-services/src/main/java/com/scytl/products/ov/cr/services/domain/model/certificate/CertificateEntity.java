/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.cr.services.domain.model.certificate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scytl.products.ov.commons.domain.model.Constants;
import com.scytl.products.ov.commons.domain.model.platform.PlatformCAEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.persistence.UniqueConstraint;

/**
 * Entity representing the info of a Certificate
 */
@Entity
@Table(name = "CERTIFICATES_REGISTRY", uniqueConstraints =
@UniqueConstraint(name = "CERTIFICATES_REGISTRY_UK1", columnNames = {"CERTIFICATE_NAME", "PLATFORM_NAME", "TENANT_ID" }))

public class CertificateEntity extends PlatformCAEntity {

    /**
     * The identifier for this cerficate
     */
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "certificatesRegistrySeq")
    @SequenceGenerator(name = "certificatesRegistrySeq", sequenceName = "CERTIFICATES_REGISTRY_SEQ")
    @JsonIgnore
    private Long id;

    /**
     * The identifier of a tenant for the current certificate.
     */
    @Column(name = "TENANT_ID")
    @Size(max = Constants.COLUMN_LENGTH_100)
    @JsonIgnore
    private String tenantId;

    /**
     * The identifier of an election event for the current certificate.
     */
    @Column(name = "ELECTION_EVENT_ID")
    @Size(max = Constants.COLUMN_LENGTH_100)
    @JsonIgnore
    private String electionEventId;

    /**
     * Gets The identifier for this certificate..
     *
     * @return Value of The identifier for this ballot..
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the identifier for this certificate..
     *
     * @param id
     *            New value of The identifier for this ballot..
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets The identifier of an election event for the current certificate..
     *
     * @return Value of The identifier of an election event for the current
     *         ballot..
     */
    public String getElectionEventId() {
        return electionEventId;
    }

    /**
     * Sets the identifier of an election event for the current certificate..
     *
     * @param electionEventId
     *            New value of The identifier of an election event for the
     *            current certificate..
     */
    public void setElectionEventId(String electionEventId) {
        this.electionEventId = electionEventId;
    }

    /**
     * Gets The identifier of a tenant for the current certificate..
     *
     * @return Value of The identifier of a tenant for the current certificate..
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * Sets the identifier of a tenant for the current certificate..
     *
     * @param tenantId
     *            New value of The identifier of a tenant for the current
     *            certificate..
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

}
