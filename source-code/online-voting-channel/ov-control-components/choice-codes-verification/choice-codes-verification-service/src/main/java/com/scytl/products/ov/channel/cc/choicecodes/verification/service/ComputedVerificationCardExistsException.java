/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.choicecodes.verification.service;

public class ComputedVerificationCardExistsException extends Exception {
    private static final long serialVersionUID = 3512258354343313543L;
    
    public ComputedVerificationCardExistsException(String msg, Throwable t) {
        super(msg, t);
    }
}


