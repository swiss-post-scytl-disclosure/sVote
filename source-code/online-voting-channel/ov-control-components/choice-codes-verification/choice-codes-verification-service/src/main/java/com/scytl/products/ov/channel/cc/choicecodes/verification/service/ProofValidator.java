/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.choicecodes.verification.service;

import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.products.ov.commons.dto.ChoiceCodesVerificationInput;

public interface ProofValidator {

	boolean validate(ZpSubgroup encryptionParameters, ChoiceCodesVerificationInput choiceCodesVerificationInput);

}