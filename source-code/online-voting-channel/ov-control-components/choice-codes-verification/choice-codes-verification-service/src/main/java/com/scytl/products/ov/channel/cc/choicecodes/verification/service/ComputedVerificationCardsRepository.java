/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.choicecodes.verification.service;


public interface ComputedVerificationCardsRepository {
    
    /**
     * Saves the computed status of a verification card
     * @param electionEventId
     * @param verificationCardId
     * @throws ComputedVerificationCardExistsException 
     */
    void saveComputedVerificationCard(String electionEventId, String verificationCardId) throws ComputedVerificationCardExistsException;

    /**
     * Retrieves the cast attempts value
     * @param electionEventId
     * @param verificationCardId
     * @return cast attempts
     * @throws ComputedVerificationCardNotFoundException 
     */
    int getComputeVerificationCardVoteCastAttempts(String electionEventId, String verificationCardId) throws ComputedVerificationCardNotFoundException;

    /**
     * Updates the cast attempts value by increasing it by 1
     * @param electionEventId
     * @param verificationCardId
     * @throws ComputedVerificationCardNotFoundException
     */
    void updateComputedVerificationCardVoteCastAttempt(String electionEventId, String verificationCardId) throws ComputedVerificationCardNotFoundException;

}


