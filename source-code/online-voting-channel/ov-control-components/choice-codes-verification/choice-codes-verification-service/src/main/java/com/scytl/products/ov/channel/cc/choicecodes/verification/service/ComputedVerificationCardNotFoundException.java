/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.choicecodes.verification.service;

public class ComputedVerificationCardNotFoundException extends Exception {
    private static final long serialVersionUID = 1770612747049760627L;

    public ComputedVerificationCardNotFoundException(String msg) {
        super(msg);
    }

}
