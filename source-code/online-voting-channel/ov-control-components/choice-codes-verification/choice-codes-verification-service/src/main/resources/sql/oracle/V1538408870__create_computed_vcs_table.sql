  CREATE TABLE "COMPUTED_VERIFICATION_CARDS"
   (
    "ELECTION_EVENT_ID" VARCHAR2(100) NOT NULL,
	"VERIFICATION_CARD_ID" VARCHAR2(100) NOT NULL,
	CONSTRAINT COMPUTED_VERIFICATION_CARDS_PK PRIMARY KEY ("ELECTION_EVENT_ID", "VERIFICATION_CARD_ID")
   );