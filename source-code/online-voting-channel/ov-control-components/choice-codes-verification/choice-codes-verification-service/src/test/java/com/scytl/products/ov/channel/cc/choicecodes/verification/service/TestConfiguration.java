/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.choicecodes.verification.service;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.cryptolib.proofs.service.ProofsService;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.channel.cc.commons.slogger.ServiceTransactionInfo;
import com.scytl.products.ov.channel.cc.commons.slogger.ServiceTransactionInfoProvider;
import com.scytl.products.ov.channel.cc.commons.util.FingerprintGenerator;

@Configuration
@Import({ProofValidatorImpl.class })
public class TestConfiguration {

    @Bean
    public ProofsServiceAPI proofsService() throws GeneralCryptoLibException {
        return new ProofsService();
    }

    @Bean
    public SecureLoggingWriter secureLoggingWriter() {
        return Mockito.mock(SecureLoggingWriter.class);
    }

    @Bean
    public FingerprintGenerator fingerprintGenerator() {
        return Mockito.mock(FingerprintGenerator.class);
    }

    @Bean
    public ServiceTransactionInfoProvider serviceTransactionInfoProvider() {
        ServiceTransactionInfoProvider serviceTransactionInfoProvider =
            Mockito.mock(ServiceTransactionInfoProvider.class);
        ServiceTransactionInfo trInfo = Mockito.mock(ServiceTransactionInfo.class);
        Mockito.when(serviceTransactionInfoProvider.getServiceTransactionInfo()).thenReturn(trInfo);
        Mockito.when(trInfo.getTrackId()).thenReturn("trackid");
        return serviceTransactionInfoProvider;
    }

}
