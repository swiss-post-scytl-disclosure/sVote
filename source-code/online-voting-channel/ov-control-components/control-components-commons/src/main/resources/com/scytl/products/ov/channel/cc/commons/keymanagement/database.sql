create table cc_node_keys (
    node_id varchar(10) not null,
    keys blob not null,
    constraint cc_node_keys_pk primary key (node_id)
);

create table cc_election_signing_keys (
    node_id varchar(10) not null,
    election_event_id varchar(100) not null,
    keys blob not null,
    password blob not null,
    constraint cc_election_signing_keys_pk primary key (node_id, election_event_id)
);

create table cc_mixing_keys (
    node_id varchar(10) not null,
    election_event_id varchar(100) not null,
    electoral_authority_id varchar(100) not null,
    private_key blob not null,
    public_key blob not null,
    signature blob not null,
    constraint cc_mixing_keys_pk primary key (node_id, election_event_id, electoral_authority_id)
);

create table cc_choice_code_keys (
    node_id varchar(10) not null,
    election_event_id varchar(100) not null,
    verification_card_set_id varchar(100) not null,
    encryption_parameters blob not null,
    generation_private_key blob not null,
    generation_public_key blob not null,
    generation_signature blob not null,
    decryption_private_key blob not null,
    decryption_public_key blob not null,
    decryption_signature blob not null,
    constraint cc_choice_code_keys_pk primary key (node_id, election_event_id, verification_card_set_id)
);