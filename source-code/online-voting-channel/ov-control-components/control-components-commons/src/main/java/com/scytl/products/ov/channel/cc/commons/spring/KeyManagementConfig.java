/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import java.time.Duration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.cryptolib.api.certificates.CertificatesServiceAPI;
import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.api.stores.StoresServiceAPI;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.channel.cc.commons.keymanagement.KeyManager;
import com.scytl.products.ov.channel.cc.commons.keymanagement.KeyManagerImpl;
import com.scytl.products.ov.channel.cc.commons.keymanagement.NodeKeysActivator;
import com.scytl.products.ov.channel.cc.commons.keymanagement.NodeKeysActivatorImpl;
import com.scytl.products.ov.channel.cc.commons.slogger.ServiceTransactionInfoProvider;
import com.scytl.products.ov.channel.cc.commons.util.FingerprintGenerator;

/**
 * Key management configuration.
 */
@Configuration
public class KeyManagementConfig {
    @Value("${keys.nodeId}")
    private String nodeId;

    private Duration cacheExpiryPeriod;

    @Autowired
    private SecureLoggingWriter secureLoggingWriter;

    @Autowired
    private FingerprintGenerator fingerprintGenerator;

    @Autowired
    private ServiceTransactionInfoProvider serviceTransactionInfoProvider;

    @Value("${keys.cacheExpiryPeriod:60000}")
    public void setCacheExpiryPeriod(String value) {
        cacheExpiryPeriod = Duration.ofMillis(Long.parseLong(value));
    }

    /**
     * Defines the key manager.
     * 
     * @param dataSource
     *            the transaction aware data source
     * @param asymmetricService
     *            the asymmetric service
     * @param certificatesService
     *            the certificates service
     * @param elGamalService
     *            the ElGamal service
     * @param primitivesService
     *            the primitives service
     * @param storesService
     *            the stores service
     * @return the key manager
     */
    @Bean(initMethod = "startup", destroyMethod = "shutdown")
    public KeyManager keyManager(@Qualifier("TransactionAware") DataSource dataSource,
            AsymmetricServiceAPI asymmetricService, CertificatesServiceAPI certificatesService,
            ElGamalServiceAPI elGamalService, PrimitivesServiceAPI primitivesService, StoresServiceAPI storesService) {
        return new KeyManagerImpl.Builder().setNodeId(nodeId).setDataSource(dataSource)
            .setAsymmetricService(asymmetricService).setCertificatesService(certificatesService)
            .setElGamalService(elGamalService).setPrimitivesService(primitivesService).setStoresService(storesService)
            .setSecureLoggingWriter(secureLoggingWriter).setFingerprintGenerator(fingerprintGenerator)
            .setServiceTransactionInfoProvider(serviceTransactionInfoProvider).setCacheExpiryPeriod(cacheExpiryPeriod)
            .build();
    }

    /**
     * Defines the node keys activator.
     * 
     * @param manager
     *            the key manager
     * @return the service
     */
    @Bean
    public NodeKeysActivator nodeKeysActivator(KeyManager manager) {
        return new NodeKeysActivatorImpl(manager);
    }
}
