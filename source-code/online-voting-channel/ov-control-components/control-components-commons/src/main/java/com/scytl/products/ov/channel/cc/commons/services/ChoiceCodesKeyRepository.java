/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.services;

import java.security.KeyManagementException;
import java.security.cert.X509Certificate;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;
import com.scytl.products.ov.channel.cc.commons.keymanagement.FingerprintGeneratorException;
import com.scytl.products.ov.channel.cc.commons.keymanagement.KeyNotFoundException;
import com.scytl.products.ov.commons.dto.KeyCreationDTO;

public interface ChoiceCodesKeyRepository {

    void addGeneratedKey(KeyCreationDTO toBeReturned)
            throws GeneralCryptoLibException, KeyManagementException, FingerprintGeneratorException;

    ElGamalPrivateKey getGenerationPrivateKey(String electionEventId, String verificationCardSetId)
            throws KeyManagementException;

    ElGamalPrivateKey getDecryptionPrivateKey(String electionEventId, String verificationCardSetId)
            throws KeyManagementException;

    ElGamalPublicKey getDecryptionPublicKey(String electionEventId, String verificationCardSetId)
            throws KeyManagementException;

    ZpSubgroup getEncryptionParameters(String electionEventId, String verificationCardSetId)
            throws KeyManagementException;

    X509Certificate getPlatformCACertificate() throws KeyNotFoundException, KeyManagementException;
}
