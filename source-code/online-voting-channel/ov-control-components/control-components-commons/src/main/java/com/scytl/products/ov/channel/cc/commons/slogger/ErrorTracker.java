/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.slogger;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import org.apache.log4j.spi.ErrorHandler;

import com.scytl.slogger.SecureLoggerException;

/**
 * Error tracker is an extension of {@link ErrorHandler} which allows to check
 * if there was no error.
 */
@ThreadSafe
interface ErrorTracker extends ErrorHandler {
    /**
     * Checks whether there was no error. The error if any is re-thrown as
     * {@link SecureLoggerException}.
     * 
     * @throws SecureLoggerException
     *             there was an error.
     */
    void checkNoError() throws SecureLoggerException;

    /**
     * Sets the target`.
     * 
     * @param target
     *            the target or {@code null}.
     */
    void setTarget(@Nullable ErrorHandler target);
}
