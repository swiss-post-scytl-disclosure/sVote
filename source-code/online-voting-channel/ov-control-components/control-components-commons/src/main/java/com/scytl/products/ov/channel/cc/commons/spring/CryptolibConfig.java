/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import com.scytl.cryptolib.api.certificates.CertificatesServiceAPI;
import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.primitives.PrimitivesServiceAPI;
import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.cryptolib.api.stores.StoresServiceAPI;
import com.scytl.cryptolib.asymmetric.service.PollingAsymmetricServiceFactory;
import com.scytl.cryptolib.certificates.service.PollingCertificatesServiceFactory;
import com.scytl.cryptolib.elgamal.service.PollingElGamalServiceFactory;
import com.scytl.cryptolib.primitives.service.PollingPrimitivesServiceFactory;
import com.scytl.cryptolib.proofs.service.PollingProofsServiceFactory;
import com.scytl.cryptolib.stores.service.PollingStoresServiceFactory;

/**
 * Cryptolib services configuration.
 */
@Configuration
public class CryptolibConfig {

    /**
     * Defines the asymmetric service. The returned service is thread-safe,
     * 
     * @return the asymmetric service.
     */
    @Bean
    public AsymmetricServiceAPI asymmetricService() {
        return new PollingAsymmetricServiceFactory().create();
    }

    /**
     * Defines the certificates service. The returned service is thread-safe,
     * 
     * @return the certificates service.
     */
    @Bean
    public CertificatesServiceAPI certificatesService() {
        return new PollingCertificatesServiceFactory().create();
    }

    /**
     * Defines the ElGamal service. The returned service is thread-safe,
     * 
     * @return the ElGamal service.
     */
    @Bean
    public ElGamalServiceAPI elGamalService() {
        return new PollingElGamalServiceFactory().create();
    }

    /**
     * Defines the primitives service. The returned service is thread-safe,
     * 
     * @return the primitives service.
     */
    @Bean
    public PrimitivesServiceAPI primitivesService() {
        return new PollingPrimitivesServiceFactory().create();
    }

    /**
     * Defines the proof service. The returned service is thread-safe,
     * 
     * @return the proof service.
     */
    @Bean
    public ProofsServiceAPI proofsService() {
        return new PollingProofsServiceFactory().create();
    }

    /**
     * Defines the stores service. The returned service is thread-safe,
     * 
     * @return the stores service.
     */
    @Bean
    public StoresServiceAPI storesService() {
        return new PollingStoresServiceFactory().create();
    }
}
