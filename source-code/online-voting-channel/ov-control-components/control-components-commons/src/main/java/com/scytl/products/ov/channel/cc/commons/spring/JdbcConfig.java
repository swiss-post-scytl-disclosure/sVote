/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import oracle.jdbc.pool.OracleDataSource;

/**
 * JDBC configuration.
 */
public class JdbcConfig {

    @Value("${db.url}")
    private String url;

    @Value("${db.user}")
    private String user;

    @Value("${db.password}")
    private String password;

    @Value("${db.maxPoolSize:20}")
    private int maxPoolSize;

    @Value("${db.idleTimeout:60000}")
    private long idleTimeout;

    /**
     * Defines the original data source.
     * 
     * @return the original data source
     * @throws SQLException
     *             failed to define the data source.
     */
    @Bean(destroyMethod = "close")
    @Qualifier("Original")
    public DataSource originalDataSource() throws SQLException {
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setURL(url);
        dataSource.setUser(user);
        dataSource.setPassword(password);

        HikariConfig config = new HikariConfig();
        config.setDataSource(dataSource);
        config.setMaximumPoolSize(maxPoolSize);
        config.setMinimumIdle(0);
        config.setIdleTimeout(idleTimeout);

        return new HikariDataSource(config);
    }
}
