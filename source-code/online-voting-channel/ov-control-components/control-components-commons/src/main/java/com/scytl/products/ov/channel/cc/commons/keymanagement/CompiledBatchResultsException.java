/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

public final class CompiledBatchResultsException extends RuntimeException {
	
	private static final long serialVersionUID = 8088581307463642003L;

	public CompiledBatchResultsException(Throwable cause) {
		super(cause);
	}

	public CompiledBatchResultsException(String message) {
		super(message);
	}

	public CompiledBatchResultsException(String message, Throwable cause) {
		super(message, cause);
	}
}
