/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.security.KeyManagementException;
import java.security.KeyStore.PasswordProtection;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Generator.
 */
@ThreadSafe
interface Generator {
    /**
     * Generates new choice code keys fora given specification. The generated
     * public keys are signed with the specified election signing keys as
     * follows: <pre>
     * sign(electionEventId + verificationCardSetId + key)
     * </pre>
     * 
     * @param spec the choice codes keys specs
     * 
     * @param electionSigningKeys
     *            the election signing keys
     * @return the generated keys
     * @throws KeyManagementException
     *             failed to generate the keys
     */
    ChoiceCodeKeys generateChoiceCodeKeys(ChoiceCodeKeysSpec spec, ElectionSigningKeys electionSigningKeys)
            throws KeyManagementException;

    /**
     * Generates new election signing keys for given election, validity period.
     * The generated keys are signed with the specified node keys.
     * 
     * @param electionEventId
     *            the election identifier
     * @param validFrom
     *            the start of the validity period
     * @param validTo
     *            the end of the validity period (inclusive)
     * @param nodeKeys
     *            the node keys
     * @return the generated keys
     * @throws KeyManagementException
     *             failed to generate the keys
     */
    ElectionSigningKeys generateElectionSigningKeys(String electionEventId, Date validFrom, Date validTo,
            NodeKeys nodeKeys) throws KeyManagementException;

    /**
     * Generates new mixing keys for a given specification. The generated keys
     * are signed with the specified election signing keys as follows: <pre>
     * sign(electionEventId + electoralAuthorityId + key)
     * </pre>
     * 
     * @param spec
     *            the specification
     * @param electionSigningKeys
     *            the election signing keys
     * @return the generated keys
     * @throws KeyManagementException
     *             failed to generate the keys
     */
    MixingKeys generateMixingKeys(MixingKeysSpec spec, ElectionSigningKeys electionSigningKeys)
            throws KeyManagementException;

    /**
     * Generates new node keys based on given CCN CA keys.
     * 
     * @param caPrivateKey
     *            the CA private key
     * @param caCertificateChain
     *            the CA certificate chain
     * @return the generated keys
     * @throws KeyManagementException
     *             failed to generate the keys.
     */
    NodeKeys generateNodeKeys(PrivateKey caPrivateKey, X509Certificate[] caCertificateChain)
            throws KeyManagementException;

    /**
     * Generates a password.
     * 
     * @return a password
     * @throws KeyManagementException
     *             failed to generate a password.
     */
    PasswordProtection generatePassword() throws KeyManagementException;
}
