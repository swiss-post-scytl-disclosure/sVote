/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.security.KeyManagementException;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Service responsible for the node keys activation.
 */
@ThreadSafe
public interface NodeKeysActivator {

    /**
     * Creates and/or activates node keys. This is a shortcut for
     * {@code activateNodeKeys(folder, "ccncakey")}.
     * 
     * @param folder
     *            the folder with {@code key.p12} and {@code password} files
     * @param nodeCAAlias
     *            the node CCN CA alias
     * @throws InvalidKeyStoreException
     *             the key store is invalid
     * @throws InvalidPasswordException
     *             the password is invalid
     * @throws InvalidNodeCAException
     *             the CCN CA keys and certificates are invalid
     * @throws KeyManagementException
     *             failed to create and/or activate the node keys
     * @throws NoSuchFileException
     *             key store file or password file does not exist
     * @throws IOException
     *             I/O error occurred.
     */
    void activateNodeKeys(Path folder)
            throws InvalidKeyStoreException, InvalidPasswordException,
            InvalidNodeCAException, KeyManagementException,
            NoSuchFileException, IOException;

    /**
     * Creates and/or activates node keys. This is a shortcut for
     * {@code activateNodeKeys(folder.resolve("keys.p12"), nodeCAAlias, folder.resolve("password"))}.
     * 
     * @param folder
     *            the folder with {@code key.p12} and {@code password} files
     * @param nodeCAAlias
     *            the node CCN CA alias
     * @throws InvalidKeyStoreException
     *             the key store is invalid
     * @throws InvalidPasswordException
     *             the password is invalid
     * @throws InvalidNodeCAException
     *             the CCN CA keys and certificates are invalid
     * @throws KeyManagementException
     *             failed to create and/or activate the node keys
     * @throws NoSuchFileException
     *             key store file or password file does not exist
     * @throws IOException
     *             I/O error occurred.
     */
    void activateNodeKeys(Path folder, String nodeCAAlias)
            throws InvalidKeyStoreException, InvalidPasswordException,
            InvalidNodeCAException, KeyManagementException,
            NoSuchFileException, IOException;

    /**
     * <p>
     * Creates and/or activates node keys in the specified {@link KeyManager}
     * using CCN CA keys which are stored in the provided PKCS12 key store and
     * are protected with the provided password.
     * <p>
     * The activation consists of the following steps:
     * <ul>
     * <li>Extract password from the password file.</li>
     * <li>Try activate the node keys which can already exist.</li>
     * <li>If there is no node keys in the database extract CCN CA from the key
     * store, create new node keys and activate them.</li>
     * <li>Finally remove the password file.</li>
     * </ul>
     * 
     * @param keyStoreFile
     *            the PKCS12 key store file
     * @param nodeCAAlias
     *            the CCN CA keys alias
     * @param passwordFile
     *            the password file
     * @throws InvalidKeyStoreException
     *             the key store is invalid
     * @throws InvalidPasswordException
     *             the password is invalid
     * @throws InvalidNodeCAException
     *             the CCN CA keys and certificates are invalid
     * @throws KeyManagementException
     *             failed to create and/or activate the node keys
     * @throws NoSuchFileException
     *             key store file or password file does not exist
     * @throws IOException
     *             I/O error occurred.
     */
    void activateNodeKeys(Path keyStoreFile, String nodeCAAlias,
            Path passwordFile)
            throws InvalidKeyStoreException, InvalidPasswordException,
            InvalidNodeCAException, KeyManagementException,
            NoSuchFileException, IOException;
}
