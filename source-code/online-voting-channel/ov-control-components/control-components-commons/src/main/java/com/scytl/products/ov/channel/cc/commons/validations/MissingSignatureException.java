/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.validations;

/**
 * The payload cannot be used as it is not signed.
 */
public class MissingSignatureException extends Exception {

    private static final long serialVersionUID = 1366728663878581163L;

    private static final String ERROR_MESSAGE_TEMPLATE = "Payload %s is not signed";

    public MissingSignatureException(String id) {
        super(String.format(ERROR_MESSAGE_TEMPLATE, id));
    }
}
