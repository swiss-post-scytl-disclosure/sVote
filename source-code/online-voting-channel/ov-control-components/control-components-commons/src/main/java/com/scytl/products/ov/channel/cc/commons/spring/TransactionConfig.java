/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Transaction configuration.
 */
@Configuration
@EnableTransactionManagement
public class TransactionConfig {
    /**
     * Defines the transaction-aware data source.
     * 
     * @param dataSource
     *            the original data source.
     * @return the transaction-aware data source.
     */
    @Bean
    @Qualifier("TransactionAware")
    public DataSource transactionAwareDataSourceProxy(
            @Qualifier("Original") DataSource dataSource) {
        return new TransactionAwareDataSourceProxy(dataSource);
    }

    /**
     * Defines the transaction manager.
     * 
     * @param dataSource
     *            the original data source
     * @return the transaction manager.
     */
    @Bean
    public PlatformTransactionManager transactionManager(
            @Qualifier("Original") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
