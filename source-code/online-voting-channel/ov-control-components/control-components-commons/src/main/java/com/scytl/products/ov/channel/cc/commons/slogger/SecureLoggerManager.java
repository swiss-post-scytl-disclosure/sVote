/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.slogger;

import javax.annotation.concurrent.ThreadSafe;

import com.scytl.slogger.SecureLoggerException;

/**
 * Secure logger manager.
 */
@ThreadSafe
public interface SecureLoggerManager {
    /**
     * The default secure logger name.
     */
    public static final String DEFAULT_SECURE_LOGGER_NAME = "SecureLogger";

    /**
     * Closes the default secure logger. This is a shortcut for
     * {@code closeSecureLogger(DEFAULT_SECURE_LOGGER_NAME)}.
     * 
     * @throws SecureLoggerException
     *             failed to close the logger.
     */
    void closeSecureLogger() throws SecureLoggerException;

    /**
     * Closes the specified secure log.
     * 
     * @param name
     *            the logger name
     * @throws SecureLoggerException
     *             failed to close the logger.
     */
    void closeSecureLogger(String name) throws SecureLoggerException;

    /**
     * Opens the default secure logger. This is a shortcut for
     * {@code openSecureLogger(DEFAULT_SECURE_LOGGER_NAME)}.
     * 
     * @throws SecureLoggerException
     *             failed to activate the logger.
     */
    void openSecureLogger() throws SecureLoggerException;

    /**
     * Opens the specified secure logger.
     * 
     * @param name
     *            the logger name
     * @throws SecureLoggerException
     *             failed to activate the logger.
     */
    void openSecureLogger(String name) throws SecureLoggerException;
}
