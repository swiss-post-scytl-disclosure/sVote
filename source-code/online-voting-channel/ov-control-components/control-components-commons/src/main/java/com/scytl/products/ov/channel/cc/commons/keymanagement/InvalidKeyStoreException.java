/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.security.KeyManagementException;

/**
 * Key store is invalid.
 */
@SuppressWarnings("serial")
public final class InvalidKeyStoreException
        extends KeyManagementException {
    /**
     * Constructor.
     * 
     * @param message
     *            the message.
     */
    public InvalidKeyStoreException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause.
     */
    public InvalidKeyStoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
