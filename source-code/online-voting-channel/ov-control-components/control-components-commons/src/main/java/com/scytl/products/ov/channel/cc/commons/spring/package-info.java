/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * This package provides common classes for using Spring Framework.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.scytl.products.ov.channel.cc.commons.spring;
