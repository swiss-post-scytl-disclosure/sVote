/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

public final class FingerprintGeneratorException extends Exception {
	
	private static final long serialVersionUID = 4632665266864988092L;

	public FingerprintGeneratorException(Throwable cause) {
		super(cause);
	}

	public FingerprintGeneratorException(String message) {
		super(message);
	}

	public FingerprintGeneratorException(String message, Throwable cause) {
		super(message, cause);
	}
}
