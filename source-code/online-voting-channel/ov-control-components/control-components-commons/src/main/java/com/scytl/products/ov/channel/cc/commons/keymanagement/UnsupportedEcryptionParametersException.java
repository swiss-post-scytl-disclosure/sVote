/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.security.KeyManagementException;

/**
 * Key already exists.
 */
public final class UnsupportedEcryptionParametersException extends KeyManagementException {

    private static final long serialVersionUID = -4001558457944171148L;

    /**
     * Constructor.
     * 
     * @param message
     *            the message.
     */
    public UnsupportedEcryptionParametersException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause.
     */
    public UnsupportedEcryptionParametersException(String message, Throwable cause) {
        super(message, cause);
    }
}
