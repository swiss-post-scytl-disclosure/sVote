/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;

/**
 * Mixing keys container.
 */
class MixingKeys {
    private final ElGamalPrivateKey privateKey;

    private final ElGamalPublicKey publicKey;

    private final byte[] publicKeySignature;

    /**
     * Constructor.
     * 
     * @param privateKey
     *            the private key
     * @param publicKey
     *            the public key
     * @param publicKeySignature
     *            the public key signature
     */
    public MixingKeys(ElGamalPrivateKey privateKey,
            ElGamalPublicKey publicKey, byte[] publicKeySignature) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.publicKeySignature = publicKeySignature;
    }

    /**
     * Returns the private key.
     * 
     * @return the private key.
     */
    public ElGamalPrivateKey privateKey() {
        return privateKey;
    }

    /**
     * Returns the public key.
     * 
     * @return the public key.
     */
    public ElGamalPublicKey publicKey() {
        return publicKey;
    }

    /**
     * Returns the public key signature.
     * 
     * @return the public key signature.
     */
    public byte[] publicKeySignature() {
        return publicKeySignature;
    }
}
