/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.exponentiation.exception;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 *
 */
public class CryptoLibProofException extends GeneralCryptoLibException {

    private static final long serialVersionUID = -4073825160489723099L;

    public CryptoLibProofException(String message, Throwable e) {
        super(message, e);
    }

}
