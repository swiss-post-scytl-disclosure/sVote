/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.security.KeyManagementException;
import java.security.KeyStore.PasswordProtection;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Database.
 */
@ThreadSafe
interface Database {
    /**
     * Loads the choice code keys for given election and verification card set.
     * 
     * @param electionEventId
     *            the election identifier
     * @param verificationCardSetId
     *            the verification card set identifier
     * @return the keys
     * @throws KeyNotFoundException
     *             the keys not found
     * @throws KeyManagementException
     *             failed to load the keys.
     */
    ChoiceCodeKeys loadChoiceCodeKeys(String electionEventId, String verificationCardSetId)
            throws KeyNotFoundException, KeyManagementException;

    /**
     * Loads election signing keys for a given election.
     * 
     * @param electionEventId
     *            th election identifier
     * @return the keys
     * @throws KeyNotFoundException
     *             the keys not found
     * @throws KeyManagementException
     *             failed to load the keys.
     */
    ElectionSigningKeys loadElectionSigningKeys(String electionEventId)
            throws KeyNotFoundException, KeyManagementException;

    /**
     * Loads the mixing keys for given election and electoral authority.
     * 
     * @param electionEventId
     *            the election identifier
     * @param electoralAuthorityId
     *            the electoral authority identifier
     * @return the keys
     * @throws KeyNotFoundException
     *             the keys not found
     * @throws KeyManagementException
     *             failed to load the keys.
     */
    MixingKeys loadMixingKeys(String electionEventId, String electoralAuthorityId)
            throws KeyNotFoundException, KeyManagementException;

    /**
     * Loads node keys protected with a given password.
     * 
     * @param password
     *            the password
     * @return the keys
     * @throws KeyNotFoundException
     *             the keys not found
     * @throws InvalidPasswordException
     *             the password is invalid
     * @throws KeyManagementException
     *             failed to load the keys.
     */
    NodeKeys loadNodeKeys(PasswordProtection password)
            throws KeyNotFoundException, InvalidPasswordException, KeyManagementException;

    /**
     * Saves given choice code keys for the specified election and verification
     * card set.
     * 
     * @param electionEventId
     *            the election identifier
     * @param verificationCardSetId
     *            the verification card set identifier
     * @param keys
     *            the keys
     * @throws KeyAlreadyExistsException
     *             the keys already exist
     * @throws KeyManagementException
     *             failed to save the keys.
     */
    void saveChoiceCodeKeys(String electionEventId, String verificationCardSetId, ChoiceCodeKeys keys)
            throws KeyAlreadyExistsException, KeyManagementException;

    /**
     * Saves given election singing keys for the specified election.
     * 
     * @param electionEventId
     *            the election identifier
     * @param keys
     *            the keys
     * @throws KeyAlreadyExistsException
     *             the keys already exist
     * @throws KeyManagementException
     *             failed to save the keys.
     */
    void saveElectionSigningKeys(String electionEventId, ElectionSigningKeys keys)
            throws KeyAlreadyExistsException, KeyManagementException;

    /**
     * Saves given mixing keys for the specified election and electoral
     * authority.
     * 
     * @param electionEventId
     *            the election identifier
     * @param electoralAuthorityId
     *            the electoral authority identifier
     * @param keys
     *            the keys
     * @throws KeyAlreadyExistsException
     *             the keys already exist
     * @throws KeyManagementException
     *             failed to save the keys.
     */
    void saveMixingKeys(String electionEventId, String electoralAuthorityId, MixingKeys keys)
            throws KeyAlreadyExistsException, KeyManagementException;

    /**
     * Saves given node keys and protects them with the specified password.
     * 
     * @param keys
     *            the keys
     * @param password
     *            the password
     * @throws KeyAlreadyExistsException
     *             the keys already exist
     * @throws KeyManagementException
     *             failed to save the keys.
     */
    void saveNodeKeys(NodeKeys keys, PasswordProtection password)
            throws KeyAlreadyExistsException, KeyManagementException;

    /**
     * Sets the encryption keys to be used to protect the sensitive data.
     * 
     * @param privateKey
     *            the private key
     * @param publicKey
     *            the public key
     */
    void setEncryptionKeys(PrivateKey privateKey, PublicKey publicKey);

    /**
     * Checks the existence of the choice code keys for given election and
     * verification card set.
     * 
     * @param electionEventId
     *            the election identifier
     * @param verificationCardSetId
     *            the verification card set identifier
     * @return true if the keys exist, false otherwise
     * @throws KeyManagementException
     *             failed to check the existence of the keys.
     */
    boolean hasMixingKeys(String electionEventId, String electoralAuthorityId) throws KeyManagementException;

    /**
     * Checks the existence of the choice code keys for given election and
     * verification card set.
     * 
     * @param electionEventId
     *            the election identifier
     * @param verificationCardSetId
     *            the verification card set identifier
     * @return true if the keys exist, false otherwise
     * @throws KeyManagementException
     *             failed to check the existence of the keys.
     */
    boolean hasChoiceCodeKeys(String electionEventId, String verificationCardSetId) throws KeyManagementException;

}
