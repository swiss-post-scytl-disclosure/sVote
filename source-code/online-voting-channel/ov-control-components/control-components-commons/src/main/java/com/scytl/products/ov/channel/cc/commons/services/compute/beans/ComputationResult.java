/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.services.compute.beans;

import java.math.BigInteger;
import java.util.Map;

import com.scytl.cryptolib.elgamal.bean.ElGamalComputationsValues;
import com.scytl.cryptolib.mathematical.groups.impl.ZpGroupElement;
import com.scytl.cryptolib.proofs.proof.Proof;

public class ComputationResult {

    private final Map<BigInteger, BigInteger> primeToComputedPrime;

    private final Proof exponentiatonProof;

    private final Map<ElGamalComputationsValues, ElGamalComputationsValues> exponentiationMap;

    private final ZpGroupElement derivedPublicKey;

    public ComputationResult(Map<BigInteger, BigInteger> primeToComputedPrime,
            Map<ElGamalComputationsValues, ElGamalComputationsValues> exponentiationMap, Proof exponentiatonProof,
            ZpGroupElement derivedPublicKey) {
        super();
        this.primeToComputedPrime = primeToComputedPrime;
        this.exponentiatonProof = exponentiatonProof;
        this.exponentiationMap = exponentiationMap;
        this.derivedPublicKey = derivedPublicKey;
    }

    public Map<BigInteger, BigInteger> getPrimeToComputedPrime() {
        return primeToComputedPrime;
    }

    public Map<ElGamalComputationsValues, ElGamalComputationsValues> getExponentiationMap() {
        return exponentiationMap;
    }

    public Proof getExponentiatonProof() {
        return exponentiatonProof;
    }

    public ZpGroupElement getDerivedPublicKey() {
        return derivedPublicKey;
    }
}
