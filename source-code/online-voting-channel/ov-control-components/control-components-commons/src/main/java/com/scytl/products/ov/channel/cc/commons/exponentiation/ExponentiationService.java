/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.exponentiation;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.concurrent.ThreadSafe;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalComputationsValues;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.mathematical.groups.impl.ZpSubgroup;

/**
 * Exponentiation service.
 */
@ThreadSafe
public interface ExponentiationService {
    /**
     * Exponentiate given cleartexts treating them as elements of the specified
     * group.
     * 
     * @param cleartexts
     *            the cleartexts
     * @param exponent
     *            the exponent
     * @param group
     *            the group
     * @return the powers and proof
     * @throws GeneralCryptoLibException
     *             failed to exponentiate the cleartexts.
     */
    PowersAndProof<BigInteger> exponentiateCleartexts(
            List<BigInteger> cleartexts, Exponent exponent,
            ZpSubgroup group)
            throws GeneralCryptoLibException;

    /**
     * Exponentiate given ElGamal cipertexts treating them as elements of the
     * specified group.
     * 
     * @param ciphertexts
     *            the ciphertexts
     * @param exponent
     *            the exponent
     * @param group
     *            the group
     * @return the powers and proof
     * @throws GeneralCryptoLibException
     *             failed to exponentiate the cleartexts.
     */
    PowersAndProof<ElGamalComputationsValues> exponentiateCiphertexts(
            List<ElGamalComputationsValues> ciphertexts, Exponent exponent,
            ZpSubgroup group)
            throws GeneralCryptoLibException;
}
