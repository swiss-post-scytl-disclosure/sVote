/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.security.KeyManagementException;

/**
 * Key already exists.
 */
public final class FailedSigningException extends KeyManagementException {

    private static final long serialVersionUID = -3867396631445335707L;

    /**
     * Constructor.
     * 
     * @param message
     *            the message.
     */
    public FailedSigningException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause.
     */
    public FailedSigningException(String message, Throwable cause) {
        super(message, cause);
    }
}
