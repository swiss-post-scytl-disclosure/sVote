/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.time.Duration;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Keys container.
 */
@ThreadSafe
class KeysContainer {
    private final Object keys;

    private final long expiryPeriod;
    
    private volatile long lastUsedTime = System.currentTimeMillis();

    /**
     * Constructor.
     * 
     * @param keys
     * @param created
     * @param expiryPeriod
     */
    public KeysContainer(Object keys, Duration expiryPeriod) {
        this.keys = keys;
        this.expiryPeriod = expiryPeriod.toMillis();
    }

    /**
     * Returns if the container is expired.
     * 
     * @return the container is expired.
     */
    public boolean isExpired() {
        return System.currentTimeMillis() - lastUsedTime > expiryPeriod;
    }

    /**
     * Returns the keys.
     * 
     * @return the keys.
     */
    public Object getKeys() {
        lastUsedTime = System.currentTimeMillis();
        return keys;
    }
}
