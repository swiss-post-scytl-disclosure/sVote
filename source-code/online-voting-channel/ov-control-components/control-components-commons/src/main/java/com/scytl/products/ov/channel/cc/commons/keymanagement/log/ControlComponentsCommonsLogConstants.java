/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement.log;

/**
 * Constants for logging in control components.
 */
public class ControlComponentsCommonsLogConstants {

    /**
     * Key idVerification card id - additional information.
     */
    public static final String PUBKEY_IDS = "#pubkey_ids";

    /**
     * Control component id
     */
    public static final String CONTROL_COMPONENT_ID = "#ccx_id";

    /**
     * certificate serial number
     */
    public static final String CERT_SN = "#cert_sn";

    /**
     * certificate common name
     */
    public static final String CERT_CN = "#cert_cn";

    /**
     * issuer certificate common name
     */
    public static final String ISSUER_CN = "#issuer_cn";

    /**
     * error description
     */
    public static final String ERR_DESC = "#err_desc=";

    /**
     * verification card id for which the private key is computed"
     */
    public static final String VERIFICATION_CARD_ID = "#vc_id=";

    /**
     * verification card set id for which the private key is computed"
     */
    public static final String VERIFICATION_CARD_SET_ID = "#vcs_id";

    /**
     * ballot casting key proof"
     */
    public static final String BALLOT_CASTING_KEY_PROOF = "#cm_proof";

    /**
     * primes proof"
     */
    public static final String PRIMES_PROOF = "#primes_proof";

    /**
     * encrypted primes"
     */
    public static final String ENCRYPTED_PRIMES = "#encprimes";

    /**
     * encrypted ballot casting key"
     */
    public static final String ENCRYPTED_BALLOT_CASTING_KEY = "#hash_enccm";

    /**
     * exponentiated encrypted primes"
     */
    public static final String EXPONENTIATED_ENCRYPTED_PRIMES = "#comp_encprimes";

    public static final String HASH_ENPCC = "#hash_encpcc";

    public static final String HASH_COMPUTED_ENPCC = "#hash_computed_encpcc";

    public static final String PC = "#pc";

    public static final String COMPUTED_PC = "#pc_comp";

    public static final String PC_PROOF = "#pc_proof";

    public static final String PC_PROOF_SIGNATURE = "#pc_proof_sign";

    public static final String DEC_CC_PROOF = "#dec_proof_CC";

    public static final String HASH_ENCCM = "#hash_enccm";

    public static final String EXPONENTIATED_ENCRYPTED_BALLOT_CASTING_KEY = "#hash_computed_enccm";

    public static final String HASH_COMPUTED_ENCCM = "#hash_computed_enccm";

    public static final String HASH_VOTER_CHOICE_CODE_GEN_PUBLIC_KEY = "#vcc_gen_publickey";

    public static final String REQUEST_ID = "#request_id";

    public static final String PCCC = "#pccc";

    public static final String PD_CC = "#pd_cc";

    public static final String VOTE_SET_ID = "#vote_set_id";

    public static final String VOTES = "#votes";

    public static final String VOTE_ENCRYPTION_KEY = "#vote_encryption_key";

    public static final String TIMESTAMP = "#timestamp";

    public static final String COMMITMENT_PARAMETERS = "#commitment_parameters";

    public static final String DECRYPTION_PROOFS = "#decryption_proofs";

    public static final String SHUFFLED_VOTES = "#shuffled_votes";

    public static final String SHUFFLED_PROOF = "#shuffle_proof";

    public static final String PREVIOUS_VOTES = "#previous_votes";

    public static final String PREVIOUS_VOTE_ENCRYPTION_KEY = "#previous_vote_encryption_key";

    public static final String VOTE_SET_SIGNATURE = "#vote_set_signature";

    public static final String ELECTORAL_BOARD_ID = "#electoral_board_id";
    
    public static final String VOTE_CAST_ATTEMPTS = "#vote_cast_attempts"; 
}
