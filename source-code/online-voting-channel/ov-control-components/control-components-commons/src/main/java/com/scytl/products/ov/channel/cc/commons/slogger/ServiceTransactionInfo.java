/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.slogger;

public class ServiceTransactionInfo {

    private final String trackId;

    public ServiceTransactionInfo(String trackId) {
        super();
        this.trackId = trackId;
    }

    public String getTrackId() {
        return trackId;
    }

}
