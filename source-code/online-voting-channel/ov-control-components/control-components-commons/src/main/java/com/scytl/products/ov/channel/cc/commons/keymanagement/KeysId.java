/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Keys identifier used in the keys cache.
 */
@SuppressWarnings("serial")
final class KeysId implements Serializable {
    private final Class<?> keysClass;

    private final String[] tags;

    private KeysId(Class<?> keysClass, String... tags) {
        this.keysClass = keysClass;
        this.tags = tags;
    }

    /**
     * Returns a choice code instance for given election and verification card
     * set.
     * 
     * @param electionEventId
     *            the election event identifier
     * @param verificationCardSetId
     *            the verification card set identifier
     * @return an instance.
     */
    public static KeysId getChoiceCodeInstance(String electionEventId,
            String verificationCardSetId) {
        return new KeysId(ChoiceCodeKeys.class, electionEventId,
            verificationCardSetId);
    }

    /**
     * Returns a election signing instance for given election.
     * 
     * @param electionEventId
     *            the election event identifier
     * @return an instance.
     */
    public static KeysId getElectionSigningInstance(
            String electionEventId) {
        return new KeysId(ElectionSigningKeys.class, electionEventId);
    }

    /**
     * Returns a choice code instance for given election and electoral
     * authority.
     * 
     * @param electionEventId
     *            the election event identifier
     * @param electoralAuthorityId
     *            the electoral authority identifier
     * @return an instance.
     */
    public static KeysId getMixingInstance(String electionEventId,
            String electoralAuthorityId) {
        return new KeysId(MixingKeys.class, electionEventId,
            electoralAuthorityId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        KeysId other = (KeysId) obj;
        if (keysClass == null) {
            if (other.keysClass != null) {
                return false;
            }
        } else if (!keysClass.equals(other.keysClass)) {
            return false;
        }
        if (!Arrays.equals(tags, other.tags)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((keysClass == null) ? 0 : keysClass.hashCode());
        result = prime * result + Arrays.hashCode(tags);
        return result;
    }
}
