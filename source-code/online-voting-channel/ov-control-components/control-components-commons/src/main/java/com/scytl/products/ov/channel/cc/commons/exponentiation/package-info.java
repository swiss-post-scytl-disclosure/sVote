/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * This package provides exponentiation API and implementation to be used for
 * generation and verification of the choice codes and for generation of the
 * ballot casting keys.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.scytl.products.ov.channel.cc.commons.exponentiation;
