/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.products.oscore.logging.api.factory.SecureLoggingFactory;
import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.channel.cc.commons.keymanagement.KeyManager;
import com.scytl.products.ov.channel.cc.commons.slogger.SecureLoggerManager;
import com.scytl.products.ov.channel.cc.commons.slogger.SecureLoggerManagerImpl;

/**
 * Secure logger configuration.
 */
@Configuration
public class SecureLoggerConfig {

    /**
     * Defines the secure logger manager.
     * 
     * @param manager
     *            the key manager
     * @return the secure logger manager.
     */
    @Bean
    public SecureLoggerManager secureLoggerManager(KeyManager manager) {
        return new SecureLoggerManagerImpl(manager);
    }
    
    @Bean
    public SecureLoggingWriter secureLoggingWriter(
            final SecureLoggingFactory secureLoggingFactory) {
        return secureLoggingFactory.getLogger("SecureLogger");
    }
}
