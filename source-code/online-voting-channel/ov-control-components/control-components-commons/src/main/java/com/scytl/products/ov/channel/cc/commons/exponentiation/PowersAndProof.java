/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.exponentiation;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.concurrent.Immutable;

import com.scytl.cryptolib.proofs.proof.Proof;

/**
 * Powers and proof are the exponentiation result.
 * 
 * @param <T>
 *            the type of the bases
 */
@Immutable
public final class PowersAndProof<T> {
    private final List<T> powers;

    private final Proof proof;

    /**
     * Constructor. For internal use only.
     * 
     * @param powers
     *            the powers
     * @param proof
     *            the proofs
     */
    PowersAndProof(List<T> powers, Proof proof) {
        this.powers = unmodifiableList(powers);
        this.proof = proof;
    }

    /**
     * Creates a new instance for given powers and proof. Normally clients
     * should not create new instances of the class, this method is provided
     * mostly for testing purposes.
     * 
     * @param powers
     *            the powers
     * @param proof
     *            the proof
     * @return a new instance.
     */
    public static <T> PowersAndProof<T> newInstance(List<T> powers,
            Proof proof) {
        return new PowersAndProof<>(new ArrayList<>(powers), proof);
    }

    /**
     * Returns the powers. In the returned list is read-only.
     * 
     * @return the powers.
     */
    public List<T> powers() {
        return powers;
    }

    /**
     * Returns the proof.
     * 
     * @return the proof.
     */
    public Proof proof() {
        return proof;
    }
}
