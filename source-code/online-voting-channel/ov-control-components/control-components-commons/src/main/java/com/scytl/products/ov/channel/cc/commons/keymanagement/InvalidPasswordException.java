/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import java.security.KeyManagementException;

/**
 * Password is invalid.
 */
@SuppressWarnings("serial")
public final class InvalidPasswordException
        extends KeyManagementException {
    /**
     * Constructor.
     * 
     * @param message
     *            the message.
     */
    public InvalidPasswordException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause.
     */
    public InvalidPasswordException(String message, Throwable cause) {
        super(message, cause);
    }
}
