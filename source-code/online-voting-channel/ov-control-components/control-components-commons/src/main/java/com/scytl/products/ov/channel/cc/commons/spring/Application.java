/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Application.
 */
@ThreadSafe
public interface Application {
    /**
     * Runs the application with given command-line arguments. Implementation
     * can report multiple problems by adding additional
     * {@link ApplicationException} to the suppressed exceptions of the thrown
     * one.
     * 
     * @param args
     *            the arguments
     * @throws ApplicationException
     *             the application failed.
     */
    void run(String[] args) throws ApplicationException;

    /**
     * Shutdowns the application normally. The operation is asynchronous, it
     * will be completed when {@link #run(String[])} returns.
     */
    void shutdown();

    /**
     * Shutdowns the application abnormally. The operation is asynchronous, it
     * will be completed when {@link #run(String[])} returns.
     * 
     * @param e
     *            the exception.
     */
    void shutdown(Throwable e);
}
