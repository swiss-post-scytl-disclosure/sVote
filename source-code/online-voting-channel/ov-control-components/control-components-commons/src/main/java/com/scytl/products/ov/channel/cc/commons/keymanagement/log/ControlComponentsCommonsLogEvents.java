/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement.log;

import com.scytl.products.oscore.logging.api.domain.LogEvent;

/**
 * Enum with the log events for this context.
 */
public enum ControlComponentsCommonsLogEvents implements LogEvent {
   
    KEY_PAIR_GENERATED_STORED("GENCCSK", "000", "Key pair successfully generated and stored"),
    CONTROL_COMPONENT_SIGNING_CERTIFICATED_GENERATED("GENCCCRT", "000", "Control Component Signing Certificate successfully generated"),
    KEY_PAIR_GENERATED_CEK("GENCCKEYS", "000", "Choice Codes Decryption key pair and Choice Codes Generation key pair successfully generated"),
    KEY_PAIR_GENERATED_ELK("GENELK", "000", "Mixing key pair successfully generated"),
    KEY_PAIR_STORED_CEK("GENCCKEYSSTORE", "000", "Choice Codes Decryption key pair and Choice Codes Generation key pair successfully stored"),
    KEY_PAIR_STORED_ELK("GENELKSTORE", "000", "Mixing key pair succesfully stored"),
    PUBLIC_KEY_SIGNED_CEK("GENCCKEYSSIGN", "000", "Choice Codes Decryption public key and Choice Codes Generation public key successfully signed"),
    PUBLIC_KEY_SIGNED_ELK("GENELKSIGN", "000", "Mixing public key successfully signed"),
    CHOICE_CODE_PRIVATE_KEY_DERIVED_GEN("DERIVCCEKGEN","000","Voter choice code generation private key successfully derived" ),
    BALLOT_CASTING_KEY_EXPONENTIATION_KEY_DERIVED_GEN("DERIVBCKKGEN","000","Ballot casting key generation exponentiation key successfully derived"),
    CHOICE_CODE_PRIVATE_KEY_DERIVED_COMP("DERIVCCEKCOMP","000","Voter choice code generation private key successfully derived" ),
    VOTE_CAST_CODE_EXPONENTIATION_KEY_DERIVED_COMP("DERIVBCKKCOMP","000","Vote cast code exponentiation key successfully derived"),
    PARTIAL_CHOICE_CODE_COMPUTED_GEN("GENPCC","000","Partial choice code successfully computed" ),
    PARTIAL_VOTE_CAST_CODE_COMPUTED_GEN("GENPVCC","000","Partial vote cast code successfully computed" ),
    PARTIAL_CODE_GEN_SIGN("GENPCSIGN","000","Partial codes correctly signed" ),
    PARTIAL_CHOICE_CODE_PROOF_KNOWLEDGE_EXPONENT_COMPUTED ("GENPCCPROOF","000","Proof of knowledge of the exponent successfully computed."),
    PARTIAL_VOTE_CAST_CODE_PROOF_KNOWLEDGE_EXPONENT_COMPUTED ("GENPVCCPROOF","000","Proof of knowledge of the exponent successfully computed."),
    PARTIAL_CODE_COMPUTED("PCCOMP","000","Partial code successfully computed" ),
    PARTIAL_CODE_PROOF_KNOWLEDGE_EXPONENT_COMPUTED ("PCCOMPPROOF","000","Proof of knowledge of the exponent successfully computed."),
    PARTIAL_CODE_COMP_SIGN("PCCOMPSIGN","000","Partial code and proofs correctly signed" ),
    PARTIAL_DECRYPTION_COMPUTED("PDEC","000","Partial decryption successfully computed"), 
    PARTIAL_DECRYPTION_PROOF_KNOWLEDGE_EXPONENT_COMPUTED ("PDECPROOF","000","Proof of knowledge of the exponent successfully computed."),
    PARTIAL_DECRYPTION_SIGN("PDECSIGN","000","Partial decryption and proof correctly signed"), 
    SUCCESSFUL_VOTE_VALIDATION("VOTVAL","000","Successful vote validation" ),
    CM_LOGGING("CMVAL","000","Confirmation Message logging" ),    
    VERIFICATION_CARD_PROCESSED_CHECK("VCPROCCHK", "000", "Verification card is yet to be computed"),
    CONFIRMATION_ATTEMPTS_NOT_EXCEEDED("CMEXC", "000", "Confirmation attempts not exceeded"),
    VOTE_SET_SIGNATURE_VALIDATED("MPDONSIGNVAL","000","Vote set signature successfully validated"),
    VOTE_SET_MIXING_DECRYPTED("MPDON","000","Vote set mixed and decrypted"),
    VOTE_ENCRYPTION_KEY_REMOVAL ("MPDONENCKEY","000","Vote encryption key contribution removed"),
    VOTE_SET_SUCCESSFULLY_SIGNED("MPDONSIGN","000","Vote set successfully signed"),
    //ERROR CASES
    SCHNORR_PROOF_NOT_VALID("VOTVAL","X30","Schnorr proof not valid" ),
    EXPONENTIATION_PROOF_NOT_VALID("VOTVAL","X31","Exponentiation proof not valid" ),
    PLAINTEXT_EQUALITY_PROOF_NOT_VALID("VOTVAL","X32","Plaintext equality proof not valid" ),
    ERROR_CONTROL_COMPONENT_SIGNING_CERTIFICATED("GENCCCRT", "X34", "Error generating the Control Component Signing Certificate"),
    ERROR_GENERATING_KEY_PAIR_CEK("GENCCKEYS", "X35", "Error generating the key pair"),
    ERROR_STORING_KEY_PAIR_CEK("GENCCKEYSSTORE", "X36", "Error storing key pair"),
    ERROR_SIGNING_PUBLIC_KEY_CEK("GENCCKEYSSIGN", "X37", "Error signing public key"),
    ERROR_GENERATING_KEY_PAIR_ELK("GENELK", "X38", "Error generating the key pair"),
    ERROR_STORING_KEY_PAIR_ELK("GENELKSTORE", "X39", "Error storing key pair"),
    ERROR_SIGNING_PUBLIC_KEY_ELK("GENELKSIGN", "X40", "Error signing public key"),
    ERROR_COMPUTING_PARTIAL_CHOICE_CODE_GEN("GENPCC","X41","Partial choice code cannot be computed" ),
    ERROR_PARTIAL_CODE_NOT_SIGNED("GENPCSIGN","X42","Partial codes cannot be signed" ),
    ERROR_PARTIAL_VOTE_CAST_CODE_NOT_COMPUTED_GEN("GENPVCC","X43","Partial Vote Cast Code cannot be computed" ),
    CHOICE_CODE_PRIVATE_KEY_DERIVED_GEN_FAILED("DERIVCCEKGEN","X65","The derivation of the voter choice code generation private key has failed"),
    BALLOT_CASTING_KEY_EXPONENTIATION_KEY_DERIVED_GEN_FAILED("DERIVBCKKGEN","X81","The derivation of the ballot casting key exponentiation key has failed"),
    CHOICE_CODE_PRIVATE_KEY_DERIVED_COMP_FAILED("DERIVCCEKCOMP","X66","The derivation of the choice code private key has failed"),
    VOTE_CAST_CODE_EXPONENTIATION_KEY_DERIVED_COMP_FAILED("DERIVBCKKCOMP","X82","The derivation of the vote cast code exponentiation key has failed"),
    ERROR_PARTIAL_CODE_NOT_COMPUTED("PCCOMP","X73","Partial code cannot be computed" ),
    ERROR_EXPONENT_PROOF_NOT_COMPUTED("PCCOMPPROOF","X74","Proof of knowledge of the exponent cannot be computed" ), 
    ERROR_PARTIAL_CODE_COMP_NOT_SIGNED("PCCOMPSIGN","X75","Partial code and proof cannot be signed" ),
    ERROR_PARTIAL_DECRYPTION_NOT_COMPUTED("PDEC","X78","Partial decryption cannot be computed"),
    ERROR_KNOWLEDGE_PROOF_NOT_COMPUTED("PDECPROOF","X79","Proof of knowledge of the private key cannot be computed"),
    ERROR_PARTIAL_DECRYPTION_PROOF_NOT_SIGNED("PDECSIGN","X80","Partial decryption and proof cannot be signed"),
    ERROR_IN_VOTE_SET_SIGNATURE("MPDONSIGNVAL","X69","Vote set signature validation failed"), 
    ERROR_IN_VOTE_SET_MIXING_DECRYPTED("MPDON","X70","Error mixing and decrypting vote set"),
    ERROR_IN_VOTE_ENCRYPTION_KEY_REMOVAL ("MPDONENCKEY","X71","Could not remove the vote encryption key contribution"),
    ERROR_IN_VOTE_SET_SIGNING("MPDONSIGN","X72","Vote set signature failed"),
    ERROR_VERIFICATION_CARD_PROCESSED_CHECK("VCPROCCHK", "X83", "Verification card has already been computed"),
    ERROR_CONFIRMATION_ATTEMPTS_EXCEEDED("CMEXC", "X84", "Confirmation attempts exceeded");

	private final String layer;

	private final String action;

	private final String outcome;

	private final String info;

	ControlComponentsCommonsLogEvents(final String action, final String outcome, final String info) {
		this.layer = "";
		this.action = action;
		this.outcome = outcome;
		this.info = info;
	}

	/**
	 * @see ControlComponentsCommonsLogEvents#getAction()
	 */
	@Override
	public String getAction() {
		return action;
	}

	/**
	 * @see ControlComponentsCommonsLogEvents#getOutcome()
	 */
	@Override
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @see ControlComponentsCommonsLogEvents#getInfo()
	 */
	@Override
	public String getInfo() {
		return info;
	}

	/**
	 * @see ControlComponentsCommonsLogEvents#getLayer()
	 */
	@Override
	public String getLayer() {
		return layer;
	}
}
