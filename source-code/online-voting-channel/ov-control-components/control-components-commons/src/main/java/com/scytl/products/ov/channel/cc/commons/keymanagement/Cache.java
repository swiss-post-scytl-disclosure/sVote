/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Cache for keeping keys in memory.
 */
@ThreadSafe
interface Cache {
    /**
     * Returns the choice code keys if available for given election and
     * verification card set.
     * 
     * @param electionEventId
     *            the election event identifier
     * @param verificationCardSetId
     *            the verification card set identifier
     * @return the keys if available or {@code null}.
     */
    @Nullable
    ChoiceCodeKeys getChoiceCodeKeys(String electionEventId,
            String verificationCardSetId);

    /**
     * Returns the election signing keys if available for given election.
     * 
     * @param electionEventId
     *            the election event identifier
     * @return the keys if available or {@code null}.
     */
    @Nullable
    ElectionSigningKeys getElectionSigningKeys(String electionEventId);

    /**
     * Returns the mixing keys if available for given election and electoral
     * authority.
     * 
     * @param electionEventId
     *            the election event identifier
     * @param electoralAuthorityId
     *            the electoral authority identifier.
     * @return the keys if available or {@code null}.
     */
    @Nullable
    MixingKeys getMixingKeys(String electionEventId,
            String electoralAuthorityId);

    /**
     * Puts the choice code keys for given election and verification card set.
     * 
     * @param electionEventId
     *            the election event identifier
     * @param verificationCardSetId
     *            the verification card set identifier
     * @param keys
     *            the keys.
     */
    void putChoiceCodeKeys(String electionEventId,
            String verificationCardSetId, ChoiceCodeKeys keys);

    /**
     * Puts the election signing keys for a given election.
     * 
     * @param electionEventId
     *            the election event identifier
     * @param keys
     *            the keys.
     */
    void putElectionSigningKeys(String electionEventId,
            ElectionSigningKeys keys);

    /**
     * Puts the mixing keys for given election and electoral authority.
     * 
     * @param electionEventId
     *            the election event identifier
     * @param electoralAuthorityId
     *            the electoral authority identifier
     * @param keys
     *            the keys.
     */
    void putMixingKeys(String electionEventId, String electoralAuthorityId,
            MixingKeys keys);

    /**
     * Shuts the cache down.
     */
    void shutdown();

    /**
     * Starts the cache up.
     */
    void startup();
}
