/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.scytl.cryptolib.api.proofs.ProofsServiceAPI;
import com.scytl.products.ov.channel.cc.commons.exponentiation.ExponentiationService;
import com.scytl.products.ov.channel.cc.commons.exponentiation.ExponentiationServiceImpl;

/**
 * Exponentiation service configuration
 */
@Configuration
public class ExponentiationConfig {
    @Bean
    ExponentiationService exponentiationService(
            ProofsServiceAPI proofsService) {
        return new ExponentiationServiceImpl(proofsService);
    }
}
