/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
/**
 * This package provides common secure logger mechanisms to be shared by all
 * control components.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.scytl.products.ov.channel.cc.commons.slogger;
