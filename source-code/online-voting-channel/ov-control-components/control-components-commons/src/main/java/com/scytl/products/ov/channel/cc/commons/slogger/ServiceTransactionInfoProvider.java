/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.slogger;
/**
 * @date 21/06/18 08:01
 * Copyright (C) 2015 Scytl Secure Electronic Voting SA
 * All rights reserved.
 */

import com.scytl.products.oscore.logging.api.domain.TransactionInfo;
import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;

/**
 * Provider for {@link TransactionInfo}. It encapsulates the creation and
 * obtention of the transaction info for non web applications.
 */
public class ServiceTransactionInfoProvider extends TransactionInfoProvider {

    private final ThreadLocal<ServiceTransactionInfo> serviceTransactionInfoThreadLocal;

    /**
     * Constructs the Track Id wrapper.
     */
    public ServiceTransactionInfoProvider() {
        serviceTransactionInfoThreadLocal = new ThreadLocal<>();
    }

    public void generate(final String tenant, final String clientIpAddress, final String serverIpAddress,
            final String serviceTrackId) {
        super.generate(tenant, clientIpAddress, serverIpAddress);

        final ServiceTransactionInfo serviceTransactionInfo = new ServiceTransactionInfo(serviceTrackId);

        setServiceTransactionInfo(serviceTransactionInfo);

    }

    public void setServiceTransactionInfo(final ServiceTransactionInfo serviceTransactionInfo) {

        serviceTransactionInfoThreadLocal.set(serviceTransactionInfo);
    }

    public ServiceTransactionInfo getServiceTransactionInfo() {
        return serviceTransactionInfoThreadLocal.get();
    }

}
