/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import com.scytl.products.ov.commons.messaging.MessagingService;
import com.scytl.products.ov.commons.messaging.MessagingServiceImpl;

/**
 * Generic messaging configuration.
 */
public class MessagingConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger("std");

    @Value("${mb[0].hostname}")
    private String serverHost;

    @Value("${mb[0].port}")
    private Integer port;

    @Value("${mb[0].vhost}")
    private String virtualHost;

    @Value("${mb[0].user}")
    private String username;

    @Value("${mb[0].password}")
    private String password;

    /**
     * Defines the messaging service.
     * 
     * @return the messaging service.
     */
    @Bean(destroyMethod = "shutdown")
    public MessagingService messagingService() {
        LOGGER.info("Configuring connection to {}:{}/{} as {}", serverHost,
            port, virtualHost, username);
        MessagingServiceImpl.Builder builder =
            new MessagingServiceImpl.Builder();
        builder.setHostName(serverHost);
        builder.setPort(port);
        if (virtualHost != null && !virtualHost.isEmpty()) {
            builder.setVirtualHost(virtualHost);
        }
        builder.setUsername(username);
        builder.setPassword(password);
        builder.setSenderPoolSize(1);
        builder.useSSL();
        return builder.build();
    }
}
