/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import com.scytl.cryptolib.api.elgamal.ElGamalServiceAPI;
import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.cryptolib.elgamal.bean.ElGamalKeyPair;
import com.scytl.cryptolib.elgamal.cryptoapi.CryptoAPIElGamalKeyPairGenerator;
import com.scytl.cryptolib.elgamal.service.ElGamalService;

/**
 * Tests of {@link ChoiceCodeKeys}.
 */
public class ChoiceCodeKeysTest {

    @BeforeClass
    public static void beforeClass() {
        if (Security
            .getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @Test
    public void testBuilder() throws GeneralCryptoLibException {
        ElGamalServiceAPI service = new ElGamalService();
        CryptoAPIElGamalKeyPairGenerator generator =
            service.getElGamalKeyPairGenerator();
        ElGamalEncryptionParameters parameters =
            new ElGamalEncryptionParameters(BigInteger.valueOf(7),
                BigInteger.valueOf(3), BigInteger.valueOf(2));

        ElGamalKeyPair generationKeys =
            generator.generateKeys(parameters, 1);
        byte[] generationPublicKeySignature = {1 };
        ElGamalKeyPair decryptionKeys =
            generator.generateKeys(parameters, 1);
        byte[] decryptionPublicKeySignature = {2 };

        ChoiceCodeKeys.Builder builder = new ChoiceCodeKeys.Builder();
        builder.setGenerationKeys(generationKeys.getPrivateKeys(),
            generationKeys.getPublicKeys(), generationPublicKeySignature);
        builder.setDecryptionKeys(decryptionKeys.getPrivateKeys(),
            decryptionKeys.getPublicKeys(), decryptionPublicKeySignature);

        ChoiceCodeKeys keys = builder.build();

        assertEquals(keys.generationPrivateKey(),
            generationKeys.getPrivateKeys());
        assertEquals(keys.generationPublicKey(),
            generationKeys.getPublicKeys());
        assertArrayEquals(keys.generationPublicKeySignature(),
            generationPublicKeySignature);
        assertEquals(keys.decryptionPrivateKey(),
            decryptionKeys.getPrivateKeys());
        assertEquals(keys.decryptionPublicKey(),
            decryptionKeys.getPublicKeys());
        assertArrayEquals(keys.decryptionPublicKeySignature(),
            decryptionPublicKeySignature);
    }
}
