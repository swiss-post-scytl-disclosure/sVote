/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Test;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.cryptolib.elgamal.cryptoapi.EncryptionParameters;

/**
 * Tests of {@link ChoiceCodeKeysSpec}.
 */
public class ChoiceCodeKeysSpecTest {
    @Test
    public void testBuilder() throws GeneralCryptoLibException {
        ChoiceCodeKeysSpec.Builder builder =
            new ChoiceCodeKeysSpec.Builder();
        builder.setElectionEventId("electionEventId");
        builder.setVerificationCardSetId("verificationCardSetId");
        builder.setGenerationLength(1);
        builder.setDecryptionLength(2);
        EncryptionParameters parameters =
            new ElGamalEncryptionParameters(BigInteger.valueOf(7),
                BigInteger.valueOf(3), BigInteger.valueOf(2));
        builder.setParameters(parameters);

        ChoiceCodeKeysSpec spec = builder.build();

        assertEquals("electionEventId", spec.electionEventId());
        assertEquals("verificationCardSetId",
            spec.verificationCardSetId());
        assertEquals(1, spec.generationLength());
        assertEquals(2, spec.decryptionLength());
        assertEquals(parameters, spec.parameters());
    }
}
