/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import static org.junit.Assert.*;

import java.time.Duration;

import org.junit.Test;

/**
 * Tests of {@link KeysContainer}.
 */
public class KeysContainerTest {
    private static final Object KEYS = new Object();

    @Test
    public void testIsExpired() {
        KeysContainer container =
            new KeysContainer(KEYS, Duration.ofMillis(500));
        assertFalse(container.isExpired());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            fail("Thread is interrupted.");
        }
        assertTrue(container.isExpired());
        
        container.getKeys();
        assertFalse(container.isExpired());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            fail("Thread is interrupted.");
        }
        assertTrue(container.isExpired());
    }

    @Test
    public void testGetKeys() {
        KeysContainer container =
            new KeysContainer(KEYS, Duration.ofSeconds(1));
        assertEquals(KEYS, container.getKeys());
    }
}
