/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.services.compute;

import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.mathematical.groups.impl.Exponent;
import com.scytl.cryptolib.primitives.service.PrimitivesService;
import com.scytl.products.ov.commons.test.CryptoUtils;
import com.scytl.products.ov.commons.test.ElGamalTestKeyPair;
import java.math.BigInteger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class KeyDerivationServiceTest {
    private static final String VERIFICATION_CARD_ID = "394cbc1c2288400daa4597646ce72db2";

    private static final BigInteger Q = new BigInteger("8185259497159793380159895763146767663788219323391069709923002090418551763564517977371021795304710684832972373293942907460425847273228445883822472729562211276881708293257669989007077226079843554580545317683800174632467462070873041030176741653427676096179366225977616000296888777215899490787264927157325546043244213195388405683562504775673044659657555754638673558733553957036819728402579547281296977097980265568026104009671696453408000508744025683259061202409983602300713652133690119131956946329475140796877947373669563265509013399491392665539532563187727646704532770365823469904320136696927628115410254608705755029379");

    @Mock
    private PrimitivesService primitivesService;

    @InjectMocks
    private KeyDerivationService keyDerivationService = new KeyDerivationService();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDeriveKeyHappyPath() throws Exception {
        ElGamalPrivateKey privateKey = ElGamalTestKeyPair.create().getPrivateKeys();        
        Exponent exponent = privateKey.getKeys().get(0);
        when(primitivesService.getKDFDeriver()).thenReturn(CryptoUtils.getKDFDeriver());
        Exponent cryptoAPIDerivedKey = keyDerivationService.deriveKey(exponent,VERIFICATION_CARD_ID,Q);
        assertTrue(cryptoAPIDerivedKey.getValue().compareTo(Q) < 0);
    }
}