/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.keymanagement;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests of {@link KeysId}
 */
public class KeysIdTest {
    @Test
    public void testGetChoiceCodeInstance() {
        KeysId id1 = KeysId.getChoiceCodeInstance("ee1", "v1");
        KeysId id2 = KeysId.getChoiceCodeInstance("ee1", "v1");
        KeysId id3 = KeysId.getChoiceCodeInstance("ee1", "v2");
        KeysId id4 = KeysId.getChoiceCodeInstance("ee2", "v1");
        KeysId id5 = KeysId.getChoiceCodeInstance("ee2", "v2");
        
        assertTrue(id1.equals(id1));
        assertTrue(id1.equals(id2));
        assertFalse(id1.equals(id3));
        assertFalse(id1.equals(id4));
        assertFalse(id1.equals(id5));
    }

    @Test
    public void testGetElectionSigningInstance() {
        KeysId id1 = KeysId.getElectionSigningInstance("ee1");
        KeysId id2 = KeysId.getElectionSigningInstance("ee1");
        KeysId id3 = KeysId.getElectionSigningInstance("ee2");
        
        assertTrue(id1.equals(id1));
        assertTrue(id1.equals(id2));
        assertFalse(id1.equals(id3));
    }

    @Test
    public void testGetMixingInstance() {
        KeysId id1 = KeysId.getMixingInstance("ee1", "ea1");
        KeysId id2 = KeysId.getMixingInstance("ee1", "ea1");
        KeysId id3 = KeysId.getMixingInstance("ee1", "ea2");
        KeysId id4 = KeysId.getMixingInstance("ee2", "ea1");
        KeysId id5 = KeysId.getMixingInstance("ee2", "ea2");
        
        assertTrue(id1.equals(id1));
        assertTrue(id1.equals(id2));
        assertFalse(id1.equals(id3));
        assertFalse(id1.equals(id4));
        assertFalse(id1.equals(id5));
    }
}
