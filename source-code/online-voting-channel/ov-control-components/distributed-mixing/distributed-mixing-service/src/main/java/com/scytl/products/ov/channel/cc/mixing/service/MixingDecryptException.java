/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.mixing.service;

public class MixingDecryptException extends Exception {
    private static final long serialVersionUID = 8413768882319130870L;

    public MixingDecryptException(String message) {
        super(message);
    }

	public MixingDecryptException(String message, Throwable cause) {
		super(message, cause);
	}

}
