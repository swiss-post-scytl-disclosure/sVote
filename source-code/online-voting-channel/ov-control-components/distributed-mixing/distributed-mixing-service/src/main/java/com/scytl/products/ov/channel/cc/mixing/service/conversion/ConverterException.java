/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.mixing.service.conversion;

/**
 * A mixing output failed to be converted.
 */
public class ConverterException extends Exception {
    public ConverterException(Throwable cause) {
        super("Could not convert the output from mixing", cause);
    }
}
