/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.mixing.service.conversion;

import java.util.function.Supplier;

import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.products.ov.commons.dto.mixing.MixingPayload;

/**
 * Provides the blueprint for a converter that takes the output of the mixing library and converts
 * it to an sVote mixing DTO.
 */
public interface MixingOutputConverter {
    /**
     * Converts the output of the mixing into an sVote mixing payload.
     *
     * @param inputPayload the input supplied to the mixing
     * @param mixingOutput the output of the mixing
     * @param remainingVoteEncryptionKey the key that resulted from extracting the public
     *         part of the last node's vote decryption key
     * @return an sVote mixing payload
     * @throws ConverterException if the conversion did not happen
     */
    MixingPayload convert(MixingPayload inputPayload, Object mixingOutput,
            Supplier<ElGamalPublicKey> remainingVoteEncryptionKey) throws ConverterException;
}
