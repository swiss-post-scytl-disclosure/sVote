/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.mixing.service;

import com.scytl.products.ov.commons.dto.MixingDTO;

public interface InternalMixDecryptService {

    void executeMixing(MixingDTO dto);

}
