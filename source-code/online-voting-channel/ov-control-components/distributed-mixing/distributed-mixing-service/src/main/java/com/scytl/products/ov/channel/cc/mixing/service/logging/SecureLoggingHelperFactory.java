/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.mixing.service.logging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.scytl.products.oscore.logging.api.writer.SecureLoggingWriter;
import com.scytl.products.ov.channel.cc.commons.util.FingerprintGenerator;
import com.scytl.products.ov.channel.cc.mixing.service.SecureLoggingHelper;
import com.scytl.products.ov.commons.beans.domain.model.vote.VoteSetId;

/**
 * Factory that creates secure logging helpers.
 */
public class SecureLoggingHelperFactory {

    private final SecureLoggingWriter secureLoggingWriter;

    private final FingerprintGenerator fingerprintGenerator;

    private final String nodeName;

    public SecureLoggingHelperFactory(@Autowired SecureLoggingWriter secureLoggingWriter,
            @Autowired FingerprintGenerator fingerprintGenerator, @Value("${keys.nodeId}") String nodeName) {
        this.secureLoggingWriter = secureLoggingWriter;
        this.fingerprintGenerator = fingerprintGenerator;
        this.nodeName = nodeName;
    }

    public SecureLoggingHelper create(String requestId, VoteSetId voteSetId) {
        return new SecureLoggingHelper(secureLoggingWriter, fingerprintGenerator, nodeName, requestId, voteSetId);
    }
}
