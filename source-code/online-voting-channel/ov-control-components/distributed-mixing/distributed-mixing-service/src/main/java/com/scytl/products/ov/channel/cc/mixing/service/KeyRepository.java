/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.mixing.service;

import java.security.KeyManagementException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalPrivateKey;
import com.scytl.cryptolib.elgamal.bean.ElGamalPublicKey;
import com.scytl.products.ov.channel.cc.commons.keymanagement.FingerprintGeneratorException;
import com.scytl.products.ov.channel.cc.commons.keymanagement.KeyNotFoundException;
import com.scytl.products.ov.commons.dto.KeyCreationDTO;

public interface KeyRepository {

    ElGamalPrivateKey getBallotDecryptionKey(String electionId, String electoralBoardId)
            throws KeyNotFoundException, KeyManagementException, GeneralCryptoLibException;

    ElGamalPublicKey getBallotEncryptionKey(String electionId, String electoralBoardId)
            throws KeyNotFoundException, KeyManagementException, GeneralCryptoLibException;

	void addGeneratedKey(KeyCreationDTO toBeReturned)
			throws GeneralCryptoLibException, KeyManagementException, FingerprintGeneratorException;

    /**
     * Gets the key used to sign objects (such as payloads) as coming from a
     * particular node. There is a signing key per election.
     *
     * @param electionEventId
     *            the election event identifier
     * @return the signing key
     * @throws KeyNotFoundException
     * @throws KeyManagementException
     */
    PrivateKey getSigningKey(String electionEventId) throws KeyNotFoundException, KeyManagementException;

    /**
     * Gets the verification certificate chain, consisting of the certificate of
     * the selected election's verification key, and all intermediate
     * certificates up to (but not including) the platform CA certificate.
     *
     * @param electionEventId
     *            the election event identifier
     * @return the certificate chain of the signed payload verification key
     * @throws KeyNotFoundException
     * @throws KeyManagementException
     */
    X509Certificate[] getVerificationCertificateChain(String electionEventId)
            throws KeyNotFoundException, KeyManagementException;

    /**
     * Returns the platform CA certificate.
     *
     * @return the platform CA certificate.
     * @throws KeyNotFoundException
     * @throws KeyManagementException
     */
    X509Certificate getPlatformCACertificate() throws KeyNotFoundException, KeyManagementException;
}
