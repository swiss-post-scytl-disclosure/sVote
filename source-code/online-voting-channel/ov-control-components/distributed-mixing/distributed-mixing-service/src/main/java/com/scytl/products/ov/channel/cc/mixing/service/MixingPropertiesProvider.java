/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.mixing.service;

import java.security.KeyManagementException;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.products.ov.channel.cc.commons.keymanagement.KeyNotFoundException;
import com.scytl.products.ov.commons.dto.MixingDTO;
import com.scytl.products.ov.mixnet.commons.configuration.ApplicationConfig;

public interface MixingPropertiesProvider {

	ApplicationConfig createApplicationConfigforBallotBox(MixingDTO mixingDTO) throws KeyNotFoundException, KeyManagementException, GeneralCryptoLibException;
	
}
