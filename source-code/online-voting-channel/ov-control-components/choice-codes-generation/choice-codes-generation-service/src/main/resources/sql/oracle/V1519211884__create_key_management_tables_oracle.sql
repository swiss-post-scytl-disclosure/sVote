--------------------------------------------------------
--  DDL for Table NODE_KEYS
--------------------------------------------------------

  CREATE TABLE "CC_NODE_KEYS"
   (
    "NODE_ID" VARCHAR2(10) NOT NULL,
	"KEYS" BLOB NOT NULL,
	CONSTRAINT CC_NODE_KEYS_PK PRIMARY KEY ("NODE_ID")
   ) ;


--------------------------------------------------------
--  DDL for Table ELECTION_SIGNING_KEYS
--------------------------------------------------------

  CREATE TABLE "CC_ELECTION_SIGNING_KEYS"
   (
    "NODE_ID" VARCHAR2(10) NOT NULL,
    "ELECTION_EVENT_ID" VARCHAR2(100) NOT NULL,
    "KEYS" BLOB NOT NULL,
    "PASSWORD" BLOB NOT NULL,
    CONSTRAINT CC_ELECTION_SIGNING_KEYS_PK PRIMARY KEY ("NODE_ID", "ELECTION_EVENT_ID")
   ) ;


--------------------------------------------------------
--  DDL for Table MIXING_KEYS
--------------------------------------------------------

  CREATE TABLE "CC_MIXING_KEYS"
   (
    "NODE_ID" VARCHAR2(10) NOT NULL,
    "ELECTION_EVENT_ID" VARCHAR2(100) NOT NULL,
    "ELECTORAL_AUTHORITY_ID" VARCHAR2(100) NOT NULL,
    "PRIVATE_KEY" BLOB NOT NULL,
    "PUBLIC_KEY" BLOB NOT NULL,
    "SIGNATURE" BLOB NOT NULL,
    CONSTRAINT CC_MIXING_KEYS_PK PRIMARY KEY ("NODE_ID", "ELECTION_EVENT_ID", "ELECTORAL_AUTHORITY_ID")
   ) ;


--------------------------------------------------------
--  DDL for Table CHOICE_CODE_KEYS
--------------------------------------------------------

  CREATE TABLE "CC_CHOICE_CODE_KEYS"
   (
    "NODE_ID" VARCHAR2(10) NOT NULL,
    "ELECTION_EVENT_ID" VARCHAR2(100) NOT NULL,
    "VERIFICATION_CARD_SET_ID" VARCHAR2(100) NOT NULL,
    "GENERATION_PRIVATE_KEY" BLOB NOT NULL,
    "GENERATION_PUBLIC_KEY" BLOB NOT NULL,
    "GENERATION_SIGNATURE" BLOB NOT NULL,
    "DECRYPTION_PRIVATE_KEY" BLOB NOT NULL,
    "DECRYPTION_PUBLIC_KEY" BLOB NOT NULL,
    "DECRYPTION_SIGNATURE" BLOB NOT NULL,
    CONSTRAINT CC_CHOICE_CODE_KEYS_PK PRIMARY KEY ("NODE_ID", "ELECTION_EVENT_ID", "VERIFICATION_CARD_SET_ID")
   ) ;


