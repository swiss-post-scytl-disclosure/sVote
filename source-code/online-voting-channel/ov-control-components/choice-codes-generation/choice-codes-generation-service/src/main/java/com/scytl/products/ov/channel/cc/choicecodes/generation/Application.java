/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.choicecodes.generation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.scytl.products.ov.channel.cc.choicecodes.generation.service.ChoiceCodesGenerationException;
import com.scytl.products.ov.channel.cc.choicecodes.generation.service.ChoiceCodesGenerationService;
import com.scytl.products.ov.channel.cc.choicecodes.generation.service.ChoiceCodesKeyGenerationService;
import com.scytl.products.ov.channel.cc.commons.spring.AbstractControlComponentApplication;
import com.scytl.products.ov.channel.cc.commons.spring.ApplicationException;
import com.scytl.products.ov.channel.cc.commons.spring.Applications;
import com.scytl.products.ov.channel.cc.commons.spring.ExponentiationConfig;
import com.scytl.products.ov.channel.cc.commons.spring.CryptolibConfig;
import com.scytl.products.ov.channel.cc.commons.spring.JdbcConfig;
import com.scytl.products.ov.channel.cc.commons.spring.KeyManagementConfig;
import com.scytl.products.ov.channel.cc.commons.spring.MessagingConfig;
import com.scytl.products.ov.channel.cc.commons.spring.SecureLoggerConfig;
import com.scytl.products.ov.channel.cc.commons.spring.TransactionConfig;

/**
 * The choice code verification application.
 */
@Configuration
@Import({CryptolibConfig.class, JdbcConfig.class, TransactionConfig.class,
        KeyManagementConfig.class, SecureLoggerConfig.class,
        MessagingConfig.class, ExponentiationConfig.class })
@ComponentScan
public class Application extends AbstractControlComponentApplication {
    @Autowired
    private ChoiceCodesGenerationService choiceCodesGenerationService;

    @Autowired
    private ChoiceCodesKeyGenerationService keyGenerationService;

    /**
     * The main method.
     * 
     * @param args
     *            the command-line arguments.
     */
    public static void main(String[] args) {
        Applications.main(Application.class, args, true);
    }

    @Override
    protected void activateMessageEndpoints() throws ApplicationException {
        try {
            choiceCodesGenerationService.startup();
            keyGenerationService.startup();
        } catch (ChoiceCodesGenerationException e) {
            throw new ApplicationException(
                "Failed to activate message endpoints.", e);
        }
    }

    @Override
    protected void deactivateMessageEndpoints()
            throws ApplicationException {
        try {
            choiceCodesGenerationService.shutdown();
            keyGenerationService.shutdown();
        } catch (ChoiceCodesGenerationException e) {
            throw new ApplicationException(
                "Failed to deactivate message endpoints.", e);
        }
    }
}
