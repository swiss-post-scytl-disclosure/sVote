/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.choicecodes.generation.service;

public interface ChoiceCodesKeyGenerationService {

    void startup() throws ChoiceCodesGenerationException;
    
    void shutdown() throws ChoiceCodesGenerationException;
}


