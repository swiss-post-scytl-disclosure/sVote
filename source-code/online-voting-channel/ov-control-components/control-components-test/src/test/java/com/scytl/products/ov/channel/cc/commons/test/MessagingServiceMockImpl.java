/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.commons.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

import com.scytl.products.ov.commons.messaging.Destination;
import com.scytl.products.ov.commons.messaging.DestinationNotFoundException;
import com.scytl.products.ov.commons.messaging.InvalidMessageException;
import com.scytl.products.ov.commons.messaging.MessageListener;
import com.scytl.products.ov.commons.messaging.MessagingException;
import com.scytl.products.ov.commons.messaging.MessagingService;

public class MessagingServiceMockImpl implements MessagingService {
    
    private Map<String, Object> queueMock = new HashMap<>();
    
    @Override
    public void send(Destination destination, Object message)
            throws DestinationNotFoundException, InvalidMessageException, MessagingException {
        queueMock.put(destination.name(), message);
    }
    
    public Map<String, Object> getQueueMock() {
        return queueMock;
    }
    
    
    // Methods below are not used for testing

    @Override
    public void createReceiver(Destination destination,
            MessageListener listener)
            throws DestinationNotFoundException, MessagingException {
        
    }

    @Override
    public void createReceiver(Destination destination,
            MessageListener listener, Executor executor)
            throws DestinationNotFoundException, MessagingException {
        
    }

    @Override
    public void destroyReceiver(Destination destination, MessageListener listener) throws MessagingException {
    }

    @Override
    public void shutdown() {
        
    }

}


