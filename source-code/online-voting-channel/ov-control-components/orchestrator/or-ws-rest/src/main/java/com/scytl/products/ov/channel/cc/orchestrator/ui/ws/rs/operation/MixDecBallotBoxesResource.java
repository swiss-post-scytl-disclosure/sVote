/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.ui.ws.rs.operation;

import com.scytl.products.oscore.logging.core.provider.TransactionInfoProvider;
import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.BallotBoxStatus;
import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.services.MixDecBallotBoxService;
import com.scytl.products.ov.commons.beans.domain.model.EntityId;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationExceptionMessages;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.commons.ui.ws.rs.ErrorCodes;
import com.scytl.products.ov.commons.util.HttpRequestService;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(MixDecBallotBoxesResource.RESOURCE_PATH)
@Stateless(name = "or-MixDecBallotBoxesResource")
public class MixDecBallotBoxesResource {

    /* Base path to resource */
    static final String RESOURCE_PATH = "mixdec/secured/tenant/{tenantId}/electionevent/{electionEventId}/ballotboxes";

    private static final String PATH_PARAMETER_TENANT_ID = "tenantId";

    private static final String PATH_PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    @Inject
    private MixDecBallotBoxService mixDecBallotBoxService;

    @Inject
    private TrackIdInstance trackIdInstance;

    @Inject
    private TransactionInfoProvider transactionInfoProvider;

    @Inject
    private HttpRequestService httpRequestService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response processBallotBoxes(@HeaderParam(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackingId,
            @PathParam(PATH_PARAMETER_TENANT_ID) String tenantId,
            @PathParam(PATH_PARAMETER_ELECTION_EVENT_ID) String electionEventId, @NotNull List<EntityId> ballotBoxIds,
            @Context HttpServletRequest request) throws ApplicationException {

        validateParameters(tenantId, electionEventId);

        trackIdInstance.setTrackId(trackingId);

        // transaction id generation
        transactionInfoProvider.generate(tenantId, httpRequestService.getIpClientAddress(request),
                httpRequestService.getIpServer(request));

        List<BallotBoxStatus> ballotBoxStatusList =
                mixDecBallotBoxService.processBallotBoxes(tenantId, electionEventId, ballotBoxIds, trackingId);

        return Response.ok().entity(ballotBoxStatusList).build();
    }

    /**
     * Validates parameters common to all requests.
     */
    private static void validateParameters(String tenantId, String electionEventId) throws ApplicationException {
        if ((null == tenantId) || tenantId.isEmpty()) {
            throw new ApplicationException(ApplicationExceptionMessages.EXCEPTION_MESSAGE_QUERY_PARAMETER_IS_NULL,
                    RESOURCE_PATH, ErrorCodes.MISSING_QUERY_PARAMETER, PATH_PARAMETER_TENANT_ID);
        }

        if ((null == electionEventId) || electionEventId.isEmpty()) {
            throw new ApplicationException(ApplicationExceptionMessages.EXCEPTION_MESSAGE_QUERY_PARAMETER_IS_NULL,
                    RESOURCE_PATH, ErrorCodes.MISSING_QUERY_PARAMETER, PATH_PARAMETER_ELECTION_EVENT_ID);
        }
    }

}
