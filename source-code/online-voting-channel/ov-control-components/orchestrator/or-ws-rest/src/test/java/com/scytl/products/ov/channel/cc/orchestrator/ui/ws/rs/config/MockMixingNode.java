/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.ui.ws.rs.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.commons.messaging.MessageListener;
import com.scytl.products.ov.commons.messaging.MessagingException;
import com.scytl.products.ov.commons.messaging.MessagingService;
import com.scytl.products.ov.commons.messaging.Queue;

/**
 * Simulates a mix/decrypt control component node.
 */
public class MockMixingNode implements AutoCloseable {
    private static final Logger logger = LoggerFactory.getLogger(MockMixingNode.class);

    private final MessagingService messagingService;

    private final Queue requestQueue;

    private final MessageListener listener;

    /**
     * @param messagingService
     *            an instance of the messaging service to be able to send
     *            response messages
     * @param nodeName
     *            the name of the mocked node
     * @param requestQueueName
     *            the name of the queue the node listens to
     * @param responseQueueName
     *            the name of the queue the node responds to
     */
    public MockMixingNode(MessagingService messagingService, String nodeName, String requestQueueName,
            String responseQueueName, MessageListener listener)
            throws MessagingException {
        this.messagingService = messagingService;
        this.requestQueue = new Queue(requestQueueName);
        this.listener = listener;

        // Start listening to the request queue.
        messagingService.createReceiver(requestQueue, listener);
    }

    @Override
    public void close() {
        try {
            messagingService.destroyReceiver(requestQueue, listener);
        } catch (MessagingException e) {
            logger.error("The mock messaging listener failed to destroy its receiver for queue {}", requestQueue);
        }
    }
}
