/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.ui.ws.rs.operation;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.scytl.products.ov.channel.cc.orchestrator.ui.ws.rs.config.QpidMessageBroker;

import junit.framework.TestSuite;

/**
 * A group of tests that need an actual message broker.
 */
@RunWith(Suite.class)
@SuiteClasses({AlwaysFailMixDecArquillian.class, EventuallyWorkMixDecArquillian.class,
        MixDecBallotBoxesResourceArquillian.class, ChoiceCodesResourceArquillian.class })
public class MessageBrokerArquillianTestSuite extends TestSuite {

    @ClassRule
    public static QpidMessageBroker messageBroker = new QpidMessageBroker(MessageBrokerArquillianTestSuite.class
        .getClassLoader().getResource(QpidMessageBroker.CONFIG_FILE_PATH).toExternalForm());
}
