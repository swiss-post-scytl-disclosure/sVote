/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.commons.polling;

import java.util.Optional;
import java.util.UUID;

/**
 * To be used in combination with PollingService {@link PollingService}, a
 * PollingResultsHandler interacts with the resource to be polled defining when
 * the results are ready to be fetched, and what actions to perform with them
 * before being returned (if any).
 * 
 * @param <T>
 *            Type of the value to be returned when ready
 */
public interface ResultsHandler<T> {

    /**
     * Handles the results if they are ready.
     * 
     * @param correlationId
     *            key to be used to identify the required value in the resource
     * @return An optional with the handled results if they are ready or an
     *         empty optional otherwise
     */
    Optional<T> handleResultsIfReady(UUID correlationId);
}
