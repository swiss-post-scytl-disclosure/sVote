/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.domain.platform;

import javax.ejb.Stateless;

import com.scytl.products.ov.commons.beans.validation.CertificateValidationServiceImpl;

@Stateless
@OrchestratorCertificateValidationService
public class OrchestratorCertificateValidationServiceImpl extends CertificateValidationServiceImpl {

}
