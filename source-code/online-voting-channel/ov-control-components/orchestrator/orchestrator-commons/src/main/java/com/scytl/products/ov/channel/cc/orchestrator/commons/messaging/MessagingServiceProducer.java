/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.commons.messaging;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;

import com.scytl.products.ov.commons.messaging.MessagingService;
import com.scytl.products.ov.commons.messaging.MessagingServiceImpl;

/**
 * Producer for {@link MessagingService}.
 */
@Dependent
public class MessagingServiceProducer {

    /**
     * Returns a {@link MessagingService} instance.
     * 
     * @return a {@link MessagingService} instance.
     */
    @Produces
    @ApplicationScoped
    public MessagingService messagingService() {
        return new MessagingServiceImpl.Builder()
            .setHostName(System.getProperty("cc_mb_hostname"))
            .setPort(Integer.parseInt(System.getProperty("cc_mb_port")))
            .setVirtualHost(System.getProperty("cc_mb_vhost"))
            .setUsername(System.getProperty("cc_mb_user"))
            .setPassword(System.getProperty("cc_mb_password"))
            .setSenderPoolSize(Runtime.getRuntime().availableProcessors())
            .useSSL().build();
    }

    public void shutdownMessagingService(
            @Disposes MessagingService service) {
        service.shutdown();
    }
}
