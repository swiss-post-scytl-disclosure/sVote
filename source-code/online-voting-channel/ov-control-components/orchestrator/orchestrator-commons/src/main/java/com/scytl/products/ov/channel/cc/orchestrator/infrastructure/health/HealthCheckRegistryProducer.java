/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.infrastructure.health;

import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.sql.DataSource;

import com.scytl.products.ov.channel.cc.orchestrator.infrastructure.logging.OrchestratorLoggingInitializationState;
import com.scytl.products.ov.commons.infrastructure.health.CachingHealthCheck;
import com.scytl.products.ov.commons.infrastructure.health.DatabaseHealthCheck;
import com.scytl.products.ov.commons.infrastructure.health.HealthCheckRegistry;
import com.scytl.products.ov.commons.infrastructure.health.HealthCheckValidationType;
import com.scytl.products.ov.commons.logging.infrastructure.health.SecureLoggerHealthCheck;
import com.scytl.products.ov.commons.util.PropertiesFileReader;

@Dependent
public class HealthCheckRegistryProducer {

    @Inject
    private OrchestratorLoggingInitializationState loggingInitializationState;

    @Resource(name = "or")
    DataSource dataSource;

    @Produces
    public HealthCheckRegistry getHealthCheckRegistry() {
        final PropertiesFileReader properties = PropertiesFileReader.getInstance();
        final String validationQuery = properties.getPropertyValue("health.check.db.validation.query");
        final int cacheTtl = Integer.parseInt(properties.getPropertyValue("health.check.db.validation.cache.ttl"));

        final HealthCheckRegistry healthCheckRegistry = new HealthCheckRegistry();
        healthCheckRegistry.register(HealthCheckValidationType.DATABASE,
            new CachingHealthCheck(new DatabaseHealthCheck(dataSource, validationQuery), cacheTtl));
        healthCheckRegistry.register(HealthCheckValidationType.LOGGING_INITIALIZED,
            new SecureLoggerHealthCheck(loggingInitializationState));

        return healthCheckRegistry;
    }
}
