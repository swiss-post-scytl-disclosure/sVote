/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.infrastructure.logging;

import javax.ejb.Singleton;

import com.scytl.products.ov.commons.beans.domain.model.platform.LoggingInitializationState;

@Singleton
public class OrchestratorLoggingInitializationState extends LoggingInitializationState {

}
