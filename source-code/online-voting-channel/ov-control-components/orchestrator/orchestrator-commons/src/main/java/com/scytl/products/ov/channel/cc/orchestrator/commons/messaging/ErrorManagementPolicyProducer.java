/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.commons.messaging;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import com.scytl.products.ov.commons.errormanagement.ExecutionPolicy;
import com.scytl.products.ov.commons.errormanagement.ExponentialBackoffExecutionPolicy;
import com.scytl.products.ov.commons.messaging.DestinationNotFoundException;
import com.scytl.products.ov.commons.messaging.InvalidMessageException;

/**
 * Produces error management policies
 */
@Dependent
public class ErrorManagementPolicyProducer {

    private static final int DEFAULT_MAX_RETRIES = 10;

    private static final int DEFAULT_INITIAL_DELAY = 1000; // 1 second

    @Produces
    @ApplicationScoped
    public ExecutionPolicy errorManagementPolicy() {
        return new ExponentialBackoffExecutionPolicy(DEFAULT_MAX_RETRIES, DEFAULT_INITIAL_DELAY,
            Stream.of(DestinationNotFoundException.class, InvalidMessageException.class).collect(Collectors.toSet()));
    }
}
