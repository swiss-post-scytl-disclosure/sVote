/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.commons.polling;

import java.util.UUID;

/**
 * <p>
 * Extension of {@link ResultsHandler} which allows external notification about
 * of the readiness of the results.
 * <p>
 * Implementation must be thread-safe.
 */
public interface ReactiveResultsHandler<T> extends ResultsHandler<T> {
    /**
     * Notifies the handler that the results with a given correlation identifier
     * are ready for {@link #handleResultsIfReady(UUID)} method.
     * 
     * @param correlationId
     *            the identifier.
     */
    void resultsReady(UUID correlationId);
}
