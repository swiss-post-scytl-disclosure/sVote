--------------------------------------------------------
--  DDL for Table PARTIAL_RESULTS
--------------------------------------------------------

CREATE TABLE "OR_PARTIAL_RESULTS"
(
"CORRELATION_ID_HI" NUMBER(19,0) NOT NULL,
"CORRELATION_ID_LO" NUMBER(19,0) NOT NULL,
"RESULT" BLOB NOT NULL
);


--------------------------------------------------------
--  DDL for Index PARTIAL_RESULTS_CORRELATION
--------------------------------------------------------

CREATE INDEX "OR_PARTIAL_RESULTS_CORRELATION" ON "OR_PARTIAL_RESULTS" ("CORRELATION_ID_HI","CORRELATION_ID_LO");
