/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.commons.config;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.scytl.products.ov.channel.cc.orchestrator.commons.config.TopicsConfig.Parser;
import com.scytl.products.ov.commons.messaging.Topic;

/**
 * Tests of {@link TopicsConfig}.
 */
public class TopicsConfigTest {

    @Test
    public void test() {
        String json = "{\"or-ha\":\"or-ha-fanout\"}";
        Parser parser = new Parser();
        parser.parse(json);
        assertEquals(new Topic("or-ha-fanout"), parser.ha);
    }
}
