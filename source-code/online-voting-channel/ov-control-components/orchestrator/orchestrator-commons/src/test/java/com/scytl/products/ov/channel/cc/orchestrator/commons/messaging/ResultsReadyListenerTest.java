/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.commons.messaging;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.nio.ByteBuffer;
import java.util.UUID;

import org.junit.Test;

import com.scytl.products.ov.channel.cc.orchestrator.commons.polling.ReactiveResultsHandler;

/**
 * Tests of {@link ResultsReadyListener}.
 */
public class ResultsReadyListenerTest {

    @Test
    public void testOnMessage() {
        UUID correlationId = UUID.randomUUID();
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES * 2);
        buffer.putLong(correlationId.getMostSignificantBits());
        buffer.putLong(correlationId.getLeastSignificantBits());

        @SuppressWarnings("unchecked")
        ReactiveResultsHandler<Object> handler =
            mock(ReactiveResultsHandler.class);
        ResultsReadyListener listener = new ResultsReadyListener(handler);

        listener.onMessage(buffer.array());

        verify(handler).resultsReady(correlationId);
    }
}
