/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services;

import javax.inject.Inject;

import com.scytl.products.ov.channel.cc.orchestrator.commons.config.QueuesConfig;
import com.scytl.products.ov.channel.cc.orchestrator.commons.infrastructure.persistence.Jdbc;
import com.scytl.products.ov.channel.cc.orchestrator.commons.infrastructure.persistence.PartialResultsRepository;
import com.scytl.products.ov.channel.cc.orchestrator.commons.polling.AbstractPartialResultsHandler;

public final class ChoiceCodesGenerationResultsHandler
        extends AbstractPartialResultsHandler<byte[]> {

    /**
     * Constructor.
     * 
     * @param repository
     */
    @Inject
    public ChoiceCodesGenerationResultsHandler(
            @Jdbc PartialResultsRepository<byte[]> repository) {
        super(repository);
    }

    @Override
    protected int getPartialResultsCount() {
        return QueuesConfig.GENERATION_CONTRIBUTIONS_RES_QUEUES.length;
    }
}
