/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.choicecodes.infrastructure.queues;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services.ChoiceCodesDecryptionContributionsService;
import com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services.ChoiceCodesGenerationContributionsService;
import com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services.ChoiceCodesKeyGenerationService;
import com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services.ChoiceCodesVerificationContributionsService;
import com.scytl.products.ov.commons.messaging.MessagingException;

/**
 * Defines any steps to be performed when the ORCHESTRATOR context is first
 * initialized and destroyed.
 */
public class OrchestratorChoiceCodesQueuesContextListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private static final String CONTEXT = "OR";

    @Inject
    ChoiceCodesDecryptionContributionsService choiceCodesDecryptionContributionsService;

    @Inject
    ChoiceCodesVerificationContributionsService choiceCodesVerificationContributionsService;

    @Inject
    ChoiceCodesGenerationContributionsService choiceCodesGenerationContributionsService;

    @Inject
    ChoiceCodesKeyGenerationService choiceCodesKeyGenerationService;

    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {

        LOG.info(CONTEXT + " - triggering the consumption of the Control Components response queues");

        try {
            choiceCodesDecryptionContributionsService.startup();
            choiceCodesVerificationContributionsService.startup();
            choiceCodesGenerationContributionsService.startup();
            choiceCodesKeyGenerationService.startup();
        } catch (MessagingException e) {
            LOG.error("Error consuming a Control Component response queue: ", e);
            throw new IllegalStateException("Error consuming a Control Component response queue: ", e);
        }

    }

    @Override
    public void contextDestroyed(final ServletContextEvent sce) {
        LOG.info(CONTEXT + " - triggering the disconnetion of the Control Components response queues");

        try {
            choiceCodesDecryptionContributionsService.shutdown();
            choiceCodesVerificationContributionsService.shutdown();
            choiceCodesGenerationContributionsService.shutdown();
            choiceCodesKeyGenerationService.shutdown();
        } catch (MessagingException e) {
            LOG.error("Error disconnecting from a Control Component response queue: ", e);
            throw new IllegalStateException("Error disconnectiong a Control Component response queue: ", e);
        }
    }
}
