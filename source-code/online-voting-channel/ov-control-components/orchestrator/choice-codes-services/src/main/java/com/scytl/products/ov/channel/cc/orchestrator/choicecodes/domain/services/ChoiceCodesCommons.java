/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services;

import com.scytl.products.ov.commons.dto.WithDTOResultsKeyFields;

public class ChoiceCodesCommons {
	
    private static final String ID_SEPARATOR = ".";

    private static final String COMMONS_TYPE = "CHOICE_CODES_";

    private static final String DECRYPT_ID = COMMONS_TYPE + "DECRYPT";

    private static final String COMPUTE_ID = COMMONS_TYPE + "COMPUTE";

    private static final String KEY_GENERATION_ID = COMMONS_TYPE + "KEY_GENERATION";
    
  
	private ChoiceCodesCommons() {
	}
	
    private static String getPartialResultsKey(String... resultsKeyFields) {
        return String.join(ID_SEPARATOR, resultsKeyFields);
    }

    private static String getDecryptPartialResultsKey(String... resultsKeyFields) {
        return String.join(ID_SEPARATOR, DECRYPT_ID, getPartialResultsKey(resultsKeyFields));
    }

    public static String getDecryptPartialResultsKey(WithDTOResultsKeyFields withDTOResultsKeyFields) {
        return getDecryptPartialResultsKey(withDTOResultsKeyFields.getResultsKeyFields());
    }

    private static String getComputePartialResultsKey(String... resultsKeyFields) {
        return String.join(ID_SEPARATOR, COMPUTE_ID, getPartialResultsKey(resultsKeyFields));
    }

    public static String getComputePartialResultsKey(WithDTOResultsKeyFields withDTOResultsKeyFields) {
        return getComputePartialResultsKey(withDTOResultsKeyFields.getResultsKeyFields());
    }

    public static String getChoiceCodesKeyGenerationKey(WithDTOResultsKeyFields keyCreationDTO) {
        return String.join(ID_SEPARATOR, KEY_GENERATION_ID, getPartialResultsKey(keyCreationDTO.getResultsKeyFields()));
    }

}
