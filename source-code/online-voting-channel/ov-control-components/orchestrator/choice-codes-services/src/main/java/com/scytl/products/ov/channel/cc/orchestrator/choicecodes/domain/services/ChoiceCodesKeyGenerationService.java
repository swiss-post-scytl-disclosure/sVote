/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services;

import java.time.ZonedDateTime;
import java.util.List;

import com.scytl.cryptolib.api.exceptions.GeneralCryptoLibException;
import com.scytl.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.messaging.MessagingException;

public interface ChoiceCodesKeyGenerationService {

    ChoiceCodesKeysResponse requestChoiceCodesKeyGenerationSync(String trackingId, String tenantId,
            String electionEventId, final List<String> verificationCardSetIds, ZonedDateTime keysDateFrom,
            ZonedDateTime keysDateTo, ElGamalEncryptionParameters elGamalEncryptionParameters)
            throws ResourceNotFoundException, GeneralCryptoLibException;

    void startup() throws MessagingException;
    
    void shutdown() throws MessagingException;
}
