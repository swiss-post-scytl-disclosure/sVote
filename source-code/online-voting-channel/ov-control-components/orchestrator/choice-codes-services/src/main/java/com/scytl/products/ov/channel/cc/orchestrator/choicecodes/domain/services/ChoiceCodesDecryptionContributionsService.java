/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.choicecodes.domain.services;

import java.util.List;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.dto.ChoiceCodesVerificationDecryptResPayload;
import com.scytl.products.ov.commons.messaging.MessagingException;

public interface ChoiceCodesDecryptionContributionsService {

    List<ChoiceCodesVerificationDecryptResPayload> requestChoiceCodesDecryptionContributionsSync(String trackingId,
            String tenantId, String electionEventId, String verificationCardSetId, String verificationCardId,
            String gammaStr) throws ResourceNotFoundException;

    void startup() throws MessagingException;
    
    void shutdown() throws MessagingException;
}
