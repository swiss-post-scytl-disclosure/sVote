/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.infrastructure.remote;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.remote.ElectionInformationClient;
import org.slf4j.Logger;

import javax.inject.Inject;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitConsumer;
import com.scytl.products.ov.commons.tracking.TrackIdInstance;
import com.scytl.products.ov.commons.util.PropertiesFileReader;
import com.scytl.products.ov.commons.validation.ValidationResult;
import com.scytl.products.ov.commons.infrastructure.remote.client.RetrofitException;

/**
 * The validation repository implementation.
 */
public class ValidationRepositoryImpl {

	// The name of the property path.
	private static final String PROPERTY_PATH_VALIDATIONS = "path.validations";

	// The value of the property validations.
	private static final String PATH_VALIDATIONS =
		PropertiesFileReader.getInstance().getPropertyValue(PROPERTY_PATH_VALIDATIONS);

	@Inject
	private TrackIdInstance trackId;

	private ElectionInformationClient electionInformationClient;

	@Inject
	private Logger LOG;

	@Inject
	ValidationRepositoryImpl(final ElectionInformationClient electionInformationClient) {
		this.electionInformationClient = electionInformationClient;
	}

	/**
	 * Validates if an election is in dates.
	 * 
	 * @param tenantId - the tenant id
	 * @param electionEventId - the election event id.
	 * @param ballotBoxId - the ballot box id.
	 * @return the result of the validation.
	 * @throws ResourceNotFoundException
	 */
	public ValidationResult validateElectionInDates(String tenantId, String electionEventId, String ballotBoxId)
			throws ResourceNotFoundException {
		try {
			LOG.info("Validating election dates in authentication");
			ValidationResult ballotBoxStatus = RetrofitConsumer.processResponse(getElectionInformationClient().validateElectionInDates(trackId.getTrackId(),
				PATH_VALIDATIONS, tenantId, electionEventId, ballotBoxId));
			LOG.info("Election is: {}", ballotBoxStatus.getValidationError().getValidationErrorType().name());
			return ballotBoxStatus;
		} catch (RetrofitException e) {
			throw e;
		} catch (ResourceNotFoundException e) {
			throw e;
		}
	}

	/**
	 * Gets electionInformationClient.
	 *
	 * @return Value of electionInformationClient.
	 */
	public ElectionInformationClient getElectionInformationClient() {
		return electionInformationClient;
	}
}
