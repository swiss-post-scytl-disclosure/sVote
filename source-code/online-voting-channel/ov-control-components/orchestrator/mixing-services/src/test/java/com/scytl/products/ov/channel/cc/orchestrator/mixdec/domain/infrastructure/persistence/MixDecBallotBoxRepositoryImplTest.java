/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.infrastructure.persistence;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.MixDecBallotBox;
import com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.persistence.MixDecBallotBoxRepositoryImpl;

@RunWith(MockitoJUnitRunner.class)
public class MixDecBallotBoxRepositoryImplTest {

    private static String TENANT_ID = "100";

    private static String EE_ID = "2903294";

    private static String BB_ID = "3902904";

    @InjectMocks
    private MixDecBallotBoxRepositoryImpl sut = new MixDecBallotBoxRepositoryImpl();

    @Mock
    private EntityManager entityManagerMock;

    @Mock
    private TypedQuery<MixDecBallotBox> mockQuery;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this.getClass());
    }

    @Test
    public void testFindByTenantEEAndBBAndObtainResults() {

        when(entityManagerMock.createQuery(anyString(), eq(MixDecBallotBox.class))).thenReturn(mockQuery);
        List<MixDecBallotBox> votes = singletonList(new MixDecBallotBox());
        when(mockQuery.getResultList()).thenReturn(votes);
        List<MixDecBallotBox> obtainedVotes = sut.findByTenantIdElectionEventIdBallotBoxId(TENANT_ID, EE_ID, BB_ID);

        assertFalse(obtainedVotes.isEmpty());
    }

    @Test
    public void testFindByTenantEEAndBBNoResults() {

        when(entityManagerMock.createQuery(anyString(), eq(MixDecBallotBox.class))).thenReturn(mockQuery);
        List<MixDecBallotBox> votes = emptyList();
        when(mockQuery.getResultList()).thenReturn(votes);
        List<MixDecBallotBox> obtainedVotes = sut.findByTenantIdElectionEventIdBallotBoxId(TENANT_ID, EE_ID, BB_ID);

        assertTrue(obtainedVotes.isEmpty());
    }
}
