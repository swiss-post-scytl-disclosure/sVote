/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.scytl.products.ov.commons.dto.KeyCreationDTO;

public class MixDecCommonsTest {

    @Test
    public void whenGenerateKeyThenExpectedValue() {

        String ee = "AAAAAAAAAAA";
        String ea = "BBBBBBBBBBB";
        String tracking = "CCCCCCCCCCC";

        KeyCreationDTO keyCreationDTO = new KeyCreationDTO();
        keyCreationDTO.setElectionEventId(ee);
        keyCreationDTO.setResourceId(ea);
        keyCreationDTO.setRequestId(tracking);

        String output = MixDecCommons.getMixDecKeyGenerationKey(keyCreationDTO);
        String expectedOutput = String.format("MIXDEC_KEY_GENERATION.%s.%s.%s", ee, ea, tracking);

        assertEquals(expectedOutput, output);
    }
}
