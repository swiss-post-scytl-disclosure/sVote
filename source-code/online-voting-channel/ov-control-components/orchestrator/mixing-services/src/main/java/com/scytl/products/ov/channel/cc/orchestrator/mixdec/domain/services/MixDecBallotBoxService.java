/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.services;

import com.scytl.products.ov.commons.dto.MixingDTO;
import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.BallotBoxStatus;
import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.MixDecBallotBox;
import com.scytl.products.ov.commons.beans.domain.model.EntityId;
import com.scytl.products.ov.commons.beans.exceptions.ApplicationException;
import com.scytl.products.ov.commons.beans.domain.model.ballotbox.BallotBoxId;
import com.scytl.products.ov.commons.dto.mixing.MixingPayload;
import com.scytl.products.ov.commons.messaging.MessagingException;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;

public interface MixDecBallotBoxService {


    /**
     * Starts the service up.
     *
     * @throws MessagingException
     */
    void startup() throws MessagingException;
    
    /**
     * Shuts the service down.
     *
     * @throws MessagingException
     */
    void shutdown() throws MessagingException;

    List<BallotBoxStatus> processBallotBoxes(String tenantId, String electionEventId,
            List<EntityId> ballotBoxIds, String trackingId);

    List<MixDecBallotBox> getProcessedBallotBox(String tenantId, String electionEventId, String ballotBoxId);

    void writeEncryptedBallotBox(OutputStream stream, String tenantId, String electionEventId, String ballotBoxId)
            throws IOException;

    void addProcessedBallotBoxWithStatus(MixingPayload payload);

    void addMixingErrorStatus(String tenantId, String electionEventId, String ballotBoxId, String errorMessage);

    void addProcessingBallotBoxError(String tenantId, String electionEventId, String ballotBoxId, String errorMessage);

    List<BallotBoxStatus> getMixDecBallotBoxStatus(String tenantId, String electionEventId, String[] ballotBoxIds);

    /**
     * Start a mixing-and-decryption process from a DTO.
     * @param mixingDTO the data to process.
     */
    void process(MixingDTO mixingDTO) throws TimeoutException, ApplicationException;

    /**
     * Gets the payloads that resulted of mixing a ballot box, as JSON strings.
     * 
     * @param mixingDTO
     *            the data to process.
     * @return the payloads as JSON strings, indexed by an identifier containing
     *         the ballot box ID, the node name and the vote set index.
     */
    Stream<PayloadJson> getOutputPayloadsJson(BallotBoxId ballotBoxId);
}
