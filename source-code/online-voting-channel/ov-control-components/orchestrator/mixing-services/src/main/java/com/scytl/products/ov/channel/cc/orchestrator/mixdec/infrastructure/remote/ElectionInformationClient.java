/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

import javax.ws.rs.core.MediaType;

import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.ui.Constants;
import com.scytl.products.ov.commons.validation.ValidationResult;

import okhttp3.ResponseBody;

/**
 * Defines the methods to access via REST to a set of operations.
 */
public interface ElectionInformationClient {

    String PATH_CLEANSED_BALLOT_BOXES = "pathCleansedBallotBoxes";

    /** The Constant PATH_VALIDATIONS. */
    String PATH_VALIDATIONS = "pathValidations";

    /** The Constant PATH_BALLOT_BOX. */
    String PATH_BALLOT_BOX = "pathBallotBox";

    /** The Constant BALLOT_BOX_ID. */
    String BALLOT_BOX_ID = "ballotBoxId";

    /** The Constant TENANT_ID. */
    String TENANT_ID = "tenantId";

    /** The Constant ELECTION_EVENT_ID. */
    String ELECTION_EVENT_ID = "electionEventId";

    /** The Accept Parameter header */
    String PARAMETER_ACCEPT = "Accept";

    /**
     * Gets the ballot box information.
     * 
     * @param trackId
     *            the track id
     * @param pathBallotBox
     *            the path ballot box
     * @param tenantId
     *            the tenant id
     * @param electionEventId
     *            the election event id
     * @param ballotBoxId
     *            the ballot box id
     * @return the ballot box information
     * @throws ResourceNotFoundException
     *             the resource not found exception
     */
    @GET("{pathBallotBox}/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}")
    @Headers("Accept:" + MediaType.APPLICATION_JSON)
    Call<String> getBallotBoxInformation(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String trackId,
            @Path(PATH_BALLOT_BOX) String pathBallotBox, @Path(TENANT_ID) String tenantId,
            @Path(ELECTION_EVENT_ID) String electionEventId, @Path(BALLOT_BOX_ID) String ballotBoxId)
            throws ResourceNotFoundException;

    /**
     * The endpoint to validate if an election is in dates.
     *
     * @param requestId
     *            - the request id for logging purposes.
     * @param pathValidations
     *            the path materials
     * @param tenantId
     *            - the tenant id
     * @param electionEventId
     *            - the election event id
     * @param ballotBoxId
     *            - the ballot box id
     * @return the validation error for a given tenant, election event and
     *         ballot box ids.
     * @throws ResourceNotFoundException
     *             if the rest operation fails.
     */
    @POST("{pathValidations}/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}")
    Call<ValidationResult> validateElectionInDates(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String requestId,
            @Path(PATH_VALIDATIONS) String pathValidations, @Path(TENANT_ID) String tenantId,
            @Path(ELECTION_EVENT_ID) String electionEventId, @Path(BALLOT_BOX_ID) String ballotBoxId)
            throws ResourceNotFoundException;

    /**
     * Retrieve all encrypted cleansed votes of a specific ballot box
     * 
     * @param requestId
     * @param pathCleansedBallotBoxes
     * @param tenantId
     * @param electionEventId
     * @param ballotBoxId
     * @return
     */
    @GET("{pathCleansedBallotBoxes}/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}")
    @Headers("Accept:" + MediaType.APPLICATION_OCTET_STREAM)
    @Streaming
    Call<ResponseBody> getEncryptedBallotBox(@Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String requestId,
            @Path(PATH_CLEANSED_BALLOT_BOXES) String pathCleansedBallotBoxes, @Path(TENANT_ID) final String tenantId,
            @Path(ELECTION_EVENT_ID) final String electionEventId, @Path(BALLOT_BOX_ID) final String ballotBoxId);


    /**
     * Retrieve all encrypted cleansed votes of a specific ballot box in the form of mixing
     * payloads, to be sent to the control components' mixing nodes.
     *
     * @param requestId a tracking identifier
     * @param pathCleansedBallotBoxes the URL path prefix for cleansed ballot boxes
     * @param tenantId the tenant identifier
     * @param electionEventId the election event identifier
     * @param ballotBoxId the ballot box identifier
     * @return a stream of signed mixing payloads
     */
    @GET("{pathCleansedBallotBoxes}/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/payloads")
    @Headers("Accept:" + MediaType.APPLICATION_OCTET_STREAM)
    @Streaming
    Call<ResponseBody> getMixingPayloads(
            @Header(Constants.PARAMETER_HEADER_X_REQUEST_ID) String requestId,
            @Path(PATH_CLEANSED_BALLOT_BOXES) String pathCleansedBallotBoxes,
            @Path(TENANT_ID) String tenantId,
            @Path(ELECTION_EVENT_ID) String electionEventId,
            @Path(BALLOT_BOX_ID) String ballotBoxId);

}
