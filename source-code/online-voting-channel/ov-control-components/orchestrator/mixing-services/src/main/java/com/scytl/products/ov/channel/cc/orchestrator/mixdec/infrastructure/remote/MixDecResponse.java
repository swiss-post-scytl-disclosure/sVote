/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.remote;

import com.scytl.products.ov.commons.dto.MixingDTO;
import java.io.Serializable;

public class MixDecResponse implements Serializable {
    private static final long serialVersionUID = 1270110859020205368L;

    private MixingDTO mixingDTO;

    private String errorMessage;

    public MixingDTO getMixingDTO() {
        return mixingDTO;
    }

    public void setMixingDTO(MixingDTO mixingDTO) {
        this.mixingDTO = mixingDTO;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
