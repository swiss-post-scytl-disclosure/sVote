/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.persistence;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.MixDecBallotBoxStatus;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;

public interface MixDecBallotBoxStatusRepository {

    void save(String tenantId, String electionEventId, String ballotBoxId, String status, String errorMessage) throws DuplicateEntryException;

    void update(String tenantId, String electionEventId, String ballotBoxId, String status, String errorMessage) throws EntryPersistenceException, ResourceNotFoundException;

    MixDecBallotBoxStatus getMixDecBallotBoxStatus(String tenantId, String electionEventId, String ballotBoxId) throws ResourceNotFoundException;

}
