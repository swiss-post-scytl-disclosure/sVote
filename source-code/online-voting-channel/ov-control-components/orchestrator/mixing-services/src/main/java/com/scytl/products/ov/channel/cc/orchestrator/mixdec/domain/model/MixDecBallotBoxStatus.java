/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scytl.products.ov.commons.domain.model.Constants;

/**
 * Entity representing a ballot box.
 */
@Entity
@Table(name = "MIXDEC_BALLOT_BOX_STATUS")
public class MixDecBallotBoxStatus {

    /** The id of a ballot box. */
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "mixDecBallotBoxStatusSeq")
    @SequenceGenerator(name = "mixDecBallotBoxStatusSeq", sequenceName = "MIXDEC_BALLOT_BOX_STATUS_SEQ")
    @JsonIgnore
    private Integer id;

    /** The tenant id. */
    @Column(name = "TENANT_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    private String tenantId;

    /** The voting card id. */
    @Column(name = "ELECTION_EVENT_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    private String electionEventId;

    /** The ballot box id. */
    @Column(name = "BALLOT_BOX_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    private String ballotBoxId;

    /** The mixdec status of the ballot box. */
    @Column(name = "STATUS")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_10)
    private String status;
    
    @Column(name = "ERROR_MESSAGE")
    @Size(max = Constants.COLUMN_LENGTH_150)
    private String errorMessage;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(final String tenantId) {
        this.tenantId = tenantId;
    }

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public void setBallotBoxId(final String ballotBoxId) {
        this.ballotBoxId = ballotBoxId;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public void setElectionEventId(final String electionEventId) {
        this.electionEventId = electionEventId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
