/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.persistence;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.MixDecBallotBox;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import java.util.List;

public interface MixDecBallotBoxRepository {

    /**
     * Find a mixed and decrypted ballot box that matches the input params.
     *
     * @param tenantId
     * @param electionEventId
     * @param ballotBoxId
     * @return
     */
    List<MixDecBallotBox> findByTenantIdElectionEventIdBallotBoxId(String tenantId, String electionEventId,
            String ballotBoxId);
    
    
    /**
     * Find a mixed and decrypted ballot box that matches in input params, paginated
     * 
     * @param tenantId
     * @param electionEventId
     * @param ballotBoxId
     * @param start first record
     * @param size maximum records retrieved 
     */
    List<MixDecBallotBox> findByTenantIdElectionEventIdBallotBoxId(String tenantId, String electionEventId,
            String ballotBoxId, int start, int size);

    /**
     * Saves a mixed decrypted ballot box vote
     * 
     * @param tenantId
     * @param electionEventId
     * @param ballotBoxId
     * @param encryptedVote
     * 
     * @throws DuplicateEntryException
     */
    void save(String tenantId, String electionEventId, String ballotBoxId, String encryptedVote)
            throws DuplicateEntryException;
}
