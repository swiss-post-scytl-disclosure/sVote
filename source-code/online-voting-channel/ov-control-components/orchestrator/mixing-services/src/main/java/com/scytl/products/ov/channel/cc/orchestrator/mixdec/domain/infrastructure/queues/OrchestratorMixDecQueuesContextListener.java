/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.infrastructure.queues;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.services.MixDecBallotBoxService;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.services.MixDecKeyGenerationService;
import com.scytl.products.ov.commons.messaging.MessagingException;

/**
 * Defines any steps to be performed when the ORCHESTRATOR context is first
 * initialized and destroyed.
 */
public class OrchestratorMixDecQueuesContextListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger("std");

    private static final String CONTEXT = "OR";

    @Inject
    MixDecKeyGenerationService mixDecKeyGenerationService;

    @Inject
    MixDecBallotBoxService mixDecBallotBoxService;

    /**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {

        LOG.info(CONTEXT + " - triggering the consumption of the Control Components response queues");

        try {
            mixDecKeyGenerationService.startup();
            mixDecBallotBoxService.startup();
        } catch (MessagingException e) {
            LOG.error("Error consuming a Control Component response queue: ", e);
            throw new IllegalStateException("Error consuming a Control Component response queue: ", e);
        }

    }

    /**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    @Override
    public void contextDestroyed(final ServletContextEvent sce) {

    }
}
