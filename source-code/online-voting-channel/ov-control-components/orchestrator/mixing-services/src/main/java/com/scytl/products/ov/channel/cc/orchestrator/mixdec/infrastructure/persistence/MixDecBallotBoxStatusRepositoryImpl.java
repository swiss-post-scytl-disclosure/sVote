/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.persistence;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.MixDecBallotBoxStatus;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;
import com.scytl.products.ov.commons.beans.exceptions.EntryPersistenceException;
import com.scytl.products.ov.commons.beans.exceptions.ResourceNotFoundException;
import com.scytl.products.ov.commons.dto.MixingError;
import com.scytl.products.ov.commons.infrastructure.persistence.BaseRepositoryImpl;

@Stateless
public class MixDecBallotBoxStatusRepositoryImpl extends BaseRepositoryImpl<MixDecBallotBoxStatus, Integer>
        implements MixDecBallotBoxStatusRepository {

    private static final String PARAMETER_TENANT_ID = "tenantId";

    private static final String PARAMETER_ELECTION_EVENT_ID = "electionEventId";

    private static final String PARAMETER_BALLOT_BOX_ID = "ballotBoxId";

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(String tenantId, String electionEventId, String ballotBoxId, String status, String errorMessage)
            throws DuplicateEntryException {

        MixDecBallotBoxStatus mixDecBallotBoxStatus = new MixDecBallotBoxStatus();
        mixDecBallotBoxStatus.setTenantId(tenantId);
        mixDecBallotBoxStatus.setElectionEventId(electionEventId);
        mixDecBallotBoxStatus.setBallotBoxId(ballotBoxId);
        mixDecBallotBoxStatus.setStatus(status);
        mixDecBallotBoxStatus.setErrorMessage(cutErrorMessage(errorMessage));

        save(mixDecBallotBoxStatus);
    }

    @Override
    public MixDecBallotBoxStatus getMixDecBallotBoxStatus(String tenantId, String electionEventId, String ballotBoxId)
            throws ResourceNotFoundException {
        try {
            final TypedQuery<MixDecBallotBoxStatus> query = entityManager.createQuery(
                "from MixDecBallotBoxStatus where tenantId = :tenantId and electionEventId = :electionEventId and ballotBoxId = :ballotBoxId ",
                MixDecBallotBoxStatus.class);

            query.setParameter(PARAMETER_TENANT_ID, tenantId);
            query.setParameter(PARAMETER_ELECTION_EVENT_ID, electionEventId);
            query.setParameter(PARAMETER_BALLOT_BOX_ID, ballotBoxId);

            return query.getSingleResult();
        } catch (NoResultException ex) {
            throw new ResourceNotFoundException(
                "Unable to find status in the database of the ballot box " + ballotBoxId, ex);
        }
    }

    @Override
    public void update(String tenantId, String electionEventId, String ballotBoxId, String status, String errorMessage)
            throws EntryPersistenceException, ResourceNotFoundException {

        MixDecBallotBoxStatus mixDecBallotBoxStatus = getMixDecBallotBoxStatus(tenantId, electionEventId, ballotBoxId);

        mixDecBallotBoxStatus.setStatus(status);
        mixDecBallotBoxStatus.setErrorMessage(cutErrorMessage(errorMessage));

        update(mixDecBallotBoxStatus);
    }

    private String cutErrorMessage(String errorMessage) {
        if (errorMessage != null && errorMessage.length() > 150) {
            return errorMessage.substring(0, 150);
        }
        return errorMessage;
    }

}
