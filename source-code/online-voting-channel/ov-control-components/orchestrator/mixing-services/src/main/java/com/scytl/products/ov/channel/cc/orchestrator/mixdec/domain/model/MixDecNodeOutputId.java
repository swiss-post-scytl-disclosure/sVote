/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model;

import com.scytl.products.ov.commons.beans.domain.model.vote.VoteSetId;
import com.scytl.products.ov.commons.domain.model.Constants;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class MixDecNodeOutputId implements Serializable {

    private static final long serialVersionUID = -1627650493332590139L;

    // The tenant identifier
    @Column(name = "TENANT_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    private String tenantId;

    // The election event identifier
    @Column(name = "ELECTION_EVENT_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    private String electionEventId;

    // The ballot box identifier
    @Column(name = "BALLOT_BOX_ID")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    private String ballotBoxId;

    // The locator of the vote set in the ballot box
    @Column(name = "VOTE_SET_INDEX")
    @NotNull
    private int voteSetIndex;

    // The name of the node that processed the payload
    @Column(name = "NODE_NAME")
    @NotNull
    @Size(max = Constants.COLUMN_LENGTH_100)
    private String nodeName;
    
    @SuppressWarnings("unused") // No-argument constructor to keep JPA happy. 
    private MixDecNodeOutputId() {
    }

    public MixDecNodeOutputId(String tenantId, String electionEventId, String ballotBoxId,
            int voteSetIndex, String nodeName) {
        this.tenantId = tenantId;
        this.electionEventId = electionEventId;
        this.ballotBoxId = ballotBoxId;
        this.nodeName = nodeName;
        this.voteSetIndex = voteSetIndex;
    }

    public MixDecNodeOutputId(VoteSetId voteSetId, String nodeName) {
        tenantId = voteSetId.getBallotBoxId().getTenantId();
        electionEventId = voteSetId.getBallotBoxId().getElectionEventId();
        ballotBoxId = voteSetId.getBallotBoxId().getId();
        voteSetIndex = voteSetId.getIndex();
        this.nodeName = nodeName;
    }

    @Override
    public boolean equals(Object o) {
        if (null == o) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (o.getClass() != MixDecNodeOutput.class) {
            return false;
        }

        MixDecNodeOutputId other = (MixDecNodeOutputId) o;
        return Objects.equals(tenantId, other.tenantId) && Objects
                .equals(electionEventId, other.electionEventId) && Objects
                .equals(ballotBoxId, other.ballotBoxId) && (voteSetIndex == other.voteSetIndex)
                && Objects.equals(nodeName, other.nodeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tenantId, electionEventId, ballotBoxId, voteSetIndex, nodeName);
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getElectionEventId() {
        return electionEventId;
    }

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public int getVoteSetIndex() {
        return voteSetIndex;
    }

    public String getNodeName() {
        return nodeName;
    }
}
