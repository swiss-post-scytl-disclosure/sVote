/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model;

public class BallotBoxStatus {

    public static final String STATUS_UNKNOWN = "UNKNOWN";

    public static final  String STATUS_NOT_CLOSED = "NOT_CLOSED";

    public static final String STATUS_NOT_FOUND = "NOT_FOUND";

    public static final String STATUS_PROCESSING = "PROCESSING";

    public static final String STATUS_CLOSED = "CLOSED";

    public static final String STATUS_ERROR = "ERROR";
    
    public static final String STATUS_MIXED = "MIXED";

    private String ballotBoxId;

    private String processStatus = STATUS_UNKNOWN;
    
    private String errorMessage = null;

    public String getBallotBoxId() {
        return ballotBoxId;
    }

    public void setBallotBoxId(String ballotBoxId) {
        this.ballotBoxId = ballotBoxId;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
