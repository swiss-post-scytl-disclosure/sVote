/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.remote;

/**
 * Exception used when the remote resource exists but it's not in the available
 * state
 */
public class ResourceNotReadyException extends Exception {
    private static final long serialVersionUID = 1380313381728852937L;

    public ResourceNotReadyException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
