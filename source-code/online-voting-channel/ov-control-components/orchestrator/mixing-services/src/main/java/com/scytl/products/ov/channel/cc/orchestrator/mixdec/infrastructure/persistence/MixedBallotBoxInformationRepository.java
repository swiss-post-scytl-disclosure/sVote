/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.persistence;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.MixDecBallotBox;
import java.util.List;

public interface MixedBallotBoxInformationRepository {

        List<MixDecBallotBox> findByTenantIdElectionEventIdBallotBoxId(String tenantId, String electionEventId,
                                                                       String ballotBoxId);
}
