/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.remote.producer;

import com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.remote.ElectionInformationClient;
import javax.enterprise.inject.Produces;

public class ElectionInformationRemoteClientProducer extends RemoteClientProducer {

    private static final String URI_ELECTION_INFORMATION = System.getProperty("ELECTION_INFORMATION_CONTEXT_URL");

    @Produces
    ElectionInformationClient electionInformationClient() {
        return createRestClient(URI_ELECTION_INFORMATION, ElectionInformationClient.class);
    }
}
