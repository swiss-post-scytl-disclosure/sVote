/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain;

import com.scytl.products.ov.commons.infrastructure.csv.AbstractCSVWriter;

import java.io.OutputStream;

/**
 * Writer in csv for every item to be downloaded of a ballot box
 */
public class MixDecExportedBallotBoxItemWriter extends AbstractCSVWriter<String> {

    public MixDecExportedBallotBoxItemWriter(OutputStream outputStream) {
        super(outputStream);
    }

    @Override
    protected String[] extractValues(String vote) {
        return new String[] { vote };
    }

}
