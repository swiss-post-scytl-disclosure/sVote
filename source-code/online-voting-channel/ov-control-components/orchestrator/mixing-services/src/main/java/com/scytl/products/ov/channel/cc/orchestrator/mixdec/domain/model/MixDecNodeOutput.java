/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * The results of mixing and decrypting a ballot box in one node.
 */
@Entity
@Table(name = "MIXDEC_NODE_OUTPUT")
public class MixDecNodeOutput {

    @EmbeddedId
    private MixDecNodeOutputId id;

    @Column(name = "PAYLOAD")
    @Lob
    private String payloadJson;

    @SuppressWarnings("unused") // No-argument constructor to keep JPA happy. 
    private MixDecNodeOutput() {
    }

    /**
     * Creates an entity containing the output of a mixing node.
     *
     * @param id the identifier of the output
     * @param payloadJson the node output, in JSON format
     */
    public MixDecNodeOutput(MixDecNodeOutputId id, String payloadJson) {
        this.id = id;
        this.payloadJson = payloadJson;
    }

    public String getNodeName() {
        return id.getNodeName();
    }

    public String getTenantId() {
        return id.getTenantId();
    }

    public String getElectionEventId() {
        return id.getElectionEventId();
    }

    public String getBallotBoxId() {
        return id.getBallotBoxId();
    }

    public int getVoteSetIndex() {
        return id.getVoteSetIndex();
    }

    public String getPayloadJson() {
        return payloadJson;
    }
}
