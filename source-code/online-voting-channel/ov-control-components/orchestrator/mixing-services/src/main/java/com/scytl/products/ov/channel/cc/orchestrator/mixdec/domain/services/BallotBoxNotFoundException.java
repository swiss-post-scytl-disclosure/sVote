/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.services;

/**
 * Failed trying to find the results of mixing and decrypting a ballot box, most likely due to the process never having
 * been started.
 */
@SuppressWarnings("serial")
public class BallotBoxNotFoundException extends Exception {
    public BallotBoxNotFoundException(String ballotBoxId) {
        super(String.format("Ballot box %s is not currently being mixed", ballotBoxId));
    }
}
