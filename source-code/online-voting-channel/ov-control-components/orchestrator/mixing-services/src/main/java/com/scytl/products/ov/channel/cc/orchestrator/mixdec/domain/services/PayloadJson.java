/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.services;

import com.scytl.products.ov.commons.beans.domain.model.ballotbox.BallotBoxId;

/**
 * A container for JSON representing a mixing payload, used to transmit payloads
 * with some metadata without having to deserialise them from JSON.
 */
public class PayloadJson {

    private final BallotBoxId bbid;

    private final String nodeName;

    private final int voteSetIndex;

    private final String contents;

    public PayloadJson(BallotBoxId bbid, String nodeName, int voteSetIndex, String contents) {
        this.bbid = bbid;
        this.nodeName = nodeName;
        this.voteSetIndex = voteSetIndex;
        this.contents = contents;
    }

    public String getFileName() {
        return String.format("%s-%d-%s.json", bbid, voteSetIndex, nodeName);
    }

    public String getContents() {
        return contents;
    }
}
