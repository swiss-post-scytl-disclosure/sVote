/*
 * Copyright 2018 Scytl Secure Electronic Voting SA
 *
 * All rights reserved
 *
 * See our extended copyright notice in *file 'Copyright.txt' which is part of this source code package
 */
package com.scytl.products.ov.channel.cc.orchestrator.mixdec.infrastructure.persistence;

import com.scytl.products.ov.commons.dto.MixingDTO;
import com.scytl.products.ov.channel.cc.orchestrator.mixdec.domain.model.MixDecNodeOutput;
import com.scytl.products.ov.commons.beans.exceptions.DuplicateEntryException;

import java.util.List;

/**
 * Repository that manages the partial results of mixing and decryption, i.e. what comes out of each node after
 * processing.
 */
public interface MixDecNodeOutputRepository {

    /**
     * Saves the results of mixing and decrypting a ballot box.
     *
     * @param mixingDTO the results of mixing and decrypting a ballot box
     * @return the saved partial results entity
     */
    MixDecNodeOutput save(MixingDTO mixingDTO) throws DuplicateEntryException;

    /**
     * Find the output of all mixing nodes for a particular ballot box.
     *
     * @param tenantId the tenant identifier
     * @param electionEventId the election event identifier
     * @param ballotBoxId the ballot box identifier
     * @return the output of all nodes for the selected ballot box
     */
    List<MixDecNodeOutput> findForAllNodes(String tenantId, String electionEventId, String ballotBoxId);
}
